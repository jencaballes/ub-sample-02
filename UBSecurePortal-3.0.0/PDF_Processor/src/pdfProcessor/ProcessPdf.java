package pdfProcessor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

import com.itextpdf.io.IOException;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.pdf.PdfDictionary;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfStream;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.xobject.PdfImageXObject;
import com.itextpdf.layout.element.Image;

public class ProcessPdf {
	private static String WHITE_IMAGE = ProcessPdf.class.getClassLoader()
			.getResource("resources/images/white.png").toString();
	private final String SOFTBANK_LOGO = ProcessPdf.class.getClassLoader()
			.getResource("resources/images/SoftBank.png").toString();
	private final float SOFTBANK_X = 521.1F;
	private final float SOFTBANK_Y = 824.9F;
	private final float CHECKMARX_X = 518.1F;
	private final float CHECKMARX_Y = 808.7F;

	private String source_file;
	private String destination_file;
	private PdfDocument pdf_doc;
	private File new_source_file;
	private File new_destination_file;

	public void setSourceFile(String basePath, String dataFolder, String uploadFolder, String pPrefix, String sPrefix,
			String dot, long lProjectId, long lScanId, String strRegistrationDate, long lReportId,
			String strReportName) {
		StringBuilder sourceFile = new StringBuilder();
		sourceFile.append(basePath);
		sourceFile.append(File.separator);
		sourceFile.append(dataFolder);
		sourceFile.append(File.separator);
		sourceFile.append(uploadFolder);
		sourceFile.append(File.separator);
		sourceFile.append(pPrefix);
		sourceFile.append(String.valueOf(lProjectId));
		sourceFile.append(File.separator);
		sourceFile.append(sPrefix);
		sourceFile.append(String.valueOf(lScanId));
		sourceFile.append(File.separator);
		sourceFile.append(strRegistrationDate);
		sourceFile.append(dot);
		sourceFile.append(String.valueOf(lScanId));
		sourceFile.append(dot);
		sourceFile.append(String.valueOf(lReportId));
		sourceFile.append(dot);
		sourceFile.append(strReportName);

		source_file = sourceFile.toString();
	}

	public void setDestinationFile(String basePath, String dataFolder, String uploadFolder, String pPrefix,
			String sPrefix, String dot, long lProjectId, long lScanId, String strRegistrationDate, long lReportId,
			String strReportName) {
		StringBuilder destinationFile = new StringBuilder();
		destinationFile.append(basePath);
		destinationFile.append(File.separator);
		destinationFile.append(dataFolder);
		destinationFile.append(File.separator);
		destinationFile.append(uploadFolder);
		destinationFile.append(File.separator);
		destinationFile.append(pPrefix);
		destinationFile.append(String.valueOf(lProjectId));
		destinationFile.append(File.separator);
		destinationFile.append(sPrefix);
		destinationFile.append(String.valueOf(lScanId));
		destinationFile.append(File.separator);
		destinationFile.append(strRegistrationDate);
		destinationFile.append(dot);
		destinationFile.append(String.valueOf(lScanId));
		destinationFile.append(dot);
		destinationFile.append(String.valueOf(lReportId));
		destinationFile.append(dot);
		destinationFile.append("NEW");
		destinationFile.append(dot);
		destinationFile.append(strReportName);

		destination_file = destinationFile.toString();
	}

	public void openPdfDoc() throws FileNotFoundException, java.io.IOException {
		pdf_doc = new PdfDocument(new PdfReader(source_file), new PdfWriter(destination_file));
	}

	public void closePdfDoc() {
		pdf_doc.close();
	}

	public void manipulatePDF() throws FileNotFoundException, IOException, java.io.IOException {
		PdfDictionary page = pdf_doc.getFirstPage().getPdfObject();
		PdfDictionary resources = page.getAsDictionary(PdfName.Resources);
		PdfDictionary xobjects = resources.getAsDictionary(PdfName.XObject);
		PdfName imgRef = xobjects.keySet().iterator().next();
		PdfStream stream = xobjects.getAsStream(imgRef);

		Image checkmarx_image = drawImage(new PdfImageXObject(stream));

		coverExistingCheckmarxLogo(stream);
		addSoftBankLogo();
		addCheckmarxLogo(checkmarx_image);

		closePdfDoc();

		// The new PDF becomes the source
		createNewSourceFile();

		// The old report becomes the destination
		createNewDestinationFile();

		// Copy the content of the new PDF to the old PDF
		copyFileUsingStream(new_source_file, new_destination_file);

		// Delete the modified PDF
		deleteNewSourceFile();
	}

	private void deleteNewSourceFile() {
		new_source_file.delete();
	}

	private void createNewDestinationFile() {
		new_destination_file = new File(source_file);
	}

	private void createNewSourceFile() {
		new_source_file = new File(destination_file);
	}

	private void addCheckmarxLogo(Image checkmarx_image) {
		checkmarx_image.scale(0.20f, 0.20f);
		ImageEventHandler checkmarx_handler = new ImageEventHandler(checkmarx_image, CHECKMARX_X, CHECKMARX_Y);
		pdf_doc.addEventHandler(PdfDocumentEvent.END_PAGE, checkmarx_handler);
	}

	private void addSoftBankLogo() throws java.io.IOException {
		Image soft_bank = new Image(ImageDataFactory.create(SOFTBANK_LOGO));
		soft_bank.scale(0.30f, 0.30f);
		ImageEventHandler soft_bank_handler = new ImageEventHandler(soft_bank, SOFTBANK_X, SOFTBANK_Y);
		pdf_doc.addEventHandler(PdfDocumentEvent.END_PAGE, soft_bank_handler);

	}

	private void coverExistingCheckmarxLogo(PdfStream stream) throws IOException, java.io.IOException {
		ImageData data = ImageDataFactory.create(WHITE_IMAGE);
		Image white_image = drawImage(new PdfImageXObject(data));
		replaceStream(stream, white_image.getXObject().getPdfObject());
	}

	private void replaceStream(PdfStream orig, PdfStream stream) throws IOException {
		orig.clear();
		orig.setData(stream.getBytes());
		for (PdfName name : stream.keySet()) {
			orig.put(name, stream.get(name));
		}
	}

	private Image drawImage(PdfImageXObject image) throws IOException, java.io.IOException {
		BufferedImage bi = image.getBufferedImage();
		BufferedImage newBi = new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_INT_ARGB);
		newBi.getGraphics().drawImage(bi, 0, 0, null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(newBi, "png", baos);
		return new Image(ImageDataFactory.create(baos.toByteArray()));
	}

	private void copyFileUsingStream(File source, File dest) throws IOException, java.io.IOException {
		FileInputStream is = null;
		FileOutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}

}
