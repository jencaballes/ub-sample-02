package jp.ubsecure.portal.jubjub.portlet.custom.exception;

/**
 * @category Error Handling
 * @since 12/09/2014
 */
@SuppressWarnings("serial")
public class UBSPortalException extends Exception {
	
	private String errorMessage;
	private String errorCode;
	
	public UBSPortalException(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public UBSPortalException(String errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
	}
	
	public UBSPortalException(String errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	
	public UBSPortalException(String errorCode, String errorMessage, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
