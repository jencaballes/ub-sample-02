package jp.ubsecure.portal.jubjub.portlet.enumclass;

public enum ScanProcess {
	UNIT_TEST(1, "単体テスト"),
	SYSTEM_TEST(2, "システムテスト"),
	REGULAR(3, "定期診断");

	int iValue;
	String strProcess;
	
	private ScanProcess(int iValue, String strProcess){
		this.iValue= iValue;
		this.strProcess = strProcess;
	}
	
	public int getInteger() {
		return iValue;
	}
	
	public String getString () {
		return strProcess;
	}
}
