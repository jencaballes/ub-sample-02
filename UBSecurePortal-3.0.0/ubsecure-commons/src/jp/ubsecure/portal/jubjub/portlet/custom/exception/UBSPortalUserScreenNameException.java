package jp.ubsecure.portal.jubjub.portlet.custom.exception;

import com.liferay.portal.kernel.exception.UserScreenNameException;

public class UBSPortalUserScreenNameException extends UserScreenNameException {
	public static class MustNotExceedMaximumBytes extends UBSPortalUserScreenNameException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5234308304950442808L;

		public MustNotExceedMaximumBytes(String message) {
			super();

			this.message = message;
		}

		public final String message;

	}
}
