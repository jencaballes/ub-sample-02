package jp.ubsecure.portal.jubjub.portlet.constants;

public class PortalMessages {

		/* -------------------- SUCCESS MESSAGES -------------------- */
		public static final String ADD_PROJECT_SUCCESSFUL			= "success-project-add";
		public static final String DELETE_PROJECT_SUCCESSFUL 		= "success-project-delete";
		public static final String UPDATE_PROJECT_SUCCESSFUL 		= "success-project-update";
		public static final String COMPLETE_PROJECT_SUCCESSFUL 		= "success-project-complete";
		public static final String OPEN_PROJECT_SUCCESSFUL			= "success-project-open";
		
		public static final String ADD_SCAN_SUCCESSFUL 				= "success-scan-add";
		public static final String STOP_SCAN_SUCCESSFUL 			= "success-scan-stop";
		public static final String REEXECUTE_SCAN_SUCCESSFUL 		= "success-scan-reexecute";
		public static final String REQUEST_REVIEW_SUCCESSFUL 		= "success-review-request";
		public static final String CANCEL_REVIEW_SUCCESSFUL 		= "success-review-cancel";
		public static final String REGENERATE_REPORT_SUCCESSFUL 	= "success-report-regenerate";
		public static final String DELETE_SCAN_SUCCESSFUL 			= "success-scan-delete";
		public static final String UPDATE_SCAN_SUCCESSFUL 			= "success-scan-update";
		public static final String RESEND_SUCCESSFUL 			    = "success-resend";
		public static final String ADD_REVIEW_SCRUTINY_SUCCESSFUL 	="success-add-review-scrutiny";
		
		public static final String RECRAWL_SUCCESSFUL 				= "success-recrawl";
		public static final String ABORT_CRAWL_SUCCESSFUL 			= "success-crawl-abort";
		public static final String INTERRUPTED_CRAWL_SUCCESSFUL 	= "success-crawl-interrupted";
		public static final String CANCEL_CRAWL_SUCCESSFUL 			= "success-crawl-cancel";
		public static final String RESTART_CRAWL_SUCCESSFUL 		= "success-crawl-restart";
		public static final String COPY_CRAWL_SETTING_SUCCESSFUL 	= "success-crawl-copy";
		public static final String INTERRUPTED_SCAN_SUCCESSFUL 		= "success-scan-interrupted";
		public static final String RESTART_SCAN_SUCCESSFUL 			= "success-scan-restart";
		public static final String COPY_CRAWL_RESULT_AND_SCAN_SUCCESSFUL = "success-scan-copy-crawl";
		public static final String CANCEL_SCAN_SUCCESSFUL			 = "success-scan-cancel";
		public static final String REGISTER_SCAN_SUCCESSFUL			 = "success-scan-register";
		
		public static final String IMPORT_SCAN_SUCCESSFUL 			= "success-scan-import";
		public static final String AUTO_CRAWL_SUCCESSFUL 			= "success-auto-crawl";
		public static final String UPLOAD_SCAN_SUCCESSFUL 			= "success-upload-scan";
	//	
	//	
	//	
//		/* ---------- EVENT MESSAGES ---------- */
		public static final String USER_EVENT_VIEW_CREATE_PROJECT 		= "VIEW PROJECT REGISTRATION";		// PROJECT LIST	(CX, ANDROID)
		public static final String USER_EVENT_ADD_PROJECT 				= "ADD PROJECT";					// PROJECT REGISTRATION (CX, ANDROID)
		public static final String USER_EVENT_VIEW_ENTIRE_SCAN_LIST 	= "VIEW ENTIRE SCAN LIST";			// PROJECT LIST (CX, ANDROID)
		public static final String USER_EVENT_SEARCH_PROJECT			= "SEARCH PROJECT";					// PROJECT LIST (CX, ANDROID)
		public static final String USER_EVENT_VIEW_SCAN_LIST 			= "VIEW SCAN LIST";					// PROJECT LIST (CX, ANDROID)
		public static final String USER_EVENT_DELETE_PROJECT 			= "DELETE PROJECT";					// PROJECT LIST (CX, ANDROID)
		public static final String USER_EVENT_SEARCH_SCAN		 		= "SEARCH SCAN";					// ENTIRE SCAN LIST, SCAN LIST (CX, ANDROID)
		public static final String USER_EVENT_STOP_SCAN 				= "STOP SCAN";						// ENTIRE SCAN LIST, SCAN LIST (CX, ANDROID)
		public static final String USER_EVENT_REEXECUTE_SCAN 			= "REEXECUTE SCAN";					// ENTIRE SCAN LIST, SCAN LIST (CX, ANDROID)
		public static final String USER_EVENT_REQUEST_REVIEW 			= "REQUEST REVIEW";					// ENTIRE SCAN LIST, SCAN LIST (CX)
		public static final String USER_EVENT_CANCEL_REVIEW 			= "CANCEL REVIEW";					// ENTIRE SCAN LIST, SCAN LIST (CX)
		public static final String USER_EVENT_REGENERATE_REPORT 		= "REGENERATE REPORT";				// ENTIRE SCAN LIST, SCAN LIST (CX)
		public static final String USER_EVENT_DELETE_SCAN 				= "DELETE SCAN";					// ENTIRE SCAN LIST, SCAN LIST (CX, ANDROID)
		public static final String USER_EVENT_VIEW_SCAN_CHANGE 			= "VIEW SCAN CHANGE";				// ENTIRE SCAN LIST, SCAN LIST (CX)
		public static final String USER_EVENT_UPDATE_SCAN 				= "UPDATE SCAN";					// SCAN CHANGE (CX)
		public static final String USER_EVENT_VIEW_PROJECT_CHANGE 		= "VIEW PROJECT CHANGE";			// SCAN LIST (CX)
		public static final String USER_EVENT_UPDATE_PROJECT 			= "UPDATE PROJECT";					// PROJECT CHANGE (CX, ANDROID)
		public static final String USER_EVENT_COMPLETE_PROJECT 			= "COMPLETE PROJECT";				// SCAN LIST (CX, ANDROID)
		public static final String USER_EVENT_VIEW_CREATE_SCAN 			= "VIEW SCAN REGISTRATION";			// SCAN LIST (CX, ANDROID)
		public static final String USER_EVENT_ADD_SCAN 					= "ADD SCAN";						// SCAN REGISTRATION (CX, ANDROID)
		public static final String USER_EVENT_VIEW_PROJECT_LIST 		= "VIEW PROJECT LIST";				// PROJECT LIST (CX, ANDROID)
		public static final String USER_EVENT_DOWNLOAD_REPORT 			= "DOWNLOAD REPORT";
		public static final String USER_EVENT_DOWNLOAD_REFERENCE		= "DOWNLOAD REFERENCE";
		public static final String USER_EVENT_DOWNLOAD_DESCRIPTION		= "DOWNLOAD VULNERABILITY DESCRIPTION";
		public static final String USER_EVENT_ADD_USER					= "ADD USER";
		public static final String USER_EVENT_EDIT_USER					= "EDIT USER";
		public static final String USER_EVENT_DELETE_USER				= "DELETE USER";		
		public static final String USER_EVENT_SEARCH_USER				= "SEARCH USER";
		public static final String USER_EVENT_CLEAR_SEARCH_USER			= "CLEAR SEARCH USER";
//		public static final String USER_EVENT_REFRESH_SCAN_LIST 		= "REFRESH SCAN LIST";
		public static final String USER_EVENT_VIEW_ANNOUNCEMENT			= "VIEW ANNOUNCEMENT";
		public static final String USER_EVENT_OPEN_PROJECT				= "OPEN PROJECT";
		public static final String USER_EVENT_DOWNLOAD_MANUAL			= "DOWNLOAD MANUAL";
		public static final String USER_EVENT_CLEAR_SEARCH_PROJECT		= "CLEAR SEARCH PROJECT";
		public static final String USER_EVENT_CLEAR_SEARCH_SCAN			= "CLEAR SEARCH SCAN";
		public static final String USER_EVENT_DOWNLOAD_SEARCHED_PROJECTS= "DOWNLOAD SEARCHED PROJECTS";
		public static final String USER_EVENT_REGISTER_SCAN_SIMPLE_SETTING     		= "REGISTER SCAN SIMPLE SETTING";
		public static final String USER_EVENT_REGISTER_SCAN_ADVANCED_SETTING     	= "REGISTER SCAN ADVANCED SETTING";
		public static final String USER_EVENT_REGISTER_SCAN_TARGET_INFORMATION   	= "REGISTER SCAN TARGET INFORMATION";
		public static final String USER_EVENT_ADD_TARGET_INFORMATION   				= "ADD TARGET INFORMATION";
		public static final String USER_EVENT_DELETE_TARGET_INFORMATION   			= "DELETE TARGET INFORMATION";
		public static final String USER_EVENT_RECRAWL					= "RECRAWL";
		public static final String USER_EVENT_ABORT_CRAWL				= "ABORT CRAWL";
		public static final String USER_EVENT_INTERRUPTED_CRAWL			= "INTERRUPTED CRAWL";
		public static final String USER_EVENT_CANCEL_CRAWL				= "CANCEL CRAWL";
		public static final String USER_EVENT_RESTART_CRAWL				= "RESTART CRAWL";
		public static final String USER_EVENT_COPY_CRAWL_SETTING		= "COPY CRAWL SETTING";
		public static final String USER_EVENT_INTERRUPTED_SCAN			= "INTERRUPTED SCAN";
		public static final String USER_EVENT_CANCEL_SCAN				= "CANCEL SCAN";
		public static final String USER_EVENT_RESTART_SCAN				= "RESTART SCAN";
		public static final String USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN= "COPY CRAWL RESULT AND SCAN";
		public static final String USER_EVENT_PERFORM_SCAN				= "PERFORM SCAN ACTION";
		public static final String USER_EVENT_REVIEW_DETECTION_RESULT 	= "REVIEW DETECTION RESULT";
		public static final String USER_EVENT_RESEND_DETECTION_RESULT   = "RESEND DETECTION RESULT";
		public static final String USER_EVENT_CLEAR_DETECTION_RESULT    = "CLEAR DETECTION RESULT";
		public static final String USER_EVENT_DOWNLOAD_SCREEN_TRANSITION_DIAGRAM = "DOWNLOAD SCREEN TRANSITION DIAGRAM"; 
		public static final String USER_EVENT_UPLOAD_SCAN 				= "UPLOAD SCAN"; 
		public static final String USER_EVENT_IMPORT_SCAN 				= "IMPORT SCAN"; 
		public static final String USER_EVENT_REGISTER_AUTOCRAWLING_SETTING = "REGISTER AUTOCRAWLING SETTING";
		public static final String USER_EVENT_CREATE_LOGIN_INFORMATION      = "CREATE LOGIN INFORMATION";	
		public static final String USER_EVENT_GET_LOGIN_INFORMATION         = "GET LOGIN INFORMATION";	
		public static final String USER_EVENT_ADD_LOGIN_INFORMATION         = "ADD LOGIN INFORMATION";
		public static final String USER_EVENT_DELETE_LOGIN_INFORMATION		= "DELETE LOGIN INFORMATION";
		public static final String USER_EVENT_UPDATE_LOGIN_INFORMATION		= "UPDATE LOGIN INFORMATION";
		public static final String USER_EVENT_SEARCH_DETECTION_RESULT 	= "SEARCH DETECTION RESULT";
		public static final String USER_EVENT_CONFIRM_DETECTION_RESULT 	= "CONFIRM DETECTION RESULT";
		public static final String GROUP_EVENT_ADD_GROUP				= "ADD GROUP";
		public static final String GROUP_EVENT_EDIT_GROUP				= "EDIT GROUP";
		public static final String GROUP_EVENT_DELETE_GROUP				= "DELETE GROUP";
		public static final String GROUP_EVENT_SEARCH_GROUP				= "SEARCH GROUP";
		public static final String GROUP_EVENT_CLEAR_SEARCH_GROUP		= "CLEAR SEARCH GROUP";
		
		public static final String USER_LOGIN							= "USER LOGIN";
		public static final String CX_SUITE_LOGIN						= "CX SUITE LOGIN";
	//	
		public static final String CHANGE_PWD							= "CHANGE PASSWORD";
		
		public static final String USER_EVENT_VIEW_REGISTER_SCAN_SIMPLE_SETTING      = "VIEW REGISTER SCAN SIMPLE SETTING";
		public static final String USER_EVENT_VIEW_REGISTER_SCAN_ADVANCED_SETTING    = "VIEW REGISTER SCAN ADVANCED SETTING";
		public static final String USER_EVENT_VIEW_REGISTER_SCAN_TARGET_INFORMATION  = "VIEW REGISTER SCAN TARGET INFORMATION";
		public static final String USER_EVENT_VIEW_REGISTER_SCAN_AUTOCRAWL_SETTING   = "VIEW REGISTER SCAN AUTOCRAWL SETTING";
		public static final String USER_EVENT_VIEW_REGISTER_SCAN_LOGIN_SETTING       = "VIEW REGISTER SCAN LOGIN SETTING";
//		public static final String INITIAL_CHANGE_PWD 					= "INITIAL PASSWORD CHANGE";
	//	
	//	
	//	
		public static final String USER_ID_INVALID							= "error-user-id-invalid";
		public static final String USER_DOES_NOT_EXIST						= "error-user-does-not-exist";
		public static final String USER_NO_RIGHTS 							= "message-no-access-rights";
		public static final String USER_NO_RIGHTS_ACTION					= "message-no-access-rights-action";
	//	
		public static final String USER_INVALID								= "error-user-invalid";
		public static final String PROJECT_INVALID							= "error-project-invalid";
		public static final String SCAN_INVALID								= "error-scan-invalid";
		public static final String FILE_INVALID 							= "error-file-invalid";
		public static final String FILE_PATH_INVALID 						= "error-file-path-invalid";
		
		public static final String CASE_NUMBER_INVALID						= "error-case-number-invalid";
		public static final String CASE_NUMBER_TOO_LONG						= "error-case-number-is-too-long";
		public static final String CASE_NUMBER_ALREADY_EXIST				= "error-case-number-already-exists";
		
		public static final String PROJECT_NAME_TOO_LONG					= "error-project-name-is-too-long";
		public static final String PROJECT_NAME_INVALID						= "error-project-name-invalid";
		public static final String NO_PROJECT_NAME							= "error-please-enter-a-project-name";
		public static final String PROJECT_NAME_STARTS_WITH_CX_				= "error-project-name-starts-with-cx_";
		public static final String NO_CASE_NAME								= "error-please-enter-a-case-name";
		public static final String NO_TARGET_URL							= "error-please-enter-a-target-url";
		public static final String NO_PRODUCTION_ENVIRONMENT_URL			= "error-please-enter-a-production-environment-url";
		public static final String NO_TARGET_URL_HOST							= "error-please-enter-a-target-url-host";
		public static final String NO_PRODUCTION_ENVIRONMENT_URL_HOST			= "error-please-enter-a-production-environment-url-host";
		public static final String NO_TARGET_URL_PROTOCOL						= "error-please-enter-a-target-url-protocol";
		public static final String NO_PRODUCTION_ENVIRONMENT_URL_PROTOCOL		= "error-please-enter-a-production-environment-url-protocol";
		public static final String NO_TARGET_URL_PORT							= "error-please-enter-a-target-url-port";
		public static final String NO_PRODUCTION_ENVIRONMENT_URL_PORT			= "error-please-enter-a-production-environment-url-port";
		
		public static final String PROJECT_TARGET_URL_INVALID				  			= "error-invalid-project-target-url-value";
		public static final String PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID 			= "error-invalid-project-production-environment-url-value";
		public static final String PROJECT_TARGET_URL_PROTOCOL_INVALID				  	= "error-invalid-project-target-url-protocol-value";
		public static final String PROJECT_PRODUCTION_ENVIRONMENT_URL_PROTOCOL_INVALID 	= "error-invalid-project-production-environment-url-protocol-value";
		public static final String PROJECT_TARGET_URL_HOST_INVALID				 		= "error-invalid-project-target-url-host-value";
		public static final String PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_INVALID 		= "error-invalid-project-production-environment-url-host-value";
		public static final String PROJECT_TARGET_URL_HOST_LOCALHOST_INVALID				 		= "error-invalid-project-target-url-host-localhost-value";
		public static final String PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_LOCALHOST_INVALID 		= "error-invalid-project-production-environment-url-host-localhost-value";
		public static final String PROJECT_TARGET_URL_PORT_INVALID				 		= "error-invalid-project-target-url-port-value";
		public static final String PROJECT_PRODUCTION_ENVIRONMENT_URL_PORT_INVALID 		= "error-invalid-project-production-environment-url-port-value";
		public static final String PROJECT_TARGET_URL_LOCALHOST_INVALID				    = "error-invalid-project-target-url-localhost-value";
		public static final String PROJECT_PRODUCTION_ENVIRONMENT_URL_LOCALHOST_INVALID = "error-invalid-project-production-environment-url-localhost-value";
		public static final String PROJECT_ID_INVALID						= "error-invalid-project-id-value";
		
		public static final String PROJECT_DOES_NOT_EXIST					= "error-project-does-not-exist";
		
		public static final String PROJECT_TYPE_NOT_CX						= "error-project-project-type-not-cxsuite";
		public static final String PROJECT_TYPE_NOT_ANDROID					= "error-project-project-type-not-android";
		public static final String PROJECT_TYPE_NOT_IOS						= "error-project-project-type-not-ios";
		public static final String PROJECT_TYPE_NOT_VEX						= "error-project-project-type-not-vex";
		
		public static final String PROJECT_STATUS_INVALID					= "error-project-status-invalid";
			
		public static final String PACKAGE_NAME_TOO_LONG					= "error-package-name-too-long";
		public static final String PACKAGE_NAME_INVALID						= "error-package-name-invalid";
		
		public static final String NO_ATTRIBUTE								= "error-please-select-an-attribute";
			
		public static final String NO_OWNER_GROUP							= "error-please-select-a-group";
		public static final String OWNER_GROUP_DOES_NOT_EXIST				= "error-group-does-not-exist";
		
		public static final String NO_PRESET_ID								= "error-please-select-a-preset-id";
		public static final String PRESET_DOES_NOT_EXIST					= "error-preset-does-not-exist";
		public static final String PRESET_ID_INVALID						= "error-preset-id-invalid";
			
		public static final String NO_PROJECT_END_DATE						= "error-please-select-a-project-end-date";
		public static final String PROJECT_END_DATE_INVALID					= "error-invalid-project-end-date";
		
		public static final String CHECKLIST_FILE_SIZE_TOO_BIG				= "error-file-size-is-too-big-20";
		
		public static final String PROJECT_USER_DOES_NOT_EXIST				= "error-project-user-does-not-exist";
		public static final String PROJECT_USER_DOES_NOT_BELONG_TO_GROUP	= "error-user-does-not-belong-to-the-group";
		public static final String NO_PROJECT_USERS							= "error-please-select-a-user";
		
		public static final String SCAN_ID_INVALID							= "error-invalid-scan-id-value";
		
		public static final String SCAN_DOES_NOT_EXIST						= "error-scan-does-not-exist";
		
		public static final String APK_FILE_NAME_TOO_LONG					= "error-file-name-too-long";
		public static final String APK_FILE_SIZE_TOO_BIG					= "error-file-size-is-too-big-300";
		public static final String ZIP_FILE_SIZE_TOO_BIG					= "error-file-size-is-too-big-200";
		public static final String ZIP_FILE_SIZE_TOO_BIG_IOS				= "error-file-size-is-too-big-300";
		public static final String ZIP_FILE_SIZE_TOO_BIG_VEX				= "error-file-size-is-too-big-100";
		public static final String EXP_FILE_SIZE_TOO_BIG_VEX				= "error-file-size-is-too-big-30000";
		
		public static final String NO_PROCESS								= "error-please-select-a-process";
		
		public static final String SCAN_REGISTRATION_DATE_INVALID			= "error-invalid-scan-registration-date";
		
		public static final String ORGANIZATION_ID_INVALID					= "error-organization-id-invalid";
		
		public static final String REPORT_ID_INVALID						= "error-invalid-report-id-value";
		
//		public static final String REPORT_NAME_INVALID						= "error-report-name-invalid";
		
		public static final String REPORT_TYPE_INVALID						= "error-report-type-invalid";
		
//		public static final String REPORT_STATUS_INVALID					= "error-report-status-invalid";
		
		public static final String NO_REPORT								= "error-no-report-for-the-scan";
		public static final String NO_DESCRIPTION							= "error-no-vulnerability-description";
		
		public static final String CX_SESSION_ID_INVALID					= "error-cx-session-id-invalid";
		
		public static final String START_TIME_INVALID						= "error-start-time-invalid";
		public static final String END_TIME_INVALID							= "error-end-time-invalid";
		public static final String START_URL_INVALID						= "error-start-url-invalid";
		public static final String AUTHORIZED_PATROL_URL_INVALID			= "error-authorized-patrol-url-invalid";
		public static final String START_URL_LOCALHOST_INVALID						= "error-start-url-localhost-invalid";
		public static final String AUTHORIZED_PATROL_URL_LOCALHOST_INVALID			= "error-authorized-patrol-url-localhost-invalid";
		public static final String LOGIN_URL_INVALID						        = "error-login-url-invalid";
		public static final String LOGIN_URL_LOCALHOST_INVALID						= "error-login-url-localhost-invalid";
		public static final String PARAMETER_NAME_ID_INVALID				= "error-parameter-name-id-invalid";
		public static final String PARAMETER_NAME_PASS_INVALID				= "error-parameter-name-pass-invalid";
		public static final String NO_START_URL						        = "error-please-enter-start-url";
		public static final String NO_AUTHORIZED_PATROL_URL			        = "error-please-enter-authorized-patrol-url";
		public static final String NO_LOGIN_URL						        = "error-please-enter-login-url";
		public static final String NO_PARAMETER_NAME_ID						= "error-please-enter-parameter-name-id";
		public static final String NO_PARAMETER_NAME_PASS					= "error-please-enter-parameter-name-pass";
		public static final String PROTOCOL_INVALID							= "error-protocol-invalid";
		public static final String HOST_INVALID								= "error-host-invalid";
		public static final String HOST_LOCALHOST_INVALID					= "error-host-localhost-invalid";
		public static final String PORT_INVALID								= "error-port-invalid";
		public static final String NO_HOST								    = "error-please-enter-host";
		public static final String NO_PORT								    = "error-please-enter-port";
		public static final String SCAN_NAME_INVALID						= "error-scan-name-invalid";
		public static final String NO_SCAN_NAME						        = "error-please-enter-a-scan-name";
		public static final String TARGET_INFORMATION_LIST_INVALID			= "error-target-information-list-invalid";
		public static final String NO_TARGET_INFORMATION_LIST			    = "error-please-enter-at-least-one-target-information";
		public static final String START_TIME_AND_END_TIME_INVALID			= "error-start-time-and-end-time-invalid";
		public static final String NO_START_DATE							= "error-start-time-date-empty";
		public static final String NO_END_DATE								= "error-end-time-date-empty";
		public static final String IP_ADDRESS_INVALID						= "error-ip-address-invalid";
		public static final String NO_IP_ADDRESS							= "error-please-enter-ip-address";
		public static final String LOGIN_PARAM_VALUE_INVALID				= "error-login-param-value-invalid";
		public static final String SESSION_GET_SCAN_NAME_FAILED		                = "error-failed-to-get-scan-name-from-previous-screens";
		public static final String SESSION_GET_SCAN_SETTING_FAILED		            = "error-failed-to-get-scan-settings-from-previous-screens";
		public static final String SESSION_GET_TARGET_INFORMATION_LIST_FAILED		= "error-failed-to-get-target-information-list-from-previous-screens";
		public static final String GET_CRAWL_LOGIN_INFO_FAILED						= "error-failed-to-get-crawl-login-info";
		public static final String CREATE_TEMPORARY_PROJECT_FAILED					= "error-failed-to-create-temporary-project-for-get-crawl-login-info";
		public static final String DELETE_TEMPORARY_PROJECT_FAILED					= "error-failed-to-delete-temporary-project-for-get-crawl-login-info";
		public static final String NO_LOGIN_PARAMETER_VALUE							= "error-please-enter-login-parameter-value";
		public static final String REVIEW_COMMENT_EMPTY								= "error-review-comment-empty";
		public static final String REVIEW_COMMENT_INVALID							= "error-review-comment-invalid";
		public static final String REVIEW_COMMENT_TOO_LONG							= "error-review-comment-too-long";
		public static final String DETECTION_NUMBER_INVALID							= "error-detection-number-invalid";
		public static final String DETECTION_RESULT_ID_EMPTY				= "error-detection-result-id-empty";
		public static final String INVALID_CHARACTER_INPUT 					= "error-invalid-character-input";
		public static final String UNUSABLE_CHARACTER_INPUT					= "error-unusable-character-input";
		
		public static final String NO_EXTERNAL_PROXY_HOST					= "error-no-external-proxy-host";
		public static final String NO_EXTERNAL_PROXY_PORT					= "error-no-external-proxy-port";
		public static final String NO_EXTERNAL_PROXY_AUTH_ID				= "error-no-external-proxy-auth-id";
		public static final String NO_EXTERNAL_PROXY_AUTH_PASSWORD			= "error-no-external-proxy-auth-password";
		
		public static final String NO_USE_CLIENT_CERTIFICATE				= "error-no-use-client-certificate";
		public static final String NO_CERTIFICATE_FILE						= "error-no-certificate-file";
		public static final String NO_CERTIFICATE_PASSWORD					= "error-no-certificate-password";
		
		public static final String NO_NTLM_AUTH_ID							= "error-no-ntlm-auth-id";
		public static final String NO_NTLM_AUTH_PASSWORD					= "error-no-ntlm-password";
		public static final String NO_NTLM_AUTH_DOMAIN						= "error-no-ntlm-auth-domain";
		public static final String NO_NTLM_AUTH_HOST						= "error-no-ntlm-auth-host";
		
		public static final String NO_DIGEST_AUTH_ID						= "error-no-digest-auth-id";
		public static final String NO_DIGEST_AUTH_PASSWORD					= "error-no-digest-auth-password";
		
		public static final String NO_BASIC_AUTH_ID							= "error-no-basic-auth-id";
		public static final String NO_BASIC_AUTH_PASSWORD					= "error-no-basic-auth-password";
		
		public static final String EXTERNAL_PROXY_HOST_INVALID				= "error-invalid-external-proxy-host";
		public static final String EXTERNAL_PROXY_HOST_LOCALHOST_INVALID	= "error-invalid-external-proxy-host-localhost";
		public static final String EXTERNAL_PROXY_PORT_INVALID				= "error-invalid-external-proxy-port";
		public static final String EXTERNAL_PROXY_AUTH_ID_INVALID			= "error-invalid-external-proxy-auth-id";
		public static final String EXTERNAL_PROXY_AUTH_PASSWORD_INVALID		= "error-invalid-external-proxy-auth-password";
		
		public static final String USE_CLIENT_CERTIFICATE_INVALID			= "error-invalid-use-client-certificate";
		public static final String CERTIFICATE_FILE_INVALID					= "error-invalid-certificate-file";
		public static final String CERTIFICATE_PASSWORD_INVALID				= "error-invalid-certificate-password";
		
		public static final String NTLM_AUTH_ID_INVALID						= "error-invalid-ntlm-auth-id";
		public static final String NTLM_AUTH_PASSWORD_INVALID				= "error-invalid-ntlm-password";
		public static final String NTLM_AUTH_DOMAIN_INVALID					= "error-invalid-ntlm-auth-domain";
		public static final String NTLM_AUTH_DOMAIN_LOCALHOST_INVALID		= "error-invalid-ntlm-auth-domain-localhost";
		public static final String NTLM_AUTH_HOST_INVALID					= "error-invalid-ntlm-auth-host";
		public static final String NTLM_AUTH_HOST_LOCALHOST_INVALID			= "error-invalid-ntlm-auth-host-localhost";
		
		public static final String DIGEST_AUTH_ID_INVALID					= "error-invalid-digest-auth-id";
		public static final String DIGEST_AUTH_PASSWORD_INVALID				= "error-invalid-digest-auth-password";
		
		public static final String BASIC_AUTH_ID_INVALID					= "error-invalid-basic-auth-id";
		public static final String BASIC_AUTH_PASSWORD_INVALID				= "error-invalid-basic-auth-password";
		
		public static final String ACCESS_EXCLUSION_PATH_INVALID			= "error-invalid-access-exclusion-path";
		
		public static final String UNAUTHORIZED_PATROL_URL_INVALID				= "error-unauthorized-patrol-url-invalid";
		public static final String PARAMETER_NAME_AS_PASSWORD_INVALID			= "error-parameter-name-as-password-invalid";
		public static final String MAX_NUM_DETECTION_LINK_INVALID				= "error-max-num-detection-link-invalid";
		public static final String MAX_NUM_DETECTION_LINK_PER_PAGE_INVALID		= "error-max-num-detection-link-per-page-invalid";
		public static final String DETECTION_LINK_DEPTH_LIMIT_INVALID			= "error-detection-link-depth-limit-invalid";
		public static final String WAIT_TIME_INVALID							= "error-wait-time-invalid";
		public static final String NUM_OF_THREAD_INVALID						= "error-num-of-thread-invalid";
		public static final String TIME_OUT_TIME_INVALID			   			= "error-time-out-time-invalid";
		public static final String REQUEST_HEADER_INVALID						= "error-request-header-invalid";
		public static final String NUM_OF_RETRY_AT_ERROR_INVALID				= "error-num-of-retry-at-error-invalid";
		public static final String LOGIN_FORM_PATH_INVALID				        = "error-login-form-path-invalid";
		public static final String LOGIN_URL_NOT_FOUND_IN_TARGET_INFORMATION_LIST	= "error-login-url-is-not-found-in-target-information";
		public static final String LOGIN_STATUS_DETECTION_INVALID               = "error-login-status-detection-invalid";
		public static final String SESSION_ERROR_DETECTION_INVALID              = "error-session-error-detection-invalid";
		public static final String SCREEN_TRANSITION_ERROR_DETECTION_INVALID    = "error-screen-transition-error-detection-invalid";
		public static final String REGULAR_EXPRESSION_INVALID    				= "error-regular-expression-invalid";
		public static final String REGULAR_EXPRESSION_GROUPING_INVALID    		= "error-regular-expression-grouping-invalid";
		
		public static final String NO_UNAUTHORIZED_PATROL_URL				= "error-please-enter-unauthorized-patrol-url";
		public static final String NO_MAX_NUM_DETECTION_LINK				= "error-please-enter-max-num-detection-link";
		public static final String NO_MAX_NUM_DETECTION_LINK_PER_PAGE		= "error-please-enter-max-num-detection-link-per-page";
		public static final String NO_DETECTION_LINK_DEPTH_LIMIT			= "error-please-enter-detection-link-depth-limit";
		public static final String NO_WAIT_TIME								= "error-please-enter-wait-time";
		public static final String NO_NUM_OF_THREAD							= "error-please-enter-num-of-thread";
		public static final String NO_TIME_OUT_TIME			   				= "error-please-enter-time-out-time";
		public static final String NO_REQUEST_HEADER						= "error-please-enter-request-header";
		public static final String NO_NUM_OF_RETRY_AT_ERROR					= "error-please-enter-num-of-retry-at-error";
		public static final String NO_LOGIN_FORM_PATH						= "error-please-enter-login-form-path";
		
		public static final String SCAN_CANNOT_BE_STOPPED					= "error-scan-cannot-be-stopped";
		public static final String SCAN_CANNOT_BE_REEXECUTED				= "error-scan-cannot-be-reexecuted";
		public static final String REVIEW_CANNOT_BE_REQUESTED				= "error-request-review-cannot-be-sent";
		public static final String REVIEW_CANNOT_BE_CANCELLED				= "error-review-cannot-be-cancelled";
		public static final String REPORT_CANNOT_BE_REGENERATED				= "error-report-cannot-be-regenerated";
		public static final String CRAWL_CANNOT_BE_RECRAWLED 				= "error-crawl-cannot-be-recrawl";
		public static final String CRAWL_CANNOT_BE_ABORTED 					= "error-crawl-cannot-be-abort";
		public static final String CRAWL_CANNOT_BE_INTERRUPTEDED 			= "error-crawl-cannot-be-interrupted";
		public static final String CRAWL_CANNOT_BE_CANCELED					= "error-crawl-cannot-be-cancel";
		public static final String CRAWL_CANNOT_BE_RESTARTED 				= "error-crawl-cannot-be-restart";
		public static final String CRAWL_CANNOT_BE_COPIED 					= "error-crawl-cannot-be-copy";
		public static final String SCAN_CANNOT_BE_INTERRUPTED				= "error-scan-cannot-be-interrupted";
		public static final String SCAN_CANNOT_BE_RESTARTED 				= "error-scan-cannot-be-restart";
		public static final String SCAN_CANNOT_BE_COPIED_AND_SCANNED 		= "error-scan-cannot-be-copy-crawl";
		public static final String SCAN_CANNOT_BE_CANCELED					= "error-scan-cannot-be-cancel";
		public static final String SCAN_CANNOT_BE_UPDATED					= "error-scan-cannot-be-updated";
		
//		public static final String SCAN_STATUS_INVALID						= "error-scan-status-invalid";
	//	
		public static final String CX_ANDROID_PROJECT_ID_INVALID			= "error-cx-invalid-project-id-value";
	//	
		public static final String CX_ANDROID_SCAN_ID_INVALID				= "error-cx-invalid-scan-id-value";
	//	
//		public static final String CX_SCAN_STATUS_INVALID					= "error-cx-scan-status-invalid";
	//	
		public static final String SYSTEM_EXCEPTION							= "error-system-exception";
		public static final String PORTAL_EXCEPTION							= "error-portal-exception";
		public static final String ORM_EXCEPTION							= "error-orm-exception";
		public static final String COMMON_EXCEPTION							= "error-common-exception";
		public static final String FILE_NOT_FOUND_EXCEPTION					= "error-file-not-found-exception";
		public static final String IO_EXCEPTION								= "error-io-exception";
		public static final String MALFORMEDURL_EXCEPTION					= "error-malformed-url-exception";
	//	
		public static final String ADD_SCAN_FAILED							= "error-scan-create-failed";
		public static final String UPDATE_SCAN_FAILED						= "error-scan-update-failed";
		public static final String ADD_PROJECT_FAILED 						= "error-project-create-failed";
		public static final String UPDATE_PROJECT_FAILED					= "error-project-update-failed";
		public static final String COMPLETE_PROJECT_FAILED					= "error-project-cannot-be-completed";
//		public static final String ANDROID_UPDATE_SCAN_STATUS_FAILED		= "error-android-scan-update-failed";
//		public static final String CX_UPDATE_SCAN_STATUS_FAILED				= "error-cx-scan-update-failed";
		public static final String STOP_SCAN_FAILED							= "error-stop-scan-failed";
		public static final String REEXECUTE_SCAN_FAILED					= "error-reexecute-scan-failed";
		public static final String REQUEST_REVIEW_FAILED					= "error-request-review-failed";
		public static final String CANCEL_REVIEW_FAILED						= "error-cancel-review-failed";
		public static final String REGENERATE_REPORT_FAILED					= "error-regenerate-report-failed";
		public static final String DELETE_SCAN_FAILED						= "error-scan-delete-failed";
		public static final String DELETE_REPORT_FAILED						= "error-report-delete-failed";
		public static final String DELETE_PROJECT_FAILED 					= "error-project-delete-failed";
		public static final String OPEN_PROJECT_FAILED						= "error-project-open-failed";
		
		public static final String ADD_VEX_SCAN_FAILED						= "error-vex-scan-create-failed";
		
		public static final String RECRAWL_FAILED							= "error-re-crawl-failed";
		public static final String ABORT_CRAWL_FAILED						= "error-crawl-abort-failed";
		public static final String INTERRUPTED_CRAWL_FAILED					= "error-crawl-interrupted-failed";
		public static final String CANCEL_CRAWL_FAILED						= "error-crawl-cancel-failed";
		public static final String RESTART_CRAWL_FAILED						= "error-crawl-restart-failed";
		public static final String COPY_CRAWL_SETTING_FAILED				= "error-crawl-copy-setting-failed";
		public static final String INTERRUPTED_SCAN_FAILED					= "error-scan-interrupted-failed";
		public static final String CANCEL_SCAN_FAILED						= "error-scan-cancel-failed";
		public static final String RESTART_SCAN_FAILED						= "error-scan-restart-failed";
		public static final String COPY_CRAWL_RESULT_AND_SCAN_FAILED		= "error-crawl-copy-setting-scan-failed";
		public static final String AUTO_CRAWL_FAILED						= "error-crawl-auto-failed";
		public static final String UPLOAD_SCAN_FAILED						= "error-upload-scan-failed";
		public static final String IMPORT_SCAN_FAILED						= "error-import-scan-failed";
		public static final String UPLOAD_CERTIFICATE_FAILED			    = "error-upload-certificate-failed";
		public static final String RESEND_FAILED							= "error-resend-failed";
		public static final String ADD_REVIEW_SCRUTINY_FAILED 			= "error-add-review-scrutiny";
		
	//	
		public static final String ANDO_REGISTER_SCAN_FAILED				= "ando-register-scan-failed";
		public static final String ANDO_STOP_SCAN_FAILED					= "ando-stop-scan-failed";
		public static final String ANDO_REEXECUTE_SCAN_FAILED				= "ando-reexecute-scan-failed";
		public static final String ANDO_DELETE_SCAN_FAILED					= "ando-delete-scan-failed";
		public static final String ANDO_CREATE_PROJECT_FAILED				= "ando-create-project-failed";
		public static final String ANDO_UPDATE_PROJECT_FAILED				= "ando-update-project-failed";
		public static final String ANDO_DELETE_PROJECT_FAILED				= "ando-delete-project-failed";
		public static final String ANDO_GET_PACKAGE_NAMES_FAILED			= "ando-get-package-names-failed";
	//	
		public static final String CX_REGISTER_SCAN_FAILED					= "cx-register-scan-failed";
		public static final String CX_REEXECUTE_SCAN_FAILED					= "cx-reexecute-scan-failed";
		public static final String CX_DELETE_SCAN_FAILED					= "cx-delete-scan-failed";
		public static final String CX_UPDATE_PROJECT_FAILED					= "cx-update-project-failed";
		public static final String CX_DELETE_PROJECT_FAILED					= "cx-delete-project-failed";
		public static final String CX_REGENERATE_REPORT_FAILED				= "cx-regenerate-report-failed";
		public static final String CX_CANCEL_SCAN_FAILED					= "cx-cancel-scan-failed";
	//	
		public static final String IOS_REGISTER_SCAN_FAILED					= "ios-register-scan-failed";
		public static final String IOS_REEXECUTE_SCAN_FAILED				= "ios-reexecute-scan-failed";
		public static final String IOS_DELETE_SCAN_FAILED					= "ios-delete-scan-failed";
		public static final String IOS_UPDATE_PROJECT_FAILED				= "ios-update-project-failed";
		public static final String IOS_DELETE_PROJECT_FAILED				= "ios-delete-project-failed";
		public static final String IOS_REGENERATE_REPORT_FAILED				= "ios-regenerate-report-failed";
		public static final String IOS_CANCEL_SCAN_FAILED					= "ios-cancel-scan-failed";
	//	
		public static final String VEX_REGISTER_SCAN_FAILED					= "vex-register-scan-failed";
		public static final String VEX_REEXECUTE_SCAN_FAILED				= "vex-reexecute-scan-failed";
		public static final String VEX_DELETE_SCAN_FAILED					= "vex-delete-scan-failed";
		public static final String VEX_UPDATE_PROJECT_FAILED				= "vex-update-project-failed";
		public static final String VEX_DELETE_PROJECT_FAILED				= "vex-delete-project-failed";
		public static final String VEX_REGENERATE_REPORT_FAILED				= "vex-regenerate-report-failed";
		public static final String VEX_CANCEL_SCAN_FAILED					= "vex-cancel-scan-failed";
		
		public static final String VEX_RECRAWL_FAILED							= "vex-re-crawl-failed";
		public static final String VEX_ABORT_CRAWL_FAILED						= "vex-crawl-abort-failed";
		public static final String VEX_INTERRUPTED_CRAWL_FAILED					= "vex-crawl-interrupted-failed";
		public static final String VEX_CANCEL_CRAWL_FAILED						= "vex-crawl-cancel-failed";
		public static final String VEX_RESTART_CRAWL_FAILED						= "vex-crawl-restart-failed";
		public static final String VEX_COPY_CRAWL_SETTING_FAILED				= "vex-crawl-copy-setting-failed";
		public static final String VEX_INTERRUPTED_SCAN_FAILED					= "vex-scan-interrupted-failed";
		public static final String VEX_RESTART_SCAN_FAILED						= "vex-scan-restart-failed";
		public static final String VEX_COPY_CRAWL_RESULT_AND_SCAN_FAILED		= "vex-crawl-copy-setting-scan-failed";
		public static final String VEX_AUTO_CRAWL_FAILED						= "vex-crawl-auto-failed";
		public static final String VEX_HOST_INVALID							= "vex-host-invalid";
	//	
		public static final String NEXT_PAGINATION_ERROR					= "next-pagination-error";
	//	
		public static final String RECIPIENTS_INVALID 						= "Invalid Recipients";
		public static final String EMAIL_EVENT_INVALID 						= "Invalid Email Event";
		public static final String ENTITY_ID_INVALID 						= "Invalid Entity ID";
		public static final String COMPANY_ID_INVALID 						= "Invalid Company ID";
	//	
		public static final String ADDRESS_EXCEPTION 						= "AddressException";
		public static final String ROLE_INVALID 							= "Invalid role";
	//	
		public static final String EMAIL_ADDRESS_INVALID					= "error-email-address-invalid";
	//	
		public static final String NO_MANUAL								= "error-no-manual";
		public static final String DOWNLOAD_MANUAL_FAILED					= "error-download-manual-failed";
		public static final String SCAN_COUNT_INVALID						= "error-scan-count-invalid";
		public static final String HASH_VALUE_INVALID						= "error-hash-value-invalid";
		public static final String CX_SCAN_ID_INVALID						= "error-cx-scan-id-invalid";
		public static final String NO_OF_USERS_INVALID 						= "error-no-of-users-invalid";
		public static final String LAST_LOGIN_DATE_INVALID					= "error-last-login-date-invalid";
		public static final String EXPIRY_DATE_INVALID						= "error-expiration-date-invalid";
		public static final String GROUP_NAME_TOO_LONG						= "error-group-search-byte-size";
	//	
		public static final String USER_EVENT_DOWNLOAD_SUMMARY				= "DOWNLOAD SUMMARY";
		public static final String PERIOD_INVALID							= "error-period-invalid";
		public static final String NO_SUMMARY								= "error-no-summary";
		public static final String NO_PROJECT_LIST							= "error-no-project-list";
		public static final String DOWNLOAD_SUMMARY_ERROR					= "error-download-manual-failed";
		
		public static final String APK_FILE_NAME_CONTAINS_MULTIBYTE_CHARACTERS	= "error-file-name-contains-multibyte-characters";
		public static final String IMPORT_SCAN_NO_INSPECTION_LOG   			    = "error-import-scan-no-inspection-log";
		public static final String NO_WEB_INSPECTION_SIGNATURE_SET_GROUP        = "error-empty-web-inspection-signature-set-group";
		public static final String NO_SERVER_FILE_SIGNATURE_SET_GROUP           = "error-empty-server-file-signature-set-group";
		public static final String NO_SERVER_SETTING_SIGNATURE_SET_GROUP        = "error-empty-server-setting-signature-set-group";
		public static final String START_TIME_END_TIME_DIFFERENCE				= "error-end-time-greater-than-start-time-by-10-minutes";
		
		public static final String SOURCE_CODE_MAX_LINE_750000				= "error-no-of-lines-is-too-big-750000";
		public static final String UPLOAD_FILE_ALREADY_UPLOADED				= "error-upload-file-already-uploaded";
		
		public static final String VEX_ERROR_MESSAGE_CRAWLING_FAILURE				= "自動巡回に失敗しました。";
		public static final String VEX_ERROR_MESSAGE_SCANNING_FAILURE				= "スキャンに失敗しました。";		
}
