package jp.ubsecure.portal.jubjub.portlet.enumclass;

public enum ReportType {
	PDF_REPORT(1),
	XML_REPORT(2),
	CSV_META_INFORMATION(3),
	CSV_VULNERABILITY(4),
	ANALYZE_REPORT(5),
	WORD_REPORT(6),
	XLS_REPORT(7);

	int iValue;
	
	private ReportType(int iValue){
		this.iValue= iValue;
	}
	
	public int getInteger() {
		return iValue;
	}
}