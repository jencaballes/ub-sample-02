package jp.ubsecure.portal.jubjub.portlet.enumclass;

public enum ProjectType {
	CX_SUITE(1),
	ANDROID(2),
	IOS(3),
	VEX(4);
	
	int iValue;
	
	private ProjectType(int iValue){
		this.iValue= iValue;
	}
	
	public int getInteger() {
		return iValue;
	}
}