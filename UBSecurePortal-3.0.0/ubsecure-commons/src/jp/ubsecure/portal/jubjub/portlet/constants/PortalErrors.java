package jp.ubsecure.portal.jubjub.portlet.constants;

public class PortalErrors {

	/* ---------- ERROR CODES ---------- */
	public static final String ADD_PROJECT_FAILED 								= "U.Portal.Failed.AddProject.0001";				// PORTLET
//	public static final String DELETE_PROJECT_FAILED 							= "U.Portal.Failed.DeleteProject.0001";			// PORTLET
	public static final String STOP_SCAN_FAILED 								= "U.Portal.Failed.StopScan.0001";				// PORTLET
	public static final String REEXECUTE_SCAN_FAILED 							= "U.Portal.Failed.ReexecuteScan.0001";			// PORTLET
	public static final String REQUEST_REVIEW_FAILED 							= "U.Portal.Failed.RequestReview.0001";			// PORTLET
	public static final String CANCEL_REVIEW_FAILED								= "U.Portal.Failed.CancelReview.0001";			// PORTLET
	public static final String REGENERATE_REPORT_FAILED 						= "U.Portal.Failed.RegenerateReport.0001";		// PORTLET
	public static final String DELETE_SCAN_FAILED 								= "U.Portal.Failed.DeleteScan.0001";				// PORTLET
	public static final String UPDATE_PROJECT_FAILED							= "U.Portal.Failed.UpdateProject.0001";			// PORTLET
	public static final String COMPLETE_PROJECT_FAILED 							= "U.Portal.Failed.CompleteProject.0001";		// PORTLET
	public static final String OPEN_PROJECT_FAILED								= "U.Portal.Failed.OpenProject.0001";
	public static final String ADD_SCAN_FAILED 									= "U.Portal.Failed.AddScan.0001";				// PORTLET
	public static final String UPDATE_SCAN_FAILED	 							= "U.Portal.Failed.UpdateScan.0001";				// PORTLET
	public static final String RECRAWL_FAILED						           	= "U.Portal.Failed.ReCrawl.0001";
	public static final String ABORT_CRAWL_FAILED								= "U.Portal.Failed.AbortCrawl.0001";
	public static final String INTERRUPTED_CRAWL_FAILED							= "U.Portal.Failed.InterruptedCrawl.0001";
	public static final String CANCEL_CRAWL_FAILED								= "U.Portal.Failed.CancelCrawl.0001";
	public static final String RESTART_CRAWL_FAILED								= "U.Portal.Failed.RestartCrawl.0001";
	public static final String COPY_CRAWL_SETTING_FAILED						= "U.Portal.Failed.CopyCrawlSetting.0001";
	public static final String INTERRUPTED_SCAN_FAILED							= "U.Portal.Failed.InterruptedScan.0001";
	public static final String RESTART_SCAN_FAILED								= "U.Portal.Failed.RestartScan.0001";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_FAILED				= "U.Portal.Failed.CopyCrawlResultAndScan.0001";
	public static final String CANCEL_SCAN_FAILED								= "U.Portal.Failed.CancelScan.0001";
	public static final String IMPORT_SCAN_FAILED								= "U.Portal.Failed.ImportScan.0001";
	public static final String UPLOAD_SCAN_FAILED								= "U.Portal.Failed.UploadScan.0001";
	public static final String AUTO_CRAWL_FAILED								= "U.Portal.Failed.AutoCrawl.0001";
	public static final String ADD_VEX_SCAN_FAILED 								= "U.Portal.Failed.AddVexScan.0001";
	public static final String GET_CRAWL_LOGIN_INFO_FAILED 						= "U.Portal.Failed.GetLoginInfo.0001";
	public static final String UPDATE_CRAWL_LOGIN_SETTING_LIST_FAILED 			= "U.Portal.Failed.UpdateLoginSettingList.0001";
	public static final String GET_SESSION_DATA_FAILED 						    = "U.Portal.Failed.GetSessionData.0001";
	public static final String RESEND_FAILED									= "U.Portal.Failed.Resend.0001";
	public static final String VEX_GET_VEX_USERS								= "U.Portal.Get.VexUser.0001";
	public static final String SET_LOGIN_CREDENTIALS							= "U.Portal.Set.Login.Credentials.0001";
	public static final String ADD_LOGIN_SETTING_FAILED							= "U.Portal.Failed.Login.AddLoginSetting.0001";
	public static final String UPDATE_LOGIN_SETTING_FAILED						= "U.Portal.Failed.Login.UpdateLoginSetting.0001";
	public static final String EXECUTE_OPTIMIZATION_FAILED						= "U.Portal.Failed.ExecuteOptimization.0001";
	
//	
	public static final String LOGIN_ID_PASSWORD_FAILED			= "U.Portal.Login.0001";
	public static final String LOGIN_INVALID_USER_ID			= "U.Portal.Login.0002";
	public static final String LOGIN_ACCOUNT_LOCKED				= "U.Portal.Login.0003";
	public static final String LOGIN_ACCOUNT_EXPIRED			= "U.Portal.Login.0004";
	public static final String LOGIN_INVALID_PASSWORD			= "U.Portal.Login.0005";
//	public static final String CHANGE_PASSWORD_NOT_MATCH		= "U.Portal.Password.0001";
//	public static final String CHANGE_PASSWORD_OLD_SAME			= "U.Portal.Password.0002";
	public static final String CHANGE_PASSWORD_EMPTY			= "U.Portal.Password.0003";
//	public static final String CHANGE_PASSWORD_ALPHANUM			= "U.Portal.Password.0004";
	public static final String CHANGE_PASSWORD_TOO_LONG			= "U.Portal.Password.0005";
	public static final String ADD_USER_EMPTY_USER_ID			= "U.Portal.AddUser.0001";
	public static final String ADD_USER_TAKEN_USER_ID			= "U.Portal.AddUser.0002";
	public static final String ADD_USER_TOO_LONG_USER_ID		= "U.Portal.AddUser.0003";
	public static final String ADD_USER_EMPTY_USERNAME			= "U.Portal.AddUser.0004";
	public static final String ADD_USER_TOO_LONG_USERNAME		= "U.Portal.AddUser.0005";
	public static final String ADD_USER_EMPTY_PASSWORD			= "U.Portal.AddUser.0006";
	public static final String ADD_USER_SHORT_PASSWORD			= "U.Portal.AddUser.0007";
	public static final String ADD_USER_ALPHANUM_PASSWORD		= "U.Portal.AddUser.0008";
	public static final String ADD_USER_TOO_LONG_PASSWORD		= "U.Portal.AddUser.0009";
	public static final String ADD_USER_EMPTY_GROUP				= "U.Portal.AddUser.0010";
	public static final String ADD_USER_INVALID_EXPIRATION_DATE	= "U.Portal.AddUser.0011";
	public static final String ADD_USER_EMPTY_EMAIL_ADDRESS		= "U.Portal.AddUser.0012";
	public static final String ADD_USER_TOO_LONG_EMAIL_ADDRESS	= "U.Portal.AddUser.0013";
	public static final String ADD_USER_EMAIL_ADDRESS_INVALID	= "U.Portal.AddUser.0014";
	public static final String ADD_USER_INVALID_GROUP			= "U.Portal.AddUser.0015";
//	public static final String ADD_USER_EMAIL_ADDRESS_ALREADY_EXISTS = "U.Portal.AddUser.0015";
	public static final String EDIT_USER_EMPTY_USERNAME			= "U.Portal.EditUser.0001";
	public static final String EDIT_USER_TOO_LONG_USERNAME		= "U.Portal.EditUser.0002";
//	public static final String EDIT_USER_EMPTY_PASSWORD			= "U.Portal.EditUser.0003";
	public static final String EDIT_USER_SHORT_PASSWORD			= "U.Portal.EditUser.0004";
	public static final String EDIT_USER_TOO_LONG_PASSWORD		= "U.Portal.EditUser.0005";
	public static final String EDIT_USER_PASSWORD_OLD_SAME		= "U.Portal.EditUser.0006";
	public static final String EDIT_USER_ALPHANUM_PASSWORD		= "U.Portal.EditUser.0007";
	public static final String EDIT_USER_EMPTY_GROUP			= "U.Portal.EditUser.0008";
	public static final String EDIT_USER_INVALID_EXPIRATION_DATE= "U.Portal.EditUser.0009";
//	public static final String EDIT_USER_INVALID_USER			= "U.Portal.EditUser.0010";
	public static final String EDIT_USER_EMPTY_EMAIL_ADDRESS	= "U.Portal.EditUser.0011";
	public static final String EDIT_USER_TOO_LONG_EMAIL_ADDRESS	= "U.Portal.EditUser.0012";
	public static final String EDIT_USER_EMAIL_ADDRESS_INVALID	= "U.Portal.EditUser.0013";
//	public static final String EDIT_USER_EMAIL_ADDRESS_ALREADY_EXISTS = "U.Portal.EditUser.0014";
	public static final String SEARCH_USER_INVALID_USER_ID		= "U.Portal.SearchUser.0001";
	public static final String SEARCH_USER_INVALID_NAME			= "U.Portal.SearchUser.0002";
	public static final String SEARCH_USER_INVALID_DATE			= "U.Portal.SearchUser.0003";
//	public static final String SEARCH_USER_INVALID_EMAIL_ADDRESS= "U.Portal.SearchUser.0004";
	public static final String SEARCH_USER_TOO_LONG_EMAIL_ADDRESS= "U.Portal.SearchUser.0005";
	public static final String SEARCH_USER_INVALID_LAST_LOGIN_DATE = "U.Portal.SearchUser.0006";
	public static final String SEARCH_GROUP_INVALID_NAME		= "U.Portal.SearchGroup.0001";
	public static final String SEARCH_GROUP_INVALID_NO_OF_USERS	= "U.Portal.SearchGroup.0002";
	public static final String ADD_GROUP_EMPTY 					= "U.Portal.AddGroup.0001";
	public static final String ADD_GROUP_EXCEEDS_BYTE_SIZE		= "U.Portal.AddGroup.0002";
	public static final String ADD_GROUP_ALREADY_EXISTS 		= "U.Portal.AddGroup.0003";
	public static final String EDIT_GROUP_EMPTY 				= "U.Portal.EditGroup.0001";
	public static final String EDIT_GROUP_EXCEEDS_BYTE_SIZE		= "U.Portal.EditGroup.0002";
	public static final String EDIT_GROUP_ALREADY_EXISTS 		= "U.Portal.EditGroup.0003";
//	public static final String EDIT_GROUP_INVALID_GROUP 		= "U.Portal.EditGroup.0004";
	public static final String EDIT_GROUP_WITH_PROJECT 			= "U.Portal.EditGroup.0005";
	public static final String DELETE_GROUP_WITH_USERS 			= "U.Portal.DeleteGroup.0001";
//	public static final String DELETE_GROUP_WITH_PROJECT 		= "U.Portal.DeleteGroup.0002";
//	
//	
//	
//	
	public static final String INVALID_SCAN_START_TIME			= "S.PortalAPI.GetScan.0005";
	public static final String INVALID_SCAN_END_TIME		 	= "S.PortalAPI.GetScan.0006";
	
	public static final String INVALID_PROJECT_ID 				= "S.PortalAPI.GetProject.0001";
	public static final String PARSE_EXCEPTION					= "S.PortalAPI.DataProcess.0001";
	public static final String INVALID_CX_ANDROID_SCAN_ID 		= "S.PortalAPI.GetCxAndroidScanId.0001";
	public static final String INVALID_CX_ANDROID_PROJECT_ID 	= "S.PortalAPI.GetProject.0002";
	public static final String INVALID_SCAN_ID			 		= "S.PortalAPI.GetScan.0001";
	public static final String INVALID_SESSION_ID 				= "S.PortalAPI.GetCxSuiteSessionId.0001";
//	public static final String INVALID_GROUP_ID 				= "S.PortalAPI.GetGroupId.0001";
	public static final String INVALID_SCAN_REGISTRATION_DATE 	= "S.PortalAPI.GetScan.0002";
	public static final String INVALID_SCAN 					= "S.PortalAPI.GetPortalScan.0001";
//	public static final String INVALID_REPORT 					= "S.PortalAPI.GetReport.0001";
	public static final String INVALID_FILE 					= "S.PortalAPI.FileProcess.0001";
	public static final String INVALID_FILE_NAME 				= "S.PortalAPI.FileProcess.0002";
	public static final String INVALID_PROJECT 					= "S.PortalAPI.GetProject.0003";
//	public static final String INVALID_OWNER_GROUP 				= "S.PortalAPI.GetOwnerGroup.0001";
	public static final String INVALID_USER 					= "S.PortalAPI.GetUser.0001";
	public static final String NO_PROJECT_USERS 				= "S.PortalAPI.GetUser.0002";
	public static final String INVALID_ORG_ID_LIST 				= "S.PortalAPI.GetOrgIdList.0001";
	public static final String INVALID_PROJECT_TYPE 			= "S.PortalAPI.GetProject.0004";
	public static final String INVALID_CASE_NUMBER 				= "U.Portal.CreateProject.0014";
	public static final String INVALID_PROJECT_STATUS 			= "S.PortalAPI.GetProject.0005";
//	public static final String INVALID_PROCESS 					= "U.Portal.UpdateScan.0001";
	public static final String DELETE_REPORT_FAILED 			= "S.PortalAPI.DeleteReport.0001";
	public static final String INVALID_PROJECT_NAME 			= "U.Portal.CreateProject.0001";
	public static final String ORM_EXCEPTION	 				= "S.PortalAPI.PortalDBError.0001";
	public static final String NO_SUCH_PROJECT_EXCEPTION 		= "S.PortalAPI.GetProject.0006";
	public static final String NO_SUCH_SCAN_EXCEPTION 			= "S.PortalAPI.GetScan.0003";
	public static final String SYSTEM_EXCEPTION 				= "S.PortalAPI.PortalDBError.0002";
	public static final String PORTAL_EXCEPTION 				= "S.PortalAPI.PortalDBError.0003";
	public static final String NUMBER_FORMAT_EXCEPTION 			= "S.PortalAPI.DataProcess.0002";
	public static final String NULL_POINTER_EXCEPTION 			= "S.PortalAPI.DataProcess.0003";
	public static final String IO_EXCEPTION 					= "S.PortalAPI.FileProcess.0003";
	public static final String MALFORMEDURL_EXCEPTION 			= "S.PortalAPI.URL.0001";
	public static final String INVALID_REPORT_TYPE 				= "S.PortalAPI.GetReport.0002";
//	public static final String INVALID_SCAN_STATUS 				= "S.PortalAPI.GetScan.0004";
//	public static final String INVALID_PROJECT_ATTRIBUTE 		= "S.PortalAPI.GetProject.0007";
	public static final String INVALID_PROJECT_END_DATE 		= "S.PortalAPI.GetProject.0008";
//	
//	
//	
	public static final String FILE_PROCESS_ERROR 				= "S.PortalAPI.FileProcess.0004";
	public static final String NO_WRITE_PERMISSION 				= "S.PortalAPI.FileProcess.0005";
	public static final String ANDROID_SERVER_ERROR 			= "S.Engine.AndroidAPICall.0001";
	public static final String CONFIG_FILE_ERROR 				= "S.PortalAPI.GetConfigFile.0001";
	public static final String CONFIG_VALUE_ERROR 				= "S.PortalAPI.GetConfigFile.0002";
	public static final String CONFIG_VALUE_EMPTY_ERROR			= "S.PortalAPI.GetConfigFile.0003";
	public static final String CONFIG_VALUE_NULL_ERROR			= "S.PortalAPI.GetConfigFile.0004";
	public static final String SETUP_WIZARD_ERROR 				= "S.PortalAPI.GetSetupWizard.0001";
	public static final String VUlN_MASTER_FILE_ERROR 			= "S.PortalAPI.GetVulnMasterValue.0001";
	public static final String VUlN_MASTER_VALUE_ERROR 			= "S.PortalAPI.GetVulnMasterValue.0002";
	public static final String INVALID_FILE_PATH	 			= "S.PortalAPI.FileProcess.0006";
//	public static final String INVALID_SCAN_STATISTICS 			= "S.PortalAPI.GetAndroidStatistics.0001";
//	public static final String YCLOUD_UPLOAD_FAILED 			= "S.PortalAPI.YCloudUpload.0001";
	public static final String INVALID_REPORT_ID	 			= "S.PortalAPI.GetReport.0003";
	public static final String JSON_EXCEPTION		 			= "S.PortalAPI.ParseDBResults.0001";
//	public static final String INVALID_REPORT_STATUS 			= "S.PortalAPI.GetReport.0004";
	public static final String INVALID_REPORT_NAME	 			= "S.PortalAPI.GetReport.0005";
//	public static final String INVALID_BUCKET_NAME	 			= "S.PortalAPI.GetReport.0006";
//	public static final String VALIDATOR_EXCEPTION 				= "S.PortalAPI.ValidateAttribute.0001";
	public static final String READONLY_EXCEPTION 				= "S.PortalAPI.ValidateAttribute.0002";
	public static final String UNHANDLED_EXCEPTION 				= "S.PortalAPI.UnhandledErrors.0001";
	public static final String PORTLET_EXCEPTION				= "S.PortalAPI.PortletError.0001";
	public static final String INTERRUPTED_EXCEPTION			= "S.PortalAPI.ThreadInterrupted.0001";
//	
//	
//	
//	/* -------------------- SEARCH PROJECT -------------------- */
	public static final String SEARCH_PROJECT_PROJECT_ID_INVALID				= "U.Portal.SearchProject.0006";
	public static final String SEARCH_PROJECT_CASE_NUMBER_INVALID				= "U.Portal.SearchProject.0004";
	public static final String SEARCH_PROJECT_CASE_NUMBER_TOO_LONG				= "U.Portal.SearchProject.0001";
	public static final String SEARCH_PROJECT_PROJECT_NAME_TOO_LONG				= "U.Portal.SearchProject.0002";
	public static final String SEARCH_PROJECT_PROJECT_NAME_INVALID 				= "U.Portal.SearchProject.0005";
	public static final String SEARCH_PROJECT_PROJECT_END_DATE_INVALID			= "U.Portal.SearchProject.0007";
	public static final String SEARCH_PROJECT_SCAN_COUNT_INVALID				= "U.Portal.SearchProject.0008";
//	public static final String SEARCH_PROJECT_PROJECT_DOES_NOT_EXIST			= "U.Portal.SearchProject.0003";
	public static final String SEARCH_PROJECT_GROUP_NAME_TOO_LONG				= "U.Portal.SearchProject.0009";
//	
//	
//	
//	/* -------------------- PROJECT REGISTRATION -------------------- */
	public static final String ADD_PROJECT_CASE_NUMBER_INVALID					= "U.Portal.CreateProject.0015";
	public static final String ADD_PROJECT_CASE_NUMBER_TOO_LONG					= "U.Portal.CreateProject.0003";
	public static final String ADD_PROJECT_CASE_NUMBER_ALREADY_EXISTS			= "U.Portal.CreateProject.0004";
	public static final String ADD_PROJECT_NO_PROJECT_NAME						= "U.Portal.CreateProject.0002";
	public static final String ADD_PROJECT_PROJECT_NAME_TOO_LONG				= "U.Portal.CreateProject.0001";
	public static final String ADD_PROJECT_NO_OWNER_GROUP						= "U.Portal.CreateProject.0005";
	public static final String ADD_PROJECT_NO_PRESET_ID							= "U.Portal.CreateProject.0020";
	public static final String ADD_PROJECT_OWNER_GROUP_DOES_NOT_EXIST			= "U.Portal.CreateProject.0006";
	public static final String ADD_PROJECT_PRESET_DOES_NOT_EXIST				= "U.Portal.CreateProject.0021";
	public static final String ADD_PROJECT_NO_PROJECT_END_DATE					= "U.Portal.CreateProject.0016";
	public static final String ADD_PROJECT_FILE_TYPE_INVALID					= "U.Portal.CreateProject.0009";
//	public static final String ADD_PROJECT_CONTENT_TYPE_INVALID					= "U.Portal.CreateProject.0010";
//	public static final String ADD_PROJECT_NO_FILE								= "U.Portal.CreateProject.0008";
	public static final String ADD_PROJECT_FILE_EMPTY							= "U.Portal.CreateProject.0011";
	public static final String ADD_PROJECT_FILE_SIZE_TOO_BIG_20					= "U.Portal.SetScanConfig.0003";
	public static final String ADD_PROJECT_NO_ATTRIBUTE							= "U.Portal.CreateProject.0007";
	public static final String ADD_PROJECT_NO_PROJECT_USERS						= "U.Portal.CreateProject.0012";
	public static final String ADD_PROJECT_USER_DOES_NOT_EXIST					= "U.Portal.CreateProject.0013";
	public static final String ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP		= "U.Portal.CreateProject.0014";
//	public static final String ADD_PROJECT_WEB_SERVICE_API_FAILED				= "S.Engine.CreateProjectServlet.0002";
	public static final String ADD_PROJECT_PROJECT_END_DATE_INVALID 			= "U.Poratl.CreateProject.0017";
	public static final String ADD_PROJECT_PACKAGE_NAME_INVALID 				= "U.Portal.CreateProject.0018";
	public static final String ADD_PROJECT_PACKAGE_NAME_TOO_LONG				= "U.Portal.SetScanConfig.0004";
	public static final String ADD_PROJECT_PROJECT_NAME_INVALID					= "U.Portal.CreateProject.0019";
	public static final String ADD_PROJECT_PRESET_INVALID 						= "U.Portal.CreateProject.0020";
	public static final String ADD_PROJECT_NO_TARGET_URL						= "U.Portal.CreateProject.0021";
	public static final String ADD_PROJECT_NO_CASE_NAME							= "U.Portal.CreateProject.0022";
	public static final String ADD_PROJECT_NO_PRODUCTION_ENVIRONMENT_URL		= "U.Portal.CreateProject.0023";
	public static final String ADD_PROJECT_TARGET_URL_INVALID					= "U.Portal.CreateProject.0024";
	public static final String ADD_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID	= "U.Portal.CreateProject.0025";
	public static final String ADD_PROJECT_NO_WEB_INSPECTION_SIGNATURE_SET_GROUP= "U.Portal.CreateProject.0026";
	public static final String ADD_PROJECT_NO_SERVER_FILE_SIGNATURE_SET_GROUP	= "U.Portal.CreateProject.0027";
	public static final String ADD_PROJECT_NO_SERVER_SETTING_SIGNATURE_SET_GROUP= "U.Portal.CreateProject.0028";
	

//	
//	
//	/* -------------------- DELETE PROJECT -------------------- */
	public static final String DELETE_PROJECT_PROJECT_ID_INVALID				= "S.Portal.DeleteProject.0001";
	public static final String DELETE_PROJECT_PROJECT_DOES_NOT_EXIST			= "S.Portal.DeleteProject.0002";
	public static final String DELETE_PROJECT_PROJECT_TYPE_INVALID				= "S.Portal.DeleteProject.0003";
	public static final String DELETE_PROJECT_WEB_SERVICE_API_FAILED			= "S.Engine.DeleteProjectServlet.0002";
	public static final String DELETE_PROJECT_USER_NO_RIGHTS					= "S.Portal.DeleteProject.0004";
//	
//	
//	
//	/* -------------------- PROJECT CHANGE -------------------- */
	public static final String UPDATE_PROJECT_CASE_NUMBER_INVALID				= "U.Portal.UpdateProject.0015";
	public static final String UPDATE_PROJECT_CASE_NUMBER_TOO_LONG				= "U.Portal.UpdateProject.0001";
	public static final String UPDATE_PROJECT_CASE_NUMBER_ALREADY_EXISTS		= "U.Portal.UpdateProject.0002";
	public static final String UPDATE_PROJECT_NO_PROJECT_NAME					= "U.Portal.UpdateProject.0003";
	public static final String UPDATE_PROJECT_PROJECT_NAME_TOO_LONG				= "U.Portal.EditProject.0002";
	public static final String UPDATE_PROJECT_NO_OWNER_GROUP					= "U.Portal.UpdateProject.0004";
	public static final String UPDATE_PROJECT_NO_PRESET_ID						= "U.Portal.UpdateProject.0021";
	public static final String UPDATE_PROJECT_OWNER_GROUP_DOES_NOT_EXIST		= "U.Portal.UpdateProject.0005";
	public static final String UPDATE_PROJECT_PRESET_DOES_NOT_EXIST				= "U.Portal.UpdateProject.0022";
	public static final String UPDATE_PROJECT_NO_PROJECT_END_DATE				= "U.Portal.UpdateProject.0016";
	public static final String UPDATE_PROJECT_FILE_TYPE_INVALID					= "U.Portal.UpdateProject.0008";
//	public static final String UPDATE_PROJECT_CONTENT_TYPE_INVALID				= "U.Portal.UpdateProject.0009";
//	public static final String UPDATE_PROJECT_NO_FILE							= "U.Portal.UpdateProject.0007";
	public static final String UPDATE_PROJECT_FILE_EMPTY						= "U.Portal.UpdateProject.0010";
	public static final String UPDATE_PROJECT_FILE_SIZE_TOO_BIG_20				= "U.Portal.UpdateProject.0011";
	public static final String UPDATE_PROJECT_NO_ATTRIBUTE						= "U.Portal.UpdateProject.0006";
	public static final String UPDATE_PROJECT_NO_PROJECT_USERS					= "U.Portal.UpdateProject.0012";
	public static final String UPDATE_PROJECT_USER_DOES_NOT_EXIST				= "U.Portal.UpdateProject.0013";
	public static final String UPDATE_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP		= "U.Portal.UpdateProject.0014";
	public static final String UPDATE_PROJECT_PROJECT_ID_INVALID				= "U.Portal.EditProject.0001";
	public static final String UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST			= "S.Portal.UpdateProject.0001";
//	public static final String UPDATE_PROJECT_WEB_SERVICE_API_FAILED_PROJECT	= "S.Engine.EditProjectServlet.0001";
	public static final String UPDATE_PROJECT_PROJECT_END_DATE_INVALID 			= "U.Poratl.UpdateProject.0017";
	public static final String UPDATE_PROJECT_PROJECT_TYPE_INVALID 				= "S.Portal.UpdateProject.0002";
	public static final String UPDATE_PROJECT_PACKAGE_NAME_INVALID 				= "U.Portal.UpdateProject.0018";
	public static final String UPDATE_PROJECT_PACKAGE_NAME_TOO_LONG				= "U.Portal.UpdateProject.0019";
	public static final String UPDATE_PROJECT_PROJECT_NAME_INVALID				= "U.Portal.UpdateProject.0020";
	public static final String UPDATE_PROJECT_PRESET_INVALID					= "U.Portal.UpdateProject.0023";
	public static final String UPDATE_PROJECT_NO_TARGET_URL						= "U.Portal.UpdateProject.0024";
	public static final String UPDATE_PROJECT_NO_CASE_NAME						= "U.Portal.UpdateProject.0025";
	public static final String UPDATE_PROJECT_NO_PRODUCTION_ENVIRONMENT_URL		= "U.Portal.UpdateProject.0026";
	public static final String UPDATE_PROJECT_TARGET_URL_INVALID				= "U.Portal.UpdateProject.0027";
	public static final String UPDATE_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID	= "U.Portal.UpdateProject.0028";
	public static final String UPDATE_PROJECT_NO_WEB_INSPECTION_SIGNATURE_SET_GROUP	= "U.Portal.CreateProject.0029";
	public static final String UPDATE_PROJECT_NO_SERVER_FILE_SIGNATURE_SET_GROUP	= "U.Portal.CreateProject.0030";
	public static final String UPDATE_PROJECT_NO_SERVER_SETTING_SIGNATURE_SET_GROUP	= "U.Portal.CreateProject.0031";
	
	
	/* -------------------- PROJECT COMPLETION -------------------- */
	public static final String COMPLETE_PROJECT_PROJECT_ID_INVALID				= "S.Portal.CompleteProject.0001";
	public static final String COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST			= "S.Portal.CompleteProject.0002";
	public static final String COMPLETE_PROJECT_PROJECT_CANNOT_BE_COMPLETED		= "S.Portal.CompleteProject.0003";
	public static final String COMPLETE_PROJECT_PROJECT_TYPE_INVALID			= "S.Portal.CompleteProject.0004";
	public static final String COMPLETE_PROJECT_USER_NO_RIGHTS 					= "S.Portal.CompleteProject.0005";
	
	
	
	/* -------------------- OPEN PROJECT -------------------- */
	public static final String OPEN_PROJECT_PROJECT_DOES_NOT_EXIST				= "S.Portal.OpenProject.0001";
	public static final String OPEN_PROJECT_PROJECT_ID_INVALID					= "S.Portal.OpenProject.0002";
	public static final String OPEN_PROJECT_PROJECT_TYPE_INVALID				= "S.Portal.OpenProject.0003";
	public static final String OPEN_PROJECT_USER_NO_RIGHTS						= "S.Portal.OpenProject.0004";
//	
//	
//	
//	/* -------------------- GET PROJECT -------------------- */
	public static final String GET_PROJECT_PROJECT_ID_INVALID					= "S.Portal.GetProject.0001";
	public static final String GET_PROJECT_PROJECT_DOES_NOT_EXIST				= "S.Portal.GetProject.0002";
	public static final String GET_PROJECT_PROJECT_TYPE_INVALID					= "S.Portal.GetProject.0003";
//	
//	
//	
//	/* -------------------- SEARCH SCAN -------------------- */
	public static final String SEARCH_SCAN_REGISTRATION_DATE_INVALID			= "U.Portal.SearchScan.0001";
//	public static final String SEARCH_SCAN_SCAN_DOES_NOT_EXIST					= "U.Portal.SearchScan.0002";
//	public static final String SEARCH_SCAN_USER_NOT_ALLOWED						= "U.Portal.SearchScan.0003";
	public static final String SEARCH_SCAN_SCAN_ID_INVALID						= "U.Portal.SearchScan.0004";
	public static final String SEARCH_SCAN_HASH_VALUE_INVALID					= "U.Portal.SearchScan.0005";
	public static final String SEARCH_SCAN_CX_SCAN_ID_INVALID					= "U.Portal.SearchScan.0006";
	public static final String SEARCH_SCAN_PROJECT_NAME_TOO_LONG				= "U.Portal.SearchScan.0007";
	public static final String SEARCH_SCAN_PROJECT_NAME_INVALID					= "U.Portal.SearchScan.0008";
	public static final String SEARCH_SCAN_GROUP_NAME_TOO_LONG					= "U.Portal.SearchScan.0009";
	public static final String SEARCH_SCAN_START_TIME_INVALID					= "U.Portal.SearchScan.0010";
	public static final String SEARCH_SCAN_END_TIME_INVALID						= "U.Portal.SearchScan.0011";
	public static final String SEARCH_SCAN_START_AND_END_TIME_INVALID			= "U.Portal.SearchScan.0012";
	
	
	/* -------------------- SCAN REGISTRATION -------------------- */
	public static final String ADD_SCAN_PROJECT_ID_INVALID						= "S.Portal.CreateScan.0001";
	public static final String ADD_SCAN_NO_FILE									= "U.Portal.CreateScan.0001";
	public static final String ADD_SCAN_FILE_NOT_APK							= "U.Portal.CreateScan.0002";
	public static final String ADD_SCAN_FILE_NOT_ZIP							= "U.Portal.CreateScan.0003";
	public static final String ADD_SCAN_FILE_SIZE_TOO_BIG_50					= "U.Portal.Scan.0005";
	public static final String ADD_SCAN_FILE_SIZE_TOO_BIG 						= "U.Portal.CreateScan.0004";
	public static final String ADD_SCAN_FILE_EMPTY								= "U.Portal.CreateScan.0005";
	public static final String ADD_SCAN_NO_PROCESS								= "U.Portal.CreateScan.0006";
	public static final String ADD_SCAN_PROJECT_DOES_NOT_EXIST					= "S.Portal.CreateScan.0002";
//	public static final String ADD_SCAN_WEB_SERVICE_API_FAILED					= "S.Portal.CreateScan.0003";
	public static final String ADD_SCAN_PROJECT_TYPE_INVALID 					= "S.Portal.CreateScan.0004";
	public static final String ADD_SCAN_FILE_NAME_TOO_LONG						= "U.Portal.Scan.0003";
	public static final String ADD_SCAN_PRESET_ID_INVALID						= "S.Portal.CreateScan.0007";
	public static final String ADD_SCAN_FILE_NAME_INVALID						= "U.Portal.CreateScan.0007";
	public static final String SOURCE_CODE_MAX_LINE_750000					    = "U.Portal.CreateScan.0008";
//	
//	
//	/* -------------------- SCAN CHANGE -------------------- */
	public static final String UPDATE_SCAN_SCAN_ID_INVALID						= "S.Portal.UpdateScan.0001";
	public static final String UPDATE_SCAN_SCAN_DOES_NOT_EXIST					= "S.Portal.UpdateScan.0002";
	public static final String UPDATE_SCAN_PROJECT_DOES_NOT_EXIST				= "S.Portal.UpdateScan.0003";
	public static final String UPDATE_SCAN_PROJECT_TYPE_INVALID					= "S.Portal.UpdateScan.0004";
//	public static final String UPDATE_SCAN_WEB_SERVICE_API_FAILED				= "S.Portal.UpdateScan.0005";
	public static final String UPDATE_SCAN_NO_PROCESS							= "U.Portal.UpdateScan.0001";
	public static final String UPDATE_SCAN_PROJECT_ID_INVALID					= "S.Portal.UpdateScan.0006";
	public static final String UPDATE_SCAN_PRESET_ID_INVALID					= "S.Portal.UpdateScan.0007";
//	
//	
//	
//	/* -------------------- DOWNLOAD REPORT -------------------- */
//	public static final String DOWNLOAD_REPORT_SCAN_ID_INVALID					= "S.Portal.DownloadReport.0001";
//	public static final String DOWNLOAD_REPORT_REPORT_ID_INVALID				= "S.Portal.DownloadReport.0002";
//	public static final String DOWNLOAD_REPORT_PROJECT_DOES_NOT_EXIST			= "S.Portal.DownloadReport.0003";
//	public static final String DOWNLOAD_REPORT_SCAN_DOES_NOT_EXIST				= "S.Portal.DownloadReport.0004";
	public static final String DOWNLOAD_REPORT_NO_REPORT						= "S.Portal.DownloadReport.0005";
//	public static final String DOWNLOAD_REPORT_PROJECT_ID_INVALID 				= "S.Portal.DownloadReport.0006";
//	public static final String DOWNLOAD_REPORT_PROJECT_TYPE_INVALID 			= "S.Portal.DownloadReport.0007";
	public static final String DOWNLOAD_REPORT_USER_NO_RIGHTS					= "S.Portal.DownloadReport.0009";
//	
//	
//	
//	/* -------------------- DOWNLOAD REFERENCE -------------------- */
//	public static final String DOWNLOAD_REFERENCE_SCAN_ID_INVALID				= "S.Portal.DownloadReference.0001";
//	public static final String DOWNLOAD_REFERENCE_REPORT_ID_INVALID				= "S.Portal.DownloadReference.0002";
//	public static final String DOWNLOAD_REFERENCE_PROJECT_DOES_NOT_EXIST		= "S.Portal.DownloadReference.0003";
//	public static final String DOWNLOAD_REFERENCE_PROJECT_TYPE_INVALID			= "S.Portal.DownloadReference.0004";
//	public static final String DOWNLOAD_REFERENCE_SCAN_DOES_NOT_EXIST			= "S.Portal.DownloadReference.0005";
//	public static final String DOWNLOAD_REFERENCE_NO_REFERENCE					= "S.Portal.DownloadReference.0006";
//	public static final String DOWNLOAD_REFERENCE_PROJECT_ID_INVALID 			= "S.Portal.DownloadReference.0007";
	public static final String DOWNLOAD_REFERENCE_USER_NO_RIGHTS				= "S.Portal.DownloadReference.0008";
//	
//	
//	
//	/* -------------------- STOP SCAN -------------------- */
	public static final String STOP_SCAN_SCAN_ID_INVALID						= "S.Portal.StopScan.0001";
	public static final String STOP_SCAN_PROJECT_ID_INVALID						= "S.Portal.StopScan.0006";
	public static final String STOP_SCAN_PROJECT_DOES_NOT_EXIST					= "S.Portal.StopScan.0002";
	public static final String STOP_SCAN_SCAN_DOES_NOT_EXIST					= "S.Portal.StopScan.0003";
	public static final String STOP_SCAN_SCAN_NOT_IN_WAITING_SCANNING_STATE		= "S.Portal.StopScan.0004";
	public static final String STOP_SCAN_WEB_SERVICE_API_FAILED					= "S.Portal.StopScan.0005";
	public static final String STOP_SCAN_PROJECT_TYPE_INVALID					= "S.Portal.StopScan.0007";
	public static final String STOP_SCAN_USER_NO_RIGHTS 						= "S.Portal.StopScan.0008";
//	
//	
//	
//	/* -------------------- REEXECUTE SCAN -------------------- */
	public static final String REEXECUTE_SCAN_SCAN_ID_INVALID								= "S.Portal.ReexecuteScan.0001";
	public static final String REEXECUTE_SCAN_PROJECT_ID_INVALID 							= "S.Portal.ReexecuteScan.0006";
	public static final String REEXECUTE_SCAN_PROJECT_DOES_NOT_EXIST						= "S.Portal.ReexecuteScan.0002";
	public static final String REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST							= "S.Portal.ReexecuteScan.0003";
	public static final String REEXECUTE_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE	= "S.Portal.ReexecuteScan.0004";
//	public static final String REEXECUTE_SCAN_WEB_SERVICE_API_FAILED						= "S.Portal.ReexecuteScan.0005";
	public static final String REEXECUTE_SCAN_PROJECT_TYPE_INVALID							= "S.Portal.ReexecuteScan.0007";
	public static final String REEXECUTE_SCAN_USER_NO_RIGHTS 								= "S.Portal.ReexecuteScan.0008";
	public static final String REEXECUTE_SCAN_PRESET_ID_INVALID								= "S.Portal.ReexecuteScan.0009";
//	
//	
//	
//	/* -------------------- REQUEST REVIEW -------------------- */
	public static final String REQUEST_REVIEW_SCAN_ID_INVALID						= "S.Portal.RequestReview.0001";
	public static final String REQUEST_REVIEW_PROJECT_ID_INVALID 					= "S.Portal.RequestReview.0006";
	public static final String REQUEST_REVIEW_PROJECT_DOES_NOT_EXIST				= "S.Portal.RequestReview.0002";
	public static final String REQUEST_REVIEW_PROJECT_TYPE_INVALID					= "S.Portal.RequestReview.0003";
	public static final String REQUEST_REVIEW_SCAN_DOES_NOT_EXIST					= "S.Portal.RequestReview.0004";
	public static final String REQUEST_REVIEW_SCAN_NOT_IN_COMPLETE_STATE			= "S.Portal.RequestReview.0005";
	public static final String REQUEST_REVIEW_USER_NO_RIGHTS 						= "S.Portal.RequestReview.0007";
//	
//	
//	
//	/* -------------------- CANCEL REVIEW -------------------- */
	public static final String CANCEL_REVIEW_SCAN_ID_INVALID						= "S.Portal.CancelReview.0001";
//	public static final String CANCEL_REVIEW_PROJECT_ID_INVALID 					= "S.Portal.CancelReview.0006";
	public static final String CANCEL_REVIEW_PROJECT_DOES_NOT_EXIST					= "S.Portal.CancelReview.0002";
	public static final String CANCEL_REVIEW_PROJECT_TYPE_INVALID					= "S.Portal.CancelReview.0003";
	public static final String CANCEL_REVIEW_SCAN_DOES_NOT_EXIST					= "S.Portal.CancelReview.0004";
	public static final String CANCEL_REVIEW_SCAN_NOT_IN_REVIEW_STATE				= "S.Portal.CancelReview.0005";
	public static final String CANCEL_REVIEW_USER_NO_RIGHTS 						= "S.Portal.CancelReview.0007";
//	
//	
//	
//	/* -------------------- REGENERATE REPORT -------------------- */
	public static final String REGENERATE_REPORT_SCAN_ID_INVALID						= "S.Portal.RegenerateReport.0001";
	public static final String REGENERATE_REPORT_PROJECT_ID_INVALID 					= "S.Portal.RegenerateReport.0007";
	public static final String REGENERATE_REPORT_PROJECT_DOES_NOT_EXIST					= "S.Portal.RegenerateReport.0002";
	public static final String REGENERATE_REPORT_PROJECT_TYPE_INVALID					= "S.Portal.RegenerateReport.0003";
	public static final String REGENERATE_REPORT_SCAN_DOES_NOT_EXIST					= "S.Portal.RegenerateReport.0004";
	public static final String REGENERATE_REPORT_SCAN_NOT_IN_REVIEW_COMPLETE_STATE		= "S.Portal.RegenerateReport.0005";
	public static final String REGENERATE_REPORT_WEB_SERVICE_API_FAILED					= "S.Portal.RegenerateReport.0006";
	public static final String REGENERATE_REPORT_USER_NO_RIGHTS 						= "S.Portal.RegenerateReport.0008";
//	
//	
//	
//	/* -------------------- DELETE SCAN -------------------- */
	public static final String DELETE_SCAN_SCAN_ID_INVALID							= "U.Portal.DeleteScan.0002";
	public static final String DELETE_SCAN_PROJECT_ID_INVALID 						= "S.Portal.DeleteScan.0005";
	public static final String DELETE_SCAN_PROJECT_DOES_NOT_EXIST					= "U.Portal.DeleteScan.0003";
	public static final String DELETE_SCAN_SCAN_DOES_NOT_EXIST						= "U.Portal.DeleteScan.0004";
//	public static final String DELETE_SCAN_SCAN_NOT_IN_COMPLETE_STATE				= "S.Portal.DeleteScan.0001";
	public static final String DELETE_SCAN_WEB_SERVICE_API_FAILED					= "S.Engine.DeleteScanServlet.0002";
	public static final String DELETE_SCAN_PROJECT_TYPE_INVALID 					= "S.Portal.DeleteScan.0006";
	public static final String DELETE_SCAN_USER_NO_RIGHTS 							= "S.Portal.DeleteScan.0007";
//	
//	/* -------------------- RECRAWL -------------------- */
	public static final String RECRAWL_ID_INVALID									= "S.Portal.Recrawl.0001";
	public static final String RECRAWL_PROJECT_ID_INVALID 							= "S.Portal.Recrawl.0006";
	public static final String RECRAWL_PROJECT_DOES_NOT_EXIST						= "S.Portal.Recrawl.0002";
	public static final String RECRAWL_SCAN_DOES_NOT_EXIST							= "S.Portal.Recrawl.0003";
	public static final String RECRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE	= "S.Portal.Recrawl.0004";
	public static final String RECRAWL_WEB_SERVICE_API_FAILED						= "S.Portal.Recrawl.0005";
	public static final String RECRAWL_PROJECT_TYPE_INVALID							= "S.Portal.Recrawl.0007";
	public static final String RECRAWL_USER_NO_RIGHTS 								= "S.Portal.Recrawl.0008";
	public static final String RECRAWL_PRESET_ID_INVALID							= "S.Portal.Recrawl.0009";
//	
//	/* -------------------- ABORT CRAWL -------------------- */
	public static final String ABORT_CRAWL_ID_INVALID									= "S.Portal.AbortCrawl.0001";
	public static final String ABORT_CRAWL_PROJECT_ID_INVALID 							= "S.Portal.AbortCrawl.0006";
	public static final String ABORT_CRAWL_PROJECT_DOES_NOT_EXIST						= "S.Portal.AbortCrawl.0002";
	public static final String ABORT_CRAWL_SCAN_DOES_NOT_EXIST							= "S.Portal.AbortCrawl.0003";
	public static final String ABORT_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE	= "S.Portal.AbortCrawl.0004";
	public static final String ABORT_CRAWL_WEB_SERVICE_API_FAILED						= "S.Portal.AbortCrawl.0005";
	public static final String ABORT_CRAWL_PROJECT_TYPE_INVALID							= "S.Portal.AbortCrawl.0007";
	public static final String ABORT_CRAWL_USER_NO_RIGHTS 								= "S.Portal.AbortCrawl.0008";
	public static final String ABORT_CRAWL_PRESET_ID_INVALID							= "S.Portal.AbortCrawl.0009";
//
//	/* -------------------- INTERRUPTED CRAWL -------------------- */
	public static final String INTERRUPTED_CRAWL_ID_INVALID										= "S.Portal.InterruptedCrawl.0001";
	public static final String INTERRUPTED_CRAWL_PROJECT_ID_INVALID 							= "S.Portal.InterruptedCrawl.0006";
	public static final String INTERRUPTED_CRAWL_PROJECT_DOES_NOT_EXIST							= "S.Portal.InterruptedCrawl.0002";
	public static final String INTERRUPTED_CRAWL_SCAN_DOES_NOT_EXIST							= "S.Portal.InterruptedCrawl.0003";
	public static final String INTERRUPTED_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.InterruptedCrawl.0004";
	public static final String INTERRUPTED_CRAWL_WEB_SERVICE_API_FAILED							= "S.Portal.InterruptedCrawl.0005";
	public static final String INTERRUPTED_CRAWL_PROJECT_TYPE_INVALID							= "S.Portal.InterruptedCrawl.0007";
	public static final String INTERRUPTED_CRAWL_USER_NO_RIGHTS 								= "S.Portal.InterruptedCrawl.0008";
	public static final String INTERRUPTED_CRAWL_PRESET_ID_INVALID								= "S.Portal.InterruptedCrawl.0009";
//	
//	/* -------------------- CANCEL CRAWL -------------------- */
	public static final String CANCEL_CRAWL_ID_INVALID										= "S.Portal.CancelCrawl.0001";
	public static final String CANCEL_CRAWL_PROJECT_ID_INVALID 								= "S.Portal.CancelCrawl.0006";
	public static final String CANCEL_CRAWL_PROJECT_DOES_NOT_EXIST							= "S.Portal.CancelCrawl.0002";
	public static final String CANCEL_CRAWL_SCAN_DOES_NOT_EXIST								= "S.Portal.CancelCrawl.0003";
	public static final String CANCEL_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.CancelCrawl.0004";
	public static final String CANCEL_CRAWL_WEB_SERVICE_API_FAILED							= "S.Portal.CancelCrawl.0005";
	public static final String CANCEL_CRAWL_PROJECT_TYPE_INVALID							= "S.Portal.CancelCrawl.0007";
	public static final String CANCEL_CRAWL_USER_NO_RIGHTS 									= "S.Portal.CancelCrawl.0008";
	public static final String CANCEL_CRAWL_PRESET_ID_INVALID								= "S.Portal.CancelCrawl.0009";
//	
//	/* -------------------- RESTART CRAWL -------------------- */
	public static final String RESTART_CRAWL_ID_INVALID										= "S.Portal.RestartCrawl.0001";
	public static final String RESTART_CRAWL_PROJECT_ID_INVALID 							= "S.Portal.RestartCrawl.0006";
	public static final String RESTART_CRAWL_PROJECT_DOES_NOT_EXIST							= "S.Portal.RestartCrawl.0002";
	public static final String RESTART_CRAWL_SCAN_DOES_NOT_EXIST							= "S.Portal.RestartCrawl.0003";
	public static final String RESTART_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.RestartCrawl.0004";
	public static final String RESTART_CRAWL_WEB_SERVICE_API_FAILED							= "S.Portal.RestartCrawl.0005";
	public static final String RESTART_CRAWL_PROJECT_TYPE_INVALID							= "S.Portal.RestartCrawl.0007";
	public static final String RESTART_CRAWL_USER_NO_RIGHTS 								= "S.Portal.RestartCrawl.0008";
	public static final String RESTART_CRAWL_PRESET_ID_INVALID								= "S.Portal.RestartCrawl.0009";
//	
	//
//	/* -------------------- INTERRUPTED SCAN -------------------- */
	public static final String INTERRUPTED_SCAN_ID_INVALID										= "S.Portal.InterruptedScan.0001";
	public static final String INTERRUPTED_SCAN_PROJECT_ID_INVALID 								= "S.Portal.InterruptedScan.0006";
	public static final String INTERRUPTED_SCAN_PROJECT_DOES_NOT_EXIST							= "S.Portal.InterruptedScan.0002";
	public static final String INTERRUPTED_SCAN_SCAN_DOES_NOT_EXIST								= "S.Portal.InterruptedScan.0003";
	public static final String INTERRUPTED_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.InterruptedScan.0004";
	public static final String INTERRUPTED_SCAN_WEB_SERVICE_API_FAILED							= "S.Portal.InterruptedScan.0005";
	public static final String INTERRUPTED_SCAN_PROJECT_TYPE_INVALID							= "S.Portal.InterruptedScan.0007";
	public static final String INTERRUPTED_SCAN_USER_NO_RIGHTS 									= "S.Portal.InterruptedScan.0008";
	public static final String INTERRUPTED_SCAN_PRESET_ID_INVALID								= "S.Portal.InterruptedScan.0009";
//	
//	/* -------------------- CANCEL SCAN -------------------- */
	public static final String CANCEL_SCAN_ID_INVALID										= "S.Portal.CancelScan.0001";
	public static final String CANCEL_SCAN_PROJECT_ID_INVALID 								= "S.Portal.CancelScan.0006";
	public static final String CANCEL_SCAN_PROJECT_DOES_NOT_EXIST							= "S.Portal.CancelScan.0002";
	public static final String CANCEL_SCAN_SCAN_DOES_NOT_EXIST								= "S.Portal.CancelScan.0003";
	public static final String CANCEL_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.CancelScan.0004";
	public static final String CANCEL_SCAN_WEB_SERVICE_API_FAILED							= "S.Portal.CancelScan.0005";
	public static final String CANCEL_SCAN_PROJECT_TYPE_INVALID								= "S.Portal.CancelScan.0007";
	public static final String CANCEL_SCAN_USER_NO_RIGHTS 									= "S.Portal.CancelScan.0008";
	public static final String CANCEL_SCAN_PRESET_ID_INVALID								= "S.Portal.CancelScan.0009";
//	
//	/* -------------------- RESTART SCAN -------------------- */
	public static final String RESTART_SCAN_ID_INVALID										= "S.Portal.RestartScan.0001";
	public static final String RESTART_SCAN_PROJECT_ID_INVALID 								= "S.Portal.RestartScan.0006";
	public static final String RESTART_SCAN_PROJECT_DOES_NOT_EXIST							= "S.Portal.RestartScan.0002";
	public static final String RESTART_SCAN_SCAN_DOES_NOT_EXIST								= "S.Portal.RestartScan.0003";
	public static final String RESTART_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.RestartScan.0004";
	public static final String RESTART_SCAN_WEB_SERVICE_API_FAILED							= "S.Portal.RestartScan.0005";
	public static final String RESTART_SCAN_PROJECT_TYPE_INVALID							= "S.Portal.RestartScan.0007";
	public static final String RESTART_SCAN_USER_NO_RIGHTS 									= "S.Portal.RestartScan.0008";
	public static final String RESTART_SCAN_PRESET_ID_INVALID								= "S.Portal.RestartScan.0009";
//	
//	/* -------------------- COPY CRAWL SETTING AND SCAN -------------------- */
	public static final String COPY_CRAWL_RESULT_AND_SCAN_ID_INVALID										= "S.Portal.CopyCrawlAndScan.0001";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_PROJECT_ID_INVALID 								= "S.Portal.CopyCrawlAndScan.0006";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_PROJECT_DOES_NOT_EXIST							= "S.Portal.CopyCrawlAndScan.0002";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_SCAN_DOES_NOT_EXIST								= "S.Portal.CopyCrawlAndScan.0003";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.CopyCrawlAndScan.0004";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_WEB_SERVICE_API_FAILED							= "S.Portal.CopyCrawlAndScan.0005";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_PROJECT_TYPE_INVALID								= "S.Portal.CopyCrawlAndScan.0007";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_USER_NO_RIGHTS 									= "S.Portal.CopyCrawlAndScan.0008";
	public static final String COPY_CRAWL_RESULT_AND_SCAN_PRESET_ID_INVALID									= "S.Portal.CopyCrawlAndScan.0009";
//	
	
//	/* -------------------- UPDATE SCAN SETTING -------------------- */
	public static final String UPDATE_SCAN_SETTING_ID_INVALID										= "S.Portal.UpdateScanPatrolSettings.0001";
	public static final String UPDATE_SCAN_SETTING_PROJECT_ID_INVALID 								= "S.Portal.UpdateScanPatrolSettings.0006";
	public static final String UPDATE_SCAN_SETTING_PROJECT_DOES_NOT_EXIST							= "S.Portal.UpdateScanPatrolSettings.0002";
	public static final String UPDATE_SCAN_SETTING_SCAN_DOES_NOT_EXIST								= "S.Portal.UpdateScanPatrolSettings.0003";
	public static final String UPDATE_SCAN_SETTING_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.UpdateScanPatrolSettings.0004";
	public static final String UPDATE_SCAN_SETTING_WEB_SERVICE_API_FAILED							= "S.Portal.UpdateScanPatrolSettings.0005";
	public static final String UPDATE_SCAN_SETTING_PROJECT_TYPE_INVALID								= "S.Portal.UpdateScanPatrolSettings.0007";
	public static final String UPDATE_SCAN_SETTING_USER_NO_RIGHTS 									= "S.Portal.UpdateScanPatrolSettings.0008";
	public static final String UPDATE_SCAN_SETTING_PRESET_ID_INVALID								= "S.Portal.UpdateScanPatrolSettings.0009";
//
//	/* -------------------- IMPORT SCAN -------------------- */
	public static final String IMPORT_SCAN_ID_INVALID										= "S.Portal.ImportScan.0001";
	public static final String IMPORT_SCAN_PROJECT_ID_INVALID 								= "S.Portal.ImportScan.0006";
	public static final String IMPORT_SCAN_PROJECT_DOES_NOT_EXIST							= "S.Portal.ImportScan.0002";
	public static final String IMPORT_SCAN_SCAN_DOES_NOT_EXIST								= "S.Portal.ImportScan.0003";
	public static final String IMPORT_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.ImportScan.0004";
	public static final String IMPORT_SCAN_WEB_SERVICE_API_FAILED							= "S.Portal.ImportScan.0005";
	public static final String IMPORT_SCAN_PROJECT_TYPE_INVALID								= "S.Portal.ImportScan.0007";
	public static final String IMPORT_SCAN_USER_NO_RIGHTS 									= "S.Portal.ImportScan.0008";
	public static final String IMPORT_SCAN_PRESET_ID_INVALID								= "S.Portal.ImportScan.0009";
	public static final String IMPORT_SCAN_NO_FILE											= "U.Portal.ImportScan.0010";
	public static final String IMPORT_SCAN_FILE_NOT_EXP										= "U.Portal.ImportScan.0011";
	public static final String IMPORT_SCAN_FILE_NOT_ZIP										= "U.Portal.ImportScan.0012";
	public static final String IMPORT_SCAN_FILE_SIZE_TOO_BIG_50								= "U.Portal.ImportScan.0013";
	public static final String IMPORT_SCAN_FILE_SIZE_TOO_BIG 								= "U.Portal.ImportScan.0014";
	public static final String IMPORT_SCAN_FILE_EMPTY										= "U.Portal.ImportScan.0015";
	public static final String IMPORT_SCAN_NO_PROCESS										= "U.Portal.ImportScan.0016";
	public static final String IMPORT_SCAN_FILE_NAME_TOO_LONG								= "U.Portal.ImportScan.0017";
	public static final String IMPORT_SCAN_FILE_NAME_INVALID								= "U.Portal.ImportScan.0018";
	public static final String IMPORT_SCAN_NO_INSPECTION_LOG   						        = "U.Portal.ImportScan.0019";
//	
//	/* -------------------- UPLOAD SCAN -------------------- */
	public static final String UPLOAD_SCAN_ID_INVALID										= "S.Portal.UploadScan.0001";
	public static final String UPLOAD_SCAN_PROJECT_ID_INVALID 								= "S.Portal.UploadScan.0006";
	public static final String UPLOAD_SCAN_PROJECT_DOES_NOT_EXIST							= "S.Portal.UploadScan.0002";
	public static final String UPLOAD_SCAN_SCAN_DOES_NOT_EXIST								= "S.Portal.UploadScan.0003";
	public static final String UPLOAD_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE		= "S.Portal.UploadScan.0004";
	public static final String UPLOAD_SCAN_WEB_SERVICE_API_FAILED							= "S.Portal.UploadScan.0005";
	public static final String UPLOAD_SCAN_PROJECT_TYPE_INVALID								= "S.Portal.UploadScan.0007";
	public static final String UPLOAD_SCAN_USER_NO_RIGHTS 									= "S.Portal.UploadScan.0008";
	public static final String UPLOAD_SCAN_PRESET_ID_INVALID								= "S.Portal.UploadScan.0009";
	public static final String UPLOAD_SCAN_NO_FILE											= "U.Portal.UploadScan.0010";
	public static final String UPLOAD_SCAN_FILE_NOT_EXP										= "U.Portal.UploadScan.0011";
	public static final String UPLOAD_SCAN_FILE_NOT_ZIP										= "U.Portal.UploadScan.0012";
	public static final String UPLOAD_SCAN_FILE_SIZE_TOO_BIG_50								= "U.Portal.UploadScan.0013";
	public static final String UPLOAD_SCAN_FILE_SIZE_TOO_BIG 								= "U.Portal.UploadScan.0014";
	public static final String UPLOAD_SCAN_FILE_EMPTY										= "U.Portal.UploadScan.0015";
	public static final String UPLOAD_SCAN_NO_PROCESS										= "U.Portal.UploadScan.0016";
	public static final String UPLOAD_SCAN_FILE_NAME_TOO_LONG								= "U.Portal.UploadScan.0017";
	public static final String UPLOAD_SCAN_FILE_NAME_INVALID								= "U.Portal.UploadScan.0018";
	public static final String UPLOAD_SCAN_FILE_ALREADY_UPLOADED							= "U.Portal.UploadScan.0019";
//	
//	/* -------------------- AUTO CRAWL -------------------- */
	public static final String AUTO_CRAWL_ID_INVALID									= "S.Portal.AutoCrawl.0001";
	public static final String AUTO_CRAWL_PROJECT_ID_INVALID 							= "S.Portal.AutoCrawl.0006";
	public static final String AUTO_CRAWL_PROJECT_DOES_NOT_EXIST						= "S.Portal.AutoCrawl.0002";
	public static final String AUTO_CRAWL_SCAN_DOES_NOT_EXIST							= "S.Portal.AutoCrawl.0003";
	public static final String AUTO_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE	= "S.Portal.AutoCrawl.0004";
	public static final String AUTO_CRAWL_WEB_SERVICE_API_FAILED						= "S.Portal.AutoCrawl.0005";
	public static final String AUTO_CRAWL_PROJECT_TYPE_INVALID							= "S.Portal.AutoCrawl.0007";
	public static final String AUTO_CRAWL_USER_NO_RIGHTS 								= "S.Portal.AutoCrawl.0008";
	public static final String AUTO_CRAWL_PRESET_ID_INVALID								= "S.Portal.AutoCrawl.0009";
//
//	/* -------------------- RESEND RESULT -------------------- */
	public static final String RESEND_ID_INVALID									= "S.Portal.Resend.0001";
	public static final String RESEND_PROJECT_ID_INVALID 							= "S.Portal.Resend.0002";
	public static final String RESEND_PROJECT_DOES_NOT_EXIST						= "S.Portal.Resend.0003";
	public static final String RESEND_SCAN_DOES_NOT_EXIST							= "S.Portal.Resend.0004";
	public static final String RESEND_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE	= "S.Portal.Resend.0005";
	public static final String RESEND_WEB_SERVICE_API_FAILED						= "S.Portal.Resend.0006";
	public static final String RESEND_PROJECT_TYPE_INVALID							= "S.Portal.Resend.0007";
	public static final String RESEND_USER_NO_RIGHTS 								= "S.Portal.Resend.0008";
	public static final String RESEND_PRESET_ID_INVALID								= "S.Portal.Resend.0009";
	public static final String RESEND_DETECTION_RESULT_ID_INVALID					= "S.Portal.Resend.0010";
//	
//	/* -------------------- GET SCAN -------------------- */
	public static final String GET_SCAN_SCAN_ID_INVALID								= "S.Portal.GetScan.0001";
	public static final String GET_SCAN_SCAN_DOES_NOT_EXIST 						= "S.Portal.GetScan.0002";
	public static final String GET_SCAN_PROJECT_TYPE_INVALID 						= "S.Portal.GetScan.0003";
//	
//	
//	
//	/* -------------------- COMMON FOR PROJECT & SCAN MANAGEMENT -------------------- */
	public static final String GET_USER_USER_ID_INVALID								= "S.Portal.GetUser.0001";
	public static final String GET_USER_USER_DOES_NOT_EXIST							= "S.Portal.GetUser.0002";
	public static final String GET_ORGANIZATION_ORGANIZATION_ID_INVALID 			= "S.Portal.GetOrganization.0001";
	public static final String GET_PROJECT_USERS_PROJECT_ID_INVALID 				= "S.Portal.GetProjectUsers.0001";
//	
//	
//	
	public static final String CXSUITE_LOGIN_ERROR									= "S.Engine.Login.0001";
//	public static final String UNABLE_TO_GET_CX_RESOLVER							= "S.Engine.GetCxResolver.0001";
	public static final String CX_WEBSERVICE_URL_ERROR								= "S.Engine.GetWebServiceUrl.0001";
	public static final String INVALID_CX_SCAN_PARAM								= "S.Portal.RegisterCxScan.0001";
	public static final String INVALID_CX_SOURCE_FILE								= "S.Portal.CxSourceCodeUpload.0001";
	public static final String INVALID_CX_SESSION_ID								= "S.Portal.CxSessionIdParameter.0001";
	public static final String INVALID_CX_PROJECTNAME								= "S.Portal.CxProjectName.0001";
	public static final String INVALID_CX_PROJECT_ID								= "S.Portal.CxProjectID.0001";
	public static final String INVALID_CX_SCAN_ID									= "S.Portal.CxScanID.0001";
	public static final String INVALID_CX_RUN_ID									= "S.Portal.CxRunID.0001";
	public static final String INVALID_CX_PROJECT_CONFIG							= "S.Engine.CxProjectConfig.0001";
	public static final String INVALID_CX_PROJECT_SETTINGS							= "S.Engine.CxProjectSettings.0001";
	public static final String PROJECT_CONFIG_UPDATE_ERROR							= "S.Engine.CxProjectUpdate.0001";
//	public static final String ERROR_READING_SOURCE_FILE							= "S.Portal.CxSourceCodeUpload.0002";
	public static final String UNSUCCESSFUL_CX_SOURCE_FILE_UPLOAD 					= "S.Portal.CxSourceCodeUpload.0003";
	public static final String INVALID_CX_SCAN_ARGS 								= "S.Portal.CxRegisterScan.0001";
	public static final String UNABLE_TO_GET_CXSUITE_RUN_ID 						= "S.Engine.CxRegisterScan.0002";
	public static final String CX_SCAN_EXECUTION_ERROR								= "S.Engine.CxRegisterScan.0003";
//	public static final String INVALID_CX_REPORT_PARAM								= "S.Portal.DownloadReferenceReport.0001";
	public static final String UNABLE_TO_GET_UPLOADED_SOURCE_FILE					= "S.Portal.SourceCodeScan.0001";
	public static final String UNABLE_TO_GET_CX_CONFIG_VALUES 						= "S.Portal.RegisterCxScan.0004";
	public static final String INVALID_SIGNATURE_ID									= "S.Portal.RegisterCxScan.0005";
	public static final String INVALID_SCAN_CONFIG_ID								= "S.Portal.RegisterCxScan.0006";
//	public static final String NO_CX_PROJECT_ID_RETRIEVED							= "S.Engine.RegisterCxScan.0007";
//	public static final String NO_CX_CONFIGURATION									= "S.Engine.RegisterCxScan.0008";
	public static final String INVALID_FILE_HASH_VALUE 								= "S.Portal.FileHashValue.0001";
//	public static final String UPDATE_PROJECT_ID_ERROR 								= "S.Portal.RegisterCxScan.0009";
//	public static final String UNABLE_TO_GET_CX_PROJECT 							= "S.Portal.RegisterCxScan.0010";
//	public static final String UNABLE_TO_GET_CX_SCAN_STATUS 						= "S.Engine.CxScanStatus.0001";
	public static final String INVALID_CX_REPORT_DIRECTORY 							= "S.Portal.CxScanReport.0001";
//	public static final String INVALID_CX_REPORT_ID									= "S.Portal.CxReportID.0001";
//	public static final String INVALID_CX_SCAN_REGISTRATION_DATE 					= "S.Portal.CxScanReport.0002";
	public static final String CX_REPORT_DB_ERROR 									= "S.Portal.CxScanReport.0003";
	public static final String CX_GENERATE_SCAN_REPORT_ERROR						= "S.Portal.CxScanReport.0010";
//	public static final String CX_GENERATE_SCAN_REPORT_INCOMPLETE					= "S.Engine.CxScanReport.0011";
	public static final String CX_SCAN_ID_IS_NOT_FOUND_IN_CX_SERVER					= "S.Engine.CxScanReport.0012";
//	public static final String CX_GENERATE_VULN_REPORT_ERROR						= "S.Portal.CxScanReport.0004";
//	public static final String CX_GENERATE_VULN_DATA_ERROR							= "S.Portal.CxScanReport.0005";
	public static final String CX_SCAN_REPORT_ERROR									= "S.Portal.CxScanReport.0006";
	public static final String INVALID_CX_REPORT_NAME								= "S.Portal.CxScanReport.0007";
//	public static final String UBSPORTAL_EXCEPTION 									= "U.UBSPortalException";
//	public static final String CX_GENERATE_META_REPORT_ERROR						= "S.Portal.CxScanReport.0008";
	public static final String INVALID_CX_REPORT_PARAMETER							= "S.Portal.CxScanReport.0009";
	public static final String INVALID_SCAN_PARAMETER								= "S.Portal.CxScanReport.0015";
//	public static final String INVALID_XML_REPORT									= "S.Portal.CxScanReport.0012";
//	public static final String XML_PARSING_PROBLEM 									= "S.Portal.CxScanReport.0013";
	public static final String INVALID_REGISTRATION_DATE 							= "S.Portal.CxScanReport.0014";
	public static final String CANNOT_GET_YCLOUD_CONFIG_FILE 						= "S.Portal.YCloudUtil.0001";
	public static final String YCLOUD_RESPONSE_ERR 									= "S.Portal.YCloudUtil.0002";
	public static final String YCLOUD_INSTANCE_ERR 									= "S.Portal.YCloudUtil.0003";
	public static final String INVALID_BUCKETNAME 									= "S.Portal.YCloudUtil.0004";
//	public static final String BUCKETNAME_EXISTS 									= "S.Portal.YCloudUtil.0005";
	public static final String INVALID_FILENAME 									= "S.Portal.YCloudUtil.0006";
//	public static final String CREATE_BUCKET_UNSUCCESSFUL  							= "S.Portal.YCloudUtil.0007";
//	public static final String YCLOUD_UPLOAD_ERROR  								= "S.Portal.YCloudUtil.0008"; 
//	public static final String INVALID_TAG_PARAMETER  								= "S.Portal.XmlUtil.0001";
//	public static final String CX_SCAN_STATUS_UPDATE_ERROR 							= "S.Portal.CxScanStatus.0002";
	public static final String DOWNLOAD_REPORT_FAILED 								= "S.Portal.DownloadReport.0008";

	public static final String CX_PROCESSING_EXCEPTION 								= "S.CxSuite.ProcessException.0001";
//	
//	/* -------------------- PORTAL API ERROR -------------------- */
	public static final String API_INVALID_PROJECT_NAME						= "S.PortalAPI.CreateProject.0001";
	public static final String API_INVALID_SCAN_CONFIG						= "S.PortalAPI.GetScanConfig.0001";
	public static final String CX_SERVER_ERROR			 					= "S.Engine.CxSuiteAPICall.0001";
	public static final String DECODER_EXCEPTION 							= "S.Portal.Decode.0001";
	public static final String YCLOUD_REPORT_DELETE_ERROR 					= "S.Portal.YCloudUtil.0001";
	
	public static final String DOWNLOAD_VULN_LIST_FAILED 					= "S.Portal.DownloadVulnList.0001";
	public static final String DOWNLOAD_VULN_LIST_USER_NO_RIGHTS			= "S.Portal.DownloadVulnList.0002";
//	public static final String ILLEGAL_STATE_EXCEPTION 						= "S.PortalAPI.CxGenerateReport.0001";
	public static final String CX_REGENERATE_SCAN_REPORT_ERROR 				= "S.PortalAPI.CxRegenerateReport.0001";
//	public static final String THREADEXCEPTION 								= "S.PortalAPI.CxThreadException.0001";
//	public static final String SCHEDULER_FAILED 							= "S.PortalAPI.CxSchedulerFailed.0001";
//	
	public static final String ANDROID_DELETE_NON_EXISTING_PROJECT 			= "U.Engine.DeleteProject.0001";
	public static final String ANDROID_DELETE_NON_EXISTING_SCAN				= "U.Portal.DeleteScan.0001";
//	public static final String ANDROID_NON_EXISTING_SCAN_STATUS				= "U.Engine.GetScanStatus.0001";
	public static final String ANDROID_STOP_NON_EXISTING_SCAN				= "U.Engine.StopScan.0001";

	public static final String ALGORITHM_DOES_NOT_EXIST 					= "S.Portal.EncryptDecrypt.0001";
	public static final String PADDING_DOES_NOT_EXIST 						= "S.Portal.EncryptDecrypt.0002";
	public static final String KEY_INVALID 									= "S.Portal.EncryptDecrypt.0003";
	public static final String PADDING_BAD 									= "S.Portal.EncryptDecrypt.0004";
	public static final String BLOCK_SIZE_ILLEGAL 							= "S.Portal.EncryptDecrypt.0005";

//	public static final String ANDROID_GET_STATISTICS_ERROR					= "U.Engine.GetStatistics.0001";
//	public static final String ANDROID_GET_SCAN_REPORT_ERROR				= "U.Engine.GetScanReport.0001";
//	
//	public static final String CX_STATUS_RESPONSE							= "S.Engine.CxAPICall.0001";
//	public static final String CX_NULL_RESPONSE_ROUTE						= "S.Engine.CxStatusRoute.0001";
//	public static final String CX_FALSE_RESPONSE_ROUTE						= "S.Engine.CxStatusRoute.0002";
//	public static final String CX_NULL_CURRENT_STATUS_RESPONSE_ROUTE		= "S.Engine.CxStatusRoute.0003";
//	public static final String CX_SCAN_ID_ERROR_RESPONSE_ROUTE				= "S.Engine.CxStatusRoute.0004";
//	public static final String CX_NO_CONNECTION_ROUTE						= "S.Engine.CxStatusRoute.0005";
//	public static final String FAILED_SCAN_DELETE_SCHEDULER_ERROR			= "S.PortalAPI.FailedScanDeleteSchedulerException.0001";
//	
	public static final String MAIL_SENDER_RUN_RECIPIENTS_INVALID 			= "S.Portal.MailSenderRun.0001";
//	public static final String MESSAGING_EXCEPTION 							= "S.Portal.MessagingException.0001";
	public static final String ADDRESS_EXCEPTION 							= "S.Portal.AddressException.0001";
//	
	public static final String GENERATE_EMAIL_EMAIL_EVENT_INVALID 			= "S.Portal.GenerateEmail.0001";
	public static final String GENERATE_EMAIL_ENTITY_ID_INVALID 			= "S.Portal.GenerateEmail.0002";
	public static final String GENERATE_EMAIL_USER_DOES_NOT_EXIST 			= "S.Portal.GenerateEmail.0003";
	public static final String GENERATE_EMAIL_SCAN_DOES_NOT_EXIST 			= "S.Portal.GenerateEmail.0004";
	public static final String GENERATE_EMAIL_SUBJECT_INVALID 				= "S.Portal.GenerateEmail.0005";
	public static final String GENERATE_EMAIL_CONTENT_INVALID 				= "S.Portal.GenerateEmail.0006";
	public static final String GENERATE_EMAIL_COMPANY_ID_INVALID			= "S.Portal.GenerateEmail.0007";
//	
	public static final String GET_RECIPIENTS_BY_ROLE_ROLE_INVALID 			= "S.Portal.GetRecipientsByRole.0001";
//	
	public static final String FILE_NOT_FOUND_EXCEPTION 					= "S.Portal.FileNotFoundException.0001";
	public static final String UNSUPPORTED_ENCODING_EXCEPTION 				= "S.Portal.UnsupportedEncodingException.0001";
//	
	public static final String DOWNLOAD_MANUAL_NO_MANUAL					= "S.Portal.DownloadManual.0001";
	public static final String DOWNLOAD_MANUAL_FAILED						= "S.Portal.DownloadManual.0002";
//	
//	public static final String FAILED_SCAN_DELETE_NO_MODIFIED_DATE			= "S.PortalAPI.FailedScanDelete.0001";
//	public static final String INVALID_HASH_VALUE							= "S.Portal.HashValue.0001";
//	
//	public static final String DOWNLOAD_SUMMARY_USER_NO_RIGHTS				= "S.Portal.DownloadSummary.0001";
	public static final String DOWNLOAD_SUMMARY_PERIOD_INVALID				= "S.Portal.DownloadSummary.0002";
	public static final String DOWNLOAD_SUMMARY_NO_SUMMARY					= "S.Portal.DownloadSummary.0003";

//	public static final String CX_GENERATE_VULN_SUMMARY_DATA_ERROR			= "S.Portal.VulnSummaryData.0001";
//	public static final String CX_GENERATE_META_SUMMARY_DATA_ERROR			= "S.Portal.MetaSummaryData.0001";
	
	public static final String DOWNLOAD_PROJECT_LIST_NO_LIST				= "S.Portal.DownloadProjectList.0001";
	
//	public static final String INVALID_IOS_SCAN_PARAM 						= "S.Portal.RegisterIOSScan.0001";
//	public static final String INVALID_IOS_SOURCE_FILE 						= "S.Portal.IOSSourceCodeUpload.0001";
//	public static final String INVALID_IOS_PROJECTNAME 						= "S.Portal.IOSProjectName.0001";
//	
	public static final String GET_PRESET_LIST_WEB_SERVICE_API_FAILED 		= "S.Engine.GetPresetList.0001";
//	
	public static final String CX_LOGIN_RESPONSE_IS_NULL 					= "S.Engine.Login.0002";
	public static final String CX_LOGIN_EXPIRED_LICENSE 					= "S.Engine.Login.0003";
	public static final String CX_LOGIN_WEB_SERVICE_API_FAILED				= "S.Engine.Login.0004";
	public static final String CX_LOGIN_INVALID_URL 						= "S.Engine.Login.0005";
//	
	public static final String PARSE_CONFIGURATION_EXCEPTION 				= "S.PortalAPI.ParseXMLFile.0001";
	public static final String SAX_EXCEPTION 								= "S.PortalAPI.ParseXMLFile.0002";
	public static final String FACTORY_CONFIGURATION_ERROR 					= "S.PortalAPI.ParseXMLFile.0003";
	
	public static final String INVALID_REPORT 					= "S.PortalAPI.GetReport.0001";
	public static final String INVALID_SCAN_STATUS 				= "S.PortalAPI.GetScan.0004";
	
}
