package jp.ubsecure.portal.jubjub.portlet.custom.exception;

import com.liferay.portal.kernel.exception.UserPasswordException;

public class UBSPortalUserPasswordException extends UserPasswordException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4459045119401939215L;

	public UBSPortalUserPasswordException(int type) {
		super(type);
	}

	public static class MustNotExceedMaximumBytes extends UBSPortalUserPasswordException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3745218238882381929L;

		public MustNotExceedMaximumBytes(String message) {
			super(PASSWORD_INVALID);
			
			this.message = message;
		}
		
		public final String message;
	}
	
	public static class MustNotContainSpecialCharacters extends UBSPortalUserPasswordException {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3634445887236190910L;

		public MustNotContainSpecialCharacters(String message) {
			super(PASSWORD_INVALID);
			
			this.message = message;
		}
		
		public final String message;
	}
}
