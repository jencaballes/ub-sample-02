package jp.ubsecure.portal.jubjub.portlet.custom.exception;

import com.liferay.portal.kernel.exception.UserEmailAddressException;

/**
 * UBSPortalUserEmailAddress Exception
 */
public class UBSPortalUserEmailAddressException extends UserEmailAddressException {

	/** serialVersionUID */
	private static final long serialVersionUID = -8715394222026004009L;

	public static class MustNotExceedMaximumBytes extends UBSPortalUserScreenNameException {

		/** serialVersionUID */
		private static final long serialVersionUID = -3220932432702057658L;

		/**
		 * Must not exceed maximum bytes
		 * 
		 * @param message
		 */
		public MustNotExceedMaximumBytes(String message) {
			super();

			this.message = message;
		}

		public final String message;

	}
}
