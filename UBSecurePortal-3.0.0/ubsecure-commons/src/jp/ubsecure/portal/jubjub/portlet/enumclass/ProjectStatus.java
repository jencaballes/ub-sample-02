package jp.ubsecure.portal.jubjub.portlet.enumclass;

public enum ProjectStatus {
	COMPLETE(1, "完了"),
	NOT_YET_COMPLETE(2, "公開中");

	int iValue;
	String strStatus;
	
	private ProjectStatus(int iValue, String strStatus){
		this.iValue= iValue;
		this.strStatus = strStatus;
	}
	
	public int getInteger() {
		return iValue;
	}
	
	public String getString() {
		return strStatus;
	}
}