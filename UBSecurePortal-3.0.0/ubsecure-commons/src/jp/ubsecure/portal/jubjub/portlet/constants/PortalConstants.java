package jp.ubsecure.portal.jubjub.portlet.constants;

public class PortalConstants {

	/* NUMERIC CONSTANTS */
	public static final long LONG_ZERO = 0L;

	public static final double DOUBLE_ZERO = 0.0;

	public static final int MEGABYTE_EQUIVALENT = 1048576;

	public static final int FAIL = 6;

	public static final int PROJECT_TYPE_LIMIT = 3;

	public static final int REPORT_COUNT_LIMIT = 4;

	public static final String STR_DOT = ".";

	public static final String APK_FILE_EXTENSION = "apk";

	public static final String EXCEL_FILE_EXTENSION = "xls";

	public static final String ZIP_FILE_EXTENSION = "zip";

	public static final String CSV_FILE_EXTENSION = "csv";

	public static final String CONTENT_DISP = "Content-Disposition";

	public static final String ATTACHMENT = "attachment; filename=";

	public static final String PROCESS_UT = "UT";

	public static final String PROCESS_ST = "ST";

	public static final String PROCESS_PA = "PA";

	public static final String CX_FOLDER = "SOURCE_";

	public static final String REPORT_DATE_FORMAT = "yyyyMMdd";

	public static final String CONFIG_FILE = "ubsportal-config.properties";

	public static final String VULNERABILITY_MASTER_FILE = "vulnerability-master.csv";

	public static final String RESOURCE_FOLDER = "ubsportal-resource";

	public static final String UPLOAD_FOLDER = "ubsportal-upload";

	public static final String DATA_FOLDER = "data";

	public static final String DOWNLOAD_FOLDER = "ubsportal-download";

	public static final String DOWNLOAD_REPORT_PREFIX = "report_PJ";

	public static final String PROJECT_PREFIX = "project-";

	public static final String SCAN_PREFIX = "scan-";

	public static final String SUMMARY_CSV = "summary";

	public static final String DETAIL_CSV = "detail";

	public static final String DOWNLOAD_DATE_FORMAT = "yyyyMMddhhmmss";

	public static final String STR_UNDERSCORE = "_";

	public static final String UTF8 = "UTF-8";

	public static final String SHIFT_JIS = "Shift-JIS";

	public static final boolean TRUE = true;

	public static final boolean FALSE = false;

	public static final String STRING_EMPTY = "";

	public static final String STR_SPACE = " ";

	public static final String STR_FWD_SLASH = "/";

	public static final int CX_CURRENT_STATUS_QUEUED = 1;

	public static final String CX_SIGNATURE_ID = "cxsuite.default.presetID";

	public static final String CX_SCAN_CONFIG_ID = "cxsuite.default.scanconfigID";

	public static final String CX_SCHEDULER_USERNAME = "cxsuite.scheduler.username";

	public static final String CX_SCHEDULER_PASSWORD = "cxsuite.scheduler.password";

	public static final String CX_RESOLVERURL = "cxsuite.cxresolverurl";

	public static final int LANGUAGE_ID = 1041;

	public static final String YYYYMMDD = "yyyy/MM/dd";

	public static final String METACSV = "META.csv";

	public static final String VULNCSV = "VULN.csv";

	public static final int METACSVTYPE = 3;

	public static final int VULNCSVTYPE = 4;
	
	public static final int XMLTYPE = 2;
	
	public static final int WORDTYPE = 6;
	
	public static final int XLSTYPE = 7;

	public static final int HTTP_OK_STATUS = 200;

	public static final int HTTP_NO_CONTENT = 204;

	public static final int READ_SPAN = 2048;

	public static final String COMMA = ",";

	public static final String BATCH = "Batch";

	public static final String SEVERITY_HIGH_EN = "High";

	public static final String SEVERITY_MID_EN = "Medium";

	public static final String SEVERITY_LOW_EN = "Low";

	public static final String SEVERITY_INFO_EN = "Information";
	
	public static final String SEVERITY_REMARKS_EN = "Remarks";

	public static final String SEVERITY_HIGH_JP = "高";

	public static final String SEVERITY_MID_JP = "中";

	public static final String SEVERITY_LOW_JP = "低";

	public static final String SEVERITY_INFO_JP = "情報";
	
	public static final String SEVERITY_REMARKS_JP = "備考";

	public static final String TAG_QUERY = "Query";

	public static final String TAG_RESULT = "Result";

	public static final String TAG_PATH = "Path";

	public static final String TAG_PATHNODE = "PathNode";

	public static final String TAG_NAME = "Name";

	public static final String TAG_ATTRIB_NAME = "name";

	public static final String TAG_ATTRIB_STATE = "State";

	public static final String TAG_ATTRIB_CXXMLRESULT = "CxXMLResults";

	public static final String TAG_ATTRIB_SCANSTART = "ScanStart";

	public static final String TAG_ATTRIB_SCANTIME = "ScanTime";

	public static final String TAG_ATTRIB_LOC = "LinesOfCodeScanned";

	public static final String TAG_ATTRIB_PRESET = "Preset";

	public static final String EXCLUDED_STATE = "1";

	public static final String HEADER_NUMBER = "No";

	public static final String HEADER_SEVERITY = "危険度";

	public static final String HEADER_VULN_NAME = "脆弱性名";

	public static final String HEADER_QUERY_NAME = "検出クエリ名";

	public static final String HEADER_NAME_INPUT = "名前（入力箇所）";

	public static final String HEADER_REPORTID = "Report ID";

	public static final String HEADER_SCAN_ID = "スキャンID";

	public static final String HEADER_PROJECT_NAME = "プロジェクト名";

	public static final String HEADER_SIGNATURE_ID = "シグネチャID";

	public static final String HEADER_CATEGORY = "シグネチャの脆弱性カテゴリ";

	public static final String HEADER_FILE_NAME = "脆弱性を検出したファイルの名称";

	public static final String HEADER_TRIGGER1 = "検出トリガ１";

	public static final String HEADER_TRIGGER2 = "検出トリガ２";

	public static final String HEADER_TRIGGER3 = "検出トリガ３";

	public static final String HEADER_FLAG = "除外フラグ";

	public static final String HEADER_COMMENT = "コメント";

	public static final String HEADER_CASE_NUMBER = "案件番号";

	public static final String HEADER_HIGH = "危険度高の数";

	public static final String HEADER_MED = "危険度中の数";

	public static final String HEADER_LOW = "危険度低の数";

	public static final String HEADER_INFO = "危険度情報の数";

	public static final String HEADER_ATTRIBUTE = "新規/定期";

	public static final String HEADER_PROCESS = "UT/ST/定期";

	public static final String HEADER_SCAN_START_TIME = "スキャン開始時間";

	public static final String HEADER_SCAN_EXEC = "スキャン実行時間";

	public static final String HEADER_LOC = "ソースコード行数";

	public static final String HEADER_PRESET = "使用したプリセット名";

	public static final String HEADER_GROUP_NAME = "会社名（所属グループ）	";

	public static final String HEADER_SIGNATURE = "使用したシグネチャセットの名称";

	public static final String HEADER_EXCLUSION_PKG = "除外パッケージ";

	public static final String HEADER_VEX_VERSION = "VEXのバージョン";

	public static final int TYPE_WEB_SCAN = 1;

	public static final int TYPE_SERVER_FILES = 2;

	public static final int TYPE_SERVER_SETTINGS = 3;
	
	/*----------------REPORT NAME---------------------------*/
	public static final String CXSUITE_REPORT_WORD				= "REPORT.docx";
	public static final String CXSUITE_REPORT_PDF 				= "REPORT.pdf";
	public static final String CXSUITE_REPORT_XML 				= "REPORT.xml";
	public static final String CXSUITE_REPORT_VULN 				= "VULN.csv";
	public static final String CXSUITE_REPORT_META 				= "META.csv";
	public static final String VEX_REPORT_XLS				    = "crawler_flow.xls";

	/* -------------------- JSP PATH -------------------- */
	public static final String MVC_PATH = "mvcPath";

	public static final String CX_PROJECT_REGISTRATION_JSP 					= "/html/cxsuite/projectmanagement/edit_project.jsp";

	public static final String CX_PROJECT_LIST_JSP 							= "/html/cxsuite/projectmanagement/project_list.jsp";

	public static final String CX_PROJECT_ACTION_JSP 						= "/html/cxsuite/projectmanagement/project_action.jsp";

	public static final String CX_SCAN_LIST_JSP 							= "/html/cxsuite/scanmanagement/scan_list.jsp";

	public static final String CX_SCAN_REGISTRATION_JSP 					= "/html/cxsuite/scanmanagement/edit_scan.jsp";

	public static final String CX_ENTIRE_SCAN_LIST_JSP 						= "/html/cxsuite/scanmanagement/entire_scan_list.jsp";

	public static final String CX_SCAN_ACTION_JSP 							= "/html/cxsuite/scanmanagement/scan_action.jsp";

	public static final String CX_ENTIRE_SCAN_ACTION_JSP 					= "/html/cxsuite/scanmanagement/entire_scan_action.jsp";

	public static final String CX_SCAN_LIST_REFRESH_JSP 					= "/html/cxsuite/scanmanagement/scan_list_refresh.jsp";

	public static final String CX_ENTIRE_SCAN_LIST_REFRESH_JSP 				= "/html/cxsuite/scanmanagement/entire_scan_list_refresh.jsp";

	public static final String CX_DOWNLOAD_REFERENCE_JSP 					= "/html/cxsuite/scanmanagement/download_reference.jsp";

	public static final String CX_DOWNLOAD_JSP 								= "/html/cxsuite/scanmanagement/download.jsp";

	public static final String ANDROID_PROJECT_REGISTRATION_JSP 			= "/html/android/projectmanagement/edit_project.jsp";

	public static final String ANDROID_PROJECT_LIST_JSP 					= "/html/android/projectmanagement/project_list.jsp";

	public static final String ANDROID_PROJECT_ACTION_JSP 					= "/html/android/projectmanagement/project_action.jsp";

	public static final String ANDROID_SCAN_LIST_JSP 						= "/html/android/scanmanagement/scan_list.jsp";

	public static final String ANDROID_SCAN_REGISTRATION_JSP 				= "/html/android/scanmanagement/edit_scan.jsp";

	public static final String ANDROID_ENTIRE_SCAN_LIST_JSP 				= "/html/android/scanmanagement/entire_scan_list.jsp";

	public static final String ANDROID_SCAN_ACTION_JSP 						= "/html/android/scanmanagement/scan_action.jsp";

	public static final String ANDROID_ENTIRE_SCAN_ACTION_JSP 				= "/html/android/scanmanagement/entire_scan_action.jsp";

	public static final String ANDROID_SCAN_LIST_REFRESH_JSP 				= "/html/android/scanmanagement/scan_list_refresh.jsp";

	public static final String ANDROID_ENTIRE_SCAN_LIST_REFRESH_JSP 		= "/html/android/scanmanagement/entire_scan_list_refresh.jsp";

	public static final String ANDROID_DOWNLOAD_JSP 						= "/html/android/scanmanagement/download.jsp";

	public static final String ANNOUNCEMENT_JSP 							= "/html/announcement/announcement.jsp";

	public static final String IOS_PROJECT_REGISTRATION_JSP 				= "/html/ios/projectmanagement/edit_project.jsp";

	public static final String IOS_PROJECT_LIST_JSP 						= "/html/ios/projectmanagement/project_list.jsp";

	public static final String IOS_PROJECT_ACTION_JSP 						= "/html/ios/projectmanagement/project_action.jsp";

	public static final String IOS_SCAN_LIST_JSP 							= "/html/ios/scanmanagement/scan_list.jsp";

	public static final String IOS_SCAN_REGISTRATION_JSP 					= "/html/ios/scanmanagement/edit_scan.jsp";

	public static final String IOS_ENTIRE_SCAN_LIST_JSP 					= "/html/ios/scanmanagement/entire_scan_list.jsp";

	public static final String IOS_SCAN_ACTION_JSP 							= "/html/ios/scanmanagement/scan_action.jsp";

	public static final String IOS_ENTIRE_SCAN_ACTION_JSP 					= "/html/ios/scanmanagement/entire_scan_action.jsp";

	public static final String IOS_SCAN_LIST_REFRESH_JSP 					= "/html/ios/scanmanagement/scan_list_refresh.jsp";

	public static final String IOS_ENTIRE_SCAN_LIST_REFRESH_JSP 			= "/html/ios/scanmanagement/entire_scan_list_refresh.jsp";

	public static final String IOS_DOWNLOAD_REFERENCE_JSP 					= "/html/ios/scanmanagement/download_reference.jsp";

	public static final String IOS_DOWNLOAD_JSP 							= "/html/ios/scanmanagement/download.jsp";

	public static final String VEX_PROJECT_REGISTRATION_JSP 				= "/html/vex/projectmanagement/edit_project.jsp";

	public static final String VEX_PROJECT_LIST_JSP 						= "/html/vex/projectmanagement/project_list.jsp";

	public static final String VEX_PROJECT_ACTION_JSP 						= "/html/vex/projectmanagement/project_action.jsp";

	public static final String VEX_SCAN_LIST_JSP 							= "/html/vex/scanmanagement/scan_list.jsp";

	public static final String VEX_SCAN_REGISTRATION_JSP 					= "/html/vex/scanmanagement/edit_scan.jsp";

	public static final String VEX_ENTIRE_SCAN_LIST_JSP 					= "/html/vex/scanmanagement/entire_scan_list.jsp";

	public static final String VEX_SCAN_ACTION_JSP 							= "/html/vex/scanmanagement/scan_action.jsp";

	public static final String VEX_ENTIRE_SCAN_ACTION_JSP 					= "/html/vex/scanmanagement/entire_scan_action.jsp";

	public static final String VEX_SCAN_LIST_REFRESH_JSP 					= "/html/vex/scanmanagement/scan_list_refresh.jsp";

	public static final String VEX_ENTIRE_SCAN_LIST_REFRESH_JSP 			= "/html/vex/scanmanagement/entire_scan_list_refresh.jsp";
	
	public static final String VEX_SCAN_DETAILS_JSP 						= "/html/vex/scanmanagement/scan_details.jsp";

	public static final String VEX_DOWNLOAD_REFERENCE_JSP 					= "/html/vex/scanmanagement/download_reference.jsp";

	public static final String VEX_DOWNLOAD_JSP 							= "/html/vex/scanmanagement/download.jsp";

	public static final String VEX_REGISTER_SCAN_SIMPLE_SETTING_JSP 		= "/html/vex/scanmanagement/register_scan_simple_setting.jsp";

	public static final String VEX_REGISTER_SCAN_ADVANCED_SETTING_JSP 		= "/html/vex/scanmanagement/register_scan_advanced_setting.jsp";

	public static final String VEX_REGISTER_SCAN_TARGET_INFORMATION_JSP 	= "/html/vex/scanmanagement/register_scan_target_information.jsp";
	
	public static final String VEX_REVIEW_DETECTION_RESULT_JSP 				= "/html/vex/scanmanagement/review_detection_result.jsp";
	
	public static final String VEX_REVIEW_DETECTION_RESULT_ACTION_JSP		= "/html/vex/scanmanagement/review_action.jsp";
	
	public static final String VEX_AUTOCRAWLING_SETTING_JSP					= "/html/vex/scanmanagement/register_scan_autocrawling_setting.jsp";
	
	public static final String VEX_LOGIN_FORM_JSP							= "/html/vex/scanmanagement/register_scan_login_form.jsp";
	
	public static final String VEX_REVIEW_DETECTION_DETECTION_JUDGEMENT_JSP	= "/html/vex/scanmanagement/review_detection_judgement.jsp";
	
	public static final String VEX_REVIEW_DETECTION_REVIEW_SCRUTINY_JSP		= "/html/vex/scanmanagement/review_scrutiny.jsp";

	/* -------------------- CONTENT TYPE -------------------- */
	public static final String CONTENT_TYPE_XLS 			= "application/vnd.ms-excel";

	public static final String CONTENT_TYPE_APK 			= "application/vnd.android.package-archive";

	public static final String CONTENT_TYPE_ZIP 			= "application/x-zip-compressed";

	public static final String CONTENT_TYPE_PDF 			= "application/pdf";

	public static final String CONTENT_TYPE_GENERAL_ZIP 	= "application/zip";

	public static final String CONTENT_TYPE_TEXT_HTML 		= "text/html";

	public static final String CONTENT_TYPE_JSON 			= "application/json";
	
	public static final String CONTENT_TYPE_EXP 			= ".exp";
	
	public static final String CONTENT_TYPE_PKCS12 			=".p12, .pfx";
	
	public static final String CONTENT_TYPE_DOCX 			="application/vnd.openxmlformats-officedocument.wordprocessingml.document";
	
	public static final String CONTENT_TYPE_DOC 			="application/msword";

	/* -------------------- USER EVENTS -------------------- */
	public static final int USER_EVENT_VIEW_PROJECT_LIST 		= 1; // PROJECT LIST // (CX, ANDROID, // IOS, VEX)

	public static final int USER_EVENT_VIEW_CREATE_PROJECT 		= 2; // PROJECT LIST // (CX, ANDROID, // IOS, VEX)

	public static final int USER_EVENT_ADD_PROJECT 				= 3; // PROJECT REGISTRATION // (CX, ANDROID, IOS, // VEX)

	public static final int USER_EVENT_VIEW_ENTIRE_SCAN_LIST 	= 4; // PROJECT // LIST (CX, // ANDROID, // IOS, VEX)

	public static final int USER_EVENT_SEARCH_PROJECT 			= 5; // PROJECT LIST (CX, // ANDROID, IOS, // VEX)

	public static final int USER_EVENT_VIEW_SCAN_LIST 			= 6; // PROJECT LIST (CX, // ANDROID, IOS, // VEX)

	public static final int USER_EVENT_DELETE_PROJECT 			= 7; // PROJECT LIST (CX, // ANDROID, IOS, // VEX)

	public static final int USER_EVENT_SEARCH_SCAN 				= 8; // ENTIRE SCAN LIST, // SCAN LIST (CX, // ANDROID, IOS, VEX)

	public static final int USER_EVENT_STOP_SCAN 				= 9; // ENTIRE SCAN LIST, SCAN // LIST (CX, ANDROID, // IOS, VEX)

	public static final int USER_EVENT_REEXECUTE_SCAN 			= 10; // ENTIRE SCAN LIST, // SCAN LIST (CX, // ANDROID, IOS, // VEX)

	public static final int USER_EVENT_REQUEST_REVIEW 			= 11; // ENTIRE SCAN LIST, // SCAN LIST (CX)

	public static final int USER_EVENT_CANCEL_REVIEW  			= 12; // ENTIRE SCAN LIST, // SCAN LIST (CX)

	public static final int USER_EVENT_REGENERATE_REPORT  		= 13; // ENTIRE SCAN // LIST, SCAN // LIST (CX)

	public static final int USER_EVENT_DELETE_SCAN  			= 14; // ENTIRE SCAN LIST, // SCAN LIST (CX, // ANDROID, IOS, // VEX)

	public static final int USER_EVENT_VIEW_SCAN_CHANGE  		= 15; // ENTIRE SCAN // LIST, SCAN // LIST (CX)

	public static final int USER_EVENT_UPDATE_SCAN  			= 16; // SCAN CHANGE (CX)

	public static final int USER_EVENT_VIEW_PROJECT_CHANGE  	= 17; // SCAN LIST // (CX, // ANDROID, // IOS, VEX)

	public static final int USER_EVENT_UPDATE_PROJECT  			= 18; // PROJECT CHANGE // (CX, ANDROID, // IOS, VEX)

	public static final int USER_EVENT_COMPLETE_PROJECT  		= 19; // SCAN LIST (CX, // ANDROID, IOS, // VEX)

	public static final int USER_EVENT_VIEW_CREATE_SCAN  		= 20; // SCAN LIST (CX, // ANDROID, IOS, // VEX)

	public static final int USER_EVENT_ADD_SCAN  				= 21; // SCAN REGISTRATION (CX, // ANDROID, IOS, VEX)

	public static final int USER_EVENT_SELECT_GROUP 			= 22;

	public static final int USER_EVENT_DOWNLOAD_REPORT  		= 23;

	public static final int USER_EVENT_DOWNLOAD_REFERENCE		= 24;

	public static final int USER_EVENT_UPDATE_ACTION 			= 25;

	public static final int USER_EVENT_FILTER_USERS 			= 26;

	public static final int USER_EVENT_ADD_USER 				= 27;

	public static final int USER_EVENT_REMOVE_USER 				= 28;

	public static final int USER_EVENT_DOWNLOAD_VULN 			= 29;

	public static final int USER_EVENT_OPEN_PROJECT 			= 30;

	public static final int USER_EVENT_CLEAR_SEARCH_PROJECT 	= 31;

	public static final int USER_EVENT_CLEAR_SEARCH_SCAN		= 32;

	public static final int USER_EVENT_DOWNLOAD_SUMMARY 		= 33;

	public static final int USER_EVENT_DOWNLOAD_PROJECT_LIST 	= 34;

	public static final int USER_EVENT_ADD_TARGET_INFORMATION 	= 35;

	public static final int USER_EVENT_DELETE_TARGET_INFORMATION = 36;

	public static final int USER_EVENT_VIEW_IMPORT_SCAN 		= 37;

	public static final int USER_EVENT_RECRAWL 					= 38;

	public static final int USER_EVENT_ABORT_CRAWL 				= 39;

	public static final int USER_EVENT_INTERRUPTED_CRAWL 		= 40;

	public static final int USER_EVENT_CANCEL_CRAWL				= 41;

	public static final int USER_EVENT_RESTART_CRAWL 			= 42;

	public static final int USER_EVENT_COPY_CRAWL_SETTING		= 43;

	public static final int USER_EVENT_INTERRUPTED_SCAN 		= 44;

	public static final int USER_EVENT_CANCEL_SCAN 				= 45;

	public static final int USER_EVENT_RESTART_SCAN 			= 46;

	public static final int USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN = 47;

	public static final int USER_EVENT_CONFIRM_RESULT_REVIEW 	= 48;

	public static final int USER_EVENT_DOWNLOAD_SCREEN_TRANSITION_DIAGRAM = 49;
	
	public static final int USER_EVENT_RESEND					 = 50;
	
	public static final int USER_EVENT_UPLOAD_SCAN 				 = 51;
	
	public static final int USER_EVENT_IMPORT_SCAN 				 = 52;
	
	public static final int USER_EVENT_REGISTER_SCAN_SIMPLE_SETTING     = 53;
	
	public static final int USER_EVENT_REGISTER_SCAN_ADVANCED_SETTING	= 54;
	
	public static final int USER_EVENT_GET_LOGIN_INFORMATION	        = 55;
	
	public static final int USER_EVENT_ADD_LOGIN_INFORMATION	        = 56;
	
	public static final int USER_EVENT_UPDATE_LOGIN_INFORMATION	        = 57;
	
	public static final int USER_EVENT_VIEW_DETECTION_RESULT 			= 58;
	
	public static final int USER_EVENT_SEARCH_DETECTION_REVIEW 			= 59;
	
	public static final int USER_EVENT_RESEND_DETECTION_REVIEW 			= 60;
	
	public static final int USER_EVENT_CLEAR_DETECTION_RESULT   		= 61;
	
	public static final int USER_EVENT_CONFIRM_DETECTION_RESULT   		= 62;
	
	public static final int USER_EVENT_PERFORM_SCAN 					= 63;

	/* -------------------- SCREEN LIST -------------------- */
	public static final int SCREEN_PROJECT_LIST				= 1;

	public static final int SCREEN_PROJECT_REGISTRATION		= 2;

	public static final int SCREEN_ENTIRE_SCAN_LIST			= 3;

	public static final int SCREEN_SCAN_LIST 				= 4;

	public static final int SCREEN_PROJECT_CHANGE 			= 5;

	public static final int SCREEN_SCAN_REGISTRATION 		= 6;

	public static final int SCREEN_SCAN_CHANGE 				= 7;
	
	public static final int SCREEN_SCAN_SIMPLE_SETTING_REGISTRATION	    	= 8;

	public static final int SCREEN_SCAN_ADVANCED_SETTING_REGISTRATION    	= 9;

	public static final int SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION  	= 10;

	public static final int SCREEN_SCAN_AUTOCRAWLING_SETTING_REGISTRATION	= 11;
	
	public static final int SCREEN_SCAN_DETECTION_RESULT_REVIEW         	= 12;
	

	/* -------------------- SUCCESS CODES -------------------- */
	public static final String ADD_PROJECT_SUCCESSFUL 			= "U.Portal.Success.AddProject";

	public static final String DELETE_PROJECT_SUCCESSFUL 		= "U.Portal.Success.DeleteProject";

	public static final String STOP_SCAN_SUCCESSFUL 			= "U.Portal.Success.StopScan";

	public static final String REEXECUTE_SCAN_SUCCESSFUL 		= "U.Portal.Success.ReexecuteScan";

	public static final String REQUEST_REVIEW_SUCCESSFUL 		= "U.Portal.Success.RequestReview";

	public static final String CANCEL_REVIEW_SUCCESSFUL 		= "U.Portal.Success.CancelReview";

	public static final String REGENERATE_REPORT_SUCCESSFUL 	= "U.Portal.Success.RegenerateReport";

	public static final String DELETE_SCAN_SUCCESSFUL 			= "U.Portal.Success.DeleteScan";

	public static final String UPDATE_PROJECT_SUCCESSFUL 		= "U.Portal.Success.UpdateProject";

	public static final String COMPLETE_PROJECT_SUCCESSFUL 		= "U.Portal.Success.CompleteProject";

	public static final String ADD_SCAN_SUCCESSFUL			 	= "U.Portal.Success.AddScan";

	public static final String UPDATE_SCAN_SUCCESSFUL 			= "U.Portal.Success.UpdateScan";

	public static final String OPEN_PROJECT_SUCCESSFUL 			= "U.Portal.Success.OpenProject";

	public static final String RECRAWL_SUCCESSFUL 				= "U.Portal.Success.Recrawl";

	public static final String ABORT_CRAWL_SUCCESSFUL 			= "U.Portal.Success.AbortCrawl";

	public static final String INTERRUPTED_CRAWL_SUCCESSFUL 	= "UABORT_.Portal.Success.InterruptedCrawl";

	public static final String CANCEL_CRAWL_SUCCESSFUL 			= "U.Portal.Success.CancelCrawl";

	public static final String RESTART_CRAWL_SUCCESSFUL 		= "U.Portal.Success.RestartCrawl";

	public static final String COPY_CRAWL_SETTING_SUCCESSFUL 	= "U.Portal.Success.CopyCrawlSetting";

	public static final String INTERRUPTED_SCAN_SUCCESSFUL 		= "U.Portal.Success.InterruptedScan";

	public static final String CANCEL_SCAN_SUCCESSFUL 			= "U.Portal.Success.CancelScan";

	public static final String RESTART_SCAN_SUCCESSFUL 			= "U.Portal.Success.RestartScan";

	public static final String COPY_CRAWL_RESULT_AND_SCAN_SUCCESSFUL = "U.Portal.Success.CopyCrawlAndScan";

	public static final String REGISTER_SCAN_SUCCESSFUL 		= "U.Portal.Success.RegisterScan";
	
	public static final String RESEND_SUCCESSFUL 				= "U.Portal.Success.Resend";
	
	public static final String UPLOAD_SCAN_SUCCESSFUL			= "U.Portal.Success.UploadScan";
	
	public static final String IMPORT_SCAN_SUCCESSFUL			= "U.Portal.Success.ImportScan";
	
	public static final String AUTO_CRAWL_SUCCESSFUL 			= "U.Portal.Success.AbortCrawl";
	
	public static final String ADD_REVIEW_SCRUTINY_SUCCESSFUL	= "U.Portal.Success.AddReviewScrutiny";

	/* ------------- USER MANAGEMENT ---------- */
	public static final String SYSTEM_ADMINISTRATOR = "System Administrator";

	public static final String OVERALL_ADMINISTRATOR = "Overall Administrator";

	public static final String GROUP_ADMINISTRATOR = "Group Administrator"; // not yet supported at this phase

	public static final String GENERAL_USER = "General User";

	public static final String ADMINISTRATOR = "Administrator";

	public static final String CX_NAME = "ソースコード診断";

	public static final String ANDROID_NAME = "Android静的解析";

	public static final String IOS_NAME = "iOS静的解析";

	public static final String VEX_NAME = "Vex動的解析";

	public static final String VEX_HOST = "vex.server.localhost";

	public static final String VEX_USERNAME = "vex.server.username";

	public static final String VEX_PASSWORD = "vex.server.password";

	public static final String ADD_USER = "add user";

	public static final String EDIT_USER = "edit user";

	public static final int ACCOUNT_LOCK_FALSE = 1;

	public static final int ACCOUNT_LOCK_TRUE = 2;

	public static final String SUCCESS_ADD = "successAdd";

	public static final String SUCCESS_EDIT = "successEdit";

	public static final String SUCCESS_DELETE = "successDelete";

	public static final String SUCCESS_PWD_EDIT = "successPwdEdit";

	public static final String RENDER_PARAMETER_STRUTS_ACTION = "_125_struts_action";

	public static final String URL_USER_ADMIN_VIEW = "/users_admin/view";

	public static final String RENDER_PARAMETER_TOOLBAR_ITEM = "_125_toolbarItem";

	public static final String URL_VIEW_ALL_USERS = "view-all-users";

	public static final String RENDER_PARAMETER_USERLIST_VIEW = "_125_usersListView";

	public static final String URL_FLAT_USERS = "flat-users";

	public static final String BACK_URL = "backURL";

	public static final String USER_ID = "userId";

	public static final String PID = "pid";

	public static final String APPLICATION_JASON = "application/json";

	public static final String UBSP = "ubsp";

	public static final String USERLISTS = "userLists";

	public static final String REDIRECT = "redirect";

	public static final String CLOSE_REDIRECT = "closeRedirect";

	public static final double VERSION = 6.0;

	public static final String REDIRECT_TOKEN = "&#";

	public static final String ADD_PROCESS_ACTION = "add-process-action-success-action";

	public static final String SUCCESS_MESSAGE = "successMessage";

	public static final String REQUEST_PROCESSED = "request_processed";

	public static final String AUTO_PASSWORD = "autoPassword";

	public static final String REMINDER_QUERY_QUESTION = "reminderQueryQuestion";

	public static final String REMINDER_QUERY_ANSWER = "reminderQueryAnswer";

	public static final String AUTO_SCREENNAME = "autoScreenName";

	public static final String PREFIX_ID = "prefixId";

	public static final String SUFFIX_ID = "suffixId";

	public static final String MALE = "male";

	public static final String OPEN_ID = "openId";

	public static final String STR_LANGUAGE_ID = "languageId";

	public static final String TIMEZONE_ID = "timeZoneId";

	public static final String GREETING = "greeting";

	public static final String COMMENTS = "comments";

	public static final String SMS_SN = "smsSn";

	public static final String AIM_SN = "aimSn";

	public static final String FACEBOOK_SN = "facebookSn";

	public static final String ICQ_SN = "icqSn";

	public static final String JABBER_SN = "jabberSn";

	public static final String MSN_SN = "msnSn";

	public static final String MYSPACE_SN = "mySpaceSn";

	public static final String SKYPE_SN = "skypeSn";

	public static final String TWITTER_SN = "twitterSn";

	public static final String YM_SN = "ymSn";

	public static final String JOB_TITLE = "jobTitle";

	public static final String GROUP_SEARCH_CONTAINTER_PK = "groupsSearchContainerPrimaryKeys";

	public static final String GROUP_ID = "groupID";

	public static final String USER_GROUP_CONTAINER_PK = "userGroupSearchContainerPrimaryKeys";

	public static final String USERS_SEARCH_CONTAINER_PK = "usersSearchContainerPrimaryKeys";

	public static final String ACCOUNT_LOCK = "accountlock";

	public static final String EMAIL_ADDRESS = "emailAddress";

	public static final String ADD_EDIT_USER = "addEditUser";

	public static final String FIRST_NAME = "firstName";

	public static final String PASSWORD = "password";

	public static final String PASSWORD_2 = "password2";

	public static final String ROLE_NAME = "rolename";

	public static final String GROUP_NAME = "groupname";

	public static final String DATE_FORMAT = "yyyy/MM/dd";

	public static final String DATE_FORMAT_HH_MM = "yyyy/MM/dd HH:mm";
	
	public static final String DATE_FORMAT_M_D_Y_HH_MM = "MM/dd/yyyy HH:mm";

	public static final String TIME_FORMAT_HH_MM = "HH:mm";

	public static final String TIMEZONE_TOKYO = "Asia/Tokyo";

	public static final String EXPIRATION_DATE = "expirationDate";

	public static final String ACC_EXPIRATION_DATE = "acctexpirationdate";

	public static final String STR_EMAIL = "strEmailAddress";

	public static final String ADD = "add";

	public static final String EDIT = "edit";

	public static final String STR_FIRSTNAME = "strFirstName";

	public static final String SERVICE_CONTEXT = "serviceContext";

	public static final String ORGANIZATION = "organization";

	public static final String COND1 = "strEmailAddress.getBytes().length";

	public static final String COND2 = "strFirstName == null || strFirstName == ''";

	public static final String COND3 = "strFirstName.getBytes().length";

	public static final String COND5 = "strUserRole.equalsIgnoreCase(PortalConstants.GENERAL_USER)";

	public static final String COND6 = "dteExpirationDate.before(new Date())";

	public static final String COND7 = "strScreenName.getBytes().length";

	public static final String DELETE_USER_IDS = "deleteUserIds";

	public static final String LONG_DELETE_USER_IDS = "lDeleteUserIds";

	public static final String DELETE_USER_F = "deleteUser";

	public static final String IS_SEARCHED = "isSearched";

	public static final String USER_ID_REQUEST = "user_id";

	public static final String USER_NAME_REQUEST = "user_name";

	public static final String EXPIRATION_DATE_LOW = "expirationDateLow";

	public static final String EXPIRATION_DATE_HIGH = "expirationDateHigh";

	public static final String GROUP_NAME_REQUEST = "group_name";

	public static final String ACCOUNT_LOCK_REQUEST = "account_lock";

	public static final String SEARCH_USER_F = "searchUser";

	public static final String CLEAR_SEARCH_USER_F = "clearSearchUser";

	public static final String DATE_EXPIRATION_LOW = "dteExpirationDateLow";

	public static final String DATE_EXPIRATION_HIGH = "dteExpirationDateHigh";

	public static final String EXPIRY_LOW = "expiryLow";

	public static final String EXPIRY_HIGH = "expiryHigh";

	public static final String LONG_COMPANY_ID = "lCompanyId";

	public static final String STR_ROLE = "strRole";

	public static final String STR_GROUP_NAME = "strGroupName";

	public static final String LONG_ORG_ID = "lOrgId";

	public static final String LIST_BY_ROLE = "uListByRole";

	public static final String LONG_ROLE_ID = "lRoleId";

	public static final String STR_SCREEN_NAME = "strScreenName";

	public static final String PERCENT = "\\%";

	public static final String ERROR = "error";

	public static final int API_VERSION = 1;

	public static final int INT_ZERO = 0;

	public static final int INT_ONE = 1;

	public static final char CHAR_PERCENT = '%';

	/* ------------- GROUP MANAGEMENT ---------- */

	public static final String ORGANIZATION_ID = "organizationId";

	public static final String PARENT_ORG_SEARCH_CONTAINER_PK = "parentOrganizationSearchContainerPrimaryKeys";

	public static final String NAME = "name";

	public static final String COND8 = "sGroupName.getBytes().length";

	public static final String ADD_UPDATE_GROUP = "addUpdateGroup";

	public static final String STATUS_ID = "statusId";

	public static final String TYPE = "type";

	public static final String REGION_ID = "regionId";

	public static final String COUNTRY_ID = "countryId";

	public static final String SITE = "site";

	public static final String CXSUITE = "cxsuite";

	public static final String ANDROID = "android";
	public static final String IOS = "ios";
	public static final String VEX = "vex";
	public static final String CXSERVICE_USE_AUTH = "cxServiceUseAuth";
	public static final String ANDROIDSERVICE_USE_AUTH = "androidServiceUseAuth";
	public static final String IOSSERVICE_USE_AUTH = "iOSServiceUseAuth";
	public static final String VEXSERVICE_USE_AUTH = "vexServiceUseAuth";
	public static final String S_GROUP_NAME = "sGroupName";
	public static final String LONG_PARENT_ORG_ID = "lParentOrganizationId";
	public static final String S_TYPE = "sType";
	public static final String LONG_REGION_ID = "lRegionId";
	public static final String LONG_COUNTRY_ID = "lCountryId";
	public static final String INT_STATUS_ID = "iStatusId";
	public static final String S_COMMENTS = "sComments";
	public static final String B_SITE = "bSite";
	public static final String L_ADDRESSES = "lstAddresses";
	public static final String L_EMAIL = "lstEmailAddresses";
	public static final String L_ORG_LABORS = "lstOrgLabors";
	public static final String L_PHONES = "lstPhones";
	public static final String L_WEBSITES = "lstwebsites";
	public static final String SCSERVICE_CONTEXT = "scServiceContext";
	public static final String DELETE_ORG_IDS = "deleteOrganizationIds";
	public static final String LONG_DELETE_ORG_IDS = "lDeleteOrganizationIds";
	public static final String DELETE_GROUP = "deleteGroup";
	public static final String LONG_DELETE_ORG_ID = "lDeleteOrganizationId";
	public static final String GROUP_NAME_R = "groupName";
	public static final String COND9 = "sGroupName.getBytes().length";
	public static final String SEARCH_GROUP = "searchGroup";
	public static final String ORG_LISTS = "orgLists";
	public static final String URL_VIEW_ALL_ORGS = "view-all-organizations";
	public static final String URL_FLAT_ORGS = "flat-organizations";

	/* ------------- LOGIN ---------- */

	public static final String EXPANDO_DIR = "com.liferay.portal.model.User";
	public static final String CUSTOM_FIELDS = "CUSTOM_FIELDS";
	public static final String INT_AUTH = "iAuth";
	public static final String DATE_EXPIRED_DATE = "dtExpiredDate";
	public static final String DATE_CURRENT_DATE = "dtCurrentDate";
	public static final String USER_LOGIN = "userLogin";
	public static final String EXPIRED_USER = "expiredUser";
	public static final String FUNCTION_NAME = "functionName";
	public static final String GET_USER_BY_EMAIL_ADDRESS = "getUserByEmailAddress";
	public static final String STR_LOGIN = "strLogin";
	public static final String GET_DATA = "getData";
	public static final String CLASS_NAME = "className";
	public static final String TABLE_NAME = "tableName";
	public static final String COLUMN_NAME = "columnName";
	public static final String USER = "user";
	public static final String AUTH_BY_EMAIL = "authenticateByEmailAddress";
	public static final String HEADER_MAP = "headermap";
	public static final String PARAMETER_MAP = "parameterMap";
	public static final String RESULTS_MAP = "resultsMap";
	public static final String PORTLET_CONFIG = "portletConfig";
	public static final String ACTION_REQUEST = "actionRequest";
	public static final String ACTION_RESPONSE = "actionResponse";
	public static final String ORIG_LOGIN = "origLogin";
	public static final String USER_ROLE = "userRole";
	public static final String CXSESSION_ID = "CxSessionId";
	public static final String USE_ANDROID = "useAndroid";
	public static final String USE_CXSUITE = "useCxSuite";
	public static final String USE_IOS = "useIOS";
	public static final String USE_VEX = "useVex";
	public static final String LOGIN = "login";
	public static final String LOGGED_IN = "loggedIn";
	public static final String SESSION_ID = "sessionId";
	public static final String CXNAME = "cxName";
	public static final String ANDROIDNAME = "androidName";
	public static final String IOSNAME = "iOSName";
	public static final String VEXNAME = "vexName";
	public static final String LOGGED_IN_CX = "loggedInCx";

	/* ------------------------- METHOD NAMES ------------------------- */
	public static final String METHOD_GET_PROJECTS = "getProjects";
	public static final String METHOD_GET_PROJECT = "getProject";
	public static final String METHOD_GET_PROJECT_USERS = "getProjectUsers";
	public static final String METHOD_UPDATE_PROJECT = "updateProject";
	public static final String METHOD_DELETE_PROJECT = "deleteProject";
	public static final String METHOD_COMPLETE_PROJECT = "completeProject";
	// public static final String METHOD_VIEW_SCAN_LIST = "viewScanList";
	public static final String METHOD_GET_SCAN = "getScan";
	public static final String METHOD_ADD_SCAN = "addScan";
	public static final String METHOD_GET_SCANS = "getScans";
	// public static final String METHOD_VIEW_ENTIRE_SCAN_LIST =
	// "viewEntireScanList";
	public static final String METHOD_GET_ENTIRE_SCANS 												= "getEntireScans";
	public static final String METHOD_STOP_SCAN 													= "stopScan";
	public static final String METHOD_REEXECUTE_SCAN 												= "reexecuteScan";
	public static final String METHOD_DELETE_SCAN 													= "deleteScan";
	public static final String METHOD_GET_SCANS_FOR_REFRESH 										= "getScansForRefresh";
	public static final String METHOD_GET_ENTIRE_SCANS_FOR_REFRESH 									= "getEntireScansForRefresh";
	public static final String METHOD_GET_USER 														= "getUser";
	public static final String METHOD_GET_ORGANIZATIONS 											= "getOrganizations";
	public static final String METHOD_GET_USER_ORGANIZATIONS 										= "getUserOrganizations";
	public static final String METHOD_GET_ORGANIZATION_USERS 										= "getOrganizationUsers";
	public static final String METHOD_GET_PRESETS 													= "getPresets";
	public static final String METHOD_UPDATE_SCAN 													= "updateScan";
	public static final String METHOD_REQUEST_REVIEW 												= "requestReview";
	public static final String METHOD_CANCEL_REVIEW 												= "cancelReview";
	public static final String METHOD_REGENERATE_REPORT 											= "regenerateReport";
	// public static final String METHOD_CHECK_CX_SCAN_STATUS =
	// "checkCxScanStatus";
	public static final String METHOD_IS_PROJECT_COMPLETE 											= "isProjectComplete";
	public static final String METHOD_GET_REPORTS 													= "getReports";
	public static final String METHOD_SERVE_RESOURCE 												= "serveResource";
	public static final String METHOD_IS_APK_VALID 													= "isAPKValid";
	public static final String METHOD_CONVERT_HEX_TO_STRING 										= "convertHexToString";
	public static final String METHOD_CHECK_DB_CONNECTION 											= "checkDBConnection";
	public static final String METHOD_RUN 															= "run";
	public static final String METHOD_REGISTER_ANDROID_SCAN 										= "registerAndroidScan";
	public static final String METHOD_STOP_ANDROID_SCAN 											= "stopAndroidScan";
	public static final String METHOD_RERUN_ANDROID_SCAN 											= "reRunAndroidScan";
	public static final String METHOD_DELETE_ANDROID_SCAN 											= "deleteAndroidScan";
	public static final String METHOD_CONVERT_ENTRY 												= "convertEntry";
	public static final String METHOD_PARSE_ENTRY 													= "parseEntry";
	public static final String METHOD_GET_VULNERABILITY_NAME 										= "getVulnerabilityName";
	public static final String METHOD_GET_SEVERITY 													= "getSeverity";
	public static final String METHOD_CREATE_ANDROID_PROJECT 										= "createAndroidProject";
	public static final String METHOD_UPDATE_ANDROID_PROJECT_NAME 									= "updateAndroidProjectName";
	public static final String METHOD_DELETE_ANDROID_PROJECT 										= "deleteAndroidProject";
	public static final String METHOD_GET_EXCLUDED_PACKAGE_NAMES 									= "getExcludedPackageNames";
	public static final String METHOD_GET_DB_CONFIG 												= "getDBConfig";
	public static final String METHOD_GET_CONFIG 													= "getConfig";
	public static final String METHOD_PARSE_ANDROID_APK 											= "parseAndroidApk";
	public static final String METHOD_PARSE_ANDROID_CHECKLIST 										= "parseAndroidChecklist";
	public static final String METHOD_GET_FILE_SIZE 												= "getFileSize";
	public static final String METHOD_CREATE_UPLOAD_DIRECTORY 										= "createUploadDirectory";
	public static final String METHOD_CREATE_DOWNLOAD_REPORT_PATH 									= "createDownloadReportPath";
	public static final String METHOD_CREATE_DOWNLOAD_REFERENCE_PATH 								= "createDownloadReferencePath";
	public static final String METHOD_CREATE_CRAWL_SETTING_YAML_PATH 								= "createCrawlSettingYAMLPath";
	public static final String METHOD_GET_REPORT_FULL_NAME 											= "getReportFullName";
	public static final String METHOD_GET_REPORT_FILE_NAME 											= "getReportFileName";
	public static final String METHOD_GET_REPORT_ZIP_NAME 											= "getReportZipName";
	public static final String METHOD_UPLOAD_SOURCE_FILE											= "uploadSourceFile";
	public static final String METHOD_LOCATE_PROJECT_DIRECTORY 										= "locateProjectDirectory";
	public static final String METHOD_LOCATE_SCAN_DIRECTORY 										= "locateScanDirectory";
	public static final String METHOD_GET_ZIPPED_REPORTS 											= "getZippedReports";
	public static final String METHOD_GET_ANALYZED_ZIPPPED_REPORT 									= "getAnalyzedZippedReport";
	public static final String METHOD_DELETE_REPORT_FILES_BY_SCAN_ID 								= "deleteReportFilesByScanId";
	public static final String METHOD_DELETE_REPORT_FILES_BY_PROJECT_ID 							= "deleteReportFilesByProjectId";
	public static final String METHOD_GET_VULN_FILE_PATH 											= "getVulnFilePath";
	public static final String METHOD_GET_RESOLVER_URL 												= "getResolverUrl";
	public static final String METHOD_LOGIN 														= "login";
	public static final String METHOD_GET_WEB_SERVICE_URL 											= "getWebServiceUrl";
	public static final String METHOD_DELETE_CX_PROJECT 											= "deleteCxProject";
	public static final String METHOD_UPDATE_CX_PROJECT_NAME 										= "updateCxProjectName";
	public static final String METHOD_REGISTER_CX_SCAN 												= "registerCxScan";
	public static final String METHOD_SCAN_CX 														= "scanCx";
	public static final String METHOD_CANCEL_CX_SCAN 												= "cancelCxScan";
	public static final String METHOD_RERUN_CX_SCAN 												= "reRunCxScan";
	public static final String METHOD_DELETE_CX_SCAN 												= "deleteCxScan";
	public static final String METHOD_GET_REFERENCE_INFO_REPORT 									= "getReferenceInfoReport";
	public static final String METHOD_REGENERATE_CX_REPORT 											= "reGenerateCxReport";
	public static final String METHOD_YCLOUD_UTIL 													= "YCloudUtil";
	public static final String METHOD_GET_INSTANCE 													= "getInstance";
	public static final String METHOD_DOWNLOAD_OBJECT 												= "downloadObject";
	public static final String METHOD_DELETE_FILE													= "deleteFile";
	public static final String METHOD_DOWNLOAD_VULN_LIST 											= "downloadVulnList";
	public static final String METHOD_ENCRYPT 														= "encrypt";
	public static final String METHOD_DECRYPT 														= "decrypt";
	public static final String METHOD_GENERATE_EMAIL 												= "generateEmail";
	public static final String METHOD_GET_RECIPIENTS_BY_ROLE 										= "getRecipientsByRole";
	public static final String METHOD_GET_USER_BY_EMAIL_ADDRESS 									= "getUserByEmailAddress";
	public static final String METHOD_VALIDATE_RECIPIENTS 											= "validateRecipients";
	public static final String METHOD_GET_COMPANY_ID 												= "getCompanyId";
	public static final String METHOD_GET_PROJECTS_COUNT 											= "getProjectsCount";
	public static final String METHOD_GET_SEARCHED_PROJECTS_COUNT 									= "getSearchedProjectsCount";
	public static final String METHOD_GET_ENTIRE_SCANS_COUNT 										= "getEntireScansCount";
	public static final String METHOD_GET_SCANS_COUNT 												= "getScansCount";
	public static final String METHOD_CREATE_TEMP_MAIL_FILE 										= "createTempMailFile";
	public static final String METHOD_CREATE_SCAN 													= "createScan";
	public static final String METHOD_GET_BYTES 													= "getBytes";
	public static final String METHOD_GET_CX_SDK_WEB_SERVICE_STUB 									= "getCxSDKWebServiceStub";
	public static final String METHOD_OPEN_PROJECT 													= "openProject";
	public static final String METHOD_GET_MANUAL_FILE_PATH 											= "getManualFilePath";
	public static final String METHOD_ADD_RECIPIENT 												= "addRecipient";
	public static final String METHOD_ADD_VEX_SCAN_SETTING 											= "addVexScanSetting";
	public static final String METHOD_RECRAWL 														= "reCrawl";
	public static final String METHOD_ABORT_CRAWL 													= "abortCrawl";
	public static final String METHOD_INTERRUPTED_CRAWL 											= "interruptedCrawl";
	public static final String METHOD_CANCEL_CRAWL 													= "cancelCrawl";
	public static final String METHOD_RESTART_CRAWL 												= "restartCrawl";
	public static final String METHOD_COPY_CRAWL_SETTING 											= "copyCrawlSetting";
	public static final String METHOD_INTERRUPTED_SCAN 												= "interruptedScan";
	public static final String METHOD_CANCEL_SCAN 													= "cancelScan";
	public static final String METHOD_RESTART_SCAN 													= "restartSCan";
	public static final String METHOD_COPY_CRAWL_RESULT_AND_SCAN 									= "copyCrawlAndScan";
	public static final String METHOD_GET_TARGET_INFORMATIONS 										= "getTargetInformations";
	public static final String METHOD_GET_LOGIN_SETTINGS 											= "getLoginSettings";
	public static final String METHOD_GET_VEX_DETECTION_RESULT_REVIEW 								= "getVexDetectionResultReview";
	public static final String METHOD_ADD_VEX_SCAN_ADVANCED_SETTING 								= "addVexScanAdvancedSetting";
	public static final String METHOD_ADD_VEX_SCAN_TARGET_INFORMATION 								= "addVexScanTargetInformation";
	public static final String METHOD_UPLOAD_SCAN													= "uploadScan";
	public static final String METHOD_IMPORT_SCAN 													= "importScan";
	public static final String METHOD_ADD_VEX_LOGIN_SETTINGS										= "addUpdateLoginSettingList";
	public static final String METHOD_GET_CRAWL_LOGIN_INFO										    = "getCrawlLoginInfo";
	public static final String METHOD_UPLOAD_CERTIFICATE_FILE										= "uploadCertificateFile";
	public static final String METHOD_RESEND_DETECTION_RESULT										= "resendDetectionResult";
	public static final String METHOD_CONFIRM_DETECTION_RESULT										= "confirmDetectionResult";
	public static final String METHOD_GENERATEVEX__DETECTION_RESULT_SQL_QUERY                       = "generateVexDetectionResultSQLQuery";
	public static final String METHOD_QPOS_HELPER                                                   = "qPosHelper";
	public static final String METHOD_GET_VEX_DETECTION_RESULT_COUNT                                = "getDetectionResultsCount";
	public static final String METHOD_GET_VEX_DETECTION_RESULTS                               		= "getDetectionResults";
	public static final String METHOD_GET_VEX_DETECTION_SORT_RESULTS                                = "sortDetectionResults";
	public static final String METHOD_VEX_GET_BASE_PATH												= "getVexBasePath";
	public static final String METHOD_VEX_GET_VEX_USERS												= "getVexUsers";
	public static final String METHOD_SET_LOGIN_CREDENTIALS											= "setLoginCredentials";
	public static final String METHOD_UPDATE_SCAN_PATROL_SETTINGS									= "updateScanPatrolSettings";
	public static final String METHOD_OPTIMIZATION 													= "optimization";
	
	/* ------------------------- CONFIG KEY ------------------------- */
	public static final String CONFIG_KEY_CX_PROCESS_UT_NAME			 = "cxsuite.processUT.name";
	public static final String CONFIG_KEY_CX_PROCESS_UT_DESC			 = "cxsuite.processUT.description";
	public static final String CONFIG_KEY_CX_PROCESS_ST_NAME			 = "cxsuite.processST.name";
	public static final String CONFIG_KEY_CX_PROCESS_ST_DESC			 = "cxsuite.processST.description";
	public static final String CONFIG_KEY_CX_PROCESS_PD_NAME			 = "cxsuite.processPD.name";
	public static final String CONFIG_KEY_CX_PROCESS_PD_DESC			 = "cxsuite.processPD.description";

	public static final String CONFIG_KEY_ANNOUNCEMENT					 = "announcement.content";

	public static final String CONFIG_KEY_EMAIL_SENDER_EMAIL_ADDRESS 	 = "email.sender.email.address";
	public static final String CONFIG_KEY_EMAIL_SENDER_PASSWORD 		 = "email.sender.password";
	public static final String CONFIG_KEY_EMAIL_RETURN_PATH 			 = "email.return.path";

	public static final String CONFIG_KEY_EMAIL_PASSWORD_CHANGE_SUBJECT  = "email.password.change.subject";
	public static final String CONFIG_KEY_EMAIL_PASSWORD_CHANGE_CONTENT  = "email.password.change.content";

	public static final String CONFIG_KEY_EMAIL_PROJECT_CREATE_SUBJECT 	 = "email.project.create.subject";
	public static final String CONFIG_KEY_EMAIL_PROJECT_CREATE_CONTENT 	 = "email.project.create.content";

	public static final String CONFIG_KEY_EMAIL_PROJECT_CHANGE_SUBJECT 	 = "email.project.change.subject";
	public static final String CONFIG_KEY_EMAIL_PROJECT_CHANGE_CONTENT 	 = "email.project.change.content";

	public static final String CONFIG_KEY_EMAIL_ACCOUNT_CREATE_SUBJECT   = "email.account.create.subject";
	public static final String CONFIG_KEY_EMAIL_ACCOUNT_CREATE_CONTENT 	 = "email.account.create.content";

	public static final String CONFIG_KEY_EMAIL_ACCOUNT_CHANGE_SUBJECT 	 = "email.account.change.subject";
	public static final String CONFIG_KEY_EMAIL_ACCOUNT_CHANGE_CONTENT 	 = "email.account.change.content";

	public static final String CONFIG_KEY_EMAIL_REQUEST_REVIEW_SUBJECT 	 = "email.request.review.subject";
	public static final String CONFIG_KEY_EMAIL_REQUEST_REVIEW_CONTENT 	 = "email.request.review.content";

	public static final String CONFIG_KEY_EMAIL_REVIEW_DONE_SUBJECT 	 = "email.review.done.subject";
	public static final String CONFIG_KEY_EMAIL_REVIEW_DONE_CONTENT 	 = "email.review.done.content";

	public static final String CONFIG_KEY_EMAIL_SCAN_DONE_SUBJECT 		 = "email.scan.done.subject";
	public static final String CONFIG_KEY_EMAIL_SCAN_DONE_CONTENT 		 = "email.scan.done.content";

	public static final String CONFIG_KEY_EMAIL_SCAN_FAILED_SUBJECT 	 = "email.scan.failed.subject";
	public static final String CONFIG_KEY_EMAIL_SCAN_FAILED_CONTENT 	 = "email.scan.failed.content";

	public static final String CONFIG_KEY_EMAIL_SCAN_CANCELLED_SUBJECT 	 = "email.scan.cancelled.subject";
	public static final String CONFIG_KEY_EMAIL_SCAN_CANCELLED_CONTENT 	 = "email.scan.cancelled.content";
	
	public static final String CONFIG_KEY_VEX_REQUEST_HEADERS            = "vex.scan.request.headers";

	public static final String CONFIG_KEY_CX_UPLOAD_PATH				 = "cx.upload.base.path";
	public static final String CONFIG_KEY_CX_DOWNLOAD_PATH				 = "cx.download.base.path";
	public static final String CONFIG_KEY_ANDROID_UPLOAD_PATH			 = "android.upload.base.path";
	public static final String CONFIG_KEY_ANDROID_DOWNLOAD_PATH			 = "android.download.base.path";
	public static final String CONFIG_KEY_IOS_UPLOAD_PATH				 = "ios.upload.base.path";
	public static final String CONFIG_KEY_IOS_DOWNLOAD_PATH				 = "ios.download.base.path";
	public static final String CONFIG_KEY_VEX_UPLOAD_PATH				 = "vex.upload.base.path";
	public static final String CONFIG_KEY_VEX_DOWNLOAD_PATH				 = "vex.download.base.path";
	public static final String CONFIG_KEY_RESOURCE_PATH				 	 = "resource.base.path";

	/* ------------------------- PARAMETER NAMES ------------------------- */
	public static final String PARAM_USER_ACTION					 = "userAction";
	public static final String PARAM_ANDROID_SERVICE_USE_AUTH		 = "androidServiceUseAuth";
	public static final String PARAM_PROJECT					 	 = "project";
	public static final String PARAM_ORGANIZATION_LIST				 = "organizationList";
	public static final String PARAM_PRESET_LIST					 = "presetList";
	public static final String PARAM_ORG_USERS_LIST					 = "orgUsersList";
	public static final String PARAM_PROJECT_USERS_LIST				 = "projectUsersList";
	public static final String PARAM_PACKAGE_NAMES_LIST				 = "packageNamesList";
	public static final String PARAM_CASE_NUMBER					 = "caseNumber";
	public static final String PARAM_PROJECT_NAME					 = "projectName";
	public static final String PARAM_PRESET_VALUE					 = "presetValue";
	public static final String PARAM_OWNER_GROUP					 = "ownerGroup";
	public static final String PARAM_LIBRARY_SETTING				 = "librarySetting";
	public static final String PARAM_SELECTED_USERS					 = "selectedUsers";
	public static final String PARAM_PROJECT_END_DATE				 = "projectEndDate";
	public static final String PARAM_SCREEN_NO					 	 = "screenNo";
	public static final String PARAM_PROJECT_ID					 	 = "projectId";
	public static final String PARAM_NEW_FILE					  	 = "newFile";
	public static final String PARAM_FILE_NAME					 	 = "fileName";
	public static final String PARAM_STATUS					 		 = "status";
	public static final String PARAM_SCAN_NAME								 = "scanName";
	public static final String PARAM_IMPLEMENTATION_ENVIRONMENT				 = "implementationEnvironment";
	public static final String PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP		 = "webInspectionSignatureSetGroup";
	public static final String PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST		 = "webInspectionSignatureSetList";
	public static final String PARAM_SERVER_FILES							 = "serverFiles";
	public static final String PARAM_SERVER_SETTINGS						 = "serverSettings";
	public static final String PARAM_SERVER_FILES_SIGNATURE_SET_GROUP		 = "serverFilesSignatureSetGroup";
	public static final String PARAM_SERVER_FILES_SIGNATURE_SET_LIST		 = "serverFilesSignatureSetList";
	public static final String PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP	 = "serverSettingsSignatureSetGroup";
	public static final String PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST		 = "serverSettingsSignatureSetList";
	public static final String PARAM_START_TIME								 = "startTime";
	public static final String PARAM_END_TIME 								= "endTime";
	public static final String PARAM_EMAIL_NOTIFICATION 					= "emailNotification";
	public static final String PARAM_START_URL 								= "startURL";
	public static final String PARAM_AUTHORIZED_PATROL_URL 					= "authorizedPatrolURL";
	public static final String PARAM_PARAMETER_NAME_ID 						= "parameterNameId";
	public static final String PARAM_PARAMETER_VALUE_ID 					= "parameterValueId";
	public static final String PARAM_PARAMETER_NAME_PASS 					= "parameterNamePass";
	public static final String PARAM_PARAMETER_VALUE_PASS 					= "parameterValuePass";
	public static final String PARAM_LOGIN_URL 								= "loginURL";
	public static final String PARAM_PROJECT_LIST 							= "projectList";
	public static final String PARAM_IS_FROM_SEARCH 						= "isFromSearch";
	public static final String PARAM_REG_DATE_LOW 							= "regDateLow";
	public static final String PARAM_REG_DATE_HIGH 							= "regDateHigh";
	public static final String PARAM_SCAN_LIST 								= "scanList";
	public static final String PARAM_SELECTED_GROUP 						= "selectedGroup";
	// public static final String PARAM_USER_ID_TOO_LONG = "userIdTooLong";
	// public static final String PARAM_USER_NAME_TOO_LONG = "userNameTooLong";
	public static final String PARAM_USER_DOES_NOT_EXIST 					= "userDoesNotExist";
	public static final String PARAM_USER_DOES_NOT_BELONG_TO_GROUP 			= "userDoesNotBelongToGroup";
	public static final String PARAM_SCAN_ID 								= "scanId";
	public static final String PARAM_CX_SERVICE_USE_AUTH 					= "cxServiceUseAuth";
	public static final String PARAM_ATTRIBUTE 								= "attribute";
	public static final String PARAM_SCAN 									= "scan";
	public static final String PARAM_DETECTION_RESULTS						= "detectionResults";
	public static final String PARAM_PROCESS_MAP 							= "processMap";
	public static final String PARAM_PROCESS 								= "process";
	public static final String PARAM_START 									= "start";
	public static final String PARAM_EXCLUSION_RULE_FILE 					= "exclusionRuleFile";
	public static final String PARAM_INSPECTION_FILE 						= "inspectionFile";
	public static final String PARAM_CRAWLING_ONLY							= "setCrawlingOnly";
	// public static final String PARAM_TYPE = "type";
	public static final String PARAM_REMOVED_USERS 							= "usersToRemove";
	public static final String PARAM_FIELD_NUMBER 							= "fieldNumber";
	public static final String PARAM_ANDROID_SERVER_ERROR_MSG 				= "androidServerErrorMsg";
	public static final String PARAM_CX_SERVER_ERROR_MSG 					= "cxServerErrorMsg";
	public static final String PARAM_VIEW_PROJECT_LIST 						= "viewProjectList";
	public static final String PARAM_DO_VIEW_PROJECT_LIST 					= "doViewProjectList";
	public static final String PARAM_ANNOUNCEMENT 							= "announcement";
	public static final String PARAM_IS_MAXIMIZED 							= "isMaximized";
	public static final String PARAM_MAXIMIZED_URL 							= "maximizedUrl";
	public static final String PARAM_IS_VIEW_ENTIRE_SCAN_LIST 				= "isViewEntireScanList";
	public static final String PARAM_IS_FROM_STOP_SCAN 						= "isFromStopScan";
	public static final String PARAM_IS_FROM_REEXECUTE_SCAN 				= "isFromReexecuteScan";
	public static final String PARAM_IS_FROM_DELETE_SCAN					= "isFromDeleteScan";
	public static final String PARAM_IS_FROM_REQUEST_REVIEW 				= "isFromRequestReview";
	public static final String PARAM_IS_FROM_CANCEL_REVIEW					= "isFromCancelReview";
	public static final String PARAM_IS_FROM_REGENERATE_REPORT 				= "isFromRegenerateReport";
	public static final String PARAM_IS_FROM_DELETE_USER					= "isFromDeleteUser";
	public static final String PARAM_HAS_FILE_UPLOAD 						= "hasFileUpload";
	public static final String PARAM_CX_API_CALL_ERROR_MSG 					= "cxAPICallErrorMsg";
	public static final String PARAM_NO_OF_SCANS 							= "scanCount";
	public static final String PARAM_PROJECT_END_DATE_LOW 					= "projectEndDateLow";
	public static final String PARAM_PROJECT_END_DATE_HIGH 					= "projectEndDateHigh";
	public static final String PARAM_HASH_VALUE								= "hashValue";
	public static final String PARAM_SCAN_MANAGER 							= "scanManager";
	public static final String PARAM_CX_SCAN_ID 							= "cxSuiteScanId";
	public static final String PARAM_ORDER_BY_TYPE 							= "orderByType";
	public static final String PARAM_ORDER_BY_COL 							= "orderByCol";
	public static final String PARAM_IOS_SERVICE_USE_AUTH 					= "iOSServiceUseAuth";
	public static final String PARAM_CUR 									= "cur";
	public static final String PARAM_USERNAME 								= "userName";
	public static final String PARAM_VIEW_SCAN_CHANGE 						= "viewScanChange";
	public static final String PARAM_VEX_SERVICE_USE_AUTH 					= "vexServiceUseAuth";
	public static final String PARAM_TARGET_INFORMATION_LIST_SIZE			= "listTableSize";
	public static final String PARAM_PROTOCOL_GROUP			 				= "protocolGroup";
	public static final String PARAM_HOST			 						= "host";
	public static final String PARAM_PORT			 						= "port";
	public static final String PARAM_SCAN_SETTING 							= "scanSetting";
	public static final String PARAM_TARGET_INFORMATION_LIST			 	= "targetInformationList";
	public static final String PARAM_LOGIN_SETTING_LIST						= "loginSettingList";
	public static final String PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT 		= "implementationenvironment";
	public static final String PARAM_SCAN_START_URL 						= "startURL";
	public static final String PARAM_SCAN_START_TIME 						= "startTime";
	public static final String PARAM_SCAN_END_TIME			 				= "endTime";
	public static final String PARAM_CASE_NAME 								= "caseName";
	public static final String PARAM_TARGET_URL 							= "targetUrl";
	public static final String PARAM_PRODUCTION_ENVIRONMENT_URL 			= "productionEnvironmentUrl";
	public static final String PARAM_TARGET_URL_PROTOCOL_GROUP			 	= "targetURLprotocolGroup";
	public static final String PARAM_TARGET_URL_PROTOCOL					= "targetURLprotocol";
	public static final String PARAM_TARGET_URL_HOST						= "targetURLhost";
	public static final String PARAM_TARGET_URL_PORT						= "targetURLport";
	public static final String PARAM_PRODUCTION_ENVIRONMENT_URL_PROTOCOL_GROUP	= "productionEnvUrlprotocolGroup";
	public static final String PARAM_PRODUCTION_ENVIRONMENT_URL_PROTOCOL		= "productionEnvUrlprotocol";
	public static final String PARAM_PRODUCTION_ENVIRONMENT_URL_HOST			= "productionEnvUrlhost";
	public static final String PARAM_PRODUCTION_ENVIRONMENT_URL_PORT			= "productionEnvUrlport";
	public static final String PARAM_TARGET_URL_LIST_SIZE						= "targetURLlistTableSize";
	public static final String PARAM_PRODUCTION_ENVIRONMENT_URL_LIST_SIZE		= "productionEnvUrllistTableSize";
	public static final String PARAM_SCAN_START_HOUR 						= "startHourTime";
	public static final String PARAM_SCAN_END_HOUR 							= "endHourTime";
	
	public static final String PARAM_SCAN_AUTHORIZED_PATROL_URL 			="authorizedPatrolUrl";
	public static final String PARAM_SCAN_SET_EMAIL_NOTIFICATION	 		="setEmailNotification";
	public static final String PARAM_SCAN_LOGIN_SETTING_URL					="loginUrl";
	public static final String PARAM_SCAN_lOGIN_PROTOCOL					="protocol";
	public static final String PARAM_SCAN_lOGIN_HOST						="host";
	public static final String PARAM_SCAN_lOGIN_PORT						="port";
	
	public static final String PARAM_DETECTION_RESULT_ID 					= "detectionresultid";
	public static final String PARAM_DETECTION_SCAN_ID 						= "scanid";
	public static final String PARAM_DETECTION_RISK_LEVEL 					= "risklevel";
	public static final String PARAM_DETECTION_FUNCTION_NAME				= "functionname";
	public static final String PARAM_DETECTION_CATEGORY 					= "category";
	public static final String PARAM_DETECTION_OVERVIEW 					= "overview";
	public static final String PARAM_DETECTION_URL 							= "url";
	public static final String PARAM_DETECTION_PARAMETER_NAME 				= "parametername";
	public static final String PARAM_DETECTION_JUDGEMENT 					= "detectionjudgment";
	public static final String PARAM_DETECTION_REVIEW_COMMENT 				= "reviewcomment";
	public static final String PARAM_DETECTION_IS_RESEND_IN_VEX 			= "isresendinvex";
	public static final String PARAM_DETECTION_NUMBER                       = "number";
	public static final String PARAM_DETECTION_RESULT_LIST                  = "detectionResultList";
	public static final String PARAM_DETECTION_RESULT                       = "searchedDetectionResultReview";
	public static final String PARAM_DETECTION_IS_DETECTED					= "isDetected";
	public static final String PARAM_DETECTION_IS_CONFIRMED					= "isConfirmed";
	public static final String PARAM_DETECTION_CONFIRM_NUMBER               = "confirmNumber";
	public static final String PARAM_DETECTION_CONFIRM_RESULT_ID 			= "confirmDetectionresultid";
	
	public static final String PARAM_TARGET_INFO_PROTOCOL					= "protocol";
	public static final String PARAM_TARGET_INFO_HOST						= "host";
	public static final String PARAM_TARGET_INFO_PORT						= "port";
	public static final String PARAM_TARGET_INFO_HTTP_VERSION				= "httpversion";
	public static final String PARAM_TARGET_INFO_SET_KEEP_ALIVE_CONNECTION	= "setkeepaliveconnection";
	public static final String PARAM_TARGET_INFO_SET_RESPONSE_CONTENT_LENGTH = "setresponsecontentlength";
	public static final String PARAM_TARGET_INFO_USE_ACCEPTED_ENCODING_HEADER = "useacceptencodingheader";
	public static final String PARAM_TARGET_INFO_UNZIP_RESPONSE 			= "unzipresponse";
	public static final String PARAM_TARGET_INFO_HTTP_PROTOCOL 				= "httpprotocol";
	public static final String PARAM_TARGET_INFO_EXTERNAL_PROXY_HOST	 	= "externalproxyhost";
	public static final String PARAM_TARGET_INFO_EXTERNAL_PROXY_PORT 		= "externalproxyport";
	public static final String PARAM_TARGET_INFO_EXTERNAL_PROXY_AUTH_ID 	= "externalproxyauthid";
	public static final String PARAM_TARGET_INFO_EXTERNAL_PROXY_AUTH_PASSWORD = "externalproxyauthpassword";
	public static final String PARAM_TARGET_INFO_USE_CLIENT_CERTIFICATE 	= "useclientcertificate";
	public static final String PARAM_TARGET_INFO_CERTIFICATE_FILE			= "certificatefile";
	public static final String PARAM_TARGET_INFO_CERTIFICATE_FILE_PASSWORD	= "certificatefilepassword";
	public static final String PARAM_TARGET_INFO_NTLM_AUTH_ID				= "ntlmauthid";
	public static final String PARAM_TARGET_INFO_NTLM_AUTH_PASSWORD			= "ntlmauthpassword";
	public static final String PARAM_TARGET_INFO_NTLM_AUTH_DOMAIN			= "ntlmauthdomain";
	public static final String PARAM_TARGET_INFO_NTLM_AUTH_HOST				= "ntlmauthhost";
	public static final String PARAM_TARGET_INFO_DIGEST_AUTH_ID				= "digestauthid";
	public static final String PARAM_TARGET_INFO_DIGEST_AUTH_PASSWORD		= "digestauthpassword";
	public static final String PARAM_TARGET_INFO_BASIC_AUTH_ID				= "basicauthid";
	public static final String PARAM_TARGET_INFO_BASIC_AUTH_PASSWORD		= "basicauthpassword";
	public static final String PARAM_TARGET_INFO_ACCESS_EXCLUSION_PATH		= "accessexclusionpath";
	public static final String PARAM_PROJECT_ADVANCED_SCAN_SETTING 			= "projectAdvancedScanSetting";
	public static final String PARAM_PROJECT_TARGET_INFORMATION 			= "projectTargetInformation";
	public static final String PARAM_PROJECT_TARGET_URLS 			        = "projectTargetURLs";
	public static final String PARAM_PROJECT_PRODUCTION_ENVIRONMENT_URLS 	= "projectProductionEnvironmentURLs";
	
	public static final String PARAM_IP_ADDRESS							= "ipaddress";
	public static final String PARAM_AUTOCRAWL_SETTING					= "autoCrawlingSetting";
	public static final String PARAM_UNAUTHORIZED_PATROL_URL	    	= "unauthorizedpatrolURL";      
	public static final String PARAM_LOGIN_STATUS_DETECTION				= "loginstatusdetection";
	public static final String PARAM_SET_NOT_SEND_PASSWORD				= "setnotsendpassword";
	public static final String PARAM_PARAMETER_NAME_AS_PASSWORD			= "parameternameaspassword";
	public static final String PARAM_MAX_NUM_DETECTION_LINK				= "maxnumdetectionlink";
	public static final String PARAM_MAX_NUM_DETECTION_LINK_PER_PAGE 	= "maxnumdetectionlinkperpage";	
	public static final String PARAM_DETECTION_LINK_DEPTH_LIMIT			= "detectionlinkdepthlimit";
	public static final String PARAM_WAIT_TIME							= "waittime";
	public static final String PARAM_NUM_OF_THREAD						= "numofthreadgroup";
	public static final String PARAM_TIME_OUT_TIME						= "timeouttime";
	public static final String PARAM_SET_AUTO_POST_FORM					= "setautopostform";
	public static final String PARAM_SET_POST_METHOD					= "setpostmethod";
	public static final String PARAM_REQUEST_HEADER						= "requestheader";
	public static final String PARAM_NUM_OF_RETRY_AT_ERROR				= "numofretryaterror";
	public static final String PARAM_SESSION_ERROR_DETECTION			= "sessionerrordetection";
	public static final String PARAM_SCREEN_TRANSITION_ERROR_DETECTION	= "screentransitionerrordetection";
	public static final String PARAM_EXCLUDE_PATH_INFO_REGEX			= "excludepathinforegex";
	public static final String PARAM_EXCLUDE_NAME_REGEX					= "excludeparameterdeletenameregex";
	public static final String PARAM_EXCLUDE_PARAM_REGEX				= "excludeparameterdeleteparamregex";
	public static final String PARAM_EXTRACT_URL_REGEX					= "extracturlregex";
	public static final String PARAM_NOT_PARSE_EXTENTION				= "notparseextension";
	public static final String PARAM_NOT_PARSE_FILENAME_REGEX			= "notparsefilenameregex";
	public static final String PARAM_GET_LOGIN_PARAMETERS	            = "loginparameters";
	public static final String PARAM_TARGET_INFORMATION_GROUP           = "targetInformationGroup";
	public static final String PARAM_LOGIN_PATH                         = "loginformpath";
	public static final String PARAM_LOGIN_PARAMETERS                   = "loginparameters";
	public static final String PARAM_LOGIN_PATH_LABEL                   = "loginformpathlabel";
	public static final String PARAM_SHOW_DISPLAY_FORM					= "showdisplayform";
	public static final String PARAM_SHOW_DISPLAY_FORM_PARAMS			= "showdisplayformparams";
	public static final String PARAM_SHOW_DISPLAY_MODAL					= "showdisplaymodal";
	public static final String PARAM_PARAMETER_NAME						= "parameterName";
	public static final String PARAM_PARAMETER_VALUE					= "parameterValue";
	public static final String PARAM_CHECK_PARAMETER 					= "checkItem";
	public static final String PARAM_LOGIN_INFO_LIST_SIZE			    = "loginInfoListSize";
	public static final String PARAM_LOGIN_INFO_NUM_OF_PARAMS 			= "totalNumOfParams";
	public static final String PARAM_LOGIN_INFO_INDEX 					= "rowIndex";
	//
	//
	//
	// /* ------------------------- ACTION ------------------------- */
	public static final String ACTION_VIEW_PROJECT_LIST 		= "viewProjectList";
	public static final String ACTION_VIEW_EDIT_PROJECT 		= "viewEditProject";
	public static final String ACTION_VIEW_ENTIRE_SCAN_LIST 	= "viewEntireScanList";
	public static final String ACTION_SEARCH_PROJECTS 			= "searchProjects";
	public static final String ACTION_VIEW_SCAN_LIST 			= "viewScanList";
	// public static final String ACTION_DELETE_PROJECT 		= "deleteProject";
	public static final String ACTION_ADD_PROJECT 				= "addProject";
	// public static final String ACTION_SEARCH_SCANS 			= "searchScans";
	// public static final String ACTION_STOP_SCAN 				= "stopScan";
	// public static final String ACTION_REEXECUTE_SCAN 		= "reexecuteScan";
	// public static final String ACTION_REQUEST_REVIEW 		= "requestReview";
	// public static final String ACTION_CANCEL_REVIEW 			= "cancelReview";
	// public static final String ACTION_REGENERATE_REPORT 		= "regenerateReport";
	// public static final String ACTION_DELETE_SCAN 			= "deleteScan";
	public static final String ACTION_VIEW_EDIT_SCAN 			= "viewEditScan";
	public static final String ACTION_UPDATE_PROJECT 			= "updateProject";
	// public static final String ACTION_COMPLETE_PROJECT 		= "completeProject";
	// public static final String ACTION_OPEN_PROJECT 			= "openProject";
	public static final String ACTION_ADD_SCAN 					= "addScan";
	public static final String ACTION_UPDATE_SCAN 				= "updateScan";
	public static final String ACTION_JAVAX_PORTLET 			= "javax.portlet.action";
	// public static final String ACTION_CLEAR_FILTER_PROJECTS 	= "clearFilterProjects";
	// public static final String ACTION_CLEAR_FILTER_SCANS 	= "clearFilterScans";
	public static final String ACTION_VIEW_SCAN_REGISTRATION = "viewScanRegistration";
	public static final String ACTION_VIEW_VEX_SCAN_REGISTRATION_SIMPLE_SETTING = "viewRegisterScanSimpleSetting";
	public static final String ACTION_VIEW_VEX_DETECTION_RESULT = "viewDetectionResult";
	//
	public static final String YCLOUD = "ycloud";
	public static final String ANALYZE_REPORT = "ANALYZEREPORT.zip";
	// public static final String CXPORTALWEBSERVICEURL =
	// "cxsuite.cxportalweburl";
	//
	public static final String DELIMITER_ESCAPED_DOT 			= "\\.";
	public static final String DELIMITER_COMMA 					= ",";
	public static final String DELIMITER_NEXT_LINE 				= "\n";
	public static final String DELIMITER_DOT 					= ".";
	public static final String DELIMITER_125 					= "_125_";
	//
	//
	// /* ------------------------- INPUT FIELD NUMBERS ------------------------- */
	public static final int FIELD_CASE_NUMBER					= 1;
	public static final int FIELD_PROJECT_NAME 					= 2;
	public static final int FIELD_OWNER_GROUP 					= 3;
	public static final int FIELD_PROJECT_END_DATE 				= 4;
	public static final int FIELD_ATTRIBUTE 					= 5;
	public static final int FIELD_PACKAGE_NAME 					= 6;
	public static final int FIELD_CHECKLIST 					= 7;
	// public static final int FIELD_USER_ID 					= 8;
	// public static final int FIELD_USER_NAME 					= 9;
	public static final int FIELD_PROJECT_USERS 				= 10;
	public static final int FIELD_SCAN_FILE 					= 11;
	public static final int FIELD_SCAN_PROCESS 					= 12;
	public static final int FIELD_PRESET 						= 13;
	//
	// public static final String SCHEDULER_NAME 				= "CompletedThread";
	// public static final String INTERRUPTED_SCHEDULER 		= "InterruptedThread";
	//
	public static final String PORTAL_ERROR_PRE_S 				= "S.";
	public static final String PORTAL_ERROR_PRE_U 				= "U.";
	public static final String ORM_EXCEPTION 					= "ORMException";

	public static final String STRING_ASTERISK 					= "*";
	public static final String STRING_BACK_SLASH 				= "\\";
	public static final String STRING_DOUBLE_QUOTE 				= "\"";
	public static final String STRING_COLON 					= ":";
	public static final String STRING_SLASH 					= "/";
	public static final String STRING_QUESTION_MARK 			= "?";
	public static final String STRING_LESS_THAN 				= "<";
	public static final String STRING_GREATER_THAN 				= ">";
	public static final String STRING_BLANK_SPACE 				= " ";
	public static final String STRING_PIPE 						= "|";
	public static final String STRING_SPACE_OPEN_PARENTHESIS	= " (";
	public static final String STRING_CLOSE_PARENTHESIS			= ")";
	public static final String STRING_AMPERSAND			        = "&";
	public static final String STRING_SINGLE_QUOTE 				= "\'";

	
	public static final String DB_DRIVER_CLASS_NAME 			= "driverClassName";
	public static final String DB_URL 							= "url";
	public static final String DB_USERNAME 						= "username";
	public static final String DB_PASSWORD 						= "password";

	
	public static final int STRING_BYTE_MAX_40					= 40;
	public static final int STRING_BYTE_MAX_75					= 75;
	public static final int STRING_BYTE_MAX_100					= 100;
	public static final int STRING_BYTE_MAX_200					= 200;
	public static final int STRING_BYTE_MIN_8					= 8;
	public static final int PAGINATION_DELTA					= 100;
	public static final int CX_MAX_LINE_OF_CODE					= 750000;


	public static final String FILE_DOWNLOAD_COOKIE						= "fileDownload=true;path=/";
	public static final String SET_COOKIE 								= "Set-Cookie";

	/* ------------------------- CHANGE PASSWORD ------------------------- */
	public static final String CHANGE_P = "changeP";
	public static final String STRPNAME = "password0";
	public static final String STRPNAME1 = "password1";
	public static final String STRPNAME2 = "password2";
	
	public static final String CATALINA_BASE							= "catalina.base";
	public static final String VULNLISTPDF 								= "vulnlist.pdf";
	public static final String VULNLIST_IOS_PDF							= "vulnlistios.pdf";
	public static final String VULNLIST_VEX_PDF							= "vulnlistvex.pdf";
	public static final String WEBAPPS_FOLDER 							= "webapps";
	public static final String UBSECURE_FOLDER 							= "ubsecure-portlet";
	public static final String DOC_FOLDER 								= "doc";
	public static final String PRESET_PREFIX 							= "cxsuite.list.presetID.";
	
	public static final String STRING_WAITING							= "(%d 件待ち)";
	public static final String STRING_REVIEW_DONE						= "(精査済)";
	
	public static final String STOPPED_SCAN_MESSAGE						= "スキャンが中止されました。";
	public static final String FAILED_INFO_ICON_PATH					= "/o/ubsecure-theme/images/common/failed-info.png";
	
	public static final String USER_AGENT_MSIE 							= "MSIE";
	public static final String USER_AGENT_FIREFOX 						= "Firefox";
	public static final String USER_AGENT_CHROME 						= "Chrome";

	public static final double MAX_SIZE_CHECKLIST						= 20;
	public static final double MAX_SIZE_APK 							= 300;
	public static final double MAX_SIZE_ZIP 							= 300; //TODO Redo to 300;
	public static final double MAX_SIZE_IOS								= 300; 
	public static final double MAX_SIZE_VEX								= 100;
	public static final double MAX_SIZE_EXP								= 30000;
	
	public static final String EMAIL_RECIPIENTS 						= "recipients";
	public static final String EMAIL_SUBJECT 							= "subject";
	public static final String EMAIL_CONTENT 							= "content";

	public static final int EMAIL_EVENT_PASSWORD_CHANGE					= 1;
	public static final int EMAIL_EVENT_ADD_ACCOUNT						= 2;
	public static final int EMAIL_EVENT_UPDATE_ACCOUNT					= 3;
	public static final int EMAIL_EVENT_ADD_PROJECT						= 4;
	public static final int EMAIL_EVENT_UPDATE_PROJECT					= 5;
	public static final int EMAIL_EVENT_REQUEST_REVIEW					= 6;
	public static final int EMAIL_EVENT_REVIEW_DONE						= 7;
	public static final int EMAIL_EVENT_SCAN_DONE						= 8;
	public static final int EMAIL_EVENT_SCAN_FAILED						= 9;
	public static final int EMAIL_EVENT_SCAN_CANCELLED					= 10;
	
	public static final String MAIL_CONTENT_USERNAME_PLACEHOLDER 			= "{0}";
	public static final String MAIL_CONTENT_PROJECT_ID_PLACEHOLDER			= "{1}";
	public static final String MAIL_CONTENT_SCAN_ID_PLACEHOLDER				= "{2}";
	public static final String MAIL_CONTENT_ACCOUNT_NAME_PLACEHOLDER		= "{3}";
	public static final String MAIL_CONTENT_GROUP_NAME_PLACEHOLDER			= "{4}";
	public static final String MAIL_CONTENT_PROJECT_NAME_PLACEHOLDER		= "{5}";
	public static final String MAIL_CONTENT_ERROR_MESSAGE_PLACEHOLDER		= "{6}";
	public static final String MAIL_CONTENT_SCAN_EXECUTOR_NAME_PLACEHOLDER	= "{7}";
	
	public static final String DELETED_FLAG								= "1";
	
	public static final String COMMAND_BIN_BASH 						= "/bin/bash";
	public static final String COMMAND_MINUS_C 							= "-c";
	public static final String COMMAND_SENDMAIL_T_I_F					= "sendmail -t -i -f";
	public static final String COMMAND_REMOVE 							= "rm ";
	
	public static final String TEMP_MAIL_FILE 							= "tempMail";
	
	public static final String MAIL_HEADER_FROM 						= "From:";
	public static final String MAIL_HEADER_TO	 						= "To:";
	public static final String MAIL_HEADER_SUBJECT 						= "Subject:";
	public static final String MAIL_HEADER_CONTENT_TYPE 				= "Content-Type:text/plain;charset=UTF-8";
	public static final String MAIL_HEADER_CONTENT_TRANSFER_ENCODING	= "Content-Transfer-Encoding:8bit";
	
	public static final String FORMAT_SPECIFIER_STRING					= "%s";
	public static final String STRING_CARRIAGE_RETURN					= "\r\n";
	public static final String TEXT_FILE_EXTENSION 						= ".txt";
	
	public static final int MAX_BYTE_SIZE_PER_CHUNK						= 1048576;
	public static final int MIN_BYTE_SIZE_PER_CHUNK 					= 64;
	
	public static final String API_FUNCTION_LOGIN 										= "login()";
	public static final String API_FUNCTION_GET_PROJECT_CONFIGURATION 					= "getProjectConfiguration()";
	public static final String API_FUNCTION_UPDATE_PROJECT_INCREMENTAL_CONFIGURATION	= "updateProjectIncrementalConfiguration()";
	public static final String API_FUNCTION_GET_WEB_SERVICE_URL							= "getWebServiceUrl()";
	public static final String API_FUNCTION_CREATE_SCAN_REPORT 							= "createScanReport()";
	public static final String API_FUNCTION_CANCEL_SCAN									= "cancelScan()";
	public static final String API_FUNCTION_DELETE_SCANS 								= "deleteScans()";
	public static final String API_FUNCTION_DELETE_PROJECTS								= "deleteProjects()";
	public static final String API_FUNCTION_SCAN 										= "scan()";
	
	public static final String BEFORE 									= "BEFORE ";
	public static final String AFTER 									= "AFTER ";
	
	public static final String API_CALL_LOGIN							= "CXSUITE LOGIN";
	public static final String API_CALL_UPDATE_PROJECT 					= "CXSUITE UPDATE PROJECT";
	public static final String API_CALL_DELETE_SCAN						= "CXSUITE DELETE SCAN";
	public static final String API_CALL_REPORT_GENERATION 				= "CXSUITE REPORT GENERATION";
	public static final String API_CALL_CANCEL_SCAN						= "CXSUITE CANCEL SCAN";
	public static final String API_CALL_DELETE_PROJECT					= "CXSUITE DELETE PROJECT";
	public static final String API_CALL_SCAN							= "CXSUITE SCAN";
	public static final String API_CALL_PROJECT_CONFIG_RETRIEVAL		= "CXSUITE PROJECT CONFIGURATION RETRIEVAL";
	public static final String API_CALL_CX_CONNECTION_CHECKING			= "CXSUITE CONNECTION CHECKING";
	
	public static final String CX_CANNOT_RETRIEVE_SCAN_DETAILS_ERROR_MSG_EN		= "Cannot retrieve scan's details. The scan may have been deleted or changed";
	
	public static final String MAIL_ADDRESS 			= "mailAddress";
	public static final String MAIL_ADDRESS_REQUEST 	= "mail_address";
	
	public static final String INVALID_DATE				= "invalid date";

	public static final String STRING_NIHONGO_CLOSE_SQUARE_BRACE	= "」";
	public static final String STRING_TILDE							= " ~ ";
	public static final String STRING_NIHONGO_COMMA					= "、";
	public static final String STRING_NIHONGO_FILTERED_BY			= "で絞込み。";
	
	public static final String FILTER_PROJECT_BY_PROJECT_ID			= " プロジェクトID 「";
	public static final String FILTER_PROJECT_BY_GROUP_NAME			= " グループ名 「";
	public static final String FILTER_PROJECT_BY_CASE_NUMBER		= " 案件番号 「";
	public static final String FILTER_PROJECT_BY_PROJECT_NAME		= " プロジェクト名 「";
	public static final String FILTER_PROJECT_BY_PROJECT_END_DATE	= " プロジェクト終了予定日 「";
	public static final String FILTER_PROJECT_BY_STATUS				= " ステータス 「";
	public static final String FILTER_PROJECT_BY_SCAN_COUNT			= " スキャン数 「";
	
	public static final String FILTER_SCAN_BY_SCAN_ID				= " スキャンID 「";
	public static final String FILTER_SCAN_BY_PROJECT_NAME			= " プロジェクト名 「";
	public static final String FILTER_SCAN_BY_GROUP_NAME			= " グループ名「";
	public static final String FILTER_SCAN_BY_FILE_NAME				= " ファイル名 「";
	public static final String FILTER_SCAN_BY_HASH_VALUE			= " ハッシュ値 「";
	public static final String FILTER_SCAN_BY_SCAN_MANAGER			= " スキャン実行者 「";
	public static final String FILTER_SCAN_BY_SCAN_REGISTRATION_DATE= " スキャン登録日時 「";
	public static final String FILTER_SCAN_BY_STATUS				= " ステータス 「";
	public static final String FILTER_SCAN_BY_PROCESS				= " 工程 「";
	public static final String FILTER_SCAN_BY_CX_SCAN_ID			= " CxSuiteスキャンID 「";
	
	public static final String LAST_LOGIN_DATE_LOW 					= "lastLoginDateLow";
	public static final String LAST_LOGIN_DATE_HIGH 				= "lastLoginDateHigh";
	public static final String LAST_LOGIN_LOW						= "lastLoginLow";
	public static final String LAST_LOGIN_HIGH						= "lastLoginHigh";
	public static final String DATE_LAST_LOGIN_LOW					= "dteLastLoginDateLow";
	public static final String DATE_LAST_LOGIN_HIGH					= "dteLastLoginDateHigh";

	public static final String PARAM_NO_USERS						= "noOfUsers";
	
	public static final String CLEAR_SEARCH							= "clearSearch";
	
	public static final String CX_CANNOT_RETRIEVE_SCAN_DETAILS_ERROR_MSG_JP	= "スキャンの詳細を取得できませんでした。スキャンが削除または変更されている可能性があります。";
	
	public static final String SORT_ASCENDING						= "asc";
	public static final String PARAM_PREVIOUS_SCREEN				= "previousScreen";
	public static final String PARAM_CURRENT_SCREEN					= "currentScreen";
	
	public static final String SORT_DESCENDING						= "desc";
	public static final String PARAM_USER_LIST_SORT_ORDER 			= "userListSortOrder";
	public static final String PARAM_AVAILABLE_USERS_SORT_ORDER 	= "availableUsersSortOrder";
	
	public static final String PARAM_CX_DOES_NOT_EXIST_ERROR_MSG	= "cxDoesNotExistErrorMessage";
	public static final String PARAM_SCAN_ID_REGENERATED_REPORT		= "scanRegeneratedReport";
	
	public static final String CX_REGENERATE_REPORT_FAILED 			= "レポート再作成に失敗しました。 ソースコード診断依頼の処理中にエラーが発生しました。";

	public static final String STRING_CX_	 						= "cx_";

	public static final String PARAM_PERIOD_START_DATE				= "startDate";
	public static final String PARAM_PERIOD_END_DATE				= "endDate";
	public static final String SUMMARY_PREFIX 						= "summary-";
	public static final String REVIEW_DETECTION_PREFIX 				= "review-detection-result-";
	public static final String METHOD_GENERATE_CX_VULN_SUMMARY		= "generateCxVulnSummary";
	public static final String HEADER_SUMMARY_GROUP_NAME			= "グループ名";
	public static final String HEADER_SUMMARY_PROCESS 				= "UT/ST";
	public static final String HEADER_SUMMARY_PRESET				= "プリセット";
	public static final String HEADER_SUMMARY_DIAGNOSIS_DATE		= "診断日";
	public static final String HEADER_SUMMARY_COMPANY_NAME			= "会社名";
	public static final String METHOD_GENERATE_CX_META_SUMMARY		= "generateCxMetaSummary";
	public static final String METHOD_GET_CX_VULN_SUMMARY_DATA		= "getCxVulnSummaryData";
	public static final String METHOD_GET_CX_DETECTION_RESULT_SUMMARY_DATA		= "getDetectionResultSummaryData";
	public static final String TAG_ATTRIB_PROJECTNAME				= "ProjectName";
	public static final String TAG_ATTRIB_SCANID					= "ScanId";
	public static final String GET_CX_META_SUMMARY_DATA				= "getCxMetaSummaryData";
	public static final String SUMMARY_ZIP							= "SUMMARY_";
	public static final String VULN_SUMMARY							= "VULN_";
	public static final String META_SUMMARY							= "META_";
	public static final String METHOD_DOWNLOAD_SUMMARY				= "downloadSummary";
	public static final String UTF_8								= "UTF-8";
	public static final String STRING_FORMAT_2LEADING_ZERO_DOUBLE	= "%02d";
	public static final char CHAR_COMMA 							= ',';
	public static final String METHOD_META_CSV_READER				= "metaCsvReader";
	public static final String METHOD_VULN_CSV_READER 				= "vulnCsvReader";
	public static final String METHOD_GENERATE_ANDROID_VULN_SUMMARY = "generateAndroidVulnSummary";
	public static final String METHOD_GENERATE_ANDROID_META_SUMMARY = "generateAndroidMetaSummary";
	public static final String VEX_TAG_ATTRIB_AUDIT_ID				= "audit_id";
	public static final String VEX_TAG_ATTRIB_DEFINITION			= "definition";
	public static final String VEX_TAG_ATTRIB_FUNCTION_NAME			= "function_name";
	public static final String VEX_TAG_ATTRIB_URL					= "url";
	public static final String VEX_TAG_ATTRIB_CATEGORY				= "category";
	public static final String VEX_TAG_ATTRIB_SIGNATURE_ID			= "signature_id";
	public static final String VEX_TAG_ATTRIB_RISK_LEVEL			= "risk_level";
	public static final String VEX_TAG_ATTRIB_SUMMARY				= "summary";
	public static final String VEX_TAG_ATTRIB_TARGET_NAME			= "target_name";
	public static final String VEX_TAG_ATTRIB_TARGET				= "target";
	public static final String VEX_TAG_ATTRIB_HOST_LIST				= "host_list";
	public static final String VEX_TAG_ATTRIB_COMPANY_NAME			= "own_company_name";
	public static final String VEX_TAG_ATTRIB_PROJECT_NAME			= "target_system";
	public static final String VEX_TAG_ATTRIB_TARGET_ENV			= "target_env";
	
	public static final String PROJECTLISTCSV						= "PROJECT_LIST.csv";
	public static final String CONTENT_TYPE_CSV 					= "text/csv";
	public static final String HEADER_PROJECT_ID					= "ID";
	public static final String HEADER_PROJECT_GROUP_NAME 			= "グループ名";
	public static final String HEADER_PROJECT_END_DATE 				= "プロジェクト終了予定日";
	public static final String HEADER_PROJECT_STATUS				= "ステータス";
	public static final String HEADER_SCAN_COUNT					= "スキャン数";
	public static final String PARAM_ACTION_SEARCH_PROJECT			= "actionSearchProject";
	public static final String PARAM_HAS_ERROR						= "hasError";
	
	//VEX META HEADERS
	public static final String HEADER_REPORT_ID						= "Report ID";
	public static final String HEADER_IMPLEMENTATION_ENV			= "実施環境";
	public static final String HEADER_PROPOSAL_NUMBER				= "案件番号";
	public static final String HEADER_DIAGNOSIS_START_DATE			= "診断開始日";
	public static final String HEADER_DIAGNOSIS_END_DATE			= "診断開始日";
	public static final String HEADER_HIGH_RISK_NUM					= "危険度高の数";
	public static final String HEADER_MEDIUM_RISK_NUM				= "危険度中の数";
	public static final String HEADER_LOW_RISK_NUM					= "危険度低の数";
	public static final String HEADER_RISK_NOTES_COUNT				= "危険度備考の数";
	public static final String HEADER_RISK_INFO_COUNT				= "危険度情報の数";
	public static final String HEADER_SCAN_EXECUTION_TIME			= "スキャン実行時間";
	public static final String HEADER_WEB_SIGNATURE_NAME			= "使用したシグネチャセット名(Web)";
	public static final String HEADER_SERVER_SETTING_NAME			= "使用したシグネチャセット名(SeverSettings)";
	public static final String HEADER_SERVER_FILE_NAME				= "使用したシグネチャセット名(SeverFiles)";

	//VEX VULN HEADERS
	public static final String HEADER_DEGREE_OF_RISK				= "危険度";
	public static final String HEADER_VULN_CATEGORY					= "脆弱性カテゴリ";
	public static final String HEADER_DETECTION_URL					= "検出URL";
	public static final String HEADER_DETECTION_PARAM				= "検出パラメータ";
	public static final String HEADER_ORIGINAL_VALUE				= "元値";
	public static final String HEADER_CHANGE_VALUE					= "変更値";
	public static final String HEADER_DETECTION_TRIGGER				= "検出トリガ";
	public static final String HEADER_CORRESPONDANCE_SITUATION		= "対応状況";
	public static final String HEADER_PLANNED_RESPONSE_DATE			= "対応予定日";
	
	public static final String METHOD_GENERATE_PDF 					= "generatePDF";
	
	public static final String HEADER_REQUEST_REVIEW 				= "精査依頼";
	public static final String AFTER_REVIEW 						= "精査後";
	
	public static final String IOS_SIGNATURE_ID = "ios.default.presetID";

	public static final String VEX_SIGNATURE_ID = "vex.default.presetID";

	public static final String METHOD_GET_PRESET_LIST = "getPresetList";

	public static final String CX_LICENSE_IS_EXPIRED_JP 			= "ライセンス問題によりログインに失敗しました。Checkmarx管理者に連絡して下さい。";

	public static final String ERROR_LOG_MESSAGE 										= "Error Message";
	public static final String ERROR_LOG_MESSAGE_PARAM_EMPTY							= "%s is empty.";
	public static final String ERROR_LOG_MESSAGE_PARAM_TOO_LONG							= "%s is too long.";
	public static final String ERROR_LOG_MESSAGE_PARAM_INVALID		 					= "%s is invalid.";
	public static final String ERROR_LOG_MESSAGE_PARAM_SHORT 							= "%s is too short.";
	public static final String ERROR_LOG_MESSAGE_PARAM_PASSED							= "%s has already passed.";
	public static final String ERROR_LOG_MESSAGE_PASSWORD_SAME_OLD 						= "Password is the same as the old one.";
	public static final String ERROR_LOG_MESSAGE_PARAM_ALREADY_EXISTS 					= "%s already exists.";
	public static final String ERROR_LOG_MESSAGE_PARAM_ALREADY_UPLOADED					= "%s already uploaded.";
	public static final String ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW					= "%s high passed %s low.";
	public static final String ERROR_LOG_MESSAGE_PARAM_INCORRECT 						= "%s is incorrect.";
	public static final String ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST 					= "%s does not exist.";
	public static final String ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED						= "No %s is selected.";
	public static final String ERROR_LOG_MESSAGE_PARAM_FILE_TYPE_INVALID 				= "%s file type is invalid.";
	public static final String ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG 				= "%s file size is too big.";
	public static final String ERROR_LOG_MESSAGE_SOURCE_CODE_MAX_LINE_750000			= "%s file contains more than 750,000 lines of source code.";
	public static final String ERROR_API_MESSAGE_SOURCE_CODE_MAX_LINE_750000			= "Scan has failed due to falling outside of the defined engines scan ranges";
	public static final String ERROR_LOG_MESSAGE_USER_DOES_NOT_BELONG_TO_GROUP 			= "User does not belong to the specified group.";
	public static final String ERROR_LOG_MESSAGE_PROJECT_REGISTRATION_FAILED 			= "Failed to register the project.";
	public static final String ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION 					= "User has no access to the action.";
	public static final String ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED 				= "Failed to register the scan.";
	public static final String ERROR_LOG_MESSAGE_SCAN_CHANGE_FAILED 					= "Failed to update the scan.";
	public static final String ERROR_LOG_MESSAGE_PROJECT_CHANGE_FAILED 					= "Failed to update the project.";
	public static final String ERROR_LOG_MESSAGE_PARAM_CANNOT_BE_ACTION	 				= "%s cannot be %s.";
	public static final String ERROR_LOG_MESSAGE_SCAN_DELETION_FAILED 					= "Failed to delete the scan.";
	public static final String ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION 					= "No write permission.";
	public static final String ERROR_LOG_MESSAGE_PARAM_NULL 							= "%s is null.";
	public static final String ERROR_LOG_MESSAGE_CX_LICENSE_EXPIRED 					= "CxSuite License is already expired.";
	public static final String ERROR_LOG_MESSAGE_CX_LOGIN_FAILED 						= "Failed to login to CxSuite.";
	public static final String ERROR_LOG_MESSAGE_WEB_SERVICE_DELETE_PROJECT_FAILED 		= "Web Service API failed to delete the project.";
	public static final String ERROR_LOG_MESSAGE_WEB_SERVICE_UPDATE_PROJECT_FAILED 		= "Web Service API failed to update the project.";
	public static final String ERROR_LOG_MESSAGE_WEB_SERVICE_SCAN_FAILED 				= "Web Service API failed to execute the scan.";
	public static final String ERROR_LOG_MESSAGE_WEB_SERVICE_STOP_SCAN_FAILED 			= "Web Service API failed to stop the scan.";
	public static final String ERROR_LOG_MESSAGE_WEB_SERVICE_DELETE_SCAN_FAILED 		= "Web Service API failed to delete the scan.";
	public static final String ERROR_LOG_MESSAGE_WEB_SERVICE_REGENERATE_REPORT_FAILED 	= "Web Service API failed to regenerate the report.";
	public static final String ERROR_LOG_MESSAGE_WEB_SERVICE_GET_PRESET_LIST_FAILED 	= "Web Service API failed to retrieve the preset list.";
	public static final String ERROR_LOG_MESSAGE_SCAN_ALREADY_DELETED_FROM_CXSUITE 		= "The scan is already deleted from CxSuite server.";
	public static final String ERROR_LOG_MESSAGE_COMPLETE_PROJECT_FAILED 				= "Failed to complete the project.";
	public static final String ERROR_LOG_MESSAGE_OPEN_PROJECT_FAILED 					= "Failed to open the project.";
	public static final String ERROR_LOG_MESSAGE_REPORT_DELETION_FAILED 				= "Failed to delete the report.";
	public static final String ERROR_LOG_MESSAGE_GROUP_ASSIGNED_TO_PROJECTS 			= "The group is currently assigned to projects or users are currently assigned to the group.";
	public static final String ERROR_LOG_MESSAGE_SESSION_GET_DATA_FAILED 				= "Failed to get %s data from previous screens.";
	public static final String ERROR_LOG_MESSAGE_IMPORT_SCAN_NO_INSPECTION_LOG			= "Uploaded file has no inspection log.";
	public static final String ERROR_LOG_MESSAGE_ACCOUNT_LOCK 							= "The account is locked.";
	public static final String ERROR_LOG_MESSAGE_START_TIME_AND_END_TIME_INVALID		= "Start Time is after End Time.";
	public static final String ERROR_LOG_START_TIME_END_TIME_DIFFERENCE					= "Difference between Start Time and End Time should be greater than 10 minutes.";
	public static final String ERROR_RETRIEVAL_BASE_PATH								= "Error in retrieving configuration Base Path. Please check configuration file. Default base path will be used.";
	
	public static final String ERROR_LOG_PARAM_USER_ID 									= "User ID";
	public static final String ERROR_LOG_PARAM_ORG_ID_LIST 								= "Organization ID List";
	public static final String ERROR_LOG_PARAM_USERNAME									= "Username";
	public static final String ERROR_LOG_PARAM_EMAIL_ADDRESS							= "Email address";
	public static final String ERROR_LOG_PARAM_PASSWORD									= "Password";
	public static final String ERROR_LOG_PARAM_GROUP_NAME								= "Group name";
	public static final String ERROR_LOG_PARAM_EXPIRATION_DATE							= "Expiration date";
	public static final String ERROR_LOG_PARAM_LAST_LOGIN_DATE							= "Last login date";
	public static final String ERROR_LOG_PARAM_NO_OF_USERS								= "No. of users";
	public static final String ERROR_LOG_PARAM_USER										= "User";
	public static final String ERROR_LOG_PARAM_PROJECT_ID								= "Project ID";
	public static final String ERROR_LOG_PARAM_CASE_NUMBER								= "Case number";
	public static final String ERROR_LOG_PARAM_PROJECT_NAME								= "Project name";
	public static final String ERROR_LOG_PARAM_PROJECT_END_DATE							= "Project end date";
	public static final String ERROR_LOG_PARAM_NO_OF_SCANS								= "No. of scans";
	public static final String ERROR_LOG_PARAM_PROJECT									= "Project";
	public static final String ERROR_LOG_PARAM_PROJECT_TYPE								= "Project type";
	public static final String ERROR_LOG_PARAM_GROUP									= "Group";
	public static final String ERROR_LOG_PARAM_EXCLUSION_PACKAGE_NAME					= "Exclusion package name";
	public static final String ERROR_LOG_PARAM_CHECKLIST								= "Checklist";
	public static final String ERROR_LOG_PARAM_PROJECT_USER								= "Project user";
	public static final String ERROR_LOG_PARAM_SCAN_ID									= "Scan ID";
	public static final String ERROR_LOG_PARAM_SCAN										= "Scan";
	public static final String ERROR_LOG_PARAM_APK										= "APK";
	public static final String ERROR_LOG_PARAM_FILENAME									= "Filename";
	public static final String ERROR_LOG_PARAM_FILE										= "File";
	public static final String ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID					= "CxSuite/Android Scan ID";
	public static final String ERROR_LOG_PARAM_HASH_VALUE								= "Hash value";
	public static final String ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE					= "Scan registration date";
	public static final String ERROR_LOG_PARAM_GROUP_ID 								= "Group ID";
	public static final String ERROR_LOG_PARAM_PRESET_ID 								= "Query Set ID";
	public static final String ERROR_LOG_PARAM_SESSION_ID 								= "Session ID";
	public static final String ERROR_LOG_PARAM_ATTRIBUTE 								= "Attribute";
	public static final String ERROR_LOG_PARAM_ZIP 										= "ZIP";
	public static final String ERROR_LOG_PARAM_PROCESS 									= "Process";
	public static final String ERROR_LOG_PARAM_REVIEW									= "Review";
	public static final String ERROR_LOG_PARAM_REPORT 									= "Report";
	public static final String ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID 				= "CxSuite/Android Project ID";
	public static final String ERROR_LOG_PARAM_FILE_PATH 								= "File path";
	public static final String ERROR_LOG_PARAM_VULN_MASTER_FILE 						= "Vulnerability Master File";
	public static final String ERROR_LOG_PARAM_BUCKET_NAME 								= "Bucket name";
	public static final String ERROR_LOG_PARAM_CONFIG_VALUE 							= "Config value";
	public static final String ERROR_LOG_PARAM_SCAN_CONFIG 								= "Scan configuration";
	public static final String ERROR_LOG_PARAM_RESPONSE 								= "Response";
	public static final String ERROR_LOG_PARAM_WEB_SERVICE_URL 							= "Web Service URL";
	public static final String ERROR_LOG_PARAM_PROJECT_CONFIG 							= "Project configuration";
	public static final String ERROR_LOG_PARAM_PROJECT_SETTINGS 						= "Project settings";
	public static final String ERROR_LOG_PARAM_CLISCANARGS 								= "CliScanArgs";
	public static final String ERROR_LOG_PARAM_RUN_ID 									= "CxSuite Run ID";
	public static final String ERROR_LOG_PARAM_NODE_LIST 								= "Node List";
	public static final String ERROR_LOG_PARAM_RECIPIENT 								= "Recipient";
	public static final String ERROR_LOG_PARAM_CONFIG_FILE 								= "Configuration file";
	public static final String ERROR_LOG_PARAM_NODE 									= "Node";
	public static final String ERROR_LOG_PARAM_DB_CONFIG_VALUE 							= "Database configuration";
	public static final String ERROR_LOG_PARAM_EMAIL_EVENT 								= "Email event";
	public static final String ERROR_LOG_PARAM_ENTITY_ID 								= "Entity ID";
	public static final String ERROR_LOG_PARAM_COMPANY_ID 								= "Company ID";
	
	public static final String ERROR_LOG_PARAM_CASE_NAME								= "Case Name";
	public static final String ERROR_LOG_PARAM_TARGET_URL								= "Target url";
	public static final String ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL				= "Production Environment url";
	public static final String ERROR_LOG_PARAM_TARGET_URL_HOST							= "Target url host";
	public static final String ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_HOST			= "Production Environment url host";
	public static final String ERROR_LOG_PARAM_TARGET_URL_PROTOCOL						= "Target url protocol";
	public static final String ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_PROTOCOL		= "Production Environment url protocol";
	public static final String ERROR_LOG_PARAM_TARGET_URL_PORT							= "Target url port";
	public static final String ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_PORT			= "Production Environment url port";
	public static final String ERROR_LOG_PARAM_START_TIME 								= "Start Time";
	public static final String ERROR_LOG_PARAM_END_TIME 								= "End Time";
	public static final String ERROR_LOG_PARAM_START_URL 								= "Start Url";
	public static final String ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL					= "Authorized Patrol Url";
	public static final String ERROR_LOG_PARAM_LOGIN_URL								= "Login Url";
	public static final String ERROR_LOG_PARAM_PARAMETER_NAME_ID						= "Parameter Name Id";
	public static final String ERROR_LOG_PARAM_PARAMETER_NAME_PASS						= "Parameter Name Pass";
	public static final String ERROR_LOG_PARAM_PROTOCOL									= "Protocol";
	public static final String ERROR_LOG_PARAM_HOST										= "Host";
	public static final String ERROR_LOG_PARAM_PORT										= "Port";
	public static final String ERROR_LOG_PARAM_SCAN_NAME								= "Scan Name";
	public static final String ERROR_LOG_PARAM_TARGET_INFORMATION_LIST					= "Target Information List";
	public static final String ERROR_LOG_PARAM_CRAWLING_ONLY							= "Perform Crawling Only";
	
	public static final String ERROR_LOG_PARAM_EXTERNAL_PROXY_HOST						= "External Proxy Host";
	public static final String ERROR_LOG_PARAM_EXTERNAL_PROXY_PORT 						= "External Proxy Port";
	public static final String ERROR_LOG_PARAM_EXTERNAL_PROXY_AUTH_ID 					= "External Proxy Auth ID";
	public static final String ERROR_LOG_PARAM_EXTERNAL_PROXY_AUTH_PASSWORD 			= "External Proxy Auth Password";
	
	public static final String ERROR_LOG_PARAM_USE_CLIENT_CERTIFICATE 					= "Use Client Certificate";
	public static final String ERROR_LOG_PARAM_CERTIFICATE_FILE 						= "Certificate File";
	public static final String ERROR_LOG_PARAM_CERTIFICATE_PASSWORD 					= "Certificate Password";
	
	public static final String ERROR_LOG_PARAM_NTLM_AUTH_ID 							= "NTLM Auth ID";
	public static final String ERROR_LOG_PARAM_NTLM_AUTH_PASSWORD 						= "NTLM Auth Password";
	public static final String ERROR_LOG_PARAM_NTLM_AUTH_DOMAIN							= "NTLM Auth Domain";
	public static final String ERROR_LOG_PARAM_NTLM_AUTH_HOST 							= "NTLM Auth Host";
	
	public static final String ERROR_LOG_PARAM_DIGEST_AUTH_ID 							= "Digest Auth ID";
	public static final String ERROR_LOG_PARAM_DIGEST_AUTH_PASSWORD 					= "Digest Auth Password";
	
	public static final String ERROR_LOG_PARAM_BASIC_AUTH_ID			 				= "Basic Auth ID";
	public static final String ERROR_LOG_PARAM_BASIC_AUTH_PASSWORD  					= "Basic Auth Password";
	
	public static final String ERROR_LOG_PARAM_ACCESS_EXCLUSION_PATH					= "Access Exclusion Path";
	
	public static final String ERROR_LOG_PARAM_DETECTION_RESULT_ID 						= "Detection Result ID";
	public static final String ERROR_LOG_PARAM_DETECTION_SCAN_ID		 				= "Scan ID";
	public static final String ERROR_LOG_PARAM_DETECTION_RISK_LEVEL 					= "Risk Level";
	public static final String ERROR_LOG_PARAM_DETECTION_CATEGORY 						= "Category";
	public static final String ERROR_LOG_PARAM_DETECTION_OVERVIEW 						= "Overview";
	public static final String ERROR_LOG_PARAM_DETECTION_FUNCTION_NAME					= "Function Name";
	public static final String ERROR_LOG_PARAM_DETECTION_URL 							= "Url";
	public static final String ERROR_LOG_PARAM_DETECTION_PARAMETER_NAME					= "Parameter Name";
	public static final String ERROR_LOG_PARAM_DETECTION_JUDGEMENT 						= "Detection Judgment";
	public static final String ERROR_LOG_PARAM_DETECTION_REVIEW_COMMENT					= "Review Comment";
	public static final String ERROR_LOG_PARAM_DETECTION_IS_RESEND_IN_VEX 				= "Is Resend In Vex";
	public static final String ERROR_LOG_PARAM_DETECTION_NUMBER 						= "Number";
	
	public static final String ERROR_LOG_PARAM_IP_ADDRESS								= "IP Address";
	public static final String ERROR_LOG_PARAM_SCAN_SETTING								= "Scan Setting";
	public static final String ERROR_LOG_PARAM_LOGIN_SETTING_LIST						= "Login Setting List";
	
	public static final String ERROR_LOG_PARAM_UNAUTHORIZED_PATROL_URL					= "Unauthorized Patrol Url";  
	public static final String ERROR_LOG_PARAM_LOGIN_STATUS_DETECTION					= "Login Status Detection";
	public static final String ERROR_LOG_PARAM_SET_NOT_SEND_PASSWORD					= "Set Not Send Password Except Login Processing";
	public static final String ERROR_LOG_PARAM_PARAMETER_NAME_AS_PASSWORD				= "Parameter Name Regarded as Password";
	public static final String ERROR_LOG_PARAM_MAX_NUM_DETECTION_LINK					= "Maximum Number Detection Link";
	public static final String ERROR_LOG_PARAM_MAX_NUM_DETECTION_LINK_PER_PAGE 			= "Maximum Number Detection Link Per Page";	
	public static final String ERROR_LOG_PARAM_DETECTION_LINK_DEPTH_LIMIT				= "Detection Link Depth Limit";
	public static final String ERROR_LOG_PARAM_WAIT_TIME								= "Wait Time";
	public static final String ERROR_LOG_PARAM_NUM_OF_THREAD							= "Number Of Thread";
	public static final String ERROR_LOG_PARAM_TIME_OUT_TIME							= "Timeout Time";
	public static final String ERROR_LOG_PARAM_SET_AUTO_POST_FORM						= "Set Auto Post Form";
	public static final String ERROR_LOG_PARAM_SET_POST_METHOD							= "Set Post Method";
	public static final String ERROR_LOG_PARAM_REQUEST_HEADER							= "Request Header";
	public static final String ERROR_LOG_PARAM_NUM_OF_RETRY_AT_ERROR					= "Number of Retry At Error";
	public static final String ERROR_LOG_PARAM_SESSION_ERROR_DETECTION					= "Session Error Detection";
	public static final String ERROR_LOG_PARAM_SCREEN_TRANSITION_ERROR_DETECTION		= "Screen Transition Error Detection";
	public static final String ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID				= "Regular Expression Is Invalid";
	public static final String ERROR_LOG_PARAM_REGULAR_EXPRESSION_GROUPING_INVALID		= "Regular Expression Grouping Is Invalid";
	public static final String ERROR_LOG_PARAM_LOGIN_FORM_PATH							= "Login Form Path";
	public static final String ERROR_LOG_PARAM_LOGIN_PARAMETER_VALUE					= "Login Parameter Value";

	public static final String ERROR_LOG_ACTION_STOP									= "stopped";
	public static final String ERROR_LOG_ACTION_REEXECUTE								= "reexecuted";
	public static final String ERROR_LOG_ACTION_REQUEST									= "requested";
	public static final String ERROR_LOG_ACTION_CANCEL 									= "cancelled";
	public static final String ERROR_LOG_ACTION_REGENERATE								= "regenerated";
	
	public static final String ERROR_LOG_RECRAWL					           			= "recrawled";
	public static final String ERROR_LOG_ABORT_CRAWL									= "crawl aborted";
	public static final String ERROR_LOG_INTERRUPTED_CRAWL								= "crawl interrupted";
	public static final String ERROR_LOG_CANCEL_CRAWL									= "crawl cancelled";
	public static final String ERROR_LOG_RESTART_CRAWL									= "crawl restarted";
	public static final String ERROR_LOG_COPY_CRAWL_SETTING								= "crawl copied setting";
	public static final String ERROR_LOG_INTERRUPTED_SCAN								= "scan interrupted";
	public static final String ERROR_LOG_RESTART_SCAN									= "scan restarted";
	public static final String ERROR_LOG_COPY_CRAWL_RESULT_AND_SCAN						= "copied crawl result and scanned";
	public static final String ERROR_LOG_CANCEL_SCAN									= "scan canceled";
	public static final String ERROR_LOG_DELETE_SCAN									= "scan deleted";
	public static final String ERROR_LOG_AUTO_CRAWL										= "auto crawl";
	public static final String ERROR_LOG_UPDATE_SCAN_SETTINGS							= "update scan settings";
	
	public static final String ERROR_LOG_PARAM_EXP 										= "EXP";
	
	public static final String SERVICE 													= "service";
	public static final String MANUAL_LOGIN_PDF 										= "manual_Login.pdf";
	public static final String MANUAL_SOURCE_CODE_DIAGNOSIS_PDF 						= "manual_Source_Code_Diagnosis.pdf";
	public static final String MANUAL_ANDROID_STATIC_ANALYSIS_PDF 						= "manual_Android_Static_Analysis.pdf";
	public static final String MANUAL_IOS_STATIC_ANALYSIS_PDF 							= "manual_iOS_Static_Analysis.pdf";
	public static final String MANUAL_VEX_DYNAMIC_ANALYSIS_PDF 							= "manual_Vex_Dynamic_Analysis.pdf";
	
	public static final String USERS_ADMIN_PREFIX							= "users.admin.";
	public static final String MY_ACCOUNT_PREFIX							= "my.account.";
	public static final String ORGANIZATION_PREFIX							= "organization.";
	
	public static final String USERS_ADMIN_PASSWORD_VISIBLE					= "users.admin.password.visible";
	public static final String USERS_ADMIN_ADDRESSES_VISIBLE				= "users.admin.addresses.visible";
	public static final String USERS_ADMIN_ADDITIONAL_EMAIL_VISIBLE			= "users.admin.additional.email.visible";
	public static final String USERS_ADMIN_ANNOUNCEMENTS_VISIBLE			= "users.admin.announcements.visible";
	public static final String USERS_ADMIN_CATEGORIZATION_VISIBLE			= "users.admin.categorization.visible";
	public static final String USERS_ADMIN_COMMENTS_VISIBLE					= "users.admin.comments.visible";
	public static final String USERS_ADMIN_CUSTOM_FIELDS_VISIBLE			= "users.admin.custom.fields.visible";
	public static final String USERS_ADMIN_DETAILS_VISIBLE					= "users.admin.details.visible";
	public static final String USERS_ADMIN_DISPLAY_SETTINGS_VISIBLE			= "users.admin.display.settings.visible";
	public static final String USERS_ADMIN_INSTANT_MESSENGER_VISIBLE		= "users.admin.instant.messenger.visible";
	public static final String USERS_ADMIN_OPEN_ID_VISIBLE					= "users.admin.open.id.visible";
	public static final String USERS_ADMIN_ORGANIZATIONS_VISIBLE			= "users.admin.organizations.visible";
	public static final String USERS_ADMIN_PERSONAL_SITE_VISIBLE			= "users.admin.personal.site.visible";
	public static final String USERS_ADMIN_PHONE_NUMBERS_VISIBLE			= "users.admin.phone.numbers.visible";
	public static final String USERS_ADMIN_ROLES_VISIBLE					= "users.admin.roles.visible";
	public static final String USERS_ADMIN_SITES_VISIBLE					= "users.admin.sites.visible";
	public static final String USERS_ADMIN_SMS_VISIBLE						= "users.admin.sms.visible";
	public static final String USERS_ADMIN_SOCIAL_NETWORK_VISIBLE			= "users.admin.social.network.visible";
	public static final String USERS_ADMIN_USER_GROUPS_VISIBLE				= "users.admin.user.groups.visible";
	public static final String USERS_ADMIN_WEBSITES_VISIBLE					= "users.admin.websites.visible";
	
	public static final String MY_ACCOUNT_PASSWORD_VISIBLE					= "my.account.password.visible";
	public static final String MY_ACCOUNT_ADDRESSES_VISIBLE					= "my.account.addresses.visible";
	public static final String MY_ACCOUNT_ADDITIONAL_EMAIL_VISIBLE			= "my.account.additional.email.visible";
	public static final String MY_ACCOUNT_ANNOUNCEMENTS_VISIBLE				= "my.account.announcements.visible";
	public static final String MY_ACCOUNT_CATEGORIZATION_VISIBLE			= "my.account.categorization.visible";
	public static final String MY_ACCOUNT_COMMENTS_VISIBLE					= "my.account.comments.visible";
	public static final String MY_ACCOUNT_CUSTOM_FIELDS_VISIBLE				= "my.account.custom.fields.visible";
	public static final String MY_ACCOUNT_DETAILS_VISIBLE					= "my.account.details.visible";
	public static final String MY_ACCOUNT_DISPLAY_SETTINGS_VISIBLE			= "my.account.display.settings.visible";
	public static final String MY_ACCOUNT_INSTANT_MESSENGER_VISIBLE			= "my.account.instant.messenger.visible";
	public static final String MY_ACCOUNT_OPEN_ID_VISIBLE					= "my.account.open.id.visible";
	public static final String MY_ACCOUNT_ORGANIZATIONS_VISIBLE				= "my.account.organizations.visible";
	public static final String MY_ACCOUNT_PERSONAL_SITE_VISIBLE				= "my.account.personal.site.visible";
	public static final String MY_ACCOUNT_PHONE_NUMBERS_VISIBLE				= "my.account.phone.numbers.visible";
	public static final String MY_ACCOUNT_ROLES_VISIBLE						= "my.account.roles.visible";
	public static final String MY_ACCOUNT_SITES_VISIBLE						= "my.account.sites.visible";
	public static final String MY_ACCOUNT_SMS_VISIBLE						= "my.account.sms.visible";
	public static final String MY_ACCOUNT_SOCIAL_NETWORK_VISIBLE			= "my.account.social.network.visible";
	public static final String MY_ACCOUNT_USER_GROUPS_VISIBLE				= "my.account.user.groups.visible";
	public static final String MY_ACCOUNT_WEBSITES_VISIBLE					= "my.account.websites.visible";
	
	public static final String ORGANIZATION_ADDITIONAL_EMAIL_VISIBLE		= "organization.additional.email.visible";
	public static final String ORGANIZATION_ADDRESSES_VISIBLE 				= "organization.addresses.visible";
	public static final String ORGANIZATION_CATEGORIZATION_VISIBLE 			= "organization.categorization.visible";
	public static final String ORGANIZATION_COMMENTS_VISIBLE 				= "organization.comments.visible";
	public static final String ORGANIZATION_CUSTOM_FIELDS_VISIBLE 			= "organization.custom.fields.visible";
	public static final String ORGANIZATION_DETAILS_VISIBLE 				= "organization.details.visible";
	public static final String ORGANIZATION_ORGANIZATION_SITE_VISIBLE 		= "organization.organization.site.visible";
	public static final String ORGANIZATION_PHONE_NUMBERS_VISIBLE 			= "organization.phone.numbers.visible";
	public static final String ORGANIZATION_REMINDER_QUERIES_VISIBLE 		= "organization.reminder.queries.visible";
	public static final String ORGANIZATION_SERVICES_VISIBLE 				= "organization.services.visible";
	public static final String ORGANIZATION_WEBSITES_VISIBLE 				= "organization.websites.visible";
	
	public static final String KEY_FILTER_LIST								= "filter-list";
	public static final String KEY_FILTER_LIST_COMMA						= "filter-list-comma";
	public static final String KEY_FILTER_BY_PROJECT_ID						= "filter-by-project-id";
	public static final String KEY_FILTER_BY_GROUP_NAME						= "filter-by-group-name";
	public static final String KEY_FILTER_BY_CASE_NUMBER					= "filter-by-case-number";
	public static final String KEY_FILTER_BY_PROJECT_NAME					= "filter-by-project-name";
	public static final String KEY_FILTER_BY_PROJECT_END_DATE				= "filter-by-project-end-date";
	public static final String KEY_FILTER_BY_STATUS							= "filter-by-status";
	public static final String KEY_FILTER_BY_SCAN_COUNT						= "filter-by-scan-count";
	public static final String KEY_FILTER_BY_SCAN_ID						= "filter-by-scan-id";
	public static final String KEY_FILTER_BY_FILENAME						= "filter-by-file-name";
	public static final String KEY_FILTER_BY_HASH_VALUE						= "filter-by-hash-value";
	public static final String KEY_FILTER_BY_SCAN_MANAGER					= "filter-by-scan-manager";
	public static final String KEY_FILTER_BY_SCAN_REGISTRATION_DATE			= "filter-by-scan-registration-date";
	public static final String KEY_FILTER_BY_SCAN_PROCESS					= "filter-by-scan-process";
	public static final String KEY_FILTER_BY_CX_SCAN_ID						= "filter-by-cx-scan-id";
	public static final String KEY_FILTER_BY_CASE_NAME						= "filter-by-case-name";
	public static final String KEY_FILTER_BY_TARGET_URL						= "filter-by-target-url";
	public static final String KEY_FILTER_BY_PRODUCTION_ENVIRONMENT_URL		= "filter-by-production-environment-url";
	public static final String KEY_FILTER_BY_IMPLEMENTATION_ENVIRONMENT		= "filter-by-implementation-environment";
	public static final String KEY_FILTER_BY_START_URL						= "filter-by-start-url";
	public static final String KEY_FILTER_BY_STARTTIME						= "filter-by-start-time";
	public static final String KEY_FILTER_BY_ENDTIME						= "filter-by-end-time";
	
	public static final String KEY_FILTER_BY_DETECTION_RESULT_ID 					= "filter-by-detection-result-id";
	public static final String KEY_FILTER_BY_DETECTION_SCAN_ID 						= "filter-by-scan-id";
	public static final String KEY_FILTER_BY_DETECTION_RISK_LEVEL 					= "filter-by-risk-level";
	public static final String KEY_FILTER_BY_DETECTION_CATEGORY 					= "filter-by-category";
	public static final String KEY_FILTER_BY_DETECTION_OVERVIEW 					= "filter-by-overview";
	public static final String KEY_FILTER_BY_DETECTION_FUNCTION_NAME				= "filter-by-function-name";
	public static final String KEY_FILTER_BY_DETECTION_URL 							= "filter-by-url";
	public static final String KEY_FILTER_BY_DETECTION_PARAMETER_NAME 				= "filter-by-parameter-name";
	public static final String KEY_FILTER_BY_DETECTION_JUDGEMENT 					= "filter-by-detection-judgment";
	public static final String KEY_FILTER_BY_DETECTION_REVIEW_COMMENT 				= "filter-by-review-comment";
	public static final String KEY_FILTER_BY_DETECTION_IS_RESEND_IN_VEX 			= "filter-by-is-resend-in-vex";
	public static final String KEY_FILTER_BY_DETECTION_NUMBER                       = "filter-by-number";
	
	public static final String KEY_PROJECT_STATUS_COMPLETE					= "project-status-complete";
	public static final String KEY_PROJECT_STATUS_OPEN						= "project-status-open";

	public static final String KEY_SCAN_STATUS_WAITING						= "scan-status-waiting";
	public static final String KEY_SCAN_STATUS_WAITING_PRIORITY_NUMBER		= "scan-status-waiting-priority-number";
	public static final String KEY_SCAN_STATUS_ONGOING						= "scan-status-ongoing";
	public static final String KEY_SCAN_STATUS_REPORT_MAKING				= "scan-status-report-making";
	public static final String KEY_SCAN_STATUS_COMPLETE						= "scan-status-complete";
	public static final String KEY_SCAN_STATUS_UNDER_REVIEW					= "scan-status-under-review";
	public static final String KEY_SCAN_STATUS_REVIEW_DONE					= "scan-status-review-done";
	public static final String KEY_SCAN_STATUS_FAILURE						= "scan-status-failure";
	public static final String KEY_SCAN_SOURCECODE_LIMIT				    = "error-no-of-lines-is-too-big-750000";
	
	public static final String KEY_CRAWLING_STATUS_WAITING_PRIORITY_NUMBER	= "crawling-status-waiting-priority-number";
	public static final String KEY_CRAWLING_STATUS_WAITING					= "crawling-status-waiting";
	public static final String KEY_CRAWLING_STATUS_ONGOING					= "crawling-status-ongoing";
	public static final String KEY_CRAWLING_STATUS_INTERRUPTED				= "crawling-status-interrupted";
	public static final String KEY_CRAWLING_STATUS_FAILURE					= "crawling-status-failure";
	public static final String KEY_SCAN_STATUS_INTERRUPTED					= "scan-status-interrupted";
	public static final String KEY_CRAWLING_STATUS_COMPLETED				= "crawling-status-completed";
	
	public static final String KEY_SCAN_PROCESS_UNIT_TEST					= "scan-process-unit";
	public static final String KEY_SCAN_PROCESS_SYSTEM_TEST					= "scan-process-system";
	public static final String KEY_SCAN_PROCESS_REGULAR						= "scan-process-regular";
	public static final String KEY_SCAN_PRODUCTION_ENVIRONMENT				= "scan-production-environment";
	public static final String KEY_SCAN_VERIFICATION_ENVIRONMENT			= "scan-verification-environment";
	
	public static final String KEY_JAVAX_PORTLET_NAME						= "javax.portlet.name=";
	public static final String PORTLET_NAME_MY_ACCOUNT						= "com_liferay_my_account_web_portlet_MyAccountPortlet";
	public static final String PORTLET_NAME_USERS_ADMIN						= "com_liferay_users_admin_web_portlet_UsersAdminPortlet";
	public static final String PORTLET_NAME_LOGIN							= "com_liferay_login_web_portlet_LoginPortlet";

	public static final String KEY_MVC_COMMAND_NAME							= "mvc.command.name=";
	public static final String COMMAND_NAME_EDIT_USER						= "/users_admin/edit_user";
	public static final String COMMAND_NAME_EDIT_ORGANIZATION				= "/users_admin/edit_organization";
	public static final String COMMAND_NAME_LOGIN							= "/login/login";
	
	public static final String KEY_SERVICE_RANKING							= "service.ranking:Integer=";
	public static final String SERVICE_RANKING_200							= "200";
	public static final String SERVICE_RANKING_100							= "100";

	public static final String KEY_COMPONENT_NAME							= "component.name=";
	public static final String COMPONENT_NAME_EDIT_USER						= "com.liferay.my.account.web.internal.portlet.action.EditUserMVCActionCommand";
	public static final String COMPONENT_NAME_LOGIN							= "com.liferay.login.web.internal.portlet.action.LoginMVCActionCommand";
	
	public static final String KEY_PATH										= "path=";
	public static final String PATH_DOWNLOAD_MANUAL							= "/portal/download_manual";
	public static final String PATH_UPDATE_PASSWORD							= "/portal/update_password";
	
	public static final String KEY_KEY										= "key=";
	public static final String KEY_POST_LOGIN_EVENT							= "login.events.post";
	
	public static final String KEY_PORTLET_DISPLAY_CATEGORY					= "com.liferay.portlet.display-category=";
	public static final String DISPLAY_CATEGORY_SAMPLE						= "category.sample";
	
	public static final String KEY_PORTLET_ICON								= "com.liferay.portlet.icon=";
	public static final String ICON_ICON_PNG								= "/icon.png";
	
	public static final String KEY_PORTLET_INSTANCEABLE						= "com.liferay.portlet.instanceable=";
	public static final String INSTANCEABLE_FALSE							= "false";
	
	public static final String KEY_PRIVATE_SESSION_ATTRIBUTES				= "com.liferay.portlet.private-session-attributes=";
	public static final String PRIVATE_SESSION_ATTRIBUTES_FALSE				= "false";
	
	public static final String KEY_HEADER_PORTLET_CSS						= "com.liferay.portlet.header-portlet-css=";
	public static final String PORTLET_CSS_MAIN_CSS							= "/css/main.css";
	
	public static final String KEY_FOOTER_PORTLET_JAVASCRIPT				= "com.liferay.portlet.footer-portlet-javascript=";
	public static final String PORTLET_JAVASCRIPT_JQUERY_JS					= "/js/jquery-1.10.1.min.js";
	public static final String PORTLET_JAVASCRIPT_JQUERY_UI_JS				= "/js/jquery-ui.min.js";
	public static final String PORTLET_JAVASCRIPT_FILEDOWNLOAD_JS			= "/js/jquery.fileDownload.js";
	public static final String PORTLET_JAVASCRIPT_BOOTSTRAP_JS				= "/js/bootstrap.min.js";
	public static final String PORTLET_JAVASCRIPT_MAIN_JS					= "/js/main.js";
	
	public static final String KEY_PORTLET_CSS_CLASS_WRAPPER				= "com.liferay.portlet.css-class-wrapper=";
	public static final String CSS_CLASS_WRAPPER_ANDROID_PORTLET			= "android-portlet";
	public static final String CSS_CLASS_WRAPPER_ANNOUNCEMENT_PORTLET		= "announcement-portlet";
	public static final String CSS_CLASS_WRAPPER_CXSUITE_PORTLET			= "cxsuite-portlet";
	public static final String CSS_CLASS_WRAPPER_IOS_PORTLET				= "ios-portlet";
	public static final String CSS_CLASS_WRAPPER_VEX_PORTLET				= "vex-portlet";
	
	public static final String KEY_PORTLET_DISPLAY_NAME						= "javax.portlet.display-name=";
	public static final String DISPLAY_NAME_ANDROID_PORTLET					= "Android Portlet";
	public static final String DISPLAY_NAME_ANNOUNCEMENT_PORTLET			= "Announcement Portlet";
	public static final String DISPLAY_NAME_CXSUITE_PORTLET					= "CxSuite Portlet";
	public static final String DISPLAY_NAME_IOS_PORTLET						= "IOS Portlet";
	public static final String DISPLAY_NAME_VEX_PORTLET						= "Vex Portlet";
	
	public static final String KEY_INIT_VIEW_TEMPLATE						= "javax.portlet.init-param.view-template=";
	
	public static final String KEY_PORTLET_EXPIRATION_CACHE					= "javax.portlet.expiration-cache=";
	public static final String EXPIRATION_CACHE								= "0";
	
	public static final String KEY_PORTLET_SUPPORTS_MIME_TYPE				= "javax.portlet.supports.mime-type=";
	
	public static final String KEY_PORTLET_RESOURCE_BUNDLE					= "javax.portlet.resource-bundle=";
	public static final String RESOURCE_BUNDLE_LANGUAGE_JP					= "content.Language-jp";
	
	public static final String KEY_PORTLET_INFO_TITLE						= "javax.portlet.info.title=";
	public static final String INFO_TITLE_ANDROID_PORTLET					= "Android Portlet";
	public static final String INFO_TITLE_ANNOUNCEMENT_PORTLET				= "Announcement Portlet";
	public static final String INFO_TITLE_CXSUITE_PORTLET					= "CxSuite Portlet";
	public static final String INFO_TITLE_IOS_PORTLET						= "IOS Portlet";
	public static final String INFO_TITLE_VEX_PORTLET						= "Vex Portlet";
	
	public static final String KEY_PORTLET_INFO_SHORT_TITLE					= "javax.portlet.info.short-title=";
	public static final String INFO_SHORT_TITLE_ANDROID						= "Android";
	public static final String INFO_SHORT_TITLE_ANNOUNCEMENT				= "Announcement";
	public static final String INFO_SHORT_TITLE_CXSUITE						= "CxSuite";
	public static final String INFO_SHORT_TITLE_IOS							= "IOS";
	public static final String INFO_SHORT_TITLE_VEX							= "VEX";
	
	public static final String KEY_SECURITY_ROLE_REF						= "javax.portlet.security-role-ref=";
	public static final String ROLE_ADMINISTRATOR							= "administrator";
	public static final String ROLE_GUEST									= "guest";
	public static final String ROLE_POWER_USER								= "power-user";
	public static final String ROLE_USER									= "user";
	
	public static final String KEY_CONTEXT_ID								= "context.id=";
	public static final String CONTEXT_ID_UBSECURE_CUSTOM_JSP_BAG			= "UBSecurePortalCustomJspBag";
	
	public static final String KEY_CONTEXT_NAME								= "context.name=";
	public static final String CONTEXT_NAME_UBSECURE_CUSTOM_JSP_BAG			= "UBSecure Portal Custom JSP Bag";
	
	public static final String CUSTOM_JSPS									= "META-INF/custom_jsps/";
	public static final String CUSTOM_JSP_ENTRY_JSP							= "*.jsp";
	public static final String CUSTOM_JSP_ENTRY_JSPF						= "*.jspf";
	
	public static final String KEY_LABEL_DETAILS							= "label-details";
	public static final String KEY_LABEL_PASSWORD							= "label-password";
	public static final String KEY_LABEL_USER_INFORMATION					= "label-user-information";
	public static final String KEY_LABEL_GROUP_INFORMATION					= "label-group-information";
	
	public static final String KEY_FORM_NAVIGATOR_ENTRY_ORDER						= "form.navigator.entry.order:Integer=";
	public static final String ENTRY_ORDER_USER_ADDITIONAL_EMAIL_ADDRESSES			= "60";
	public static final String ENTRY_ORDER_USER_ADDRESSES							= "80";
	public static final String ENTRY_ORDER_USER_ANNOUNCEMENTS						= "40";
	public static final String ENTRY_ORDER_USER_CATEGORIZATION						= "10";
	public static final String ENTRY_ORDER_USER_COMMENTS							= "20";
	public static final String ENTRY_ORDER_USER_CUSTOM_FIELDS						= "10";
	public static final String ENTRY_ORDER_USER_DETAILS								= "80";
	public static final String ENTRY_ORDER_USER_DISPLAY_SETTINGS					= "30";
	public static final String ENTRY_ORDER_USER_INSTANT_MESSENGER					= "40";
	public static final String ENTRY_ORDER_USER_OPEN_ID								= "10";
	public static final String ENTRY_ORDER_USER_ORGANIZATIONS						= "60";
	public static final String ENTRY_ORDER_USER_PASSWORD							= "70";
	public static final String ENTRY_ORDER_USER_PERSONAL_SITE						= "20";
	public static final String ENTRY_ORDER_USER_PHONE_NUMBERS						= "70";
	public static final String ENTRY_ORDER_USER_ROLES								= "30";
	public static final String ENTRY_ORDER_USER_SITES								= "50";
	public static final String ENTRY_ORDER_USER_SMS									= "20";
	public static final String ENTRY_ORDER_USER_SOCIAL_NETWORK						= "30";
	public static final String ENTRY_ORDER_USER_USER_GROUPS							= "40";
	public static final String ENTRY_ORDER_USER_WEBSITES							= "50";
	public static final String ENTRY_ORDER_ORGANIZATION_ADDITIONAL_EMAIL_ADDRESSES	= "30";
	public static final String ENTRY_ORDER_ORGANIZATION_ADDRESSES					= "50";
	public static final String ENTRY_ORDER_ORGANIZATION_CATEGORIZATION				= "10";
	public static final String ENTRY_ORDER_ORGANIZATION_COMMENTS					= "30";
	public static final String ENTRY_ORDER_ORGANIZATION_CUSTOM_FIELDS				= "10";
	public static final String ENTRY_ORDER_ORGANIZATION_DETAILS						= "30";
	public static final String ENTRY_ORDER_ORGANIZATION_ORGANIZATION_SITE			= "20";
	public static final String ENTRY_ORDER_ORGANIZATION_PHONE_NUMBERS				= "40";
	public static final String ENTRY_ORDER_ORGANIZATION_REMINDER_QUERIES			= "20";
	public static final String ENTRY_ORDER_ORGANIZATION_SERVICES					= "10";
	public static final String ENTRY_ORDER_ORGANIZATION_WEBSITES					= "20";
	
	public static final String KEY_FORM_NAVIGATOR_CATEGORY_ORDER					= "form.navigator.category.order:Integer=";
	public static final String CATEGORY_ORDER_USER_IDENTIFICATION					= "20";
	public static final String CATEGORY_ORDER_USER_MISCELLANEOUS					= "10";
	public static final String CATEGORY_ORDER_USER_USER_INFORMATION					= "30";
	public static final String CATEGORY_ORDER_ORGANIZATION_IDENTIFICATION			= "20";
	public static final String CATEGORY_ORDER_ORGANIZATION_MISCELLANEOUS			= "10";
	public static final String CATEGORY_ORDER_ORGANIZATION_ORGANIZATION_INFORMATION	= "30";
	
	public static final String KEY_LANGUAGE_ID										= "language.id=";
	public static final String LANGUAGE_JA_JP										= "ja_JP";
	
	public static final String PARAM_ERROR 		= "error";
	
	public static final int SCAN_STATUS_LIMIT = 6;
	public static final int REPORT_TYPE_LIMIT = 6;
	
	public static final String METHOD_IS_CASE_NUMBER_VALID 											= "isCaseNumberValid";
	public static final String METHOD_DELETE_RELATED_TO_PROJECT 									= "deleteRelatedToProject";
	public static final String METHOD_GET_OVERALL_ADMIN_PROJECTS 									= "getOverallAdminProjects";
	public static final String METHOD_GET_GROUP_ADMIN_PROJECTS 										= "getGroupAdminProjects";
	public static final String METHOD_GET_USER_PROJECTS 											= "getUserProjects";
	public static final String METHOD_GET_PROJECTS_BY_OWNER_GROUP 									= "getProjectsByOwnerGroup";
	public static final String METHOD_GET_PROJECTS_BY_OWNER_GROUP_LIST								= "getProjectsByOwnerGroupList";
	public static final String METHOD_UPDATE_PROJECT_USERS 											= "updateProjectUsers";
	public static final String METHOD_ADD_REPORT 													= "addReport";
	public static final String METHOD_GET_REFERENCES 												= "getReferences";
	public static final String METHOD_GET_REPORT_BY_SCAN_ID 										= "getReportByScanId";
	public static final String METHOD_GET_SCAN_REGISTRATION_DATE 									= "getScanRegistrationDate";
	public static final String METHOD_UPDATE_ANDROID_SCAN_STATUS 									= "updateAndroidScanStatus";
	public static final String METHOD_DELETE_REPORT 												= "deleteReport";
	public static final String METHOD_GET_FILE_PATH 												= "getFilePath";
	public static final String METHOD_GET_SCAN_STATUS 												= "getScanStatus";
	public static final String METHOD_COUNT_SCAN_WAITING 											= "countScanWaiting";
	public static final String METHOD_GET_SCANS_BY_PROJECT_ID 										= "getScansByProjectId";
	public static final String METHOD_GENERATE_OVERALL_ADMIN_SQL_QUERY 								= "generateOverallAdminSQLQuery";
	public static final String METHOD_GET_OVERALL_ADMIN_RESULTS 									= "getOverallAdminResults";
	public static final String METHOD_GENERATE_USER_SQL_QUERY 										= "generateUserSQLQuery";
	public static final String METHOD_GET_USER_RESULTS 												= "getUserResults";
	public static final String METHOD_GENERATE_ENTIRE_SCAN_SQL_QUERY 								= "generateEntireScanSQLQuery";
	public static final String METHOD_GET_ENTIRE_SCAN_RESULTS 										= "getEntireScanResults";
	public static final String METHOD_GENERATE_SCAN_SQL_QUERY 										= "generateScanSQLQuery";
	public static final String METHOD_GET_SCAN_RESULTS												= "getScanResults";
	public static final String METHOD_DELETE_PROJECT_USER 											= "deleteProjectUser";
	public static final String METHOD_GET_USER_PROJECTS_COUNT										= "getUserProjectsCount";
	public static final String METHOD_GET_SEARCHED_USER_PROJECTS_COUNT								= "getSearchedUserProjectsCount";
	public static final String YCLOUD_BUCKETNAME 						= "ycloud.bucketname";
	public static final String METHOD_IS_DELETED_SCAN_IN_CX 			= "isDeletedScanInCxServer";
	public static final String SQL_PLACEHOLDER_0		= "{0}";
	public static final String SQL_PLACEHOLDER_1		= "{1}";
	public static final String SQL_PLACEHOLDER_2		= "{2}";
	public static final String SQL_PLACEHOLDER_3		= "{3}";
	public static final String SQL_PLACEHOLDER_4		= "{4}";
	public static final String SQL_PLACEHOLDER_5		= "{5}";
	public static final String SQL_PLACEHOLDER_6		= "{6}";
	public static final String SQL_PLACEHOLDER_7		= "{7}";
	public static final String SQL_PLACEHOLDER_8		= "{8}";
	public static final String SQL_PLACEHOLDER_9 		= "{9}";
	public static final String SQL_PLACEHOLDER_10		= "{10}";
	public static final String SQL_PLACEHOLDER_11		= "{11}";
	public static final String SQL_PLACEHOLDER_12		= "{12}";
	public static final String SQL_PLACEHOLDER_13		= "{13}";
	public static final String SQL_PLACEHOLDER_14		= "{14}";
	public static final String SQL_PLACEHOLDER_15		= "{15}";
	public static final String SQL_PLACEHOLDER_16		= "{16}";
	public static final String SQL_PLACEHOLDER_17		= "{17}";
	public static final String SQL_PLACEHOLDER_18		= "{18}";
	public static final String SQL_PLACEHOLDER_19		= "{19}";
	public static final String SQL_PLACEHOLDER_20		= "{20}";
	public static final String STRING_OPEN_CURLY_BRACE		= "{";
	public static final String STRING_CLOSE_CURLY_BRACE		= "}";
	public static final String STRING_WHERE_STATEMENT 		= " WHERE ";
	public static final String STRING_AND_STATEMENT 		= " AND ";
	public static final String SQL_WILDCARD_PERCENT			= "%";
	public static final String METHOD_GET_REPORTS_BY_REPORTTYPE_PROJECTTYPE_STARTDATE_ENDDATE = "getReportsByReportTypeProjectTypeStartDateEndDate";
	public static final String METHOD_GET_CSV_REPORTS_BY_PROJECTTYPE_STARTDATE_ENDDATE = "getCSVReportsByProjectTypeStartDateEndDate";
	public static final String METHOD_GET_USER_PROJECTS_TO_DOWNLOAD = "getUserProjectsToDownload";
	public static final String METHOD_GET_OVERALL_ADMIN_PROJECTS_TO_DOWNLOAD = "getOverallAdminProjectsToDownload";
	public static final String METHOD_GET_MAX_REPORT_ID 			= "getMaxReportId";
	public static final String ERROR_LOG_PARAM_STATUS 									= "Status";
	public static final String ERROR_LOG_PARAM_REPORT_TYPE 								= "Report type";
	public static final String ERROR_LOG_PARAM_REPORT_ID 								= "Report ID";

	public static final String FILTER_GROUP_BY_GROUP_NAME			= " グループ名 「";
	public static final String FILTER_GROUP_BY_NO_OF_USERS			= " 所属ユーザ数「";
	public static final String FILTER_GROUP_BY_SERVICE				= " 利用可能サービス「";

	public static final String FILTER_USER_BY_GROUP_NAME			= " グループ名「";
	public static final String FILTER_USER_BY_USER_ID				= " ユーザID「";
	public static final String FILTER_USER_BY_USER_NAME				= " ユーザ名「";
	public static final String FILTER_USER_BY_MAIL_ADDRESS			= " メールアドレス「";
	public static final String FILTER_USER_BY_LAST_LOGIN_DATE		= " 最終ログイン日時「";
	public static final String FILTER_USER_BY_EXPIRY_DATE			= " 有効期限「";
	public static final String FILTER_USER_BY_ACCOUNT_LOCK			= " アカウントロック「";

	public static final String STRING_SERVICE_ANDROID				= "Android静的解析";
	public static final String STRING_SERVICE_CXSUITE				= "ソースコード診断";
	public static final String STRING_SERVICE_IOS					= "iOS静的解析";
	public static final String STRING_SERVICE_VEX					= "Vex動的解析";
	public static final String STRING_ACCOUNT_LOCK					= "ロックなし";
	public static final String STRING_ACCOUNT_UNLOCK				= "ロック中";
	
	public static final String VEX_TARGET_ENVIRONEMENT_PRODUCTION   = "本番環境";
	public static final String VEX_TARGET_ENVIRONEMENT_VERIFICATION = "検証環境";
	
	public static final String FORM_NAVIGATOR_USERS_ADMIN_PREFIX	= "users.admin.";

	public static final int OVERALL_USER_ADMIN_ADMINISTRATOR_		= 2;//TODO duplicate but different value
	
	public static final String KEY_FILTER_BY_USER_ID				= "filter-by-user-id";
	public static final String KEY_FILTER_BY_USERNAME				= "filter-by-username";
	public static final String KEY_FILTER_BY_MAIL_ADDRESS			= "filter-by-mail-address";
	public static final String KEY_FILTER_BY_LAST_LOGIN_DATE		= "filter-by-last-login-date";
	public static final String KEY_FILTER_BY_EXPIRY_DATE			= "filter-by-expiry-date";
	public static final String KEY_FILTER_BY_ACCOUNT_LOCK_STATE		= "filter-by-account-lock-state";
	public static final String KEY_FILTER_BY_USER_COUNT				= "filter-by-user-count";
	public static final String KEY_FILTER_BY_SERVICE				= "filter-by-service";

	public static final String ERROR_LOG_MESSAGE_PARAM_CONSISTS_MULTIBYTE_CHARACTERS	= "%s contains multibyte characters.";

	public static final int SCAN_WAITING			=1;
	public static final int SCANNING				=2;
	public static final int REPORT_MAKING			=3;
	public static final int COMPLETE				=4;
	public static final int UNDER_REVIEW			=5;
	public static final int FAILURE					=6;
	public static final int REGENERATING			=7;
	public static final int SCAN_INTERRUPTED		=8;
	public static final int CRAWLING_WAITING		=9;
	public static final int CRAWLING				=10;
	public static final int CRAWLING_INTERRUPTED	=11;
	public static final int CRAWLING_FAILURE		=12;
	/*public static final int CRAWLING_CANCELLED		=13;
	public static final int SCANNING_CANCELLED		=14;*/
	public static final int CRAWLING_COMPLETED		=13;
	
	public static final String AUDIT_RESULT_FOLDER 		= "audit_result";
	public static final String CRAWLER_LOG_FOLDER   	= "crawler_log";
	public static final String PROXY_LOG_FOLDER     	= "proxy_log";
	public static final String BACKEND_RESULT_FOLDER    = "backend_result";
	public static final String SLITHY_RESULT_FOLDER     = "slithy_result";
	public static final String DETECT_RESULT 			= "detect_result";
	
	public static final String COMPLETE_STATUS 			= "complete";
	public static final String WAITING_STATUS 			= "waiting";
	public static final String BEFORE_START_STATUS 		= "before_start";
	
	public static final String STR_DETECTION_JUDGEMENT 						= "検出";
	public static final String STR_DETECTION_JUDGEMENT_OVER_DETECTION 		= "過検知";
	public static final String STR_DETECTION_JUDGEMENT_FIXED		 		= "修正済み";
	public static final String STR_DETECTION_RESEND_RESULT					= "resendResult";
	public static final String STR_DETECTION_RESEND							= "resend";
	public static final String DETECTION_ARROW_ICON_PATH					= "/o/ubsecure-theme/images/arrows/blue_down.png";
	
	public static final String CX_UPLOAD_PATH								= "cxUploadPath";
	public static final String CX_DOWNLOAD_PATH								= "cxDownloadPath";
	public static final String ANDROID_UPLOAD_PATH							= "androidUploadPath";
	public static final String ANDROID_DOWNLOAD_PATH						= "androidDownloadPath";
	public static final String IOS_UPLOAD_PATH								= "iosUploadPath";
	public static final String IOS_DOWNLOAD_PATH							= "iosDownloadPath";
	public static final String VEX_UPLOAD_PATH								= "vexUploadPath";
	public static final String VEX_DOWNLOAD_PATH							= "vexDownloadPath";
	public static final String RESOURCE_PATH								= "resourcePath";
	
	public static final int PROJECT_TYPE_CX_SUITE 							= 1;
	public static final int PROJECT_TYPE_ANDROID 							= 2;
	public static final int PROJECT_TYPE_IOS								= 3;
	public static final int PROJECT_TYPE_VEX								= 4;	
	
	public static final String CONFIG_ANNOUNCEMENT_FILE 					= "ubsportal-announcement.properties";
	public static final String ANDROID_EDIT_PROJECT_ONCLICK_ERROR 			= "androidEditProjectOnclickError";
	
	public static final String COMMA_SPACE 									= " ,";
	public static final String CX_SERVER_ERROR_MSG 							= "cxServerErrorMsg: ";
	public static final String CX_PROJECT_CONFIG_MSG                     	="プロジェクトの詳細を取得できませんでした。プロジェクトが削除または変更されている可能性があります。";
	public static final String SUCCESSFUL_UPLOAD                     		="successfulUpload";
	
	public static final String DOWNLOAD_OPTIONS								= "X-Download-Options";
	public static final String NO_OPEN										= "noopen";
}
