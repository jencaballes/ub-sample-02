package jp.ubsecure.portal.jubjub.portlet.enumclass;

public enum UserRole {
	SYSTEM_ADMIN(1, "システム管理者"),
	OVERALL_ADMIN(2, "全体管理者"),
	GROUP_ADMIN(3, "グループ管理者"),
	GEN_USER(4, "一般ユーザ"),
	ADMINISTRATOR(5, "Administrator");

	int iValue;
	String strUserRole;
	
	private UserRole(int iValue, String strUserRole){
		this.iValue= iValue;
		this.strUserRole = strUserRole;
	}
	
	public int getInteger() {
		return iValue;
	}
	
	public String getString() {
		return strUserRole;
	}
}