package jp.ubsecure.portal.jubjub.portlet.enumclass;

public enum ProjectAttribute {
	NEW(1, "新規"),
	REGULAR(2, "定期");

	int iValue;
	String strAttribute;
	
	private ProjectAttribute(int iValue, String strAttribute){
		this.iValue= iValue;	
		this.strAttribute = strAttribute;
	}
	
	public int getInteger() {
		return iValue;
	}
	
	public String getString() {
		return strAttribute;
	}
}