package jp.ubsecure.portal.jubjub.portlet.enumclass;

public enum ScanStatus {
	SCAN_WAITING(1, "スキャン待ち"),
	SCANNING(2, "スキャン中"),
	REPORT_MAKING(3, "レポート作成中"),
	COMPLETE(4, "完了"),
	UNDER_REVIEW(5, "精査中"),
	FAILURE(6, "失敗"),
	REGENERATING(7, "完了"),
	SCAN_INTERRUPTED(8,"スキャン中断"),
	CRAWLING_WAITING(9,"巡回待ち"),
	CRAWLING(10,"巡回中"),
	CRAWLING_INTERRUPTED(11,"巡回中断"),
	CRAWLING_FAILURE(12,"巡回失敗"),
	/*CRAWLING_CANCELLED(13,"巡回失敗"),
	SCANNING_CANCELLED(14,"失敗"),*/
	CRAWLING_COMPLETED(13,"自動巡回完了");
	
	int iValue;
	String strStatus;
	
	private ScanStatus(int iValue, String strStatus) {
		this.iValue = iValue;
		this.strStatus = strStatus;
	}
	
	public int getInteger() {
		return iValue;
	}
	
	public String getString() {
		return strStatus;
	}
}
