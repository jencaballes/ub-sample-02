package jp.ubsecure.portal.jubjub.portlet.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.xml.XmlHelper;

public class ConfigurationFileParser {
	
	public static void testMethod() {
		System.out.println("ConfigurationFileParser test");
	}
	
	public static final List<String> DEFAULT_EXCLUDED_PACKAGE_NAMES;
	static {
		DEFAULT_EXCLUDED_PACKAGE_NAMES = new ArrayList<String>();
		DEFAULT_EXCLUDED_PACKAGE_NAMES.add("android.default.package.android");
		DEFAULT_EXCLUDED_PACKAGE_NAMES.add("android.default.package.google");
		DEFAULT_EXCLUDED_PACKAGE_NAMES.add("android.default.package.apache");
	}
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ConfigurationFileParser.class);
	
	public static LinkedHashMap<String, String> getConfig(List<String> lstConfigKeys) throws UBSPortalException{
		LinkedHashMap<String, String> configValues = new LinkedHashMap<String, String>();
		Map<String, Object> params = new HashMap<String, Object>();
		File configFile = null;
		FileInputStream inputStream = null; 
		InputStreamReader reader = null;
		
		try {
			
			if (!CommonUtil.isListNullOrEmpty(lstConfigKeys)) {
				configFile = getConfigurationFile();
				if (!CommonUtil.isValidAndExistingFile(configFile)) {
					params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_FILE));
					throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR);
				}
				
				inputStream = new FileInputStream(configFile);
				reader = new InputStreamReader(inputStream , PortalConstants.UTF8);
				
				Properties properties = new Properties();
				properties.load(reader);
				inputStream.close();
				reader.close();

				for (String key : lstConfigKeys) {
					String strValue = (String) properties.getProperty(key);
					if (CommonUtil.isStringNullOrEmpty(strValue)) {
						params.put("strValue", strValue);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
						throw new UBSPortalException(PortalErrors.CONFIG_VALUE_ERROR);
					}
					configValues.put(key, strValue);
				}
			}
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_CONFIG, params, e);
			throw e;
		} catch (FileNotFoundException e) {
			params.put("configFile exist", configFile.exists());
			
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} catch (IOException e) {
			params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
			
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} catch (Exception e) {
			params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
			params.put("lstConfigKeys", lstConfigKeys);
			
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} finally {
			configFile = null;
			inputStream = null;
			reader = null;
			params.clear();
			params = null;
		}
		
		return configValues;
	}
	
	public static Map<String, String> getPresetConfig() throws UBSPortalException{
		LinkedHashMap<String, String> configValues = new LinkedHashMap<String, String>();
		Map<String, Object> params = new HashMap<String, Object>();
		File configFile = null;
		FileInputStream inputStream = null; 
		InputStreamReader reader = null;
		String key = null;
		List<String> keyList = new ArrayList<String>();
		@SuppressWarnings("rawtypes")
		Enumeration propertyList = null;
		
		try {
			
			configFile = getConfigurationFile();
			if (!CommonUtil.isValidAndExistingFile(configFile)) {
				params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_FILE));
				throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR);
			}
			
			inputStream = new FileInputStream(configFile);
			reader = new InputStreamReader(inputStream , PortalConstants.UTF8);

			OrderedProperties orderedProperties = new OrderedProperties();
			orderedProperties.load(reader);
			inputStream.close();
			reader.close();
			
			propertyList = orderedProperties.keys();
			while (propertyList.hasMoreElements()) {
				key = propertyList.nextElement().toString();
				
				if (key.startsWith(PortalConstants.PRESET_PREFIX)) {
					keyList.add(key);
				}
			}
			
			configValues = getConfig(keyList);
			
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_CONFIG, params, e);
			throw e;
		} catch (FileNotFoundException e) {
			params.put("configFile exist", configFile.exists());
			
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} catch (IOException e) {
			params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
			
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} catch (Exception e) {
			params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} finally {
			configFile = null;
			inputStream = null;
			reader = null;
			params.clear();
			params = null;
		}
		
		return configValues;
	}
	
	/**
	 * Get config value from ubsportal-config.properties file.
	 * @param strConfigKey
	 * @return
	 * @throws UBSPortalException
	 */
	public static String getConfig(String strConfigKey) throws UBSPortalException{
		return getConfigValue(strConfigKey, getConfigurationFile());
	}
	
	/**
	 * Get config value from specific file.
	 * @param strConfigKey
	 * @param configFile - specify file path
	 * @return
	 * @throws UBSPortalException
	 */
	public static String getConfigFromFile(String strConfigKey, File configFile) throws UBSPortalException{
		return getConfigValue(strConfigKey, configFile);
	}
		
	private static String getConfigValue(String strConfigKey, File configFile) throws UBSPortalException{
		Map<String, Object> params = new HashMap<String, Object>();
		String strConfigValue = null;
		FileInputStream  inputStream = null;
		InputStreamReader reader = null;
		try {
			
			if (!CommonUtil.isStringNullOrEmpty(strConfigKey)) {
				if (!CommonUtil.isValidAndExistingFile(configFile)) {
					params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_FILE));
					throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR);
				}
				
				inputStream = new FileInputStream(configFile);
				reader = new InputStreamReader(inputStream, PortalConstants.UTF8);
				
				Properties properties = new Properties();
				properties.load(reader);
				inputStream.close();
				reader.close();
	
				strConfigValue = (String) properties.getProperty(strConfigKey);
				
				if (!PortalConstants.CONFIG_KEY_ANNOUNCEMENT.equals(strConfigKey)){
					if (strConfigValue == null) {
						params.put("strConfigValue", strConfigValue);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
						throw new UBSPortalException(PortalErrors.CONFIG_VALUE_NULL_ERROR);
					} else if (strConfigValue.equals(PortalConstants.STRING_EMPTY)) {
						params.put("strConfigValue", strConfigValue);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
						throw new UBSPortalException(PortalErrors.CONFIG_VALUE_EMPTY_ERROR);
					}
				}
				
			}
				
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_CONFIG, params, e);
			throw e;
		} catch (FileNotFoundException e) {
			params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
			
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} catch (IOException e) {
			params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
			
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} catch (Exception e) {
			params.put("configFile valid", CommonUtil.isValidAndExistingFile(configFile));
			params.put("strConfigKey", strConfigKey);
			
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_GET_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, e);
		} finally {
			reader = null;
			inputStream = null;
			configFile = null;
			params.clear();
			params = null;
		}
		
		return strConfigValue;
	}
	
	public static File getConfigurationFile() {
		String resourceBaseDirectory = getBasePath(PortalConstants.RESOURCE_PATH, PortalConstants.CONFIG_KEY_RESOURCE_PATH);
		if(CommonUtil.isStringNullOrEmpty(resourceBaseDirectory)){
			resourceBaseDirectory = getDefaultResourceBasePath();
		}
		return new File(resourceBaseDirectory
				+ File.separator 
				+ PortalConstants.CONFIG_FILE);
	}
	
	public static Map<String, String> getDBConfig() throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String> config = new HashMap<String, String>();
		
		File xmlReport = null;
		DocumentBuilderFactory dbFactory = null;
		DocumentBuilder dBuilder = null;
		Document doc = null;
		Node node = null;		
		NodeList nodeList = null;
		
		String strUrl = null;
		String strPassword = null;
		String strUsername = null;
		String strDriverClassName = null;
		
		try {
			xmlReport = new File(System.getProperty("catalina.base")
				+ File.separator 
				+ "conf" 
				+ File.separator 
				+ "Catalina" 
				+ File.separator
				+ "localhost" 
				+ File.separator 
				+ "ROOT.xml");
			
			if (!CommonUtil.isValidAndExistingFile(xmlReport)) {
				params.put("xmlReport valid", CommonUtil.isValidAndExistingFile(xmlReport));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.SETUP_WIZARD_ERROR);
			}
			
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(xmlReport);
			doc.getDocumentElement().normalize();
			nodeList = doc.getChildNodes();
						
			if (nodeList == null) {
				params.put("nodeList", nodeList);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NODE_LIST));
				throw new UBSPortalException(PortalErrors.SETUP_WIZARD_ERROR);
			}		
				
			node = XmlHelper.getNode("Context", nodeList);
			nodeList = node.getChildNodes();
			node = XmlHelper.getNode("Resource", nodeList);
			
			if (CommonUtil.isNodeNull(node)) {
				params.put("node", node);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NODE));
				throw new UBSPortalException(PortalErrors.SETUP_WIZARD_ERROR);
			}
			
			if (node.hasAttributes()) {
				strUrl = XmlHelper.getNodeAttr("url", node);
				strPassword = XmlHelper.getNodeAttr("password", node);
				strUsername = XmlHelper.getNodeAttr("username", node);
				strDriverClassName = XmlHelper.getNodeAttr("driverClassName", node);
				
				if (CommonUtil.isStringNullOrEmpty(strUrl) || CommonUtil.isStringNullOrEmpty(strPassword) 
						|| CommonUtil.isStringNullOrEmpty(strUsername) || CommonUtil.isStringNullOrEmpty(strDriverClassName)) {
					params.put("strUrl", strUrl);
					params.put("strDriverClassName", strDriverClassName);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_DB_CONFIG_VALUE));
					throw new UBSPortalException(PortalErrors.SETUP_WIZARD_ERROR);	
				}
				
				config.put("url", strUrl);
				config.put("password", strPassword);
				config.put("username", strUsername);
				config.put("driverClassName", strDriverClassName);
			}
		} catch (UBSPortalException e) {
			log.debug(PortalErrors.SETUP_WIZARD_ERROR, PortalConstants.METHOD_GET_DB_CONFIG, params, e);
			throw e;
		} catch (Exception e) {
			params.put("xmlReport", xmlReport);
			log.debug(PortalErrors.SETUP_WIZARD_ERROR, PortalConstants.METHOD_GET_DB_CONFIG, params, e);
			throw new UBSPortalException(PortalErrors.SETUP_WIZARD_ERROR, e);
		} finally {
			dbFactory = null;
			dBuilder = null;
			doc = null;
			node = null;		
			strUrl = null;
			strPassword = null;
			strUsername = null;
			strDriverClassName = null;
			params.clear();
			params = null;
		}
		
		return config;
	}
	
	public static String getResourceBasePath(String configKeyName, String configKey){
		return getBasePath(configKeyName, configKey);
	}
	
	private static String getBasePath(String configKeyName, String configKey){
		String configValue = PortalConstants.STRING_EMPTY;
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			params.put(configKeyName, configKey);
			configValue = PropsUtil.get(configKey);
			if(CommonUtil.isStringNullOrEmpty(configValue)){
				throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.ERROR_RETRIEVAL_BASE_PATH, null);
			}
		} catch (UBSPortalException e) {
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_VEX_GET_BASE_PATH, params, e);
		} catch (Exception e){
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_VEX_GET_BASE_PATH, params, e);
		}
		
		return configValue;
	}
	
	private static String getDefaultResourceBasePath(){
		String defaultResourcePath = PropsUtil.get(PropsKeys.LIFERAY_HOME) 
									+ File.separator 
									+ PortalConstants.DATA_FOLDER 
									+ File.separator 
									+ PortalConstants.RESOURCE_FOLDER;
		return defaultResourcePath;
	}
}
