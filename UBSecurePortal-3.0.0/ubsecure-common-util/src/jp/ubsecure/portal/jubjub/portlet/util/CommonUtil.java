package jp.ubsecure.portal.jubjub.portlet.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;

public class CommonUtil {

	public static boolean isObjectNull(Object object) {
		return object == null;
	}

	public static boolean isStringNullOrEmpty(String strString) {
		return strString == null || strString.isEmpty();
	}

	public static boolean isMapNullOrEmpty(Map<?, ?> map) {
		return map == null || map.isEmpty();
	}

	public static boolean isListNullOrEmpty(List<?> lstList) {
		return lstList == null || lstList.isEmpty();
	}

	public static boolean isValidAndExistingFile(File file) {
		return file != null && file.isFile() && file.exists() && file.length() > 0;
	}

	public static boolean isNodeListNullOrEmpty(NodeList nodeList) {
		return nodeList == null || nodeList.getLength() == 0;
	}

	public static boolean isNodeNull(Node node) {
		return node == null;
	}

	public static boolean isOutOfRange(int iNumber, int iLessThan, int iGreaterThan) {
		return iNumber < iLessThan || iNumber > iGreaterThan;
	}

	public static boolean isPortalError(String errorCode) {
		return errorCode != null && (errorCode.startsWith(PortalConstants.PORTAL_ERROR_PRE_S)
				|| errorCode.startsWith(PortalConstants.PORTAL_ERROR_PRE_U));
	}

	public static boolean isCollectionNullOrEmpty(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	public static boolean isValidBytes(byte[] tbytes) {
		return tbytes != null && tbytes.length > 0;
	}

	public static boolean isValidFile(File file) {
		return file != null && file.isFile() && file.length() > 0;
	}

	/*
	 * public static boolean masterFileExists() { //TODO: return
	 * VulnerabilityMasterParser.getvulnerabilityMasterFile().exists(); }
	 */

	public static int getStringBytes(String strValue) {
		int bytes = 0;

		for (int i = 0; i < strValue.length(); i++) {
			String character = strValue.charAt(i) + PortalConstants.STRING_EMPTY;
			int charByte = character.getBytes().length;

			if (charByte > 2) {
				bytes += 2;
			} else {
				bytes += charByte;
			}
		}

		return bytes;
	}

	public static boolean canWrite(String strFilePath) {
		File temp = null;
		FileOutputStream os = null;

		try {
			new File(strFilePath).mkdirs();
			temp = new File(strFilePath, "sample.txt");
			os = new FileOutputStream(temp);
			os.close();
			temp.delete();
			return true;
		} catch (IOException e) {
			return false;
		} finally {
			temp = null;
			os = null;
		}
	}

	public static String getCxErrorMessage(String errorCode) {
		String errorMessage = null;

		if (errorCode.equals(PortalErrors.SYSTEM_EXCEPTION)) {
			errorMessage = PortalMessages.SYSTEM_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.PORTAL_EXCEPTION)) {
			errorMessage = PortalMessages.PORTAL_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.PARSE_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.ORM_EXCEPTION)) {
			errorMessage = PortalMessages.ORM_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.JSON_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.NUMBER_FORMAT_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.READONLY_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.UNHANDLED_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.IO_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.GET_PROJECT_USERS_PROJECT_ID_INVALID)) {
			errorMessage = PortalMessages.PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_USER)) {
			errorMessage = PortalMessages.USER_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT_TYPE)) {
			errorMessage = PortalMessages.PROJECT_TYPE_NOT_CX;
		} else if (errorCode.equals(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID)) {
			errorMessage = PortalMessages.PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.COMPLETE_PROJECT_FAILED)) {
			errorMessage = PortalMessages.COMPLETE_PROJECT_FAILED;
		} else if (errorCode.equals(PortalErrors.INVALID_CASE_NUMBER)) {
			errorMessage = PortalMessages.CASE_NUMBER_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT)) {
			errorMessage = PortalMessages.PROJECT_INVALID;
		} else if (errorCode.equals(PortalErrors.NO_PROJECT_USERS)) {
			errorMessage = PortalMessages.NO_PROJECT_USERS;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT_ID)) {
			errorMessage = PortalMessages.PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_SESSION_ID)) {
			errorMessage = PortalMessages.CX_SESSION_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_SCAN_ID)) {
			errorMessage = PortalMessages.SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_REPORT_ID)) {
			errorMessage = PortalMessages.REPORT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_SCAN_REGISTRATION_DATE)) {
			errorMessage = PortalMessages.SCAN_REGISTRATION_DATE_INVALID;
		} else if (errorCode.equals(PortalErrors.REQUEST_REVIEW_FAILED)) {
			errorMessage = PortalMessages.REQUEST_REVIEW_FAILED;
		} else if (errorCode.equals(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.CANCEL_REVIEW_FAILED)) {
			errorMessage = PortalMessages.CANCEL_REVIEW_FAILED;
		} else if (errorCode.equals(PortalErrors.INVALID_SCAN)) {
			errorMessage = PortalMessages.SCAN_INVALID;
		} else if (errorCode.equals(PortalErrors.DELETE_REPORT_FAILED)) {
			errorMessage = PortalMessages.DELETE_REPORT_FAILED;
		} else if (errorCode.equals(PortalErrors.DELETE_SCAN_FAILED)) {
			errorMessage = PortalMessages.DELETE_SCAN_FAILED;
		} else if (errorCode.equals(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.STOP_SCAN_SCAN_ID_INVALID)) {
			errorMessage = PortalMessages.SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.REEXECUTE_SCAN_SCAN_ID_INVALID)) {
			errorMessage = PortalMessages.SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.REQUEST_REVIEW_SCAN_ID_INVALID)) {
			errorMessage = PortalMessages.SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.CANCEL_REVIEW_SCAN_ID_INVALID)) {
			errorMessage = PortalMessages.SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.REGENERATE_REPORT_SCAN_ID_INVALID)) {
			errorMessage = PortalMessages.SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.STOP_SCAN_FAILED)) {
			errorMessage = PortalMessages.STOP_SCAN_FAILED;
		} else if (errorCode.equals(PortalErrors.REEXECUTE_SCAN_FAILED)) {
			errorMessage = PortalMessages.REEXECUTE_SCAN_FAILED;
		} else if (errorCode.equals(PortalErrors.REGENERATE_REPORT_FAILED)) {
			errorMessage = PortalMessages.REGENERATE_REPORT_FAILED;
		} else if (errorCode.equals(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT_END_DATE)) {
			errorMessage = PortalMessages.PROJECT_END_DATE_INVALID;
		} else {
			errorMessage = errorCode;
		}

		return errorMessage;
	}

	public static String getAndroidErrorMessage(String errorCode) {
		String errorMessage = null;

		if (errorCode.equals(PortalErrors.ORM_EXCEPTION)) {
			errorMessage = PortalMessages.ORM_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.JSON_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.PORTAL_EXCEPTION)) {
			errorMessage = PortalMessages.PORTAL_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.SYSTEM_EXCEPTION)) {
			errorMessage = PortalMessages.SYSTEM_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.NUMBER_FORMAT_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.UNHANDLED_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.READONLY_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.IO_EXCEPTION)) {
			errorMessage = PortalMessages.COMMON_EXCEPTION;
		} else if (errorCode.equals(PortalErrors.INVALID_USER)) {
			errorMessage = PortalMessages.USER_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT_ID)) {
			errorMessage = PortalMessages.PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT_TYPE)) {
			errorMessage = PortalMessages.PROJECT_TYPE_NOT_ANDROID;
		} else if (errorCode.equals(PortalErrors.INVALID_CASE_NUMBER)) {
			errorMessage = PortalMessages.CASE_NUMBER_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT_NAME)) {
			errorMessage = PortalMessages.PROJECT_NAME_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT_STATUS)) {
			errorMessage = PortalMessages.PROJECT_STATUS_INVALID;
		} else if (errorCode.equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)) {
			errorMessage = PortalMessages.PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
			errorMessage = PortalMessages.PROJECT_TYPE_NOT_ANDROID;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT)) {
			errorMessage = PortalMessages.PROJECT_INVALID;
		} else if (errorCode.equals(PortalErrors.NO_PROJECT_USERS)) {
			errorMessage = PortalMessages.NO_PROJECT_USERS;
		} else if (errorCode.equals(PortalErrors.INVALID_FILE)) {
			errorMessage = PortalMessages.FILE_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID)) {
			errorMessage = PortalMessages.CX_ANDROID_PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_ID_INVALID)) {
			errorMessage = PortalMessages.PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.USER_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID)) {
			errorMessage = PortalMessages.PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.NO_SUCH_PROJECT_EXCEPTION)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.COMPLETE_PROJECT_FAILED)) {
			errorMessage = PortalMessages.COMPLETE_PROJECT_FAILED;
		} else if (errorCode.equals(PortalErrors.INVALID_SCAN_ID)) {
			errorMessage = PortalMessages.SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_REPORT_ID)) {
			errorMessage = PortalMessages.REPORT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_REPORT_TYPE)) {
			errorMessage = PortalMessages.REPORT_TYPE_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_SCAN_REGISTRATION_DATE)) {
			errorMessage = PortalMessages.SCAN_REGISTRATION_DATE_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_SCAN)) {
			errorMessage = PortalMessages.SCAN_INVALID;
		} else if (errorCode.equals(PortalErrors.ADD_SCAN_FAILED)) {
			errorMessage = PortalMessages.ADD_SCAN_FAILED;
		} else if (errorCode.equals(PortalErrors.INVALID_CX_ANDROID_SCAN_ID)) {
			errorMessage = PortalMessages.CX_ANDROID_SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.NO_SUCH_SCAN_EXCEPTION)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.INVALID_SESSION_ID)) {
			errorMessage = PortalMessages.CX_SESSION_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.STOP_SCAN_FAILED)) {
			errorMessage = PortalMessages.STOP_SCAN_FAILED;
		} else if (errorCode.equals(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.STOP_SCAN_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.REEXECUTE_SCAN_PROJECT_TYPE_INVALID)) {
			errorMessage = PortalMessages.PROJECT_TYPE_NOT_ANDROID;
		} else if (errorCode.equals(PortalErrors.REEXECUTE_SCAN_SCAN_ID_INVALID)) {
			errorMessage = PortalMessages.SCAN_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.REEXECUTE_SCAN_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.REEXECUTE_SCAN_FAILED)) {
			errorMessage = PortalMessages.REEXECUTE_SCAN_FAILED;
		} else if (errorCode.equals(PortalErrors.INVALID_FILE_PATH)) {
			errorMessage = PortalMessages.FILE_PATH_INVALID;
		} else if (errorCode.equals(PortalErrors.DELETE_SCAN_FAILED)) {
			errorMessage = PortalMessages.DELETE_SCAN_FAILED;
		} else if (errorCode.equals(PortalErrors.DELETE_REPORT_FAILED)) {
			errorMessage = PortalMessages.DELETE_REPORT_FAILED;
		} else if (errorCode.equals(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.SCAN_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST)) {
			errorMessage = PortalMessages.PROJECT_DOES_NOT_EXIST;
		} else if (errorCode.equals(PortalErrors.GET_PROJECT_USERS_PROJECT_ID_INVALID)) {
			errorMessage = PortalMessages.PROJECT_ID_INVALID;
		} else if (errorCode.equals(PortalErrors.INVALID_PROJECT_END_DATE)) {
			errorMessage = PortalMessages.PROJECT_END_DATE_INVALID;
		} else {
			errorMessage = errorCode;
		}

		return errorMessage;
	}
	
	public static boolean isValidDate(String strDate) {
		boolean isValid = true;
		DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
		try {
			format.parse(strDate);
		} catch (ParseException ps) {
			isValid = false;
		}
		return isValid;
	}
	
	/**
	 * @param origString the string to escape
	 * @return escaped string
	 */
	public static String escapeJsPartOfString(String origString) {
		return escape(escape(escape(origString, "javascript"), "<script>"), "</script>");
	}
	
	/**
	 * @param origString original string to escape
	 * @param strToScape the string to search
	 * @return escaped string
	 */
	private static String escape(String origString, String strToScape) {
		String escapedStrng = StringPool.BLANK;
		String subString = origString;
		String DOUBLE_BACKSLASH = "\\\\";
		int length = strToScape.length();
		int index = subString.toLowerCase().indexOf(strToScape);
		int startIndex = 0;
		
		if (index == -1) {
			return origString;
		}

		do {

			index += startIndex;
			escapedStrng += origString.substring(startIndex, index) + DOUBLE_BACKSLASH
					+ origString.substring(index, index + length) + DOUBLE_BACKSLASH;
			subString = origString.substring(index + length);
			startIndex = index + length;
			index = subString.toLowerCase().indexOf(strToScape);

		} while (index != -1);

		return escapedStrng += subString;
	}
	
	// TODO: create another class
	/*
	 * public static String convertHexToString (String hexValue) { Map<String,
	 * Object> params = new HashMap<String, Object>(); String result =
	 * PortalConstants.STRING_EMPTY; Hex hexString = new Hex(); int i = 0;
	 * String temp = PortalConstants.STRING_EMPTY; String converted =
	 * PortalConstants.STRING_EMPTY;
	 * 
	 * try { while (i < hexValue.length()) { if (hexValue.charAt(i) ==
	 * PortalConstants.CHAR_PERCENT) { converted += hexValue.substring(i+1,
	 * (i+3)); i += 3; } else { temp = hexValue.charAt(i) +
	 * PortalConstants.STRING_EMPTY; converted += new
	 * String(hexString.encode(temp.getBytes())); i++; } }
	 * 
	 * result = new String(hexString.decode(converted.getBytes())); } catch
	 * (DecoderException e) { params.put("hexValue", hexValue);
	 * log.debug(PortalErrors.DECODER_EXCEPTION,
	 * PortalConstants.METHOD_CONVERT_HEX_TO_STRING, params, e); }
	 * 
	 * return result; }
	 * 
	 * public static boolean isDBConnected () throws UBSPortalException {
	 * boolean bConnected = false; Connection conn = null;
	 * 
	 * try { conn = DataAccess.getConnection();
	 * 
	 * if (conn == null) { throw new
	 * UBSPortalException(PortalErrors.ORM_EXCEPTION,
	 * PortalMessages.ORM_EXCEPTION); }
	 * 
	 * Statement statement = conn.createStatement(); statement.executeQuery(
	 * "SELECT * FROM user_ LIMIT 1");
	 * 
	 * bConnected = true; conn.close(); } catch (SQLException e) {
	 * log.debug(PortalErrors.ORM_EXCEPTION,
	 * PortalConstants.METHOD_CHECK_DB_CONNECTION, null, e); throw new
	 * UBSPortalException(PortalErrors.ORM_EXCEPTION,
	 * PortalMessages.ORM_EXCEPTION, e); } catch (UBSPortalException e) {
	 * log.debug(PortalErrors.ORM_EXCEPTION,
	 * PortalConstants.METHOD_CHECK_DB_CONNECTION, null, e); throw new
	 * UBSPortalException(PortalErrors.ORM_EXCEPTION,
	 * PortalMessages.ORM_EXCEPTION, e); }
	 * 
	 * return bConnected; }
	 */
}
