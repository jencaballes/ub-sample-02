package jp.ubsecure.portal.jubjub.portlet.util;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Used to retrieve properties from config file in the order the properties
 * are listed 
 *
 */
public class OrderedProperties extends Properties {
	private static final long serialVersionUID = 1L;
	
	public static void testMethod() {
		System.out.println("OrderedProperties test");
	}
	
	private Map<Object, Object> linkMap = new LinkedHashMap<Object, Object>();
	
	public void clear () {
		linkMap.clear();
	}
	
	public boolean contains (Object value) {
		return linkMap.containsValue(value);
	}
	
	public boolean containsKey (Object key) {
		return linkMap.containsKey(key);
	}
	
	public boolean containsValue (Object value) {
		return linkMap.containsValue(value);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Enumeration elements () {
		throw new RuntimeException("Method elements is not supported in OrderedProperties class");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Set entrySet () {
		return linkMap.entrySet();
	}
	
	public boolean equals (Object o) {
		return linkMap.equals(o);
	}
	
	public Object get (Object key) {
		return linkMap.get(key);
	}
	
	public String getProperty (String key) {
		Object oval = get(key);
		
		if (oval == null) {
			return null;
		}

		return (oval instanceof String) ? (String) oval : null;
	}
	
	public boolean isEmpty () {
		return linkMap.isEmpty();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Enumeration keys () {
		Set keys = linkMap.keySet();
		return Collections.enumeration(keys);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Set keySet () {
		return linkMap.keySet();
	}
	
	public void list (PrintStream out) {
		this.list(new PrintWriter(out, true));
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void list (PrintWriter out) {
		for (Map.Entry e : (Set<Map.Entry>)this.entrySet()) {
			String key = (String) e.getKey();
			String val = (String) e.getValue();
			
			if (val.length() > 40) {
				val = val.substring(0, 37) + "...";
			}
			out.println(key + " = " + val);
		}
	}
	
	public Object put (Object key, Object value) {
		return linkMap.put(key, value);
	}
	
	public int size () {
		return linkMap.size();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Collection values () {
		return linkMap.values();
	}
}
