package jp.ubsecure.portal.jubjub.portlet.util;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;

public class Encrypter {
	private static String algorithm = "DES";
	private static Key key = null;
	private static Cipher cipher = null;
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(Encrypter.class);
	
	private static void setUp() throws Exception {
		key = KeyGenerator.getInstance(algorithm).generateKey();
		cipher = Cipher.getInstance(algorithm);
	}
	
	public static void testMethod() {
		System.out.println("Encrypter test");
		log.info("Encrypter", "test");
	}
	
	public static byte [] encrypt (String input) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		byte [] bytes = null;
		try {
			setUp();
			
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] inputBytes = Base64.encodeBase64(input.getBytes());
			bytes = cipher.doFinal(inputBytes);
		} catch (Exception e) {
			if (e instanceof NoSuchAlgorithmException) {
				log.debug(PortalErrors.ALGORITHM_DOES_NOT_EXIST, PortalConstants.METHOD_ENCRYPT, null, e);
			} else if (e instanceof NoSuchPaddingException) {
				log.debug(PortalErrors.PADDING_DOES_NOT_EXIST, PortalConstants.METHOD_ENCRYPT, null, e);
			} else if (e instanceof InvalidKeyException) {
				log.debug(PortalErrors.KEY_INVALID, PortalConstants.METHOD_ENCRYPT, null, e);
			} else if (e instanceof BadPaddingException) {
				log.debug(PortalErrors.PADDING_BAD, PortalConstants.METHOD_ENCRYPT, null, e);
			} else if (e instanceof IllegalBlockSizeException) {
				log.debug(PortalErrors.BLOCK_SIZE_ILLEGAL, PortalConstants.METHOD_ENCRYPT, null, e);
			}
		}
		
		return bytes;
	}
	
	public static String decrypt (String encryptedData, Key encKey) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		String recovered = PortalConstants.STRING_EMPTY;
		
		try {
			cipher = Cipher.getInstance(algorithm);
			cipher.init(Cipher.DECRYPT_MODE, encKey);
			byte [] encryptionBytes = Base64.decodeBase64(encryptedData.getBytes());
			byte [] recoveredBytes = cipher.doFinal(encryptionBytes);
			recovered = new String(recoveredBytes, "UTF8");
		} catch (Exception e) {
			if (e instanceof NoSuchAlgorithmException) {
				log.debug(PortalErrors.ALGORITHM_DOES_NOT_EXIST, PortalConstants.METHOD_DECRYPT, null, e);
			} else if (e instanceof NoSuchPaddingException) {
				log.debug(PortalErrors.PADDING_DOES_NOT_EXIST, PortalConstants.METHOD_DECRYPT, null, e);
			} else if (e instanceof InvalidKeyException) {
				log.debug(PortalErrors.KEY_INVALID, PortalConstants.METHOD_DECRYPT, null, e);
			} else if (e instanceof BadPaddingException) {
				log.debug(PortalErrors.PADDING_BAD, PortalConstants.METHOD_DECRYPT, null, e);
			} else if (e instanceof IllegalBlockSizeException) {
				log.debug(PortalErrors.BLOCK_SIZE_ILLEGAL, PortalConstants.METHOD_DECRYPT, null, e);
			}
		}
		
		return recovered;
	}
	
	public static boolean isEncrypted (String encryptedData, Key encKey) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, Exception {
		boolean bIsEncrypted = true;
		
		try {
			cipher = Cipher.getInstance(algorithm);
			cipher.init(Cipher.DECRYPT_MODE, encKey);
			byte [] encryptionBytes = Base64.decodeBase64(encryptedData.getBytes());
			cipher.doFinal(encryptionBytes);
		} catch (Exception e) {
			bIsEncrypted = false;
			if (e instanceof NoSuchAlgorithmException) {
				log.debug(PortalErrors.ALGORITHM_DOES_NOT_EXIST, PortalConstants.METHOD_DECRYPT, null, e);
			} else if (e instanceof NoSuchPaddingException) {
				log.debug(PortalErrors.PADDING_DOES_NOT_EXIST, PortalConstants.METHOD_DECRYPT, null, e);
			} else if (e instanceof InvalidKeyException) {
				log.debug(PortalErrors.KEY_INVALID, PortalConstants.METHOD_DECRYPT, null, e);
			} else if (e instanceof BadPaddingException) {
				log.debug(PortalErrors.PADDING_BAD, PortalConstants.METHOD_DECRYPT, null, e);
			} else if (e instanceof IllegalBlockSizeException) {
				log.debug(PortalErrors.BLOCK_SIZE_ILLEGAL, PortalConstants.METHOD_DECRYPT, null, e);
			}
		}
		
		return bIsEncrypted;
	}
}
