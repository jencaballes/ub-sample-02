package jp.ubsecure.portal.jubjub.portlet.xml;

import java.util.ArrayList;
import java.util.List;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class for XML parsing
 * 
 * @author ASI558
 *
 */
public class XmlHelper {

	public static void testMethod() {
		System.out.println("XmlHelper test");
	}
	
	/**
	 * @param tagName tag name to look for
	 * @param nodes nodes where the tag name to be searched
	 * @return node value where the tag name is located
	 */
	public static Node getNode(String tagName, NodeList nodes) {
		int nodeLength = 0;
		if (!CommonUtil.isNodeListNullOrEmpty(nodes)) {
			nodeLength = nodes.getLength();
		    for ( int x = 0; x < nodeLength; x++ ) {
		        Node node = nodes.item(x);
		        if (!CommonUtil.isNodeNull(node) && !CommonUtil.isStringNullOrEmpty(tagName)) {
		        	if (tagName.equalsIgnoreCase(node.getNodeName())) {
			            return node;
			        }
		        }
		        
		    }
		}
	    return null;
	}
	
	/**
	 * get the corresponding value of a node given the tag name
	 * @param tagName the tag name to look for
	 * @param nodes the node
	 * @return the value of the node
	 */
	public static String getNodeValue(String tagName, NodeList nodes ) {
		int length = 0;
		int childNodeLength = 0;
		if (!CommonUtil.isNodeListNullOrEmpty(nodes)) {
			length = nodes.getLength();
		    for ( int x = 0; x < length; x++ ) {
		        Node node = nodes.item(x);
		        if (!CommonUtil.isNodeNull(node)) {
		        	if (node.getNodeName().equalsIgnoreCase(tagName)) {
			            NodeList childNodes = node.getChildNodes();
			            if (!CommonUtil.isNodeListNullOrEmpty(childNodes)) {
			            	childNodeLength = childNodes.getLength();
				            for (int y = 0; y < childNodeLength ; y++ ) {
				                Node data = childNodes.item(y);
				                if (!CommonUtil.isNodeNull(data) && data.getNodeType() == Node.TEXT_NODE )
				                    return data.getNodeValue();
				            }
			            }
			        }
		        }
		    }
		}
		
		
	    return "";
	}
	 
	/**
	 * get the attribute value of a specific attribute
	 * @param attrName the attribute name to search
	 * @param node the node where attribute name be searched
	 * @return the attribute value
	 */
	public static String getNodeAttr(String attrName, Node node ) {
		if (!CommonUtil.isNodeNull(node)) {
			NamedNodeMap attrs = node.getAttributes();
			if (attrs != null) {
				int length = attrs.getLength();
			    for (int y = 0; y < length; y++ ) {
			        Node attr = attrs.item(y);
			        if (!CommonUtil.isNodeNull(attr) && attr.getNodeName().equalsIgnoreCase(attrName)) {
			            return attr.getNodeValue();
			        }
			    }
			}
		}
	    return "";
	}
	
	/**
	 * returns the list of nodes given a specific tag to look for
	 * @param tag the specific tag to look for
	 * @param nodeList the nodelist to look for
	 * @return
	 */
	public static List<Node> getNodeList(String tag, NodeList nodeList) {
		List<Node> resultList = null;
		int length = 0;
		Node resultNode = null;
		String resultNodeName = null;
		if (!CommonUtil.isNodeListNullOrEmpty(nodeList)) {
			resultList = new ArrayList<Node>();
			length = nodeList.getLength();
			for (int j = 0; j < length; j++) {
				resultNode = nodeList.item(j);
				if (!CommonUtil.isNodeNull(resultNode)) {
					resultNodeName = resultNode.getNodeName().trim();
					if (tag.equalsIgnoreCase(resultNodeName)) { 
						resultList.add(resultNode);
					}
				}	
			}	
		}
		return resultList;
	}
	
	/**
	 * returns the list of result nodes given a specific tag to look for
	 * @param tag the specific tag to look for
	 * @param nodeList the result nodelist to look for with state = 1 
	 * @return
	 */
	public static List<Node> getResultList(String tag, NodeList nodeList) {
		List<Node> resultList = null;
		int length = 0;
		Node resultNode = null;
		String resultNodeName = null;
		String state = null;
		if (!CommonUtil.isNodeListNullOrEmpty(nodeList)) {
			resultList = new ArrayList<Node>();
			length = nodeList.getLength();
			for (int j = 0; j < length; j++) {
				resultNode = nodeList.item(j);
				if (!CommonUtil.isNodeNull(resultNode)) {
					resultNodeName = resultNode.getNodeName().trim();
					if (tag.equalsIgnoreCase(resultNodeName)) { 
						state = getNodeAttr(PortalConstants.TAG_ATTRIB_STATE, resultNode);
						if (!CommonUtil.isStringNullOrEmpty(state) && !state.equals(PortalConstants.EXCLUDED_STATE)) { //exclude state = 1
							resultList.add(resultNode);
						}
					}
				}	
			}	
		}
		return resultList;
	}
}
