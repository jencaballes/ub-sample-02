package jp.ubsecure.portal.jubjub.portlet.log;

import java.io.File;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PropsUtil;

/**
 * UBSPortalDebugger Logger Implementation
 * 
 * @category Log
 * @since 12/09/2014
 */
public class UBSPortalDebugger {
	
	/**
	 * log4j.xml path
	 */
	private static String LOG4J_PATH = "";
	private static String DEFAULT_LOG4J_PATH = "/usr/local/liferay/data/ubsportal-resource/log4j.xml";
	private static String KEY_CONFIG_BASE_PATH = "resource.base.path";
	
	/** load log4j.xml */
	static {
		LOG4J_PATH = getBasePath(KEY_CONFIG_BASE_PATH);
		DOMConfigurator.configure(LOG4J_PATH);
	}

	private Logger logger;

	/** Long zero constants */
	private static final long LONG_ZERO = 0L;

	/** Batch string constants */
	private static final String BATCH = "Batch";

	/** Portal exception constants */
	private static final String PORTAL_EXCEPTION = "S.PortalAPI.PortalDBError.0003";

	/** System exception constants */
	public static final String SYSTEM_EXCEPTION = "S.PortalAPI.PortalDBError.0002";

	/**
	 * UBSPortalDebugger constructor with class parameter
	 * 
	 * @param c
	 *            Class files
	 */
	private UBSPortalDebugger(Class<?> c) {
		logger = Logger.getLogger(c);
	}

	/**
	 * UBSPortalDebugger constructor with string parameter
	 * 
	 * @param strLogger
	 */
	private UBSPortalDebugger(String strLogger) {
		logger = Logger.getLogger(strLogger);
	}

	/**
	 * Create UBSPortalDebugger instance
	 * 
	 * @param c
	 *            class
	 * @return new UBSPortalDebugger instance
	 */
	public static UBSPortalDebugger getInstance(Class<?> c) {
		return new UBSPortalDebugger(c);
	}

	/**
	 * Create UBSPortalDebugger instance
	 * 
	 * @param strLogger
	 * @return new UBSPortalDebugger instance
	 */
	public static UBSPortalDebugger getInstance(String strLogger) {
		return new UBSPortalDebugger(strLogger);
	}

	/**
	 * Info log method
	 * 
	 * @param strUserAction
	 * @param lUserId
	 */
	public void info(String strUserAction, long lUserId) {
		if (lUserId <= LONG_ZERO) {
			logger.info("[ USER ACTION : " + strUserAction + " ] [ USER ID : N/A ]");
		} else {
			logger.info("[ USER ACTION : " + strUserAction + " ] [ USER ID : " + lUserId + " ]");
		}
	}

	/**
	 * Info log method
	 * 
	 * @param strAPIFunction
	 * @param strMessage
	 */
	public void info(String strAPIFunction, String strMessage) {
		logger.info("[ " + strAPIFunction + " : " + strMessage + " ]");
	}
	
	/**
	 * Info log method for successful or failed login
	 * 
	 * @param strUserAction
	 * @param lUserId
	 * @param status
	 */
	public void info(String strUserAction, long lUserId, String status) {
		if (lUserId <= LONG_ZERO) {
			logger.info("[ USER ACTION : " + strUserAction + " ] [ USER ID : N/A ] [ STATUS : " + status + " ]");
		} else {
			logger.info("[ USER ACTION : " + strUserAction + " ] [ USER ID : " + lUserId + " ] [ STATUS : " + status + " ]");
		}
	}
	
	public void info(String strUserAction, long lUserId, long scanId)  {
		if (lUserId <= LONG_ZERO || scanId <= LONG_ZERO) {
			if (lUserId <= LONG_ZERO && scanId <= LONG_ZERO) {
				logger.info("[ USER ACTION : " + strUserAction + " ] [ USER ID : N/A ] [ SCAN ID : N/A ]");
			} else if (scanId <= LONG_ZERO) {
				logger.info("[ USER ACTION : " + strUserAction + " ] [ USER ID : " + lUserId + " ] [ SCAN ID : N/A ]");
			} else if (lUserId <= LONG_ZERO) {
				logger.info("[ USER ACTION : " + strUserAction + " ] [ USER ID : N/A ] [ SCAN ID : " + scanId + " ]");
			}
		} else {
			logger.info("[ USER ACTION : " + strUserAction + " ] [ USER ID : " + lUserId + " ] [ SCAN ID : " + scanId + " ]");
		}
	}

	/**
	 * Debug log method
	 * 
	 * @param strErrorCode
	 * @param strMethodName
	 * @param params
	 * @param cause
	 */
	public void debug(String strErrorCode, String strMethodName, Map<String, Object> params, Throwable cause) {
		String strException = null;
		if (null == cause.getCause()) {
			strException = cause.getClass().getCanonicalName();
		} else {
			strException = cause.getCause().getClass().getCanonicalName();
		}

		if (isStringNullOrEmpty(strErrorCode) && cause instanceof PortalException) {
			strErrorCode = PORTAL_EXCEPTION;
		} else if (isStringNullOrEmpty(strErrorCode) && cause instanceof SystemException) {
			strErrorCode = SYSTEM_EXCEPTION;
		}

		logger.error("[ " + strErrorCode + " ]" + " Method Name: " + strMethodName + " Cause: " + strException
				+ " Params: { " + toStringParams(params) + " }", cause);
	}

	/**
	 * Get logger
	 * 
	 * @return Logger
	 */
	public Logger getLogger() {
		return this.logger;
	}

	/**
	 * Batch info log method
	 * 
	 * @param strMessage
	 */
	public void batchInfo(String strMessage) {
		if (logger.getName().equals(BATCH)) {
			logger.info(strMessage);
		}
	}

	/**
	 * Warn log method
	 * 
	 * @param strErrorCode
	 * @param strMethodName
	 * @param params
	 * @param cause
	 */
	public void warn(String strErrorCode, String strMethodName, Map<String, Object> params, Throwable cause) {
		String strException = null;

		if (null != cause) {
			if (null == cause.getCause()) {
				strException = cause.getClass().getCanonicalName();
			} else {
				strException = cause.getCause().getClass().getCanonicalName();
			}

			if (isStringNullOrEmpty(strErrorCode) && cause instanceof PortalException) {
				strErrorCode = PORTAL_EXCEPTION;
			} else if (isStringNullOrEmpty(strErrorCode) && cause instanceof SystemException) {
				strErrorCode = SYSTEM_EXCEPTION;
			}

			logger.warn("[ " + strErrorCode + " ]" + " Method Name: " + strMethodName + " Cause: " + strException
					+ " Params: { " + toStringParams(params) + " }", cause);
		} else {
			logger.warn("[ " + strErrorCode + " ]" + " Method Name: " + strMethodName + " Params: { "
					+ toStringParams(params) + " }");
		}
	}

	/**
	 * ToString method
	 * 
	 * @param params
	 * @return
	 */
	private String toStringParams(Map<String, Object> params) {
		StringBuilder builder = new StringBuilder();
		if (!isMapNullOrEmpty(params)) {
			Set<String> keySet = params.keySet();
			builder = new StringBuilder();
			for (String key : keySet) {
				builder.append(key + " = " + params.get(key) + ", ");
			}
		} else {
			builder.append("N/A");
		}
		String strParams = builder.toString();
		builder = null;

		return strParams;
	}
	
	/**
	 * Check ig string is empty
	 * 
	 * @param strString
	 * @return return true if string is empty, else return false
	 */
	private boolean isStringNullOrEmpty(String strString) {
		return strString == null || strString.isEmpty();
	}

	/**
	 * Check if map is emptyF
	 * 
	 * @param map
	 * @return return true if map is empty, else return false
	 */
	private boolean isMapNullOrEmpty(Map<?, ?> map) {
		return map == null || map.isEmpty();
	}
	
	private static String getBasePath(String configKey){
		String configValue = DEFAULT_LOG4J_PATH;
		try {
			configValue = PropsUtil.get(configKey) + File.separator + "log4j.xml";
		}  catch (Exception e){
			//do nothing
		}
		
		return configValue;
	}
}