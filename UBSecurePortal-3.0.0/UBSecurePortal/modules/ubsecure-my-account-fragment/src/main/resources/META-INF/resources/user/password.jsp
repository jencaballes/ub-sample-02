<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>

<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Set" %>

<%
User selUser = (User)request.getAttribute("user.selUser");

PasswordPolicy passwordPolicy = (PasswordPolicy)request.getAttribute("user.passwordPolicy");

boolean passwordReset = false;
boolean passwordResetDisabled = false;

if (((selUser == null) || (selUser.getLastLoginDate() == null)) && ((passwordPolicy == null) || (passwordPolicy.isChangeable() && passwordPolicy.isChangeRequired()))) {
	passwordReset = true;
	passwordResetDisabled = true;
}
else {
	passwordReset = BeanParamUtil.getBoolean(selUser, request, "passwordReset");

	if ((passwordPolicy != null) && !passwordPolicy.isChangeable()) {
		passwordResetDisabled = true;
	}
}

boolean bShowDBConnError = false;

try {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	bShowDBConnError = true;
}
%>

<style>
	.password-field {
		height: 31px;
		border-radius: 0;
		font-size: 9px;
		width: 253px;
		padding: 6px 12px;
	}
</style>

<liferay-ui:error-marker key="<%= WebKeys.ERROR_SECTION %>" value="password" />

<aui:model-context bean="<%= selUser %>" model="<%= User.class %>" />

<liferay-ui:success key="successPwdEdit" message="success-password-edit" />

<%
if (SessionErrors.contains(renderRequest, UserPasswordException.MustMatchCurrentPassword.class)
		|| SessionErrors.contains(renderRequest, UserPasswordException.MustNotBeNull.class)
		|| SessionErrors.contains(renderRequest, UserPasswordException.MustBeLonger.class)) {
	%>
	<div class="alert alert-danger">
		<liferay-ui:message key="error-password-invalid" />
	</div>
	<%
} else if (SessionErrors.contains(renderRequest, UserPasswordException.MustMatch.class)) {
	%>
	<div class="alert alert-danger">
		<liferay-ui:message key="error-password-did-not-match" />
	</div>
	<%
} else if (SessionErrors.contains(renderRequest, UserPasswordException.MustNotBeEqualToCurrent.class)) {
	%>
	<div class="alert alert-danger">
		<liferay-ui:message key="error-new-password-same-as-old-password" />
	</div>
	<%
}
%>

<aui:fieldset>

	<!-- Begin LPS-38289 and LPS-55993 and LPS-61876 -->

	<input class="hide" type="password" />
	<input class="hide" type="password" />

	<!-- End LPS-38289 and LPS-55993 and LPS-61876 -->
	
	<table>
		
		<tr>
			 <td style="padding: 10px 20px 10px 0px">	
                    <liferay-ui:message key="label-user-name"></liferay-ui:message>			            
            </td>	
			<td style="padding: 10px 0px 10px 0px">		            			            
              <%--  <aui:input type="hidden" inlineLabel="left"  name="screenName"  value="<%= selUser.getFirstName() %>"/> --%>
				<liferay-ui:message key="<%= selUser.getFirstName() %>"></liferay-ui:message>	
			</td>	
		</tr>
		
		<c:if test="<%= portletName.equals(myAccountPortletId) %>">	
			<tr>
	            <td style="padding: 10px 20px 10px 0px">	
	               <liferay-ui:message key="label-old-password"></liferay-ui:message>
	    		</td>					          
	            <td style="padding: 10px 0px 10px 0px">		            			            
	                <aui:input autocomplete="off" label="" name="password0" size="30" type="password" cssClass="password-field" />	
	            </td>		
			</tr>
		</c:if> 
		
		<tr>
            <td style="padding: 10px 20px 10px 0px">	
                <liferay-ui:message key="label-new-password"></liferay-ui:message>
    	    </td>					          
            <td style="padding: 10px 0px 10px 0px">		            			            
                <aui:input autocomplete="off" label="" name="password1" size="30" type="password" cssClass="password-field" />
            </td>		
		</tr>
		
		<tr>
            <td style="padding: 10px 20px 10px 0px">	
                <liferay-ui:message key="label-new-password-retype"></liferay-ui:message>
    		</td>					          
            <td style="padding: 10px 0px 10px 0px">		            			            
                <aui:input autocomplete="off" label="" name="password2" size="30" type="password" cssClass="password-field">
				</aui:input>
            </td>		
		</tr>
		
		<c:if test="<%= (selUser == null) || (user.getUserId() != selUser.getUserId()) %>">
			<aui:input disabled="<%= passwordResetDisabled %>" label="password-reset-required" name="passwordReset" type="checkbox" value="<%= passwordReset %>" />
		</c:if>
	</table>
</aui:fieldset>
