/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;

/**
 * The persistence interface for the scan service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ScanPersistenceImpl
 * @see ScanUtil
 * @generated
 */
@ProviderType
public interface ScanPersistence extends BasePersistence<Scan> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ScanUtil} to access the scan persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the scans where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the matching scans
	*/
	public java.util.List<Scan> findByProjectId(long projectId);

	/**
	* Returns a range of all the scans where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of matching scans
	*/
	public java.util.List<Scan> findByProjectId(long projectId, int start,
		int end);

	/**
	* Returns an ordered range of all the scans where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching scans
	*/
	public java.util.List<Scan> findByProjectId(long projectId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns an ordered range of all the scans where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching scans
	*/
	public java.util.List<Scan> findByProjectId(long projectId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first scan in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public Scan findByProjectId_First(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Returns the first scan in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public Scan fetchByProjectId_First(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns the last scan in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public Scan findByProjectId_Last(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Returns the last scan in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public Scan fetchByProjectId_Last(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns the scans before and after the current scan in the ordered set where projectId = &#63;.
	*
	* @param scanId the primary key of the current scan
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next scan
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public Scan[] findByProjectId_PrevAndNext(long scanId, long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Removes all the scans where projectId = &#63; from the database.
	*
	* @param projectId the project ID
	*/
	public void removeByProjectId(long projectId);

	/**
	* Returns the number of scans where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the number of matching scans
	*/
	public int countByProjectId(long projectId);

	/**
	* Returns all the scans where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @return the matching scans
	*/
	public java.util.List<Scan> findByRunId(java.lang.String cxRunId);

	/**
	* Returns a range of all the scans where cxRunId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cxRunId the cx run ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of matching scans
	*/
	public java.util.List<Scan> findByRunId(java.lang.String cxRunId,
		int start, int end);

	/**
	* Returns an ordered range of all the scans where cxRunId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cxRunId the cx run ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching scans
	*/
	public java.util.List<Scan> findByRunId(java.lang.String cxRunId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns an ordered range of all the scans where cxRunId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cxRunId the cx run ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching scans
	*/
	public java.util.List<Scan> findByRunId(java.lang.String cxRunId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first scan in the ordered set where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public Scan findByRunId_First(java.lang.String cxRunId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Returns the first scan in the ordered set where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public Scan fetchByRunId_First(java.lang.String cxRunId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns the last scan in the ordered set where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public Scan findByRunId_Last(java.lang.String cxRunId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Returns the last scan in the ordered set where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public Scan fetchByRunId_Last(java.lang.String cxRunId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns the scans before and after the current scan in the ordered set where cxRunId = &#63;.
	*
	* @param scanId the primary key of the current scan
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next scan
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public Scan[] findByRunId_PrevAndNext(long scanId,
		java.lang.String cxRunId,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Removes all the scans where cxRunId = &#63; from the database.
	*
	* @param cxRunId the cx run ID
	*/
	public void removeByRunId(java.lang.String cxRunId);

	/**
	* Returns the number of scans where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @return the number of matching scans
	*/
	public int countByRunId(java.lang.String cxRunId);

	/**
	* Returns all the scans where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @return the matching scans
	*/
	public java.util.List<Scan> findByScanIdScanStatus(long scanId, int status);

	/**
	* Returns a range of all the scans where scanId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of matching scans
	*/
	public java.util.List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end);

	/**
	* Returns an ordered range of all the scans where scanId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching scans
	*/
	public java.util.List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns an ordered range of all the scans where scanId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching scans
	*/
	public java.util.List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first scan in the ordered set where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public Scan findByScanIdScanStatus_First(long scanId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Returns the first scan in the ordered set where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public Scan fetchByScanIdScanStatus_First(long scanId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns the last scan in the ordered set where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public Scan findByScanIdScanStatus_Last(long scanId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Returns the last scan in the ordered set where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public Scan fetchByScanIdScanStatus_Last(long scanId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Removes all the scans where scanId = &#63; and status = &#63; from the database.
	*
	* @param scanId the scan ID
	* @param status the status
	*/
	public void removeByScanIdScanStatus(long scanId, int status);

	/**
	* Returns the number of scans where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @return the number of matching scans
	*/
	public int countByScanIdScanStatus(long scanId, int status);

	/**
	* Returns all the scans where status = &#63;.
	*
	* @param status the status
	* @return the matching scans
	*/
	public java.util.List<Scan> findByScanStatus(int status);

	/**
	* Returns a range of all the scans where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of matching scans
	*/
	public java.util.List<Scan> findByScanStatus(int status, int start, int end);

	/**
	* Returns an ordered range of all the scans where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching scans
	*/
	public java.util.List<Scan> findByScanStatus(int status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns an ordered range of all the scans where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching scans
	*/
	public java.util.List<Scan> findByScanStatus(int status, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first scan in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public Scan findByScanStatus_First(int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Returns the first scan in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public Scan fetchByScanStatus_First(int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns the last scan in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public Scan findByScanStatus_Last(int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Returns the last scan in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public Scan fetchByScanStatus_Last(int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns the scans before and after the current scan in the ordered set where status = &#63;.
	*
	* @param scanId the primary key of the current scan
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next scan
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public Scan[] findByScanStatus_PrevAndNext(long scanId, int status,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator)
		throws NoSuchScanException;

	/**
	* Removes all the scans where status = &#63; from the database.
	*
	* @param status the status
	*/
	public void removeByScanStatus(int status);

	/**
	* Returns the number of scans where status = &#63;.
	*
	* @param status the status
	* @return the number of matching scans
	*/
	public int countByScanStatus(int status);

	/**
	* Caches the scan in the entity cache if it is enabled.
	*
	* @param scan the scan
	*/
	public void cacheResult(Scan scan);

	/**
	* Caches the scans in the entity cache if it is enabled.
	*
	* @param scans the scans
	*/
	public void cacheResult(java.util.List<Scan> scans);

	/**
	* Creates a new scan with the primary key. Does not add the scan to the database.
	*
	* @param scanId the primary key for the new scan
	* @return the new scan
	*/
	public Scan create(long scanId);

	/**
	* Removes the scan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanId the primary key of the scan
	* @return the scan that was removed
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public Scan remove(long scanId) throws NoSuchScanException;

	public Scan updateImpl(Scan scan);

	/**
	* Returns the scan with the primary key or throws a {@link NoSuchScanException} if it could not be found.
	*
	* @param scanId the primary key of the scan
	* @return the scan
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public Scan findByPrimaryKey(long scanId) throws NoSuchScanException;

	/**
	* Returns the scan with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param scanId the primary key of the scan
	* @return the scan, or <code>null</code> if a scan with the primary key could not be found
	*/
	public Scan fetchByPrimaryKey(long scanId);

	@Override
	public java.util.Map<java.io.Serializable, Scan> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the scans.
	*
	* @return the scans
	*/
	public java.util.List<Scan> findAll();

	/**
	* Returns a range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of scans
	*/
	public java.util.List<Scan> findAll(int start, int end);

	/**
	* Returns an ordered range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of scans
	*/
	public java.util.List<Scan> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator);

	/**
	* Returns an ordered range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of scans
	*/
	public java.util.List<Scan> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the scans from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of scans.
	*
	* @return the number of scans
	*/
	public int countAll();
}