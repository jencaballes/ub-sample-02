/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class VexScanSettingSoap implements Serializable {
	public static VexScanSettingSoap toSoapModel(VexScanSetting model) {
		VexScanSettingSoap soapModel = new VexScanSettingSoap();

		soapModel.setScanid(model.getScanid());
		soapModel.setStarturl(model.getStarturl());
		soapModel.setAuthorizedpatrolurl(model.getAuthorizedpatrolurl());
		soapModel.setUnauthorizedpatrolurl(model.getUnauthorizedpatrolurl());
		soapModel.setStarttime(model.getStarttime());
		soapModel.setEndtime(model.getEndtime());
		soapModel.setSetemailnotification(model.getSetemailnotification());
		soapModel.setSelectedwebsignature(model.getSelectedwebsignature());
		soapModel.setGetserverfiles(model.getGetserverfiles());
		soapModel.setGetserversettings(model.getGetserversettings());
		soapModel.setSelectedserverfilessignature(model.getSelectedserverfilessignature());
		soapModel.setSelectedserversettingssignature(model.getSelectedserversettingssignature());
		soapModel.setMaxnumdetectionlink(model.getMaxnumdetectionlink());
		soapModel.setMaxnumdetectionlinkperpage(model.getMaxnumdetectionlinkperpage());
		soapModel.setDetectionlinkdepthlimit(model.getDetectionlinkdepthlimit());
		soapModel.setWaittime(model.getWaittime());
		soapModel.setNumofthread(model.getNumofthread());
		soapModel.setTimeouttime(model.getTimeouttime());
		soapModel.setSetautopostform(model.getSetautopostform());
		soapModel.setSetpostmethod(model.getSetpostmethod());
		soapModel.setRequestheader(model.getRequestheader());
		soapModel.setLoginstatusdetection(model.getLoginstatusdetection());
		soapModel.setSetnotsendpassword(model.getSetnotsendpassword());
		soapModel.setParamnamesaspassword(model.getParamnamesaspassword());
		soapModel.setNumofretryaterror(model.getNumofretryaterror());
		soapModel.setSessionerrordetection(model.getSessionerrordetection());
		soapModel.setScreentransitionerrordetection(model.getScreentransitionerrordetection());
		soapModel.setImplementationenvironment(model.getImplementationenvironment());
		soapModel.setSetcrawlingonly(model.getSetcrawlingonly());
		soapModel.setExcludepathinforegex(model.getExcludepathinforegex());
		soapModel.setExcludeparameterdeletenameregex(model.getExcludeparameterdeletenameregex());
		soapModel.setExcludeparameterdeleteparamregex(model.getExcludeparameterdeleteparamregex());
		soapModel.setExtracturlregex(model.getExtracturlregex());
		soapModel.setNotparseextension(model.getNotparseextension());
		soapModel.setNotparsefilenameregex(model.getNotparsefilenameregex());

		return soapModel;
	}

	public static VexScanSettingSoap[] toSoapModels(VexScanSetting[] models) {
		VexScanSettingSoap[] soapModels = new VexScanSettingSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static VexScanSettingSoap[][] toSoapModels(VexScanSetting[][] models) {
		VexScanSettingSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new VexScanSettingSoap[models.length][models[0].length];
		}
		else {
			soapModels = new VexScanSettingSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static VexScanSettingSoap[] toSoapModels(List<VexScanSetting> models) {
		List<VexScanSettingSoap> soapModels = new ArrayList<VexScanSettingSoap>(models.size());

		for (VexScanSetting model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new VexScanSettingSoap[soapModels.size()]);
	}

	public VexScanSettingSoap() {
	}

	public long getPrimaryKey() {
		return _scanid;
	}

	public void setPrimaryKey(long pk) {
		setScanid(pk);
	}

	public long getScanid() {
		return _scanid;
	}

	public void setScanid(long scanid) {
		_scanid = scanid;
	}

	public String getStarturl() {
		return _starturl;
	}

	public void setStarturl(String starturl) {
		_starturl = starturl;
	}

	public String getAuthorizedpatrolurl() {
		return _authorizedpatrolurl;
	}

	public void setAuthorizedpatrolurl(String authorizedpatrolurl) {
		_authorizedpatrolurl = authorizedpatrolurl;
	}

	public String getUnauthorizedpatrolurl() {
		return _unauthorizedpatrolurl;
	}

	public void setUnauthorizedpatrolurl(String unauthorizedpatrolurl) {
		_unauthorizedpatrolurl = unauthorizedpatrolurl;
	}

	public Date getStarttime() {
		return _starttime;
	}

	public void setStarttime(Date starttime) {
		_starttime = starttime;
	}

	public Date getEndtime() {
		return _endtime;
	}

	public void setEndtime(Date endtime) {
		_endtime = endtime;
	}

	public int getSetemailnotification() {
		return _setemailnotification;
	}

	public void setSetemailnotification(int setemailnotification) {
		_setemailnotification = setemailnotification;
	}

	public String getSelectedwebsignature() {
		return _selectedwebsignature;
	}

	public void setSelectedwebsignature(String selectedwebsignature) {
		_selectedwebsignature = selectedwebsignature;
	}

	public int getGetserverfiles() {
		return _getserverfiles;
	}

	public void setGetserverfiles(int getserverfiles) {
		_getserverfiles = getserverfiles;
	}

	public int getGetserversettings() {
		return _getserversettings;
	}

	public void setGetserversettings(int getserversettings) {
		_getserversettings = getserversettings;
	}

	public String getSelectedserverfilessignature() {
		return _selectedserverfilessignature;
	}

	public void setSelectedserverfilessignature(
		String selectedserverfilessignature) {
		_selectedserverfilessignature = selectedserverfilessignature;
	}

	public String getSelectedserversettingssignature() {
		return _selectedserversettingssignature;
	}

	public void setSelectedserversettingssignature(
		String selectedserversettingssignature) {
		_selectedserversettingssignature = selectedserversettingssignature;
	}

	public int getMaxnumdetectionlink() {
		return _maxnumdetectionlink;
	}

	public void setMaxnumdetectionlink(int maxnumdetectionlink) {
		_maxnumdetectionlink = maxnumdetectionlink;
	}

	public int getMaxnumdetectionlinkperpage() {
		return _maxnumdetectionlinkperpage;
	}

	public void setMaxnumdetectionlinkperpage(int maxnumdetectionlinkperpage) {
		_maxnumdetectionlinkperpage = maxnumdetectionlinkperpage;
	}

	public int getDetectionlinkdepthlimit() {
		return _detectionlinkdepthlimit;
	}

	public void setDetectionlinkdepthlimit(int detectionlinkdepthlimit) {
		_detectionlinkdepthlimit = detectionlinkdepthlimit;
	}

	public int getWaittime() {
		return _waittime;
	}

	public void setWaittime(int waittime) {
		_waittime = waittime;
	}

	public int getNumofthread() {
		return _numofthread;
	}

	public void setNumofthread(int numofthread) {
		_numofthread = numofthread;
	}

	public int getTimeouttime() {
		return _timeouttime;
	}

	public void setTimeouttime(int timeouttime) {
		_timeouttime = timeouttime;
	}

	public int getSetautopostform() {
		return _setautopostform;
	}

	public void setSetautopostform(int setautopostform) {
		_setautopostform = setautopostform;
	}

	public int getSetpostmethod() {
		return _setpostmethod;
	}

	public void setSetpostmethod(int setpostmethod) {
		_setpostmethod = setpostmethod;
	}

	public String getRequestheader() {
		return _requestheader;
	}

	public void setRequestheader(String requestheader) {
		_requestheader = requestheader;
	}

	public String getLoginstatusdetection() {
		return _loginstatusdetection;
	}

	public void setLoginstatusdetection(String loginstatusdetection) {
		_loginstatusdetection = loginstatusdetection;
	}

	public int getSetnotsendpassword() {
		return _setnotsendpassword;
	}

	public void setSetnotsendpassword(int setnotsendpassword) {
		_setnotsendpassword = setnotsendpassword;
	}

	public String getParamnamesaspassword() {
		return _paramnamesaspassword;
	}

	public void setParamnamesaspassword(String paramnamesaspassword) {
		_paramnamesaspassword = paramnamesaspassword;
	}

	public int getNumofretryaterror() {
		return _numofretryaterror;
	}

	public void setNumofretryaterror(int numofretryaterror) {
		_numofretryaterror = numofretryaterror;
	}

	public String getSessionerrordetection() {
		return _sessionerrordetection;
	}

	public void setSessionerrordetection(String sessionerrordetection) {
		_sessionerrordetection = sessionerrordetection;
	}

	public String getScreentransitionerrordetection() {
		return _screentransitionerrordetection;
	}

	public void setScreentransitionerrordetection(
		String screentransitionerrordetection) {
		_screentransitionerrordetection = screentransitionerrordetection;
	}

	public int getImplementationenvironment() {
		return _implementationenvironment;
	}

	public void setImplementationenvironment(int implementationenvironment) {
		_implementationenvironment = implementationenvironment;
	}

	public int getSetcrawlingonly() {
		return _setcrawlingonly;
	}

	public void setSetcrawlingonly(int setcrawlingonly) {
		_setcrawlingonly = setcrawlingonly;
	}

	public String getExcludepathinforegex() {
		return _excludepathinforegex;
	}

	public void setExcludepathinforegex(String excludepathinforegex) {
		_excludepathinforegex = excludepathinforegex;
	}

	public String getExcludeparameterdeletenameregex() {
		return _excludeparameterdeletenameregex;
	}

	public void setExcludeparameterdeletenameregex(
		String excludeparameterdeletenameregex) {
		_excludeparameterdeletenameregex = excludeparameterdeletenameregex;
	}

	public String getExcludeparameterdeleteparamregex() {
		return _excludeparameterdeleteparamregex;
	}

	public void setExcludeparameterdeleteparamregex(
		String excludeparameterdeleteparamregex) {
		_excludeparameterdeleteparamregex = excludeparameterdeleteparamregex;
	}

	public String getExtracturlregex() {
		return _extracturlregex;
	}

	public void setExtracturlregex(String extracturlregex) {
		_extracturlregex = extracturlregex;
	}

	public String getNotparseextension() {
		return _notparseextension;
	}

	public void setNotparseextension(String notparseextension) {
		_notparseextension = notparseextension;
	}

	public String getNotparsefilenameregex() {
		return _notparsefilenameregex;
	}

	public void setNotparsefilenameregex(String notparsefilenameregex) {
		_notparsefilenameregex = notparsefilenameregex;
	}

	private long _scanid;
	private String _starturl;
	private String _authorizedpatrolurl;
	private String _unauthorizedpatrolurl;
	private Date _starttime;
	private Date _endtime;
	private int _setemailnotification;
	private String _selectedwebsignature;
	private int _getserverfiles;
	private int _getserversettings;
	private String _selectedserverfilessignature;
	private String _selectedserversettingssignature;
	private int _maxnumdetectionlink;
	private int _maxnumdetectionlinkperpage;
	private int _detectionlinkdepthlimit;
	private int _waittime;
	private int _numofthread;
	private int _timeouttime;
	private int _setautopostform;
	private int _setpostmethod;
	private String _requestheader;
	private String _loginstatusdetection;
	private int _setnotsendpassword;
	private String _paramnamesaspassword;
	private int _numofretryaterror;
	private String _sessionerrordetection;
	private String _screentransitionerrordetection;
	private int _implementationenvironment;
	private int _setcrawlingonly;
	private String _excludepathinforegex;
	private String _excludeparameterdeletenameregex;
	private String _excludeparameterdeleteparamregex;
	private String _extracturlregex;
	private String _notparseextension;
	private String _notparsefilenameregex;
}