/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.Scan;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the scan service. This utility wraps {@link jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ScanPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ScanPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ScanPersistenceImpl
 * @generated
 */
@ProviderType
public class ScanUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Scan scan) {
		getPersistence().clearCache(scan);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Scan> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Scan> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Scan> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Scan update(Scan scan) {
		return getPersistence().update(scan);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Scan update(Scan scan, ServiceContext serviceContext) {
		return getPersistence().update(scan, serviceContext);
	}

	/**
	* Returns all the scans where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the matching scans
	*/
	public static List<Scan> findByProjectId(long projectId) {
		return getPersistence().findByProjectId(projectId);
	}

	/**
	* Returns a range of all the scans where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of matching scans
	*/
	public static List<Scan> findByProjectId(long projectId, int start, int end) {
		return getPersistence().findByProjectId(projectId, start, end);
	}

	/**
	* Returns an ordered range of all the scans where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching scans
	*/
	public static List<Scan> findByProjectId(long projectId, int start,
		int end, OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .findByProjectId(projectId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the scans where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching scans
	*/
	public static List<Scan> findByProjectId(long projectId, int start,
		int end, OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByProjectId(projectId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first scan in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public static Scan findByProjectId_First(long projectId,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence()
				   .findByProjectId_First(projectId, orderByComparator);
	}

	/**
	* Returns the first scan in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public static Scan fetchByProjectId_First(long projectId,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .fetchByProjectId_First(projectId, orderByComparator);
	}

	/**
	* Returns the last scan in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public static Scan findByProjectId_Last(long projectId,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence()
				   .findByProjectId_Last(projectId, orderByComparator);
	}

	/**
	* Returns the last scan in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public static Scan fetchByProjectId_Last(long projectId,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .fetchByProjectId_Last(projectId, orderByComparator);
	}

	/**
	* Returns the scans before and after the current scan in the ordered set where projectId = &#63;.
	*
	* @param scanId the primary key of the current scan
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next scan
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public static Scan[] findByProjectId_PrevAndNext(long scanId,
		long projectId, OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence()
				   .findByProjectId_PrevAndNext(scanId, projectId,
			orderByComparator);
	}

	/**
	* Removes all the scans where projectId = &#63; from the database.
	*
	* @param projectId the project ID
	*/
	public static void removeByProjectId(long projectId) {
		getPersistence().removeByProjectId(projectId);
	}

	/**
	* Returns the number of scans where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the number of matching scans
	*/
	public static int countByProjectId(long projectId) {
		return getPersistence().countByProjectId(projectId);
	}

	/**
	* Returns all the scans where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @return the matching scans
	*/
	public static List<Scan> findByRunId(java.lang.String cxRunId) {
		return getPersistence().findByRunId(cxRunId);
	}

	/**
	* Returns a range of all the scans where cxRunId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cxRunId the cx run ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of matching scans
	*/
	public static List<Scan> findByRunId(java.lang.String cxRunId, int start,
		int end) {
		return getPersistence().findByRunId(cxRunId, start, end);
	}

	/**
	* Returns an ordered range of all the scans where cxRunId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cxRunId the cx run ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching scans
	*/
	public static List<Scan> findByRunId(java.lang.String cxRunId, int start,
		int end, OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .findByRunId(cxRunId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the scans where cxRunId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param cxRunId the cx run ID
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching scans
	*/
	public static List<Scan> findByRunId(java.lang.String cxRunId, int start,
		int end, OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByRunId(cxRunId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first scan in the ordered set where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public static Scan findByRunId_First(java.lang.String cxRunId,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence().findByRunId_First(cxRunId, orderByComparator);
	}

	/**
	* Returns the first scan in the ordered set where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public static Scan fetchByRunId_First(java.lang.String cxRunId,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence().fetchByRunId_First(cxRunId, orderByComparator);
	}

	/**
	* Returns the last scan in the ordered set where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public static Scan findByRunId_Last(java.lang.String cxRunId,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence().findByRunId_Last(cxRunId, orderByComparator);
	}

	/**
	* Returns the last scan in the ordered set where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public static Scan fetchByRunId_Last(java.lang.String cxRunId,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence().fetchByRunId_Last(cxRunId, orderByComparator);
	}

	/**
	* Returns the scans before and after the current scan in the ordered set where cxRunId = &#63;.
	*
	* @param scanId the primary key of the current scan
	* @param cxRunId the cx run ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next scan
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public static Scan[] findByRunId_PrevAndNext(long scanId,
		java.lang.String cxRunId, OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence()
				   .findByRunId_PrevAndNext(scanId, cxRunId, orderByComparator);
	}

	/**
	* Removes all the scans where cxRunId = &#63; from the database.
	*
	* @param cxRunId the cx run ID
	*/
	public static void removeByRunId(java.lang.String cxRunId) {
		getPersistence().removeByRunId(cxRunId);
	}

	/**
	* Returns the number of scans where cxRunId = &#63;.
	*
	* @param cxRunId the cx run ID
	* @return the number of matching scans
	*/
	public static int countByRunId(java.lang.String cxRunId) {
		return getPersistence().countByRunId(cxRunId);
	}

	/**
	* Returns all the scans where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @return the matching scans
	*/
	public static List<Scan> findByScanIdScanStatus(long scanId, int status) {
		return getPersistence().findByScanIdScanStatus(scanId, status);
	}

	/**
	* Returns a range of all the scans where scanId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of matching scans
	*/
	public static List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end) {
		return getPersistence()
				   .findByScanIdScanStatus(scanId, status, start, end);
	}

	/**
	* Returns an ordered range of all the scans where scanId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching scans
	*/
	public static List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end, OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .findByScanIdScanStatus(scanId, status, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the scans where scanId = &#63; and status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching scans
	*/
	public static List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end, OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByScanIdScanStatus(scanId, status, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first scan in the ordered set where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public static Scan findByScanIdScanStatus_First(long scanId, int status,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence()
				   .findByScanIdScanStatus_First(scanId, status,
			orderByComparator);
	}

	/**
	* Returns the first scan in the ordered set where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public static Scan fetchByScanIdScanStatus_First(long scanId, int status,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .fetchByScanIdScanStatus_First(scanId, status,
			orderByComparator);
	}

	/**
	* Returns the last scan in the ordered set where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public static Scan findByScanIdScanStatus_Last(long scanId, int status,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence()
				   .findByScanIdScanStatus_Last(scanId, status,
			orderByComparator);
	}

	/**
	* Returns the last scan in the ordered set where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public static Scan fetchByScanIdScanStatus_Last(long scanId, int status,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .fetchByScanIdScanStatus_Last(scanId, status,
			orderByComparator);
	}

	/**
	* Removes all the scans where scanId = &#63; and status = &#63; from the database.
	*
	* @param scanId the scan ID
	* @param status the status
	*/
	public static void removeByScanIdScanStatus(long scanId, int status) {
		getPersistence().removeByScanIdScanStatus(scanId, status);
	}

	/**
	* Returns the number of scans where scanId = &#63; and status = &#63;.
	*
	* @param scanId the scan ID
	* @param status the status
	* @return the number of matching scans
	*/
	public static int countByScanIdScanStatus(long scanId, int status) {
		return getPersistence().countByScanIdScanStatus(scanId, status);
	}

	/**
	* Returns all the scans where status = &#63;.
	*
	* @param status the status
	* @return the matching scans
	*/
	public static List<Scan> findByScanStatus(int status) {
		return getPersistence().findByScanStatus(status);
	}

	/**
	* Returns a range of all the scans where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of matching scans
	*/
	public static List<Scan> findByScanStatus(int status, int start, int end) {
		return getPersistence().findByScanStatus(status, start, end);
	}

	/**
	* Returns an ordered range of all the scans where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching scans
	*/
	public static List<Scan> findByScanStatus(int status, int start, int end,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .findByScanStatus(status, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the scans where status = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param status the status
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching scans
	*/
	public static List<Scan> findByScanStatus(int status, int start, int end,
		OrderByComparator<Scan> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByScanStatus(status, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first scan in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public static Scan findByScanStatus_First(int status,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence().findByScanStatus_First(status, orderByComparator);
	}

	/**
	* Returns the first scan in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public static Scan fetchByScanStatus_First(int status,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence()
				   .fetchByScanStatus_First(status, orderByComparator);
	}

	/**
	* Returns the last scan in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan
	* @throws NoSuchScanException if a matching scan could not be found
	*/
	public static Scan findByScanStatus_Last(int status,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence().findByScanStatus_Last(status, orderByComparator);
	}

	/**
	* Returns the last scan in the ordered set where status = &#63;.
	*
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching scan, or <code>null</code> if a matching scan could not be found
	*/
	public static Scan fetchByScanStatus_Last(int status,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence().fetchByScanStatus_Last(status, orderByComparator);
	}

	/**
	* Returns the scans before and after the current scan in the ordered set where status = &#63;.
	*
	* @param scanId the primary key of the current scan
	* @param status the status
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next scan
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public static Scan[] findByScanStatus_PrevAndNext(long scanId, int status,
		OrderByComparator<Scan> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence()
				   .findByScanStatus_PrevAndNext(scanId, status,
			orderByComparator);
	}

	/**
	* Removes all the scans where status = &#63; from the database.
	*
	* @param status the status
	*/
	public static void removeByScanStatus(int status) {
		getPersistence().removeByScanStatus(status);
	}

	/**
	* Returns the number of scans where status = &#63;.
	*
	* @param status the status
	* @return the number of matching scans
	*/
	public static int countByScanStatus(int status) {
		return getPersistence().countByScanStatus(status);
	}

	/**
	* Caches the scan in the entity cache if it is enabled.
	*
	* @param scan the scan
	*/
	public static void cacheResult(Scan scan) {
		getPersistence().cacheResult(scan);
	}

	/**
	* Caches the scans in the entity cache if it is enabled.
	*
	* @param scans the scans
	*/
	public static void cacheResult(List<Scan> scans) {
		getPersistence().cacheResult(scans);
	}

	/**
	* Creates a new scan with the primary key. Does not add the scan to the database.
	*
	* @param scanId the primary key for the new scan
	* @return the new scan
	*/
	public static Scan create(long scanId) {
		return getPersistence().create(scanId);
	}

	/**
	* Removes the scan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanId the primary key of the scan
	* @return the scan that was removed
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public static Scan remove(long scanId)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence().remove(scanId);
	}

	public static Scan updateImpl(Scan scan) {
		return getPersistence().updateImpl(scan);
	}

	/**
	* Returns the scan with the primary key or throws a {@link NoSuchScanException} if it could not be found.
	*
	* @param scanId the primary key of the scan
	* @return the scan
	* @throws NoSuchScanException if a scan with the primary key could not be found
	*/
	public static Scan findByPrimaryKey(long scanId)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException {
		return getPersistence().findByPrimaryKey(scanId);
	}

	/**
	* Returns the scan with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param scanId the primary key of the scan
	* @return the scan, or <code>null</code> if a scan with the primary key could not be found
	*/
	public static Scan fetchByPrimaryKey(long scanId) {
		return getPersistence().fetchByPrimaryKey(scanId);
	}

	public static java.util.Map<java.io.Serializable, Scan> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the scans.
	*
	* @return the scans
	*/
	public static List<Scan> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of scans
	*/
	public static List<Scan> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of scans
	*/
	public static List<Scan> findAll(int start, int end,
		OrderByComparator<Scan> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of scans
	*/
	public static List<Scan> findAll(int start, int end,
		OrderByComparator<Scan> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the scans from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of scans.
	*
	* @return the number of scans
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ScanPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ScanPersistence, ScanPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ScanPersistence.class);
}