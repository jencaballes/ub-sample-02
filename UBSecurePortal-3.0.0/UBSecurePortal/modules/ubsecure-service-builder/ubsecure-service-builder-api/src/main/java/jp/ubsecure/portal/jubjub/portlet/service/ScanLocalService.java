/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.Scan;

import java.io.Serializable;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Provides the local service interface for Scan. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see ScanLocalServiceUtil
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.ScanLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.ScanLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface ScanLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ScanLocalServiceUtil} to access the scan local service. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ScanLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Cancel a review request
	*
	* @param lScanId
	id of scan to cancel the request review
	* @return returns true if cancel transaction is successful
	* @throws PortalException
	an exception thrown
	*/
	public boolean cancelReview(long lScanId) throws PortalException;

	/**
	* Delete existing scan
	*
	* @param lScanId
	scan id to be deleted
	* @return returns true if delete transaction is successful
	* @throws PortalException
	*/
	public boolean deleteScan(int iType, long lScanId)
		throws PortalException;

	/**
	* Check if scan is deleted in cx server
	*
	* @param scanId
	id of the scan to be checked
	* @return return true if the scan is deleted in cx scan server, else return
	false
	* @throws PortalException
	an exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isDeletedScanInCxServer(long scanId)
		throws PortalException;

	/**
	* Request for review
	*
	* @param lScanId
	id of the scan to be requested for review
	* @return returns true if request review transaction is successful
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public boolean requestReview(long lScanId) throws PortalException;

	/**
	* Update existing scans
	*
	* @param iUserAction
	action of the user
	* @param scan
	the scan to be updated
	* @return return true if update transaction is successful
	* @throws PortalException
	an exception thrown if transaction fails
	*/
	public boolean updateScan(int iUserAction, Scan scan)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Get the number of scans waiting
	*
	* @param scanRegDate
	the registration date of the scan
	* @param type
	a type of project(android, cxsuite, ios)
	* @return Number of scans waiting
	* @throws PortalException
	*/
	public int countCrawlWaiting(Date scanRegDate, int type)
		throws PortalException;

	/**
	* Get the number of scans waiting
	*
	* @param scanRegDate
	the registration date of the scan
	* @param type
	a type of project(android, cxsuite, ios)
	* @return Number of scans waiting
	* @throws PortalException
	*/
	public int countScanWaiting(Date scanRegDate, int type)
		throws PortalException;

	/**
	* Get the total Number of All scans in all projects for each project type
	* (android, cxsuite, ios)
	*
	* @param type
	a type of the project (android, cxsuite, ios)
	* @param searchedScan
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEntireScansCount(int type,
		Map<java.lang.String, java.lang.Object> searchedScan)
		throws PortalException;

	/**
	* Get Scan Status
	*
	* @param lScanId
	id of the scan
	* @return returns a status of the scan
	* @throws PortalException
	an exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getScanStatus(long lScanId) throws PortalException;

	/**
	* Returns the number of scans.
	*
	* @return the number of scans
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getScansCount();

	/**
	* Get the number of scans
	*
	* @param lProjectId
	- id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getScansCount(long lProjectId,
		Map<java.lang.String, java.lang.Object> searchedScan)
		throws PortalException;

	/**
	* Get File path
	*
	* @param lScanId
	scan ID
	* @return filepath
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getFilePath(long lScanId) throws PortalException;

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Retrieve the scan registragion date
	*
	* @param lScanId
	id of the scan
	* @return Date
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Date getScanRegistrationDate(long lScanId);

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	/**
	* Get All Scans
	*
	* @param type
	a type of the project (android, cxsuite, ios)
	* @param searchedScan
	a key-value parameter inputted by user during search.
	* @param start
	a start index position
	* @return scanList list of the entire scan
	* @throws PortalException
	an exception thrown if error occurs.
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.Object> getEntireScans(int type,
		Map<java.lang.String, java.lang.Object> searchedScan, int start)
		throws PortalException;

	/**
	* Returns a range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of scans
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Scan> getScans(int start, int end);

	/**
	* Get List of Scans
	*
	* @param lProjectId
	- id of the project on which project scans should be retrieved
	* @param searchedScan
	- a key-value parameter inputted by user during search.
	* @param iStart
	- start index position
	* @return scanList - list of Project scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.Object> getScans(long lProjectId, User user,
		Map<java.lang.String, java.lang.Object> searchedScan, int iStart)
		throws PortalException;

	/**
	* Get Scans Per Project
	*
	* @param projectId
	id of the project on where to get the scans
	* @return scanList list of scans
	* @throws PortalException
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Scan> getScansByProjectId(long projectId)
		throws PortalException;

	/**
	* Sort Entire Scans
	*
	* @param type
	a type of project (android, cxsuite, ios)
	* @param searchedScan
	a key-value parameter inputted by user during search.
	* @param orderByCol
	a column to order
	* @param orderByType
	type of order(ascending, descending)
	* @param start
	index for the start position
	* @return scanList - sorted scan list
	* @throws PortalException
	*/
	public List<java.lang.Object> sortEntireScans(int type,
		Map<java.lang.String, java.lang.Object> searchedScan,
		java.lang.String orderByCol, java.lang.String orderByType, int start)
		throws PortalException;

	/**
	* Sort Scans
	*
	* @param lProjectId
	id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @param orderByCol
	a column to order
	* @param orderByType
	type of order(ascending, descending)
	* @param iStart
	index for the start position
	* @return scanList a sorted scan list
	* @throws PortalException
	*/
	public List<java.lang.Object> sortScans(long lProjectId,
		Map<java.lang.String, java.lang.Object> searchedScan,
		java.lang.String orderByCol, java.lang.String orderByType, int iStart)
		throws PortalException;

	/**
	* Adds the scan to the database. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Scan addScan(Scan scan);

	/**
	* Creates a new scan with the primary key. Does not add the scan to the database.
	*
	* @param scanId the primary key for the new scan
	* @return the new scan
	*/
	public Scan createScan(long scanId);

	/**
	* Create empty scan object
	*
	* @return scan object
	*/
	public Scan createScanObj();

	/**
	* Deletes the scan from the database. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Scan deleteScan(Scan scan);

	/**
	* Deletes the scan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanId the primary key of the scan
	* @return the scan that was removed
	* @throws PortalException if a scan with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Scan deleteScan(long scanId) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Scan fetchScan(long scanId);

	/**
	* Returns the scan with the primary key.
	*
	* @param scanId the primary key of the scan
	* @return the scan
	* @throws PortalException if a scan with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Scan getScan(long scanId) throws PortalException;

	/**
	* Get Scan
	*
	* @param lScanId
	id of the scan to retrieve
	* @return Scan object
	* @throws PortalException
	an exception thrown
	*/
	public Scan retrieveScan(long lScanId) throws PortalException;

	/**
	* Update Android scan status
	*
	* @param lScanId
	id of the scan to be updated
	* @param iStatus
	the status to be set
	* @return returns the updated scan
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public Scan updateAndroidScanStatus(long lScanId, int iStatus)
		throws PortalException;

	/**
	* Updates the scan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Scan updateScan(Scan scan);

	/**
	* Generate a unique scan ID
	*
	* @return returns a unique scan id
	* @throws PortalException
	an exception thrown
	*/
	public long createScanId() throws PortalException;

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);
}