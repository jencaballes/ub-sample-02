/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VexDetectionResultLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResultLocalService
 * @generated
 */
@ProviderType
public class VexDetectionResultLocalServiceWrapper
	implements VexDetectionResultLocalService,
		ServiceWrapper<VexDetectionResultLocalService> {
	public VexDetectionResultLocalServiceWrapper(
		VexDetectionResultLocalService vexDetectionResultLocalService) {
		_vexDetectionResultLocalService = vexDetectionResultLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _vexDetectionResultLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _vexDetectionResultLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _vexDetectionResultLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexDetectionResultLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexDetectionResultLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Get the number of scans
	*
	* @param lProjectId
	- id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Override
	public int getDetectionResultsCount(long lscanId,
		java.util.Map<java.lang.String, java.lang.Object> searchedDetectionResult)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexDetectionResultLocalService.getDetectionResultsCount(lscanId,
			searchedDetectionResult);
	}

	/**
	* Returns the number of vex detection results.
	*
	* @return the number of vex detection results
	*/
	@Override
	public int getVexDetectionResultsCount() {
		return _vexDetectionResultLocalService.getVexDetectionResultsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _vexDetectionResultLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _vexDetectionResultLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _vexDetectionResultLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _vexDetectionResultLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Get List of Detection Result
	*
	* @param lscanId
	- id of the scan on which detection results should be retrieved
	* @param searchedDetectionResult
	- a key-value parameter inputted by user during search.
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Override
	public java.util.List<java.lang.Object> getVexDetectionResultReviews(
		long lscanId, com.liferay.portal.kernel.model.User user,
		java.util.Map<java.lang.String, java.lang.Object> searchedDetectionResult,
		int start, java.lang.String orderByCol, java.lang.String orderByType)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexDetectionResultLocalService.getVexDetectionResultReviews(lscanId,
			user, searchedDetectionResult, start, orderByCol, orderByType);
	}

	/**
	* Returns a range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @return the range of vex detection results
	*/
	@Override
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult> getVexDetectionResults(
		int start, int end) {
		return _vexDetectionResultLocalService.getVexDetectionResults(start, end);
	}

	/**
	* Adds the vex detection result to the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was added
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult addVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult vexDetectionResult) {
		return _vexDetectionResultLocalService.addVexDetectionResult(vexDetectionResult);
	}

	/**
	* Creates a new vex detection result with the primary key. Does not add the vex detection result to the database.
	*
	* @param vexDetectionResultPK the primary key for the new vex detection result
	* @return the new vex detection result
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult createVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK) {
		return _vexDetectionResultLocalService.createVexDetectionResult(vexDetectionResultPK);
	}

	/**
	* Deletes the vex detection result from the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was removed
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult deleteVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult vexDetectionResult) {
		return _vexDetectionResultLocalService.deleteVexDetectionResult(vexDetectionResult);
	}

	/**
	* Deletes the vex detection result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result that was removed
	* @throws PortalException if a vex detection result with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult deleteVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexDetectionResultLocalService.deleteVexDetectionResult(vexDetectionResultPK);
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult fetchVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK) {
		return _vexDetectionResultLocalService.fetchVexDetectionResult(vexDetectionResultPK);
	}

	/**
	* Returns the vex detection result with the primary key.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result
	* @throws PortalException if a vex detection result with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult getVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexDetectionResultLocalService.getVexDetectionResult(vexDetectionResultPK);
	}

	/**
	* Updates the vex detection result in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was updated
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult updateVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult vexDetectionResult) {
		return _vexDetectionResultLocalService.updateVexDetectionResult(vexDetectionResult);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _vexDetectionResultLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _vexDetectionResultLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public VexDetectionResultLocalService getWrappedService() {
		return _vexDetectionResultLocalService;
	}

	@Override
	public void setWrappedService(
		VexDetectionResultLocalService vexDetectionResultLocalService) {
		_vexDetectionResultLocalService = vexDetectionResultLocalService;
	}

	private VexDetectionResultLocalService _vexDetectionResultLocalService;
}