/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the vex detection result service. This utility wraps {@link jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexDetectionResultPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResultPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexDetectionResultPersistenceImpl
 * @generated
 */
@ProviderType
public class VexDetectionResultUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(VexDetectionResult vexDetectionResult) {
		getPersistence().clearCache(vexDetectionResult);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<VexDetectionResult> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<VexDetectionResult> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<VexDetectionResult> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<VexDetectionResult> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static VexDetectionResult update(
		VexDetectionResult vexDetectionResult) {
		return getPersistence().update(vexDetectionResult);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static VexDetectionResult update(
		VexDetectionResult vexDetectionResult, ServiceContext serviceContext) {
		return getPersistence().update(vexDetectionResult, serviceContext);
	}

	/**
	* Returns all the vex detection results where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the matching vex detection results
	*/
	public static List<VexDetectionResult> findByScanId(long scanid) {
		return getPersistence().findByScanId(scanid);
	}

	/**
	* Returns a range of all the vex detection results where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @return the range of matching vex detection results
	*/
	public static List<VexDetectionResult> findByScanId(long scanid, int start,
		int end) {
		return getPersistence().findByScanId(scanid, start, end);
	}

	/**
	* Returns an ordered range of all the vex detection results where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching vex detection results
	*/
	public static List<VexDetectionResult> findByScanId(long scanid, int start,
		int end, OrderByComparator<VexDetectionResult> orderByComparator) {
		return getPersistence()
				   .findByScanId(scanid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the vex detection results where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching vex detection results
	*/
	public static List<VexDetectionResult> findByScanId(long scanid, int start,
		int end, OrderByComparator<VexDetectionResult> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByScanId(scanid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first vex detection result in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex detection result
	* @throws NoSuchVexDetectionResultException if a matching vex detection result could not be found
	*/
	public static VexDetectionResult findByScanId_First(long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexDetectionResultException {
		return getPersistence().findByScanId_First(scanid, orderByComparator);
	}

	/**
	* Returns the first vex detection result in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex detection result, or <code>null</code> if a matching vex detection result could not be found
	*/
	public static VexDetectionResult fetchByScanId_First(long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator) {
		return getPersistence().fetchByScanId_First(scanid, orderByComparator);
	}

	/**
	* Returns the last vex detection result in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex detection result
	* @throws NoSuchVexDetectionResultException if a matching vex detection result could not be found
	*/
	public static VexDetectionResult findByScanId_Last(long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexDetectionResultException {
		return getPersistence().findByScanId_Last(scanid, orderByComparator);
	}

	/**
	* Returns the last vex detection result in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex detection result, or <code>null</code> if a matching vex detection result could not be found
	*/
	public static VexDetectionResult fetchByScanId_Last(long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator) {
		return getPersistence().fetchByScanId_Last(scanid, orderByComparator);
	}

	/**
	* Returns the vex detection results before and after the current vex detection result in the ordered set where scanid = &#63;.
	*
	* @param vexDetectionResultPK the primary key of the current vex detection result
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next vex detection result
	* @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	*/
	public static VexDetectionResult[] findByScanId_PrevAndNext(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK,
		long scanid, OrderByComparator<VexDetectionResult> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexDetectionResultException {
		return getPersistence()
				   .findByScanId_PrevAndNext(vexDetectionResultPK, scanid,
			orderByComparator);
	}

	/**
	* Removes all the vex detection results where scanid = &#63; from the database.
	*
	* @param scanid the scanid
	*/
	public static void removeByScanId(long scanid) {
		getPersistence().removeByScanId(scanid);
	}

	/**
	* Returns the number of vex detection results where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the number of matching vex detection results
	*/
	public static int countByScanId(long scanid) {
		return getPersistence().countByScanId(scanid);
	}

	/**
	* Caches the vex detection result in the entity cache if it is enabled.
	*
	* @param vexDetectionResult the vex detection result
	*/
	public static void cacheResult(VexDetectionResult vexDetectionResult) {
		getPersistence().cacheResult(vexDetectionResult);
	}

	/**
	* Caches the vex detection results in the entity cache if it is enabled.
	*
	* @param vexDetectionResults the vex detection results
	*/
	public static void cacheResult(List<VexDetectionResult> vexDetectionResults) {
		getPersistence().cacheResult(vexDetectionResults);
	}

	/**
	* Creates a new vex detection result with the primary key. Does not add the vex detection result to the database.
	*
	* @param vexDetectionResultPK the primary key for the new vex detection result
	* @return the new vex detection result
	*/
	public static VexDetectionResult create(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK) {
		return getPersistence().create(vexDetectionResultPK);
	}

	/**
	* Removes the vex detection result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result that was removed
	* @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	*/
	public static VexDetectionResult remove(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexDetectionResultException {
		return getPersistence().remove(vexDetectionResultPK);
	}

	public static VexDetectionResult updateImpl(
		VexDetectionResult vexDetectionResult) {
		return getPersistence().updateImpl(vexDetectionResult);
	}

	/**
	* Returns the vex detection result with the primary key or throws a {@link NoSuchVexDetectionResultException} if it could not be found.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result
	* @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	*/
	public static VexDetectionResult findByPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexDetectionResultException {
		return getPersistence().findByPrimaryKey(vexDetectionResultPK);
	}

	/**
	* Returns the vex detection result with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result, or <code>null</code> if a vex detection result with the primary key could not be found
	*/
	public static VexDetectionResult fetchByPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK) {
		return getPersistence().fetchByPrimaryKey(vexDetectionResultPK);
	}

	public static java.util.Map<java.io.Serializable, VexDetectionResult> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the vex detection results.
	*
	* @return the vex detection results
	*/
	public static List<VexDetectionResult> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @return the range of vex detection results
	*/
	public static List<VexDetectionResult> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of vex detection results
	*/
	public static List<VexDetectionResult> findAll(int start, int end,
		OrderByComparator<VexDetectionResult> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of vex detection results
	*/
	public static List<VexDetectionResult> findAll(int start, int end,
		OrderByComparator<VexDetectionResult> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the vex detection results from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of vex detection results.
	*
	* @return the number of vex detection results
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static VexDetectionResultPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VexDetectionResultPersistence, VexDetectionResultPersistence> _serviceTracker =
		ServiceTrackerFactory.open(VexDetectionResultPersistence.class);
}