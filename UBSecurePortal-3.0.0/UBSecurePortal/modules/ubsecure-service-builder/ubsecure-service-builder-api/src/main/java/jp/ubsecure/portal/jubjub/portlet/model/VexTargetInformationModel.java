/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

/**
 * The base model interface for the VexTargetInformation service. Represents a row in the &quot;UBS_VexTargetInformation&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexTargetInformationModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexTargetInformationImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexTargetInformation
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.VexTargetInformationImpl
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.VexTargetInformationModelImpl
 * @generated
 */
@ProviderType
public interface VexTargetInformationModel extends BaseModel<VexTargetInformation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a vex target information model instance should use the {@link VexTargetInformation} interface instead.
	 */

	/**
	 * Returns the primary key of this vex target information.
	 *
	 * @return the primary key of this vex target information
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this vex target information.
	 *
	 * @param primaryKey the primary key of this vex target information
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the scanid of this vex target information.
	 *
	 * @return the scanid of this vex target information
	 */
	public long getScanid();

	/**
	 * Sets the scanid of this vex target information.
	 *
	 * @param scanid the scanid of this vex target information
	 */
	public void setScanid(long scanid);

	/**
	 * Returns the protocol of this vex target information.
	 *
	 * @return the protocol of this vex target information
	 */
	@AutoEscape
	public String getProtocol();

	/**
	 * Sets the protocol of this vex target information.
	 *
	 * @param protocol the protocol of this vex target information
	 */
	public void setProtocol(String protocol);

	/**
	 * Returns the host of this vex target information.
	 *
	 * @return the host of this vex target information
	 */
	@AutoEscape
	public String getHost();

	/**
	 * Sets the host of this vex target information.
	 *
	 * @param host the host of this vex target information
	 */
	public void setHost(String host);

	/**
	 * Returns the port of this vex target information.
	 *
	 * @return the port of this vex target information
	 */
	public int getPort();

	/**
	 * Sets the port of this vex target information.
	 *
	 * @param port the port of this vex target information
	 */
	public void setPort(int port);

	/**
	 * Returns the httpversion of this vex target information.
	 *
	 * @return the httpversion of this vex target information
	 */
	public int getHttpversion();

	/**
	 * Sets the httpversion of this vex target information.
	 *
	 * @param httpversion the httpversion of this vex target information
	 */
	public void setHttpversion(int httpversion);

	/**
	 * Returns the setkeepaliveconnection of this vex target information.
	 *
	 * @return the setkeepaliveconnection of this vex target information
	 */
	public int getSetkeepaliveconnection();

	/**
	 * Sets the setkeepaliveconnection of this vex target information.
	 *
	 * @param setkeepaliveconnection the setkeepaliveconnection of this vex target information
	 */
	public void setSetkeepaliveconnection(int setkeepaliveconnection);

	/**
	 * Returns the setresponsecontentlength of this vex target information.
	 *
	 * @return the setresponsecontentlength of this vex target information
	 */
	public int getSetresponsecontentlength();

	/**
	 * Sets the setresponsecontentlength of this vex target information.
	 *
	 * @param setresponsecontentlength the setresponsecontentlength of this vex target information
	 */
	public void setSetresponsecontentlength(int setresponsecontentlength);

	/**
	 * Returns the useacceptencodingheader of this vex target information.
	 *
	 * @return the useacceptencodingheader of this vex target information
	 */
	public int getUseacceptencodingheader();

	/**
	 * Sets the useacceptencodingheader of this vex target information.
	 *
	 * @param useacceptencodingheader the useacceptencodingheader of this vex target information
	 */
	public void setUseacceptencodingheader(int useacceptencodingheader);

	/**
	 * Returns the unzipresponse of this vex target information.
	 *
	 * @return the unzipresponse of this vex target information
	 */
	public int getUnzipresponse();

	/**
	 * Sets the unzipresponse of this vex target information.
	 *
	 * @param unzipresponse the unzipresponse of this vex target information
	 */
	public void setUnzipresponse(int unzipresponse);

	/**
	 * Returns the httpprotocol of this vex target information.
	 *
	 * @return the httpprotocol of this vex target information
	 */
	@AutoEscape
	public String getHttpprotocol();

	/**
	 * Sets the httpprotocol of this vex target information.
	 *
	 * @param httpprotocol the httpprotocol of this vex target information
	 */
	public void setHttpprotocol(String httpprotocol);

	/**
	 * Returns the externalproxyhost of this vex target information.
	 *
	 * @return the externalproxyhost of this vex target information
	 */
	@AutoEscape
	public String getExternalproxyhost();

	/**
	 * Sets the externalproxyhost of this vex target information.
	 *
	 * @param externalproxyhost the externalproxyhost of this vex target information
	 */
	public void setExternalproxyhost(String externalproxyhost);

	/**
	 * Returns the externalproxyport of this vex target information.
	 *
	 * @return the externalproxyport of this vex target information
	 */
	public int getExternalproxyport();

	/**
	 * Sets the externalproxyport of this vex target information.
	 *
	 * @param externalproxyport the externalproxyport of this vex target information
	 */
	public void setExternalproxyport(int externalproxyport);

	/**
	 * Returns the externalproxyauthid of this vex target information.
	 *
	 * @return the externalproxyauthid of this vex target information
	 */
	@AutoEscape
	public String getExternalproxyauthid();

	/**
	 * Sets the externalproxyauthid of this vex target information.
	 *
	 * @param externalproxyauthid the externalproxyauthid of this vex target information
	 */
	public void setExternalproxyauthid(String externalproxyauthid);

	/**
	 * Returns the externalproxyauthpassword of this vex target information.
	 *
	 * @return the externalproxyauthpassword of this vex target information
	 */
	@AutoEscape
	public String getExternalproxyauthpassword();

	/**
	 * Sets the externalproxyauthpassword of this vex target information.
	 *
	 * @param externalproxyauthpassword the externalproxyauthpassword of this vex target information
	 */
	public void setExternalproxyauthpassword(String externalproxyauthpassword);

	/**
	 * Returns the useclientcertificate of this vex target information.
	 *
	 * @return the useclientcertificate of this vex target information
	 */
	public int getUseclientcertificate();

	/**
	 * Sets the useclientcertificate of this vex target information.
	 *
	 * @param useclientcertificate the useclientcertificate of this vex target information
	 */
	public void setUseclientcertificate(int useclientcertificate);

	/**
	 * Returns the certificatefile of this vex target information.
	 *
	 * @return the certificatefile of this vex target information
	 */
	@AutoEscape
	public String getCertificatefile();

	/**
	 * Sets the certificatefile of this vex target information.
	 *
	 * @param certificatefile the certificatefile of this vex target information
	 */
	public void setCertificatefile(String certificatefile);

	/**
	 * Returns the certificatefilepassword of this vex target information.
	 *
	 * @return the certificatefilepassword of this vex target information
	 */
	@AutoEscape
	public String getCertificatefilepassword();

	/**
	 * Sets the certificatefilepassword of this vex target information.
	 *
	 * @param certificatefilepassword the certificatefilepassword of this vex target information
	 */
	public void setCertificatefilepassword(String certificatefilepassword);

	/**
	 * Returns the ntlmauthid of this vex target information.
	 *
	 * @return the ntlmauthid of this vex target information
	 */
	@AutoEscape
	public String getNtlmauthid();

	/**
	 * Sets the ntlmauthid of this vex target information.
	 *
	 * @param ntlmauthid the ntlmauthid of this vex target information
	 */
	public void setNtlmauthid(String ntlmauthid);

	/**
	 * Returns the ntlmauthpassword of this vex target information.
	 *
	 * @return the ntlmauthpassword of this vex target information
	 */
	@AutoEscape
	public String getNtlmauthpassword();

	/**
	 * Sets the ntlmauthpassword of this vex target information.
	 *
	 * @param ntlmauthpassword the ntlmauthpassword of this vex target information
	 */
	public void setNtlmauthpassword(String ntlmauthpassword);

	/**
	 * Returns the ntlmauthdomain of this vex target information.
	 *
	 * @return the ntlmauthdomain of this vex target information
	 */
	@AutoEscape
	public String getNtlmauthdomain();

	/**
	 * Sets the ntlmauthdomain of this vex target information.
	 *
	 * @param ntlmauthdomain the ntlmauthdomain of this vex target information
	 */
	public void setNtlmauthdomain(String ntlmauthdomain);

	/**
	 * Returns the ntlmauthhost of this vex target information.
	 *
	 * @return the ntlmauthhost of this vex target information
	 */
	@AutoEscape
	public String getNtlmauthhost();

	/**
	 * Sets the ntlmauthhost of this vex target information.
	 *
	 * @param ntlmauthhost the ntlmauthhost of this vex target information
	 */
	public void setNtlmauthhost(String ntlmauthhost);

	/**
	 * Returns the digestauthid of this vex target information.
	 *
	 * @return the digestauthid of this vex target information
	 */
	@AutoEscape
	public String getDigestauthid();

	/**
	 * Sets the digestauthid of this vex target information.
	 *
	 * @param digestauthid the digestauthid of this vex target information
	 */
	public void setDigestauthid(String digestauthid);

	/**
	 * Returns the digestauthpassword of this vex target information.
	 *
	 * @return the digestauthpassword of this vex target information
	 */
	@AutoEscape
	public String getDigestauthpassword();

	/**
	 * Sets the digestauthpassword of this vex target information.
	 *
	 * @param digestauthpassword the digestauthpassword of this vex target information
	 */
	public void setDigestauthpassword(String digestauthpassword);

	/**
	 * Returns the basicauthid of this vex target information.
	 *
	 * @return the basicauthid of this vex target information
	 */
	@AutoEscape
	public String getBasicauthid();

	/**
	 * Sets the basicauthid of this vex target information.
	 *
	 * @param basicauthid the basicauthid of this vex target information
	 */
	public void setBasicauthid(String basicauthid);

	/**
	 * Returns the basicauthpassword of this vex target information.
	 *
	 * @return the basicauthpassword of this vex target information
	 */
	@AutoEscape
	public String getBasicauthpassword();

	/**
	 * Sets the basicauthpassword of this vex target information.
	 *
	 * @param basicauthpassword the basicauthpassword of this vex target information
	 */
	public void setBasicauthpassword(String basicauthpassword);

	/**
	 * Returns the accessexclusionpath of this vex target information.
	 *
	 * @return the accessexclusionpath of this vex target information
	 */
	@AutoEscape
	public String getAccessexclusionpath();

	/**
	 * Sets the accessexclusionpath of this vex target information.
	 *
	 * @param accessexclusionpath the accessexclusionpath of this vex target information
	 */
	public void setAccessexclusionpath(String accessexclusionpath);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation vexTargetInformation);

	@Override
	public int hashCode();

	@Override
	public CacheModel<jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation> toCacheModel();

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation toEscapedModel();

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}