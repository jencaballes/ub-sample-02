package jp.ubsecure.portal.jubjub.portlet.model;

public class VexMetaSummaryData {
	
	private String companyName;
	private String projectName;
	private String reportID;
	private String implEnvironment;
	private String proposalNum;
	private String scanID;
	private String diagnosisStartDate; 
	private String diagnosisEndDate;
	private String highRiskCount;
	private String mediumRiskCount;
	private String lowRiskCount;
	private String remarksRiskCount;
	private String infoRiskCount;
	private String scanExecutionTime;
	private String webSignatureName;
	private String ServerSettingsSignatureName;
	private String serverFilesSignatuteName;
	private String vexVersion;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getProjectName() {
		return projectName;
	}
	public String getReportID() {
		return reportID;
	}
	public void setReportID(String reportID) {
		this.reportID = reportID;
	}
	public String getScanID() {
		return scanID;
	}
	public void setScanID(String scanID) {
		this.scanID = scanID;
	}
	public String getDiagnosisStartDate() {
		return diagnosisStartDate;
	}
	public void setDiagnosisStartDate(String diagnosisStartDate) {
		this.diagnosisStartDate = diagnosisStartDate;
	}
	public String getDiagnosisEndDate() {
		return diagnosisEndDate;
	}
	public void setDiagnosisEndDate(String diagnosisEndDate) {
		this.diagnosisEndDate = diagnosisEndDate;
	}
	public String getScanExecutionTime() {
		return scanExecutionTime;
	}
	public void setScanExecutionTime(String scanExecutionTime) {
		this.scanExecutionTime = scanExecutionTime;
	}
	public String getWebSignatureName() {
		return webSignatureName;
	}
	public void setWebSignatureName(String webSignatureName) {
		this.webSignatureName = webSignatureName;
	}
	public String getServerSettingsSignatureName() {
		return ServerSettingsSignatureName;
	}
	public void setServerSettingsSignatureName(String serverSettingsSignatureName) {
		ServerSettingsSignatureName = serverSettingsSignatureName;
	}
	public String getServerFilesSignatuteName() {
		return serverFilesSignatuteName;
	}
	public void setServerFilesSignatuteName(String serverFilesSignatuteName) {
		this.serverFilesSignatuteName = serverFilesSignatuteName;
	}
	public String getVexVersion() {
		return vexVersion;
	}
	public void setVexVersion(String vexVersion) {
		this.vexVersion = vexVersion;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getImplEnvironment() {
		return implEnvironment;
	}
	public void setImplEnvironment(String implEnvironment) {
		this.implEnvironment = implEnvironment;
	}
	public String getProposalNum() {
		return proposalNum;
	}
	public void setProposalNum(String proposalNum) {
		this.proposalNum = proposalNum;
	}
	public String getHighRiskCount() {
		return highRiskCount;
	}
	public void setHighRiskCount(String highRiskCount) {
		this.highRiskCount = highRiskCount;
	}
	public String getMediumRiskCount() {
		return mediumRiskCount;
	}
	public void setMediumRiskCount(String mediumRiskCount) {
		this.mediumRiskCount = mediumRiskCount;
	}
	public String getLowRiskCount() {
		return lowRiskCount;
	}
	public void setLowRiskCount(String lowRiskCount) {
		this.lowRiskCount = lowRiskCount;
	}
	public String getRemarksRiskCount() {
		return remarksRiskCount;
	}
	public void setRemarksRiskCount(String remarksRiskCount) {
		this.remarksRiskCount = remarksRiskCount;
	}
	public String getInfoRiskCount() {
		return infoRiskCount;
	}
	public void setInfoRiskCount(String infoRiskCount) {
		this.infoRiskCount = infoRiskCount;
	}
}
