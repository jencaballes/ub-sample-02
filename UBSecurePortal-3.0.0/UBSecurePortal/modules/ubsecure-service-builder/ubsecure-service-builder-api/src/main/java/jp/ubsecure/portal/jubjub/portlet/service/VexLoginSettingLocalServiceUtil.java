/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for VexLoginSetting. This utility wraps
 * {@link jp.ubsecure.portal.jubjub.portlet.service.impl.VexLoginSettingLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see VexLoginSettingLocalService
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.VexLoginSettingLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.VexLoginSettingLocalServiceImpl
 * @generated
 */
@ProviderType
public class VexLoginSettingLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.VexLoginSettingLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of vex login settings.
	*
	* @return the number of vex login settings
	*/
	public static int getVexLoginSettingsCount() {
		return getService().getVexLoginSettingsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<java.lang.Object> getLoginSettings(
		long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getLoginSettings(lScanId);
	}

	/**
	* Returns a range of all the vex login settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex login settings
	* @param end the upper bound of the range of vex login settings (not inclusive)
	* @return the range of vex login settings
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting> getVexLoginSettings(
		int start, int end) {
		return getService().getVexLoginSettings(start, end);
	}

	/**
	* Adds the vex login setting to the database. Also notifies the appropriate model listeners.
	*
	* @param vexLoginSetting the vex login setting
	* @return the vex login setting that was added
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting addVexLoginSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting vexLoginSetting) {
		return getService().addVexLoginSetting(vexLoginSetting);
	}

	/**
	* Creates a new vex login setting with the primary key. Does not add the vex login setting to the database.
	*
	* @param scanid the primary key for the new vex login setting
	* @return the new vex login setting
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting createVexLoginSetting(
		long scanid) {
		return getService().createVexLoginSetting(scanid);
	}

	public static jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting createVexLoginSettingObj() {
		return getService().createVexLoginSettingObj();
	}

	/**
	* Deletes the vex login setting from the database. Also notifies the appropriate model listeners.
	*
	* @param vexLoginSetting the vex login setting
	* @return the vex login setting that was removed
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting deleteVexLoginSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting vexLoginSetting) {
		return getService().deleteVexLoginSetting(vexLoginSetting);
	}

	/**
	* Deletes the vex login setting with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanid the primary key of the vex login setting
	* @return the vex login setting that was removed
	* @throws PortalException if a vex login setting with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting deleteVexLoginSetting(
		long scanid) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteVexLoginSetting(scanid);
	}

	public static jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting fetchVexLoginSetting(
		long scanid) {
		return getService().fetchVexLoginSetting(scanid);
	}

	/**
	* Returns the vex login setting with the primary key.
	*
	* @param scanid the primary key of the vex login setting
	* @return the vex login setting
	* @throws PortalException if a vex login setting with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting getVexLoginSetting(
		long scanid) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getVexLoginSetting(scanid);
	}

	/**
	* Updates the vex login setting in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param vexLoginSetting the vex login setting
	* @return the vex login setting that was updated
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting updateVexLoginSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting vexLoginSetting) {
		return getService().updateVexLoginSetting(vexLoginSetting);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static VexLoginSettingLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VexLoginSettingLocalService, VexLoginSettingLocalService> _serviceTracker =
		ServiceTrackerFactory.open(VexLoginSettingLocalService.class);
}