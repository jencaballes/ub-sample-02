/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VexScanSettingLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see VexScanSettingLocalService
 * @generated
 */
@ProviderType
public class VexScanSettingLocalServiceWrapper
	implements VexScanSettingLocalService,
		ServiceWrapper<VexScanSettingLocalService> {
	public VexScanSettingLocalServiceWrapper(
		VexScanSettingLocalService vexScanSettingLocalService) {
		_vexScanSettingLocalService = vexScanSettingLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _vexScanSettingLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _vexScanSettingLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _vexScanSettingLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexScanSettingLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexScanSettingLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of vex scan settings.
	*
	* @return the number of vex scan settings
	*/
	@Override
	public int getVexScanSettingsCount() {
		return _vexScanSettingLocalService.getVexScanSettingsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _vexScanSettingLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _vexScanSettingLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _vexScanSettingLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _vexScanSettingLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns a range of all the vex scan settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex scan settings
	* @param end the upper bound of the range of vex scan settings (not inclusive)
	* @return the range of vex scan settings
	*/
	@Override
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting> getVexScanSettings(
		int start, int end) {
		return _vexScanSettingLocalService.getVexScanSettings(start, end);
	}

	/**
	* Adds the vex scan setting to the database. Also notifies the appropriate model listeners.
	*
	* @param vexScanSetting the vex scan setting
	* @return the vex scan setting that was added
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting addVexScanSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting vexScanSetting) {
		return _vexScanSettingLocalService.addVexScanSetting(vexScanSetting);
	}

	/**
	* Creates a new vex scan setting with the primary key. Does not add the vex scan setting to the database.
	*
	* @param scanid the primary key for the new vex scan setting
	* @return the new vex scan setting
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting createVexScanSetting(
		long scanid) {
		return _vexScanSettingLocalService.createVexScanSetting(scanid);
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting createVexScanSettingObj() {
		return _vexScanSettingLocalService.createVexScanSettingObj();
	}

	/**
	* Deletes the vex scan setting from the database. Also notifies the appropriate model listeners.
	*
	* @param vexScanSetting the vex scan setting
	* @return the vex scan setting that was removed
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting deleteVexScanSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting vexScanSetting) {
		return _vexScanSettingLocalService.deleteVexScanSetting(vexScanSetting);
	}

	/**
	* Deletes the vex scan setting with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanid the primary key of the vex scan setting
	* @return the vex scan setting that was removed
	* @throws PortalException if a vex scan setting with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting deleteVexScanSetting(
		long scanid) throws com.liferay.portal.kernel.exception.PortalException {
		return _vexScanSettingLocalService.deleteVexScanSetting(scanid);
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting fetchVexScanSetting(
		long scanid) {
		return _vexScanSettingLocalService.fetchVexScanSetting(scanid);
	}

	/**
	* Returns the vex scan setting with the primary key.
	*
	* @param scanid the primary key of the vex scan setting
	* @return the vex scan setting
	* @throws PortalException if a vex scan setting with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting getVexScanSetting(
		long scanid) throws com.liferay.portal.kernel.exception.PortalException {
		return _vexScanSettingLocalService.getVexScanSetting(scanid);
	}

	/**
	* Updates the vex scan setting in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param vexScanSetting the vex scan setting
	* @return the vex scan setting that was updated
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting updateVexScanSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting vexScanSetting) {
		return _vexScanSettingLocalService.updateVexScanSetting(vexScanSetting);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _vexScanSettingLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _vexScanSettingLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public VexScanSettingLocalService getWrappedService() {
		return _vexScanSettingLocalService;
	}

	@Override
	public void setWrappedService(
		VexScanSettingLocalService vexScanSettingLocalService) {
		_vexScanSettingLocalService = vexScanSettingLocalService;
	}

	private VexScanSettingLocalService _vexScanSettingLocalService;
}