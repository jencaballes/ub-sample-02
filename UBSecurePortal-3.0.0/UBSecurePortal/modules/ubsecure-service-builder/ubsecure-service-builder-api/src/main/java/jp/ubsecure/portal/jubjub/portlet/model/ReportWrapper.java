/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Report}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Report
 * @generated
 */
@ProviderType
public class ReportWrapper implements Report, ModelWrapper<Report> {
	public ReportWrapper(Report report) {
		_report = report;
	}

	@Override
	public Class<?> getModelClass() {
		return Report.class;
	}

	@Override
	public String getModelClassName() {
		return Report.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("reportId", getReportId());
		attributes.put("reportDate", getReportDate());
		attributes.put("reportBucketName", getReportBucketName());
		attributes.put("reportType", getReportType());
		attributes.put("reportName", getReportName());
		attributes.put("cxReportId", getCxReportId());
		attributes.put("scanId", getScanId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long reportId = (Long)attributes.get("reportId");

		if (reportId != null) {
			setReportId(reportId);
		}

		Date reportDate = (Date)attributes.get("reportDate");

		if (reportDate != null) {
			setReportDate(reportDate);
		}

		String reportBucketName = (String)attributes.get("reportBucketName");

		if (reportBucketName != null) {
			setReportBucketName(reportBucketName);
		}

		Integer reportType = (Integer)attributes.get("reportType");

		if (reportType != null) {
			setReportType(reportType);
		}

		String reportName = (String)attributes.get("reportName");

		if (reportName != null) {
			setReportName(reportName);
		}

		Long cxReportId = (Long)attributes.get("cxReportId");

		if (cxReportId != null) {
			setCxReportId(cxReportId);
		}

		Long scanId = (Long)attributes.get("scanId");

		if (scanId != null) {
			setScanId(scanId);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _report.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _report.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _report.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _report.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<jp.ubsecure.portal.jubjub.portlet.model.Report> toCacheModel() {
		return _report.toCacheModel();
	}

	@Override
	public int compareTo(jp.ubsecure.portal.jubjub.portlet.model.Report report) {
		return _report.compareTo(report);
	}

	/**
	* Returns the report type of this report.
	*
	* @return the report type of this report
	*/
	@Override
	public int getReportType() {
		return _report.getReportType();
	}

	@Override
	public int hashCode() {
		return _report.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _report.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ReportWrapper((Report)_report.clone());
	}

	/**
	* Returns the report bucket name of this report.
	*
	* @return the report bucket name of this report
	*/
	@Override
	public java.lang.String getReportBucketName() {
		return _report.getReportBucketName();
	}

	/**
	* Returns the report name of this report.
	*
	* @return the report name of this report
	*/
	@Override
	public java.lang.String getReportName() {
		return _report.getReportName();
	}

	@Override
	public java.lang.String toString() {
		return _report.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _report.toXmlString();
	}

	/**
	* Returns the report date of this report.
	*
	* @return the report date of this report
	*/
	@Override
	public Date getReportDate() {
		return _report.getReportDate();
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Report toEscapedModel() {
		return new ReportWrapper(_report.toEscapedModel());
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Report toUnescapedModel() {
		return new ReportWrapper(_report.toUnescapedModel());
	}

	/**
	* Returns the cx report ID of this report.
	*
	* @return the cx report ID of this report
	*/
	@Override
	public long getCxReportId() {
		return _report.getCxReportId();
	}

	/**
	* Returns the primary key of this report.
	*
	* @return the primary key of this report
	*/
	@Override
	public long getPrimaryKey() {
		return _report.getPrimaryKey();
	}

	/**
	* Returns the report ID of this report.
	*
	* @return the report ID of this report
	*/
	@Override
	public long getReportId() {
		return _report.getReportId();
	}

	/**
	* Returns the scan ID of this report.
	*
	* @return the scan ID of this report
	*/
	@Override
	public long getScanId() {
		return _report.getScanId();
	}

	@Override
	public void persist() {
		_report.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_report.setCachedModel(cachedModel);
	}

	/**
	* Sets the cx report ID of this report.
	*
	* @param cxReportId the cx report ID of this report
	*/
	@Override
	public void setCxReportId(long cxReportId) {
		_report.setCxReportId(cxReportId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_report.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_report.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_report.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_report.setNew(n);
	}

	/**
	* Sets the primary key of this report.
	*
	* @param primaryKey the primary key of this report
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_report.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_report.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the report bucket name of this report.
	*
	* @param reportBucketName the report bucket name of this report
	*/
	@Override
	public void setReportBucketName(java.lang.String reportBucketName) {
		_report.setReportBucketName(reportBucketName);
	}

	/**
	* Sets the report date of this report.
	*
	* @param reportDate the report date of this report
	*/
	@Override
	public void setReportDate(Date reportDate) {
		_report.setReportDate(reportDate);
	}

	/**
	* Sets the report ID of this report.
	*
	* @param reportId the report ID of this report
	*/
	@Override
	public void setReportId(long reportId) {
		_report.setReportId(reportId);
	}

	/**
	* Sets the report name of this report.
	*
	* @param reportName the report name of this report
	*/
	@Override
	public void setReportName(java.lang.String reportName) {
		_report.setReportName(reportName);
	}

	/**
	* Sets the report type of this report.
	*
	* @param reportType the report type of this report
	*/
	@Override
	public void setReportType(int reportType) {
		_report.setReportType(reportType);
	}

	/**
	* Sets the scan ID of this report.
	*
	* @param scanId the scan ID of this report
	*/
	@Override
	public void setScanId(long scanId) {
		_report.setScanId(scanId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReportWrapper)) {
			return false;
		}

		ReportWrapper reportWrapper = (ReportWrapper)obj;

		if (Objects.equals(_report, reportWrapper._report)) {
			return true;
		}

		return false;
	}

	@Override
	public Report getWrappedModel() {
		return _report;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _report.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _report.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_report.resetOriginalValues();
	}

	private final Report _report;
}