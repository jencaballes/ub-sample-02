package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class VexReportInfoSettingItem {
	private String customer_name;
	private String target_system;
	private String audit_month;
	private String audit_period;
	private String own_company_name;
	private String audit_location;
	private String audit_place;
	private String from_ip;
	private String target_env;
	private String summary;
	private Map<String, Boolean> reportOptions; 
	private List<VexTargetHostItem> target_host_list;
	private List<VexTargetFuncItem> target_function_list;
	private List<VexEnvRemarkItem> env_remark_list;
	public VexReportInfoSettingItem() {
		super();
		this.customer_name = PortalConstants.STRING_EMPTY;
		this.target_system = PortalConstants.STRING_EMPTY;
		this.audit_month = PortalConstants.STRING_EMPTY;
		this.audit_period = PortalConstants.STRING_EMPTY;
		this.own_company_name = PortalConstants.STRING_EMPTY;
		this.audit_location = PortalConstants.STRING_EMPTY;
		this.audit_place = PortalConstants.STRING_EMPTY;
		this.from_ip = PortalConstants.STRING_EMPTY;
		this.target_env = PortalConstants.STRING_EMPTY;
		this.summary = PortalConstants.STRING_EMPTY;
		this.reportOptions = initializeReportOptions();
		this.target_host_list = new ArrayList<VexTargetHostItem>();
		this.target_function_list = new ArrayList<VexTargetFuncItem>();
		this.env_remark_list = new ArrayList<VexEnvRemarkItem>();
	}
	private Map<String, Boolean> initializeReportOptions(){
		Map<String, Boolean> reportOptionsMap = new HashMap<String, Boolean>();
		reportOptionsMap.put("useCapture", PortalConstants.FALSE);
		reportOptionsMap.put("reCapture", PortalConstants.FALSE);
		reportOptionsMap.put("output_by_signature", PortalConstants.FALSE);
		reportOptionsMap.put("output_by_request", PortalConstants.FALSE);
		reportOptionsMap.put("output_vuln_request_parameters", PortalConstants.FALSE);
		reportOptionsMap.put("output_host", PortalConstants.FALSE);
		reportOptionsMap.put("outputRequestDetail", PortalConstants.FALSE);
		reportOptionsMap.put("outputVulnDetail", PortalConstants.FALSE);
		reportOptionsMap.put("output_appendix", PortalConstants.FALSE);
		reportOptionsMap.put("xls_report_split_flg", PortalConstants.FALSE);
		
		return reportOptionsMap;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getTarget_system() {
		return target_system;
	}
	public void setTarget_system(String target_system) {
		this.target_system = target_system;
	}
	public String getAudit_month() {
		return audit_month;
	}
	public void setAudit_month(String audit_month) {
		this.audit_month = audit_month;
	}
	public String getAudit_period() {
		return audit_period;
	}
	public void setAudit_period(String audit_period) {
		this.audit_period = audit_period;
	}
	public String getOwn_company_name() {
		return own_company_name;
	}
	public void setOwn_company_name(String own_company_name) {
		this.own_company_name = own_company_name;
	}
	public String getAudit_location() {
		return audit_location;
	}
	public void setAudit_location(String audit_location) {
		this.audit_location = audit_location;
	}
	public String getAudit_place() {
		return audit_place;
	}
	public void setAudit_place(String audit_place) {
		this.audit_place = audit_place;
	}
	public String getFrom_ip() {
		return from_ip;
	}
	public void setFrom_ip(String from_ip) {
		this.from_ip = from_ip;
	}
	public String getTarget_env() {
		return target_env;
	}
	public void setTarget_env(String target_env) {
		this.target_env = target_env;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public Map<String, Boolean> getReportOptions() {
		return reportOptions;
	}
	public void setReportOptions(VexReportOptionsItem reportOptionsItem) {
		if(null != reportOptionsItem){
			Map<String, Boolean> reportOptionsMap = new HashMap<String, Boolean>();
			reportOptionsMap.put("useCapture", reportOptionsItem.getUseCapture());
			reportOptionsMap.put("reCapture", reportOptionsItem.getReCapture());
			reportOptionsMap.put("output_by_signature", reportOptionsItem.getOutput_by_signature());
			reportOptionsMap.put("output_by_request", reportOptionsItem.getOutput_by_request());
			reportOptionsMap.put("output_vuln_request_parameters", reportOptionsItem.getOutput_vuln_request_parameters());
			reportOptionsMap.put("output_host", reportOptionsItem.getOutput_host());
			reportOptionsMap.put("outputRequestDetail", reportOptionsItem.getOutputRequestDetail());
			reportOptionsMap.put("outputVulnDetail", reportOptionsItem.getOutputVulnDetail());
			reportOptionsMap.put("output_appendix", reportOptionsItem.getOutput_appendix());
			reportOptionsMap.put("xls_report_split_flg", reportOptionsItem.getXls_report_split_flg());
			
			this.reportOptions = reportOptionsMap;
		} else {
			this.reportOptions = initializeReportOptions();
		}
	}
	public void setReportOptions(Map<String, Boolean> reportOptionsMap) {
		this.reportOptions = reportOptionsMap;
	}
	public List<VexTargetHostItem> getTarget_host_list() {
		return target_host_list;
	}
	public void setTarget_host_list(List<VexTargetHostItem> target_host_list) {
		this.target_host_list = target_host_list;
	}
	public List<VexTargetFuncItem> getTarget_function_list() {
		return target_function_list;
	}
	public void setTarget_function_list(List<VexTargetFuncItem> target_function_list) {
		this.target_function_list = target_function_list;
	}
	public List<VexEnvRemarkItem> getEnv_remark_list() {
		return env_remark_list;
	}
	public void setEnv_remark_list(List<VexEnvRemarkItem> env_remark_list) {
		this.env_remark_list = env_remark_list;
	}
}
