/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public interface ScanFinder {
	public java.util.List<java.lang.Object> getScans(long lProjectId,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		int iStart) throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> getEntireScans(int iType,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		int iStart) throws com.liferay.portal.kernel.exception.PortalException;

	public int getEntireScansCount(int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan)
		throws com.liferay.portal.kernel.exception.PortalException;

	public int getScansCount(long lProjectId,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan)
		throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> sortEntireScans(int iType,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		java.lang.String orderByCol, java.lang.String orderByType, int iStart)
		throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> sortScans(long lProjectId,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		java.lang.String orderByCol, java.lang.String orderByType, int iStart)
		throws com.liferay.portal.kernel.exception.PortalException;

	public int countScanWaiting(java.util.Date scanRegDate, int type)
		throws com.liferay.portal.kernel.exception.PortalException;

	public int countCrawlWaiting(java.util.Date scanRegDate, int type)
		throws com.liferay.portal.kernel.exception.PortalException;

	public int getScanStatus(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException;
}