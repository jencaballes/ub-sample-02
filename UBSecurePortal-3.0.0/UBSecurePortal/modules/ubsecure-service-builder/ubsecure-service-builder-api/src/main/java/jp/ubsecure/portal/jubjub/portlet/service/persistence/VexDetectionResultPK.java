/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class VexDetectionResultPK implements Comparable<VexDetectionResultPK>,
	Serializable {
	public long scanresultid;
	public long scanid;

	public VexDetectionResultPK() {
	}

	public VexDetectionResultPK(long scanresultid, long scanid) {
		this.scanresultid = scanresultid;
		this.scanid = scanid;
	}

	public long getScanresultid() {
		return scanresultid;
	}

	public void setScanresultid(long scanresultid) {
		this.scanresultid = scanresultid;
	}

	public long getScanid() {
		return scanid;
	}

	public void setScanid(long scanid) {
		this.scanid = scanid;
	}

	@Override
	public int compareTo(VexDetectionResultPK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (scanresultid < pk.scanresultid) {
			value = -1;
		}
		else if (scanresultid > pk.scanresultid) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (scanid < pk.scanid) {
			value = -1;
		}
		else if (scanid > pk.scanid) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexDetectionResultPK)) {
			return false;
		}

		VexDetectionResultPK pk = (VexDetectionResultPK)obj;

		if ((scanresultid == pk.scanresultid) && (scanid == pk.scanid)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		hashCode = HashUtil.hash(hashCode, scanresultid);
		hashCode = HashUtil.hash(hashCode, scanid);

		return hashCode;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("scanresultid");
		sb.append(StringPool.EQUAL);
		sb.append(scanresultid);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("scanid");
		sb.append(StringPool.EQUAL);
		sb.append(scanid);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}