package jp.ubsecure.portal.jubjub.portlet.model;

public class PresetListItem {
	
	private long presetId;
	private String presetName;
	
	public long getPresetId() {
		return presetId;
	}
	public void setPresetId(long presetId) {
		this.presetId = presetId;
	}
	public String getPresetName() {
		return presetName;
	}
	public void setPresetName(String presetName) {
		this.presetName = presetName;
	}

}
