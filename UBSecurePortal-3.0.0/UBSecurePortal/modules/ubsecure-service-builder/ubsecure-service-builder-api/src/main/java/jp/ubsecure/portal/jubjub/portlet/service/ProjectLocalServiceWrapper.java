/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProjectLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ProjectLocalService
 * @generated
 */
@ProviderType
public class ProjectLocalServiceWrapper implements ProjectLocalService,
	ServiceWrapper<ProjectLocalService> {
	public ProjectLocalServiceWrapper(ProjectLocalService projectLocalService) {
		_projectLocalService = projectLocalService;
	}

	/**
	* Set project to complete
	*
	* @param projectId
	- ID of the project to be completed
	* @return return true if transaction is successful
	* @throws PortalException
	an exception thrown if the transaction fails.
	*/
	@Override
	public boolean completeProject(long projectId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.completeProject(projectId);
	}

	/**
	* Delete Record Related to Project
	*
	* @param projectId
	- ID of the project
	* @return return true if delete transaction is successful
	* @throws PortalException
	an exception thrown if the transaction fails.
	*/
	@Override
	public boolean deleteRelatedToProject(long projectId, long lCxProjectId,
		int iType, java.lang.String strSessionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.deleteRelatedToProject(projectId,
			lCxProjectId, iType, strSessionId);
	}

	/**
	* Check if a case number is valid. A case number should be unique
	*
	* @param caseNumber
	the case number to check
	* @param projectId
	a unique ID of the project
	* @return return true if the case number is valid, otherwise return false
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public boolean isCaseNumberValid(java.lang.String caseNumber, long projectId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.isCaseNumberValid(caseNumber, projectId);
	}

	/**
	* Reopen completed project
	*
	* @param project
	- a project to be reopen
	* @return return true if transaction is successful
	* @throws PortalException
	an exception thrown if the transaction fails.
	*/
	@Override
	public boolean openProject(
		jp.ubsecure.portal.jubjub.portlet.model.Project project)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.openProject(project);
	}

	/**
	* Update certain record in DB
	*
	* @param project
	- the project to be updated
	* @param lUsersArr
	* @param bAdmin
	* @return return true if update transaction is successful
	* @throws PortalException
	an exception thrown if the transaction fails.
	*/
	@Override
	public boolean updateProject(java.lang.String strSessionId,
		jp.ubsecure.portal.jubjub.portlet.model.Project project,
		java.util.List<java.lang.String> packageNamesList, java.io.File file,
		long[] lUsersArr, boolean bAdmin)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.updateProject(strSessionId, project,
			packageNamesList, file, lUsersArr, bAdmin);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _projectLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _projectLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _projectLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of projects.
	*
	* @return the number of projects
	*/
	@Override
	public int getProjectsCount() {
		return _projectLocalService.getProjectsCount();
	}

	/**
	* Get number of projects.
	*
	* @param type
	the type of project(android, cxsuite, ios)
	* @return count - number of projects
	* @throws PortalException
	- an exception thrown
	*/
	@Override
	public int getProjectsCount(int type)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getProjectsCount(type);
	}

	/**
	* Get the number of projects
	*
	* @param type
	- type of the project(android, cxsuite, ios)
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @return count - number of projects
	*/
	@Override
	public int getSearchedProjectsCount(int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		com.liferay.portal.kernel.model.User user)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getSearchedProjectsCount(type,
			searchedProject, user);
	}

	/**
	* Get the number of projects
	*
	* @param userId
	ID of the user
	* @param type
	a type of the project(android, cxsuite, ios)
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @return PortalException - an exception thrown
	*/
	@Override
	public int getSearchedUserProjectsCount(java.lang.String userId, int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getSearchedUserProjectsCount(userId, type,
			searchedProject);
	}

	/**
	* Get the number of projects per user
	*
	* @param userId
	- ID of the User
	* @param type
	- type of the project(android, cxsuite, ios)
	* @return count - number of projects
	* @throws PortalException
	- an exception thrown
	*/
	@Override
	public int getUserProjectsCount(java.lang.String userId, int type)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getUserProjectsCount(userId, type);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _projectLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _projectLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _projectLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Get all Overall Admin Projects
	*
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @param type
	a type of the project(android, cxsuite, ios)
	* @return projectList a list of projects to be downloaded
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public java.util.List<java.lang.Object> getOverallAdminProjectsToDownload(
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		int type, com.liferay.portal.kernel.model.User user)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getOverallAdminProjectsToDownload(searchedProject,
			type, user);
	}

	/**
	* Returns a range of all the projects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @return the range of projects
	*/
	@Override
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Project> getProjects(
		int start, int end) {
		return _projectLocalService.getProjects(start, end);
	}

	/**
	* Retrieve project list.
	*
	* @param searchedProject
	a key-value parameter inputted by user during search.
	* @param user
	login user
	* @param type
	type of the project (android, cxsuite, ios)
	* @param start
	index on where the search begin
	* @return projectList list of projects
	* @throws PortalException
	- exception thrown
	*/
	@Override
	public java.util.List<java.lang.Object> getProjects(
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		com.liferay.portal.kernel.model.User user, int type, int start)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getProjects(searchedProject, user, type,
			start);
	}

	/**
	* Retrieve Project List by owner group.
	*
	* @param ownerGroup
	owner group
	* @throws PortalException
	Exception thrown
	* @return List<Project> of Projects
	*/
	@Override
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Project> getProjectsByOwnerGroup(
		long ownerGroup)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getProjectsByOwnerGroup(ownerGroup);
	}

	/**
	* Retrieve Project List by owner groups.
	*
	* @param ownerGroupList
	owner group list
	* @throws PortalException
	Exception thrown
	* @return List<Project> of Projects
	*/
	@Override
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Project> getProjectsByOwnerGroupList(
		long[] ownerGroupList)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getProjectsByOwnerGroupList(ownerGroupList);
	}

	/**
	* Get All User Projects
	*
	* @param strUserId
	- a User ID
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @param type
	a type of the project(android, cxsuite, ios)
	* @return projectList a list of projects to be downloaded
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public java.util.List<java.lang.Object> getUserProjectsToDownload(
		java.lang.String strUserId,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		int type) throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getUserProjectsToDownload(strUserId,
			searchedProject, type);
	}

	/**
	* Sort overall Projects
	*
	* @param lUserId
	user id
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @param orderByType
	type of order(ascending, descending)
	* @param orderByCol
	- a column to order
	* @param type
	a type of the project(android, cxsuite, ios)
	* @param start
	index for the start position
	* @param end
	index for the end position
	* @return projectList a sorted project list
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public java.util.List<java.lang.Object> sortOverallProjects(long lUserId,
		int iUserRole,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		java.lang.String orderByType, java.lang.String orderByCol, int type,
		int start, int end)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.sortOverallProjects(lUserId, iUserRole,
			searchedProject, orderByType, orderByCol, type, start, end);
	}

	/**
	* Sort User Projects
	*
	* @param strUserId
	- User ID
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @param orderByType
	type of order(ascending, descending)
	* @param orderByCol
	- a column to order
	* @param type
	a type of the project(android, cxsuite, ios)
	* @param start
	index for the start position
	* @param end
	index for the end position
	* @return projectList a sorted project list
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public java.util.List<java.lang.Object> sortUserProjects(
		java.lang.String strUserId,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		java.lang.String orderByType, java.lang.String orderByCol, int type,
		int start, int end)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.sortUserProjects(strUserId,
			searchedProject, orderByType, orderByCol, type, start, end);
	}

	/**
	* Adds the project to the database. Also notifies the appropriate model listeners.
	*
	* @param project the project
	* @return the project that was added
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project addProject(
		jp.ubsecure.portal.jubjub.portlet.model.Project project) {
		return _projectLocalService.addProject(project);
	}

	/**
	* Creates a new project with the primary key. Does not add the project to the database.
	*
	* @param projectId the primary key for the new project
	* @return the new project
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project createProject(
		long projectId) {
		return _projectLocalService.createProject(projectId);
	}

	/**
	* Create Project
	*
	* @return return empty Project
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project createProjectObj() {
		return _projectLocalService.createProjectObj();
	}

	/**
	* Deletes the project from the database. Also notifies the appropriate model listeners.
	*
	* @param project the project
	* @return the project that was removed
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project deleteProject(
		jp.ubsecure.portal.jubjub.portlet.model.Project project) {
		return _projectLocalService.deleteProject(project);
	}

	/**
	* Deletes the project with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectId the primary key of the project
	* @return the project that was removed
	* @throws PortalException if a project with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project deleteProject(
		long projectId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.deleteProject(projectId);
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project fetchProject(
		long projectId) {
		return _projectLocalService.fetchProject(projectId);
	}

	/**
	* Returns the project with the primary key.
	*
	* @param projectId the primary key of the project
	* @return the project
	* @throws PortalException if a project with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project getProject(
		long projectId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.getProject(projectId);
	}

	/**
	* Updates the project in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param project the project
	* @return the project that was updated
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project updateProject(
		jp.ubsecure.portal.jubjub.portlet.model.Project project) {
		return _projectLocalService.updateProject(project);
	}

	/**
	* Generate a unique project ID
	*
	* @return return a unique Project ID
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public long createProjectId()
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectLocalService.createProjectId();
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _projectLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Clears the cache for all instances of this model.
	*/
	@Override
	public void clearCache()
		throws com.liferay.portal.kernel.exception.PortalException {
		_projectLocalService.clearCache();
	}

	@Override
	public ProjectLocalService getWrappedService() {
		return _projectLocalService;
	}

	@Override
	public void setWrappedService(ProjectLocalService projectLocalService) {
		_projectLocalService = projectLocalService;
	}

	private ProjectLocalService _projectLocalService;
}