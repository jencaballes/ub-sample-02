/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the vex scan setting service. This utility wraps {@link jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexScanSettingPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexScanSettingPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexScanSettingPersistenceImpl
 * @generated
 */
@ProviderType
public class VexScanSettingUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(VexScanSetting vexScanSetting) {
		getPersistence().clearCache(vexScanSetting);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<VexScanSetting> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<VexScanSetting> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<VexScanSetting> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<VexScanSetting> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static VexScanSetting update(VexScanSetting vexScanSetting) {
		return getPersistence().update(vexScanSetting);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static VexScanSetting update(VexScanSetting vexScanSetting,
		ServiceContext serviceContext) {
		return getPersistence().update(vexScanSetting, serviceContext);
	}

	/**
	* Returns all the vex scan settings where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the matching vex scan settings
	*/
	public static List<VexScanSetting> findByScanId(long scanid) {
		return getPersistence().findByScanId(scanid);
	}

	/**
	* Returns a range of all the vex scan settings where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex scan settings
	* @param end the upper bound of the range of vex scan settings (not inclusive)
	* @return the range of matching vex scan settings
	*/
	public static List<VexScanSetting> findByScanId(long scanid, int start,
		int end) {
		return getPersistence().findByScanId(scanid, start, end);
	}

	/**
	* Returns an ordered range of all the vex scan settings where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex scan settings
	* @param end the upper bound of the range of vex scan settings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching vex scan settings
	*/
	public static List<VexScanSetting> findByScanId(long scanid, int start,
		int end, OrderByComparator<VexScanSetting> orderByComparator) {
		return getPersistence()
				   .findByScanId(scanid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the vex scan settings where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex scan settings
	* @param end the upper bound of the range of vex scan settings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching vex scan settings
	*/
	public static List<VexScanSetting> findByScanId(long scanid, int start,
		int end, OrderByComparator<VexScanSetting> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByScanId(scanid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first vex scan setting in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex scan setting
	* @throws NoSuchVexScanSettingException if a matching vex scan setting could not be found
	*/
	public static VexScanSetting findByScanId_First(long scanid,
		OrderByComparator<VexScanSetting> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexScanSettingException {
		return getPersistence().findByScanId_First(scanid, orderByComparator);
	}

	/**
	* Returns the first vex scan setting in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex scan setting, or <code>null</code> if a matching vex scan setting could not be found
	*/
	public static VexScanSetting fetchByScanId_First(long scanid,
		OrderByComparator<VexScanSetting> orderByComparator) {
		return getPersistence().fetchByScanId_First(scanid, orderByComparator);
	}

	/**
	* Returns the last vex scan setting in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex scan setting
	* @throws NoSuchVexScanSettingException if a matching vex scan setting could not be found
	*/
	public static VexScanSetting findByScanId_Last(long scanid,
		OrderByComparator<VexScanSetting> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexScanSettingException {
		return getPersistence().findByScanId_Last(scanid, orderByComparator);
	}

	/**
	* Returns the last vex scan setting in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex scan setting, or <code>null</code> if a matching vex scan setting could not be found
	*/
	public static VexScanSetting fetchByScanId_Last(long scanid,
		OrderByComparator<VexScanSetting> orderByComparator) {
		return getPersistence().fetchByScanId_Last(scanid, orderByComparator);
	}

	/**
	* Removes all the vex scan settings where scanid = &#63; from the database.
	*
	* @param scanid the scanid
	*/
	public static void removeByScanId(long scanid) {
		getPersistence().removeByScanId(scanid);
	}

	/**
	* Returns the number of vex scan settings where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the number of matching vex scan settings
	*/
	public static int countByScanId(long scanid) {
		return getPersistence().countByScanId(scanid);
	}

	/**
	* Caches the vex scan setting in the entity cache if it is enabled.
	*
	* @param vexScanSetting the vex scan setting
	*/
	public static void cacheResult(VexScanSetting vexScanSetting) {
		getPersistence().cacheResult(vexScanSetting);
	}

	/**
	* Caches the vex scan settings in the entity cache if it is enabled.
	*
	* @param vexScanSettings the vex scan settings
	*/
	public static void cacheResult(List<VexScanSetting> vexScanSettings) {
		getPersistence().cacheResult(vexScanSettings);
	}

	/**
	* Creates a new vex scan setting with the primary key. Does not add the vex scan setting to the database.
	*
	* @param scanid the primary key for the new vex scan setting
	* @return the new vex scan setting
	*/
	public static VexScanSetting create(long scanid) {
		return getPersistence().create(scanid);
	}

	/**
	* Removes the vex scan setting with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanid the primary key of the vex scan setting
	* @return the vex scan setting that was removed
	* @throws NoSuchVexScanSettingException if a vex scan setting with the primary key could not be found
	*/
	public static VexScanSetting remove(long scanid)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexScanSettingException {
		return getPersistence().remove(scanid);
	}

	public static VexScanSetting updateImpl(VexScanSetting vexScanSetting) {
		return getPersistence().updateImpl(vexScanSetting);
	}

	/**
	* Returns the vex scan setting with the primary key or throws a {@link NoSuchVexScanSettingException} if it could not be found.
	*
	* @param scanid the primary key of the vex scan setting
	* @return the vex scan setting
	* @throws NoSuchVexScanSettingException if a vex scan setting with the primary key could not be found
	*/
	public static VexScanSetting findByPrimaryKey(long scanid)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexScanSettingException {
		return getPersistence().findByPrimaryKey(scanid);
	}

	/**
	* Returns the vex scan setting with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param scanid the primary key of the vex scan setting
	* @return the vex scan setting, or <code>null</code> if a vex scan setting with the primary key could not be found
	*/
	public static VexScanSetting fetchByPrimaryKey(long scanid) {
		return getPersistence().fetchByPrimaryKey(scanid);
	}

	public static java.util.Map<java.io.Serializable, VexScanSetting> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the vex scan settings.
	*
	* @return the vex scan settings
	*/
	public static List<VexScanSetting> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the vex scan settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex scan settings
	* @param end the upper bound of the range of vex scan settings (not inclusive)
	* @return the range of vex scan settings
	*/
	public static List<VexScanSetting> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the vex scan settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex scan settings
	* @param end the upper bound of the range of vex scan settings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of vex scan settings
	*/
	public static List<VexScanSetting> findAll(int start, int end,
		OrderByComparator<VexScanSetting> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the vex scan settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex scan settings
	* @param end the upper bound of the range of vex scan settings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of vex scan settings
	*/
	public static List<VexScanSetting> findAll(int start, int end,
		OrderByComparator<VexScanSetting> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the vex scan settings from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of vex scan settings.
	*
	* @return the number of vex scan settings
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static VexScanSettingPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VexScanSettingPersistence, VexScanSettingPersistence> _serviceTracker =
		ServiceTrackerFactory.open(VexScanSettingPersistence.class);
}