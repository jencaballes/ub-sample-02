package jp.ubsecure.portal.jubjub.portlet.model;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class VexTargetHostItem {
	private String host_info;
	public VexTargetHostItem() {
		super();
		this.host_info = PortalConstants.STRING_EMPTY;
	}
	public String getHost_info() {
		return host_info;
	}
	public void setHost_info(String host_info) {
		this.host_info = host_info;
	}
}
