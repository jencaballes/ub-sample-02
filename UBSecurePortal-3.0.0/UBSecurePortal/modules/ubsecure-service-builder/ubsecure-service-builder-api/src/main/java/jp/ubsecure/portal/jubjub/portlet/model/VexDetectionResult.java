/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the VexDetectionResult service. Represents a row in the &quot;UBS_VexDetectionResult&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResultModel
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultImpl
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl
 * @generated
 */
@ImplementationClassName("jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultImpl")
@ProviderType
public interface VexDetectionResult extends VexDetectionResultModel,
	PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<VexDetectionResult, Long> SCANRESULTID_ACCESSOR =
		new Accessor<VexDetectionResult, Long>() {
			@Override
			public Long get(VexDetectionResult vexDetectionResult) {
				return vexDetectionResult.getScanresultid();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<VexDetectionResult> getTypeClass() {
				return VexDetectionResult.class;
			}
		};

	public static final Accessor<VexDetectionResult, Long> SCANID_ACCESSOR = new Accessor<VexDetectionResult, Long>() {
			@Override
			public Long get(VexDetectionResult vexDetectionResult) {
				return vexDetectionResult.getScanid();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<VexDetectionResult> getTypeClass() {
				return VexDetectionResult.class;
			}
		};
}