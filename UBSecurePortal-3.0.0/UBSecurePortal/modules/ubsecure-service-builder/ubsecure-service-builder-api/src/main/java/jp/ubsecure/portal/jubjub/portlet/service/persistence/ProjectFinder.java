/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public interface ProjectFinder {
	public java.util.List<java.lang.Object> getOverallAdminProjects(
		long lUserId, int iUserRole, int iType,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		int iStart, int iEnd)
		throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> getUserProjects(int iType,
		java.lang.String strUserId,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		int iStart, int iEnd)
		throws com.liferay.portal.kernel.exception.PortalException;

	public int getProjectsCount(int type)
		throws com.liferay.portal.kernel.exception.PortalException;

	public int getUserProjectsCount(java.lang.String userId, int type)
		throws com.liferay.portal.kernel.exception.PortalException;

	public int getSearchedProjectsCount(long lUserId, int iUserRole, int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject)
		throws com.liferay.portal.kernel.exception.PortalException;

	public int getSearchedUserProjectsCount(java.lang.String userId, int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject)
		throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> sortOverallAdminProjects(
		long lUserId, int iUserRole, int iType,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		java.lang.String orderByCol, java.lang.String orderByType, int iStart,
		int iEnd) throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> sortUserProjects(int iType,
		java.lang.String strUserId,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject,
		java.lang.String orderByType, java.lang.String orderByCol, int iStart,
		int iEnd) throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> getOverallAdminProjectsToDownload(
		long lUserId, int iUserRole, int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject)
		throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> getUserProjectsToDownload(
		int type, java.lang.String userId,
		java.util.Map<java.lang.String, java.lang.Object> searchedProject)
		throws com.liferay.portal.kernel.exception.PortalException;
}