/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link VexScanSetting}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexScanSetting
 * @generated
 */
@ProviderType
public class VexScanSettingWrapper implements VexScanSetting,
	ModelWrapper<VexScanSetting> {
	public VexScanSettingWrapper(VexScanSetting vexScanSetting) {
		_vexScanSetting = vexScanSetting;
	}

	@Override
	public Class<?> getModelClass() {
		return VexScanSetting.class;
	}

	@Override
	public String getModelClassName() {
		return VexScanSetting.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("scanid", getScanid());
		attributes.put("starturl", getStarturl());
		attributes.put("authorizedpatrolurl", getAuthorizedpatrolurl());
		attributes.put("unauthorizedpatrolurl", getUnauthorizedpatrolurl());
		attributes.put("starttime", getStarttime());
		attributes.put("endtime", getEndtime());
		attributes.put("setemailnotification", getSetemailnotification());
		attributes.put("selectedwebsignature", getSelectedwebsignature());
		attributes.put("getserverfiles", getGetserverfiles());
		attributes.put("getserversettings", getGetserversettings());
		attributes.put("selectedserverfilessignature",
			getSelectedserverfilessignature());
		attributes.put("selectedserversettingssignature",
			getSelectedserversettingssignature());
		attributes.put("maxnumdetectionlink", getMaxnumdetectionlink());
		attributes.put("maxnumdetectionlinkperpage",
			getMaxnumdetectionlinkperpage());
		attributes.put("detectionlinkdepthlimit", getDetectionlinkdepthlimit());
		attributes.put("waittime", getWaittime());
		attributes.put("numofthread", getNumofthread());
		attributes.put("timeouttime", getTimeouttime());
		attributes.put("setautopostform", getSetautopostform());
		attributes.put("setpostmethod", getSetpostmethod());
		attributes.put("requestheader", getRequestheader());
		attributes.put("loginstatusdetection", getLoginstatusdetection());
		attributes.put("setnotsendpassword", getSetnotsendpassword());
		attributes.put("paramnamesaspassword", getParamnamesaspassword());
		attributes.put("numofretryaterror", getNumofretryaterror());
		attributes.put("sessionerrordetection", getSessionerrordetection());
		attributes.put("screentransitionerrordetection",
			getScreentransitionerrordetection());
		attributes.put("implementationenvironment",
			getImplementationenvironment());
		attributes.put("setcrawlingonly", getSetcrawlingonly());
		attributes.put("excludepathinforegex", getExcludepathinforegex());
		attributes.put("excludeparameterdeletenameregex",
			getExcludeparameterdeletenameregex());
		attributes.put("excludeparameterdeleteparamregex",
			getExcludeparameterdeleteparamregex());
		attributes.put("extracturlregex", getExtracturlregex());
		attributes.put("notparseextension", getNotparseextension());
		attributes.put("notparsefilenameregex", getNotparsefilenameregex());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long scanid = (Long)attributes.get("scanid");

		if (scanid != null) {
			setScanid(scanid);
		}

		String starturl = (String)attributes.get("starturl");

		if (starturl != null) {
			setStarturl(starturl);
		}

		String authorizedpatrolurl = (String)attributes.get(
				"authorizedpatrolurl");

		if (authorizedpatrolurl != null) {
			setAuthorizedpatrolurl(authorizedpatrolurl);
		}

		String unauthorizedpatrolurl = (String)attributes.get(
				"unauthorizedpatrolurl");

		if (unauthorizedpatrolurl != null) {
			setUnauthorizedpatrolurl(unauthorizedpatrolurl);
		}

		Date starttime = (Date)attributes.get("starttime");

		if (starttime != null) {
			setStarttime(starttime);
		}

		Date endtime = (Date)attributes.get("endtime");

		if (endtime != null) {
			setEndtime(endtime);
		}

		Integer setemailnotification = (Integer)attributes.get(
				"setemailnotification");

		if (setemailnotification != null) {
			setSetemailnotification(setemailnotification);
		}

		String selectedwebsignature = (String)attributes.get(
				"selectedwebsignature");

		if (selectedwebsignature != null) {
			setSelectedwebsignature(selectedwebsignature);
		}

		Integer getserverfiles = (Integer)attributes.get("getserverfiles");

		if (getserverfiles != null) {
			setGetserverfiles(getserverfiles);
		}

		Integer getserversettings = (Integer)attributes.get("getserversettings");

		if (getserversettings != null) {
			setGetserversettings(getserversettings);
		}

		String selectedserverfilessignature = (String)attributes.get(
				"selectedserverfilessignature");

		if (selectedserverfilessignature != null) {
			setSelectedserverfilessignature(selectedserverfilessignature);
		}

		String selectedserversettingssignature = (String)attributes.get(
				"selectedserversettingssignature");

		if (selectedserversettingssignature != null) {
			setSelectedserversettingssignature(selectedserversettingssignature);
		}

		Integer maxnumdetectionlink = (Integer)attributes.get(
				"maxnumdetectionlink");

		if (maxnumdetectionlink != null) {
			setMaxnumdetectionlink(maxnumdetectionlink);
		}

		Integer maxnumdetectionlinkperpage = (Integer)attributes.get(
				"maxnumdetectionlinkperpage");

		if (maxnumdetectionlinkperpage != null) {
			setMaxnumdetectionlinkperpage(maxnumdetectionlinkperpage);
		}

		Integer detectionlinkdepthlimit = (Integer)attributes.get(
				"detectionlinkdepthlimit");

		if (detectionlinkdepthlimit != null) {
			setDetectionlinkdepthlimit(detectionlinkdepthlimit);
		}

		Integer waittime = (Integer)attributes.get("waittime");

		if (waittime != null) {
			setWaittime(waittime);
		}

		Integer numofthread = (Integer)attributes.get("numofthread");

		if (numofthread != null) {
			setNumofthread(numofthread);
		}

		Integer timeouttime = (Integer)attributes.get("timeouttime");

		if (timeouttime != null) {
			setTimeouttime(timeouttime);
		}

		Integer setautopostform = (Integer)attributes.get("setautopostform");

		if (setautopostform != null) {
			setSetautopostform(setautopostform);
		}

		Integer setpostmethod = (Integer)attributes.get("setpostmethod");

		if (setpostmethod != null) {
			setSetpostmethod(setpostmethod);
		}

		String requestheader = (String)attributes.get("requestheader");

		if (requestheader != null) {
			setRequestheader(requestheader);
		}

		String loginstatusdetection = (String)attributes.get(
				"loginstatusdetection");

		if (loginstatusdetection != null) {
			setLoginstatusdetection(loginstatusdetection);
		}

		Integer setnotsendpassword = (Integer)attributes.get(
				"setnotsendpassword");

		if (setnotsendpassword != null) {
			setSetnotsendpassword(setnotsendpassword);
		}

		String paramnamesaspassword = (String)attributes.get(
				"paramnamesaspassword");

		if (paramnamesaspassword != null) {
			setParamnamesaspassword(paramnamesaspassword);
		}

		Integer numofretryaterror = (Integer)attributes.get("numofretryaterror");

		if (numofretryaterror != null) {
			setNumofretryaterror(numofretryaterror);
		}

		String sessionerrordetection = (String)attributes.get(
				"sessionerrordetection");

		if (sessionerrordetection != null) {
			setSessionerrordetection(sessionerrordetection);
		}

		String screentransitionerrordetection = (String)attributes.get(
				"screentransitionerrordetection");

		if (screentransitionerrordetection != null) {
			setScreentransitionerrordetection(screentransitionerrordetection);
		}

		Integer implementationenvironment = (Integer)attributes.get(
				"implementationenvironment");

		if (implementationenvironment != null) {
			setImplementationenvironment(implementationenvironment);
		}

		Integer setcrawlingonly = (Integer)attributes.get("setcrawlingonly");

		if (setcrawlingonly != null) {
			setSetcrawlingonly(setcrawlingonly);
		}

		String excludepathinforegex = (String)attributes.get(
				"excludepathinforegex");

		if (excludepathinforegex != null) {
			setExcludepathinforegex(excludepathinforegex);
		}

		String excludeparameterdeletenameregex = (String)attributes.get(
				"excludeparameterdeletenameregex");

		if (excludeparameterdeletenameregex != null) {
			setExcludeparameterdeletenameregex(excludeparameterdeletenameregex);
		}

		String excludeparameterdeleteparamregex = (String)attributes.get(
				"excludeparameterdeleteparamregex");

		if (excludeparameterdeleteparamregex != null) {
			setExcludeparameterdeleteparamregex(excludeparameterdeleteparamregex);
		}

		String extracturlregex = (String)attributes.get("extracturlregex");

		if (extracturlregex != null) {
			setExtracturlregex(extracturlregex);
		}

		String notparseextension = (String)attributes.get("notparseextension");

		if (notparseextension != null) {
			setNotparseextension(notparseextension);
		}

		String notparsefilenameregex = (String)attributes.get(
				"notparsefilenameregex");

		if (notparsefilenameregex != null) {
			setNotparsefilenameregex(notparsefilenameregex);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _vexScanSetting.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _vexScanSetting.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _vexScanSetting.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _vexScanSetting.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting> toCacheModel() {
		return _vexScanSetting.toCacheModel();
	}

	@Override
	public int compareTo(
		jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting vexScanSetting) {
		return _vexScanSetting.compareTo(vexScanSetting);
	}

	/**
	* Returns the detectionlinkdepthlimit of this vex scan setting.
	*
	* @return the detectionlinkdepthlimit of this vex scan setting
	*/
	@Override
	public int getDetectionlinkdepthlimit() {
		return _vexScanSetting.getDetectionlinkdepthlimit();
	}

	/**
	* Returns the getserverfiles of this vex scan setting.
	*
	* @return the getserverfiles of this vex scan setting
	*/
	@Override
	public int getGetserverfiles() {
		return _vexScanSetting.getGetserverfiles();
	}

	/**
	* Returns the getserversettings of this vex scan setting.
	*
	* @return the getserversettings of this vex scan setting
	*/
	@Override
	public int getGetserversettings() {
		return _vexScanSetting.getGetserversettings();
	}

	/**
	* Returns the implementationenvironment of this vex scan setting.
	*
	* @return the implementationenvironment of this vex scan setting
	*/
	@Override
	public int getImplementationenvironment() {
		return _vexScanSetting.getImplementationenvironment();
	}

	/**
	* Returns the maxnumdetectionlink of this vex scan setting.
	*
	* @return the maxnumdetectionlink of this vex scan setting
	*/
	@Override
	public int getMaxnumdetectionlink() {
		return _vexScanSetting.getMaxnumdetectionlink();
	}

	/**
	* Returns the maxnumdetectionlinkperpage of this vex scan setting.
	*
	* @return the maxnumdetectionlinkperpage of this vex scan setting
	*/
	@Override
	public int getMaxnumdetectionlinkperpage() {
		return _vexScanSetting.getMaxnumdetectionlinkperpage();
	}

	/**
	* Returns the numofretryaterror of this vex scan setting.
	*
	* @return the numofretryaterror of this vex scan setting
	*/
	@Override
	public int getNumofretryaterror() {
		return _vexScanSetting.getNumofretryaterror();
	}

	/**
	* Returns the numofthread of this vex scan setting.
	*
	* @return the numofthread of this vex scan setting
	*/
	@Override
	public int getNumofthread() {
		return _vexScanSetting.getNumofthread();
	}

	/**
	* Returns the setautopostform of this vex scan setting.
	*
	* @return the setautopostform of this vex scan setting
	*/
	@Override
	public int getSetautopostform() {
		return _vexScanSetting.getSetautopostform();
	}

	/**
	* Returns the setcrawlingonly of this vex scan setting.
	*
	* @return the setcrawlingonly of this vex scan setting
	*/
	@Override
	public int getSetcrawlingonly() {
		return _vexScanSetting.getSetcrawlingonly();
	}

	/**
	* Returns the setemailnotification of this vex scan setting.
	*
	* @return the setemailnotification of this vex scan setting
	*/
	@Override
	public int getSetemailnotification() {
		return _vexScanSetting.getSetemailnotification();
	}

	/**
	* Returns the setnotsendpassword of this vex scan setting.
	*
	* @return the setnotsendpassword of this vex scan setting
	*/
	@Override
	public int getSetnotsendpassword() {
		return _vexScanSetting.getSetnotsendpassword();
	}

	/**
	* Returns the setpostmethod of this vex scan setting.
	*
	* @return the setpostmethod of this vex scan setting
	*/
	@Override
	public int getSetpostmethod() {
		return _vexScanSetting.getSetpostmethod();
	}

	/**
	* Returns the timeouttime of this vex scan setting.
	*
	* @return the timeouttime of this vex scan setting
	*/
	@Override
	public int getTimeouttime() {
		return _vexScanSetting.getTimeouttime();
	}

	/**
	* Returns the waittime of this vex scan setting.
	*
	* @return the waittime of this vex scan setting
	*/
	@Override
	public int getWaittime() {
		return _vexScanSetting.getWaittime();
	}

	@Override
	public int hashCode() {
		return _vexScanSetting.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _vexScanSetting.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new VexScanSettingWrapper((VexScanSetting)_vexScanSetting.clone());
	}

	/**
	* Returns the authorizedpatrolurl of this vex scan setting.
	*
	* @return the authorizedpatrolurl of this vex scan setting
	*/
	@Override
	public java.lang.String getAuthorizedpatrolurl() {
		return _vexScanSetting.getAuthorizedpatrolurl();
	}

	/**
	* Returns the excludeparameterdeletenameregex of this vex scan setting.
	*
	* @return the excludeparameterdeletenameregex of this vex scan setting
	*/
	@Override
	public java.lang.String getExcludeparameterdeletenameregex() {
		return _vexScanSetting.getExcludeparameterdeletenameregex();
	}

	/**
	* Returns the excludeparameterdeleteparamregex of this vex scan setting.
	*
	* @return the excludeparameterdeleteparamregex of this vex scan setting
	*/
	@Override
	public java.lang.String getExcludeparameterdeleteparamregex() {
		return _vexScanSetting.getExcludeparameterdeleteparamregex();
	}

	/**
	* Returns the excludepathinforegex of this vex scan setting.
	*
	* @return the excludepathinforegex of this vex scan setting
	*/
	@Override
	public java.lang.String getExcludepathinforegex() {
		return _vexScanSetting.getExcludepathinforegex();
	}

	/**
	* Returns the extracturlregex of this vex scan setting.
	*
	* @return the extracturlregex of this vex scan setting
	*/
	@Override
	public java.lang.String getExtracturlregex() {
		return _vexScanSetting.getExtracturlregex();
	}

	/**
	* Returns the loginstatusdetection of this vex scan setting.
	*
	* @return the loginstatusdetection of this vex scan setting
	*/
	@Override
	public java.lang.String getLoginstatusdetection() {
		return _vexScanSetting.getLoginstatusdetection();
	}

	/**
	* Returns the notparseextension of this vex scan setting.
	*
	* @return the notparseextension of this vex scan setting
	*/
	@Override
	public java.lang.String getNotparseextension() {
		return _vexScanSetting.getNotparseextension();
	}

	/**
	* Returns the notparsefilenameregex of this vex scan setting.
	*
	* @return the notparsefilenameregex of this vex scan setting
	*/
	@Override
	public java.lang.String getNotparsefilenameregex() {
		return _vexScanSetting.getNotparsefilenameregex();
	}

	/**
	* Returns the paramnamesaspassword of this vex scan setting.
	*
	* @return the paramnamesaspassword of this vex scan setting
	*/
	@Override
	public java.lang.String getParamnamesaspassword() {
		return _vexScanSetting.getParamnamesaspassword();
	}

	/**
	* Returns the requestheader of this vex scan setting.
	*
	* @return the requestheader of this vex scan setting
	*/
	@Override
	public java.lang.String getRequestheader() {
		return _vexScanSetting.getRequestheader();
	}

	/**
	* Returns the screentransitionerrordetection of this vex scan setting.
	*
	* @return the screentransitionerrordetection of this vex scan setting
	*/
	@Override
	public java.lang.String getScreentransitionerrordetection() {
		return _vexScanSetting.getScreentransitionerrordetection();
	}

	/**
	* Returns the selectedserverfilessignature of this vex scan setting.
	*
	* @return the selectedserverfilessignature of this vex scan setting
	*/
	@Override
	public java.lang.String getSelectedserverfilessignature() {
		return _vexScanSetting.getSelectedserverfilessignature();
	}

	/**
	* Returns the selectedserversettingssignature of this vex scan setting.
	*
	* @return the selectedserversettingssignature of this vex scan setting
	*/
	@Override
	public java.lang.String getSelectedserversettingssignature() {
		return _vexScanSetting.getSelectedserversettingssignature();
	}

	/**
	* Returns the selectedwebsignature of this vex scan setting.
	*
	* @return the selectedwebsignature of this vex scan setting
	*/
	@Override
	public java.lang.String getSelectedwebsignature() {
		return _vexScanSetting.getSelectedwebsignature();
	}

	/**
	* Returns the sessionerrordetection of this vex scan setting.
	*
	* @return the sessionerrordetection of this vex scan setting
	*/
	@Override
	public java.lang.String getSessionerrordetection() {
		return _vexScanSetting.getSessionerrordetection();
	}

	/**
	* Returns the starturl of this vex scan setting.
	*
	* @return the starturl of this vex scan setting
	*/
	@Override
	public java.lang.String getStarturl() {
		return _vexScanSetting.getStarturl();
	}

	/**
	* Returns the unauthorizedpatrolurl of this vex scan setting.
	*
	* @return the unauthorizedpatrolurl of this vex scan setting
	*/
	@Override
	public java.lang.String getUnauthorizedpatrolurl() {
		return _vexScanSetting.getUnauthorizedpatrolurl();
	}

	@Override
	public java.lang.String toString() {
		return _vexScanSetting.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _vexScanSetting.toXmlString();
	}

	/**
	* Returns the endtime of this vex scan setting.
	*
	* @return the endtime of this vex scan setting
	*/
	@Override
	public Date getEndtime() {
		return _vexScanSetting.getEndtime();
	}

	/**
	* Returns the starttime of this vex scan setting.
	*
	* @return the starttime of this vex scan setting
	*/
	@Override
	public Date getStarttime() {
		return _vexScanSetting.getStarttime();
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting toEscapedModel() {
		return new VexScanSettingWrapper(_vexScanSetting.toEscapedModel());
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting toUnescapedModel() {
		return new VexScanSettingWrapper(_vexScanSetting.toUnescapedModel());
	}

	/**
	* Returns the primary key of this vex scan setting.
	*
	* @return the primary key of this vex scan setting
	*/
	@Override
	public long getPrimaryKey() {
		return _vexScanSetting.getPrimaryKey();
	}

	/**
	* Returns the scanid of this vex scan setting.
	*
	* @return the scanid of this vex scan setting
	*/
	@Override
	public long getScanid() {
		return _vexScanSetting.getScanid();
	}

	@Override
	public void persist() {
		_vexScanSetting.persist();
	}

	/**
	* Sets the authorizedpatrolurl of this vex scan setting.
	*
	* @param authorizedpatrolurl the authorizedpatrolurl of this vex scan setting
	*/
	@Override
	public void setAuthorizedpatrolurl(java.lang.String authorizedpatrolurl) {
		_vexScanSetting.setAuthorizedpatrolurl(authorizedpatrolurl);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_vexScanSetting.setCachedModel(cachedModel);
	}

	/**
	* Sets the detectionlinkdepthlimit of this vex scan setting.
	*
	* @param detectionlinkdepthlimit the detectionlinkdepthlimit of this vex scan setting
	*/
	@Override
	public void setDetectionlinkdepthlimit(int detectionlinkdepthlimit) {
		_vexScanSetting.setDetectionlinkdepthlimit(detectionlinkdepthlimit);
	}

	/**
	* Sets the endtime of this vex scan setting.
	*
	* @param endtime the endtime of this vex scan setting
	*/
	@Override
	public void setEndtime(Date endtime) {
		_vexScanSetting.setEndtime(endtime);
	}

	/**
	* Sets the excludeparameterdeletenameregex of this vex scan setting.
	*
	* @param excludeparameterdeletenameregex the excludeparameterdeletenameregex of this vex scan setting
	*/
	@Override
	public void setExcludeparameterdeletenameregex(
		java.lang.String excludeparameterdeletenameregex) {
		_vexScanSetting.setExcludeparameterdeletenameregex(excludeparameterdeletenameregex);
	}

	/**
	* Sets the excludeparameterdeleteparamregex of this vex scan setting.
	*
	* @param excludeparameterdeleteparamregex the excludeparameterdeleteparamregex of this vex scan setting
	*/
	@Override
	public void setExcludeparameterdeleteparamregex(
		java.lang.String excludeparameterdeleteparamregex) {
		_vexScanSetting.setExcludeparameterdeleteparamregex(excludeparameterdeleteparamregex);
	}

	/**
	* Sets the excludepathinforegex of this vex scan setting.
	*
	* @param excludepathinforegex the excludepathinforegex of this vex scan setting
	*/
	@Override
	public void setExcludepathinforegex(java.lang.String excludepathinforegex) {
		_vexScanSetting.setExcludepathinforegex(excludepathinforegex);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_vexScanSetting.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_vexScanSetting.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_vexScanSetting.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the extracturlregex of this vex scan setting.
	*
	* @param extracturlregex the extracturlregex of this vex scan setting
	*/
	@Override
	public void setExtracturlregex(java.lang.String extracturlregex) {
		_vexScanSetting.setExtracturlregex(extracturlregex);
	}

	/**
	* Sets the getserverfiles of this vex scan setting.
	*
	* @param getserverfiles the getserverfiles of this vex scan setting
	*/
	@Override
	public void setGetserverfiles(int getserverfiles) {
		_vexScanSetting.setGetserverfiles(getserverfiles);
	}

	/**
	* Sets the getserversettings of this vex scan setting.
	*
	* @param getserversettings the getserversettings of this vex scan setting
	*/
	@Override
	public void setGetserversettings(int getserversettings) {
		_vexScanSetting.setGetserversettings(getserversettings);
	}

	/**
	* Sets the implementationenvironment of this vex scan setting.
	*
	* @param implementationenvironment the implementationenvironment of this vex scan setting
	*/
	@Override
	public void setImplementationenvironment(int implementationenvironment) {
		_vexScanSetting.setImplementationenvironment(implementationenvironment);
	}

	/**
	* Sets the loginstatusdetection of this vex scan setting.
	*
	* @param loginstatusdetection the loginstatusdetection of this vex scan setting
	*/
	@Override
	public void setLoginstatusdetection(java.lang.String loginstatusdetection) {
		_vexScanSetting.setLoginstatusdetection(loginstatusdetection);
	}

	/**
	* Sets the maxnumdetectionlink of this vex scan setting.
	*
	* @param maxnumdetectionlink the maxnumdetectionlink of this vex scan setting
	*/
	@Override
	public void setMaxnumdetectionlink(int maxnumdetectionlink) {
		_vexScanSetting.setMaxnumdetectionlink(maxnumdetectionlink);
	}

	/**
	* Sets the maxnumdetectionlinkperpage of this vex scan setting.
	*
	* @param maxnumdetectionlinkperpage the maxnumdetectionlinkperpage of this vex scan setting
	*/
	@Override
	public void setMaxnumdetectionlinkperpage(int maxnumdetectionlinkperpage) {
		_vexScanSetting.setMaxnumdetectionlinkperpage(maxnumdetectionlinkperpage);
	}

	@Override
	public void setNew(boolean n) {
		_vexScanSetting.setNew(n);
	}

	/**
	* Sets the notparseextension of this vex scan setting.
	*
	* @param notparseextension the notparseextension of this vex scan setting
	*/
	@Override
	public void setNotparseextension(java.lang.String notparseextension) {
		_vexScanSetting.setNotparseextension(notparseextension);
	}

	/**
	* Sets the notparsefilenameregex of this vex scan setting.
	*
	* @param notparsefilenameregex the notparsefilenameregex of this vex scan setting
	*/
	@Override
	public void setNotparsefilenameregex(java.lang.String notparsefilenameregex) {
		_vexScanSetting.setNotparsefilenameregex(notparsefilenameregex);
	}

	/**
	* Sets the numofretryaterror of this vex scan setting.
	*
	* @param numofretryaterror the numofretryaterror of this vex scan setting
	*/
	@Override
	public void setNumofretryaterror(int numofretryaterror) {
		_vexScanSetting.setNumofretryaterror(numofretryaterror);
	}

	/**
	* Sets the numofthread of this vex scan setting.
	*
	* @param numofthread the numofthread of this vex scan setting
	*/
	@Override
	public void setNumofthread(int numofthread) {
		_vexScanSetting.setNumofthread(numofthread);
	}

	/**
	* Sets the paramnamesaspassword of this vex scan setting.
	*
	* @param paramnamesaspassword the paramnamesaspassword of this vex scan setting
	*/
	@Override
	public void setParamnamesaspassword(java.lang.String paramnamesaspassword) {
		_vexScanSetting.setParamnamesaspassword(paramnamesaspassword);
	}

	/**
	* Sets the primary key of this vex scan setting.
	*
	* @param primaryKey the primary key of this vex scan setting
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_vexScanSetting.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_vexScanSetting.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the requestheader of this vex scan setting.
	*
	* @param requestheader the requestheader of this vex scan setting
	*/
	@Override
	public void setRequestheader(java.lang.String requestheader) {
		_vexScanSetting.setRequestheader(requestheader);
	}

	/**
	* Sets the scanid of this vex scan setting.
	*
	* @param scanid the scanid of this vex scan setting
	*/
	@Override
	public void setScanid(long scanid) {
		_vexScanSetting.setScanid(scanid);
	}

	/**
	* Sets the screentransitionerrordetection of this vex scan setting.
	*
	* @param screentransitionerrordetection the screentransitionerrordetection of this vex scan setting
	*/
	@Override
	public void setScreentransitionerrordetection(
		java.lang.String screentransitionerrordetection) {
		_vexScanSetting.setScreentransitionerrordetection(screentransitionerrordetection);
	}

	/**
	* Sets the selectedserverfilessignature of this vex scan setting.
	*
	* @param selectedserverfilessignature the selectedserverfilessignature of this vex scan setting
	*/
	@Override
	public void setSelectedserverfilessignature(
		java.lang.String selectedserverfilessignature) {
		_vexScanSetting.setSelectedserverfilessignature(selectedserverfilessignature);
	}

	/**
	* Sets the selectedserversettingssignature of this vex scan setting.
	*
	* @param selectedserversettingssignature the selectedserversettingssignature of this vex scan setting
	*/
	@Override
	public void setSelectedserversettingssignature(
		java.lang.String selectedserversettingssignature) {
		_vexScanSetting.setSelectedserversettingssignature(selectedserversettingssignature);
	}

	/**
	* Sets the selectedwebsignature of this vex scan setting.
	*
	* @param selectedwebsignature the selectedwebsignature of this vex scan setting
	*/
	@Override
	public void setSelectedwebsignature(java.lang.String selectedwebsignature) {
		_vexScanSetting.setSelectedwebsignature(selectedwebsignature);
	}

	/**
	* Sets the sessionerrordetection of this vex scan setting.
	*
	* @param sessionerrordetection the sessionerrordetection of this vex scan setting
	*/
	@Override
	public void setSessionerrordetection(java.lang.String sessionerrordetection) {
		_vexScanSetting.setSessionerrordetection(sessionerrordetection);
	}

	/**
	* Sets the setautopostform of this vex scan setting.
	*
	* @param setautopostform the setautopostform of this vex scan setting
	*/
	@Override
	public void setSetautopostform(int setautopostform) {
		_vexScanSetting.setSetautopostform(setautopostform);
	}

	/**
	* Sets the setcrawlingonly of this vex scan setting.
	*
	* @param setcrawlingonly the setcrawlingonly of this vex scan setting
	*/
	@Override
	public void setSetcrawlingonly(int setcrawlingonly) {
		_vexScanSetting.setSetcrawlingonly(setcrawlingonly);
	}

	/**
	* Sets the setemailnotification of this vex scan setting.
	*
	* @param setemailnotification the setemailnotification of this vex scan setting
	*/
	@Override
	public void setSetemailnotification(int setemailnotification) {
		_vexScanSetting.setSetemailnotification(setemailnotification);
	}

	/**
	* Sets the setnotsendpassword of this vex scan setting.
	*
	* @param setnotsendpassword the setnotsendpassword of this vex scan setting
	*/
	@Override
	public void setSetnotsendpassword(int setnotsendpassword) {
		_vexScanSetting.setSetnotsendpassword(setnotsendpassword);
	}

	/**
	* Sets the setpostmethod of this vex scan setting.
	*
	* @param setpostmethod the setpostmethod of this vex scan setting
	*/
	@Override
	public void setSetpostmethod(int setpostmethod) {
		_vexScanSetting.setSetpostmethod(setpostmethod);
	}

	/**
	* Sets the starttime of this vex scan setting.
	*
	* @param starttime the starttime of this vex scan setting
	*/
	@Override
	public void setStarttime(Date starttime) {
		_vexScanSetting.setStarttime(starttime);
	}

	/**
	* Sets the starturl of this vex scan setting.
	*
	* @param starturl the starturl of this vex scan setting
	*/
	@Override
	public void setStarturl(java.lang.String starturl) {
		_vexScanSetting.setStarturl(starturl);
	}

	/**
	* Sets the timeouttime of this vex scan setting.
	*
	* @param timeouttime the timeouttime of this vex scan setting
	*/
	@Override
	public void setTimeouttime(int timeouttime) {
		_vexScanSetting.setTimeouttime(timeouttime);
	}

	/**
	* Sets the unauthorizedpatrolurl of this vex scan setting.
	*
	* @param unauthorizedpatrolurl the unauthorizedpatrolurl of this vex scan setting
	*/
	@Override
	public void setUnauthorizedpatrolurl(java.lang.String unauthorizedpatrolurl) {
		_vexScanSetting.setUnauthorizedpatrolurl(unauthorizedpatrolurl);
	}

	/**
	* Sets the waittime of this vex scan setting.
	*
	* @param waittime the waittime of this vex scan setting
	*/
	@Override
	public void setWaittime(int waittime) {
		_vexScanSetting.setWaittime(waittime);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexScanSettingWrapper)) {
			return false;
		}

		VexScanSettingWrapper vexScanSettingWrapper = (VexScanSettingWrapper)obj;

		if (Objects.equals(_vexScanSetting,
					vexScanSettingWrapper._vexScanSetting)) {
			return true;
		}

		return false;
	}

	@Override
	public VexScanSetting getWrappedModel() {
		return _vexScanSetting;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _vexScanSetting.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _vexScanSetting.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_vexScanSetting.resetOriginalValues();
	}

	private final VexScanSetting _vexScanSetting;
}