package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.Date;

public class ResultItem {
	private long projectId;
	private long scanId;
	private String cxAndroidProjectId;
	private String cxAndroidScanId;
	private int type;
	private int status;
	private String cxRunId;
	private String projectName;
	private String fileName;
	private String hashValue;
	private String scanManager;
	private Date scanRegistrationDate;
	private int process;
	private String filePath;
	private String caseNumber;
	private String groupName; 
	private int reportCount;
	private int referenceCount;
	private int highCount; 
	private int reportThreadStat;
	private long cxReportId;
	private int reportType;
	private int reviewFlag;
	private String failedScanCause;
	
	//Added new scan variables for vex service.
	private String scanStartURL;
	private Date scanStartTime;
	private Date scanEndTime;
	private int implementationEnvironment; 
	
	public String getScanStartURL() {
		return scanStartURL;
	}
	public void setScanStartURL(String scanStartURL) {
		this.scanStartURL = scanStartURL;
	}
	public Date getScanStartTime() {
		return scanStartTime;
	}
	public void setScanStartTime(Date scanStartTime) {
		this.scanStartTime = scanStartTime;
	}
	public Date getScanEndTime() {
		return scanEndTime;
	}
	public void setScanEndTime(Date scanEndTime) {
		this.scanEndTime = scanEndTime;
	}
	public int getImplementationEnvironment() {
		return implementationEnvironment;
	}
	public void setImplementationEnvironment(int implementationEnvironment) {
		this.implementationEnvironment = implementationEnvironment;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	public long getScanId() {
		return scanId;
	}
	public void setScanId(long scanId) {
		this.scanId = scanId;
	}
	public String getCxAndroidProjectId() {
		return cxAndroidProjectId;
	}
	public void setCxAndroidProjectId(String cxAndroidProjectId) {
		this.cxAndroidProjectId = cxAndroidProjectId;
	}
	public String getCxAndroidScanId() {
		return cxAndroidScanId;
	}
	public void setCxAndroidScanId(String cxAndroidScanId) {
		this.cxAndroidScanId = cxAndroidScanId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCxRunId() {
		return cxRunId;
	}
	public void setCxRunId(String cxRunId) {
		this.cxRunId = cxRunId;
	}
	
	public String getProjectName () {
		return projectName;
	}
	
	public void setProjectName (String projectName) {
		this.projectName = projectName;
	}
	
	public String getFileName () {
		return fileName;
	}
	
	public void setFileName (String fileName) {
		this.fileName = fileName;
	}
	
	public String getHashValue () {
		return hashValue;
	}
	
	public void setHashValue (String hashValue) {
		this.hashValue = hashValue;
	}
	
	public String getScanManager () {
		return scanManager;
	}
	
	public void setScanManager (String scanManager) {
		this.scanManager = scanManager;
	}
	
	public Date getScanRegistrationDate () {
		return scanRegistrationDate;
	}
	
	public void setScanRegistrationDate (Date scanRegistrationDate) {
		this.scanRegistrationDate = scanRegistrationDate;
	}
	
	public int getProcess () {
		return process;
	}
	
	public void setProcess (int process) {
		this.process = process;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public int getReportCount () {
		return reportCount;
	}
	
	public void setReportCount (int reportCount) {
		this.reportCount = reportCount;
	}
	
	public int getReferenceCount () {
		return referenceCount;
	}
	
	public void setReferenceCount (int referenceCount) {
		this.referenceCount = referenceCount;
	}
	
	public int getHighCount () {
		return highCount;
	}
	
	public void setHighCount (int highCount) {
		this.highCount = highCount;
	}
	public int getReportThreadStat() {
		return reportThreadStat;
	}
	public void setReportThreadStat(int reportThreadStat) {
		this.reportThreadStat = reportThreadStat;
	}
	public long getCxReportId() {
		return cxReportId;
	}
	public void setCxReportId(long cxReportId) {
		this.cxReportId = cxReportId;
	}
	public int getReportType() {
		return reportType;
	}
	public void setReportType(int reportType) {
		this.reportType = reportType;
	}
	public int getReviewFlag() {
		return reviewFlag;
	}
	public void setReviewFlag(int reviewFlag) {
		this.reviewFlag = reviewFlag;
	}
	public String getFailedScanCause() {
		return failedScanCause;
	}
	public void setFailedScanCause(String failedScanCause) {
		this.failedScanCause = failedScanCause;
	}
}