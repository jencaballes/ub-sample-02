/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ProjectSoap implements Serializable {
	public static ProjectSoap toSoapModel(Project model) {
		ProjectSoap soapModel = new ProjectSoap();

		soapModel.setProjectId(model.getProjectId());
		soapModel.setType(model.getType());
		soapModel.setOwnerGroup(model.getOwnerGroup());
		soapModel.setCxAndroidProjectId(model.getCxAndroidProjectId());
		soapModel.setProjectName(model.getProjectName());
		soapModel.setCaseNumber(model.getCaseNumber());
		soapModel.setStatus(model.getStatus());
		soapModel.setAttribute(model.getAttribute());
		soapModel.setProjectEndDate(model.getProjectEndDate());
		soapModel.setProjectCreateDate(model.getProjectCreateDate());
		soapModel.setChecklistFileName(model.getChecklistFileName());
		soapModel.setPresetId(model.getPresetId());
		soapModel.setCaseName(model.getCaseName());
		soapModel.setTargetUrl(model.getTargetUrl());
		soapModel.setProductionEnvironmentUrl(model.getProductionEnvironmentUrl());
		soapModel.setSelectedWebSignature(model.getSelectedWebSignature());
		soapModel.setGetServerFiles(model.getGetServerFiles());
		soapModel.setGetServerSettings(model.getGetServerSettings());
		soapModel.setSelectedServerFilesSignature(model.getSelectedServerFilesSignature());
		soapModel.setSelectedServerSettingsSignature(model.getSelectedServerSettingsSignature());

		return soapModel;
	}

	public static ProjectSoap[] toSoapModels(Project[] models) {
		ProjectSoap[] soapModels = new ProjectSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProjectSoap[][] toSoapModels(Project[][] models) {
		ProjectSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProjectSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProjectSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProjectSoap[] toSoapModels(List<Project> models) {
		List<ProjectSoap> soapModels = new ArrayList<ProjectSoap>(models.size());

		for (Project model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProjectSoap[soapModels.size()]);
	}

	public ProjectSoap() {
	}

	public long getPrimaryKey() {
		return _projectId;
	}

	public void setPrimaryKey(long pk) {
		setProjectId(pk);
	}

	public long getProjectId() {
		return _projectId;
	}

	public void setProjectId(long projectId) {
		_projectId = projectId;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public long getOwnerGroup() {
		return _ownerGroup;
	}

	public void setOwnerGroup(long ownerGroup) {
		_ownerGroup = ownerGroup;
	}

	public String getCxAndroidProjectId() {
		return _cxAndroidProjectId;
	}

	public void setCxAndroidProjectId(String cxAndroidProjectId) {
		_cxAndroidProjectId = cxAndroidProjectId;
	}

	public String getProjectName() {
		return _projectName;
	}

	public void setProjectName(String projectName) {
		_projectName = projectName;
	}

	public String getCaseNumber() {
		return _caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		_caseNumber = caseNumber;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public int getAttribute() {
		return _attribute;
	}

	public void setAttribute(int attribute) {
		_attribute = attribute;
	}

	public Date getProjectEndDate() {
		return _projectEndDate;
	}

	public void setProjectEndDate(Date projectEndDate) {
		_projectEndDate = projectEndDate;
	}

	public Date getProjectCreateDate() {
		return _projectCreateDate;
	}

	public void setProjectCreateDate(Date projectCreateDate) {
		_projectCreateDate = projectCreateDate;
	}

	public String getChecklistFileName() {
		return _checklistFileName;
	}

	public void setChecklistFileName(String checklistFileName) {
		_checklistFileName = checklistFileName;
	}

	public long getPresetId() {
		return _presetId;
	}

	public void setPresetId(long presetId) {
		_presetId = presetId;
	}

	public String getCaseName() {
		return _caseName;
	}

	public void setCaseName(String caseName) {
		_caseName = caseName;
	}

	public String getTargetUrl() {
		return _targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		_targetUrl = targetUrl;
	}

	public String getProductionEnvironmentUrl() {
		return _productionEnvironmentUrl;
	}

	public void setProductionEnvironmentUrl(String productionEnvironmentUrl) {
		_productionEnvironmentUrl = productionEnvironmentUrl;
	}

	public String getSelectedWebSignature() {
		return _selectedWebSignature;
	}

	public void setSelectedWebSignature(String selectedWebSignature) {
		_selectedWebSignature = selectedWebSignature;
	}

	public int getGetServerFiles() {
		return _getServerFiles;
	}

	public void setGetServerFiles(int getServerFiles) {
		_getServerFiles = getServerFiles;
	}

	public int getGetServerSettings() {
		return _getServerSettings;
	}

	public void setGetServerSettings(int getServerSettings) {
		_getServerSettings = getServerSettings;
	}

	public String getSelectedServerFilesSignature() {
		return _selectedServerFilesSignature;
	}

	public void setSelectedServerFilesSignature(
		String selectedServerFilesSignature) {
		_selectedServerFilesSignature = selectedServerFilesSignature;
	}

	public String getSelectedServerSettingsSignature() {
		return _selectedServerSettingsSignature;
	}

	public void setSelectedServerSettingsSignature(
		String selectedServerSettingsSignature) {
		_selectedServerSettingsSignature = selectedServerSettingsSignature;
	}

	private long _projectId;
	private int _type;
	private long _ownerGroup;
	private String _cxAndroidProjectId;
	private String _projectName;
	private String _caseNumber;
	private int _status;
	private int _attribute;
	private Date _projectEndDate;
	private Date _projectCreateDate;
	private String _checklistFileName;
	private long _presetId;
	private String _caseName;
	private String _targetUrl;
	private String _productionEnvironmentUrl;
	private String _selectedWebSignature;
	private int _getServerFiles;
	private int _getServerSettings;
	private String _selectedServerFilesSignature;
	private String _selectedServerSettingsSignature;
}