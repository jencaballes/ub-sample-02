/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexDetectionResultException;
import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;

/**
 * The persistence interface for the vex detection result service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexDetectionResultPersistenceImpl
 * @see VexDetectionResultUtil
 * @generated
 */
@ProviderType
public interface VexDetectionResultPersistence extends BasePersistence<VexDetectionResult> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link VexDetectionResultUtil} to access the vex detection result persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the vex detection results where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the matching vex detection results
	*/
	public java.util.List<VexDetectionResult> findByScanId(long scanid);

	/**
	* Returns a range of all the vex detection results where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @return the range of matching vex detection results
	*/
	public java.util.List<VexDetectionResult> findByScanId(long scanid,
		int start, int end);

	/**
	* Returns an ordered range of all the vex detection results where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching vex detection results
	*/
	public java.util.List<VexDetectionResult> findByScanId(long scanid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator);

	/**
	* Returns an ordered range of all the vex detection results where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching vex detection results
	*/
	public java.util.List<VexDetectionResult> findByScanId(long scanid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first vex detection result in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex detection result
	* @throws NoSuchVexDetectionResultException if a matching vex detection result could not be found
	*/
	public VexDetectionResult findByScanId_First(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator)
		throws NoSuchVexDetectionResultException;

	/**
	* Returns the first vex detection result in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex detection result, or <code>null</code> if a matching vex detection result could not be found
	*/
	public VexDetectionResult fetchByScanId_First(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator);

	/**
	* Returns the last vex detection result in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex detection result
	* @throws NoSuchVexDetectionResultException if a matching vex detection result could not be found
	*/
	public VexDetectionResult findByScanId_Last(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator)
		throws NoSuchVexDetectionResultException;

	/**
	* Returns the last vex detection result in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex detection result, or <code>null</code> if a matching vex detection result could not be found
	*/
	public VexDetectionResult fetchByScanId_Last(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator);

	/**
	* Returns the vex detection results before and after the current vex detection result in the ordered set where scanid = &#63;.
	*
	* @param vexDetectionResultPK the primary key of the current vex detection result
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next vex detection result
	* @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	*/
	public VexDetectionResult[] findByScanId_PrevAndNext(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK,
		long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator)
		throws NoSuchVexDetectionResultException;

	/**
	* Removes all the vex detection results where scanid = &#63; from the database.
	*
	* @param scanid the scanid
	*/
	public void removeByScanId(long scanid);

	/**
	* Returns the number of vex detection results where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the number of matching vex detection results
	*/
	public int countByScanId(long scanid);

	/**
	* Caches the vex detection result in the entity cache if it is enabled.
	*
	* @param vexDetectionResult the vex detection result
	*/
	public void cacheResult(VexDetectionResult vexDetectionResult);

	/**
	* Caches the vex detection results in the entity cache if it is enabled.
	*
	* @param vexDetectionResults the vex detection results
	*/
	public void cacheResult(
		java.util.List<VexDetectionResult> vexDetectionResults);

	/**
	* Creates a new vex detection result with the primary key. Does not add the vex detection result to the database.
	*
	* @param vexDetectionResultPK the primary key for the new vex detection result
	* @return the new vex detection result
	*/
	public VexDetectionResult create(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK);

	/**
	* Removes the vex detection result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result that was removed
	* @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	*/
	public VexDetectionResult remove(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK)
		throws NoSuchVexDetectionResultException;

	public VexDetectionResult updateImpl(VexDetectionResult vexDetectionResult);

	/**
	* Returns the vex detection result with the primary key or throws a {@link NoSuchVexDetectionResultException} if it could not be found.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result
	* @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	*/
	public VexDetectionResult findByPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK)
		throws NoSuchVexDetectionResultException;

	/**
	* Returns the vex detection result with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result, or <code>null</code> if a vex detection result with the primary key could not be found
	*/
	public VexDetectionResult fetchByPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK);

	@Override
	public java.util.Map<java.io.Serializable, VexDetectionResult> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the vex detection results.
	*
	* @return the vex detection results
	*/
	public java.util.List<VexDetectionResult> findAll();

	/**
	* Returns a range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @return the range of vex detection results
	*/
	public java.util.List<VexDetectionResult> findAll(int start, int end);

	/**
	* Returns an ordered range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of vex detection results
	*/
	public java.util.List<VexDetectionResult> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator);

	/**
	* Returns an ordered range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of vex detection results
	*/
	public java.util.List<VexDetectionResult> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexDetectionResult> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the vex detection results from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of vex detection results.
	*
	* @return the number of vex detection results
	*/
	public int countAll();
}