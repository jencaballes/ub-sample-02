package jp.ubsecure.portal.jubjub.portlet.model;

public class IOSVulnSummaryEntry {

	private String number;
	private String groupName;
	private String projectName;
	private String caseNumber;
	private String scanId;
	private String preset;
	private String executionDate;
	private String severity;
	private String vulnerabilityName;
	private String queryName;
	private String inputName;
	private String reportId;
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getProjectName() {
		return projectName;
	}
	
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public String getCaseNumber() {
		return caseNumber;
	}
	
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
	public String getScanId() {
		return scanId;
	}
	
	public void setScanId(String scanId) {
		this.scanId = scanId;
	}
	
	public String getPreset() {
		return preset;
	}
	
	public void setPreset(String preset) {
		this.preset = preset;
	}
	
	public String getExecutionDate() {
		return executionDate;
	}
	
	public void setExecutionDate(String executionDate) {
		this.executionDate = executionDate;
	}
	
	public String getSeverity() {
		return severity;
	}
	
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	
	public String getVulnerabilityName() {
		return vulnerabilityName;
	}
	
	public void setVulnerabilityName(String vulnerabilityName) {
		this.vulnerabilityName = vulnerabilityName;
	}
	
	public String getQueryName() {
		return queryName;
	}
	
	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}
	
	public String getInputName() {
		return inputName;
	}
	
	public void setInputName(String inputName) {
		this.inputName = inputName;
	}
	
	public String getReportId() {
		return reportId;
	}
	
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
}
