package jp.ubsecure.portal.jubjub.portlet.model;

public class CsvMetaSummaryEntry {
	private String companyName;
	private String projectName;
	private String attribute;
	private String caseNumber;
	private String scanId;
	private String executionDate;
	private String process;
	private String highCount;
	private String mediumCount;
	private String lowCount;
	private String infoCount;
	private String executionTime;
	private String LOC;
	private String preset;
	private String requestReview;
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getScanId() {
		return scanId;
	}

	public void setScanId(String scanId) {
		this.scanId = scanId;
	}

	public String getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(String executionDate) {
		this.executionDate = executionDate;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getHighCount() {
		return highCount;
	}

	public void setHighCount(String highCount) {
		this.highCount = highCount;
	}

	public String getMediumCount() {
		return mediumCount;
	}

	public void setMediumCount(String mediumCount) {
		this.mediumCount = mediumCount;
	}

	public String getLowCount() {
		return lowCount;
	}

	public void setLowCount(String lowCount) {
		this.lowCount = lowCount;
	}

	public String getInfoCount() {
		return infoCount;
	}

	public void setInfoCount(String infoCount) {
		this.infoCount = infoCount;
	}

	public String getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}

	public String getLOC() {
		return LOC;
	}

	public void setLOC(String lOC) {
		LOC = lOC;
	}

	public String getPreset() {
		return preset;
	}

	public void setPreset(String preset) {
		this.preset = preset;
	}
	
	public String getRequestReview() {
		return requestReview;
	}

	public void setRequestReview(String requestReview) {
		this.requestReview = requestReview;
	}
}
