/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Scan service. Represents a row in the &quot;UBS_Scan&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ScanModel
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.ScanImpl
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl
 * @generated
 */
@ImplementationClassName("jp.ubsecure.portal.jubjub.portlet.model.impl.ScanImpl")
@ProviderType
public interface Scan extends ScanModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Scan, Long> SCAN_ID_ACCESSOR = new Accessor<Scan, Long>() {
			@Override
			public Long get(Scan scan) {
				return scan.getScanId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Scan> getTypeClass() {
				return Scan.class;
			}
		};
}