package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.ArrayList;
import java.util.List;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class VexProjectAddSettingItem {
	private String division;
	private String extensionFilterRule;
	private String filenameFilterRule;
	private String memo;
	private String owner;
	private String project_name;
	private Boolean unLoggingHandler; 
	private List<VexTargetInfoSettingItem> targetInfoList;
	public VexProjectAddSettingItem() {
		super();
		this.division = PortalConstants.STRING_EMPTY;
		this.extensionFilterRule = PortalConstants.STRING_EMPTY;
		this.filenameFilterRule = PortalConstants.STRING_EMPTY;
		this.memo = PortalConstants.STRING_EMPTY;
		this.owner = PortalConstants.STRING_EMPTY;
		this.project_name = PortalConstants.STRING_EMPTY;
		this.unLoggingHandler = PortalConstants.FALSE;
		this.targetInfoList = new ArrayList<VexTargetInfoSettingItem>();
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getExtensionFilterRule() {
		return extensionFilterRule;
	}
	public void setExtensionFilterRule(String extensionFilterRule) {
		this.extensionFilterRule = extensionFilterRule;
	}
	public String getFilenameFilterRule() {
		return filenameFilterRule;
	}
	public void setFilenameFilterRule(String filenameFilterRule) {
		this.filenameFilterRule = filenameFilterRule;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public Boolean getUnLoggingHandler() {
		return unLoggingHandler;
	}
	public void setUnLoggingHandler(Boolean unLoggingHandler) {
		this.unLoggingHandler = unLoggingHandler;
	}
	public List<VexTargetInfoSettingItem> getTargetInfoList() {
		return targetInfoList;
	}
	public void setTargetInfoList(List<VexTargetInfoSettingItem> targetInfoList) {
		this.targetInfoList = targetInfoList;
	}
}
