/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the ProjectUsers service. Represents a row in the &quot;UBS_ProjectUsers&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ProjectUsersModel
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersImpl
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersModelImpl
 * @generated
 */
@ImplementationClassName("jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersImpl")
@ProviderType
public interface ProjectUsers extends ProjectUsersModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ProjectUsers, String> USER_ID_ACCESSOR = new Accessor<ProjectUsers, String>() {
			@Override
			public String get(ProjectUsers projectUsers) {
				return projectUsers.getUserId();
			}

			@Override
			public Class<String> getAttributeClass() {
				return String.class;
			}

			@Override
			public Class<ProjectUsers> getTypeClass() {
				return ProjectUsers.class;
			}
		};

	public static final Accessor<ProjectUsers, Long> PROJECT_ID_ACCESSOR = new Accessor<ProjectUsers, Long>() {
			@Override
			public Long get(ProjectUsers projectUsers) {
				return projectUsers.getProjectId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<ProjectUsers> getTypeClass() {
				return ProjectUsers.class;
			}
		};
}