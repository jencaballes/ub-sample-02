/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexLoginSettingException;
import jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting;

/**
 * The persistence interface for the vex login setting service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexLoginSettingPersistenceImpl
 * @see VexLoginSettingUtil
 * @generated
 */
@ProviderType
public interface VexLoginSettingPersistence extends BasePersistence<VexLoginSetting> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link VexLoginSettingUtil} to access the vex login setting persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the vex login settings where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the matching vex login settings
	*/
	public java.util.List<VexLoginSetting> findByScanId(long scanid);

	/**
	* Returns a range of all the vex login settings where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex login settings
	* @param end the upper bound of the range of vex login settings (not inclusive)
	* @return the range of matching vex login settings
	*/
	public java.util.List<VexLoginSetting> findByScanId(long scanid, int start,
		int end);

	/**
	* Returns an ordered range of all the vex login settings where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex login settings
	* @param end the upper bound of the range of vex login settings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching vex login settings
	*/
	public java.util.List<VexLoginSetting> findByScanId(long scanid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexLoginSetting> orderByComparator);

	/**
	* Returns an ordered range of all the vex login settings where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex login settings
	* @param end the upper bound of the range of vex login settings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching vex login settings
	*/
	public java.util.List<VexLoginSetting> findByScanId(long scanid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexLoginSetting> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first vex login setting in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex login setting
	* @throws NoSuchVexLoginSettingException if a matching vex login setting could not be found
	*/
	public VexLoginSetting findByScanId_First(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexLoginSetting> orderByComparator)
		throws NoSuchVexLoginSettingException;

	/**
	* Returns the first vex login setting in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex login setting, or <code>null</code> if a matching vex login setting could not be found
	*/
	public VexLoginSetting fetchByScanId_First(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexLoginSetting> orderByComparator);

	/**
	* Returns the last vex login setting in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex login setting
	* @throws NoSuchVexLoginSettingException if a matching vex login setting could not be found
	*/
	public VexLoginSetting findByScanId_Last(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexLoginSetting> orderByComparator)
		throws NoSuchVexLoginSettingException;

	/**
	* Returns the last vex login setting in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex login setting, or <code>null</code> if a matching vex login setting could not be found
	*/
	public VexLoginSetting fetchByScanId_Last(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexLoginSetting> orderByComparator);

	/**
	* Removes all the vex login settings where scanid = &#63; from the database.
	*
	* @param scanid the scanid
	*/
	public void removeByScanId(long scanid);

	/**
	* Returns the number of vex login settings where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the number of matching vex login settings
	*/
	public int countByScanId(long scanid);

	/**
	* Caches the vex login setting in the entity cache if it is enabled.
	*
	* @param vexLoginSetting the vex login setting
	*/
	public void cacheResult(VexLoginSetting vexLoginSetting);

	/**
	* Caches the vex login settings in the entity cache if it is enabled.
	*
	* @param vexLoginSettings the vex login settings
	*/
	public void cacheResult(java.util.List<VexLoginSetting> vexLoginSettings);

	/**
	* Creates a new vex login setting with the primary key. Does not add the vex login setting to the database.
	*
	* @param scanid the primary key for the new vex login setting
	* @return the new vex login setting
	*/
	public VexLoginSetting create(long scanid);

	/**
	* Removes the vex login setting with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanid the primary key of the vex login setting
	* @return the vex login setting that was removed
	* @throws NoSuchVexLoginSettingException if a vex login setting with the primary key could not be found
	*/
	public VexLoginSetting remove(long scanid)
		throws NoSuchVexLoginSettingException;

	public VexLoginSetting updateImpl(VexLoginSetting vexLoginSetting);

	/**
	* Returns the vex login setting with the primary key or throws a {@link NoSuchVexLoginSettingException} if it could not be found.
	*
	* @param scanid the primary key of the vex login setting
	* @return the vex login setting
	* @throws NoSuchVexLoginSettingException if a vex login setting with the primary key could not be found
	*/
	public VexLoginSetting findByPrimaryKey(long scanid)
		throws NoSuchVexLoginSettingException;

	/**
	* Returns the vex login setting with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param scanid the primary key of the vex login setting
	* @return the vex login setting, or <code>null</code> if a vex login setting with the primary key could not be found
	*/
	public VexLoginSetting fetchByPrimaryKey(long scanid);

	@Override
	public java.util.Map<java.io.Serializable, VexLoginSetting> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the vex login settings.
	*
	* @return the vex login settings
	*/
	public java.util.List<VexLoginSetting> findAll();

	/**
	* Returns a range of all the vex login settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex login settings
	* @param end the upper bound of the range of vex login settings (not inclusive)
	* @return the range of vex login settings
	*/
	public java.util.List<VexLoginSetting> findAll(int start, int end);

	/**
	* Returns an ordered range of all the vex login settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex login settings
	* @param end the upper bound of the range of vex login settings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of vex login settings
	*/
	public java.util.List<VexLoginSetting> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexLoginSetting> orderByComparator);

	/**
	* Returns an ordered range of all the vex login settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex login settings
	* @param end the upper bound of the range of vex login settings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of vex login settings
	*/
	public java.util.List<VexLoginSetting> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexLoginSetting> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the vex login settings from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of vex login settings.
	*
	* @return the number of vex login settings
	*/
	public int countAll();
}