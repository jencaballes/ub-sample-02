package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.Date;

public class ReportItem {
	private long reportId;
	private Date reportDate;
	private String bucketName;
	private int reportType;
	private String reportName;
	private long cxReportId;
	private long scanId;
	private long projectId;
	private String caseNumber;
	private int attribute;
	private int process;
	private String groupName;
	private String cxAndroidProjectId;
	private String cxAndroidScanId;
	private int reviewFlag;
	
	public long getReportId() {
		return reportId;
	}
	
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	
	public Date getReportDate() {
		return reportDate;
	}
	
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	
	public String getBucketName() {
		return bucketName;
	}
	
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
	
	public int getReportType() {
		return reportType;
	}
	
	public void setReportType(int reportType) {
		this.reportType = reportType;
	}
	
	public String getReportName() {
		return reportName;
	}
	
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	public long getCxReportId() {
		return cxReportId;
	}
	
	public void setCxReportId(long cxReportId) {
		this.cxReportId = cxReportId;
	}
	
	public long getScanId() {
		return scanId;
	}
	
	public void setScanId(long scanId) {
		this.scanId = scanId;
	}
	
	public long getProjectId() {
		return projectId;
	}
	
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	
	public String getCaseNumber() {
		return caseNumber;
	}
	
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
	public int getProcess() {
		return process;
	}
	
	public void setProcess(int process) {
		this.process = process;
	}

	public int getAttribute() {
		return attribute;
	}

	public void setAttribute(int attribute) {
		this.attribute = attribute;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getCxAndroidProjectId() {
		return cxAndroidProjectId;
	}

	public void setCxAndroidProjectId(String cxAndroidProjectId) {
		this.cxAndroidProjectId = cxAndroidProjectId;
	}

	public String getCxAndroidScanId() {
		return cxAndroidScanId;
	}

	public void setCxAndroidScanId(String cxAndroidScanId) {
		this.cxAndroidScanId = cxAndroidScanId;
	}
	
	public int getReviewFlag() {
		return reviewFlag;
	}

	public void setReviewFlag(int reviewFlag) {
		this.reviewFlag = reviewFlag;
	}
}
