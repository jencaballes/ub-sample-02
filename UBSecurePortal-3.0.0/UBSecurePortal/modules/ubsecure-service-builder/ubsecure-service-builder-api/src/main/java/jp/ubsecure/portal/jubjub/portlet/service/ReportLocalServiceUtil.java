/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Report. This utility wraps
 * {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ReportLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ReportLocalService
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.ReportLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.ReportLocalServiceImpl
 * @generated
 */
@ProviderType
public class ReportLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ReportLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of reports.
	*
	* @return the number of reports
	*/
	public static int getReportsCount() {
		return getService().getReportsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Get CSV Reports
	*
	* @param projectType
	- a type of project(android, cxsuite, ios)
	* @param calStartDate
	- project start date
	* @param calEndDate
	- project end date
	* @return reportList list of project reports
	* @throws PortalException
	an exception thrown
	*/
	public static java.util.List<java.lang.Object> getCSVReportsByProjectTypeStartDateEndDate(
		int projectType, java.util.Calendar calStartDate,
		java.util.Calendar calEndDate)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .getCSVReportsByProjectTypeStartDateEndDate(projectType,
			calStartDate, calEndDate);
	}

	/**
	* Get Reports base on scan id
	*
	* @param lScanId
	scan id of the scan reports
	* @return List of reports
	* @throws PortalException
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Report> getReportByScanId(
		long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getReportByScanId(lScanId);
	}

	/**
	* Returns a range of all the reports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of reports
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Report> getReports(
		int start, int end) {
		return getService().getReports(start, end);
	}

	/**
	* Get Reports
	*
	* @param lScanId
	scan id of the scan reports
	* @return List of reports
	* @throws PortalException
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Report> getReports(
		long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getReports(lScanId);
	}

	/**
	* Get list of reports
	*
	* @param lScanId
	* @param reportTypeArr
	a type of reports(PDF_REPORT(1), XML_REPORT(2),
	CSV_META_INFORMATION(3), CSV_VULNERABILITY(4),
	ANALYZE_REPORT(5);)
	* @param type
	type of report project(android, cxsuite, ios)
	* @return List of Reports
	* @throws PortalException
	an exception thrown
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Report> getReports(
		long lScanId, int[] reportTypeArr, int status, int type)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getReports(lScanId, reportTypeArr, status, type);
	}

	/**
	* Get reports based on report type, project type, start date and end date
	*
	* @param reportType
	type of report
	(PDF_REPORT,XML_REPORT,CSV_META_INFORMATION,CSV_VULNERABILITY,
	ANALYZE_REPORT)
	* @param projectType
	a type of a project(android, cxsuite, ios)
	* @param calStartDate
	project start date
	* @param calEndDate
	project end date
	* @return List of reports
	* @throws PortalException
	*/
	public static java.util.List<java.lang.Object> getReportsByReportTypeProjectTypeStartDateEndDate(
		int reportType, int projectType, java.util.Calendar calStartDate,
		java.util.Calendar calEndDate)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .getReportsByReportTypeProjectTypeStartDateEndDate(reportType,
			projectType, calStartDate, calEndDate);
	}

	/**
	* Adds the report to the database. Also notifies the appropriate model listeners.
	*
	* @param report the report
	* @return the report that was added
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Report addReport(
		jp.ubsecure.portal.jubjub.portlet.model.Report report) {
		return getService().addReport(report);
	}

	/**
	* Register a report
	*
	* @param lScanId
	id of the scan
	* @param iReportType
	type of report
	(PDF_REPORT,XML_REPORT,CSV_META_INFORMATION,CSV_VULNERABILITY,
	ANALYZE_REPORT)
	* @return Report
	* @throws PortalException
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Report addReport(
		long lScanId, int iReportType)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().addReport(lScanId, iReportType);
	}

	/**
	* Creates a new report with the primary key. Does not add the report to the database.
	*
	* @param reportId the primary key for the new report
	* @return the new report
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Report createReport(
		long reportId) {
		return getService().createReport(reportId);
	}

	/**
	* Deletes the report from the database. Also notifies the appropriate model listeners.
	*
	* @param report the report
	* @return the report that was removed
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Report deleteReport(
		jp.ubsecure.portal.jubjub.portlet.model.Report report) {
		return getService().deleteReport(report);
	}

	/**
	* Deletes the report with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reportId the primary key of the report
	* @return the report that was removed
	* @throws PortalException if a report with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Report deleteReport(
		long reportId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteReport(reportId);
	}

	public static jp.ubsecure.portal.jubjub.portlet.model.Report fetchReport(
		long reportId) {
		return getService().fetchReport(reportId);
	}

	/**
	* Get references
	*
	* @param lScanId
	id of the scan
	* @param reportType
	type of report
	(PDF_REPORT,XML_REPORT,CSV_META_INFORMATION,CSV_VULNERABILITY,
	ANALYZE_REPORT)
	* @return Report
	* @throws PortalException
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Report getReferences(
		long lScanId, int reportType, int status)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getReferences(lScanId, reportType, status);
	}

	/**
	* Returns the report with the primary key.
	*
	* @param reportId the primary key of the report
	* @return the report
	* @throws PortalException if a report with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Report getReport(
		long reportId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getReport(reportId);
	}

	/**
	* Updates the report in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param report the report
	* @return the report that was updated
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Report updateReport(
		jp.ubsecure.portal.jubjub.portlet.model.Report report) {
		return getService().updateReport(report);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Get Max Report ID
	*
	* @return reportID
	* @throws PortalException
	*/
	public static long getMaxReportId()
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getMaxReportId();
	}

	public static ReportLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ReportLocalService, ReportLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ReportLocalService.class);
}