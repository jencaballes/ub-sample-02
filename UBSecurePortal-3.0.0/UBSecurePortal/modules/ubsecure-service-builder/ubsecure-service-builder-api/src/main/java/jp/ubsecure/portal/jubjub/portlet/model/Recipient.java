package jp.ubsecure.portal.jubjub.portlet.model;

import javax.mail.internet.InternetAddress;

public class Recipient {

	private InternetAddress emailAddress;
	private String username;
	
	public InternetAddress getEmailAddress () {
		return emailAddress;
	}
	
	public void setEmailAddress (InternetAddress emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getUsername () {
		return username;
	}
	
	public void setUsername (String username) {
		this.username = username;
	}
}
