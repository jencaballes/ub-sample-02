package jp.ubsecure.portal.jubjub.portlet.model;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class VexProjectDnsItem {
	private String ip;	 
	private String name;
	public VexProjectDnsItem() {
		super();
		this.ip = PortalConstants.STRING_EMPTY;
		this.name = PortalConstants.STRING_EMPTY;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
