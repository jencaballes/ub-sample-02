/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class VexDetectionResultSoap implements Serializable {
	public static VexDetectionResultSoap toSoapModel(VexDetectionResult model) {
		VexDetectionResultSoap soapModel = new VexDetectionResultSoap();

		soapModel.setScanresultid(model.getScanresultid());
		soapModel.setScanid(model.getScanid());
		soapModel.setDetectionresultid(model.getDetectionresultid());
		soapModel.setRisklevel(model.getRisklevel());
		soapModel.setCategory(model.getCategory());
		soapModel.setOverview(model.getOverview());
		soapModel.setFunctionname(model.getFunctionname());
		soapModel.setUrl(model.getUrl());
		soapModel.setParametername(model.getParametername());
		soapModel.setDetectionjudgment(model.getDetectionjudgment());
		soapModel.setReviewcomment(model.getReviewcomment());
		soapModel.setIsresendinvex(model.getIsresendinvex());
		soapModel.setListofdetectedresult(model.getListofdetectedresult());
		soapModel.setListofinspectiondatetime(model.getListofinspectiondatetime());

		return soapModel;
	}

	public static VexDetectionResultSoap[] toSoapModels(
		VexDetectionResult[] models) {
		VexDetectionResultSoap[] soapModels = new VexDetectionResultSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static VexDetectionResultSoap[][] toSoapModels(
		VexDetectionResult[][] models) {
		VexDetectionResultSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new VexDetectionResultSoap[models.length][models[0].length];
		}
		else {
			soapModels = new VexDetectionResultSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static VexDetectionResultSoap[] toSoapModels(
		List<VexDetectionResult> models) {
		List<VexDetectionResultSoap> soapModels = new ArrayList<VexDetectionResultSoap>(models.size());

		for (VexDetectionResult model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new VexDetectionResultSoap[soapModels.size()]);
	}

	public VexDetectionResultSoap() {
	}

	public VexDetectionResultPK getPrimaryKey() {
		return new VexDetectionResultPK(_scanresultid, _scanid);
	}

	public void setPrimaryKey(VexDetectionResultPK pk) {
		setScanresultid(pk.scanresultid);
		setScanid(pk.scanid);
	}

	public long getScanresultid() {
		return _scanresultid;
	}

	public void setScanresultid(long scanresultid) {
		_scanresultid = scanresultid;
	}

	public long getScanid() {
		return _scanid;
	}

	public void setScanid(long scanid) {
		_scanid = scanid;
	}

	public String getDetectionresultid() {
		return _detectionresultid;
	}

	public void setDetectionresultid(String detectionresultid) {
		_detectionresultid = detectionresultid;
	}

	public int getRisklevel() {
		return _risklevel;
	}

	public void setRisklevel(int risklevel) {
		_risklevel = risklevel;
	}

	public String getCategory() {
		return _category;
	}

	public void setCategory(String category) {
		_category = category;
	}

	public String getOverview() {
		return _overview;
	}

	public void setOverview(String overview) {
		_overview = overview;
	}

	public String getFunctionname() {
		return _functionname;
	}

	public void setFunctionname(String functionname) {
		_functionname = functionname;
	}

	public String getUrl() {
		return _url;
	}

	public void setUrl(String url) {
		_url = url;
	}

	public String getParametername() {
		return _parametername;
	}

	public void setParametername(String parametername) {
		_parametername = parametername;
	}

	public String getDetectionjudgment() {
		return _detectionjudgment;
	}

	public void setDetectionjudgment(String detectionjudgment) {
		_detectionjudgment = detectionjudgment;
	}

	public String getReviewcomment() {
		return _reviewcomment;
	}

	public void setReviewcomment(String reviewcomment) {
		_reviewcomment = reviewcomment;
	}

	public int getIsresendinvex() {
		return _isresendinvex;
	}

	public void setIsresendinvex(int isresendinvex) {
		_isresendinvex = isresendinvex;
	}

	public String getListofdetectedresult() {
		return _listofdetectedresult;
	}

	public void setListofdetectedresult(String listofdetectedresult) {
		_listofdetectedresult = listofdetectedresult;
	}

	public String getListofinspectiondatetime() {
		return _listofinspectiondatetime;
	}

	public void setListofinspectiondatetime(String listofinspectiondatetime) {
		_listofinspectiondatetime = listofinspectiondatetime;
	}

	private long _scanresultid;
	private long _scanid;
	private String _detectionresultid;
	private int _risklevel;
	private String _category;
	private String _overview;
	private String _functionname;
	private String _url;
	private String _parametername;
	private String _detectionjudgment;
	private String _reviewcomment;
	private int _isresendinvex;
	private String _listofdetectedresult;
	private String _listofinspectiondatetime;
}