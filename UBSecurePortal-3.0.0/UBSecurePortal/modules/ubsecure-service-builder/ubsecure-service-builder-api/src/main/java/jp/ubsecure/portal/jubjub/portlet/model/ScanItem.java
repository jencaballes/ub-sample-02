package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.Date;
import java.util.List;

public class ScanItem {
	private long projectId;
	private long scanId;
	private String projectName;
	private int projectType;
	private int projectStatus;
	private String fileName;
	private String hashValue;
	private String filePath;
	private String scanManager;
	private Date scanRegistrationDate;
	private int scanStatus;
	private int process;
	private int highCount;
	private int reportCount;
	private int referenceCount;
	private Date projectEndDate;
	private String cxAndroidScanId;
	private int reviewFlag;
	private String failedScanCause;
	
	//Added new scan variables for vex service.
	private String scanStartURL;
	private Date scanStartTime;
	private Date scanEndTime;
	private int implementationEnvironment; 

	// Added new Scan variable for Vex Details 
	private String scanAuthorizedPatrolURL; 
	private int setEmailNotification;
	private String loginUrl;
	private String protocol; 
	private String host; 
	private int port;
	
	private List<VexLoginSettingItem> loginSetting = null;
	private int maximumDetectionLinks;
	
	public List<VexLoginSettingItem> getLoginSetting() {
		return loginSetting;
	}

	public void setLoginSetting(List<VexLoginSettingItem> loginSetting) {
		this.loginSetting = loginSetting;
	}
	
	public int getImplementationEnvironment() {
		return implementationEnvironment;
	}

	public void setImplementationEnvironment(int implementationEnvironment) {
		this.implementationEnvironment = implementationEnvironment;
	}

	public String getScanStartURL() {
		return scanStartURL;
	}

	public void setScanStartURL(String scanStartURL) {
		this.scanStartURL = scanStartURL;
	}

	public Date getScanStartTime() {
		return scanStartTime;
	}

	public void setScanStartTime(Date scanStartTime) {
		this.scanStartTime = scanStartTime;
	}

	public Date getScanEndTime() {
		return scanEndTime;
	}

	public void setScanEndTime(Date scanEndTime) {
		this.scanEndTime = scanEndTime;
	}
	
	public long getProjectId () {
		return projectId;
	}
	
	public void setProjectId (long projectId) {
		this.projectId = projectId;
	}
	
	public long getScanId () {
		return scanId;
	}
	
	public void setScanId (long scanId) {
		this.scanId = scanId;
	}
	
	public String getProjectName () {
		return projectName;
	}
	
	public void setProjectName (String projectName) {
		this.projectName = projectName;
	}
	
	public int getProjectType() {
		return projectType;
	}

	public void setProjectType(int projectType) {
		this.projectType = projectType;
	}

	public int getProjectStatus () {
		return projectStatus;
	}
	
	public void setProjectStatus (int projectStatus) {
		this.projectStatus = projectStatus;
	}
	
	public String getFileName () {
		return fileName;
	}
	
	public void setFileName (String fileName) {
		this.fileName = fileName;
	}
	
	public String getHashValue () {
		return hashValue;
	}
	
	public void setHashValue (String hashValue) {
		this.hashValue = hashValue;
	}
	
	public String getFilePath () {
		return filePath;
	}
	
	public void setFilePath (String filePath) {
		this.filePath = filePath;
	}
	
	public String getScanManager () {
		return scanManager;
	}
	
	public void setScanManager (String scanManager) {
		this.scanManager = scanManager;
	}
	
	public Date getScanRegistrationDate () {
		return scanRegistrationDate;
	}
	
	public void setScanRegistrationDate (Date scanRegistrationDate) {
		this.scanRegistrationDate = scanRegistrationDate;
	}
	
	public int getScanStatus () {
		return scanStatus;
	}
	
	public void setScanStatus (int scanStatus) {
		this.scanStatus = scanStatus;
	}
	
	public int getProcess () {
		return process;
	}
	
	public void setProcess (int process) {
		this.process = process;
	}
	
	public int getHighCount () {
		return highCount;
	}
	
	public void setHighCount (int highCount) {
		this.highCount = highCount;
	}
	
	public int getReportCount () {
		return reportCount;
	}
	
	public void setReportCount (int reportCount) {
		this.reportCount = reportCount;
	}
	
	public int getReferenceCount () {
		return referenceCount;
	}
	
	public void setReferenceCount (int referenceCount) {
		this.referenceCount = referenceCount;
	}
	
	public Date getProjectEndDate () {
		return projectEndDate;
	}
	
	public void setProjectEndDate (Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}
	
	public String getCxAndroidScanId () {
		return cxAndroidScanId;
	}
	
	public void setCxAndroidScanId (String cxAndroidScanId) {
		this.cxAndroidScanId = cxAndroidScanId;
	}
	
	public int getReviewFlag () {
		return reviewFlag;
	}
	
	public void setReviewFlag (int reviewFlag) {
		this.reviewFlag = reviewFlag;
	}

	public String getFailedScanCause() {
		return failedScanCause;
	}

	public void setFailedScanCause(String failedScanCause) {
		this.failedScanCause = failedScanCause;
	}

	public String getScanAuthorizedPatrolURL() {
		return scanAuthorizedPatrolURL;
	}

	public void setScanAuthorizedPatrolURL(String scanAuthorizedPatrolURL) {
		this.scanAuthorizedPatrolURL = scanAuthorizedPatrolURL;
	}

	public int getSetEmailNotification() {
		return setEmailNotification;
	}

	public void setSetEmailNotification(int setEmailNotification) {
		this.setEmailNotification = setEmailNotification;
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getMaximumDetectionLinks() {
		return maximumDetectionLinks;
	}

	public void setMaximumDetectionLinks(int maximumDetectionLinks) {
		this.maximumDetectionLinks = maximumDetectionLinks;
	}
}
