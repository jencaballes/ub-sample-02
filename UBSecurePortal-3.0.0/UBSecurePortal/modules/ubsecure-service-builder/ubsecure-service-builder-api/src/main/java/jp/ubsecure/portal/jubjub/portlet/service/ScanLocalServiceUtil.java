/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Scan. This utility wraps
 * {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ScanLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ScanLocalService
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.ScanLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.ScanLocalServiceImpl
 * @generated
 */
@ProviderType
public class ScanLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ScanLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Cancel a review request
	*
	* @param lScanId
	id of scan to cancel the request review
	* @return returns true if cancel transaction is successful
	* @throws PortalException
	an exception thrown
	*/
	public static boolean cancelReview(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().cancelReview(lScanId);
	}

	/**
	* Delete existing scan
	*
	* @param lScanId
	scan id to be deleted
	* @return returns true if delete transaction is successful
	* @throws PortalException
	*/
	public static boolean deleteScan(int iType, long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteScan(iType, lScanId);
	}

	/**
	* Check if scan is deleted in cx server
	*
	* @param scanId
	id of the scan to be checked
	* @return return true if the scan is deleted in cx scan server, else return
	false
	* @throws PortalException
	an exception thrown
	*/
	public static boolean isDeletedScanInCxServer(long scanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().isDeletedScanInCxServer(scanId);
	}

	/**
	* Request for review
	*
	* @param lScanId
	id of the scan to be requested for review
	* @return returns true if request review transaction is successful
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public static boolean requestReview(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().requestReview(lScanId);
	}

	/**
	* Update existing scans
	*
	* @param iUserAction
	action of the user
	* @param scan
	the scan to be updated
	* @return return true if update transaction is successful
	* @throws PortalException
	an exception thrown if transaction fails
	*/
	public static boolean updateScan(int iUserAction,
		jp.ubsecure.portal.jubjub.portlet.model.Scan scan)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().updateScan(iUserAction, scan);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Get the number of scans waiting
	*
	* @param scanRegDate
	the registration date of the scan
	* @param type
	a type of project(android, cxsuite, ios)
	* @return Number of scans waiting
	* @throws PortalException
	*/
	public static int countCrawlWaiting(java.util.Date scanRegDate, int type)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().countCrawlWaiting(scanRegDate, type);
	}

	/**
	* Get the number of scans waiting
	*
	* @param scanRegDate
	the registration date of the scan
	* @param type
	a type of project(android, cxsuite, ios)
	* @return Number of scans waiting
	* @throws PortalException
	*/
	public static int countScanWaiting(java.util.Date scanRegDate, int type)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().countScanWaiting(scanRegDate, type);
	}

	/**
	* Get the total Number of All scans in all projects for each project type
	* (android, cxsuite, ios)
	*
	* @param type
	a type of the project (android, cxsuite, ios)
	* @param searchedScan
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public static int getEntireScansCount(int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getEntireScansCount(type, searchedScan);
	}

	/**
	* Get Scan Status
	*
	* @param lScanId
	id of the scan
	* @return returns a status of the scan
	* @throws PortalException
	an exception thrown
	*/
	public static int getScanStatus(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getScanStatus(lScanId);
	}

	/**
	* Returns the number of scans.
	*
	* @return the number of scans
	*/
	public static int getScansCount() {
		return getService().getScansCount();
	}

	/**
	* Get the number of scans
	*
	* @param lProjectId
	- id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public static int getScansCount(long lProjectId,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getScansCount(lProjectId, searchedScan);
	}

	/**
	* Get File path
	*
	* @param lScanId
	scan ID
	* @return filepath
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public static java.lang.String getFilePath(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getFilePath(lScanId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Retrieve the scan registragion date
	*
	* @param lScanId
	id of the scan
	* @return Date
	*/
	public static java.util.Date getScanRegistrationDate(long lScanId) {
		return getService().getScanRegistrationDate(lScanId);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Get All Scans
	*
	* @param type
	a type of the project (android, cxsuite, ios)
	* @param searchedScan
	a key-value parameter inputted by user during search.
	* @param start
	a start index position
	* @return scanList list of the entire scan
	* @throws PortalException
	an exception thrown if error occurs.
	*/
	public static java.util.List<java.lang.Object> getEntireScans(int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		int start) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getEntireScans(type, searchedScan, start);
	}

	/**
	* Returns a range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of scans
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Scan> getScans(
		int start, int end) {
		return getService().getScans(start, end);
	}

	/**
	* Get List of Scans
	*
	* @param lProjectId
	- id of the project on which project scans should be retrieved
	* @param searchedScan
	- a key-value parameter inputted by user during search.
	* @param iStart
	- start index position
	* @return scanList - list of Project scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public static java.util.List<java.lang.Object> getScans(long lProjectId,
		com.liferay.portal.kernel.model.User user,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		int iStart) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getScans(lProjectId, user, searchedScan, iStart);
	}

	/**
	* Get Scans Per Project
	*
	* @param projectId
	id of the project on where to get the scans
	* @return scanList list of scans
	* @throws PortalException
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Scan> getScansByProjectId(
		long projectId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getScansByProjectId(projectId);
	}

	/**
	* Sort Entire Scans
	*
	* @param type
	a type of project (android, cxsuite, ios)
	* @param searchedScan
	a key-value parameter inputted by user during search.
	* @param orderByCol
	a column to order
	* @param orderByType
	type of order(ascending, descending)
	* @param start
	index for the start position
	* @return scanList - sorted scan list
	* @throws PortalException
	*/
	public static java.util.List<java.lang.Object> sortEntireScans(int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		java.lang.String orderByCol, java.lang.String orderByType, int start)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .sortEntireScans(type, searchedScan, orderByCol,
			orderByType, start);
	}

	/**
	* Sort Scans
	*
	* @param lProjectId
	id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @param orderByCol
	a column to order
	* @param orderByType
	type of order(ascending, descending)
	* @param iStart
	index for the start position
	* @return scanList a sorted scan list
	* @throws PortalException
	*/
	public static java.util.List<java.lang.Object> sortScans(long lProjectId,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		java.lang.String orderByCol, java.lang.String orderByType, int iStart)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .sortScans(lProjectId, searchedScan, orderByCol,
			orderByType, iStart);
	}

	/**
	* Adds the scan to the database. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was added
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan addScan(
		jp.ubsecure.portal.jubjub.portlet.model.Scan scan) {
		return getService().addScan(scan);
	}

	/**
	* Creates a new scan with the primary key. Does not add the scan to the database.
	*
	* @param scanId the primary key for the new scan
	* @return the new scan
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan createScan(
		long scanId) {
		return getService().createScan(scanId);
	}

	/**
	* Create empty scan object
	*
	* @return scan object
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan createScanObj() {
		return getService().createScanObj();
	}

	/**
	* Deletes the scan from the database. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was removed
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan deleteScan(
		jp.ubsecure.portal.jubjub.portlet.model.Scan scan) {
		return getService().deleteScan(scan);
	}

	/**
	* Deletes the scan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanId the primary key of the scan
	* @return the scan that was removed
	* @throws PortalException if a scan with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan deleteScan(
		long scanId) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteScan(scanId);
	}

	public static jp.ubsecure.portal.jubjub.portlet.model.Scan fetchScan(
		long scanId) {
		return getService().fetchScan(scanId);
	}

	/**
	* Returns the scan with the primary key.
	*
	* @param scanId the primary key of the scan
	* @return the scan
	* @throws PortalException if a scan with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan getScan(
		long scanId) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getScan(scanId);
	}

	/**
	* Get Scan
	*
	* @param lScanId
	id of the scan to retrieve
	* @return Scan object
	* @throws PortalException
	an exception thrown
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan retrieveScan(
		long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().retrieveScan(lScanId);
	}

	/**
	* Update Android scan status
	*
	* @param lScanId
	id of the scan to be updated
	* @param iStatus
	the status to be set
	* @return returns the updated scan
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan updateAndroidScanStatus(
		long lScanId, int iStatus)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().updateAndroidScanStatus(lScanId, iStatus);
	}

	/**
	* Updates the scan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was updated
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.Scan updateScan(
		jp.ubsecure.portal.jubjub.portlet.model.Scan scan) {
		return getService().updateScan(scan);
	}

	/**
	* Generate a unique scan ID
	*
	* @return returns a unique scan id
	* @throws PortalException
	an exception thrown
	*/
	public static long createScanId()
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().createScanId();
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ScanLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ScanLocalService, ScanLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ScanLocalService.class);
}