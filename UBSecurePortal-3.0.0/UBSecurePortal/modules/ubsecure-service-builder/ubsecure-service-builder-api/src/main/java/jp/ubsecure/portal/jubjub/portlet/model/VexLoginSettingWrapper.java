/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link VexLoginSetting}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexLoginSetting
 * @generated
 */
@ProviderType
public class VexLoginSettingWrapper implements VexLoginSetting,
	ModelWrapper<VexLoginSetting> {
	public VexLoginSettingWrapper(VexLoginSetting vexLoginSetting) {
		_vexLoginSetting = vexLoginSetting;
	}

	@Override
	public Class<?> getModelClass() {
		return VexLoginSetting.class;
	}

	@Override
	public String getModelClassName() {
		return VexLoginSetting.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("scanid", getScanid());
		attributes.put("loginurl", getLoginurl());
		attributes.put("paramname", getParamname());
		attributes.put("paramvalue", getParamvalue());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long scanid = (Long)attributes.get("scanid");

		if (scanid != null) {
			setScanid(scanid);
		}

		String loginurl = (String)attributes.get("loginurl");

		if (loginurl != null) {
			setLoginurl(loginurl);
		}

		String paramname = (String)attributes.get("paramname");

		if (paramname != null) {
			setParamname(paramname);
		}

		String paramvalue = (String)attributes.get("paramvalue");

		if (paramvalue != null) {
			setParamvalue(paramvalue);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _vexLoginSetting.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _vexLoginSetting.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _vexLoginSetting.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _vexLoginSetting.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting> toCacheModel() {
		return _vexLoginSetting.toCacheModel();
	}

	@Override
	public int compareTo(
		jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting vexLoginSetting) {
		return _vexLoginSetting.compareTo(vexLoginSetting);
	}

	@Override
	public int hashCode() {
		return _vexLoginSetting.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _vexLoginSetting.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new VexLoginSettingWrapper((VexLoginSetting)_vexLoginSetting.clone());
	}

	/**
	* Returns the loginurl of this vex login setting.
	*
	* @return the loginurl of this vex login setting
	*/
	@Override
	public java.lang.String getLoginurl() {
		return _vexLoginSetting.getLoginurl();
	}

	/**
	* Returns the paramname of this vex login setting.
	*
	* @return the paramname of this vex login setting
	*/
	@Override
	public java.lang.String getParamname() {
		return _vexLoginSetting.getParamname();
	}

	/**
	* Returns the paramvalue of this vex login setting.
	*
	* @return the paramvalue of this vex login setting
	*/
	@Override
	public java.lang.String getParamvalue() {
		return _vexLoginSetting.getParamvalue();
	}

	@Override
	public java.lang.String toString() {
		return _vexLoginSetting.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _vexLoginSetting.toXmlString();
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting toEscapedModel() {
		return new VexLoginSettingWrapper(_vexLoginSetting.toEscapedModel());
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting toUnescapedModel() {
		return new VexLoginSettingWrapper(_vexLoginSetting.toUnescapedModel());
	}

	/**
	* Returns the primary key of this vex login setting.
	*
	* @return the primary key of this vex login setting
	*/
	@Override
	public long getPrimaryKey() {
		return _vexLoginSetting.getPrimaryKey();
	}

	/**
	* Returns the scanid of this vex login setting.
	*
	* @return the scanid of this vex login setting
	*/
	@Override
	public long getScanid() {
		return _vexLoginSetting.getScanid();
	}

	@Override
	public void persist() {
		_vexLoginSetting.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_vexLoginSetting.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_vexLoginSetting.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_vexLoginSetting.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_vexLoginSetting.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the loginurl of this vex login setting.
	*
	* @param loginurl the loginurl of this vex login setting
	*/
	@Override
	public void setLoginurl(java.lang.String loginurl) {
		_vexLoginSetting.setLoginurl(loginurl);
	}

	@Override
	public void setNew(boolean n) {
		_vexLoginSetting.setNew(n);
	}

	/**
	* Sets the paramname of this vex login setting.
	*
	* @param paramname the paramname of this vex login setting
	*/
	@Override
	public void setParamname(java.lang.String paramname) {
		_vexLoginSetting.setParamname(paramname);
	}

	/**
	* Sets the paramvalue of this vex login setting.
	*
	* @param paramvalue the paramvalue of this vex login setting
	*/
	@Override
	public void setParamvalue(java.lang.String paramvalue) {
		_vexLoginSetting.setParamvalue(paramvalue);
	}

	/**
	* Sets the primary key of this vex login setting.
	*
	* @param primaryKey the primary key of this vex login setting
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_vexLoginSetting.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_vexLoginSetting.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the scanid of this vex login setting.
	*
	* @param scanid the scanid of this vex login setting
	*/
	@Override
	public void setScanid(long scanid) {
		_vexLoginSetting.setScanid(scanid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexLoginSettingWrapper)) {
			return false;
		}

		VexLoginSettingWrapper vexLoginSettingWrapper = (VexLoginSettingWrapper)obj;

		if (Objects.equals(_vexLoginSetting,
					vexLoginSettingWrapper._vexLoginSetting)) {
			return true;
		}

		return false;
	}

	@Override
	public VexLoginSetting getWrappedModel() {
		return _vexLoginSetting;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _vexLoginSetting.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _vexLoginSetting.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_vexLoginSetting.resetOriginalValues();
	}

	private final VexLoginSetting _vexLoginSetting;
}