/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class VexTargetInformationSoap implements Serializable {
	public static VexTargetInformationSoap toSoapModel(
		VexTargetInformation model) {
		VexTargetInformationSoap soapModel = new VexTargetInformationSoap();

		soapModel.setScanid(model.getScanid());
		soapModel.setProtocol(model.getProtocol());
		soapModel.setHost(model.getHost());
		soapModel.setPort(model.getPort());
		soapModel.setHttpversion(model.getHttpversion());
		soapModel.setSetkeepaliveconnection(model.getSetkeepaliveconnection());
		soapModel.setSetresponsecontentlength(model.getSetresponsecontentlength());
		soapModel.setUseacceptencodingheader(model.getUseacceptencodingheader());
		soapModel.setUnzipresponse(model.getUnzipresponse());
		soapModel.setHttpprotocol(model.getHttpprotocol());
		soapModel.setExternalproxyhost(model.getExternalproxyhost());
		soapModel.setExternalproxyport(model.getExternalproxyport());
		soapModel.setExternalproxyauthid(model.getExternalproxyauthid());
		soapModel.setExternalproxyauthpassword(model.getExternalproxyauthpassword());
		soapModel.setUseclientcertificate(model.getUseclientcertificate());
		soapModel.setCertificatefile(model.getCertificatefile());
		soapModel.setCertificatefilepassword(model.getCertificatefilepassword());
		soapModel.setNtlmauthid(model.getNtlmauthid());
		soapModel.setNtlmauthpassword(model.getNtlmauthpassword());
		soapModel.setNtlmauthdomain(model.getNtlmauthdomain());
		soapModel.setNtlmauthhost(model.getNtlmauthhost());
		soapModel.setDigestauthid(model.getDigestauthid());
		soapModel.setDigestauthpassword(model.getDigestauthpassword());
		soapModel.setBasicauthid(model.getBasicauthid());
		soapModel.setBasicauthpassword(model.getBasicauthpassword());
		soapModel.setAccessexclusionpath(model.getAccessexclusionpath());

		return soapModel;
	}

	public static VexTargetInformationSoap[] toSoapModels(
		VexTargetInformation[] models) {
		VexTargetInformationSoap[] soapModels = new VexTargetInformationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static VexTargetInformationSoap[][] toSoapModels(
		VexTargetInformation[][] models) {
		VexTargetInformationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new VexTargetInformationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new VexTargetInformationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static VexTargetInformationSoap[] toSoapModels(
		List<VexTargetInformation> models) {
		List<VexTargetInformationSoap> soapModels = new ArrayList<VexTargetInformationSoap>(models.size());

		for (VexTargetInformation model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new VexTargetInformationSoap[soapModels.size()]);
	}

	public VexTargetInformationSoap() {
	}

	public long getPrimaryKey() {
		return _scanid;
	}

	public void setPrimaryKey(long pk) {
		setScanid(pk);
	}

	public long getScanid() {
		return _scanid;
	}

	public void setScanid(long scanid) {
		_scanid = scanid;
	}

	public String getProtocol() {
		return _protocol;
	}

	public void setProtocol(String protocol) {
		_protocol = protocol;
	}

	public String getHost() {
		return _host;
	}

	public void setHost(String host) {
		_host = host;
	}

	public int getPort() {
		return _port;
	}

	public void setPort(int port) {
		_port = port;
	}

	public int getHttpversion() {
		return _httpversion;
	}

	public void setHttpversion(int httpversion) {
		_httpversion = httpversion;
	}

	public int getSetkeepaliveconnection() {
		return _setkeepaliveconnection;
	}

	public void setSetkeepaliveconnection(int setkeepaliveconnection) {
		_setkeepaliveconnection = setkeepaliveconnection;
	}

	public int getSetresponsecontentlength() {
		return _setresponsecontentlength;
	}

	public void setSetresponsecontentlength(int setresponsecontentlength) {
		_setresponsecontentlength = setresponsecontentlength;
	}

	public int getUseacceptencodingheader() {
		return _useacceptencodingheader;
	}

	public void setUseacceptencodingheader(int useacceptencodingheader) {
		_useacceptencodingheader = useacceptencodingheader;
	}

	public int getUnzipresponse() {
		return _unzipresponse;
	}

	public void setUnzipresponse(int unzipresponse) {
		_unzipresponse = unzipresponse;
	}

	public String getHttpprotocol() {
		return _httpprotocol;
	}

	public void setHttpprotocol(String httpprotocol) {
		_httpprotocol = httpprotocol;
	}

	public String getExternalproxyhost() {
		return _externalproxyhost;
	}

	public void setExternalproxyhost(String externalproxyhost) {
		_externalproxyhost = externalproxyhost;
	}

	public int getExternalproxyport() {
		return _externalproxyport;
	}

	public void setExternalproxyport(int externalproxyport) {
		_externalproxyport = externalproxyport;
	}

	public String getExternalproxyauthid() {
		return _externalproxyauthid;
	}

	public void setExternalproxyauthid(String externalproxyauthid) {
		_externalproxyauthid = externalproxyauthid;
	}

	public String getExternalproxyauthpassword() {
		return _externalproxyauthpassword;
	}

	public void setExternalproxyauthpassword(String externalproxyauthpassword) {
		_externalproxyauthpassword = externalproxyauthpassword;
	}

	public int getUseclientcertificate() {
		return _useclientcertificate;
	}

	public void setUseclientcertificate(int useclientcertificate) {
		_useclientcertificate = useclientcertificate;
	}

	public String getCertificatefile() {
		return _certificatefile;
	}

	public void setCertificatefile(String certificatefile) {
		_certificatefile = certificatefile;
	}

	public String getCertificatefilepassword() {
		return _certificatefilepassword;
	}

	public void setCertificatefilepassword(String certificatefilepassword) {
		_certificatefilepassword = certificatefilepassword;
	}

	public String getNtlmauthid() {
		return _ntlmauthid;
	}

	public void setNtlmauthid(String ntlmauthid) {
		_ntlmauthid = ntlmauthid;
	}

	public String getNtlmauthpassword() {
		return _ntlmauthpassword;
	}

	public void setNtlmauthpassword(String ntlmauthpassword) {
		_ntlmauthpassword = ntlmauthpassword;
	}

	public String getNtlmauthdomain() {
		return _ntlmauthdomain;
	}

	public void setNtlmauthdomain(String ntlmauthdomain) {
		_ntlmauthdomain = ntlmauthdomain;
	}

	public String getNtlmauthhost() {
		return _ntlmauthhost;
	}

	public void setNtlmauthhost(String ntlmauthhost) {
		_ntlmauthhost = ntlmauthhost;
	}

	public String getDigestauthid() {
		return _digestauthid;
	}

	public void setDigestauthid(String digestauthid) {
		_digestauthid = digestauthid;
	}

	public String getDigestauthpassword() {
		return _digestauthpassword;
	}

	public void setDigestauthpassword(String digestauthpassword) {
		_digestauthpassword = digestauthpassword;
	}

	public String getBasicauthid() {
		return _basicauthid;
	}

	public void setBasicauthid(String basicauthid) {
		_basicauthid = basicauthid;
	}

	public String getBasicauthpassword() {
		return _basicauthpassword;
	}

	public void setBasicauthpassword(String basicauthpassword) {
		_basicauthpassword = basicauthpassword;
	}

	public String getAccessexclusionpath() {
		return _accessexclusionpath;
	}

	public void setAccessexclusionpath(String accessexclusionpath) {
		_accessexclusionpath = accessexclusionpath;
	}

	private long _scanid;
	private String _protocol;
	private String _host;
	private int _port;
	private int _httpversion;
	private int _setkeepaliveconnection;
	private int _setresponsecontentlength;
	private int _useacceptencodingheader;
	private int _unzipresponse;
	private String _httpprotocol;
	private String _externalproxyhost;
	private int _externalproxyport;
	private String _externalproxyauthid;
	private String _externalproxyauthpassword;
	private int _useclientcertificate;
	private String _certificatefile;
	private String _certificatefilepassword;
	private String _ntlmauthid;
	private String _ntlmauthpassword;
	private String _ntlmauthdomain;
	private String _ntlmauthhost;
	private String _digestauthid;
	private String _digestauthpassword;
	private String _basicauthid;
	private String _basicauthpassword;
	private String _accessexclusionpath;
}