package jp.ubsecure.portal.jubjub.portlet.model;

public class DetectionResultSummaryData {

	String scanresultid;
    String detectionresultid;
    String risklevel;
    String category;
    String overview;
    String functionname;
    String url;
    String parametername;
	
	/**
	 * @return the scanresultid
	 */
	public String getScanresultid() {
		return scanresultid;
	}
	/**
	 * @param scanresultid the scanresultid to set
	 */
	public void setScanresultid(String scanresultid) {
		this.scanresultid = scanresultid;
	}
	/**
	 * @return the detectionresultid
	 */
	public String getDetectionresultid() {
		return detectionresultid;
	}
	/**
	 * @param detectionresultid the detectionresultid to set
	 */
	public void setDetectionresultid(String detectionresultid) {
		this.detectionresultid = detectionresultid;
	}
	/**
	 * @return the risklevel
	 */
	public String getRisklevel() {
		return risklevel;
	}
	/**
	 * @param risklevel the risklevel to set
	 */
	public void setRisklevel(String risklevel) {
		this.risklevel = risklevel;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the overview
	 */
	public String getOverview() {
		return overview;
	}
	/**
	 * @param overview the overview to set
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}
	/**
	 * @return the functionname
	 */
	public String getFunctionname() {
		return functionname;
	}
	/**
	 * @param functionname the functionname to set
	 */
	public void setFunctionname(String functionname) {
		this.functionname = functionname;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the parametername
	 */
	public String getParametername() {
		return parametername;
	}
	/**
	 * @param parametername the parametername to set
	 */
	public void setParametername(String parametername) {
		this.parametername = parametername;
	}
}
