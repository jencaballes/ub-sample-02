package jp.ubsecure.portal.jubjub.portlet.model;

public class VulnCsvEntry {
	private String number;
	private String riskLevel;
	private String vulnerabilityName;
	private String signatureId;
	private String category;
	private String fileName;
	private String trigger1;
	private String trigger2;
	private String trigger3;
	private String reportId;
	private String exclusionFlag;
	private String comment;
	
	private final String STRING_EMPTY = "";
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = isStringNullOrEmpty(number) ? STRING_EMPTY : number;
	}
	
	public String getRiskLevel() {
		return riskLevel;
	}
	
	public void setRiskLevel(String riskLevel) {
		this.riskLevel = isStringNullOrEmpty(riskLevel) ? STRING_EMPTY : riskLevel;
	}
	
	public String getVulnerabilityName() {
		return vulnerabilityName;
	}
	
	public void setVulnerabilityName(String vulnerabilityName) {
		this.vulnerabilityName = isStringNullOrEmpty(vulnerabilityName) ? STRING_EMPTY : vulnerabilityName;
	}
	
	public String getSignatureId() {
		return signatureId;
	}
	
	public void setSignatureId(String signatureId) {
		this.signatureId = isStringNullOrEmpty(signatureId) ? STRING_EMPTY : signatureId;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = isStringNullOrEmpty(category) ? STRING_EMPTY : category;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = isStringNullOrEmpty(fileName) ? STRING_EMPTY : fileName;
	}
	
	public String getTrigger1() {
		return trigger1;
	}
	
	public void setTrigger1(String trigger1) {
		this.trigger1 = isStringNullOrEmpty(trigger1) ? STRING_EMPTY : trigger1;
	}
	
	public String getTrigger2() {
		return trigger2;
	}
	
	public void setTrigger2(String trigger2) {
		this.trigger2 = isStringNullOrEmpty(trigger2) ? STRING_EMPTY : trigger2;
	}
	
	public String getTrigger3() {
		return trigger3;
	}
	
	public void setTrigger3(String trigger3) {
		this.trigger3 = isStringNullOrEmpty(trigger3) ? STRING_EMPTY : trigger3;
	}
	
	public String getReportId() {
		return reportId;
	}
	
	public void setReportId(String reportId) {
		this.reportId = isStringNullOrEmpty(reportId) ? STRING_EMPTY : reportId;
	}
	
	public String getExclusionFlag() {
		return exclusionFlag;
	}
	
	public void setExclusionFlag(String exclusionFlag) {
		this.exclusionFlag = isStringNullOrEmpty(exclusionFlag) ? STRING_EMPTY : exclusionFlag;
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = isStringNullOrEmpty(comment) ? STRING_EMPTY : comment;
	}
	
	private boolean isStringNullOrEmpty(String string) {
		return string == null || string.isEmpty();
	}
}
