package jp.ubsecure.portal.jubjub.portlet.model;

public class CsvMetaSummaryData {
	private String projectName;
	private String scanId;
	private String executionDate;
	private String highCount;
	private String mediumCount;
	private String lowCount;
	private String infoCount;
	private String executionTime;
	private String LOC;
	private String preset;
	
	public String getProjectName() {
		return projectName;
	}
	
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getScanId() {
		return scanId;
	}

	public void setScanId(String scanId) {
		this.scanId = scanId;
	}

	public String getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(String executionDate) {
		this.executionDate = executionDate;
	}

	public String getHighCount() {
		return highCount;
	}

	public void setHighCount(String highCount) {
		this.highCount = highCount;
	}

	public String getMediumCount() {
		return mediumCount;
	}

	public void setMediumCount(String mediumCount) {
		this.mediumCount = mediumCount;
	}

	public String getLowCount() {
		return lowCount;
	}

	public void setLowCount(String lowCount) {
		this.lowCount = lowCount;
	}

	public String getInfoCount() {
		return infoCount;
	}

	public void setInfoCount(String infoCount) {
		this.infoCount = infoCount;
	}

	public String getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}

	public String getLOC() {
		return LOC;
	}

	public void setLOC(String lOC) {
		LOC = lOC;
	}

	public String getPreset() {
		return preset;
	}

	public void setPreset(String preset) {
		this.preset = preset;
	}
}
