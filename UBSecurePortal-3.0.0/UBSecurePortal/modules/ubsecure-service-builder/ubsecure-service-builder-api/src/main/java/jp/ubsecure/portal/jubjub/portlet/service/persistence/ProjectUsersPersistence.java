/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;

/**
 * The persistence interface for the project users service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ProjectUsersPersistenceImpl
 * @see ProjectUsersUtil
 * @generated
 */
@ProviderType
public interface ProjectUsersPersistence extends BasePersistence<ProjectUsers> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectUsersUtil} to access the project users persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the project userses where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the matching project userses
	*/
	public java.util.List<ProjectUsers> findByProjectId(long projectId);

	/**
	* Returns a range of all the project userses where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @return the range of matching project userses
	*/
	public java.util.List<ProjectUsers> findByProjectId(long projectId,
		int start, int end);

	/**
	* Returns an ordered range of all the project userses where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project userses
	*/
	public java.util.List<ProjectUsers> findByProjectId(long projectId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator);

	/**
	* Returns an ordered range of all the project userses where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project userses
	*/
	public java.util.List<ProjectUsers> findByProjectId(long projectId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project users in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project users
	* @throws NoSuchProjectUsersException if a matching project users could not be found
	*/
	public ProjectUsers findByProjectId_First(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException;

	/**
	* Returns the first project users in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project users, or <code>null</code> if a matching project users could not be found
	*/
	public ProjectUsers fetchByProjectId_First(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator);

	/**
	* Returns the last project users in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project users
	* @throws NoSuchProjectUsersException if a matching project users could not be found
	*/
	public ProjectUsers findByProjectId_Last(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException;

	/**
	* Returns the last project users in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project users, or <code>null</code> if a matching project users could not be found
	*/
	public ProjectUsers fetchByProjectId_Last(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator);

	/**
	* Returns the project userses before and after the current project users in the ordered set where projectId = &#63;.
	*
	* @param projectUsersPK the primary key of the current project users
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project users
	* @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	*/
	public ProjectUsers[] findByProjectId_PrevAndNext(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK,
		long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException;

	/**
	* Removes all the project userses where projectId = &#63; from the database.
	*
	* @param projectId the project ID
	*/
	public void removeByProjectId(long projectId);

	/**
	* Returns the number of project userses where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the number of matching project userses
	*/
	public int countByProjectId(long projectId);

	/**
	* Returns all the project userses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching project userses
	*/
	public java.util.List<ProjectUsers> findByUserId(java.lang.String userId);

	/**
	* Returns a range of all the project userses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @return the range of matching project userses
	*/
	public java.util.List<ProjectUsers> findByUserId(java.lang.String userId,
		int start, int end);

	/**
	* Returns an ordered range of all the project userses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project userses
	*/
	public java.util.List<ProjectUsers> findByUserId(java.lang.String userId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator);

	/**
	* Returns an ordered range of all the project userses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project userses
	*/
	public java.util.List<ProjectUsers> findByUserId(java.lang.String userId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project users in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project users
	* @throws NoSuchProjectUsersException if a matching project users could not be found
	*/
	public ProjectUsers findByUserId_First(java.lang.String userId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException;

	/**
	* Returns the first project users in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project users, or <code>null</code> if a matching project users could not be found
	*/
	public ProjectUsers fetchByUserId_First(java.lang.String userId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator);

	/**
	* Returns the last project users in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project users
	* @throws NoSuchProjectUsersException if a matching project users could not be found
	*/
	public ProjectUsers findByUserId_Last(java.lang.String userId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException;

	/**
	* Returns the last project users in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project users, or <code>null</code> if a matching project users could not be found
	*/
	public ProjectUsers fetchByUserId_Last(java.lang.String userId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator);

	/**
	* Returns the project userses before and after the current project users in the ordered set where userId = &#63;.
	*
	* @param projectUsersPK the primary key of the current project users
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project users
	* @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	*/
	public ProjectUsers[] findByUserId_PrevAndNext(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK,
		java.lang.String userId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException;

	/**
	* Removes all the project userses where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public void removeByUserId(java.lang.String userId);

	/**
	* Returns the number of project userses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching project userses
	*/
	public int countByUserId(java.lang.String userId);

	/**
	* Caches the project users in the entity cache if it is enabled.
	*
	* @param projectUsers the project users
	*/
	public void cacheResult(ProjectUsers projectUsers);

	/**
	* Caches the project userses in the entity cache if it is enabled.
	*
	* @param projectUserses the project userses
	*/
	public void cacheResult(java.util.List<ProjectUsers> projectUserses);

	/**
	* Creates a new project users with the primary key. Does not add the project users to the database.
	*
	* @param projectUsersPK the primary key for the new project users
	* @return the new project users
	*/
	public ProjectUsers create(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK);

	/**
	* Removes the project users with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users that was removed
	* @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	*/
	public ProjectUsers remove(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK)
		throws NoSuchProjectUsersException;

	public ProjectUsers updateImpl(ProjectUsers projectUsers);

	/**
	* Returns the project users with the primary key or throws a {@link NoSuchProjectUsersException} if it could not be found.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users
	* @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	*/
	public ProjectUsers findByPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK)
		throws NoSuchProjectUsersException;

	/**
	* Returns the project users with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users, or <code>null</code> if a project users with the primary key could not be found
	*/
	public ProjectUsers fetchByPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK);

	@Override
	public java.util.Map<java.io.Serializable, ProjectUsers> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the project userses.
	*
	* @return the project userses
	*/
	public java.util.List<ProjectUsers> findAll();

	/**
	* Returns a range of all the project userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @return the range of project userses
	*/
	public java.util.List<ProjectUsers> findAll(int start, int end);

	/**
	* Returns an ordered range of all the project userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project userses
	*/
	public java.util.List<ProjectUsers> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator);

	/**
	* Returns an ordered range of all the project userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project userses
	*/
	public java.util.List<ProjectUsers> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the project userses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of project userses.
	*
	* @return the number of project userses
	*/
	public int countAll();
}