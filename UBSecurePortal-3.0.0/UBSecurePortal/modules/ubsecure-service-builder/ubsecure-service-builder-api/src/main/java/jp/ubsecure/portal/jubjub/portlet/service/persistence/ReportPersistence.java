/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException;
import jp.ubsecure.portal.jubjub.portlet.model.Report;

/**
 * The persistence interface for the report service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ReportPersistenceImpl
 * @see ReportUtil
 * @generated
 */
@ProviderType
public interface ReportPersistence extends BasePersistence<Report> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ReportUtil} to access the report persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the reports where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @return the matching reports
	*/
	public java.util.List<Report> findByScanId(long scanId);

	/**
	* Returns a range of all the reports where scanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of matching reports
	*/
	public java.util.List<Report> findByScanId(long scanId, int start, int end);

	/**
	* Returns an ordered range of all the reports where scanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reports
	*/
	public java.util.List<Report> findByScanId(long scanId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns an ordered range of all the reports where scanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reports
	*/
	public java.util.List<Report> findByScanId(long scanId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first report in the ordered set where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public Report findByScanId_First(long scanId,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Returns the first report in the ordered set where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report, or <code>null</code> if a matching report could not be found
	*/
	public Report fetchByScanId_First(long scanId,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns the last report in the ordered set where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public Report findByScanId_Last(long scanId,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Returns the last report in the ordered set where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report, or <code>null</code> if a matching report could not be found
	*/
	public Report fetchByScanId_Last(long scanId,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns the reports before and after the current report in the ordered set where scanId = &#63;.
	*
	* @param reportId the primary key of the current report
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next report
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public Report[] findByScanId_PrevAndNext(long reportId, long scanId,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Removes all the reports where scanId = &#63; from the database.
	*
	* @param scanId the scan ID
	*/
	public void removeByScanId(long scanId);

	/**
	* Returns the number of reports where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @return the number of matching reports
	*/
	public int countByScanId(long scanId);

	/**
	* Returns all the reports where reportType = &#63;.
	*
	* @param reportType the report type
	* @return the matching reports
	*/
	public java.util.List<Report> findByReportType(int reportType);

	/**
	* Returns a range of all the reports where reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of matching reports
	*/
	public java.util.List<Report> findByReportType(int reportType, int start,
		int end);

	/**
	* Returns an ordered range of all the reports where reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reports
	*/
	public java.util.List<Report> findByReportType(int reportType, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns an ordered range of all the reports where reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reports
	*/
	public java.util.List<Report> findByReportType(int reportType, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first report in the ordered set where reportType = &#63;.
	*
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public Report findByReportType_First(int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Returns the first report in the ordered set where reportType = &#63;.
	*
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report, or <code>null</code> if a matching report could not be found
	*/
	public Report fetchByReportType_First(int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns the last report in the ordered set where reportType = &#63;.
	*
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public Report findByReportType_Last(int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Returns the last report in the ordered set where reportType = &#63;.
	*
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report, or <code>null</code> if a matching report could not be found
	*/
	public Report fetchByReportType_Last(int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns the reports before and after the current report in the ordered set where reportType = &#63;.
	*
	* @param reportId the primary key of the current report
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next report
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public Report[] findByReportType_PrevAndNext(long reportId, int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Removes all the reports where reportType = &#63; from the database.
	*
	* @param reportType the report type
	*/
	public void removeByReportType(int reportType);

	/**
	* Returns the number of reports where reportType = &#63;.
	*
	* @param reportType the report type
	* @return the number of matching reports
	*/
	public int countByReportType(int reportType);

	/**
	* Returns all the reports where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @return the matching reports
	*/
	public java.util.List<Report> findByScanIdReportType(long scanId,
		int reportType);

	/**
	* Returns a range of all the reports where scanId = &#63; and reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of matching reports
	*/
	public java.util.List<Report> findByScanIdReportType(long scanId,
		int reportType, int start, int end);

	/**
	* Returns an ordered range of all the reports where scanId = &#63; and reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reports
	*/
	public java.util.List<Report> findByScanIdReportType(long scanId,
		int reportType, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns an ordered range of all the reports where scanId = &#63; and reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reports
	*/
	public java.util.List<Report> findByScanIdReportType(long scanId,
		int reportType, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public Report findByScanIdReportType_First(long scanId, int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Returns the first report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report, or <code>null</code> if a matching report could not be found
	*/
	public Report fetchByScanIdReportType_First(long scanId, int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns the last report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public Report findByScanIdReportType_Last(long scanId, int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Returns the last report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report, or <code>null</code> if a matching report could not be found
	*/
	public Report fetchByScanIdReportType_Last(long scanId, int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns the reports before and after the current report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param reportId the primary key of the current report
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next report
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public Report[] findByScanIdReportType_PrevAndNext(long reportId,
		long scanId, int reportType,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException;

	/**
	* Removes all the reports where scanId = &#63; and reportType = &#63; from the database.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	*/
	public void removeByScanIdReportType(long scanId, int reportType);

	/**
	* Returns the number of reports where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @return the number of matching reports
	*/
	public int countByScanIdReportType(long scanId, int reportType);

	/**
	* Caches the report in the entity cache if it is enabled.
	*
	* @param report the report
	*/
	public void cacheResult(Report report);

	/**
	* Caches the reports in the entity cache if it is enabled.
	*
	* @param reports the reports
	*/
	public void cacheResult(java.util.List<Report> reports);

	/**
	* Creates a new report with the primary key. Does not add the report to the database.
	*
	* @param reportId the primary key for the new report
	* @return the new report
	*/
	public Report create(long reportId);

	/**
	* Removes the report with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reportId the primary key of the report
	* @return the report that was removed
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public Report remove(long reportId) throws NoSuchReportException;

	public Report updateImpl(Report report);

	/**
	* Returns the report with the primary key or throws a {@link NoSuchReportException} if it could not be found.
	*
	* @param reportId the primary key of the report
	* @return the report
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public Report findByPrimaryKey(long reportId) throws NoSuchReportException;

	/**
	* Returns the report with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param reportId the primary key of the report
	* @return the report, or <code>null</code> if a report with the primary key could not be found
	*/
	public Report fetchByPrimaryKey(long reportId);

	@Override
	public java.util.Map<java.io.Serializable, Report> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the reports.
	*
	* @return the reports
	*/
	public java.util.List<Report> findAll();

	/**
	* Returns a range of all the reports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of reports
	*/
	public java.util.List<Report> findAll(int start, int end);

	/**
	* Returns an ordered range of all the reports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of reports
	*/
	public java.util.List<Report> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator);

	/**
	* Returns an ordered range of all the reports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of reports
	*/
	public java.util.List<Report> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Report> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the reports from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of reports.
	*
	* @return the number of reports
	*/
	public int countAll();
}