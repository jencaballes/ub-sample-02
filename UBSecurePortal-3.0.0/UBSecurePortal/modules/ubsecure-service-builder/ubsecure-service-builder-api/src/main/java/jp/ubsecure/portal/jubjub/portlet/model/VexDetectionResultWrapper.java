/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link VexDetectionResult}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResult
 * @generated
 */
@ProviderType
public class VexDetectionResultWrapper implements VexDetectionResult,
	ModelWrapper<VexDetectionResult> {
	public VexDetectionResultWrapper(VexDetectionResult vexDetectionResult) {
		_vexDetectionResult = vexDetectionResult;
	}

	@Override
	public Class<?> getModelClass() {
		return VexDetectionResult.class;
	}

	@Override
	public String getModelClassName() {
		return VexDetectionResult.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("scanresultid", getScanresultid());
		attributes.put("scanid", getScanid());
		attributes.put("detectionresultid", getDetectionresultid());
		attributes.put("risklevel", getRisklevel());
		attributes.put("category", getCategory());
		attributes.put("overview", getOverview());
		attributes.put("functionname", getFunctionname());
		attributes.put("url", getUrl());
		attributes.put("parametername", getParametername());
		attributes.put("detectionjudgment", getDetectionjudgment());
		attributes.put("reviewcomment", getReviewcomment());
		attributes.put("isresendinvex", getIsresendinvex());
		attributes.put("listofdetectedresult", getListofdetectedresult());
		attributes.put("listofinspectiondatetime", getListofinspectiondatetime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long scanresultid = (Long)attributes.get("scanresultid");

		if (scanresultid != null) {
			setScanresultid(scanresultid);
		}

		Long scanid = (Long)attributes.get("scanid");

		if (scanid != null) {
			setScanid(scanid);
		}

		String detectionresultid = (String)attributes.get("detectionresultid");

		if (detectionresultid != null) {
			setDetectionresultid(detectionresultid);
		}

		Integer risklevel = (Integer)attributes.get("risklevel");

		if (risklevel != null) {
			setRisklevel(risklevel);
		}

		String category = (String)attributes.get("category");

		if (category != null) {
			setCategory(category);
		}

		String overview = (String)attributes.get("overview");

		if (overview != null) {
			setOverview(overview);
		}

		String functionname = (String)attributes.get("functionname");

		if (functionname != null) {
			setFunctionname(functionname);
		}

		String url = (String)attributes.get("url");

		if (url != null) {
			setUrl(url);
		}

		String parametername = (String)attributes.get("parametername");

		if (parametername != null) {
			setParametername(parametername);
		}

		String detectionjudgment = (String)attributes.get("detectionjudgment");

		if (detectionjudgment != null) {
			setDetectionjudgment(detectionjudgment);
		}

		String reviewcomment = (String)attributes.get("reviewcomment");

		if (reviewcomment != null) {
			setReviewcomment(reviewcomment);
		}

		Integer isresendinvex = (Integer)attributes.get("isresendinvex");

		if (isresendinvex != null) {
			setIsresendinvex(isresendinvex);
		}

		String listofdetectedresult = (String)attributes.get(
				"listofdetectedresult");

		if (listofdetectedresult != null) {
			setListofdetectedresult(listofdetectedresult);
		}

		String listofinspectiondatetime = (String)attributes.get(
				"listofinspectiondatetime");

		if (listofinspectiondatetime != null) {
			setListofinspectiondatetime(listofinspectiondatetime);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _vexDetectionResult.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _vexDetectionResult.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _vexDetectionResult.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _vexDetectionResult.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult> toCacheModel() {
		return _vexDetectionResult.toCacheModel();
	}

	@Override
	public int compareTo(
		jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult vexDetectionResult) {
		return _vexDetectionResult.compareTo(vexDetectionResult);
	}

	/**
	* Returns the isresendinvex of this vex detection result.
	*
	* @return the isresendinvex of this vex detection result
	*/
	@Override
	public int getIsresendinvex() {
		return _vexDetectionResult.getIsresendinvex();
	}

	/**
	* Returns the risklevel of this vex detection result.
	*
	* @return the risklevel of this vex detection result
	*/
	@Override
	public int getRisklevel() {
		return _vexDetectionResult.getRisklevel();
	}

	@Override
	public int hashCode() {
		return _vexDetectionResult.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _vexDetectionResult.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new VexDetectionResultWrapper((VexDetectionResult)_vexDetectionResult.clone());
	}

	/**
	* Returns the category of this vex detection result.
	*
	* @return the category of this vex detection result
	*/
	@Override
	public java.lang.String getCategory() {
		return _vexDetectionResult.getCategory();
	}

	/**
	* Returns the detectionjudgment of this vex detection result.
	*
	* @return the detectionjudgment of this vex detection result
	*/
	@Override
	public java.lang.String getDetectionjudgment() {
		return _vexDetectionResult.getDetectionjudgment();
	}

	/**
	* Returns the detectionresultid of this vex detection result.
	*
	* @return the detectionresultid of this vex detection result
	*/
	@Override
	public java.lang.String getDetectionresultid() {
		return _vexDetectionResult.getDetectionresultid();
	}

	/**
	* Returns the functionname of this vex detection result.
	*
	* @return the functionname of this vex detection result
	*/
	@Override
	public java.lang.String getFunctionname() {
		return _vexDetectionResult.getFunctionname();
	}

	/**
	* Returns the listofdetectedresult of this vex detection result.
	*
	* @return the listofdetectedresult of this vex detection result
	*/
	@Override
	public java.lang.String getListofdetectedresult() {
		return _vexDetectionResult.getListofdetectedresult();
	}

	/**
	* Returns the listofinspectiondatetime of this vex detection result.
	*
	* @return the listofinspectiondatetime of this vex detection result
	*/
	@Override
	public java.lang.String getListofinspectiondatetime() {
		return _vexDetectionResult.getListofinspectiondatetime();
	}

	/**
	* Returns the overview of this vex detection result.
	*
	* @return the overview of this vex detection result
	*/
	@Override
	public java.lang.String getOverview() {
		return _vexDetectionResult.getOverview();
	}

	/**
	* Returns the parametername of this vex detection result.
	*
	* @return the parametername of this vex detection result
	*/
	@Override
	public java.lang.String getParametername() {
		return _vexDetectionResult.getParametername();
	}

	/**
	* Returns the reviewcomment of this vex detection result.
	*
	* @return the reviewcomment of this vex detection result
	*/
	@Override
	public java.lang.String getReviewcomment() {
		return _vexDetectionResult.getReviewcomment();
	}

	/**
	* Returns the url of this vex detection result.
	*
	* @return the url of this vex detection result
	*/
	@Override
	public java.lang.String getUrl() {
		return _vexDetectionResult.getUrl();
	}

	@Override
	public java.lang.String toString() {
		return _vexDetectionResult.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _vexDetectionResult.toXmlString();
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult toEscapedModel() {
		return new VexDetectionResultWrapper(_vexDetectionResult.toEscapedModel());
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult toUnescapedModel() {
		return new VexDetectionResultWrapper(_vexDetectionResult.toUnescapedModel());
	}

	/**
	* Returns the primary key of this vex detection result.
	*
	* @return the primary key of this vex detection result
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK getPrimaryKey() {
		return _vexDetectionResult.getPrimaryKey();
	}

	/**
	* Returns the scanid of this vex detection result.
	*
	* @return the scanid of this vex detection result
	*/
	@Override
	public long getScanid() {
		return _vexDetectionResult.getScanid();
	}

	/**
	* Returns the scanresultid of this vex detection result.
	*
	* @return the scanresultid of this vex detection result
	*/
	@Override
	public long getScanresultid() {
		return _vexDetectionResult.getScanresultid();
	}

	@Override
	public void persist() {
		_vexDetectionResult.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_vexDetectionResult.setCachedModel(cachedModel);
	}

	/**
	* Sets the category of this vex detection result.
	*
	* @param category the category of this vex detection result
	*/
	@Override
	public void setCategory(java.lang.String category) {
		_vexDetectionResult.setCategory(category);
	}

	/**
	* Sets the detectionjudgment of this vex detection result.
	*
	* @param detectionjudgment the detectionjudgment of this vex detection result
	*/
	@Override
	public void setDetectionjudgment(java.lang.String detectionjudgment) {
		_vexDetectionResult.setDetectionjudgment(detectionjudgment);
	}

	/**
	* Sets the detectionresultid of this vex detection result.
	*
	* @param detectionresultid the detectionresultid of this vex detection result
	*/
	@Override
	public void setDetectionresultid(java.lang.String detectionresultid) {
		_vexDetectionResult.setDetectionresultid(detectionresultid);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_vexDetectionResult.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_vexDetectionResult.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_vexDetectionResult.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the functionname of this vex detection result.
	*
	* @param functionname the functionname of this vex detection result
	*/
	@Override
	public void setFunctionname(java.lang.String functionname) {
		_vexDetectionResult.setFunctionname(functionname);
	}

	/**
	* Sets the isresendinvex of this vex detection result.
	*
	* @param isresendinvex the isresendinvex of this vex detection result
	*/
	@Override
	public void setIsresendinvex(int isresendinvex) {
		_vexDetectionResult.setIsresendinvex(isresendinvex);
	}

	/**
	* Sets the listofdetectedresult of this vex detection result.
	*
	* @param listofdetectedresult the listofdetectedresult of this vex detection result
	*/
	@Override
	public void setListofdetectedresult(java.lang.String listofdetectedresult) {
		_vexDetectionResult.setListofdetectedresult(listofdetectedresult);
	}

	/**
	* Sets the listofinspectiondatetime of this vex detection result.
	*
	* @param listofinspectiondatetime the listofinspectiondatetime of this vex detection result
	*/
	@Override
	public void setListofinspectiondatetime(
		java.lang.String listofinspectiondatetime) {
		_vexDetectionResult.setListofinspectiondatetime(listofinspectiondatetime);
	}

	@Override
	public void setNew(boolean n) {
		_vexDetectionResult.setNew(n);
	}

	/**
	* Sets the overview of this vex detection result.
	*
	* @param overview the overview of this vex detection result
	*/
	@Override
	public void setOverview(java.lang.String overview) {
		_vexDetectionResult.setOverview(overview);
	}

	/**
	* Sets the parametername of this vex detection result.
	*
	* @param parametername the parametername of this vex detection result
	*/
	@Override
	public void setParametername(java.lang.String parametername) {
		_vexDetectionResult.setParametername(parametername);
	}

	/**
	* Sets the primary key of this vex detection result.
	*
	* @param primaryKey the primary key of this vex detection result
	*/
	@Override
	public void setPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK primaryKey) {
		_vexDetectionResult.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_vexDetectionResult.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the reviewcomment of this vex detection result.
	*
	* @param reviewcomment the reviewcomment of this vex detection result
	*/
	@Override
	public void setReviewcomment(java.lang.String reviewcomment) {
		_vexDetectionResult.setReviewcomment(reviewcomment);
	}

	/**
	* Sets the risklevel of this vex detection result.
	*
	* @param risklevel the risklevel of this vex detection result
	*/
	@Override
	public void setRisklevel(int risklevel) {
		_vexDetectionResult.setRisklevel(risklevel);
	}

	/**
	* Sets the scanid of this vex detection result.
	*
	* @param scanid the scanid of this vex detection result
	*/
	@Override
	public void setScanid(long scanid) {
		_vexDetectionResult.setScanid(scanid);
	}

	/**
	* Sets the scanresultid of this vex detection result.
	*
	* @param scanresultid the scanresultid of this vex detection result
	*/
	@Override
	public void setScanresultid(long scanresultid) {
		_vexDetectionResult.setScanresultid(scanresultid);
	}

	/**
	* Sets the url of this vex detection result.
	*
	* @param url the url of this vex detection result
	*/
	@Override
	public void setUrl(java.lang.String url) {
		_vexDetectionResult.setUrl(url);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexDetectionResultWrapper)) {
			return false;
		}

		VexDetectionResultWrapper vexDetectionResultWrapper = (VexDetectionResultWrapper)obj;

		if (Objects.equals(_vexDetectionResult,
					vexDetectionResultWrapper._vexDetectionResult)) {
			return true;
		}

		return false;
	}

	@Override
	public VexDetectionResult getWrappedModel() {
		return _vexDetectionResult;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _vexDetectionResult.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _vexDetectionResult.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_vexDetectionResult.resetOriginalValues();
	}

	private final VexDetectionResult _vexDetectionResult;
}