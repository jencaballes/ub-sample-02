/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VexLoginSettingLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see VexLoginSettingLocalService
 * @generated
 */
@ProviderType
public class VexLoginSettingLocalServiceWrapper
	implements VexLoginSettingLocalService,
		ServiceWrapper<VexLoginSettingLocalService> {
	public VexLoginSettingLocalServiceWrapper(
		VexLoginSettingLocalService vexLoginSettingLocalService) {
		_vexLoginSettingLocalService = vexLoginSettingLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _vexLoginSettingLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _vexLoginSettingLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _vexLoginSettingLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexLoginSettingLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexLoginSettingLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of vex login settings.
	*
	* @return the number of vex login settings
	*/
	@Override
	public int getVexLoginSettingsCount() {
		return _vexLoginSettingLocalService.getVexLoginSettingsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _vexLoginSettingLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _vexLoginSettingLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _vexLoginSettingLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _vexLoginSettingLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	@Override
	public java.util.List<java.lang.Object> getLoginSettings(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _vexLoginSettingLocalService.getLoginSettings(lScanId);
	}

	/**
	* Returns a range of all the vex login settings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex login settings
	* @param end the upper bound of the range of vex login settings (not inclusive)
	* @return the range of vex login settings
	*/
	@Override
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting> getVexLoginSettings(
		int start, int end) {
		return _vexLoginSettingLocalService.getVexLoginSettings(start, end);
	}

	/**
	* Adds the vex login setting to the database. Also notifies the appropriate model listeners.
	*
	* @param vexLoginSetting the vex login setting
	* @return the vex login setting that was added
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting addVexLoginSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting vexLoginSetting) {
		return _vexLoginSettingLocalService.addVexLoginSetting(vexLoginSetting);
	}

	/**
	* Creates a new vex login setting with the primary key. Does not add the vex login setting to the database.
	*
	* @param scanid the primary key for the new vex login setting
	* @return the new vex login setting
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting createVexLoginSetting(
		long scanid) {
		return _vexLoginSettingLocalService.createVexLoginSetting(scanid);
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting createVexLoginSettingObj() {
		return _vexLoginSettingLocalService.createVexLoginSettingObj();
	}

	/**
	* Deletes the vex login setting from the database. Also notifies the appropriate model listeners.
	*
	* @param vexLoginSetting the vex login setting
	* @return the vex login setting that was removed
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting deleteVexLoginSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting vexLoginSetting) {
		return _vexLoginSettingLocalService.deleteVexLoginSetting(vexLoginSetting);
	}

	/**
	* Deletes the vex login setting with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanid the primary key of the vex login setting
	* @return the vex login setting that was removed
	* @throws PortalException if a vex login setting with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting deleteVexLoginSetting(
		long scanid) throws com.liferay.portal.kernel.exception.PortalException {
		return _vexLoginSettingLocalService.deleteVexLoginSetting(scanid);
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting fetchVexLoginSetting(
		long scanid) {
		return _vexLoginSettingLocalService.fetchVexLoginSetting(scanid);
	}

	/**
	* Returns the vex login setting with the primary key.
	*
	* @param scanid the primary key of the vex login setting
	* @return the vex login setting
	* @throws PortalException if a vex login setting with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting getVexLoginSetting(
		long scanid) throws com.liferay.portal.kernel.exception.PortalException {
		return _vexLoginSettingLocalService.getVexLoginSetting(scanid);
	}

	/**
	* Updates the vex login setting in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param vexLoginSetting the vex login setting
	* @return the vex login setting that was updated
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting updateVexLoginSetting(
		jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting vexLoginSetting) {
		return _vexLoginSettingLocalService.updateVexLoginSetting(vexLoginSetting);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _vexLoginSettingLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _vexLoginSettingLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public VexLoginSettingLocalService getWrappedService() {
		return _vexLoginSettingLocalService;
	}

	@Override
	public void setWrappedService(
		VexLoginSettingLocalService vexLoginSettingLocalService) {
		_vexLoginSettingLocalService = vexLoginSettingLocalService;
	}

	private VexLoginSettingLocalService _vexLoginSettingLocalService;
}