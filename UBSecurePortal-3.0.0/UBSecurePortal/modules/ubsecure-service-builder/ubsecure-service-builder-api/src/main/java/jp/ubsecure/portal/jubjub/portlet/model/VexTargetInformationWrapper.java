/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link VexTargetInformation}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexTargetInformation
 * @generated
 */
@ProviderType
public class VexTargetInformationWrapper implements VexTargetInformation,
	ModelWrapper<VexTargetInformation> {
	public VexTargetInformationWrapper(
		VexTargetInformation vexTargetInformation) {
		_vexTargetInformation = vexTargetInformation;
	}

	@Override
	public Class<?> getModelClass() {
		return VexTargetInformation.class;
	}

	@Override
	public String getModelClassName() {
		return VexTargetInformation.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("scanid", getScanid());
		attributes.put("protocol", getProtocol());
		attributes.put("host", getHost());
		attributes.put("port", getPort());
		attributes.put("httpversion", getHttpversion());
		attributes.put("setkeepaliveconnection", getSetkeepaliveconnection());
		attributes.put("setresponsecontentlength", getSetresponsecontentlength());
		attributes.put("useacceptencodingheader", getUseacceptencodingheader());
		attributes.put("unzipresponse", getUnzipresponse());
		attributes.put("httpprotocol", getHttpprotocol());
		attributes.put("externalproxyhost", getExternalproxyhost());
		attributes.put("externalproxyport", getExternalproxyport());
		attributes.put("externalproxyauthid", getExternalproxyauthid());
		attributes.put("externalproxyauthpassword",
			getExternalproxyauthpassword());
		attributes.put("useclientcertificate", getUseclientcertificate());
		attributes.put("certificatefile", getCertificatefile());
		attributes.put("certificatefilepassword", getCertificatefilepassword());
		attributes.put("ntlmauthid", getNtlmauthid());
		attributes.put("ntlmauthpassword", getNtlmauthpassword());
		attributes.put("ntlmauthdomain", getNtlmauthdomain());
		attributes.put("ntlmauthhost", getNtlmauthhost());
		attributes.put("digestauthid", getDigestauthid());
		attributes.put("digestauthpassword", getDigestauthpassword());
		attributes.put("basicauthid", getBasicauthid());
		attributes.put("basicauthpassword", getBasicauthpassword());
		attributes.put("accessexclusionpath", getAccessexclusionpath());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long scanid = (Long)attributes.get("scanid");

		if (scanid != null) {
			setScanid(scanid);
		}

		String protocol = (String)attributes.get("protocol");

		if (protocol != null) {
			setProtocol(protocol);
		}

		String host = (String)attributes.get("host");

		if (host != null) {
			setHost(host);
		}

		Integer port = (Integer)attributes.get("port");

		if (port != null) {
			setPort(port);
		}

		Integer httpversion = (Integer)attributes.get("httpversion");

		if (httpversion != null) {
			setHttpversion(httpversion);
		}

		Integer setkeepaliveconnection = (Integer)attributes.get(
				"setkeepaliveconnection");

		if (setkeepaliveconnection != null) {
			setSetkeepaliveconnection(setkeepaliveconnection);
		}

		Integer setresponsecontentlength = (Integer)attributes.get(
				"setresponsecontentlength");

		if (setresponsecontentlength != null) {
			setSetresponsecontentlength(setresponsecontentlength);
		}

		Integer useacceptencodingheader = (Integer)attributes.get(
				"useacceptencodingheader");

		if (useacceptencodingheader != null) {
			setUseacceptencodingheader(useacceptencodingheader);
		}

		Integer unzipresponse = (Integer)attributes.get("unzipresponse");

		if (unzipresponse != null) {
			setUnzipresponse(unzipresponse);
		}

		String httpprotocol = (String)attributes.get("httpprotocol");

		if (httpprotocol != null) {
			setHttpprotocol(httpprotocol);
		}

		String externalproxyhost = (String)attributes.get("externalproxyhost");

		if (externalproxyhost != null) {
			setExternalproxyhost(externalproxyhost);
		}

		Integer externalproxyport = (Integer)attributes.get("externalproxyport");

		if (externalproxyport != null) {
			setExternalproxyport(externalproxyport);
		}

		String externalproxyauthid = (String)attributes.get(
				"externalproxyauthid");

		if (externalproxyauthid != null) {
			setExternalproxyauthid(externalproxyauthid);
		}

		String externalproxyauthpassword = (String)attributes.get(
				"externalproxyauthpassword");

		if (externalproxyauthpassword != null) {
			setExternalproxyauthpassword(externalproxyauthpassword);
		}

		Integer useclientcertificate = (Integer)attributes.get(
				"useclientcertificate");

		if (useclientcertificate != null) {
			setUseclientcertificate(useclientcertificate);
		}

		String certificatefile = (String)attributes.get("certificatefile");

		if (certificatefile != null) {
			setCertificatefile(certificatefile);
		}

		String certificatefilepassword = (String)attributes.get(
				"certificatefilepassword");

		if (certificatefilepassword != null) {
			setCertificatefilepassword(certificatefilepassword);
		}

		String ntlmauthid = (String)attributes.get("ntlmauthid");

		if (ntlmauthid != null) {
			setNtlmauthid(ntlmauthid);
		}

		String ntlmauthpassword = (String)attributes.get("ntlmauthpassword");

		if (ntlmauthpassword != null) {
			setNtlmauthpassword(ntlmauthpassword);
		}

		String ntlmauthdomain = (String)attributes.get("ntlmauthdomain");

		if (ntlmauthdomain != null) {
			setNtlmauthdomain(ntlmauthdomain);
		}

		String ntlmauthhost = (String)attributes.get("ntlmauthhost");

		if (ntlmauthhost != null) {
			setNtlmauthhost(ntlmauthhost);
		}

		String digestauthid = (String)attributes.get("digestauthid");

		if (digestauthid != null) {
			setDigestauthid(digestauthid);
		}

		String digestauthpassword = (String)attributes.get("digestauthpassword");

		if (digestauthpassword != null) {
			setDigestauthpassword(digestauthpassword);
		}

		String basicauthid = (String)attributes.get("basicauthid");

		if (basicauthid != null) {
			setBasicauthid(basicauthid);
		}

		String basicauthpassword = (String)attributes.get("basicauthpassword");

		if (basicauthpassword != null) {
			setBasicauthpassword(basicauthpassword);
		}

		String accessexclusionpath = (String)attributes.get(
				"accessexclusionpath");

		if (accessexclusionpath != null) {
			setAccessexclusionpath(accessexclusionpath);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _vexTargetInformation.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _vexTargetInformation.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _vexTargetInformation.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _vexTargetInformation.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation> toCacheModel() {
		return _vexTargetInformation.toCacheModel();
	}

	@Override
	public int compareTo(
		jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation vexTargetInformation) {
		return _vexTargetInformation.compareTo(vexTargetInformation);
	}

	/**
	* Returns the externalproxyport of this vex target information.
	*
	* @return the externalproxyport of this vex target information
	*/
	@Override
	public int getExternalproxyport() {
		return _vexTargetInformation.getExternalproxyport();
	}

	/**
	* Returns the httpversion of this vex target information.
	*
	* @return the httpversion of this vex target information
	*/
	@Override
	public int getHttpversion() {
		return _vexTargetInformation.getHttpversion();
	}

	/**
	* Returns the port of this vex target information.
	*
	* @return the port of this vex target information
	*/
	@Override
	public int getPort() {
		return _vexTargetInformation.getPort();
	}

	/**
	* Returns the setkeepaliveconnection of this vex target information.
	*
	* @return the setkeepaliveconnection of this vex target information
	*/
	@Override
	public int getSetkeepaliveconnection() {
		return _vexTargetInformation.getSetkeepaliveconnection();
	}

	/**
	* Returns the setresponsecontentlength of this vex target information.
	*
	* @return the setresponsecontentlength of this vex target information
	*/
	@Override
	public int getSetresponsecontentlength() {
		return _vexTargetInformation.getSetresponsecontentlength();
	}

	/**
	* Returns the unzipresponse of this vex target information.
	*
	* @return the unzipresponse of this vex target information
	*/
	@Override
	public int getUnzipresponse() {
		return _vexTargetInformation.getUnzipresponse();
	}

	/**
	* Returns the useacceptencodingheader of this vex target information.
	*
	* @return the useacceptencodingheader of this vex target information
	*/
	@Override
	public int getUseacceptencodingheader() {
		return _vexTargetInformation.getUseacceptencodingheader();
	}

	/**
	* Returns the useclientcertificate of this vex target information.
	*
	* @return the useclientcertificate of this vex target information
	*/
	@Override
	public int getUseclientcertificate() {
		return _vexTargetInformation.getUseclientcertificate();
	}

	@Override
	public int hashCode() {
		return _vexTargetInformation.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _vexTargetInformation.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new VexTargetInformationWrapper((VexTargetInformation)_vexTargetInformation.clone());
	}

	/**
	* Returns the accessexclusionpath of this vex target information.
	*
	* @return the accessexclusionpath of this vex target information
	*/
	@Override
	public java.lang.String getAccessexclusionpath() {
		return _vexTargetInformation.getAccessexclusionpath();
	}

	/**
	* Returns the basicauthid of this vex target information.
	*
	* @return the basicauthid of this vex target information
	*/
	@Override
	public java.lang.String getBasicauthid() {
		return _vexTargetInformation.getBasicauthid();
	}

	/**
	* Returns the basicauthpassword of this vex target information.
	*
	* @return the basicauthpassword of this vex target information
	*/
	@Override
	public java.lang.String getBasicauthpassword() {
		return _vexTargetInformation.getBasicauthpassword();
	}

	/**
	* Returns the certificatefile of this vex target information.
	*
	* @return the certificatefile of this vex target information
	*/
	@Override
	public java.lang.String getCertificatefile() {
		return _vexTargetInformation.getCertificatefile();
	}

	/**
	* Returns the certificatefilepassword of this vex target information.
	*
	* @return the certificatefilepassword of this vex target information
	*/
	@Override
	public java.lang.String getCertificatefilepassword() {
		return _vexTargetInformation.getCertificatefilepassword();
	}

	/**
	* Returns the digestauthid of this vex target information.
	*
	* @return the digestauthid of this vex target information
	*/
	@Override
	public java.lang.String getDigestauthid() {
		return _vexTargetInformation.getDigestauthid();
	}

	/**
	* Returns the digestauthpassword of this vex target information.
	*
	* @return the digestauthpassword of this vex target information
	*/
	@Override
	public java.lang.String getDigestauthpassword() {
		return _vexTargetInformation.getDigestauthpassword();
	}

	/**
	* Returns the externalproxyauthid of this vex target information.
	*
	* @return the externalproxyauthid of this vex target information
	*/
	@Override
	public java.lang.String getExternalproxyauthid() {
		return _vexTargetInformation.getExternalproxyauthid();
	}

	/**
	* Returns the externalproxyauthpassword of this vex target information.
	*
	* @return the externalproxyauthpassword of this vex target information
	*/
	@Override
	public java.lang.String getExternalproxyauthpassword() {
		return _vexTargetInformation.getExternalproxyauthpassword();
	}

	/**
	* Returns the externalproxyhost of this vex target information.
	*
	* @return the externalproxyhost of this vex target information
	*/
	@Override
	public java.lang.String getExternalproxyhost() {
		return _vexTargetInformation.getExternalproxyhost();
	}

	/**
	* Returns the host of this vex target information.
	*
	* @return the host of this vex target information
	*/
	@Override
	public java.lang.String getHost() {
		return _vexTargetInformation.getHost();
	}

	/**
	* Returns the httpprotocol of this vex target information.
	*
	* @return the httpprotocol of this vex target information
	*/
	@Override
	public java.lang.String getHttpprotocol() {
		return _vexTargetInformation.getHttpprotocol();
	}

	/**
	* Returns the ntlmauthdomain of this vex target information.
	*
	* @return the ntlmauthdomain of this vex target information
	*/
	@Override
	public java.lang.String getNtlmauthdomain() {
		return _vexTargetInformation.getNtlmauthdomain();
	}

	/**
	* Returns the ntlmauthhost of this vex target information.
	*
	* @return the ntlmauthhost of this vex target information
	*/
	@Override
	public java.lang.String getNtlmauthhost() {
		return _vexTargetInformation.getNtlmauthhost();
	}

	/**
	* Returns the ntlmauthid of this vex target information.
	*
	* @return the ntlmauthid of this vex target information
	*/
	@Override
	public java.lang.String getNtlmauthid() {
		return _vexTargetInformation.getNtlmauthid();
	}

	/**
	* Returns the ntlmauthpassword of this vex target information.
	*
	* @return the ntlmauthpassword of this vex target information
	*/
	@Override
	public java.lang.String getNtlmauthpassword() {
		return _vexTargetInformation.getNtlmauthpassword();
	}

	/**
	* Returns the protocol of this vex target information.
	*
	* @return the protocol of this vex target information
	*/
	@Override
	public java.lang.String getProtocol() {
		return _vexTargetInformation.getProtocol();
	}

	@Override
	public java.lang.String toString() {
		return _vexTargetInformation.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _vexTargetInformation.toXmlString();
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation toEscapedModel() {
		return new VexTargetInformationWrapper(_vexTargetInformation.toEscapedModel());
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation toUnescapedModel() {
		return new VexTargetInformationWrapper(_vexTargetInformation.toUnescapedModel());
	}

	/**
	* Returns the primary key of this vex target information.
	*
	* @return the primary key of this vex target information
	*/
	@Override
	public long getPrimaryKey() {
		return _vexTargetInformation.getPrimaryKey();
	}

	/**
	* Returns the scanid of this vex target information.
	*
	* @return the scanid of this vex target information
	*/
	@Override
	public long getScanid() {
		return _vexTargetInformation.getScanid();
	}

	@Override
	public void persist() {
		_vexTargetInformation.persist();
	}

	/**
	* Sets the accessexclusionpath of this vex target information.
	*
	* @param accessexclusionpath the accessexclusionpath of this vex target information
	*/
	@Override
	public void setAccessexclusionpath(java.lang.String accessexclusionpath) {
		_vexTargetInformation.setAccessexclusionpath(accessexclusionpath);
	}

	/**
	* Sets the basicauthid of this vex target information.
	*
	* @param basicauthid the basicauthid of this vex target information
	*/
	@Override
	public void setBasicauthid(java.lang.String basicauthid) {
		_vexTargetInformation.setBasicauthid(basicauthid);
	}

	/**
	* Sets the basicauthpassword of this vex target information.
	*
	* @param basicauthpassword the basicauthpassword of this vex target information
	*/
	@Override
	public void setBasicauthpassword(java.lang.String basicauthpassword) {
		_vexTargetInformation.setBasicauthpassword(basicauthpassword);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_vexTargetInformation.setCachedModel(cachedModel);
	}

	/**
	* Sets the certificatefile of this vex target information.
	*
	* @param certificatefile the certificatefile of this vex target information
	*/
	@Override
	public void setCertificatefile(java.lang.String certificatefile) {
		_vexTargetInformation.setCertificatefile(certificatefile);
	}

	/**
	* Sets the certificatefilepassword of this vex target information.
	*
	* @param certificatefilepassword the certificatefilepassword of this vex target information
	*/
	@Override
	public void setCertificatefilepassword(
		java.lang.String certificatefilepassword) {
		_vexTargetInformation.setCertificatefilepassword(certificatefilepassword);
	}

	/**
	* Sets the digestauthid of this vex target information.
	*
	* @param digestauthid the digestauthid of this vex target information
	*/
	@Override
	public void setDigestauthid(java.lang.String digestauthid) {
		_vexTargetInformation.setDigestauthid(digestauthid);
	}

	/**
	* Sets the digestauthpassword of this vex target information.
	*
	* @param digestauthpassword the digestauthpassword of this vex target information
	*/
	@Override
	public void setDigestauthpassword(java.lang.String digestauthpassword) {
		_vexTargetInformation.setDigestauthpassword(digestauthpassword);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_vexTargetInformation.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_vexTargetInformation.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_vexTargetInformation.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the externalproxyauthid of this vex target information.
	*
	* @param externalproxyauthid the externalproxyauthid of this vex target information
	*/
	@Override
	public void setExternalproxyauthid(java.lang.String externalproxyauthid) {
		_vexTargetInformation.setExternalproxyauthid(externalproxyauthid);
	}

	/**
	* Sets the externalproxyauthpassword of this vex target information.
	*
	* @param externalproxyauthpassword the externalproxyauthpassword of this vex target information
	*/
	@Override
	public void setExternalproxyauthpassword(
		java.lang.String externalproxyauthpassword) {
		_vexTargetInformation.setExternalproxyauthpassword(externalproxyauthpassword);
	}

	/**
	* Sets the externalproxyhost of this vex target information.
	*
	* @param externalproxyhost the externalproxyhost of this vex target information
	*/
	@Override
	public void setExternalproxyhost(java.lang.String externalproxyhost) {
		_vexTargetInformation.setExternalproxyhost(externalproxyhost);
	}

	/**
	* Sets the externalproxyport of this vex target information.
	*
	* @param externalproxyport the externalproxyport of this vex target information
	*/
	@Override
	public void setExternalproxyport(int externalproxyport) {
		_vexTargetInformation.setExternalproxyport(externalproxyport);
	}

	/**
	* Sets the host of this vex target information.
	*
	* @param host the host of this vex target information
	*/
	@Override
	public void setHost(java.lang.String host) {
		_vexTargetInformation.setHost(host);
	}

	/**
	* Sets the httpprotocol of this vex target information.
	*
	* @param httpprotocol the httpprotocol of this vex target information
	*/
	@Override
	public void setHttpprotocol(java.lang.String httpprotocol) {
		_vexTargetInformation.setHttpprotocol(httpprotocol);
	}

	/**
	* Sets the httpversion of this vex target information.
	*
	* @param httpversion the httpversion of this vex target information
	*/
	@Override
	public void setHttpversion(int httpversion) {
		_vexTargetInformation.setHttpversion(httpversion);
	}

	@Override
	public void setNew(boolean n) {
		_vexTargetInformation.setNew(n);
	}

	/**
	* Sets the ntlmauthdomain of this vex target information.
	*
	* @param ntlmauthdomain the ntlmauthdomain of this vex target information
	*/
	@Override
	public void setNtlmauthdomain(java.lang.String ntlmauthdomain) {
		_vexTargetInformation.setNtlmauthdomain(ntlmauthdomain);
	}

	/**
	* Sets the ntlmauthhost of this vex target information.
	*
	* @param ntlmauthhost the ntlmauthhost of this vex target information
	*/
	@Override
	public void setNtlmauthhost(java.lang.String ntlmauthhost) {
		_vexTargetInformation.setNtlmauthhost(ntlmauthhost);
	}

	/**
	* Sets the ntlmauthid of this vex target information.
	*
	* @param ntlmauthid the ntlmauthid of this vex target information
	*/
	@Override
	public void setNtlmauthid(java.lang.String ntlmauthid) {
		_vexTargetInformation.setNtlmauthid(ntlmauthid);
	}

	/**
	* Sets the ntlmauthpassword of this vex target information.
	*
	* @param ntlmauthpassword the ntlmauthpassword of this vex target information
	*/
	@Override
	public void setNtlmauthpassword(java.lang.String ntlmauthpassword) {
		_vexTargetInformation.setNtlmauthpassword(ntlmauthpassword);
	}

	/**
	* Sets the port of this vex target information.
	*
	* @param port the port of this vex target information
	*/
	@Override
	public void setPort(int port) {
		_vexTargetInformation.setPort(port);
	}

	/**
	* Sets the primary key of this vex target information.
	*
	* @param primaryKey the primary key of this vex target information
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_vexTargetInformation.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_vexTargetInformation.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the protocol of this vex target information.
	*
	* @param protocol the protocol of this vex target information
	*/
	@Override
	public void setProtocol(java.lang.String protocol) {
		_vexTargetInformation.setProtocol(protocol);
	}

	/**
	* Sets the scanid of this vex target information.
	*
	* @param scanid the scanid of this vex target information
	*/
	@Override
	public void setScanid(long scanid) {
		_vexTargetInformation.setScanid(scanid);
	}

	/**
	* Sets the setkeepaliveconnection of this vex target information.
	*
	* @param setkeepaliveconnection the setkeepaliveconnection of this vex target information
	*/
	@Override
	public void setSetkeepaliveconnection(int setkeepaliveconnection) {
		_vexTargetInformation.setSetkeepaliveconnection(setkeepaliveconnection);
	}

	/**
	* Sets the setresponsecontentlength of this vex target information.
	*
	* @param setresponsecontentlength the setresponsecontentlength of this vex target information
	*/
	@Override
	public void setSetresponsecontentlength(int setresponsecontentlength) {
		_vexTargetInformation.setSetresponsecontentlength(setresponsecontentlength);
	}

	/**
	* Sets the unzipresponse of this vex target information.
	*
	* @param unzipresponse the unzipresponse of this vex target information
	*/
	@Override
	public void setUnzipresponse(int unzipresponse) {
		_vexTargetInformation.setUnzipresponse(unzipresponse);
	}

	/**
	* Sets the useacceptencodingheader of this vex target information.
	*
	* @param useacceptencodingheader the useacceptencodingheader of this vex target information
	*/
	@Override
	public void setUseacceptencodingheader(int useacceptencodingheader) {
		_vexTargetInformation.setUseacceptencodingheader(useacceptencodingheader);
	}

	/**
	* Sets the useclientcertificate of this vex target information.
	*
	* @param useclientcertificate the useclientcertificate of this vex target information
	*/
	@Override
	public void setUseclientcertificate(int useclientcertificate) {
		_vexTargetInformation.setUseclientcertificate(useclientcertificate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexTargetInformationWrapper)) {
			return false;
		}

		VexTargetInformationWrapper vexTargetInformationWrapper = (VexTargetInformationWrapper)obj;

		if (Objects.equals(_vexTargetInformation,
					vexTargetInformationWrapper._vexTargetInformation)) {
			return true;
		}

		return false;
	}

	@Override
	public VexTargetInformation getWrappedModel() {
		return _vexTargetInformation;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _vexTargetInformation.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _vexTargetInformation.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_vexTargetInformation.resetOriginalValues();
	}

	private final VexTargetInformation _vexTargetInformation;
}