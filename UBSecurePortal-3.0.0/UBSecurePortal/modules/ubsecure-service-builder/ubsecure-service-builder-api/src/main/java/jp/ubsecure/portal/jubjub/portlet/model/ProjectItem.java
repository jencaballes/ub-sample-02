package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.Date;

public class ProjectItem {
	private long projectId;
	private int type;
	private long groupId;
	private String groupName;
	private String cxAndroidProjectId;
	private String projectName;
	private String caseNumber;
	private int status;
	private int attribute;
	private Date projectEndDate;
	private Date projectCreateDate;
	private String checklistFileName;
	private int scanCount;
	private String caseName;
	private String targetUrl;
	private String productionEnvironmentUrl;
	private String targetUrlprotocol;
	private String productionEnvironmentUrlprotocol;
	private String targetUrlhost;
	private String productionEnvironmentUrlhost;
	private String targetUrlport;
	private String productionEnvironmentUrlport;
	private String selectedWebSignature;
	private int getServerFiles;
	private int getServerSettings;
	private String selectedServerFilesSignature;
	private String selectedServerSettingsSignature;
	
	public String getCaseName() {
		return caseName;
	}

	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}

	public long getProjectId () {
		return projectId;
	}
	
	public void setProjectId (long projectId) {
		this.projectId = projectId;
	}
	
	public String getTargetUrl() {
		return targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

	public String getProductionEnvironmentUrl() {
		return productionEnvironmentUrl;
	}

	public void setProductionEnvironmentUrl(String productionEnvironmentUrl) {
		this.productionEnvironmentUrl = productionEnvironmentUrl;
	}

	public String getTargetUrlprotocol() {
		return targetUrlprotocol;
	}

	public void setTargetUrlprotocol(String targetUrlprotocol) {
		this.targetUrlprotocol = targetUrlprotocol;
	}

	public String getProductionEnvironmentUrlprotocol() {
		return productionEnvironmentUrlprotocol;
	}

	public void setProductionEnvironmentUrlprotocol(String productionEnvironmentUrlprotocol) {
		this.productionEnvironmentUrlprotocol = productionEnvironmentUrlprotocol;
	}

	public String getTargetUrlhost() {
		return targetUrlhost;
	}

	public void setTargetUrlhost(String targetUrlhost) {
		this.targetUrlhost = targetUrlhost;
	}

	public String getProductionEnvironmentUrlhost() {
		return productionEnvironmentUrlhost;
	}

	public void setProductionEnvironmentUrlhost(String productionEnvironmentUrlhost) {
		this.productionEnvironmentUrlhost = productionEnvironmentUrlhost;
	}

	public String getTargetUrlport() {
		return targetUrlport;
	}

	public void setTargetUrlport(String targetUrlport) {
		this.targetUrlport = targetUrlport;
	}

	public String getProductionEnvironmentUrlport() {
		return productionEnvironmentUrlport;
	}

	public void setProductionEnvironmentUrlport(String productionEnvironmentUrlport) {
		this.productionEnvironmentUrlport = productionEnvironmentUrlport;
	}

	public int getType () {
		return type;
	}
	
	public void setType (int type) {
		this.type = type;
	}
	
	public long getOwnerGroupId () {
		return groupId;
	}
	
	public void setOwnerGroupId (long groupId) {
		this.groupId = groupId;
	}
	
	public String getOwnerGroupName () {
		return groupName;
	}
	
	public void setOwnerGroupName (String groupName) {
		this.groupName = groupName;
	}
	
	public String getCxAndroidProjectId () {
		return cxAndroidProjectId;
	}
	
	public void setCxAndroidProjectId (String cxAndroidProjectId) {
		this.cxAndroidProjectId = cxAndroidProjectId;
	}
	
	public String getProjectName () {
		return projectName;
	}
	
	public void setProjectName (String projectName) {
		this.projectName = projectName;
	}
	
	public String getCaseNumber () {
		return caseNumber;
	}
	
	public void setCaseNumber (String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
	public int getStatus () {
		return status;
	}
	
	public void setStatus (int status) {
		this.status = status;
	}
	
	public int getAttribute () {
		return attribute;
	}
	
	public void setAttribute (int attribute) {
		this.attribute = attribute;
	}
	
	public Date getProjectEndDate () {
		return projectEndDate;
	}
	
	public void setProjectEndDate (Date projectEndDate) {
		this.projectEndDate = projectEndDate;
	}
	
	public Date getProjectCreateDate () {
		return projectCreateDate;
	}
	
	public void setProjectCreateDate (Date projectCreateDate) {
		this.projectCreateDate = projectCreateDate;
	}
	
	public String getChecklistFileName () {
		return checklistFileName;
	}
	
	public void setChecklistFileName (String checklistFileName) {
		this.checklistFileName = checklistFileName;
	}
	
	public int getScanCount () {
		return scanCount;
	}
	
	public void setScanCount (int scanCount) {
		this.scanCount = scanCount;
	}

	public String getSelectedWebSignature() {
		return selectedWebSignature;
	}

	public void setSelectedWebSignature(String selectedWebSignature) {
		this.selectedWebSignature = selectedWebSignature;
	}

	public int getGetServerFiles() {
		return getServerFiles;
	}

	public void setGetServerFiles(int getServerFiles) {
		this.getServerFiles = getServerFiles;
	}

	public int getGetServerSettings() {
		return getServerSettings;
	}

	public void setGetServerSettings(int getServerSettings) {
		this.getServerSettings = getServerSettings;
	}

	public String getSelectedServerFilesSignature() {
		return selectedServerFilesSignature;
	}

	public void setSelectedServerFilesSignature(String selectedServerFilesSignature) {
		this.selectedServerFilesSignature = selectedServerFilesSignature;
	}

	public String getSelectedServerSettingsSignature() {
		return selectedServerSettingsSignature;
	}

	public void setSelectedServerSettingsSignature(String selectedServerSettingsSignature) {
		this.selectedServerSettingsSignature = selectedServerSettingsSignature;
	}
}
