/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.Project;

import java.io.File;
import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service interface for Project. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see ProjectLocalServiceUtil
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.ProjectLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.ProjectLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface ProjectLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectLocalServiceUtil} to access the project local service. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ProjectLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Set project to complete
	*
	* @param projectId
	- ID of the project to be completed
	* @return return true if transaction is successful
	* @throws PortalException
	an exception thrown if the transaction fails.
	*/
	public boolean completeProject(long projectId) throws PortalException;

	/**
	* Delete Record Related to Project
	*
	* @param projectId
	- ID of the project
	* @return return true if delete transaction is successful
	* @throws PortalException
	an exception thrown if the transaction fails.
	*/
	public boolean deleteRelatedToProject(long projectId, long lCxProjectId,
		int iType, java.lang.String strSessionId) throws PortalException;

	/**
	* Check if a case number is valid. A case number should be unique
	*
	* @param caseNumber
	the case number to check
	* @param projectId
	a unique ID of the project
	* @return return true if the case number is valid, otherwise return false
	* @throws PortalException
	an exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isCaseNumberValid(java.lang.String caseNumber, long projectId)
		throws PortalException;

	/**
	* Reopen completed project
	*
	* @param project
	- a project to be reopen
	* @return return true if transaction is successful
	* @throws PortalException
	an exception thrown if the transaction fails.
	*/
	public boolean openProject(Project project) throws PortalException;

	/**
	* Update certain record in DB
	*
	* @param project
	- the project to be updated
	* @param lUsersArr
	* @param bAdmin
	* @return return true if update transaction is successful
	* @throws PortalException
	an exception thrown if the transaction fails.
	*/
	public boolean updateProject(java.lang.String strSessionId,
		Project project, List<java.lang.String> packageNamesList, File file,
		long[] lUsersArr, boolean bAdmin) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Returns the number of projects.
	*
	* @return the number of projects
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getProjectsCount();

	/**
	* Get number of projects.
	*
	* @param type
	the type of project(android, cxsuite, ios)
	* @return count - number of projects
	* @throws PortalException
	- an exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getProjectsCount(int type) throws PortalException;

	/**
	* Get the number of projects
	*
	* @param type
	- type of the project(android, cxsuite, ios)
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @return count - number of projects
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getSearchedProjectsCount(int type,
		Map<java.lang.String, java.lang.Object> searchedProject, User user)
		throws PortalException;

	/**
	* Get the number of projects
	*
	* @param userId
	ID of the user
	* @param type
	a type of the project(android, cxsuite, ios)
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @return PortalException - an exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getSearchedUserProjectsCount(java.lang.String userId, int type,
		Map<java.lang.String, java.lang.Object> searchedProject)
		throws PortalException;

	/**
	* Get the number of projects per user
	*
	* @param userId
	- ID of the User
	* @param type
	- type of the project(android, cxsuite, ios)
	* @return count - number of projects
	* @throws PortalException
	- an exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getUserProjectsCount(java.lang.String userId, int type)
		throws PortalException;

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	/**
	* Get all Overall Admin Projects
	*
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @param type
	a type of the project(android, cxsuite, ios)
	* @return projectList a list of projects to be downloaded
	* @throws PortalException
	an exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.Object> getOverallAdminProjectsToDownload(
		Map<java.lang.String, java.lang.Object> searchedProject, int type,
		User user) throws PortalException;

	/**
	* Returns a range of all the projects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @return the range of projects
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Project> getProjects(int start, int end);

	/**
	* Retrieve project list.
	*
	* @param searchedProject
	a key-value parameter inputted by user during search.
	* @param user
	login user
	* @param type
	type of the project (android, cxsuite, ios)
	* @param start
	index on where the search begin
	* @return projectList list of projects
	* @throws PortalException
	- exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.Object> getProjects(
		Map<java.lang.String, java.lang.Object> searchedProject, User user,
		int type, int start) throws PortalException;

	/**
	* Retrieve Project List by owner group.
	*
	* @param ownerGroup
	owner group
	* @throws PortalException
	Exception thrown
	* @return List<Project> of Projects
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Project> getProjectsByOwnerGroup(long ownerGroup)
		throws PortalException;

	/**
	* Retrieve Project List by owner groups.
	*
	* @param ownerGroupList
	owner group list
	* @throws PortalException
	Exception thrown
	* @return List<Project> of Projects
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Project> getProjectsByOwnerGroupList(long[] ownerGroupList)
		throws PortalException;

	/**
	* Get All User Projects
	*
	* @param strUserId
	- a User ID
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @param type
	a type of the project(android, cxsuite, ios)
	* @return projectList a list of projects to be downloaded
	* @throws PortalException
	an exception thrown
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.Object> getUserProjectsToDownload(
		java.lang.String strUserId,
		Map<java.lang.String, java.lang.Object> searchedProject, int type)
		throws PortalException;

	/**
	* Sort overall Projects
	*
	* @param lUserId
	user id
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @param orderByType
	type of order(ascending, descending)
	* @param orderByCol
	- a column to order
	* @param type
	a type of the project(android, cxsuite, ios)
	* @param start
	index for the start position
	* @param end
	index for the end position
	* @return projectList a sorted project list
	* @throws PortalException
	an exception thrown
	*/
	public List<java.lang.Object> sortOverallProjects(long lUserId,
		int iUserRole, Map<java.lang.String, java.lang.Object> searchedProject,
		java.lang.String orderByType, java.lang.String orderByCol, int type,
		int start, int end) throws PortalException;

	/**
	* Sort User Projects
	*
	* @param strUserId
	- User ID
	* @param searchedProject
	a key-value parameter inputted by user during search
	* @param orderByType
	type of order(ascending, descending)
	* @param orderByCol
	- a column to order
	* @param type
	a type of the project(android, cxsuite, ios)
	* @param start
	index for the start position
	* @param end
	index for the end position
	* @return projectList a sorted project list
	* @throws PortalException
	an exception thrown
	*/
	public List<java.lang.Object> sortUserProjects(java.lang.String strUserId,
		Map<java.lang.String, java.lang.Object> searchedProject,
		java.lang.String orderByType, java.lang.String orderByCol, int type,
		int start, int end) throws PortalException;

	/**
	* Adds the project to the database. Also notifies the appropriate model listeners.
	*
	* @param project the project
	* @return the project that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Project addProject(Project project);

	/**
	* Creates a new project with the primary key. Does not add the project to the database.
	*
	* @param projectId the primary key for the new project
	* @return the new project
	*/
	public Project createProject(long projectId);

	/**
	* Create Project
	*
	* @return return empty Project
	*/
	public Project createProjectObj();

	/**
	* Deletes the project from the database. Also notifies the appropriate model listeners.
	*
	* @param project the project
	* @return the project that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Project deleteProject(Project project);

	/**
	* Deletes the project with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectId the primary key of the project
	* @return the project that was removed
	* @throws PortalException if a project with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Project deleteProject(long projectId) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Project fetchProject(long projectId);

	/**
	* Returns the project with the primary key.
	*
	* @param projectId the primary key of the project
	* @return the project
	* @throws PortalException if a project with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Project getProject(long projectId) throws PortalException;

	/**
	* Updates the project in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param project the project
	* @return the project that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Project updateProject(Project project);

	/**
	* Generate a unique project ID
	*
	* @return return a unique Project ID
	* @throws PortalException
	an exception thrown
	*/
	public long createProjectId() throws PortalException;

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	/**
	* Clears the cache for all instances of this model.
	*/
	public void clearCache() throws PortalException;
}