/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ScanLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ScanLocalService
 * @generated
 */
@ProviderType
public class ScanLocalServiceWrapper implements ScanLocalService,
	ServiceWrapper<ScanLocalService> {
	public ScanLocalServiceWrapper(ScanLocalService scanLocalService) {
		_scanLocalService = scanLocalService;
	}

	/**
	* Cancel a review request
	*
	* @param lScanId
	id of scan to cancel the request review
	* @return returns true if cancel transaction is successful
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public boolean cancelReview(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.cancelReview(lScanId);
	}

	/**
	* Delete existing scan
	*
	* @param lScanId
	scan id to be deleted
	* @return returns true if delete transaction is successful
	* @throws PortalException
	*/
	@Override
	public boolean deleteScan(int iType, long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.deleteScan(iType, lScanId);
	}

	/**
	* Check if scan is deleted in cx server
	*
	* @param scanId
	id of the scan to be checked
	* @return return true if the scan is deleted in cx scan server, else return
	false
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public boolean isDeletedScanInCxServer(long scanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.isDeletedScanInCxServer(scanId);
	}

	/**
	* Request for review
	*
	* @param lScanId
	id of the scan to be requested for review
	* @return returns true if request review transaction is successful
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Override
	public boolean requestReview(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.requestReview(lScanId);
	}

	/**
	* Update existing scans
	*
	* @param iUserAction
	action of the user
	* @param scan
	the scan to be updated
	* @return return true if update transaction is successful
	* @throws PortalException
	an exception thrown if transaction fails
	*/
	@Override
	public boolean updateScan(int iUserAction,
		jp.ubsecure.portal.jubjub.portlet.model.Scan scan)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.updateScan(iUserAction, scan);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _scanLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _scanLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _scanLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Get the number of scans waiting
	*
	* @param scanRegDate
	the registration date of the scan
	* @param type
	a type of project(android, cxsuite, ios)
	* @return Number of scans waiting
	* @throws PortalException
	*/
	@Override
	public int countCrawlWaiting(java.util.Date scanRegDate, int type)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.countCrawlWaiting(scanRegDate, type);
	}

	/**
	* Get the number of scans waiting
	*
	* @param scanRegDate
	the registration date of the scan
	* @param type
	a type of project(android, cxsuite, ios)
	* @return Number of scans waiting
	* @throws PortalException
	*/
	@Override
	public int countScanWaiting(java.util.Date scanRegDate, int type)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.countScanWaiting(scanRegDate, type);
	}

	/**
	* Get the total Number of All scans in all projects for each project type
	* (android, cxsuite, ios)
	*
	* @param type
	a type of the project (android, cxsuite, ios)
	* @param searchedScan
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Override
	public int getEntireScansCount(int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getEntireScansCount(type, searchedScan);
	}

	/**
	* Get Scan Status
	*
	* @param lScanId
	id of the scan
	* @return returns a status of the scan
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public int getScanStatus(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getScanStatus(lScanId);
	}

	/**
	* Returns the number of scans.
	*
	* @return the number of scans
	*/
	@Override
	public int getScansCount() {
		return _scanLocalService.getScansCount();
	}

	/**
	* Get the number of scans
	*
	* @param lProjectId
	- id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Override
	public int getScansCount(long lProjectId,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getScansCount(lProjectId, searchedScan);
	}

	/**
	* Get File path
	*
	* @param lScanId
	scan ID
	* @return filepath
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Override
	public java.lang.String getFilePath(long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getFilePath(lScanId);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _scanLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Retrieve the scan registragion date
	*
	* @param lScanId
	id of the scan
	* @return Date
	*/
	@Override
	public java.util.Date getScanRegistrationDate(long lScanId) {
		return _scanLocalService.getScanRegistrationDate(lScanId);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _scanLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _scanLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _scanLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Get All Scans
	*
	* @param type
	a type of the project (android, cxsuite, ios)
	* @param searchedScan
	a key-value parameter inputted by user during search.
	* @param start
	a start index position
	* @return scanList list of the entire scan
	* @throws PortalException
	an exception thrown if error occurs.
	*/
	@Override
	public java.util.List<java.lang.Object> getEntireScans(int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		int start) throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getEntireScans(type, searchedScan, start);
	}

	/**
	* Returns a range of all the scans.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of scans
	* @param end the upper bound of the range of scans (not inclusive)
	* @return the range of scans
	*/
	@Override
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Scan> getScans(
		int start, int end) {
		return _scanLocalService.getScans(start, end);
	}

	/**
	* Get List of Scans
	*
	* @param lProjectId
	- id of the project on which project scans should be retrieved
	* @param searchedScan
	- a key-value parameter inputted by user during search.
	* @param iStart
	- start index position
	* @return scanList - list of Project scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Override
	public java.util.List<java.lang.Object> getScans(long lProjectId,
		com.liferay.portal.kernel.model.User user,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		int iStart) throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getScans(lProjectId, user, searchedScan, iStart);
	}

	/**
	* Get Scans Per Project
	*
	* @param projectId
	id of the project on where to get the scans
	* @return scanList list of scans
	* @throws PortalException
	*/
	@Override
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Scan> getScansByProjectId(
		long projectId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getScansByProjectId(projectId);
	}

	/**
	* Sort Entire Scans
	*
	* @param type
	a type of project (android, cxsuite, ios)
	* @param searchedScan
	a key-value parameter inputted by user during search.
	* @param orderByCol
	a column to order
	* @param orderByType
	type of order(ascending, descending)
	* @param start
	index for the start position
	* @return scanList - sorted scan list
	* @throws PortalException
	*/
	@Override
	public java.util.List<java.lang.Object> sortEntireScans(int type,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		java.lang.String orderByCol, java.lang.String orderByType, int start)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.sortEntireScans(type, searchedScan,
			orderByCol, orderByType, start);
	}

	/**
	* Sort Scans
	*
	* @param lProjectId
	id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @param orderByCol
	a column to order
	* @param orderByType
	type of order(ascending, descending)
	* @param iStart
	index for the start position
	* @return scanList a sorted scan list
	* @throws PortalException
	*/
	@Override
	public java.util.List<java.lang.Object> sortScans(long lProjectId,
		java.util.Map<java.lang.String, java.lang.Object> searchedScan,
		java.lang.String orderByCol, java.lang.String orderByType, int iStart)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.sortScans(lProjectId, searchedScan,
			orderByCol, orderByType, iStart);
	}

	/**
	* Adds the scan to the database. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was added
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan addScan(
		jp.ubsecure.portal.jubjub.portlet.model.Scan scan) {
		return _scanLocalService.addScan(scan);
	}

	/**
	* Creates a new scan with the primary key. Does not add the scan to the database.
	*
	* @param scanId the primary key for the new scan
	* @return the new scan
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan createScan(long scanId) {
		return _scanLocalService.createScan(scanId);
	}

	/**
	* Create empty scan object
	*
	* @return scan object
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan createScanObj() {
		return _scanLocalService.createScanObj();
	}

	/**
	* Deletes the scan from the database. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was removed
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan deleteScan(
		jp.ubsecure.portal.jubjub.portlet.model.Scan scan) {
		return _scanLocalService.deleteScan(scan);
	}

	/**
	* Deletes the scan with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanId the primary key of the scan
	* @return the scan that was removed
	* @throws PortalException if a scan with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan deleteScan(long scanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.deleteScan(scanId);
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan fetchScan(long scanId) {
		return _scanLocalService.fetchScan(scanId);
	}

	/**
	* Returns the scan with the primary key.
	*
	* @param scanId the primary key of the scan
	* @return the scan
	* @throws PortalException if a scan with the primary key could not be found
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan getScan(long scanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.getScan(scanId);
	}

	/**
	* Get Scan
	*
	* @param lScanId
	id of the scan to retrieve
	* @return Scan object
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan retrieveScan(
		long lScanId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.retrieveScan(lScanId);
	}

	/**
	* Update Android scan status
	*
	* @param lScanId
	id of the scan to be updated
	* @param iStatus
	the status to be set
	* @return returns the updated scan
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan updateAndroidScanStatus(
		long lScanId, int iStatus)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.updateAndroidScanStatus(lScanId, iStatus);
	}

	/**
	* Updates the scan in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param scan the scan
	* @return the scan that was updated
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan updateScan(
		jp.ubsecure.portal.jubjub.portlet.model.Scan scan) {
		return _scanLocalService.updateScan(scan);
	}

	/**
	* Generate a unique scan ID
	*
	* @return returns a unique scan id
	* @throws PortalException
	an exception thrown
	*/
	@Override
	public long createScanId()
		throws com.liferay.portal.kernel.exception.PortalException {
		return _scanLocalService.createScanId();
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _scanLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _scanLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public ScanLocalService getWrappedService() {
		return _scanLocalService;
	}

	@Override
	public void setWrappedService(ScanLocalService scanLocalService) {
		_scanLocalService = scanLocalService;
	}

	private ScanLocalService _scanLocalService;
}