/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service interface for VexDetectionResult. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResultLocalServiceUtil
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.VexDetectionResultLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.VexDetectionResultLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface VexDetectionResultLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link VexDetectionResultLocalServiceUtil} to access the vex detection result local service. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.VexDetectionResultLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Get the number of scans
	*
	* @param lProjectId
	- id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getDetectionResultsCount(long lscanId,
		Map<java.lang.String, java.lang.Object> searchedDetectionResult)
		throws PortalException;

	/**
	* Returns the number of vex detection results.
	*
	* @return the number of vex detection results
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getVexDetectionResultsCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	/**
	* Get List of Detection Result
	*
	* @param lscanId
	- id of the scan on which detection results should be retrieved
	* @param searchedDetectionResult
	- a key-value parameter inputted by user during search.
	* @throws PortalException
	an exception thrown if error occurs
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.Object> getVexDetectionResultReviews(long lscanId,
		User user,
		Map<java.lang.String, java.lang.Object> searchedDetectionResult,
		int start, java.lang.String orderByCol, java.lang.String orderByType)
		throws PortalException;

	/**
	* Returns a range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @return the range of vex detection results
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<VexDetectionResult> getVexDetectionResults(int start, int end);

	/**
	* Adds the vex detection result to the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public VexDetectionResult addVexDetectionResult(
		VexDetectionResult vexDetectionResult);

	/**
	* Creates a new vex detection result with the primary key. Does not add the vex detection result to the database.
	*
	* @param vexDetectionResultPK the primary key for the new vex detection result
	* @return the new vex detection result
	*/
	public VexDetectionResult createVexDetectionResult(
		VexDetectionResultPK vexDetectionResultPK);

	/**
	* Deletes the vex detection result from the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public VexDetectionResult deleteVexDetectionResult(
		VexDetectionResult vexDetectionResult);

	/**
	* Deletes the vex detection result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result that was removed
	* @throws PortalException if a vex detection result with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public VexDetectionResult deleteVexDetectionResult(
		VexDetectionResultPK vexDetectionResultPK) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public VexDetectionResult fetchVexDetectionResult(
		VexDetectionResultPK vexDetectionResultPK);

	/**
	* Returns the vex detection result with the primary key.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result
	* @throws PortalException if a vex detection result with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public VexDetectionResult getVexDetectionResult(
		VexDetectionResultPK vexDetectionResultPK) throws PortalException;

	/**
	* Updates the vex detection result in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public VexDetectionResult updateVexDetectionResult(
		VexDetectionResult vexDetectionResult);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);
}