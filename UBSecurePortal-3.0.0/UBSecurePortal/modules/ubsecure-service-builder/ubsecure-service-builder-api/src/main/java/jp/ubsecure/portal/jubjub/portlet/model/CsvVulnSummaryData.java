package jp.ubsecure.portal.jubjub.portlet.model;

public class CsvVulnSummaryData {

	private String projectName;
	private String scanId;
	private String preset;
	private String executionDate;
	private String severity;
	private String queryName;
	private String inputName;
	
	public String getProjectName() {
		return projectName;
	}
	
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	public String getScanId() {
		return scanId;
	}
	
	public void setScanId(String scanId) {
		this.scanId = scanId;
	}
	
	public String getPreset() {
		return preset;
	}
	
	public void setPreset(String preset) {
		this.preset = preset;
	}
	
	public String getExecutionDate() {
		return executionDate;
	}
	
	public void setExecutionDate(String executionDate) {
		this.executionDate = executionDate;
	}
	
	public String getSeverity() {
		return severity;
	}
	
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	
	public String getQueryName() {
		return queryName;
	}
	
	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}
	
	public String getInputName() {
		return inputName;
	}
	
	public void setInputName(String inputName) {
		this.inputName = inputName;
	}
}
