/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexTargetInformationException;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation;

/**
 * The persistence interface for the vex target information service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexTargetInformationPersistenceImpl
 * @see VexTargetInformationUtil
 * @generated
 */
@ProviderType
public interface VexTargetInformationPersistence extends BasePersistence<VexTargetInformation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link VexTargetInformationUtil} to access the vex target information persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the vex target informations where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the matching vex target informations
	*/
	public java.util.List<VexTargetInformation> findByScanId(long scanid);

	/**
	* Returns a range of all the vex target informations where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @return the range of matching vex target informations
	*/
	public java.util.List<VexTargetInformation> findByScanId(long scanid,
		int start, int end);

	/**
	* Returns an ordered range of all the vex target informations where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching vex target informations
	*/
	public java.util.List<VexTargetInformation> findByScanId(long scanid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexTargetInformation> orderByComparator);

	/**
	* Returns an ordered range of all the vex target informations where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching vex target informations
	*/
	public java.util.List<VexTargetInformation> findByScanId(long scanid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexTargetInformation> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first vex target information in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex target information
	* @throws NoSuchVexTargetInformationException if a matching vex target information could not be found
	*/
	public VexTargetInformation findByScanId_First(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexTargetInformation> orderByComparator)
		throws NoSuchVexTargetInformationException;

	/**
	* Returns the first vex target information in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex target information, or <code>null</code> if a matching vex target information could not be found
	*/
	public VexTargetInformation fetchByScanId_First(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexTargetInformation> orderByComparator);

	/**
	* Returns the last vex target information in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex target information
	* @throws NoSuchVexTargetInformationException if a matching vex target information could not be found
	*/
	public VexTargetInformation findByScanId_Last(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexTargetInformation> orderByComparator)
		throws NoSuchVexTargetInformationException;

	/**
	* Returns the last vex target information in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex target information, or <code>null</code> if a matching vex target information could not be found
	*/
	public VexTargetInformation fetchByScanId_Last(long scanid,
		com.liferay.portal.kernel.util.OrderByComparator<VexTargetInformation> orderByComparator);

	/**
	* Removes all the vex target informations where scanid = &#63; from the database.
	*
	* @param scanid the scanid
	*/
	public void removeByScanId(long scanid);

	/**
	* Returns the number of vex target informations where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the number of matching vex target informations
	*/
	public int countByScanId(long scanid);

	/**
	* Caches the vex target information in the entity cache if it is enabled.
	*
	* @param vexTargetInformation the vex target information
	*/
	public void cacheResult(VexTargetInformation vexTargetInformation);

	/**
	* Caches the vex target informations in the entity cache if it is enabled.
	*
	* @param vexTargetInformations the vex target informations
	*/
	public void cacheResult(
		java.util.List<VexTargetInformation> vexTargetInformations);

	/**
	* Creates a new vex target information with the primary key. Does not add the vex target information to the database.
	*
	* @param scanid the primary key for the new vex target information
	* @return the new vex target information
	*/
	public VexTargetInformation create(long scanid);

	/**
	* Removes the vex target information with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanid the primary key of the vex target information
	* @return the vex target information that was removed
	* @throws NoSuchVexTargetInformationException if a vex target information with the primary key could not be found
	*/
	public VexTargetInformation remove(long scanid)
		throws NoSuchVexTargetInformationException;

	public VexTargetInformation updateImpl(
		VexTargetInformation vexTargetInformation);

	/**
	* Returns the vex target information with the primary key or throws a {@link NoSuchVexTargetInformationException} if it could not be found.
	*
	* @param scanid the primary key of the vex target information
	* @return the vex target information
	* @throws NoSuchVexTargetInformationException if a vex target information with the primary key could not be found
	*/
	public VexTargetInformation findByPrimaryKey(long scanid)
		throws NoSuchVexTargetInformationException;

	/**
	* Returns the vex target information with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param scanid the primary key of the vex target information
	* @return the vex target information, or <code>null</code> if a vex target information with the primary key could not be found
	*/
	public VexTargetInformation fetchByPrimaryKey(long scanid);

	@Override
	public java.util.Map<java.io.Serializable, VexTargetInformation> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the vex target informations.
	*
	* @return the vex target informations
	*/
	public java.util.List<VexTargetInformation> findAll();

	/**
	* Returns a range of all the vex target informations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @return the range of vex target informations
	*/
	public java.util.List<VexTargetInformation> findAll(int start, int end);

	/**
	* Returns an ordered range of all the vex target informations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of vex target informations
	*/
	public java.util.List<VexTargetInformation> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexTargetInformation> orderByComparator);

	/**
	* Returns an ordered range of all the vex target informations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of vex target informations
	*/
	public java.util.List<VexTargetInformation> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<VexTargetInformation> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the vex target informations from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of vex target informations.
	*
	* @return the number of vex target informations
	*/
	public int countAll();
}