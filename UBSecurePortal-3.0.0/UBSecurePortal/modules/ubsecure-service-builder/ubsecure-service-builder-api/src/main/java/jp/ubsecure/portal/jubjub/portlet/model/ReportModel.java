/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Report service. Represents a row in the &quot;UBS_Report&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ReportModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ReportImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Report
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.ReportImpl
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.ReportModelImpl
 * @generated
 */
@ProviderType
public interface ReportModel extends BaseModel<Report> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a report model instance should use the {@link Report} interface instead.
	 */

	/**
	 * Returns the primary key of this report.
	 *
	 * @return the primary key of this report
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this report.
	 *
	 * @param primaryKey the primary key of this report
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the report ID of this report.
	 *
	 * @return the report ID of this report
	 */
	public long getReportId();

	/**
	 * Sets the report ID of this report.
	 *
	 * @param reportId the report ID of this report
	 */
	public void setReportId(long reportId);

	/**
	 * Returns the report date of this report.
	 *
	 * @return the report date of this report
	 */
	public Date getReportDate();

	/**
	 * Sets the report date of this report.
	 *
	 * @param reportDate the report date of this report
	 */
	public void setReportDate(Date reportDate);

	/**
	 * Returns the report bucket name of this report.
	 *
	 * @return the report bucket name of this report
	 */
	@AutoEscape
	public String getReportBucketName();

	/**
	 * Sets the report bucket name of this report.
	 *
	 * @param reportBucketName the report bucket name of this report
	 */
	public void setReportBucketName(String reportBucketName);

	/**
	 * Returns the report type of this report.
	 *
	 * @return the report type of this report
	 */
	public int getReportType();

	/**
	 * Sets the report type of this report.
	 *
	 * @param reportType the report type of this report
	 */
	public void setReportType(int reportType);

	/**
	 * Returns the report name of this report.
	 *
	 * @return the report name of this report
	 */
	@AutoEscape
	public String getReportName();

	/**
	 * Sets the report name of this report.
	 *
	 * @param reportName the report name of this report
	 */
	public void setReportName(String reportName);

	/**
	 * Returns the cx report ID of this report.
	 *
	 * @return the cx report ID of this report
	 */
	public long getCxReportId();

	/**
	 * Sets the cx report ID of this report.
	 *
	 * @param cxReportId the cx report ID of this report
	 */
	public void setCxReportId(long cxReportId);

	/**
	 * Returns the scan ID of this report.
	 *
	 * @return the scan ID of this report
	 */
	public long getScanId();

	/**
	 * Sets the scan ID of this report.
	 *
	 * @param scanId the scan ID of this report
	 */
	public void setScanId(long scanId);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(jp.ubsecure.portal.jubjub.portlet.model.Report report);

	@Override
	public int hashCode();

	@Override
	public CacheModel<jp.ubsecure.portal.jubjub.portlet.model.Report> toCacheModel();

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Report toEscapedModel();

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Report toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}