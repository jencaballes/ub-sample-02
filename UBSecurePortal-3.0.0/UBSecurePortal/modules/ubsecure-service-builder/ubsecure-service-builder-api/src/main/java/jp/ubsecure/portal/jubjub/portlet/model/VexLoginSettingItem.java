package jp.ubsecure.portal.jubjub.portlet.model;

public class VexLoginSettingItem {
	private long scanid;
	private String loginurl;
	private String paramname;
	private String paramvalue;
	private int checked;
	
	public long getScanid() {
		return scanid;
	}
	public void setScanid(long scanid) {
		this.scanid = scanid;
	}
	public String getLoginurl() {
		return loginurl;
	}
	public void setLoginurl(String loginurl) {
		this.loginurl = loginurl;
	}
	public String getParamname() {
		return paramname;
	}
	public void setParamname(String paramname) {
		this.paramname = paramname;
	}
	public String getParamvalue() {
		return paramvalue;
	}
	public void setParamvalue(String paramvalue) {
		this.paramvalue = paramvalue;
	}
	public int getChecked() {
		return checked;
	}
	public void setChecked(int checked) {
		this.checked = checked;
	}
}