package jp.ubsecure.portal.jubjub.portlet.model;

public class ProjectUsersItem {
	private long userId;
	private String emailAddress;
	private String userName;
	
	public long getUserId () {
		return userId;
	}
	
	public void setUserId (long userId) {
		this.userId = userId;
	}
	
	public String getEmailAddress () {
		return emailAddress;
	}
	
	public void setEmailAddress (String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getUserName () {
		return userName;
	}
	
	public void setUserName (String userName) {
		this.userName = userName;
	}
}
