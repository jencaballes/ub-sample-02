/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Scan}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Scan
 * @generated
 */
@ProviderType
public class ScanWrapper implements Scan, ModelWrapper<Scan> {
	public ScanWrapper(Scan scan) {
		_scan = scan;
	}

	@Override
	public Class<?> getModelClass() {
		return Scan.class;
	}

	@Override
	public String getModelClassName() {
		return Scan.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("scanId", getScanId());
		attributes.put("registrationDate", getRegistrationDate());
		attributes.put("status", getStatus());
		attributes.put("process", getProcess());
		attributes.put("cxAndroidScanId", getCxAndroidScanId());
		attributes.put("scanManager", getScanManager());
		attributes.put("infoCount", getInfoCount());
		attributes.put("highCount", getHighCount());
		attributes.put("mediumCount", getMediumCount());
		attributes.put("lowCount", getLowCount());
		attributes.put("fileName", getFileName());
		attributes.put("hashValue", getHashValue());
		attributes.put("filePath", getFilePath());
		attributes.put("cxRunId", getCxRunId());
		attributes.put("projectId", getProjectId());
		attributes.put("reviewFlag", getReviewFlag());
		attributes.put("failedScanCause", getFailedScanCause());
		attributes.put("isDeletedInCx", getIsDeletedInCx());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("cxAndroidProjectId", getCxAndroidProjectId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long scanId = (Long)attributes.get("scanId");

		if (scanId != null) {
			setScanId(scanId);
		}

		Date registrationDate = (Date)attributes.get("registrationDate");

		if (registrationDate != null) {
			setRegistrationDate(registrationDate);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Integer process = (Integer)attributes.get("process");

		if (process != null) {
			setProcess(process);
		}

		String cxAndroidScanId = (String)attributes.get("cxAndroidScanId");

		if (cxAndroidScanId != null) {
			setCxAndroidScanId(cxAndroidScanId);
		}

		String scanManager = (String)attributes.get("scanManager");

		if (scanManager != null) {
			setScanManager(scanManager);
		}

		Long infoCount = (Long)attributes.get("infoCount");

		if (infoCount != null) {
			setInfoCount(infoCount);
		}

		Long highCount = (Long)attributes.get("highCount");

		if (highCount != null) {
			setHighCount(highCount);
		}

		Long mediumCount = (Long)attributes.get("mediumCount");

		if (mediumCount != null) {
			setMediumCount(mediumCount);
		}

		Long lowCount = (Long)attributes.get("lowCount");

		if (lowCount != null) {
			setLowCount(lowCount);
		}

		String fileName = (String)attributes.get("fileName");

		if (fileName != null) {
			setFileName(fileName);
		}

		String hashValue = (String)attributes.get("hashValue");

		if (hashValue != null) {
			setHashValue(hashValue);
		}

		String filePath = (String)attributes.get("filePath");

		if (filePath != null) {
			setFilePath(filePath);
		}

		String cxRunId = (String)attributes.get("cxRunId");

		if (cxRunId != null) {
			setCxRunId(cxRunId);
		}

		Long projectId = (Long)attributes.get("projectId");

		if (projectId != null) {
			setProjectId(projectId);
		}

		Integer reviewFlag = (Integer)attributes.get("reviewFlag");

		if (reviewFlag != null) {
			setReviewFlag(reviewFlag);
		}

		String failedScanCause = (String)attributes.get("failedScanCause");

		if (failedScanCause != null) {
			setFailedScanCause(failedScanCause);
		}

		String isDeletedInCx = (String)attributes.get("isDeletedInCx");

		if (isDeletedInCx != null) {
			setIsDeletedInCx(isDeletedInCx);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String cxAndroidProjectId = (String)attributes.get("cxAndroidProjectId");

		if (cxAndroidProjectId != null) {
			setCxAndroidProjectId(cxAndroidProjectId);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _scan.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _scan.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _scan.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _scan.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<jp.ubsecure.portal.jubjub.portlet.model.Scan> toCacheModel() {
		return _scan.toCacheModel();
	}

	@Override
	public int compareTo(jp.ubsecure.portal.jubjub.portlet.model.Scan scan) {
		return _scan.compareTo(scan);
	}

	/**
	* Returns the process of this scan.
	*
	* @return the process of this scan
	*/
	@Override
	public int getProcess() {
		return _scan.getProcess();
	}

	/**
	* Returns the review flag of this scan.
	*
	* @return the review flag of this scan
	*/
	@Override
	public int getReviewFlag() {
		return _scan.getReviewFlag();
	}

	/**
	* Returns the status of this scan.
	*
	* @return the status of this scan
	*/
	@Override
	public int getStatus() {
		return _scan.getStatus();
	}

	@Override
	public int hashCode() {
		return _scan.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _scan.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ScanWrapper((Scan)_scan.clone());
	}

	/**
	* Returns the cx android project ID of this scan.
	*
	* @return the cx android project ID of this scan
	*/
	@Override
	public java.lang.String getCxAndroidProjectId() {
		return _scan.getCxAndroidProjectId();
	}

	/**
	* Returns the cx android scan ID of this scan.
	*
	* @return the cx android scan ID of this scan
	*/
	@Override
	public java.lang.String getCxAndroidScanId() {
		return _scan.getCxAndroidScanId();
	}

	/**
	* Returns the cx run ID of this scan.
	*
	* @return the cx run ID of this scan
	*/
	@Override
	public java.lang.String getCxRunId() {
		return _scan.getCxRunId();
	}

	/**
	* Returns the failed scan cause of this scan.
	*
	* @return the failed scan cause of this scan
	*/
	@Override
	public java.lang.String getFailedScanCause() {
		return _scan.getFailedScanCause();
	}

	/**
	* Returns the file name of this scan.
	*
	* @return the file name of this scan
	*/
	@Override
	public java.lang.String getFileName() {
		return _scan.getFileName();
	}

	/**
	* Returns the file path of this scan.
	*
	* @return the file path of this scan
	*/
	@Override
	public java.lang.String getFilePath() {
		return _scan.getFilePath();
	}

	/**
	* Returns the hash value of this scan.
	*
	* @return the hash value of this scan
	*/
	@Override
	public java.lang.String getHashValue() {
		return _scan.getHashValue();
	}

	/**
	* Returns the is deleted in cx of this scan.
	*
	* @return the is deleted in cx of this scan
	*/
	@Override
	public java.lang.String getIsDeletedInCx() {
		return _scan.getIsDeletedInCx();
	}

	/**
	* Returns the scan manager of this scan.
	*
	* @return the scan manager of this scan
	*/
	@Override
	public java.lang.String getScanManager() {
		return _scan.getScanManager();
	}

	@Override
	public java.lang.String toString() {
		return _scan.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _scan.toXmlString();
	}

	/**
	* Returns the modified date of this scan.
	*
	* @return the modified date of this scan
	*/
	@Override
	public Date getModifiedDate() {
		return _scan.getModifiedDate();
	}

	/**
	* Returns the registration date of this scan.
	*
	* @return the registration date of this scan
	*/
	@Override
	public Date getRegistrationDate() {
		return _scan.getRegistrationDate();
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan toEscapedModel() {
		return new ScanWrapper(_scan.toEscapedModel());
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Scan toUnescapedModel() {
		return new ScanWrapper(_scan.toUnescapedModel());
	}

	/**
	* Returns the high count of this scan.
	*
	* @return the high count of this scan
	*/
	@Override
	public long getHighCount() {
		return _scan.getHighCount();
	}

	/**
	* Returns the info count of this scan.
	*
	* @return the info count of this scan
	*/
	@Override
	public long getInfoCount() {
		return _scan.getInfoCount();
	}

	/**
	* Returns the low count of this scan.
	*
	* @return the low count of this scan
	*/
	@Override
	public long getLowCount() {
		return _scan.getLowCount();
	}

	/**
	* Returns the medium count of this scan.
	*
	* @return the medium count of this scan
	*/
	@Override
	public long getMediumCount() {
		return _scan.getMediumCount();
	}

	/**
	* Returns the primary key of this scan.
	*
	* @return the primary key of this scan
	*/
	@Override
	public long getPrimaryKey() {
		return _scan.getPrimaryKey();
	}

	/**
	* Returns the project ID of this scan.
	*
	* @return the project ID of this scan
	*/
	@Override
	public long getProjectId() {
		return _scan.getProjectId();
	}

	/**
	* Returns the scan ID of this scan.
	*
	* @return the scan ID of this scan
	*/
	@Override
	public long getScanId() {
		return _scan.getScanId();
	}

	@Override
	public void persist() {
		_scan.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_scan.setCachedModel(cachedModel);
	}

	/**
	* Sets the cx android project ID of this scan.
	*
	* @param cxAndroidProjectId the cx android project ID of this scan
	*/
	@Override
	public void setCxAndroidProjectId(java.lang.String cxAndroidProjectId) {
		_scan.setCxAndroidProjectId(cxAndroidProjectId);
	}

	/**
	* Sets the cx android scan ID of this scan.
	*
	* @param cxAndroidScanId the cx android scan ID of this scan
	*/
	@Override
	public void setCxAndroidScanId(java.lang.String cxAndroidScanId) {
		_scan.setCxAndroidScanId(cxAndroidScanId);
	}

	/**
	* Sets the cx run ID of this scan.
	*
	* @param cxRunId the cx run ID of this scan
	*/
	@Override
	public void setCxRunId(java.lang.String cxRunId) {
		_scan.setCxRunId(cxRunId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_scan.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_scan.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_scan.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the failed scan cause of this scan.
	*
	* @param failedScanCause the failed scan cause of this scan
	*/
	@Override
	public void setFailedScanCause(java.lang.String failedScanCause) {
		_scan.setFailedScanCause(failedScanCause);
	}

	/**
	* Sets the file name of this scan.
	*
	* @param fileName the file name of this scan
	*/
	@Override
	public void setFileName(java.lang.String fileName) {
		_scan.setFileName(fileName);
	}

	/**
	* Sets the file path of this scan.
	*
	* @param filePath the file path of this scan
	*/
	@Override
	public void setFilePath(java.lang.String filePath) {
		_scan.setFilePath(filePath);
	}

	/**
	* Sets the hash value of this scan.
	*
	* @param hashValue the hash value of this scan
	*/
	@Override
	public void setHashValue(java.lang.String hashValue) {
		_scan.setHashValue(hashValue);
	}

	/**
	* Sets the high count of this scan.
	*
	* @param highCount the high count of this scan
	*/
	@Override
	public void setHighCount(long highCount) {
		_scan.setHighCount(highCount);
	}

	/**
	* Sets the info count of this scan.
	*
	* @param infoCount the info count of this scan
	*/
	@Override
	public void setInfoCount(long infoCount) {
		_scan.setInfoCount(infoCount);
	}

	/**
	* Sets the is deleted in cx of this scan.
	*
	* @param isDeletedInCx the is deleted in cx of this scan
	*/
	@Override
	public void setIsDeletedInCx(java.lang.String isDeletedInCx) {
		_scan.setIsDeletedInCx(isDeletedInCx);
	}

	/**
	* Sets the low count of this scan.
	*
	* @param lowCount the low count of this scan
	*/
	@Override
	public void setLowCount(long lowCount) {
		_scan.setLowCount(lowCount);
	}

	/**
	* Sets the medium count of this scan.
	*
	* @param mediumCount the medium count of this scan
	*/
	@Override
	public void setMediumCount(long mediumCount) {
		_scan.setMediumCount(mediumCount);
	}

	/**
	* Sets the modified date of this scan.
	*
	* @param modifiedDate the modified date of this scan
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_scan.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_scan.setNew(n);
	}

	/**
	* Sets the primary key of this scan.
	*
	* @param primaryKey the primary key of this scan
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_scan.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_scan.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the process of this scan.
	*
	* @param process the process of this scan
	*/
	@Override
	public void setProcess(int process) {
		_scan.setProcess(process);
	}

	/**
	* Sets the project ID of this scan.
	*
	* @param projectId the project ID of this scan
	*/
	@Override
	public void setProjectId(long projectId) {
		_scan.setProjectId(projectId);
	}

	/**
	* Sets the registration date of this scan.
	*
	* @param registrationDate the registration date of this scan
	*/
	@Override
	public void setRegistrationDate(Date registrationDate) {
		_scan.setRegistrationDate(registrationDate);
	}

	/**
	* Sets the review flag of this scan.
	*
	* @param reviewFlag the review flag of this scan
	*/
	@Override
	public void setReviewFlag(int reviewFlag) {
		_scan.setReviewFlag(reviewFlag);
	}

	/**
	* Sets the scan ID of this scan.
	*
	* @param scanId the scan ID of this scan
	*/
	@Override
	public void setScanId(long scanId) {
		_scan.setScanId(scanId);
	}

	/**
	* Sets the scan manager of this scan.
	*
	* @param scanManager the scan manager of this scan
	*/
	@Override
	public void setScanManager(java.lang.String scanManager) {
		_scan.setScanManager(scanManager);
	}

	/**
	* Sets the status of this scan.
	*
	* @param status the status of this scan
	*/
	@Override
	public void setStatus(int status) {
		_scan.setStatus(status);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ScanWrapper)) {
			return false;
		}

		ScanWrapper scanWrapper = (ScanWrapper)obj;

		if (Objects.equals(_scan, scanWrapper._scan)) {
			return true;
		}

		return false;
	}

	@Override
	public Scan getWrappedModel() {
		return _scan;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _scan.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _scan.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_scan.resetOriginalValues();
	}

	private final Scan _scan;
}