/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the VexLoginSetting service. Represents a row in the &quot;UBS_VexLoginSetting&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see VexLoginSettingModel
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingImpl
 * @see jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingModelImpl
 * @generated
 */
@ImplementationClassName("jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingImpl")
@ProviderType
public interface VexLoginSetting extends VexLoginSettingModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<VexLoginSetting, Long> SCANID_ACCESSOR = new Accessor<VexLoginSetting, Long>() {
			@Override
			public Long get(VexLoginSetting vexLoginSetting) {
				return vexLoginSetting.getScanid();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<VexLoginSetting> getTypeClass() {
				return VexLoginSetting.class;
			}
		};
}