/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ScanSoap implements Serializable {
	public static ScanSoap toSoapModel(Scan model) {
		ScanSoap soapModel = new ScanSoap();

		soapModel.setScanId(model.getScanId());
		soapModel.setRegistrationDate(model.getRegistrationDate());
		soapModel.setStatus(model.getStatus());
		soapModel.setProcess(model.getProcess());
		soapModel.setCxAndroidScanId(model.getCxAndroidScanId());
		soapModel.setScanManager(model.getScanManager());
		soapModel.setInfoCount(model.getInfoCount());
		soapModel.setHighCount(model.getHighCount());
		soapModel.setMediumCount(model.getMediumCount());
		soapModel.setLowCount(model.getLowCount());
		soapModel.setFileName(model.getFileName());
		soapModel.setHashValue(model.getHashValue());
		soapModel.setFilePath(model.getFilePath());
		soapModel.setCxRunId(model.getCxRunId());
		soapModel.setProjectId(model.getProjectId());
		soapModel.setReviewFlag(model.getReviewFlag());
		soapModel.setFailedScanCause(model.getFailedScanCause());
		soapModel.setIsDeletedInCx(model.getIsDeletedInCx());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCxAndroidProjectId(model.getCxAndroidProjectId());

		return soapModel;
	}

	public static ScanSoap[] toSoapModels(Scan[] models) {
		ScanSoap[] soapModels = new ScanSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ScanSoap[][] toSoapModels(Scan[][] models) {
		ScanSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ScanSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ScanSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ScanSoap[] toSoapModels(List<Scan> models) {
		List<ScanSoap> soapModels = new ArrayList<ScanSoap>(models.size());

		for (Scan model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ScanSoap[soapModels.size()]);
	}

	public ScanSoap() {
	}

	public long getPrimaryKey() {
		return _scanId;
	}

	public void setPrimaryKey(long pk) {
		setScanId(pk);
	}

	public long getScanId() {
		return _scanId;
	}

	public void setScanId(long scanId) {
		_scanId = scanId;
	}

	public Date getRegistrationDate() {
		return _registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		_registrationDate = registrationDate;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public int getProcess() {
		return _process;
	}

	public void setProcess(int process) {
		_process = process;
	}

	public String getCxAndroidScanId() {
		return _cxAndroidScanId;
	}

	public void setCxAndroidScanId(String cxAndroidScanId) {
		_cxAndroidScanId = cxAndroidScanId;
	}

	public String getScanManager() {
		return _scanManager;
	}

	public void setScanManager(String scanManager) {
		_scanManager = scanManager;
	}

	public long getInfoCount() {
		return _infoCount;
	}

	public void setInfoCount(long infoCount) {
		_infoCount = infoCount;
	}

	public long getHighCount() {
		return _highCount;
	}

	public void setHighCount(long highCount) {
		_highCount = highCount;
	}

	public long getMediumCount() {
		return _mediumCount;
	}

	public void setMediumCount(long mediumCount) {
		_mediumCount = mediumCount;
	}

	public long getLowCount() {
		return _lowCount;
	}

	public void setLowCount(long lowCount) {
		_lowCount = lowCount;
	}

	public String getFileName() {
		return _fileName;
	}

	public void setFileName(String fileName) {
		_fileName = fileName;
	}

	public String getHashValue() {
		return _hashValue;
	}

	public void setHashValue(String hashValue) {
		_hashValue = hashValue;
	}

	public String getFilePath() {
		return _filePath;
	}

	public void setFilePath(String filePath) {
		_filePath = filePath;
	}

	public String getCxRunId() {
		return _cxRunId;
	}

	public void setCxRunId(String cxRunId) {
		_cxRunId = cxRunId;
	}

	public long getProjectId() {
		return _projectId;
	}

	public void setProjectId(long projectId) {
		_projectId = projectId;
	}

	public int getReviewFlag() {
		return _reviewFlag;
	}

	public void setReviewFlag(int reviewFlag) {
		_reviewFlag = reviewFlag;
	}

	public String getFailedScanCause() {
		return _failedScanCause;
	}

	public void setFailedScanCause(String failedScanCause) {
		_failedScanCause = failedScanCause;
	}

	public String getIsDeletedInCx() {
		return _isDeletedInCx;
	}

	public void setIsDeletedInCx(String isDeletedInCx) {
		_isDeletedInCx = isDeletedInCx;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getCxAndroidProjectId() {
		return _cxAndroidProjectId;
	}

	public void setCxAndroidProjectId(String cxAndroidProjectId) {
		_cxAndroidProjectId = cxAndroidProjectId;
	}

	private long _scanId;
	private Date _registrationDate;
	private int _status;
	private int _process;
	private String _cxAndroidScanId;
	private String _scanManager;
	private long _infoCount;
	private long _highCount;
	private long _mediumCount;
	private long _lowCount;
	private String _fileName;
	private String _hashValue;
	private String _filePath;
	private String _cxRunId;
	private long _projectId;
	private int _reviewFlag;
	private String _failedScanCause;
	private String _isDeletedInCx;
	private Date _modifiedDate;
	private String _cxAndroidProjectId;
}