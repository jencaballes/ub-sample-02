/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for ProjectUsers. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see ProjectUsersLocalServiceUtil
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.ProjectUsersLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.ProjectUsersLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface ProjectUsersLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectUsersLocalServiceUtil} to access the project users local service. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ProjectUsersLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Delete all user in project user table
	*
	* @param userId
	a user id to be deleted
	* @return return true if delete transaction is successful
	* @throws PortalException
	- an exception thrown if transaction fails.
	*/
	public boolean deleteProjectUser(java.lang.String userId)
		throws PortalException;

	/**
	* Update the Project User table
	*
	* @param projectId
	- id of the project to be updated
	* @param lUsersArr
	- list of user to be assign on this project
	* @return return true if transaction is successful
	* @throws PortalException
	an exception thrown if transaction fails
	*/
	public boolean updateProjectUsers(long projectId, long[] lUsersArr)
		throws PortalException;

	public DynamicQuery dynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Returns the number of project userses.
	*
	* @return the number of project userses
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getProjectUsersesCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	/**
	* Get all User of a certain project.
	*
	* @param projectId
	- the id of the project
	* @return projectUsersList - List of Users of the project
	* @throws PortalException
	an exception thrown if transaction fails
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ProjectUsers> getProjectUsers(long projectId)
		throws PortalException;

	/**
	* Returns a range of all the project userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @return the range of project userses
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ProjectUsers> getProjectUserses(int start, int end);

	/**
	* Adds the project users to the database. Also notifies the appropriate model listeners.
	*
	* @param projectUsers the project users
	* @return the project users that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public ProjectUsers addProjectUsers(ProjectUsers projectUsers);

	/**
	* Creates a new project users with the primary key. Does not add the project users to the database.
	*
	* @param projectUsersPK the primary key for the new project users
	* @return the new project users
	*/
	public ProjectUsers createProjectUsers(ProjectUsersPK projectUsersPK);

	/**
	* Deletes the project users from the database. Also notifies the appropriate model listeners.
	*
	* @param projectUsers the project users
	* @return the project users that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public ProjectUsers deleteProjectUsers(ProjectUsers projectUsers);

	/**
	* Deletes the project users with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users that was removed
	* @throws PortalException if a project users with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public ProjectUsers deleteProjectUsers(ProjectUsersPK projectUsersPK)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ProjectUsers fetchProjectUsers(ProjectUsersPK projectUsersPK);

	/**
	* Returns the project users with the primary key.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users
	* @throws PortalException if a project users with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ProjectUsers getProjectUsers(ProjectUsersPK projectUsersPK)
		throws PortalException;

	/**
	* Updates the project users in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectUsers the project users
	* @return the project users that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public ProjectUsers updateProjectUsers(ProjectUsers projectUsers);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);
}