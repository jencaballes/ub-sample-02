/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ProjectUsers. This utility wraps
 * {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ProjectUsersLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ProjectUsersLocalService
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.ProjectUsersLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.ProjectUsersLocalServiceImpl
 * @generated
 */
@ProviderType
public class ProjectUsersLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.ProjectUsersLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Delete all user in project user table
	*
	* @param userId
	a user id to be deleted
	* @return return true if delete transaction is successful
	* @throws PortalException
	- an exception thrown if transaction fails.
	*/
	public static boolean deleteProjectUser(java.lang.String userId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteProjectUser(userId);
	}

	/**
	* Update the Project User table
	*
	* @param projectId
	- id of the project to be updated
	* @param lUsersArr
	- list of user to be assign on this project
	* @return return true if transaction is successful
	* @throws PortalException
	an exception thrown if transaction fails
	*/
	public static boolean updateProjectUsers(long projectId, long[] lUsersArr)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().updateProjectUsers(projectId, lUsersArr);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of project userses.
	*
	* @return the number of project userses
	*/
	public static int getProjectUsersesCount() {
		return getService().getProjectUsersesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Get all User of a certain project.
	*
	* @param projectId
	- the id of the project
	* @return projectUsersList - List of Users of the project
	* @throws PortalException
	an exception thrown if transaction fails
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers> getProjectUsers(
		long projectId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProjectUsers(projectId);
	}

	/**
	* Returns a range of all the project userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @return the range of project userses
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers> getProjectUserses(
		int start, int end) {
		return getService().getProjectUserses(start, end);
	}

	/**
	* Adds the project users to the database. Also notifies the appropriate model listeners.
	*
	* @param projectUsers the project users
	* @return the project users that was added
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers addProjectUsers(
		jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers projectUsers) {
		return getService().addProjectUsers(projectUsers);
	}

	/**
	* Creates a new project users with the primary key. Does not add the project users to the database.
	*
	* @param projectUsersPK the primary key for the new project users
	* @return the new project users
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers createProjectUsers(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK) {
		return getService().createProjectUsers(projectUsersPK);
	}

	/**
	* Deletes the project users from the database. Also notifies the appropriate model listeners.
	*
	* @param projectUsers the project users
	* @return the project users that was removed
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers deleteProjectUsers(
		jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers projectUsers) {
		return getService().deleteProjectUsers(projectUsers);
	}

	/**
	* Deletes the project users with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users that was removed
	* @throws PortalException if a project users with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers deleteProjectUsers(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteProjectUsers(projectUsersPK);
	}

	public static jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers fetchProjectUsers(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK) {
		return getService().fetchProjectUsers(projectUsersPK);
	}

	/**
	* Returns the project users with the primary key.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users
	* @throws PortalException if a project users with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers getProjectUsers(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProjectUsers(projectUsersPK);
	}

	/**
	* Updates the project users in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectUsers the project users
	* @return the project users that was updated
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers updateProjectUsers(
		jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers projectUsers) {
		return getService().updateProjectUsers(projectUsers);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ProjectUsersLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectUsersLocalService, ProjectUsersLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ProjectUsersLocalService.class);
}