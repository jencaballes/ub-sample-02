package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.Date;

public class VexScanSettingItem {
	  private long scanid;
	  private String starturl;
	  private String authorizedpatrolurl;
	  private String unauthorizedpatrolurl;
	  private Date starttime;
	  private Date endtime;
	  private int setemailnotification;
	  private String selectedwebsignature;
	  private int getserverfiles;
	  private int getserversettings;
	  private String selectedserverfilessignature;
	  private int maxnumdetectionlink;
	  private int maxnumdetectionlinkperpage;
	  private int detectionlinkdepthlimit;
	  private int waittime;
	  private int numofthread;
	  private int timeouttime;
	  private int setautopostform;
	  private int setpostmethod;
	  private String requestheader;
	  private String loginstatusdetection;
	  private int setnotsendpassword;
	  private String paramnamesaspassword;
	  private String numofretryaterror;
	  private String sessionerrordetection;
	  private String screentransitionerrordetection;
	  private int implementationenvironment;
	  private String selectedserversettingssignature;
	  private String excludepathinforegex;
	  private String excludeparameterdeleteparamregex;
	  private String excludeparameterdeletenameregex;
	  private String extracturlregex;
	  private String notparseextension;
	  private String notparsefilenameregex;
	  private int setcrawlingonly;
	  
	public long getScanid() {
		return scanid;
	}
	public void setScanid(long scanid) {
		this.scanid = scanid;
	}
	public String getStarturl() {
		return starturl;
	}
	public void setStarturl(String starturl) {
		this.starturl = starturl;
	}
	public String getAuthorizedpatrolurl() {
		return authorizedpatrolurl;
	}
	public void setAuthorizedpatrolurl(String authorizedpatrolurl) {
		this.authorizedpatrolurl = authorizedpatrolurl;
	}
	public String getUnauthorizedpatrolurl() {
		return unauthorizedpatrolurl;
	}
	public void setUnauthorizedpatrolurl(String unauthorizedpatrolurl) {
		this.unauthorizedpatrolurl = unauthorizedpatrolurl;
	}
	public Date getStarttime() {
		return starttime;
	}
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	public Date getEndtime() {
		return endtime;
	}
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	public int getSetemailnotification() {
		return setemailnotification;
	}
	public void setSetemailnotification(int setemailnotification) {
		this.setemailnotification = setemailnotification;
	}
	public String getSelectedwebsignature() {
		return selectedwebsignature;
	}
	public void setSelectedwebsignature(String selectedwebsignature) {
		this.selectedwebsignature = selectedwebsignature;
	}
	public int getGetserverfiles() {
		return getserverfiles;
	}
	public void setGetserverfiles(int getserverfiles) {
		this.getserverfiles = getserverfiles;
	}
	public int getGetserversettings() {
		return getserversettings;
	}
	public void setGetserversettings(int getserversettings) {
		this.getserversettings = getserversettings;
	}
	public String getSelectedserverfilessignature() {
		return selectedserverfilessignature;
	}
	public void setSelectedserverfilessignature(String selectedserverfilessignature) {
		this.selectedserverfilessignature = selectedserverfilessignature;
	}
	public int getMaxnumdetectionlink() {
		return maxnumdetectionlink;
	}
	public void setMaxnumdetectionlink(int maxnumdetectionlink) {
		this.maxnumdetectionlink = maxnumdetectionlink;
	}
	public int getMaxnumdetectionlinkperpage() {
		return maxnumdetectionlinkperpage;
	}
	public void setMaxnumdetectionlinkperpage(int maxnumdetectionlinkperpage) {
		this.maxnumdetectionlinkperpage = maxnumdetectionlinkperpage;
	}
	public int getDetectionlinkdepthlimit() {
		return detectionlinkdepthlimit;
	}
	public void setDetectionlinkdepthlimit(int detectionlinkdepthlimit) {
		this.detectionlinkdepthlimit = detectionlinkdepthlimit;
	}
	public int getWaittime() {
		return waittime;
	}
	public void setWaittime(int waittime) {
		this.waittime = waittime;
	}
	public int getNumofthread() {
		return numofthread;
	}
	public void setNumofthread(int numofthread) {
		this.numofthread = numofthread;
	}
	public int getTimeouttime() {
		return timeouttime;
	}
	public void setTimeouttime(int timeouttime) {
		this.timeouttime = timeouttime;
	}
	public int getSetautopostform() {
		return setautopostform;
	}
	public void setSetautopostform(int setautopostform) {
		this.setautopostform = setautopostform;
	}
	public int getSetpostmethod() {
		return setpostmethod;
	}
	public void setSetpostmethod(int setpostmethod) {
		this.setpostmethod = setpostmethod;
	}
	public String getRequestheader() {
		return requestheader;
	}
	public void setRequestheader(String requestheader) {
		this.requestheader = requestheader;
	}
	public String getLoginstatusdetection() {
		return loginstatusdetection;
	}
	public void setLoginstatusdetection(String loginstatusdetection) {
		this.loginstatusdetection = loginstatusdetection;
	}
	public int getSetnotsendpassword() {
		return setnotsendpassword;
	}
	public void setSetnotsendpassword(int setnotsendpassword) {
		this.setnotsendpassword = setnotsendpassword;
	}
	public String getParamnamesaspassword() {
		return paramnamesaspassword;
	}
	public void setParamnamesaspassword(String paramnamesaspassword) {
		this.paramnamesaspassword = paramnamesaspassword;
	}
	public String getNumofretryaterror() {
		return numofretryaterror;
	}
	public void setNumofretryaterror(String numofretryaterror) {
		this.numofretryaterror = numofretryaterror;
	}
	public String getSessionerrordetection() {
		return sessionerrordetection;
	}
	public void setSessionerrordetection(String sessionerrordetection) {
		this.sessionerrordetection = sessionerrordetection;
	}
	public String getScreentransitionerrordetection() {
		return screentransitionerrordetection;
	}
	public void setScreentransitionerrordetection(String screentransitionerrordetection) {
		this.screentransitionerrordetection = screentransitionerrordetection;
	}
	public int getImplementationenvironment() {
		return implementationenvironment;
	}
	public void setImplementationenvironment(int implementationenvironment) {
		this.implementationenvironment = implementationenvironment;
	}
	public String getSelectedserversettingssignature() {
		return selectedserversettingssignature;
	}
	public void setSelectedserversettingssignature(String selectedserversettingssignature) {
		this.selectedserversettingssignature = selectedserversettingssignature;
	}
	public String getExcludepathinforegex() {
		return excludepathinforegex;
	}
	public void setExcludepathinforegex(String excludepathinforegex) {
		this.excludepathinforegex = excludepathinforegex;
	}
	public String getExcludeparameterdeleteparamregex() {
		return excludeparameterdeleteparamregex;
	}
	public void setExcludeparameterdeleteparamregex(String excludeparameterdeleteparamregex) {
		this.excludeparameterdeleteparamregex = excludeparameterdeleteparamregex;
	}
	public String getExcludeparameterdeletenameregex() {
		return excludeparameterdeletenameregex;
	}
	public void setExcludeparameterdeletenameregex(String excludeparameterdeletenameregex) {
		this.excludeparameterdeletenameregex = excludeparameterdeletenameregex;
	}
	public String getExtracturlregex() {
		return extracturlregex;
	}
	public void setExtracturlregex(String extracturlregex) {
		this.extracturlregex = extracturlregex;
	}
	public String getNotparseextension() {
		return notparseextension;
	}
	public void setNotparseextension(String notparseextension) {
		this.notparseextension = notparseextension;
	}
	public String getNotparsefilenameregex() {
		return notparsefilenameregex;
	}
	public void setNotparsefilenameregex(String notparsefilenameregex) {
		this.notparsefilenameregex = notparsefilenameregex;
	}
	public int getSetcrawlingonly() {
		return setcrawlingonly;
	}
	public void setSetcrawlingonly(int setcrawlingonly) {
		this.setcrawlingonly = setcrawlingonly;
	}
}