/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ProjectUsersSoap implements Serializable {
	public static ProjectUsersSoap toSoapModel(ProjectUsers model) {
		ProjectUsersSoap soapModel = new ProjectUsersSoap();

		soapModel.setUserId(model.getUserId());
		soapModel.setProjectId(model.getProjectId());

		return soapModel;
	}

	public static ProjectUsersSoap[] toSoapModels(ProjectUsers[] models) {
		ProjectUsersSoap[] soapModels = new ProjectUsersSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProjectUsersSoap[][] toSoapModels(ProjectUsers[][] models) {
		ProjectUsersSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProjectUsersSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProjectUsersSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProjectUsersSoap[] toSoapModels(List<ProjectUsers> models) {
		List<ProjectUsersSoap> soapModels = new ArrayList<ProjectUsersSoap>(models.size());

		for (ProjectUsers model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProjectUsersSoap[soapModels.size()]);
	}

	public ProjectUsersSoap() {
	}

	public ProjectUsersPK getPrimaryKey() {
		return new ProjectUsersPK(_userId, _projectId);
	}

	public void setPrimaryKey(ProjectUsersPK pk) {
		setUserId(pk.userId);
		setProjectId(pk.projectId);
	}

	public String getUserId() {
		return _userId;
	}

	public void setUserId(String userId) {
		_userId = userId;
	}

	public long getProjectId() {
		return _projectId;
	}

	public void setProjectId(long projectId) {
		_projectId = projectId;
	}

	private String _userId;
	private long _projectId;
}