/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Project}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Project
 * @generated
 */
@ProviderType
public class ProjectWrapper implements Project, ModelWrapper<Project> {
	public ProjectWrapper(Project project) {
		_project = project;
	}

	@Override
	public Class<?> getModelClass() {
		return Project.class;
	}

	@Override
	public String getModelClassName() {
		return Project.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("projectId", getProjectId());
		attributes.put("type", getType());
		attributes.put("ownerGroup", getOwnerGroup());
		attributes.put("cxAndroidProjectId", getCxAndroidProjectId());
		attributes.put("projectName", getProjectName());
		attributes.put("caseNumber", getCaseNumber());
		attributes.put("status", getStatus());
		attributes.put("attribute", getAttribute());
		attributes.put("projectEndDate", getProjectEndDate());
		attributes.put("projectCreateDate", getProjectCreateDate());
		attributes.put("checklistFileName", getChecklistFileName());
		attributes.put("presetId", getPresetId());
		attributes.put("caseName", getCaseName());
		attributes.put("targetUrl", getTargetUrl());
		attributes.put("productionEnvironmentUrl", getProductionEnvironmentUrl());
		attributes.put("selectedWebSignature", getSelectedWebSignature());
		attributes.put("getServerFiles", getGetServerFiles());
		attributes.put("getServerSettings", getGetServerSettings());
		attributes.put("selectedServerFilesSignature",
			getSelectedServerFilesSignature());
		attributes.put("selectedServerSettingsSignature",
			getSelectedServerSettingsSignature());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long projectId = (Long)attributes.get("projectId");

		if (projectId != null) {
			setProjectId(projectId);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Long ownerGroup = (Long)attributes.get("ownerGroup");

		if (ownerGroup != null) {
			setOwnerGroup(ownerGroup);
		}

		String cxAndroidProjectId = (String)attributes.get("cxAndroidProjectId");

		if (cxAndroidProjectId != null) {
			setCxAndroidProjectId(cxAndroidProjectId);
		}

		String projectName = (String)attributes.get("projectName");

		if (projectName != null) {
			setProjectName(projectName);
		}

		String caseNumber = (String)attributes.get("caseNumber");

		if (caseNumber != null) {
			setCaseNumber(caseNumber);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Integer attribute = (Integer)attributes.get("attribute");

		if (attribute != null) {
			setAttribute(attribute);
		}

		Date projectEndDate = (Date)attributes.get("projectEndDate");

		if (projectEndDate != null) {
			setProjectEndDate(projectEndDate);
		}

		Date projectCreateDate = (Date)attributes.get("projectCreateDate");

		if (projectCreateDate != null) {
			setProjectCreateDate(projectCreateDate);
		}

		String checklistFileName = (String)attributes.get("checklistFileName");

		if (checklistFileName != null) {
			setChecklistFileName(checklistFileName);
		}

		Long presetId = (Long)attributes.get("presetId");

		if (presetId != null) {
			setPresetId(presetId);
		}

		String caseName = (String)attributes.get("caseName");

		if (caseName != null) {
			setCaseName(caseName);
		}

		String targetUrl = (String)attributes.get("targetUrl");

		if (targetUrl != null) {
			setTargetUrl(targetUrl);
		}

		String productionEnvironmentUrl = (String)attributes.get(
				"productionEnvironmentUrl");

		if (productionEnvironmentUrl != null) {
			setProductionEnvironmentUrl(productionEnvironmentUrl);
		}

		String selectedWebSignature = (String)attributes.get(
				"selectedWebSignature");

		if (selectedWebSignature != null) {
			setSelectedWebSignature(selectedWebSignature);
		}

		Integer getServerFiles = (Integer)attributes.get("getServerFiles");

		if (getServerFiles != null) {
			setGetServerFiles(getServerFiles);
		}

		Integer getServerSettings = (Integer)attributes.get("getServerSettings");

		if (getServerSettings != null) {
			setGetServerSettings(getServerSettings);
		}

		String selectedServerFilesSignature = (String)attributes.get(
				"selectedServerFilesSignature");

		if (selectedServerFilesSignature != null) {
			setSelectedServerFilesSignature(selectedServerFilesSignature);
		}

		String selectedServerSettingsSignature = (String)attributes.get(
				"selectedServerSettingsSignature");

		if (selectedServerSettingsSignature != null) {
			setSelectedServerSettingsSignature(selectedServerSettingsSignature);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _project.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _project.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _project.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _project.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<jp.ubsecure.portal.jubjub.portlet.model.Project> toCacheModel() {
		return _project.toCacheModel();
	}

	@Override
	public int compareTo(
		jp.ubsecure.portal.jubjub.portlet.model.Project project) {
		return _project.compareTo(project);
	}

	/**
	* Returns the attribute of this project.
	*
	* @return the attribute of this project
	*/
	@Override
	public int getAttribute() {
		return _project.getAttribute();
	}

	/**
	* Returns the get server files of this project.
	*
	* @return the get server files of this project
	*/
	@Override
	public int getGetServerFiles() {
		return _project.getGetServerFiles();
	}

	/**
	* Returns the get server settings of this project.
	*
	* @return the get server settings of this project
	*/
	@Override
	public int getGetServerSettings() {
		return _project.getGetServerSettings();
	}

	/**
	* Returns the status of this project.
	*
	* @return the status of this project
	*/
	@Override
	public int getStatus() {
		return _project.getStatus();
	}

	/**
	* Returns the type of this project.
	*
	* @return the type of this project
	*/
	@Override
	public int getType() {
		return _project.getType();
	}

	@Override
	public int hashCode() {
		return _project.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _project.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProjectWrapper((Project)_project.clone());
	}

	/**
	* Returns the case name of this project.
	*
	* @return the case name of this project
	*/
	@Override
	public java.lang.String getCaseName() {
		return _project.getCaseName();
	}

	/**
	* Returns the case number of this project.
	*
	* @return the case number of this project
	*/
	@Override
	public java.lang.String getCaseNumber() {
		return _project.getCaseNumber();
	}

	/**
	* Returns the checklist file name of this project.
	*
	* @return the checklist file name of this project
	*/
	@Override
	public java.lang.String getChecklistFileName() {
		return _project.getChecklistFileName();
	}

	/**
	* Returns the cx android project ID of this project.
	*
	* @return the cx android project ID of this project
	*/
	@Override
	public java.lang.String getCxAndroidProjectId() {
		return _project.getCxAndroidProjectId();
	}

	/**
	* Returns the production environment url of this project.
	*
	* @return the production environment url of this project
	*/
	@Override
	public java.lang.String getProductionEnvironmentUrl() {
		return _project.getProductionEnvironmentUrl();
	}

	/**
	* Returns the project name of this project.
	*
	* @return the project name of this project
	*/
	@Override
	public java.lang.String getProjectName() {
		return _project.getProjectName();
	}

	/**
	* Returns the selected server files signature of this project.
	*
	* @return the selected server files signature of this project
	*/
	@Override
	public java.lang.String getSelectedServerFilesSignature() {
		return _project.getSelectedServerFilesSignature();
	}

	/**
	* Returns the selected server settings signature of this project.
	*
	* @return the selected server settings signature of this project
	*/
	@Override
	public java.lang.String getSelectedServerSettingsSignature() {
		return _project.getSelectedServerSettingsSignature();
	}

	/**
	* Returns the selected web signature of this project.
	*
	* @return the selected web signature of this project
	*/
	@Override
	public java.lang.String getSelectedWebSignature() {
		return _project.getSelectedWebSignature();
	}

	/**
	* Returns the target url of this project.
	*
	* @return the target url of this project
	*/
	@Override
	public java.lang.String getTargetUrl() {
		return _project.getTargetUrl();
	}

	@Override
	public java.lang.String toString() {
		return _project.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _project.toXmlString();
	}

	/**
	* Returns the project create date of this project.
	*
	* @return the project create date of this project
	*/
	@Override
	public Date getProjectCreateDate() {
		return _project.getProjectCreateDate();
	}

	/**
	* Returns the project end date of this project.
	*
	* @return the project end date of this project
	*/
	@Override
	public Date getProjectEndDate() {
		return _project.getProjectEndDate();
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project toEscapedModel() {
		return new ProjectWrapper(_project.toEscapedModel());
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.Project toUnescapedModel() {
		return new ProjectWrapper(_project.toUnescapedModel());
	}

	/**
	* Returns the owner group of this project.
	*
	* @return the owner group of this project
	*/
	@Override
	public long getOwnerGroup() {
		return _project.getOwnerGroup();
	}

	/**
	* Returns the preset ID of this project.
	*
	* @return the preset ID of this project
	*/
	@Override
	public long getPresetId() {
		return _project.getPresetId();
	}

	/**
	* Returns the primary key of this project.
	*
	* @return the primary key of this project
	*/
	@Override
	public long getPrimaryKey() {
		return _project.getPrimaryKey();
	}

	/**
	* Returns the project ID of this project.
	*
	* @return the project ID of this project
	*/
	@Override
	public long getProjectId() {
		return _project.getProjectId();
	}

	@Override
	public void persist() {
		_project.persist();
	}

	/**
	* Sets the attribute of this project.
	*
	* @param attribute the attribute of this project
	*/
	@Override
	public void setAttribute(int attribute) {
		_project.setAttribute(attribute);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_project.setCachedModel(cachedModel);
	}

	/**
	* Sets the case name of this project.
	*
	* @param caseName the case name of this project
	*/
	@Override
	public void setCaseName(java.lang.String caseName) {
		_project.setCaseName(caseName);
	}

	/**
	* Sets the case number of this project.
	*
	* @param caseNumber the case number of this project
	*/
	@Override
	public void setCaseNumber(java.lang.String caseNumber) {
		_project.setCaseNumber(caseNumber);
	}

	/**
	* Sets the checklist file name of this project.
	*
	* @param checklistFileName the checklist file name of this project
	*/
	@Override
	public void setChecklistFileName(java.lang.String checklistFileName) {
		_project.setChecklistFileName(checklistFileName);
	}

	/**
	* Sets the cx android project ID of this project.
	*
	* @param cxAndroidProjectId the cx android project ID of this project
	*/
	@Override
	public void setCxAndroidProjectId(java.lang.String cxAndroidProjectId) {
		_project.setCxAndroidProjectId(cxAndroidProjectId);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_project.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_project.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_project.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the get server files of this project.
	*
	* @param getServerFiles the get server files of this project
	*/
	@Override
	public void setGetServerFiles(int getServerFiles) {
		_project.setGetServerFiles(getServerFiles);
	}

	/**
	* Sets the get server settings of this project.
	*
	* @param getServerSettings the get server settings of this project
	*/
	@Override
	public void setGetServerSettings(int getServerSettings) {
		_project.setGetServerSettings(getServerSettings);
	}

	@Override
	public void setNew(boolean n) {
		_project.setNew(n);
	}

	/**
	* Sets the owner group of this project.
	*
	* @param ownerGroup the owner group of this project
	*/
	@Override
	public void setOwnerGroup(long ownerGroup) {
		_project.setOwnerGroup(ownerGroup);
	}

	/**
	* Sets the preset ID of this project.
	*
	* @param presetId the preset ID of this project
	*/
	@Override
	public void setPresetId(long presetId) {
		_project.setPresetId(presetId);
	}

	/**
	* Sets the primary key of this project.
	*
	* @param primaryKey the primary key of this project
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_project.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_project.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the production environment url of this project.
	*
	* @param productionEnvironmentUrl the production environment url of this project
	*/
	@Override
	public void setProductionEnvironmentUrl(
		java.lang.String productionEnvironmentUrl) {
		_project.setProductionEnvironmentUrl(productionEnvironmentUrl);
	}

	/**
	* Sets the project create date of this project.
	*
	* @param projectCreateDate the project create date of this project
	*/
	@Override
	public void setProjectCreateDate(Date projectCreateDate) {
		_project.setProjectCreateDate(projectCreateDate);
	}

	/**
	* Sets the project end date of this project.
	*
	* @param projectEndDate the project end date of this project
	*/
	@Override
	public void setProjectEndDate(Date projectEndDate) {
		_project.setProjectEndDate(projectEndDate);
	}

	/**
	* Sets the project ID of this project.
	*
	* @param projectId the project ID of this project
	*/
	@Override
	public void setProjectId(long projectId) {
		_project.setProjectId(projectId);
	}

	/**
	* Sets the project name of this project.
	*
	* @param projectName the project name of this project
	*/
	@Override
	public void setProjectName(java.lang.String projectName) {
		_project.setProjectName(projectName);
	}

	/**
	* Sets the selected server files signature of this project.
	*
	* @param selectedServerFilesSignature the selected server files signature of this project
	*/
	@Override
	public void setSelectedServerFilesSignature(
		java.lang.String selectedServerFilesSignature) {
		_project.setSelectedServerFilesSignature(selectedServerFilesSignature);
	}

	/**
	* Sets the selected server settings signature of this project.
	*
	* @param selectedServerSettingsSignature the selected server settings signature of this project
	*/
	@Override
	public void setSelectedServerSettingsSignature(
		java.lang.String selectedServerSettingsSignature) {
		_project.setSelectedServerSettingsSignature(selectedServerSettingsSignature);
	}

	/**
	* Sets the selected web signature of this project.
	*
	* @param selectedWebSignature the selected web signature of this project
	*/
	@Override
	public void setSelectedWebSignature(java.lang.String selectedWebSignature) {
		_project.setSelectedWebSignature(selectedWebSignature);
	}

	/**
	* Sets the status of this project.
	*
	* @param status the status of this project
	*/
	@Override
	public void setStatus(int status) {
		_project.setStatus(status);
	}

	/**
	* Sets the target url of this project.
	*
	* @param targetUrl the target url of this project
	*/
	@Override
	public void setTargetUrl(java.lang.String targetUrl) {
		_project.setTargetUrl(targetUrl);
	}

	/**
	* Sets the type of this project.
	*
	* @param type the type of this project
	*/
	@Override
	public void setType(int type) {
		_project.setType(type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectWrapper)) {
			return false;
		}

		ProjectWrapper projectWrapper = (ProjectWrapper)obj;

		if (Objects.equals(_project, projectWrapper._project)) {
			return true;
		}

		return false;
	}

	@Override
	public Project getWrappedModel() {
		return _project;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _project.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _project.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_project.resetOriginalValues();
	}

	private final Project _project;
}