/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for VexDetectionResult. This utility wraps
 * {@link jp.ubsecure.portal.jubjub.portlet.service.impl.VexDetectionResultLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResultLocalService
 * @see jp.ubsecure.portal.jubjub.portlet.service.base.VexDetectionResultLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.impl.VexDetectionResultLocalServiceImpl
 * @generated
 */
@ProviderType
public class VexDetectionResultLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link jp.ubsecure.portal.jubjub.portlet.service.impl.VexDetectionResultLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Get the number of scans
	*
	* @param lProjectId
	- id of the project on which project scans to count
	* @param searchedScan
	a key-value parameter inputted by user during search
	* @return Number of scans
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public static int getDetectionResultsCount(long lscanId,
		java.util.Map<java.lang.String, java.lang.Object> searchedDetectionResult)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .getDetectionResultsCount(lscanId, searchedDetectionResult);
	}

	/**
	* Returns the number of vex detection results.
	*
	* @return the number of vex detection results
	*/
	public static int getVexDetectionResultsCount() {
		return getService().getVexDetectionResultsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Get List of Detection Result
	*
	* @param lscanId
	- id of the scan on which detection results should be retrieved
	* @param searchedDetectionResult
	- a key-value parameter inputted by user during search.
	* @throws PortalException
	an exception thrown if error occurs
	*/
	public static java.util.List<java.lang.Object> getVexDetectionResultReviews(
		long lscanId, com.liferay.portal.kernel.model.User user,
		java.util.Map<java.lang.String, java.lang.Object> searchedDetectionResult,
		int start, java.lang.String orderByCol, java.lang.String orderByType)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .getVexDetectionResultReviews(lscanId, user,
			searchedDetectionResult, start, orderByCol, orderByType);
	}

	/**
	* Returns a range of all the vex detection results.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex detection results
	* @param end the upper bound of the range of vex detection results (not inclusive)
	* @return the range of vex detection results
	*/
	public static java.util.List<jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult> getVexDetectionResults(
		int start, int end) {
		return getService().getVexDetectionResults(start, end);
	}

	/**
	* Adds the vex detection result to the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was added
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult addVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult vexDetectionResult) {
		return getService().addVexDetectionResult(vexDetectionResult);
	}

	/**
	* Creates a new vex detection result with the primary key. Does not add the vex detection result to the database.
	*
	* @param vexDetectionResultPK the primary key for the new vex detection result
	* @return the new vex detection result
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult createVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK) {
		return getService().createVexDetectionResult(vexDetectionResultPK);
	}

	/**
	* Deletes the vex detection result from the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was removed
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult deleteVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult vexDetectionResult) {
		return getService().deleteVexDetectionResult(vexDetectionResult);
	}

	/**
	* Deletes the vex detection result with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result that was removed
	* @throws PortalException if a vex detection result with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult deleteVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteVexDetectionResult(vexDetectionResultPK);
	}

	public static jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult fetchVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK) {
		return getService().fetchVexDetectionResult(vexDetectionResultPK);
	}

	/**
	* Returns the vex detection result with the primary key.
	*
	* @param vexDetectionResultPK the primary key of the vex detection result
	* @return the vex detection result
	* @throws PortalException if a vex detection result with the primary key could not be found
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult getVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK vexDetectionResultPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getVexDetectionResult(vexDetectionResultPK);
	}

	/**
	* Updates the vex detection result in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param vexDetectionResult the vex detection result
	* @return the vex detection result that was updated
	*/
	public static jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult updateVexDetectionResult(
		jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult vexDetectionResult) {
		return getService().updateVexDetectionResult(vexDetectionResult);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static VexDetectionResultLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VexDetectionResultLocalService, VexDetectionResultLocalService> _serviceTracker =
		ServiceTrackerFactory.open(VexDetectionResultLocalService.class);
}