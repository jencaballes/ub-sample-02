package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.ArrayList;
import java.util.List;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class VexTargetInfoSettingItem {
	private String basic_auth_id;
	private Boolean audit_flag;
	private String basic_auth_pass;
	private String client_auth_file;
	private String client_auth_passphrase;
	private Boolean client_auth_use;
	private Boolean delete_accept_encoding_header;
	private String digest_password;
	private String digest_user_id;
	private String external_proxy_host;
	private String external_proxy_port;
	private String http_version;
	private String https_enabled_protocol;
	private Boolean ignore_content_length;
	private String ntlm_domain;
	private String ntlm_hostname;
	private String ntlm_password;
	private String ntlm_user_id;
	private String protocol;
	private String proxy_auth_id;
	private String proxy_auth_pass;
	private String rejectRule;
	private String sni_flag;
	private String target_host;
	private String target_port;
	private Boolean unzip_response;
	private Boolean use_keep_alive;
	private List<VexProxyReplaceRuleItem> proxyReplaceRuleList;	 
	private List<VexProjectDnsItem> projectDnsList;
	public VexTargetInfoSettingItem() {
		super();
		this.basic_auth_id = PortalConstants.STRING_EMPTY;
		this.audit_flag = PortalConstants.FALSE;
		this.basic_auth_pass = PortalConstants.STRING_EMPTY;
		this.client_auth_file = PortalConstants.STRING_EMPTY;
		this.client_auth_passphrase = PortalConstants.STRING_EMPTY;
		this.client_auth_use = PortalConstants.FALSE;
		this.delete_accept_encoding_header = PortalConstants.FALSE;
		this.digest_password = PortalConstants.STRING_EMPTY;
		this.digest_user_id = PortalConstants.STRING_EMPTY;
		this.external_proxy_host = PortalConstants.STRING_EMPTY;
		this.external_proxy_port = PortalConstants.STRING_EMPTY;
		this.http_version = PortalConstants.STRING_EMPTY;
		this.https_enabled_protocol = PortalConstants.STRING_EMPTY;
		this.ignore_content_length = PortalConstants.FALSE;
		this.ntlm_domain = PortalConstants.STRING_EMPTY;
		this.ntlm_hostname = PortalConstants.STRING_EMPTY;
		this.ntlm_password = PortalConstants.STRING_EMPTY;
		this.ntlm_user_id = PortalConstants.STRING_EMPTY;
		this.protocol = PortalConstants.STRING_EMPTY;
		this.proxy_auth_id = PortalConstants.STRING_EMPTY;
		this.proxy_auth_pass = PortalConstants.STRING_EMPTY;
		this.rejectRule = PortalConstants.STRING_EMPTY;
		this.sni_flag = PortalConstants.STRING_EMPTY;
		this.target_host = PortalConstants.STRING_EMPTY;
		this.target_port = PortalConstants.STRING_EMPTY;
		this.unzip_response = PortalConstants.FALSE;
		this.use_keep_alive = PortalConstants.FALSE;
		this.proxyReplaceRuleList = new ArrayList<VexProxyReplaceRuleItem>();
		this.projectDnsList = new ArrayList<VexProjectDnsItem>();
	}
	public String getBasic_auth_id() {
		return basic_auth_id;
	}
	public void setBasic_auth_id(String basic_auth_id) {
		this.basic_auth_id = basic_auth_id;
	}
	public Boolean getAudit_flag() {
		return audit_flag;
	}
	public void setAudit_flag(Boolean audit_flag) {
		this.audit_flag = audit_flag;
	}
	public String getBasic_auth_pass() {
		return basic_auth_pass;
	}
	public void setBasic_auth_pass(String basic_auth_pass) {
		this.basic_auth_pass = basic_auth_pass;
	}
	public String getClient_auth_file() {
		return client_auth_file;
	}
	public void setClient_auth_file(String client_auth_file) {
		this.client_auth_file = client_auth_file;
	}
	public String getClient_auth_passphrase() {
		return client_auth_passphrase;
	}
	public void setClient_auth_passphrase(String client_auth_passphrase) {
		this.client_auth_passphrase = client_auth_passphrase;
	}
	public Boolean getClient_auth_use() {
		return client_auth_use;
	}
	public void setClient_auth_use(Boolean client_auth_use) {
		this.client_auth_use = client_auth_use;
	}
	public Boolean getDelete_accept_encoding_header() {
		return delete_accept_encoding_header;
	}
	public void setDelete_accept_encoding_header(Boolean delete_accept_encoding_header) {
		this.delete_accept_encoding_header = delete_accept_encoding_header;
	}
	public String getDigest_password() {
		return digest_password;
	}
	public void setDigest_password(String digest_password) {
		this.digest_password = digest_password;
	}
	public String getDigest_user_id() {
		return digest_user_id;
	}
	public void setDigest_user_id(String digest_user_id) {
		this.digest_user_id = digest_user_id;
	}
	public String getExternal_proxy_host() {
		return external_proxy_host;
	}
	public void setExternal_proxy_host(String external_proxy_host) {
		this.external_proxy_host = external_proxy_host;
	}
	public String getExternal_proxy_port() {
		return external_proxy_port;
	}
	public void setExternal_proxy_port(String external_proxy_port) {
		this.external_proxy_port = external_proxy_port;
	}
	public String getHttp_version() {
		return http_version;
	}
	public void setHttp_version(String http_version) {
		this.http_version = http_version;
	}
	public String getHttps_enabled_protocol() {
		return https_enabled_protocol;
	}
	public void setHttps_enabled_protocol(String https_enabled_protocol) {
		this.https_enabled_protocol = https_enabled_protocol;
	}
	public Boolean getIgnore_content_length() {
		return ignore_content_length;
	}
	public void setIgnore_content_length(Boolean ignore_content_length) {
		this.ignore_content_length = ignore_content_length;
	}
	public String getNtlm_domain() {
		return ntlm_domain;
	}
	public void setNtlm_domain(String ntlm_domain) {
		this.ntlm_domain = ntlm_domain;
	}
	public String getNtlm_hostname() {
		return ntlm_hostname;
	}
	public void setNtlm_hostname(String ntlm_hostname) {
		this.ntlm_hostname = ntlm_hostname;
	}
	public String getNtlm_password() {
		return ntlm_password;
	}
	public void setNtlm_password(String ntlm_password) {
		this.ntlm_password = ntlm_password;
	}
	public String getNtlm_user_id() {
		return ntlm_user_id;
	}
	public void setNtlm_user_id(String ntlm_user_id) {
		this.ntlm_user_id = ntlm_user_id;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getProxy_auth_id() {
		return proxy_auth_id;
	}
	public void setProxy_auth_id(String proxy_auth_id) {
		this.proxy_auth_id = proxy_auth_id;
	}
	public String getProxy_auth_pass() {
		return proxy_auth_pass;
	}
	public void setProxy_auth_pass(String proxy_auth_pass) {
		this.proxy_auth_pass = proxy_auth_pass;
	}
	public String getRejectRule() {
		return rejectRule;
	}
	public void setRejectRule(String rejectRule) {
		this.rejectRule = rejectRule;
	}
	public String getSni_flag() {
		return sni_flag;
	}
	public void setSni_flag(String sni_flag) {
		this.sni_flag = sni_flag;
	}
	public String getTarget_host() {
		return target_host;
	}
	public void setTarget_host(String target_host) {
		this.target_host = target_host;
	}
	public String getTarget_port() {
		return target_port;
	}
	public void setTarget_port(String target_port) {
		this.target_port = target_port;
	}
	public Boolean getUnzip_response() {
		return unzip_response;
	}
	public void setUnzip_response(Boolean unzip_response) {
		this.unzip_response = unzip_response;
	}
	public Boolean getUse_keep_alive() {
		return use_keep_alive;
	}
	public void setUse_keep_alive(Boolean use_keep_alive) {
		this.use_keep_alive = use_keep_alive;
	}
	public List<VexProxyReplaceRuleItem> getProxyReplaceRuleList() {
		return proxyReplaceRuleList;
	}
	public void setProxyReplaceRuleList(List<VexProxyReplaceRuleItem> proxyReplaceRuleList) {
		this.proxyReplaceRuleList = proxyReplaceRuleList;
	}
	public List<VexProjectDnsItem> getProjectDnsList() {
		return projectDnsList;
	}
	public void setProjectDnsList(List<VexProjectDnsItem> projectDnsList) {
		this.projectDnsList = projectDnsList;
	}
}
