package jp.ubsecure.portal.jubjub.portlet.model;

public class VexVulnSummaryData2 {
	
	private String riskDegree; //risk_level
	private String vulnCategory; //category
	private String vulnName; //vulnerability>summary
	private String detectionUrl; // url
	private String signatureID; //signature_id
	private String detectionParameters; // target_name
	private String originalValue; // original_value
	private String changeValue;  // modified_value
	private String detectionTrigger; // trigger
	private String conditionStatus;
	private String expectedDate; // audit_period
	
	public String getRiskDegree() {
		return riskDegree;
	}
	public void setRiskDegree(String riskDegree) {
		this.riskDegree = riskDegree;
	}
	public String getVulnCategory() {
		return vulnCategory;
	}
	public void setVulnCategory(String vulnCategory) {
		this.vulnCategory = vulnCategory;
	}
	public String getVulnName() {
		return vulnName;
	}
	public void setVulnName(String vulnName) {
		this.vulnName = vulnName;
	}
	public String getDetectionUrl() {
		return detectionUrl;
	}
	public void setDetectionUrl(String detectionUrl) {
		this.detectionUrl = detectionUrl;
	}
	public String getSignatureID() {
		return signatureID;
	}
	public void setSignatureID(String signatureID) {
		this.signatureID = signatureID;
	}
	public String getDetectionParameters() {
		return detectionParameters;
	}
	public void setDetectionParameters(String detectionParameters) {
		this.detectionParameters = detectionParameters;
	}
	public String getOriginalValue() {
		return originalValue;
	}
	public void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}
	public String getChangeValue() {
		return changeValue;
	}
	public void setChangeValue(String changeValue) {
		this.changeValue = changeValue;
	}
	public String getDetectionTrigger() {
		return detectionTrigger;
	}
	public void setDetectionTrigger(String detectionTrigger) {
		this.detectionTrigger = detectionTrigger;
	}
	public String getConditionStatus() {
		return conditionStatus;
	}
	public void setConditionStatus(String conditionStatus) {
		this.conditionStatus = conditionStatus;
	}
	public String getExpectedDate() {
		return expectedDate;
	}
	public void setExpectedDate(String expectedDate) {
		this.expectedDate = expectedDate;
	}
}
