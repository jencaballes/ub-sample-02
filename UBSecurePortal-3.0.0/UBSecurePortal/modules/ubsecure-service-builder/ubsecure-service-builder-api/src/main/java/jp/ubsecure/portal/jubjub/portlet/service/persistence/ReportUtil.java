/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.Report;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the report service. This utility wraps {@link jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ReportPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ReportPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ReportPersistenceImpl
 * @generated
 */
@ProviderType
public class ReportUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Report report) {
		getPersistence().clearCache(report);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Report> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Report> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Report> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Report> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Report update(Report report) {
		return getPersistence().update(report);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Report update(Report report, ServiceContext serviceContext) {
		return getPersistence().update(report, serviceContext);
	}

	/**
	* Returns all the reports where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @return the matching reports
	*/
	public static List<Report> findByScanId(long scanId) {
		return getPersistence().findByScanId(scanId);
	}

	/**
	* Returns a range of all the reports where scanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of matching reports
	*/
	public static List<Report> findByScanId(long scanId, int start, int end) {
		return getPersistence().findByScanId(scanId, start, end);
	}

	/**
	* Returns an ordered range of all the reports where scanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reports
	*/
	public static List<Report> findByScanId(long scanId, int start, int end,
		OrderByComparator<Report> orderByComparator) {
		return getPersistence()
				   .findByScanId(scanId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the reports where scanId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reports
	*/
	public static List<Report> findByScanId(long scanId, int start, int end,
		OrderByComparator<Report> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByScanId(scanId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first report in the ordered set where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public static Report findByScanId_First(long scanId,
		OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence().findByScanId_First(scanId, orderByComparator);
	}

	/**
	* Returns the first report in the ordered set where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report, or <code>null</code> if a matching report could not be found
	*/
	public static Report fetchByScanId_First(long scanId,
		OrderByComparator<Report> orderByComparator) {
		return getPersistence().fetchByScanId_First(scanId, orderByComparator);
	}

	/**
	* Returns the last report in the ordered set where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public static Report findByScanId_Last(long scanId,
		OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence().findByScanId_Last(scanId, orderByComparator);
	}

	/**
	* Returns the last report in the ordered set where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report, or <code>null</code> if a matching report could not be found
	*/
	public static Report fetchByScanId_Last(long scanId,
		OrderByComparator<Report> orderByComparator) {
		return getPersistence().fetchByScanId_Last(scanId, orderByComparator);
	}

	/**
	* Returns the reports before and after the current report in the ordered set where scanId = &#63;.
	*
	* @param reportId the primary key of the current report
	* @param scanId the scan ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next report
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public static Report[] findByScanId_PrevAndNext(long reportId, long scanId,
		OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence()
				   .findByScanId_PrevAndNext(reportId, scanId, orderByComparator);
	}

	/**
	* Removes all the reports where scanId = &#63; from the database.
	*
	* @param scanId the scan ID
	*/
	public static void removeByScanId(long scanId) {
		getPersistence().removeByScanId(scanId);
	}

	/**
	* Returns the number of reports where scanId = &#63;.
	*
	* @param scanId the scan ID
	* @return the number of matching reports
	*/
	public static int countByScanId(long scanId) {
		return getPersistence().countByScanId(scanId);
	}

	/**
	* Returns all the reports where reportType = &#63;.
	*
	* @param reportType the report type
	* @return the matching reports
	*/
	public static List<Report> findByReportType(int reportType) {
		return getPersistence().findByReportType(reportType);
	}

	/**
	* Returns a range of all the reports where reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of matching reports
	*/
	public static List<Report> findByReportType(int reportType, int start,
		int end) {
		return getPersistence().findByReportType(reportType, start, end);
	}

	/**
	* Returns an ordered range of all the reports where reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reports
	*/
	public static List<Report> findByReportType(int reportType, int start,
		int end, OrderByComparator<Report> orderByComparator) {
		return getPersistence()
				   .findByReportType(reportType, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the reports where reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reports
	*/
	public static List<Report> findByReportType(int reportType, int start,
		int end, OrderByComparator<Report> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByReportType(reportType, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first report in the ordered set where reportType = &#63;.
	*
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public static Report findByReportType_First(int reportType,
		OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence()
				   .findByReportType_First(reportType, orderByComparator);
	}

	/**
	* Returns the first report in the ordered set where reportType = &#63;.
	*
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report, or <code>null</code> if a matching report could not be found
	*/
	public static Report fetchByReportType_First(int reportType,
		OrderByComparator<Report> orderByComparator) {
		return getPersistence()
				   .fetchByReportType_First(reportType, orderByComparator);
	}

	/**
	* Returns the last report in the ordered set where reportType = &#63;.
	*
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public static Report findByReportType_Last(int reportType,
		OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence()
				   .findByReportType_Last(reportType, orderByComparator);
	}

	/**
	* Returns the last report in the ordered set where reportType = &#63;.
	*
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report, or <code>null</code> if a matching report could not be found
	*/
	public static Report fetchByReportType_Last(int reportType,
		OrderByComparator<Report> orderByComparator) {
		return getPersistence()
				   .fetchByReportType_Last(reportType, orderByComparator);
	}

	/**
	* Returns the reports before and after the current report in the ordered set where reportType = &#63;.
	*
	* @param reportId the primary key of the current report
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next report
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public static Report[] findByReportType_PrevAndNext(long reportId,
		int reportType, OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence()
				   .findByReportType_PrevAndNext(reportId, reportType,
			orderByComparator);
	}

	/**
	* Removes all the reports where reportType = &#63; from the database.
	*
	* @param reportType the report type
	*/
	public static void removeByReportType(int reportType) {
		getPersistence().removeByReportType(reportType);
	}

	/**
	* Returns the number of reports where reportType = &#63;.
	*
	* @param reportType the report type
	* @return the number of matching reports
	*/
	public static int countByReportType(int reportType) {
		return getPersistence().countByReportType(reportType);
	}

	/**
	* Returns all the reports where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @return the matching reports
	*/
	public static List<Report> findByScanIdReportType(long scanId,
		int reportType) {
		return getPersistence().findByScanIdReportType(scanId, reportType);
	}

	/**
	* Returns a range of all the reports where scanId = &#63; and reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of matching reports
	*/
	public static List<Report> findByScanIdReportType(long scanId,
		int reportType, int start, int end) {
		return getPersistence()
				   .findByScanIdReportType(scanId, reportType, start, end);
	}

	/**
	* Returns an ordered range of all the reports where scanId = &#63; and reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reports
	*/
	public static List<Report> findByScanIdReportType(long scanId,
		int reportType, int start, int end,
		OrderByComparator<Report> orderByComparator) {
		return getPersistence()
				   .findByScanIdReportType(scanId, reportType, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the reports where scanId = &#63; and reportType = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reports
	*/
	public static List<Report> findByScanIdReportType(long scanId,
		int reportType, int start, int end,
		OrderByComparator<Report> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByScanIdReportType(scanId, reportType, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public static Report findByScanIdReportType_First(long scanId,
		int reportType, OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence()
				   .findByScanIdReportType_First(scanId, reportType,
			orderByComparator);
	}

	/**
	* Returns the first report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching report, or <code>null</code> if a matching report could not be found
	*/
	public static Report fetchByScanIdReportType_First(long scanId,
		int reportType, OrderByComparator<Report> orderByComparator) {
		return getPersistence()
				   .fetchByScanIdReportType_First(scanId, reportType,
			orderByComparator);
	}

	/**
	* Returns the last report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report
	* @throws NoSuchReportException if a matching report could not be found
	*/
	public static Report findByScanIdReportType_Last(long scanId,
		int reportType, OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence()
				   .findByScanIdReportType_Last(scanId, reportType,
			orderByComparator);
	}

	/**
	* Returns the last report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching report, or <code>null</code> if a matching report could not be found
	*/
	public static Report fetchByScanIdReportType_Last(long scanId,
		int reportType, OrderByComparator<Report> orderByComparator) {
		return getPersistence()
				   .fetchByScanIdReportType_Last(scanId, reportType,
			orderByComparator);
	}

	/**
	* Returns the reports before and after the current report in the ordered set where scanId = &#63; and reportType = &#63;.
	*
	* @param reportId the primary key of the current report
	* @param scanId the scan ID
	* @param reportType the report type
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next report
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public static Report[] findByScanIdReportType_PrevAndNext(long reportId,
		long scanId, int reportType, OrderByComparator<Report> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence()
				   .findByScanIdReportType_PrevAndNext(reportId, scanId,
			reportType, orderByComparator);
	}

	/**
	* Removes all the reports where scanId = &#63; and reportType = &#63; from the database.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	*/
	public static void removeByScanIdReportType(long scanId, int reportType) {
		getPersistence().removeByScanIdReportType(scanId, reportType);
	}

	/**
	* Returns the number of reports where scanId = &#63; and reportType = &#63;.
	*
	* @param scanId the scan ID
	* @param reportType the report type
	* @return the number of matching reports
	*/
	public static int countByScanIdReportType(long scanId, int reportType) {
		return getPersistence().countByScanIdReportType(scanId, reportType);
	}

	/**
	* Caches the report in the entity cache if it is enabled.
	*
	* @param report the report
	*/
	public static void cacheResult(Report report) {
		getPersistence().cacheResult(report);
	}

	/**
	* Caches the reports in the entity cache if it is enabled.
	*
	* @param reports the reports
	*/
	public static void cacheResult(List<Report> reports) {
		getPersistence().cacheResult(reports);
	}

	/**
	* Creates a new report with the primary key. Does not add the report to the database.
	*
	* @param reportId the primary key for the new report
	* @return the new report
	*/
	public static Report create(long reportId) {
		return getPersistence().create(reportId);
	}

	/**
	* Removes the report with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reportId the primary key of the report
	* @return the report that was removed
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public static Report remove(long reportId)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence().remove(reportId);
	}

	public static Report updateImpl(Report report) {
		return getPersistence().updateImpl(report);
	}

	/**
	* Returns the report with the primary key or throws a {@link NoSuchReportException} if it could not be found.
	*
	* @param reportId the primary key of the report
	* @return the report
	* @throws NoSuchReportException if a report with the primary key could not be found
	*/
	public static Report findByPrimaryKey(long reportId)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException {
		return getPersistence().findByPrimaryKey(reportId);
	}

	/**
	* Returns the report with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param reportId the primary key of the report
	* @return the report, or <code>null</code> if a report with the primary key could not be found
	*/
	public static Report fetchByPrimaryKey(long reportId) {
		return getPersistence().fetchByPrimaryKey(reportId);
	}

	public static java.util.Map<java.io.Serializable, Report> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the reports.
	*
	* @return the reports
	*/
	public static List<Report> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the reports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @return the range of reports
	*/
	public static List<Report> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the reports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of reports
	*/
	public static List<Report> findAll(int start, int end,
		OrderByComparator<Report> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the reports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reports
	* @param end the upper bound of the range of reports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of reports
	*/
	public static List<Report> findAll(int start, int end,
		OrderByComparator<Report> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the reports from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of reports.
	*
	* @return the number of reports
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ReportPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ReportPersistence, ReportPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ReportPersistence.class);
}