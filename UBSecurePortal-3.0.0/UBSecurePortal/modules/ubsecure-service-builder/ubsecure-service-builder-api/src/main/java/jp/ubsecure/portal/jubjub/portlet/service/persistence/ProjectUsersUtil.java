/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the project users service. This utility wraps {@link jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ProjectUsersPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProjectUsersPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.ProjectUsersPersistenceImpl
 * @generated
 */
@ProviderType
public class ProjectUsersUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ProjectUsers projectUsers) {
		getPersistence().clearCache(projectUsers);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ProjectUsers> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ProjectUsers> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ProjectUsers> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ProjectUsers> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ProjectUsers update(ProjectUsers projectUsers) {
		return getPersistence().update(projectUsers);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ProjectUsers update(ProjectUsers projectUsers,
		ServiceContext serviceContext) {
		return getPersistence().update(projectUsers, serviceContext);
	}

	/**
	* Returns all the project userses where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the matching project userses
	*/
	public static List<ProjectUsers> findByProjectId(long projectId) {
		return getPersistence().findByProjectId(projectId);
	}

	/**
	* Returns a range of all the project userses where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @return the range of matching project userses
	*/
	public static List<ProjectUsers> findByProjectId(long projectId, int start,
		int end) {
		return getPersistence().findByProjectId(projectId, start, end);
	}

	/**
	* Returns an ordered range of all the project userses where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project userses
	*/
	public static List<ProjectUsers> findByProjectId(long projectId, int start,
		int end, OrderByComparator<ProjectUsers> orderByComparator) {
		return getPersistence()
				   .findByProjectId(projectId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project userses where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project userses
	*/
	public static List<ProjectUsers> findByProjectId(long projectId, int start,
		int end, OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByProjectId(projectId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project users in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project users
	* @throws NoSuchProjectUsersException if a matching project users could not be found
	*/
	public static ProjectUsers findByProjectId_First(long projectId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException {
		return getPersistence()
				   .findByProjectId_First(projectId, orderByComparator);
	}

	/**
	* Returns the first project users in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project users, or <code>null</code> if a matching project users could not be found
	*/
	public static ProjectUsers fetchByProjectId_First(long projectId,
		OrderByComparator<ProjectUsers> orderByComparator) {
		return getPersistence()
				   .fetchByProjectId_First(projectId, orderByComparator);
	}

	/**
	* Returns the last project users in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project users
	* @throws NoSuchProjectUsersException if a matching project users could not be found
	*/
	public static ProjectUsers findByProjectId_Last(long projectId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException {
		return getPersistence()
				   .findByProjectId_Last(projectId, orderByComparator);
	}

	/**
	* Returns the last project users in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project users, or <code>null</code> if a matching project users could not be found
	*/
	public static ProjectUsers fetchByProjectId_Last(long projectId,
		OrderByComparator<ProjectUsers> orderByComparator) {
		return getPersistence()
				   .fetchByProjectId_Last(projectId, orderByComparator);
	}

	/**
	* Returns the project userses before and after the current project users in the ordered set where projectId = &#63;.
	*
	* @param projectUsersPK the primary key of the current project users
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project users
	* @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	*/
	public static ProjectUsers[] findByProjectId_PrevAndNext(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK,
		long projectId, OrderByComparator<ProjectUsers> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException {
		return getPersistence()
				   .findByProjectId_PrevAndNext(projectUsersPK, projectId,
			orderByComparator);
	}

	/**
	* Removes all the project userses where projectId = &#63; from the database.
	*
	* @param projectId the project ID
	*/
	public static void removeByProjectId(long projectId) {
		getPersistence().removeByProjectId(projectId);
	}

	/**
	* Returns the number of project userses where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the number of matching project userses
	*/
	public static int countByProjectId(long projectId) {
		return getPersistence().countByProjectId(projectId);
	}

	/**
	* Returns all the project userses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching project userses
	*/
	public static List<ProjectUsers> findByUserId(java.lang.String userId) {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns a range of all the project userses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @return the range of matching project userses
	*/
	public static List<ProjectUsers> findByUserId(java.lang.String userId,
		int start, int end) {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the project userses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project userses
	*/
	public static List<ProjectUsers> findByUserId(java.lang.String userId,
		int start, int end, OrderByComparator<ProjectUsers> orderByComparator) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project userses where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project userses
	*/
	public static List<ProjectUsers> findByUserId(java.lang.String userId,
		int start, int end, OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project users in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project users
	* @throws NoSuchProjectUsersException if a matching project users could not be found
	*/
	public static ProjectUsers findByUserId_First(java.lang.String userId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException {
		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first project users in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project users, or <code>null</code> if a matching project users could not be found
	*/
	public static ProjectUsers fetchByUserId_First(java.lang.String userId,
		OrderByComparator<ProjectUsers> orderByComparator) {
		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last project users in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project users
	* @throws NoSuchProjectUsersException if a matching project users could not be found
	*/
	public static ProjectUsers findByUserId_Last(java.lang.String userId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException {
		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last project users in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project users, or <code>null</code> if a matching project users could not be found
	*/
	public static ProjectUsers fetchByUserId_Last(java.lang.String userId,
		OrderByComparator<ProjectUsers> orderByComparator) {
		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the project userses before and after the current project users in the ordered set where userId = &#63;.
	*
	* @param projectUsersPK the primary key of the current project users
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project users
	* @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	*/
	public static ProjectUsers[] findByUserId_PrevAndNext(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK,
		java.lang.String userId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException {
		return getPersistence()
				   .findByUserId_PrevAndNext(projectUsersPK, userId,
			orderByComparator);
	}

	/**
	* Removes all the project userses where userId = &#63; from the database.
	*
	* @param userId the user ID
	*/
	public static void removeByUserId(java.lang.String userId) {
		getPersistence().removeByUserId(userId);
	}

	/**
	* Returns the number of project userses where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching project userses
	*/
	public static int countByUserId(java.lang.String userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Caches the project users in the entity cache if it is enabled.
	*
	* @param projectUsers the project users
	*/
	public static void cacheResult(ProjectUsers projectUsers) {
		getPersistence().cacheResult(projectUsers);
	}

	/**
	* Caches the project userses in the entity cache if it is enabled.
	*
	* @param projectUserses the project userses
	*/
	public static void cacheResult(List<ProjectUsers> projectUserses) {
		getPersistence().cacheResult(projectUserses);
	}

	/**
	* Creates a new project users with the primary key. Does not add the project users to the database.
	*
	* @param projectUsersPK the primary key for the new project users
	* @return the new project users
	*/
	public static ProjectUsers create(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK) {
		return getPersistence().create(projectUsersPK);
	}

	/**
	* Removes the project users with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users that was removed
	* @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	*/
	public static ProjectUsers remove(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException {
		return getPersistence().remove(projectUsersPK);
	}

	public static ProjectUsers updateImpl(ProjectUsers projectUsers) {
		return getPersistence().updateImpl(projectUsers);
	}

	/**
	* Returns the project users with the primary key or throws a {@link NoSuchProjectUsersException} if it could not be found.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users
	* @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	*/
	public static ProjectUsers findByPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException {
		return getPersistence().findByPrimaryKey(projectUsersPK);
	}

	/**
	* Returns the project users with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectUsersPK the primary key of the project users
	* @return the project users, or <code>null</code> if a project users with the primary key could not be found
	*/
	public static ProjectUsers fetchByPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK projectUsersPK) {
		return getPersistence().fetchByPrimaryKey(projectUsersPK);
	}

	public static java.util.Map<java.io.Serializable, ProjectUsers> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the project userses.
	*
	* @return the project userses
	*/
	public static List<ProjectUsers> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the project userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @return the range of project userses
	*/
	public static List<ProjectUsers> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the project userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project userses
	*/
	public static List<ProjectUsers> findAll(int start, int end,
		OrderByComparator<ProjectUsers> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project userses
	* @param end the upper bound of the range of project userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project userses
	*/
	public static List<ProjectUsers> findAll(int start, int end,
		OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the project userses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of project userses.
	*
	* @return the number of project userses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ProjectUsersPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectUsersPersistence, ProjectUsersPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ProjectUsersPersistence.class);
}