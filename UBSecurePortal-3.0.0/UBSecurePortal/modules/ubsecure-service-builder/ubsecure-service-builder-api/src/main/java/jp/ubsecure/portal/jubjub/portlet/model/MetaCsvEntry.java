package jp.ubsecure.portal.jubjub.portlet.model;

public class MetaCsvEntry {
	private String caseNumber;
	private String cxAndroidScanId;
	private String projectName;
	private String reportId;
	private String highCount;
	private String mediumCount;
	private String infoCount;
	private String lowCount;
	private String scanDate;
	private String scanTime;
	private String signatureSetName;
	private String excludedPackageNameList;
	private String engineVersion;
	private String ownerGroup;
	
	private final String STRING_EMPTY = "";
	
	public String getCaseNumber() {
		return caseNumber;
	}
	
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = isStringNullOrEmpty(caseNumber) ? STRING_EMPTY : caseNumber;
	}
	
	public String getCxAndroidScanId() {
		return cxAndroidScanId;
	}
	
	public void setCxAndroidScanId(String cxAndroidScanId) {
		this.cxAndroidScanId = isStringNullOrEmpty(cxAndroidScanId) ? STRING_EMPTY : cxAndroidScanId;
	}
	
	public String getProjectName() {
		return projectName;
	}
	
	public void setProjectName(String projectName) {
		this.projectName = isStringNullOrEmpty(projectName) ? STRING_EMPTY : projectName;
	}
	
	public String getReportId() {
		return reportId;
	}
	
	public void setReportId(String reportId) {
		this.reportId = isStringNullOrEmpty(reportId) ? STRING_EMPTY : reportId;
	}
	
	public String getHighCount() {
		return highCount;
	}
	
	public void setHighCount(String highCount) {
		this.highCount = isStringNullOrEmpty(highCount) ? STRING_EMPTY : highCount;
	}
	
	public String getMediumCount() {
		return mediumCount;
	}
	
	public void setMediumCount(String mediumCount) {
		this.mediumCount = isStringNullOrEmpty(mediumCount) ? STRING_EMPTY : mediumCount;
	}
	
	public String getLowCount() {
		return lowCount;
	}
	
	public void setLowCount(String lowCount) {
		this.lowCount = isStringNullOrEmpty(lowCount) ? STRING_EMPTY : lowCount;
	}
	
	public String getInfoCount() {
		return infoCount;
	}
	
	public void setInfoCount(String infoCount) {
		this.infoCount = isStringNullOrEmpty(infoCount) ? STRING_EMPTY : infoCount;
	}
	
	public String getScanDate() {
		return scanDate;
	}
	
	public void setScanDate(String scanDate) {
		this.scanDate = isStringNullOrEmpty(scanDate) ? STRING_EMPTY : scanDate;
	}
	
	public String getScanTime() {
		return scanTime;
	}
	
	public void setScanTime(String scanTime) {
		this.scanTime = isStringNullOrEmpty(scanTime) ? STRING_EMPTY : scanTime;
	}
	
	public String getSignatureSetName() {
		return signatureSetName;
	}
	
	public void setSignatureSetName(String signatureSetName) {
		this.signatureSetName = isStringNullOrEmpty(signatureSetName) ? STRING_EMPTY : signatureSetName;
	}
	
	public String getExcludedPackageNameList() {
		return excludedPackageNameList;
	}
	
	public void setExcludedPackageNameList(String excludedPackageNameList) {
		this.excludedPackageNameList = isStringNullOrEmpty(excludedPackageNameList) ? STRING_EMPTY : excludedPackageNameList;
	}
	
	public String getEngineVersion() {
		return engineVersion;
	}
	
	public void setEngineVersion(String engineVersion) {
		this.engineVersion = isStringNullOrEmpty(engineVersion) ? STRING_EMPTY : engineVersion;
	}
	
	public String getOwnerGroup() {
		return ownerGroup;
	}
	
	public void setOwnerGroup(String ownerGroup) {
		this.ownerGroup = isStringNullOrEmpty(ownerGroup) ? STRING_EMPTY : ownerGroup;
	}
	
	private boolean isStringNullOrEmpty(String string) {
		return string == null || string.isEmpty();
	}
}