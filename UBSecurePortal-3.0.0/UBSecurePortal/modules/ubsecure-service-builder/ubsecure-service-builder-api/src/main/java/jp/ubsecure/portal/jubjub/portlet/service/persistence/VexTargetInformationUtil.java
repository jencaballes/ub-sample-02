/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the vex target information service. This utility wraps {@link jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexTargetInformationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexTargetInformationPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.impl.VexTargetInformationPersistenceImpl
 * @generated
 */
@ProviderType
public class VexTargetInformationUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(VexTargetInformation vexTargetInformation) {
		getPersistence().clearCache(vexTargetInformation);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<VexTargetInformation> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<VexTargetInformation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<VexTargetInformation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<VexTargetInformation> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static VexTargetInformation update(
		VexTargetInformation vexTargetInformation) {
		return getPersistence().update(vexTargetInformation);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static VexTargetInformation update(
		VexTargetInformation vexTargetInformation, ServiceContext serviceContext) {
		return getPersistence().update(vexTargetInformation, serviceContext);
	}

	/**
	* Returns all the vex target informations where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the matching vex target informations
	*/
	public static List<VexTargetInformation> findByScanId(long scanid) {
		return getPersistence().findByScanId(scanid);
	}

	/**
	* Returns a range of all the vex target informations where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @return the range of matching vex target informations
	*/
	public static List<VexTargetInformation> findByScanId(long scanid,
		int start, int end) {
		return getPersistence().findByScanId(scanid, start, end);
	}

	/**
	* Returns an ordered range of all the vex target informations where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching vex target informations
	*/
	public static List<VexTargetInformation> findByScanId(long scanid,
		int start, int end,
		OrderByComparator<VexTargetInformation> orderByComparator) {
		return getPersistence()
				   .findByScanId(scanid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the vex target informations where scanid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param scanid the scanid
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching vex target informations
	*/
	public static List<VexTargetInformation> findByScanId(long scanid,
		int start, int end,
		OrderByComparator<VexTargetInformation> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByScanId(scanid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first vex target information in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex target information
	* @throws NoSuchVexTargetInformationException if a matching vex target information could not be found
	*/
	public static VexTargetInformation findByScanId_First(long scanid,
		OrderByComparator<VexTargetInformation> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexTargetInformationException {
		return getPersistence().findByScanId_First(scanid, orderByComparator);
	}

	/**
	* Returns the first vex target information in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching vex target information, or <code>null</code> if a matching vex target information could not be found
	*/
	public static VexTargetInformation fetchByScanId_First(long scanid,
		OrderByComparator<VexTargetInformation> orderByComparator) {
		return getPersistence().fetchByScanId_First(scanid, orderByComparator);
	}

	/**
	* Returns the last vex target information in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex target information
	* @throws NoSuchVexTargetInformationException if a matching vex target information could not be found
	*/
	public static VexTargetInformation findByScanId_Last(long scanid,
		OrderByComparator<VexTargetInformation> orderByComparator)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexTargetInformationException {
		return getPersistence().findByScanId_Last(scanid, orderByComparator);
	}

	/**
	* Returns the last vex target information in the ordered set where scanid = &#63;.
	*
	* @param scanid the scanid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching vex target information, or <code>null</code> if a matching vex target information could not be found
	*/
	public static VexTargetInformation fetchByScanId_Last(long scanid,
		OrderByComparator<VexTargetInformation> orderByComparator) {
		return getPersistence().fetchByScanId_Last(scanid, orderByComparator);
	}

	/**
	* Removes all the vex target informations where scanid = &#63; from the database.
	*
	* @param scanid the scanid
	*/
	public static void removeByScanId(long scanid) {
		getPersistence().removeByScanId(scanid);
	}

	/**
	* Returns the number of vex target informations where scanid = &#63;.
	*
	* @param scanid the scanid
	* @return the number of matching vex target informations
	*/
	public static int countByScanId(long scanid) {
		return getPersistence().countByScanId(scanid);
	}

	/**
	* Caches the vex target information in the entity cache if it is enabled.
	*
	* @param vexTargetInformation the vex target information
	*/
	public static void cacheResult(VexTargetInformation vexTargetInformation) {
		getPersistence().cacheResult(vexTargetInformation);
	}

	/**
	* Caches the vex target informations in the entity cache if it is enabled.
	*
	* @param vexTargetInformations the vex target informations
	*/
	public static void cacheResult(
		List<VexTargetInformation> vexTargetInformations) {
		getPersistence().cacheResult(vexTargetInformations);
	}

	/**
	* Creates a new vex target information with the primary key. Does not add the vex target information to the database.
	*
	* @param scanid the primary key for the new vex target information
	* @return the new vex target information
	*/
	public static VexTargetInformation create(long scanid) {
		return getPersistence().create(scanid);
	}

	/**
	* Removes the vex target information with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param scanid the primary key of the vex target information
	* @return the vex target information that was removed
	* @throws NoSuchVexTargetInformationException if a vex target information with the primary key could not be found
	*/
	public static VexTargetInformation remove(long scanid)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexTargetInformationException {
		return getPersistence().remove(scanid);
	}

	public static VexTargetInformation updateImpl(
		VexTargetInformation vexTargetInformation) {
		return getPersistence().updateImpl(vexTargetInformation);
	}

	/**
	* Returns the vex target information with the primary key or throws a {@link NoSuchVexTargetInformationException} if it could not be found.
	*
	* @param scanid the primary key of the vex target information
	* @return the vex target information
	* @throws NoSuchVexTargetInformationException if a vex target information with the primary key could not be found
	*/
	public static VexTargetInformation findByPrimaryKey(long scanid)
		throws jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexTargetInformationException {
		return getPersistence().findByPrimaryKey(scanid);
	}

	/**
	* Returns the vex target information with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param scanid the primary key of the vex target information
	* @return the vex target information, or <code>null</code> if a vex target information with the primary key could not be found
	*/
	public static VexTargetInformation fetchByPrimaryKey(long scanid) {
		return getPersistence().fetchByPrimaryKey(scanid);
	}

	public static java.util.Map<java.io.Serializable, VexTargetInformation> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the vex target informations.
	*
	* @return the vex target informations
	*/
	public static List<VexTargetInformation> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the vex target informations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @return the range of vex target informations
	*/
	public static List<VexTargetInformation> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the vex target informations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of vex target informations
	*/
	public static List<VexTargetInformation> findAll(int start, int end,
		OrderByComparator<VexTargetInformation> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the vex target informations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of vex target informations
	* @param end the upper bound of the range of vex target informations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of vex target informations
	*/
	public static List<VexTargetInformation> findAll(int start, int end,
		OrderByComparator<VexTargetInformation> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the vex target informations from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of vex target informations.
	*
	* @return the number of vex target informations
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static VexTargetInformationPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<VexTargetInformationPersistence, VexTargetInformationPersistence> _serviceTracker =
		ServiceTrackerFactory.open(VexTargetInformationPersistence.class);
}