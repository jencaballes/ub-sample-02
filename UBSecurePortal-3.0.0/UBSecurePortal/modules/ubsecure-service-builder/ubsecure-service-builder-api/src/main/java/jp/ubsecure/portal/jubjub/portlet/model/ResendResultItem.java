package jp.ubsecure.portal.jubjub.portlet.model;

public class ResendResultItem {
	private boolean isResendSuccessful;
	private boolean isDetected;
	private String number;
	private String detectionResultId;
	private String resendNumber;
	private String resendDetectionResultId;
	/**
	 * @return the isResendSuccessful
	 */
	public boolean isResendSuccessful() {
		return isResendSuccessful;
	}
	/**
	 * @param isResendSuccessful the isResendSuccessful to set
	 */
	public void setResendSuccessful(boolean isResendSuccessful) {
		this.isResendSuccessful = isResendSuccessful;
	}
	/**
	 * @return the isDetected
	 */
	public boolean isDetected() {
		return isDetected;
	}
	/**
	 * @param isDetected the isDetected to set
	 */
	public void setDetected(boolean isDetected) {
		this.isDetected = isDetected;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the detectionResultId
	 */
	public String getDetectionResultId() {
		return detectionResultId;
	}
	/**
	 * @param detectionResultId the detectionResultId to set
	 */
	public void setDetectionResultId(String detectionResultId) {
		this.detectionResultId = detectionResultId;
	}
	/**
	 * @return the resendNumber
	 */
	public String getResendNumber() {
		return resendNumber;
	}
	/**
	 * @param resendNumber the resendNumber to set
	 */
	public void setResendNumber(String resendNumber) {
		this.resendNumber = resendNumber;
	}
	/**
	 * @return the resendDetectionResultId
	 */
	public String getResendDetectionResultId() {
		return resendDetectionResultId;
	}
	/**
	 * @param resendDetectionResultId the resendDetectionResultId to set
	 */
	public void setResendDetectionResultId(String resendDetectionResultId) {
		this.resendDetectionResultId = resendDetectionResultId;
	}
	
}
