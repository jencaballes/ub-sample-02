package jp.ubsecure.portal.jubjub.portlet.model;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class VexReportOptionsItem {
	private Boolean useCapture;
	private Boolean reCapture;
	private Boolean output_by_signature;
	private Boolean output_by_request;
	private Boolean output_vuln_request_parameters;
	private Boolean output_host;
	private Boolean outputRequestDetail;
	private Boolean outputVulnDetail;
	private Boolean output_appendix;
	private Boolean xls_report_split_flg;
	public VexReportOptionsItem() {
		super();
		this.useCapture = PortalConstants.FALSE;
		this.reCapture = PortalConstants.FALSE;
		this.output_by_signature = PortalConstants.FALSE;
		this.output_by_request = PortalConstants.FALSE;
		this.output_vuln_request_parameters = PortalConstants.FALSE;
		this.output_host = PortalConstants.FALSE;
		this.outputRequestDetail = PortalConstants.FALSE;
		this.outputVulnDetail = PortalConstants.FALSE;
		this.output_appendix = PortalConstants.FALSE;
		this.xls_report_split_flg = PortalConstants.FALSE;
	}
	public Boolean getUseCapture() {
		return useCapture;
	}
	public void setUseCapture(Boolean useCapture) {
		this.useCapture = useCapture;
	}
	public Boolean getReCapture() {
		return reCapture;
	}
	public void setReCapture(Boolean reCapture) {
		this.reCapture = reCapture;
	}
	public Boolean getOutput_by_signature() {
		return output_by_signature;
	}
	public void setOutput_by_signature(Boolean output_by_signature) {
		this.output_by_signature = output_by_signature;
	}
	public Boolean getOutput_by_request() {
		return output_by_request;
	}
	public void setOutput_by_request(Boolean output_by_request) {
		this.output_by_request = output_by_request;
	}
	public Boolean getOutput_vuln_request_parameters() {
		return output_vuln_request_parameters;
	}
	public void setOutput_vuln_request_parameters(Boolean output_vuln_request_parameters) {
		this.output_vuln_request_parameters = output_vuln_request_parameters;
	}
	public Boolean getOutput_host() {
		return output_host;
	}
	public void setOutput_host(Boolean output_host) {
		this.output_host = output_host;
	}
	public Boolean getOutputRequestDetail() {
		return outputRequestDetail;
	}
	public void setOutputRequestDetail(Boolean outputRequestDetail) {
		this.outputRequestDetail = outputRequestDetail;
	}
	public Boolean getOutputVulnDetail() {
		return outputVulnDetail;
	}
	public void setOutputVulnDetail(Boolean outputVulnDetail) {
		this.outputVulnDetail = outputVulnDetail;
	}
	public Boolean getOutput_appendix() {
		return output_appendix;
	}
	public void setOutput_appendix(Boolean output_appendix) {
		this.output_appendix = output_appendix;
	}
	public Boolean getXls_report_split_flg() {
		return xls_report_split_flg;
	}
	public void setXls_report_split_flg(Boolean xls_report_split_flg) {
		this.xls_report_split_flg = xls_report_split_flg;
	}
}
