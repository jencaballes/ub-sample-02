/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ProjectUsers}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProjectUsers
 * @generated
 */
@ProviderType
public class ProjectUsersWrapper implements ProjectUsers,
	ModelWrapper<ProjectUsers> {
	public ProjectUsersWrapper(ProjectUsers projectUsers) {
		_projectUsers = projectUsers;
	}

	@Override
	public Class<?> getModelClass() {
		return ProjectUsers.class;
	}

	@Override
	public String getModelClassName() {
		return ProjectUsers.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("userId", getUserId());
		attributes.put("projectId", getProjectId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String userId = (String)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long projectId = (Long)attributes.get("projectId");

		if (projectId != null) {
			setProjectId(projectId);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _projectUsers.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _projectUsers.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _projectUsers.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _projectUsers.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers> toCacheModel() {
		return _projectUsers.toCacheModel();
	}

	@Override
	public int compareTo(
		jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers projectUsers) {
		return _projectUsers.compareTo(projectUsers);
	}

	@Override
	public int hashCode() {
		return _projectUsers.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _projectUsers.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProjectUsersWrapper((ProjectUsers)_projectUsers.clone());
	}

	/**
	* Returns the user ID of this project users.
	*
	* @return the user ID of this project users
	*/
	@Override
	public java.lang.String getUserId() {
		return _projectUsers.getUserId();
	}

	@Override
	public java.lang.String toString() {
		return _projectUsers.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _projectUsers.toXmlString();
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers toEscapedModel() {
		return new ProjectUsersWrapper(_projectUsers.toEscapedModel());
	}

	@Override
	public jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers toUnescapedModel() {
		return new ProjectUsersWrapper(_projectUsers.toUnescapedModel());
	}

	/**
	* Returns the primary key of this project users.
	*
	* @return the primary key of this project users
	*/
	@Override
	public jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK getPrimaryKey() {
		return _projectUsers.getPrimaryKey();
	}

	/**
	* Returns the project ID of this project users.
	*
	* @return the project ID of this project users
	*/
	@Override
	public long getProjectId() {
		return _projectUsers.getProjectId();
	}

	@Override
	public void persist() {
		_projectUsers.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_projectUsers.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_projectUsers.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_projectUsers.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_projectUsers.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_projectUsers.setNew(n);
	}

	/**
	* Sets the primary key of this project users.
	*
	* @param primaryKey the primary key of this project users
	*/
	@Override
	public void setPrimaryKey(
		jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK primaryKey) {
		_projectUsers.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_projectUsers.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the project ID of this project users.
	*
	* @param projectId the project ID of this project users
	*/
	@Override
	public void setProjectId(long projectId) {
		_projectUsers.setProjectId(projectId);
	}

	/**
	* Sets the user ID of this project users.
	*
	* @param userId the user ID of this project users
	*/
	@Override
	public void setUserId(java.lang.String userId) {
		_projectUsers.setUserId(userId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectUsersWrapper)) {
			return false;
		}

		ProjectUsersWrapper projectUsersWrapper = (ProjectUsersWrapper)obj;

		if (Objects.equals(_projectUsers, projectUsersWrapper._projectUsers)) {
			return true;
		}

		return false;
	}

	@Override
	public ProjectUsers getWrappedModel() {
		return _projectUsers;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _projectUsers.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _projectUsers.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_projectUsers.resetOriginalValues();
	}

	private final ProjectUsers _projectUsers;
}