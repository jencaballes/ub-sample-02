package jp.ubsecure.portal.jubjub.portlet.model;

import java.util.Date;

public class VexDetectionResultItem {
	  private long scanresultid;
	  private String detectionresultid;
	  private long scanid;
	  private int risklevel;
	  private String category;
	  private String overview;
	  private String functionname;
	  private String url;
	  private String parametername;
	  private String detectionjudgment;
	  private String reviewcomment;
	  private int isresendinvex;
	  private String listofdetectedresult;
	  private String listofinspectiondatetime;
	/**
	 * @return the scanresultid
	 */
	public long getScanresultid() {
		return scanresultid;
	}
	/**
	 * @param scanresultid the scanresultid to set
	 */
	public void setScanresultid(long scanresultid) {
		this.scanresultid = scanresultid;
	}
	/**
	 * @return the detectionresultid
	 */
	public String getDetectionresultid() {
		return detectionresultid;
	}
	/**
	 * @param detectionresultid the detectionresultid to set
	 */
	public void setDetectionresultid(String detectionresultid) {
		this.detectionresultid = detectionresultid;
	}
	/**
	 * @return the scanid
	 */
	public long getScanid() {
		return scanid;
	}
	/**
	 * @param scanid the scanid to set
	 */
	public void setScanid(long scanid) {
		this.scanid = scanid;
	}
	/**
	 * @return the risklevel
	 */
	public int getRisklevel() {
		return risklevel;
	}
	/**
	 * @param risklevel the risklevel to set
	 */
	public void setRisklevel(int risklevel) {
		this.risklevel = risklevel;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the overview
	 */
	public String getOverview() {
		return overview;
	}
	/**
	 * @param overview the overview to set
	 */
	public void setOverview(String overview) {
		this.overview = overview;
	}
	/**
	 * @return the functionname
	 */
	public String getFunctionname() {
		return functionname;
	}
	/**
	 * @param functionname the functionname to set
	 */
	public void setFunctionname(String functionname) {
		this.functionname = functionname;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the parametername
	 */
	public String getParametername() {
		return parametername;
	}
	/**
	 * @param parametername the parametername to set
	 */
	public void setParametername(String parametername) {
		this.parametername = parametername;
	}
	/**
	 * @return the detectionjudgment
	 */
	public String getDetectionjudgment() {
		return detectionjudgment;
	}
	/**
	 * @param detectionjudgment the detectionjudgment to set
	 */
	public void setDetectionjudgment(String detectionjudgment) {
		this.detectionjudgment = detectionjudgment;
	}
	/**
	 * @return the reviewcomment
	 */
	public String getReviewcomment() {
		return reviewcomment;
	}
	/**
	 * @param reviewcomment the reviewcomment to set
	 */
	public void setReviewcomment(String reviewcomment) {
		this.reviewcomment = reviewcomment;
	}
	/**
	 * @return the isresendinvex
	 */
	public int getIsresendinvex() {
		return isresendinvex;
	}
	/**
	 * @param isresendinvex the isresendinvex to set
	 */
	public void setIsresendinvex(int isresendinvex) {
		this.isresendinvex = isresendinvex;
	}
	/**
	 * @return the listofdetectedresult
	 */
	public String getListofdetectedresult() {
		return listofdetectedresult;
	}
	/**
	 * @param listofdetectedresult the listofdetectedresult to set
	 */
	public void setListofdetectedresult(String listofdetectedresult) {
		this.listofdetectedresult = listofdetectedresult;
	}
	/**
	 * @return the listofinspectiondatetime
	 */
	public String getListofinspectiondatetime() {
		return listofinspectiondatetime;
	}
	/**
	 * @param listofinspectiondatetime the listofinspectiondatetime to set
	 */
	public void setListofinspectiondatetime(String listofinspectiondatetime) {
		this.listofinspectiondatetime = listofinspectiondatetime;
	}
	
}