package jp.ubsecure.portal.jubjub.portlet.model;

public class VexTargetInformationItem {
	private long scanid;
	private String protocol;
	private String host;
	private String port;
	private int httpversion;
	private int setkeepaliveconnection;
	private int setresponsecontentlength;
	private int useacceptencodingheader;
	private int unzipresponse;
	private String httpprotocol;
	private String externalproxyhost;
	private int externalproxyport;
	private String externalproxyauthid;
	private String externalproxyauthpassword;
	private int useclientcertificate;
	private String certificatefile;
	private String certificatefilepassword;
	private String ntlmauthid;
	private String ntlmauthpassword;
	private String ntlmauthdomain;
	private String ntlmauthhost;
	private String digestauthid;
	private String digestauthpassword;
	private String basicauthid;
	private String basicauthpassword;
	private String accessexclusionpath;
	public long getScanid() {
		return scanid;
	}
	public void setScanid(long scanid) {
		this.scanid = scanid;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public int getHttpversion() {
		return httpversion;
	}
	public void setHttpversion(int httpversion) {
		this.httpversion = httpversion;
	}
	public int getSetkeepaliveconnection() {
		return setkeepaliveconnection;
	}
	public void setSetkeepaliveconnection(int setkeepaliveconnection) {
		this.setkeepaliveconnection = setkeepaliveconnection;
	}
	public int getSetresponsecontentlength() {
		return setresponsecontentlength;
	}
	public void setSetresponsecontentlength(int setresponsecontentlength) {
		this.setresponsecontentlength = setresponsecontentlength;
	}
	public int getUseacceptencodingheader() {
		return useacceptencodingheader;
	}
	public void setUseacceptencodingheader(int useacceptencodingheader) {
		this.useacceptencodingheader = useacceptencodingheader;
	}
	public int getUnzipresponse() {
		return unzipresponse;
	}
	public void setUnzipresponse(int unzipresponse) {
		this.unzipresponse = unzipresponse;
	}
	public String getHttpprotocol() {
		return httpprotocol;
	}
	public void setHttpprotocol(String httpprotocol) {
		this.httpprotocol = httpprotocol;
	}
	public String getExternalproxyhost() {
		return externalproxyhost;
	}
	public void setExternalproxyhost(String externalproxyhost) {
		this.externalproxyhost = externalproxyhost;
	}
	public int getExternalproxyport() {
		return externalproxyport;
	}
	public void setExternalproxyport(int externalproxyport) {
		this.externalproxyport = externalproxyport;
	}
	public String getExternalproxyauthid() {
		return externalproxyauthid;
	}
	public void setExternalproxyauthid(String externalproxyauthid) {
		this.externalproxyauthid = externalproxyauthid;
	}
	public String getExternalproxyauthpassword() {
		return externalproxyauthpassword;
	}
	public void setExternalproxyauthpassword(String externalproxyauthpassword) {
		this.externalproxyauthpassword = externalproxyauthpassword;
	}
	public int getUseclientcertificate() {
		return useclientcertificate;
	}
	public void setUseclientcertificate(int useclientcertificate) {
		this.useclientcertificate = useclientcertificate;
	}
	public String getCertificatefile() {
		return certificatefile;
	}
	public void setCertificatefile(String certificatefile) {
		this.certificatefile = certificatefile;
	}
	public String getCertificatefilepassword() {
		return certificatefilepassword;
	}
	public void setCertificatefilepassword(String certificatefilepassword) {
		this.certificatefilepassword = certificatefilepassword;
	}
	public String getNtlmauthid() {
		return ntlmauthid;
	}
	public void setNtlmauthid(String ntlmauthid) {
		this.ntlmauthid = ntlmauthid;
	}
	public String getNtlmauthpassword() {
		return ntlmauthpassword;
	}
	public void setNtlmauthpassword(String ntlmauthpassword) {
		this.ntlmauthpassword = ntlmauthpassword;
	}
	public String getNtlmauthdomain() {
		return ntlmauthdomain;
	}
	public void setNtlmauthdomain(String ntlmauthdomain) {
		this.ntlmauthdomain = ntlmauthdomain;
	}
	public String getNtlmauthhost() {
		return ntlmauthhost;
	}
	public void setNtlmauthhost(String ntlmauthhost) {
		this.ntlmauthhost = ntlmauthhost;
	}
	public String getDigestauthid() {
		return digestauthid;
	}
	public void setDigestauthid(String digestauthid) {
		this.digestauthid = digestauthid;
	}
	public String getDigestauthpassword() {
		return digestauthpassword;
	}
	public void setDigestauthpassword(String digestauthpassword) {
		this.digestauthpassword = digestauthpassword;
	}
	public String getBasicauthid() {
		return basicauthid;
	}
	public void setBasicauthid(String basicauthid) {
		this.basicauthid = basicauthid;
	}
	public String getBasicauthpassword() {
		return basicauthpassword;
	}
	public void setBasicauthpassword(String basicauthpassword) {
		this.basicauthpassword = basicauthpassword;
	}
	public String getAccessexclusionpath() {
		return accessexclusionpath;
	}
	public void setAccessexclusionpath(String accessexclusionpath) {
		this.accessexclusionpath = accessexclusionpath;
	}
}