package jp.ubsecure.portal.jubjub.portlet.model;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class VexEnvRemarkItem {
	private String remark;
	public VexEnvRemarkItem() {
		super();
		this.remark = PortalConstants.STRING_EMPTY;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
