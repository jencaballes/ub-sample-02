package jp.ubsecure.portal.jubjub.portlet.model;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class VexTargetFuncItem {
	private String function_name;
	public VexTargetFuncItem() {
		super();
		this.function_name = PortalConstants.STRING_EMPTY;
	}
	public String getFunction_name() {
		return function_name;
	}
	public void setFunction_name(String function_name) {
		this.function_name = function_name;
	}
}
