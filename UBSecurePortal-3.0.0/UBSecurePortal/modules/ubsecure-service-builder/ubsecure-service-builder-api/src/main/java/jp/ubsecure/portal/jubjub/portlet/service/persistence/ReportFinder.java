/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence;

import aQute.bnd.annotation.ProviderType;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public interface ReportFinder {
	public java.util.List<jp.ubsecure.portal.jubjub.portlet.model.Report> getReports(
		long scanId, int[] reportTypeArr, int status)
		throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> getCSVReportsByProjectTypeStartDateEndDate(
		int projectType, java.util.Calendar calStartDate,
		java.util.Calendar calEndDate)
		throws com.liferay.portal.kernel.exception.PortalException;

	public jp.ubsecure.portal.jubjub.portlet.model.Report getReferences(
		long scanId, int reportType, int status)
		throws com.liferay.portal.kernel.exception.PortalException;

	public java.util.List<java.lang.Object> getReportsByReportTypeProjectTypeStartDateEndDate(
		int reportType, int projectType, java.util.Calendar calStartDate,
		java.util.Calendar calEndDate)
		throws com.liferay.portal.kernel.exception.PortalException;

	public long getMaxReportId()
		throws com.liferay.portal.kernel.exception.PortalException;
}