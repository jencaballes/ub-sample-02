/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class VexLoginSettingSoap implements Serializable {
	public static VexLoginSettingSoap toSoapModel(VexLoginSetting model) {
		VexLoginSettingSoap soapModel = new VexLoginSettingSoap();

		soapModel.setScanid(model.getScanid());
		soapModel.setLoginurl(model.getLoginurl());
		soapModel.setParamname(model.getParamname());
		soapModel.setParamvalue(model.getParamvalue());

		return soapModel;
	}

	public static VexLoginSettingSoap[] toSoapModels(VexLoginSetting[] models) {
		VexLoginSettingSoap[] soapModels = new VexLoginSettingSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static VexLoginSettingSoap[][] toSoapModels(
		VexLoginSetting[][] models) {
		VexLoginSettingSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new VexLoginSettingSoap[models.length][models[0].length];
		}
		else {
			soapModels = new VexLoginSettingSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static VexLoginSettingSoap[] toSoapModels(
		List<VexLoginSetting> models) {
		List<VexLoginSettingSoap> soapModels = new ArrayList<VexLoginSettingSoap>(models.size());

		for (VexLoginSetting model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new VexLoginSettingSoap[soapModels.size()]);
	}

	public VexLoginSettingSoap() {
	}

	public long getPrimaryKey() {
		return _scanid;
	}

	public void setPrimaryKey(long pk) {
		setScanid(pk);
	}

	public long getScanid() {
		return _scanid;
	}

	public void setScanid(long scanid) {
		_scanid = scanid;
	}

	public String getLoginurl() {
		return _loginurl;
	}

	public void setLoginurl(String loginurl) {
		_loginurl = loginurl;
	}

	public String getParamname() {
		return _paramname;
	}

	public void setParamname(String paramname) {
		_paramname = paramname;
	}

	public String getParamvalue() {
		return _paramvalue;
	}

	public void setParamvalue(String paramvalue) {
		_paramvalue = paramvalue;
	}

	private long _scanid;
	private String _loginurl;
	private String _paramname;
	private String _paramvalue;
}