/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VexDetectionResult in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResult
 * @generated
 */
@ProviderType
public class VexDetectionResultCacheModel implements CacheModel<VexDetectionResult>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexDetectionResultCacheModel)) {
			return false;
		}

		VexDetectionResultCacheModel vexDetectionResultCacheModel = (VexDetectionResultCacheModel)obj;

		if (vexDetectionResultPK.equals(
					vexDetectionResultCacheModel.vexDetectionResultPK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, vexDetectionResultPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{scanresultid=");
		sb.append(scanresultid);
		sb.append(", scanid=");
		sb.append(scanid);
		sb.append(", detectionresultid=");
		sb.append(detectionresultid);
		sb.append(", risklevel=");
		sb.append(risklevel);
		sb.append(", category=");
		sb.append(category);
		sb.append(", overview=");
		sb.append(overview);
		sb.append(", functionname=");
		sb.append(functionname);
		sb.append(", url=");
		sb.append(url);
		sb.append(", parametername=");
		sb.append(parametername);
		sb.append(", detectionjudgment=");
		sb.append(detectionjudgment);
		sb.append(", reviewcomment=");
		sb.append(reviewcomment);
		sb.append(", isresendinvex=");
		sb.append(isresendinvex);
		sb.append(", listofdetectedresult=");
		sb.append(listofdetectedresult);
		sb.append(", listofinspectiondatetime=");
		sb.append(listofinspectiondatetime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public VexDetectionResult toEntityModel() {
		VexDetectionResultImpl vexDetectionResultImpl = new VexDetectionResultImpl();

		vexDetectionResultImpl.setScanresultid(scanresultid);
		vexDetectionResultImpl.setScanid(scanid);

		if (detectionresultid == null) {
			vexDetectionResultImpl.setDetectionresultid(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setDetectionresultid(detectionresultid);
		}

		vexDetectionResultImpl.setRisklevel(risklevel);

		if (category == null) {
			vexDetectionResultImpl.setCategory(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setCategory(category);
		}

		if (overview == null) {
			vexDetectionResultImpl.setOverview(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setOverview(overview);
		}

		if (functionname == null) {
			vexDetectionResultImpl.setFunctionname(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setFunctionname(functionname);
		}

		if (url == null) {
			vexDetectionResultImpl.setUrl(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setUrl(url);
		}

		if (parametername == null) {
			vexDetectionResultImpl.setParametername(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setParametername(parametername);
		}

		if (detectionjudgment == null) {
			vexDetectionResultImpl.setDetectionjudgment(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setDetectionjudgment(detectionjudgment);
		}

		if (reviewcomment == null) {
			vexDetectionResultImpl.setReviewcomment(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setReviewcomment(reviewcomment);
		}

		vexDetectionResultImpl.setIsresendinvex(isresendinvex);

		if (listofdetectedresult == null) {
			vexDetectionResultImpl.setListofdetectedresult(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setListofdetectedresult(listofdetectedresult);
		}

		if (listofinspectiondatetime == null) {
			vexDetectionResultImpl.setListofinspectiondatetime(StringPool.BLANK);
		}
		else {
			vexDetectionResultImpl.setListofinspectiondatetime(listofinspectiondatetime);
		}

		vexDetectionResultImpl.resetOriginalValues();

		return vexDetectionResultImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		scanresultid = objectInput.readLong();

		scanid = objectInput.readLong();
		detectionresultid = objectInput.readUTF();

		risklevel = objectInput.readInt();
		category = objectInput.readUTF();
		overview = objectInput.readUTF();
		functionname = objectInput.readUTF();
		url = objectInput.readUTF();
		parametername = objectInput.readUTF();
		detectionjudgment = objectInput.readUTF();
		reviewcomment = objectInput.readUTF();

		isresendinvex = objectInput.readInt();
		listofdetectedresult = objectInput.readUTF();
		listofinspectiondatetime = objectInput.readUTF();

		vexDetectionResultPK = new VexDetectionResultPK(scanresultid, scanid);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(scanresultid);

		objectOutput.writeLong(scanid);

		if (detectionresultid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(detectionresultid);
		}

		objectOutput.writeInt(risklevel);

		if (category == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(category);
		}

		if (overview == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(overview);
		}

		if (functionname == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(functionname);
		}

		if (url == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(url);
		}

		if (parametername == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(parametername);
		}

		if (detectionjudgment == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(detectionjudgment);
		}

		if (reviewcomment == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reviewcomment);
		}

		objectOutput.writeInt(isresendinvex);

		if (listofdetectedresult == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(listofdetectedresult);
		}

		if (listofinspectiondatetime == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(listofinspectiondatetime);
		}
	}

	public long scanresultid;
	public long scanid;
	public String detectionresultid;
	public int risklevel;
	public String category;
	public String overview;
	public String functionname;
	public String url;
	public String parametername;
	public String detectionjudgment;
	public String reviewcomment;
	public int isresendinvex;
	public String listofdetectedresult;
	public String listofinspectiondatetime;
	public transient VexDetectionResultPK vexDetectionResultPK;
}