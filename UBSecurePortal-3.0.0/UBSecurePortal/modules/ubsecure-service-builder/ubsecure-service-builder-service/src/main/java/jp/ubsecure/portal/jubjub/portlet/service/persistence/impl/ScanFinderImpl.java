package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.dao.orm.custom.sql.CustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.ResultItem;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.ScanItem;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ScanImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ScanFinder;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * ScanFinder Implementation
 */
public class ScanFinderImpl extends ScanFinderBaseImpl implements ScanFinder {

	/** UBSPortalDebugger */ 
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ScanFinderImpl.class);
	
	/** GET_SCANS */
	private static final String GET_SCANS = ScanFinder.class.getName() + ".getScans";
	
	/** GET_SCANS_COUNT */
	private static final String GET_SCANS_COUNT = ScanFinder.class.getName() + ".getScansCount";
	
	/** SORT_SCANS */
	private static final String SORT_SCANS = ScanFinder.class.getName() + ".sortScans";
	
	/** COUNT_SCAN_WAITING */
	private static final String COUNT_SCAN_WAITING = ScanFinder.class.getName() + ".countScanWaiting";
	
	/** GET_SCAN_STATUS */
	private static final String GET_SCAN_STATUS = ScanFinder.class.getName() + ".getScanStatus";
	
	/** GET_ENTIRE_SCANS */ 
	private static final String GET_ENTIRE_SCANS = ScanFinder.class.getName() + ".getEntireScans";
	
	/** GET_ENTIRE_SCANS_COUNT */
	private static final String GET_ENTIRE_SCANS_COUNT = ScanFinder.class.getName() + ".getEntireScansCount";
	
	/** SORT_ENTIRE_SCANS */
	private static final String SORT_ENTIRE_SCANS = ScanFinder.class.getName() + ".sortEntireScans";
	
	/** COUNT_CRAWL_WAITING */
	private static final String COUNT_CRAWL_WAITING = ScanFinder.class.getName() + ".countCrawlWaiting";
	
	/**
	 * Get Scan
	 * 
	 * @param lProjectId
	 *            project id of the scan
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @param iStart
	 *            index for the start position
	 * @return List of Scans
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getScans(long lProjectId, Map<String, Object> searchedScan, int iStart) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();
		SQLQuery sqlQuery = null;

		try {
			if (lProjectId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			sqlQuery = this.generateScanSQLQuery(CustomSQLUtil.get(getClass(), GET_SCANS), searchedScan);

			QueryPos qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(lProjectId);

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				Object oStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
				Object oScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
				Object oScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
				Object oImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);

				if (!CommonUtil.isObjectNull(oScanId)) {
					String strScanId = oScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
						qPos.add(Long.parseLong(strScanId));
					}
				}

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strFileName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strHashValue.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strScanManager.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteScanRegDateLow = null;
				Calendar dteScanRegDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					try {
						dteScanRegDateLow = format.parse(strScanRegDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					try {
						dteScanRegDateHigh = Calendar.getInstance();
						dteScanRegDateHigh.setTime(format.parse(strScanRegDateHigh));
						dteScanRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteScanRegDateHigh.set(Calendar.MINUTE, 59);
						dteScanRegDateHigh.set(Calendar.SECOND, 59);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanRegDateLow) && !CommonUtil.isObjectNull(dteScanRegDateHigh)) {
					qPos.add(dteScanRegDateLow);
					qPos.add(dteScanRegDateHigh.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					qPos.add(dteScanRegDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					qPos.add(dteScanRegDateHigh.getTime());
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						String[] strStatusArr = strStatus.split(PortalConstants.COMMA);

						if (!CommonUtil.isObjectNull(strStatusArr) && strStatusArr.length > PortalConstants.INT_ZERO) {
							int[] iStatusArr = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
							int index = PortalConstants.INT_ZERO;

							for (String status : strStatusArr) {
								if (!CommonUtil.isStringNullOrEmpty(status)) {
									int iStatus = Integer.parseInt(status);

									if (iStatus == ScanStatus.REPORT_MAKING.getInteger()) {
										iStatusArr[index] = ScanStatus.REPORT_MAKING.getInteger();
										++index;
										iStatusArr[index] = ScanStatus.REGENERATING.getInteger();
									} else {
										iStatusArr[index] = iStatus;
									}
									index++;
								}
							}

							qPos.add(iStatusArr);
						}
					}
				}

				if (!CommonUtil.isObjectNull(oProcess)) {
					String strProcess = oProcess.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProcess)) {
						qPos.add(Integer.parseInt(strProcess));
					}
				}

				if (!CommonUtil.isObjectNull(oCxScanId)) {
					String strCxScanId = oCxScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
						qPos.add(strCxScanId);
					}
				}
				
				if (!CommonUtil.isObjectNull(oStartUrl)) {
					String strStartUrl = oStartUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStartUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strStartUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
								
				String strStartTime = null;
				String strEndTime = null;

				if (!CommonUtil.isObjectNull(oScanStartTime)) {
					strStartTime = oScanStartTime.toString();
				}

				if (!CommonUtil.isObjectNull(oScanEndTime)) {
					strEndTime = oScanEndTime.toString();
				}

				Calendar dteScanStartTime = null;
				Calendar dteScanEndTime = null;
				DateFormat datetimeFormat = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
				if (!CommonUtil.isStringNullOrEmpty(strStartTime)) {
					try {
						dteScanStartTime = Calendar.getInstance();
						dteScanStartTime.setTime(datetimeFormat.parse(strStartTime));
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_START_TIME));
						throw new PortalException(PortalErrors.INVALID_SCAN_START_TIME);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strEndTime)) {
					try {
						dteScanEndTime = Calendar.getInstance();
						dteScanEndTime.setTime(datetimeFormat.parse(strEndTime));
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_END_TIME));
						throw new PortalException(PortalErrors.INVALID_SCAN_END_TIME);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanStartTime) && !CommonUtil.isObjectNull(dteScanEndTime)) {
					qPos.add(dteScanStartTime.getTime());
					qPos.add(dteScanEndTime.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strStartTime)) {
					qPos.add(dteScanStartTime.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strEndTime)) {
					qPos.add(dteScanEndTime.getTime());
				}
				
				if (!CommonUtil.isObjectNull(oImplementationEnvironment)) {
					String strImplementationEnvironment = oImplementationEnvironment.toString();

					if (!CommonUtil.isStringNullOrEmpty(strImplementationEnvironment)) {
						qPos.add(Integer.parseInt(strImplementationEnvironment));
					}
				}
						
			}

			qPos.add(iStart);
			qPos.add(PortalConstants.PAGINATION_DELTA);

			scanList = (List<Object>) sqlQuery.list();

			resultList = this.getScanResults(scanList);
		} catch (ORMException orme) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCANS, params, pe);
			throw pe;
		} finally {
			if (!scanList.isEmpty()) {
				scanList.clear();
				scanList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Get All Scans
	 * 
	 * @param iType
	 *            a type of a project(android, cxsuite, ios)
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @param iStart
	 *            index for the start position
	 * @return List of scans
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getEntireScans(int iType, Map<String, Object> searchedScan, int iStart) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> entireScanList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();

		try {
			if (iType == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			SQLQuery sqlQuery = this.generateEntireScanSQLQuery(CustomSQLUtil.get(getClass(), GET_ENTIRE_SCANS),
					searchedScan);

			QueryPos qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(iType);

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				Object oStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
				Object oScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
				Object oScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
				Object oImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);

				if (!CommonUtil.isObjectNull(oScanId)) {
					String strScanId = oScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
						qPos.add(Long.parseLong(strScanId));
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strFileName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strHashValue.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strScanManager.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteScanRegDateLow = null;
				Calendar dteScanRegDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					try {
						dteScanRegDateLow = format.parse(strScanRegDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					try {
						dteScanRegDateHigh = Calendar.getInstance();
						dteScanRegDateHigh.setTime(format.parse(strScanRegDateHigh));
						dteScanRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteScanRegDateHigh.set(Calendar.MINUTE, 59);
						dteScanRegDateHigh.set(Calendar.SECOND, 59);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanRegDateLow) && !CommonUtil.isObjectNull(dteScanRegDateHigh)) {
					qPos.add(dteScanRegDateLow);
					qPos.add(dteScanRegDateHigh.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					qPos.add(dteScanRegDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					qPos.add(dteScanRegDateHigh.getTime());
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();
					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						String[] strStatusArr = strStatus.split(PortalConstants.COMMA);

						if (!CommonUtil.isObjectNull(strStatusArr) && strStatusArr.length > PortalConstants.INT_ZERO) {
							int[] iStatusArr = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
							int index = PortalConstants.INT_ZERO;

							for (String status : strStatusArr) {
								if (!CommonUtil.isStringNullOrEmpty(status)) {
									int iStatus = Integer.parseInt(status);

									if (iStatus == ScanStatus.REPORT_MAKING.getInteger()) {
										iStatusArr[index] = ScanStatus.REPORT_MAKING.getInteger();
										++index;
										iStatusArr[index] = ScanStatus.REGENERATING.getInteger();
									} else {
										iStatusArr[index] = iStatus;
									}
									index++;
								}
							}
							qPos.add(iStatusArr);
						}
					}
				}

				if (!CommonUtil.isObjectNull(oProcess)) {
					String strProcess = oProcess.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProcess)) {
						qPos.add(Integer.parseInt(strProcess));
					}
				}

				if (!CommonUtil.isObjectNull(oCxScanId)) {
					String strCxScanId = oCxScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
						qPos.add(strCxScanId);
					}
				}
				
				if (!CommonUtil.isObjectNull(oStartUrl)) {
					String strStartUrl = oStartUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStartUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strStartUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				String strStartTime = null;
				String strEndTime = null;

				if (!CommonUtil.isObjectNull(oScanStartTime)) {
					strStartTime = oScanStartTime.toString();
				}

				if (!CommonUtil.isObjectNull(oScanEndTime)) {
					strEndTime = oScanEndTime.toString();
				}

				Calendar dteScanStartTime = null;
				Calendar dteScanEndTime = null;

				DateFormat datetimeFormat = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
				if (!CommonUtil.isStringNullOrEmpty(strStartTime)) {
					try {
						dteScanStartTime = Calendar.getInstance();
						dteScanStartTime.setTime(datetimeFormat.parse(strStartTime));
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_START_TIME));
						throw new PortalException(PortalErrors.INVALID_SCAN_START_TIME);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strEndTime)) {
					try {
						dteScanEndTime = Calendar.getInstance();
						dteScanEndTime.setTime(datetimeFormat.parse(strEndTime));
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_END_TIME));
						throw new PortalException(PortalErrors.INVALID_SCAN_END_TIME);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanStartTime) && !CommonUtil.isObjectNull(dteScanEndTime)) {
					qPos.add(dteScanStartTime.getTime());
					qPos.add(dteScanEndTime.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strStartTime)) {
					qPos.add(dteScanStartTime.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strEndTime)) {
					qPos.add(dteScanEndTime.getTime());
				}
				
				if (!CommonUtil.isObjectNull(oImplementationEnvironment)) {
					String strImplementationEnvironment = oImplementationEnvironment.toString();

					if (!CommonUtil.isStringNullOrEmpty(strImplementationEnvironment)) {
						qPos.add(Integer.parseInt(strImplementationEnvironment));
					}
				}
			}

			qPos.add(iStart);
			qPos.add(PortalConstants.PAGINATION_DELTA);

			entireScanList = (List<Object>) sqlQuery.list();

			resultList = this.getEntireScanResults(entireScanList);
		} catch (ORMException orme) {
			params.put("iType", iType);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("iTYpe", iType);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
			throw pe;
		} finally {
			if (!entireScanList.isEmpty()) {
				entireScanList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Get the total number of scans
	 * 
	 * @param type
	 *            a type of a project(android, cxsuite, ios)
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @return Total number of scans
	 * @throws PortalException
	 * 
	 */
	@SuppressWarnings("unchecked")
	public int getEntireScansCount(int type, Map<String, Object> searchedScan) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = PortalConstants.INT_ZERO;
		List<Scan> scanList = new ArrayList<Scan>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (type == PortalConstants.INT_ZERO) {
				params.put("type", type);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			sqlQuery = this.generateEntireScanCountSQLQuery(CustomSQLUtil.get(getClass(), GET_ENTIRE_SCANS_COUNT),
					searchedScan);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(type);

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				
				if (!CommonUtil.isObjectNull(oScanId)) {
					String strScanId = oScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
						long lScanId = Long.parseLong(strScanId);

						qPos.add(lScanId);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strFileName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strHashValue.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strScanManager.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteScanRegDateLow = null;
				Date dteScanRegDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					try {
						dteScanRegDateLow = format.parse(strScanRegDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					try {
						dteScanRegDateHigh = format.parse(strScanRegDateHigh);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanRegDateLow) && !CommonUtil.isObjectNull(dteScanRegDateHigh)) {
					qPos.add(dteScanRegDateLow);
					qPos.add(dteScanRegDateHigh);
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					qPos.add(dteScanRegDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					qPos.add(dteScanRegDateHigh);
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						String[] strStatusArr = strStatus.split(PortalConstants.COMMA);

						if (!CommonUtil.isObjectNull(strStatusArr) && strStatusArr.length > PortalConstants.INT_ZERO) {
							int[] iStatusArr = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
							int index = PortalConstants.INT_ZERO;

							for (String status : strStatusArr) {
								if (!CommonUtil.isStringNullOrEmpty(status)) {
									int iStatus = Integer.parseInt(status);

									if (iStatus == ScanStatus.REPORT_MAKING.getInteger()) {
										iStatusArr[index] = ScanStatus.REPORT_MAKING.getInteger();
										++index;
										iStatusArr[index] = ScanStatus.REGENERATING.getInteger();
									} else {
										iStatusArr[index] = iStatus;
									}

									index++;
								}
							}
							qPos.add(iStatusArr);
						}
					}
				}

				if (!CommonUtil.isObjectNull(oProcess)) {
					String strProcess = oProcess.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProcess)) {
						int iProcess = Integer.parseInt(strProcess);

						qPos.add(iProcess);
					}
				}

				if (!CommonUtil.isObjectNull(oCxScanId)) {
					String strCxScanId = oCxScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
						qPos.add(strCxScanId);
					}
				}
				
			}
			
			scanList = sqlQuery.list();

			count = scanList.size();
		} catch (ORMException orme) {
			params.put("type", type);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS_COUNT, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS_COUNT, params, pe);
			throw pe;
		} finally {
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList.clear();
				scanList = null;
			}

			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}

		return count;
	}

	/**
	 * Get the number of scans based on project ID and search item
	 * 
	 * @param lProjectId
	 *            the scan project id
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @return Number of scans
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public int getScansCount(long lProjectId, Map<String, Object> searchedScan) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = PortalConstants.INT_ZERO;
		List<Scan> scanList = new ArrayList<Scan>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put("lProjectId", lProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			sqlQuery = this.generateScanCountSQLQuery(CustomSQLUtil.get(getClass(), GET_SCANS_COUNT), searchedScan);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(lProjectId);

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);

				if (!CommonUtil.isObjectNull(oScanId)) {
					String strScanId = oScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
						long lScanId = Long.parseLong(strScanId);

						qPos.add(lScanId);
					}
				}

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strFileName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strHashValue.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strScanManager.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteScanRegDateLow = null;
				Date dteScanRegDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					try {
						dteScanRegDateLow = format.parse(strScanRegDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					try {
						dteScanRegDateHigh = format.parse(strScanRegDateHigh);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanRegDateLow) && !CommonUtil.isObjectNull(dteScanRegDateHigh)) {
					qPos.add(dteScanRegDateLow);
					qPos.add(dteScanRegDateHigh);
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					qPos.add(dteScanRegDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					qPos.add(dteScanRegDateHigh);
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						String[] strStatusArr = strStatus.split(PortalConstants.COMMA);

						if (!CommonUtil.isObjectNull(strStatusArr) && strStatusArr.length > PortalConstants.INT_ZERO) {
							int[] iStatusArr = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
							int index = PortalConstants.INT_ZERO;

							for (String status : strStatusArr) {
								if (!CommonUtil.isStringNullOrEmpty(status)) {
									int iStatus = Integer.parseInt(status);

									if (iStatus == ScanStatus.REPORT_MAKING.getInteger()) {
										iStatusArr[index] = ScanStatus.REPORT_MAKING.getInteger();
										++index;
										iStatusArr[index] = ScanStatus.REGENERATING.getInteger();
									} else {
										iStatusArr[index] = iStatus;
									}
									index++;
								}
							}
							qPos.add(iStatusArr);
						}
					}
				}

				if (!CommonUtil.isObjectNull(oProcess)) {
					String strProcess = oProcess.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProcess)) {
						int iProcess = Integer.parseInt(strProcess);

						qPos.add(iProcess);
					}
				}

				if (!CommonUtil.isObjectNull(oCxScanId)) {
					String strCxScanId = oCxScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
						qPos.add(strCxScanId);
					}
				}
			}

			scanList = sqlQuery.list();

			count = scanList.size();
		} catch (ORMException orme) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_SCANS_COUNT, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCANS_COUNT, params, pe);
			throw pe;
		} finally {
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList.clear();
				scanList = null;
			}

			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}

		return count;
	}

	/**
	 * Sort scans
	 * 
	 * @param iType
	 *            a type of a project(android, cxsuite, ios)
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @param orderByCol
	 *            a column to order
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param iStart
	 *            index for the start position
	 * @return Sorted Scans
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> sortEntireScans(int iType, Map<String, Object> searchedScan, String orderByCol,
			String orderByType, int iStart) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> entireScanList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();

		try {
			if (iType == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			SQLQuery sqlQuery = this.generateSortEntireScanSQLQuery(CustomSQLUtil.get(getClass(), SORT_ENTIRE_SCANS),
					searchedScan, orderByCol, orderByType);

			QueryPos qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(iType);

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				Object oStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
				Object oScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
				Object oScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
				Object oImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);

				if (!CommonUtil.isObjectNull(oScanId)) {
					String strScanId = oScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
						qPos.add(Long.parseLong(strScanId));
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strFileName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strHashValue.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strScanManager.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteScanRegDateLow = null;
				Calendar dteScanRegDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					try {
						dteScanRegDateLow = format.parse(strScanRegDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					try {
						dteScanRegDateHigh = Calendar.getInstance();
						dteScanRegDateHigh.setTime(format.parse(strScanRegDateHigh));
						dteScanRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteScanRegDateHigh.set(Calendar.MINUTE, 59);
						dteScanRegDateHigh.set(Calendar.SECOND, 59);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanRegDateLow) && !CommonUtil.isObjectNull(dteScanRegDateHigh)) {
					qPos.add(dteScanRegDateLow);
					qPos.add(dteScanRegDateHigh.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					qPos.add(dteScanRegDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					qPos.add(dteScanRegDateHigh.getTime());
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();
					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						String[] strStatusArr = strStatus.split(PortalConstants.COMMA);

						if (!CommonUtil.isObjectNull(strStatusArr) && strStatusArr.length > PortalConstants.INT_ZERO) {
							int[] iStatusArr = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
							int index = PortalConstants.INT_ZERO;

							for (String status : strStatusArr) {
								if (!CommonUtil.isStringNullOrEmpty(status)) {
									int iStatus = Integer.parseInt(status);

									if (iStatus == ScanStatus.REPORT_MAKING.getInteger()) {
										iStatusArr[index] = ScanStatus.REPORT_MAKING.getInteger();
										++index;
										iStatusArr[index] = ScanStatus.REGENERATING.getInteger();
									} else {
										iStatusArr[index] = iStatus;
									}
									index++;
								}
							}
							qPos.add(iStatusArr);
						}
					}
				}

				if (!CommonUtil.isObjectNull(oProcess)) {
					String strProcess = oProcess.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProcess)) {
						qPos.add(Integer.parseInt(strProcess));
					}
				}

				if (!CommonUtil.isObjectNull(oCxScanId)) {
					String strCxScanId = oCxScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
						qPos.add(strCxScanId);
					}
				}
				
				if (!CommonUtil.isObjectNull(oStartUrl)) {
					String strStartUrl = oStartUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStartUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strStartUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				String strStartTime = null;
				String strEndTime = null;

				if (!CommonUtil.isObjectNull(oScanStartTime)) {
					strStartTime = oScanStartTime.toString();
				}

				if (!CommonUtil.isObjectNull(oScanEndTime)) {
					strEndTime = oScanEndTime.toString();
				}

				Calendar dteScanStartTime = null;
				Calendar dteScanEndTime = null;

				DateFormat datetimeFormat = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
				if (!CommonUtil.isStringNullOrEmpty(strStartTime)) {
					try {
						dteScanStartTime = Calendar.getInstance();
						dteScanStartTime.setTime(datetimeFormat.parse(strStartTime));
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_START_TIME));
						throw new PortalException(PortalErrors.INVALID_SCAN_START_TIME);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strEndTime)) {
					try {
						dteScanEndTime = Calendar.getInstance();
						dteScanEndTime.setTime(datetimeFormat.parse(strEndTime));
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_END_TIME));
						throw new PortalException(PortalErrors.INVALID_SCAN_END_TIME);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanStartTime) && !CommonUtil.isObjectNull(dteScanEndTime)) {
					qPos.add(dteScanStartTime.getTime());
					qPos.add(dteScanEndTime.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strStartTime)) {
					qPos.add(dteScanStartTime.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strEndTime)) {
					qPos.add(dteScanEndTime.getTime());
				}
				
				if (!CommonUtil.isObjectNull(oImplementationEnvironment)) {
					String strImplementationEnvironment = oImplementationEnvironment.toString();

					if (!CommonUtil.isStringNullOrEmpty(strImplementationEnvironment)) {
						qPos.add(Integer.parseInt(strImplementationEnvironment));
					}
				}
			}

			qPos.add(iStart);
			qPos.add(PortalConstants.PAGINATION_DELTA);

			entireScanList = (List<Object>) sqlQuery.list();

			resultList = this.getEntireScanResults(entireScanList);
		} catch (ORMException orme) {
			params.put("iType", iType);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("iTYpe", iType);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
			throw pe;
		} finally {
			if (!entireScanList.isEmpty()) {
				entireScanList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Sort Scans
	 * 
	 * @param lProjectId
	 *            Project id of the scans
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @param orderByCol
	 *            a column to order
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param iStart
	 *            index for the start position
	 * @return sorted scans
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> sortScans(long lProjectId, Map<String, Object> searchedScan, String orderByCol,
			String orderByType, int iStart) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();
		SQLQuery sqlQuery = null;

		try {
			if (lProjectId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			sqlQuery = this.generateSortScanSQLQuery(CustomSQLUtil.get(getClass(), SORT_SCANS), searchedScan,
					orderByCol, orderByType);

			QueryPos qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(lProjectId);

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				Object oStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
				Object oScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
				Object oScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
				Object oImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);

				if (!CommonUtil.isObjectNull(oScanId)) {
					String strScanId = oScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
						qPos.add(Long.parseLong(strScanId));
					}
				}

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strFileName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strHashValue.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strScanManager.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteScanRegDateLow = null;
				Calendar dteScanRegDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					try {
						dteScanRegDateLow = format.parse(strScanRegDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					try {
						dteScanRegDateHigh = Calendar.getInstance();
						dteScanRegDateHigh.setTime(format.parse(strScanRegDateHigh));
						dteScanRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteScanRegDateHigh.set(Calendar.MINUTE, 59);
						dteScanRegDateHigh.set(Calendar.SECOND, 59);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanRegDateLow) && !CommonUtil.isObjectNull(dteScanRegDateHigh)) {
					qPos.add(dteScanRegDateLow);
					qPos.add(dteScanRegDateHigh.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					qPos.add(dteScanRegDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					qPos.add(dteScanRegDateHigh.getTime());
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						String[] strStatusArr = strStatus.split(PortalConstants.COMMA);

						if (!CommonUtil.isObjectNull(strStatusArr) && strStatusArr.length > PortalConstants.INT_ZERO) {
							int[] iStatusArr = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
							int index = PortalConstants.INT_ZERO;

							for (String status : strStatusArr) {
								if (!CommonUtil.isStringNullOrEmpty(status)) {
									int iStatus = Integer.parseInt(status);

									if (iStatus == ScanStatus.REPORT_MAKING.getInteger()) {
										iStatusArr[index] = ScanStatus.REPORT_MAKING.getInteger();
										++index;
										iStatusArr[index] = ScanStatus.REGENERATING.getInteger();
									} else {
										iStatusArr[index] = iStatus;
									}
									index++;
								}
							}

							qPos.add(iStatusArr);
						}
					}
				}

				if (!CommonUtil.isObjectNull(oProcess)) {
					String strProcess = oProcess.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProcess)) {
						qPos.add(Integer.parseInt(strProcess));
					}
				}

				if (!CommonUtil.isObjectNull(oCxScanId)) {
					String strCxScanId = oCxScanId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
						qPos.add(strCxScanId);
					}
				}
				
				if (!CommonUtil.isObjectNull(oStartUrl)) {
					String strStartUrl = oStartUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStartUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strStartUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				String strStartTime = null;
				String strEndTime = null;

				if (!CommonUtil.isObjectNull(oScanStartTime)) {
					strStartTime = oScanStartTime.toString();
				}

				if (!CommonUtil.isObjectNull(oScanEndTime)) {
					strEndTime = oScanEndTime.toString();
				}

				Calendar dteScanStartTime = null;
				Calendar dteScanEndTime = null;
				DateFormat datetimeFormat = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
				if (!CommonUtil.isStringNullOrEmpty(strStartTime)) {
					try {
						dteScanStartTime = Calendar.getInstance();
						dteScanStartTime.setTime(datetimeFormat.parse(strStartTime));
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_START_TIME));
						throw new PortalException(PortalErrors.INVALID_SCAN_START_TIME);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strEndTime)) {
					try {
						dteScanEndTime = Calendar.getInstance();
						dteScanEndTime.setTime(datetimeFormat.parse(strEndTime));
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_END_TIME));
						throw new PortalException(PortalErrors.INVALID_SCAN_END_TIME);
					}
				}

				if (!CommonUtil.isObjectNull(dteScanStartTime) && !CommonUtil.isObjectNull(dteScanEndTime)) {
					qPos.add(dteScanStartTime.getTime());
					qPos.add(dteScanEndTime.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strStartTime)) {
					qPos.add(dteScanStartTime.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strEndTime)) {
					qPos.add(dteScanEndTime.getTime());
				}
				
				if (!CommonUtil.isObjectNull(oImplementationEnvironment)) {
					String strImplementationEnvironment = oImplementationEnvironment.toString();

					if (!CommonUtil.isStringNullOrEmpty(strImplementationEnvironment)) {
						qPos.add(Integer.parseInt(strImplementationEnvironment));
					}
				}
			}

			qPos.add(iStart);
			qPos.add(PortalConstants.PAGINATION_DELTA);

			scanList = (List<Object>) sqlQuery.list();

			resultList = this.getScanResults(scanList);
		} catch (ORMException orme) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCANS, params, pe);
			throw pe;
		} finally {
			if (!scanList.isEmpty()) {
				scanList.clear();
				scanList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Count Scan Waiting
	 * 
	 * @param scanRegDate
	 *            scan registration date
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @return Number of scan waiting
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public int countScanWaiting(Date scanRegDate, int type) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = 0;
		List<Scan> scanList = new ArrayList<Scan>();
		String strSql = null;
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		Session session = null;

		try {
			if (scanRegDate == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
			}

			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			session = openSession();
			strSql = CustomSQLUtil.get(getClass(), COUNT_SCAN_WAITING);
			sqlQuery = session.createSQLQuery(strSql);
			sqlQuery.setCacheable(false);
			sqlQuery.addEntity("Scan", ScanImpl.class);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(type);
			qPos.add(scanRegDate);
			scanList = sqlQuery.list();

			count = scanList.size();
		} catch (ORMException orme) {
			params.put("scanRegDate", scanRegDate);
			params.put("type", type);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_COUNT_SCAN_WAITING, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("scanRegDate", scanRegDate);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_COUNT_SCAN_WAITING, params, pe);
			throw pe;
		} finally {
			closeSession(session);

			if (!scanList.isEmpty()) {
				scanList.clear();
				scanList = null;
			}

			params.clear();
			params = null;
		}

		return count;
	}
	
	/**
	 * Count Crawl Waiting
	 * 
	 * @param scanRegDate
	 *            scan registration date
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @return Number of scan waiting
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public int countCrawlWaiting(Date scanRegDate, int type) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = 0;
		List<Scan> scanList = new ArrayList<Scan>();
		String strSql = null;
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		Session session = null;

		try {
			if (scanRegDate == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
			}

			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			session = openSession();
			strSql = CustomSQLUtil.get(getClass(), COUNT_CRAWL_WAITING);
			sqlQuery = session.createSQLQuery(strSql);
			sqlQuery.setCacheable(false);
			sqlQuery.addEntity("Scan", ScanImpl.class);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(type);
			qPos.add(scanRegDate);
			scanList = sqlQuery.list();

			count = scanList.size();
		} catch (ORMException orme) {
			params.put("scanRegDate", scanRegDate);
			params.put("type", type);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_COUNT_SCAN_WAITING, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("scanRegDate", scanRegDate);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_COUNT_SCAN_WAITING, params, pe);
			throw pe;
		} finally {
			closeSession(session);

			if (!scanList.isEmpty()) {
				scanList.clear();
				scanList = null;
			}

			params.clear();
			params = null;
		}

		return count;
	}

	/**
	 * Get Scan status
	 * 
	 * @param lScanId
	 *            Scan unique ID
	 * @return Status
	 * @throws PortalException
	 */
	public int getScanStatus(long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		String sql = null;
		int iStatus = 0;

		try {

			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			session = openSession();
			sql = CustomSQLUtil.get(getClass(), GET_SCAN_STATUS);

			SQLQuery query = session.createSQLQuery(sql);
			query.setCacheable(false);
			query.addScalar("status", Type.INTEGER);

			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(lScanId);

			iStatus = (int) query.uniqueResult();

		} catch (ORMException e) {
			params.put("sql", sql);
			params.put("lScanId", lScanId);

			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_SCAN_STATUS, params, e);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("sql", sql);
			params.put("lScanId", lScanId);

			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCAN_STATUS, params, pe);
			throw pe;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return iStatus;
	}

	/**
	 * Generate Scan SQL query
	 * 
	 * @param sql
	 *            SQL
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @return SQLQUERY object
	 * @throws ORMException
	 */
	private SQLQuery generateScanSQLQuery(String sql, Map<String, Object> searchedScan) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		try {
			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				Object oStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
				Object oScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
				Object oScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
				Object oImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;
				

				if (!CommonUtil.isObjectNull(oScanId) && !CommonUtil.isStringNullOrEmpty(oScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "scan.scanid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oFileName) && !CommonUtil.isStringNullOrEmpty(oFileName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_2, strStatement + "LOWER(scan.filename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oHashValue) && !CommonUtil.isStringNullOrEmpty(oHashValue.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_3, strStatement + "LOWER(scan.hashvalue) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oScanManager)
						&& !CommonUtil.isStringNullOrEmpty(oScanManager.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_4, strStatement + "LOWER(usr.firstname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "scan.registrationdate >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "scan.registrationdate >= ? AND scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					if (!CommonUtil.isStringNullOrEmpty(oStatus.toString())) {
						sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_6,
								strStatement + "scan.status IN (?,?,?,?,?,?,?,?,?,?,?,?,?)");
					}
				}

				if (!CommonUtil.isObjectNull(oProcess) && !CommonUtil.isStringNullOrEmpty(oProcess.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.process = ?");
				}

				if (!CommonUtil.isObjectNull(oCxScanId) && !CommonUtil.isStringNullOrEmpty(oCxScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_8, strStatement + "scan.cxandroidscanid = ?");
				}
				
				if (!CommonUtil.isObjectNull(oStartUrl) && !CommonUtil.isStringNullOrEmpty(oStartUrl.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_9, strStatement + "LOWER(vexsetting.starturl) LIKE ?");
				}
				
				String strStartTime= null;
				String strEndTime = null;
				
				if (!CommonUtil.isObjectNull(oScanStartTime) && !CommonUtil.isStringNullOrEmpty(oScanStartTime.toString())) {
					strStartTime = oScanStartTime.toString();
				}
				
				if (!CommonUtil.isObjectNull(oScanEndTime) && !CommonUtil.isStringNullOrEmpty(oScanEndTime.toString())) {
					strEndTime = oScanEndTime.toString();
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strStartTime) && CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_10, strStatement + "vexsetting.starttime >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strStartTime) && !CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_11, strStatement + "vexsetting.endtime <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strStartTime) && !CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_10,
							strStatement + "vexsetting.starttime >= ? AND vexsetting.endtime <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oImplementationEnvironment) && !CommonUtil.isStringNullOrEmpty(oImplementationEnvironment.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "vexsetting.implementationenvironment = ?");
				}
			}

			for (int index = 1; index < 13; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (sql.contains(strPlaceHolder)) {
					sql = sql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("scanId", Type.LONG);
			sqlQuery.addScalar("projectName", Type.STRING);
			sqlQuery.addScalar("type_", Type.INTEGER);
			sqlQuery.addScalar("projectStatus", Type.INTEGER);
			sqlQuery.addScalar("fileName", Type.STRING);
			sqlQuery.addScalar("filePath", Type.STRING);
			sqlQuery.addScalar("hashValue", Type.STRING);
			sqlQuery.addScalar("scanManager", Type.STRING);
			sqlQuery.addScalar("scanRegistrationDate", Type.CALENDAR);
			sqlQuery.addScalar("scanStatus", Type.INTEGER);
			sqlQuery.addScalar("process", Type.INTEGER);
			sqlQuery.addScalar("highCount", Type.INTEGER);
			sqlQuery.addScalar("projectEndDate", Type.CALENDAR);
			sqlQuery.addScalar("cxAndroidScanId", Type.STRING);
			sqlQuery.addScalar("reviewFlag", Type.INTEGER);
			sqlQuery.addScalar("failedScanCause", Type.STRING);
			sqlQuery.addScalar("reportCount", Type.INTEGER);
			sqlQuery.addScalar("referenceCount", Type.INTEGER);
			sqlQuery.addScalar("startUrl", Type.STRING);
			sqlQuery.addScalar("authorizedPatrolUrl", Type.STRING);
			sqlQuery.addScalar("starttime", Type.CALENDAR);
			sqlQuery.addScalar("endtime", Type.CALENDAR);
			sqlQuery.addScalar("implementationenvironment", Type.INTEGER);
			sqlQuery.addScalar("setEmailNotification", Type.INTEGER);
			sqlQuery.addScalar("loginUrl", Type.STRING);
			sqlQuery.addScalar("protocol", Type.STRING);
			sqlQuery.addScalar("host", Type.STRING);
			sqlQuery.addScalar("port", Type.INTEGER);
			
		} catch (ORMException orme) {
			params.put("sql", sql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_SCAN_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}

	/**
	 * Generate Entire Scan SQL Query
	 * 
	 * @param sql
	 *            SQL
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @return SQLQuery object
	 * @throws ORMException
	 */
	private SQLQuery generateEntireScanSQLQuery(String sql, Map<String, Object> searchedScan) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;

		try {
			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				Object oStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
				Object oScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
				Object oScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
				Object oImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;

				if (!CommonUtil.isObjectNull(oScanId) && !CommonUtil.isStringNullOrEmpty(oScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "scan.scanid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oProjectName)
						&& !CommonUtil.isStringNullOrEmpty(oProjectName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_2,
							strStatement + "LOWER(project.projectname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oGroupName) && !CommonUtil.isStringNullOrEmpty(oGroupName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_3,
							strStatement + "LOWER(organization.name) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oFileName) && !CommonUtil.isStringNullOrEmpty(oFileName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_4, strStatement + "LOWER(scan.filename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oHashValue) && !CommonUtil.isStringNullOrEmpty(oHashValue.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "LOWER(scan.hashvalue) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oScanManager)
						&& !CommonUtil.isStringNullOrEmpty(oScanManager.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_6, strStatement + "LOWER(usr.firstname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.registrationdate >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7,
							strStatement + "scan.registrationdate >= ? AND scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					if (!CommonUtil.isStringNullOrEmpty(oStatus.toString())) {
						sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_8,
								strStatement + "scan.status IN (?,?,?,?,?,?,?,?,?,?,?,?,?)");
					}
				}

				if (!CommonUtil.isObjectNull(oProcess) && !CommonUtil.isStringNullOrEmpty(oProcess.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_9, strStatement + "scan.process = ?");
				}

				if (!CommonUtil.isObjectNull(oCxScanId) && !CommonUtil.isStringNullOrEmpty(oCxScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_10, strStatement + "scan.cxandroidscanid = ?");
				}
				
				if (!CommonUtil.isObjectNull(oStartUrl) && !CommonUtil.isStringNullOrEmpty(oStartUrl.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_11, strStatement + "LOWER(vexsetting.starturl) LIKE ?");
				}
				
				String strStartTime= null;
				String strEndTime = null;
				
				if (!CommonUtil.isObjectNull(oScanStartTime) && !CommonUtil.isStringNullOrEmpty(oScanStartTime.toString())) {
					strStartTime = oScanStartTime.toString();
				}
				
				if (!CommonUtil.isObjectNull(oScanEndTime) && !CommonUtil.isStringNullOrEmpty(oScanEndTime.toString())) {
					strEndTime = oScanEndTime.toString();
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strStartTime) && CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "vexsetting.starttime >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strStartTime) && !CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_13, strStatement + "vexsetting.endtime <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strStartTime) && !CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_12,
							strStatement + "vexsetting.starttime >= ? AND vexsetting.endtime <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oImplementationEnvironment) && !CommonUtil.isStringNullOrEmpty(oImplementationEnvironment.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_14, strStatement + "vexsetting.implementationenvironment = ?");
				}
			}

			for (int index = 1; index < 15; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (sql.contains(strPlaceHolder)) {
					sql = sql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("scanId", Type.LONG);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("projectName", Type.STRING);
			sqlQuery.addScalar("groupName", Type.STRING);
			sqlQuery.addScalar("fileName", Type.STRING);
			sqlQuery.addScalar("hashValue", Type.STRING);
			sqlQuery.addScalar("scanManager", Type.STRING);
			sqlQuery.addScalar("scanRegistrationDate", Type.CALENDAR);
			sqlQuery.addScalar("scanStatus", Type.INTEGER);
			sqlQuery.addScalar("process", Type.INTEGER);
			sqlQuery.addScalar("highCount", Type.INTEGER);
			sqlQuery.addScalar("reviewFlag", Type.INTEGER);
			sqlQuery.addScalar("cxAndroidScanId", Type.STRING);
			sqlQuery.addScalar("failedScanCause", Type.STRING);
			sqlQuery.addScalar("reportCount", Type.INTEGER);
			sqlQuery.addScalar("referenceCount", Type.INTEGER);
			sqlQuery.addScalar("startUrl", Type.STRING);
			sqlQuery.addScalar("starttime", Type.CALENDAR);
			sqlQuery.addScalar("endtime", Type.CALENDAR);
			sqlQuery.addScalar("implementationenvironment", Type.INTEGER);
		} catch (ORMException orme) {
			params.put("sql", sql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_ENTIRE_SCAN_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}

	/**
	 * Create Scan Data
	 * 
	 * @param scanList
	 *            List of scans
	 * @return List of Scan Data
	 * @throws PortalException
	 */
	private List<Object> getScanResults(List<Object> scanList) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> resultList = new ArrayList<Object>();
		String serializeString = null;
		JSONArray jsonArray = null;
		ScanItem item = null;
		Date date = null;

		try {
			for (Object scan : scanList) {
				serializeString = JSONFactoryUtil.serialize(scan);
				jsonArray = JSONFactoryUtil.createJSONArray(serializeString);
				item = new ScanItem();

				item.setProjectId(jsonArray.getLong(0));
				item.setScanId(jsonArray.getLong(1));
				item.setProjectName(jsonArray.getString(2));
				item.setProjectType(jsonArray.getInt(3));
				item.setProjectStatus(jsonArray.getInt(4));
				item.setFileName(jsonArray.getString(5));
				item.setFilePath(jsonArray.getString(6));
				item.setHashValue(jsonArray.getString(7));
				item.setScanManager(jsonArray.getString(8));
				date = new Date();
				date.setTime(jsonArray.getJSONObject(9).getJSONObject("serializable").getLong("time"));
				item.setScanRegistrationDate(date);
				item.setScanStatus(jsonArray.getInt(10));
				item.setProcess(jsonArray.getInt(11));
				item.setHighCount(jsonArray.getInt(12));
				date = new Date();
				date.setTime(jsonArray.getJSONObject(13).getJSONObject("serializable").getLong("time"));
				item.setProjectEndDate(date);
				item.setCxAndroidScanId(jsonArray.getString(14));
				item.setReviewFlag(jsonArray.getInt(15));
				item.setFailedScanCause(jsonArray.getString(16));
				item.setReportCount(jsonArray.getInt(17));
				item.setReferenceCount(jsonArray.getInt(18));
				item.setScanStartURL(jsonArray.getString(19));
				item.setScanAuthorizedPatrolURL(jsonArray.getString(20));
				
				if(!CommonUtil.isObjectNull(jsonArray.getJSONObject(21))){
					date = new Date();
					date.setTime(jsonArray.getJSONObject(21).getJSONObject("serializable").getLong("time"));
					item.setScanStartTime(date);
				}
				
				if(!CommonUtil.isObjectNull(jsonArray.getJSONObject(22))){
					date = new Date();
					date.setTime(jsonArray.getJSONObject(22).getJSONObject("serializable").getLong("time"));
					item.setScanEndTime(date);
				}
				
				item.setImplementationEnvironment(jsonArray.getInt(23));
				item.setSetEmailNotification(jsonArray.getInt(24));
				item.setLoginUrl(jsonArray.getString(25));
				item.setProtocol(jsonArray.getString(26));
				item.setHost(jsonArray.getString(27));
				item.setPort(jsonArray.getInt(28));

				resultList.add(item);
			}
		} catch (JSONException jsone) {
			params.put("scanList", scanList);
			log.debug(PortalErrors.JSON_EXCEPTION, PortalConstants.METHOD_GET_SCAN_RESULTS, params, jsone);
			throw new PortalException(PortalErrors.JSON_EXCEPTION);
		} finally {
			if (!scanList.isEmpty()) {
				scanList.clear();
				scanList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Create Entire Scan Data
	 * 
	 * @param entireScanList
	 *            Entire scan list
	 * @return Entire scan data
	 * @throws PortalException
	 */
	private List<Object> getEntireScanResults(List<Object> entireScanList) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> resultList = new ArrayList<Object>();
		String serializeString = null;
		JSONArray jsonArray = null;
		ResultItem item = null;
		Date date = new Date();

		try {
			for (Object scan : entireScanList) {
				serializeString = JSONFactoryUtil.serialize(scan);
				jsonArray = JSONFactoryUtil.createJSONArray(serializeString);
				item = new ResultItem();
				item.setScanId(jsonArray.getLong(0));
				item.setProjectId(jsonArray.getLong(1));
				item.setProjectName(jsonArray.getString(2));
				item.setGroupName(jsonArray.getString(3));
				item.setFileName(jsonArray.getString(4));
				item.setHashValue(jsonArray.getString(5));
				item.setScanManager(jsonArray.getString(6));
				date = new Date();
				date.setTime(jsonArray.getJSONObject(7).getJSONObject("serializable").getLong("time"));
				item.setScanRegistrationDate(date);
				item.setStatus(jsonArray.getInt(8));
				item.setProcess(jsonArray.getInt(9));
				item.setHighCount(jsonArray.getInt(10));
				item.setReviewFlag(jsonArray.getInt(11));
				item.setCxAndroidScanId(jsonArray.getString(12));
				item.setFailedScanCause(jsonArray.getString(13));
				item.setReportCount(jsonArray.getInt(14));
				item.setReferenceCount(jsonArray.getInt(15));
				item.setScanStartURL(jsonArray.getString(16));
				
				if(!CommonUtil.isObjectNull(jsonArray.getJSONObject(17))){
					date = new Date();
					date.setTime(jsonArray.getJSONObject(17).getJSONObject("serializable").getLong("time"));
					item.setScanStartTime(date);
				}
				
				if(!CommonUtil.isObjectNull(jsonArray.getJSONObject(18))){
					date = new Date();
					date.setTime(jsonArray.getJSONObject(18).getJSONObject("serializable").getLong("time"));
					item.setScanEndTime(date);
				}
				
				item.setImplementationEnvironment(jsonArray.getInt(19));

				resultList.add(item);
			}
		} catch (JSONException jsone) {
			params.put("entireScanList", entireScanList);
			log.debug(PortalErrors.JSON_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCAN_RESULTS, params, jsone);
			throw new PortalException(PortalErrors.JSON_EXCEPTION);
		} finally {
			if (!entireScanList.isEmpty()) {
				entireScanList.clear();
				entireScanList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Generate entire scan count sql query
	 * 
	 * @param sql
	 *            SQL
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @return SQLQUERY object
	 * @throws ORMException
	 */
	private SQLQuery generateEntireScanCountSQLQuery(String sql, Map<String, Object> searchedScan) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		try {
			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;

				if (!CommonUtil.isObjectNull(oScanId) && !CommonUtil.isStringNullOrEmpty(oScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "scan.scanid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oProjectName)
						&& !CommonUtil.isStringNullOrEmpty(oProjectName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_2,
							strStatement + "LOWER(project.projectname) LIKE ?");
				}

				if (!CommonUtil.isObjectNull(oGroupName) && !CommonUtil.isStringNullOrEmpty(oGroupName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_3,
							strStatement + "LOWER(organization.name) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oFileName) && !CommonUtil.isStringNullOrEmpty(oFileName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_4, strStatement + "LOWER(scan.filename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oHashValue) && !CommonUtil.isStringNullOrEmpty(oHashValue.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "LOWER(scan.hashvalue) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oScanManager)
						&& !CommonUtil.isStringNullOrEmpty(oScanManager.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_6,
							strStatement + "LOWER(scan.scanmanager) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.registrationdate >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7,
							strStatement + "scan.registrationdate >= ? AND scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_8,
								strStatement + "scan.status IN (?,?,?,?,?,?,?,?,?,?,?,?,?)");
					}
				}

				if (!CommonUtil.isObjectNull(oProcess) && !CommonUtil.isStringNullOrEmpty(oProcess.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_9, strStatement + "scan.process = ?");
				}

				if (!CommonUtil.isObjectNull(oCxScanId) && !CommonUtil.isStringNullOrEmpty(oCxScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_10, strStatement + "scan.cxandroidscanid = ?");
				}
			}

			for (int index = 1; index < 11; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (sql.contains(strPlaceHolder)) {
					sql = sql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addEntity("Scan", ScanImpl.class);

		} catch (ORMException orme) {
			params.put("sql", sql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_SCAN_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}

	/**
	 * Generate Scan count sql query
	 * 
	 * @param sql
	 *            SQL
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @return SQLQUERY object
	 * @throws ORMException
	 */
	private SQLQuery generateScanCountSQLQuery(String sql, Map<String, Object> searchedScan) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		try {
			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;

				if (!CommonUtil.isObjectNull(oScanId) && !CommonUtil.isStringNullOrEmpty(oScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "scan.scanid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oFileName) && !CommonUtil.isStringNullOrEmpty(oFileName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_2, strStatement + "LOWER(scan.filename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oHashValue) && !CommonUtil.isStringNullOrEmpty(oHashValue.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_3, strStatement + "LOWER(scan.hashvalue) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oScanManager)
						&& !CommonUtil.isStringNullOrEmpty(oScanManager.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_4,
							strStatement + "LOWER(scan.scanmanager) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "scan.registrationdate >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "scan.registrationdate >= ? AND scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_6,
								strStatement + "scan.status IN (?,?,?,?,?,?,?,?,?,?,?,?,?)");
					}
				}

				if (!CommonUtil.isObjectNull(oProcess) && !CommonUtil.isStringNullOrEmpty(oProcess.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.process = ?");
				}

				if (!CommonUtil.isObjectNull(oCxScanId) && !CommonUtil.isStringNullOrEmpty(oCxScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_8, strStatement + "scan.cxandroidscanid = ?");
				}

				if (!CommonUtil.isObjectNull(oProjectName)
						&& !CommonUtil.isStringNullOrEmpty(oProjectName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_9,
							strStatement + "LOWER(project.projectname) LIKE ?");
				}
			}

			for (int index = 1; index < 10; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (sql.contains(strPlaceHolder)) {
					sql = sql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addEntity("Scan", ScanImpl.class);

		} catch (ORMException orme) {
			params.put("sql", sql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_SCAN_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}

	/**
	 * Generate Sort entire scan sql query
	 * 
	 * @param sql
	 *            SQL
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @param orderByCol
	 *            a column to order
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @return SQLQuery object
	 * @throws ORMException
	 */
	private SQLQuery generateSortEntireScanSQLQuery(String sql, Map<String, Object> searchedScan, String orderByCol,
			String orderByType) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;

		try {
			if (!CommonUtil.isStringNullOrEmpty(orderByCol) && !CommonUtil.isStringNullOrEmpty(orderByType)) {
				if (orderByCol.equals("scanId") || orderByCol.equals("scanRegistrationDate") || orderByCol.equals("process") || orderByCol.equals("cxAndroidScanId") || 
						orderByCol.equals("startTime") || orderByCol.equals("endTime") || orderByCol.equals("implementationEnvironment")) {

				} else if (orderByCol.equals("status")) {
					orderByCol = "scanstatus " + orderByType + PortalConstants.COMMA + "reviewflag ";
				} else if (orderByCol.equals("scanManager")) {
					orderByCol = "LOWER(firstname)";
				} else {
					orderByCol = "LOWER(" + orderByCol + ")";
				}

				if (sql.contains("{orderBy}")) {
					sql = sql.replace("{orderBy}", orderByCol + PortalConstants.STRING_BLANK_SPACE + orderByType);
				}
			} else {
				sql = sql.replace("{orderBy}", "outerresult.scanregistrationdate desc");
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				Object oStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
				Object oScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
				Object oScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
				Object oImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;

				if (!CommonUtil.isObjectNull(oScanId) && !CommonUtil.isStringNullOrEmpty(oScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "scan.scanid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oProjectName)
						&& !CommonUtil.isStringNullOrEmpty(oProjectName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_2,
							strStatement + "LOWER(project.projectname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oGroupName) && !CommonUtil.isStringNullOrEmpty(oGroupName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_3,
							strStatement + "LOWER(organization.name) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oFileName) && !CommonUtil.isStringNullOrEmpty(oFileName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_4, strStatement + "LOWER(scan.filename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oHashValue) && !CommonUtil.isStringNullOrEmpty(oHashValue.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "LOWER(scan.hashvalue) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oScanManager)
						&& !CommonUtil.isStringNullOrEmpty(oScanManager.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_6, strStatement + "LOWER(usr.firstname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.registrationdate >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7,
							strStatement + "scan.registrationdate >= ? AND scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					if (!CommonUtil.isStringNullOrEmpty(oStatus.toString())) {
						sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_8,
								strStatement + "scan.status IN (?,?,?,?,?,?,?,?,?,?,?,?,?)");
					}
				}

				if (!CommonUtil.isObjectNull(oProcess) && !CommonUtil.isStringNullOrEmpty(oProcess.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_9, strStatement + "scan.process = ?");
				}

				if (!CommonUtil.isObjectNull(oCxScanId) && !CommonUtil.isStringNullOrEmpty(oCxScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_10, strStatement + "scan.cxandroidscanid = ?");
				}
				
				if (!CommonUtil.isObjectNull(oStartUrl) && !CommonUtil.isStringNullOrEmpty(oStartUrl.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_11, strStatement + "LOWER(vexsetting.starturl) LIKE ?");
				}
				
				String strStartTime= null;
				String strEndTime = null;
				
				if (!CommonUtil.isObjectNull(oScanStartTime) && !CommonUtil.isStringNullOrEmpty(oScanStartTime.toString())) {
					strStartTime = oScanStartTime.toString();
				}
				
				if (!CommonUtil.isObjectNull(oScanEndTime) && !CommonUtil.isStringNullOrEmpty(oScanEndTime.toString())) {
					strEndTime = oScanEndTime.toString();
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strStartTime) && CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "vexsetting.starttime >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strStartTime) && !CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_13, strStatement + "vexsetting.endtime <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strStartTime) && !CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_12,
							strStatement + "vexsetting.starttime >= ? AND vexsetting.endtime <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oImplementationEnvironment) && !CommonUtil.isStringNullOrEmpty(oImplementationEnvironment.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_14, strStatement + "vexsetting.implementationenvironment = ?");
				}
			}

			for (int index = 1; index < 15; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (sql.contains(strPlaceHolder)) {
					sql = sql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("scanId", Type.LONG);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("projectName", Type.STRING);
			sqlQuery.addScalar("groupName", Type.STRING);
			sqlQuery.addScalar("fileName", Type.STRING);
			sqlQuery.addScalar("hashValue", Type.STRING);
			sqlQuery.addScalar("scanManager", Type.STRING);
			sqlQuery.addScalar("scanRegistrationDate", Type.CALENDAR);
			sqlQuery.addScalar("scanStatus", Type.INTEGER);
			sqlQuery.addScalar("process", Type.INTEGER);
			sqlQuery.addScalar("highCount", Type.INTEGER);
			sqlQuery.addScalar("reviewFlag", Type.INTEGER);
			sqlQuery.addScalar("cxAndroidScanId", Type.STRING);
			sqlQuery.addScalar("failedScanCause", Type.STRING);
			sqlQuery.addScalar("reportCount", Type.INTEGER);
			sqlQuery.addScalar("referenceCount", Type.INTEGER);
			sqlQuery.addScalar("startUrl", Type.STRING);
			sqlQuery.addScalar("starttime", Type.CALENDAR);
			sqlQuery.addScalar("endtime", Type.CALENDAR);
			sqlQuery.addScalar("implementationenvironment", Type.INTEGER);
		} catch (ORMException orme) {
			params.put("sql", sql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_ENTIRE_SCAN_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}

	/**
	 * Generate sort scan sql query
	 * 
	 * @param sql
	 *            SQL
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @param orderByCol
	 *            a column to order
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @return SQLQuery object
	 * @throws ORMException
	 */
	private SQLQuery generateSortScanSQLQuery(String sql, Map<String, Object> searchedScan, String orderByCol,
			String orderByType) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		try {
			if (!CommonUtil.isStringNullOrEmpty(orderByCol) && !CommonUtil.isStringNullOrEmpty(orderByType)) {
				if (orderByCol.equals("scanId") || orderByCol.equals("scanRegistrationDate")|| orderByCol.equals("process") || orderByCol.equals("cxAndroidScanId") || 
						orderByCol.equals("startTime") || orderByCol.equals("endTime") || orderByCol.equals("implementationEnvironment")) {

				} else if (orderByCol.equals("scanStatus")) {
					orderByCol = orderByCol + PortalConstants.STRING_BLANK_SPACE + orderByType + PortalConstants.COMMA
							+ "reviewFlag ";
				} else if (orderByCol.equals("scanManager")) {
					orderByCol = "LOWER(firstname)";
				} else {
					orderByCol = "LOWER(" + orderByCol + ")";
				}

				if (sql.contains("{orderBy}")) {
					sql = sql.replace("{orderBy}", orderByCol + PortalConstants.STRING_BLANK_SPACE + orderByType);
				}
			} else {
				sql = sql.replace("{orderBy}", "outerresult.scanregistrationdate desc");
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
				Object oScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
				Object oScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
				Object oStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
				Object oProcess = searchedScan.get(PortalConstants.PARAM_PROCESS);
				Object oCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
				Object oStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
				Object oScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
				Object oScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
				Object oImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;

				if (!CommonUtil.isObjectNull(oScanId) && !CommonUtil.isStringNullOrEmpty(oScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "scan.scanid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oFileName) && !CommonUtil.isStringNullOrEmpty(oFileName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_2, strStatement + "LOWER(scan.filename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oHashValue) && !CommonUtil.isStringNullOrEmpty(oHashValue.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_3, strStatement + "LOWER(scan.hashvalue) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oScanManager)
						&& !CommonUtil.isStringNullOrEmpty(oScanManager.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_4, strStatement + "LOWER(usr.firstname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				String strScanRegDateLow = null;
				String strScanRegDateHigh = null;

				if (!CommonUtil.isObjectNull(oScanRegDateLow)) {
					strScanRegDateLow = oScanRegDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oScanRegDateHigh)) {
					strScanRegDateHigh = oScanRegDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "scan.registrationdate >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "scan.registrationdate >= ? AND scan.registrationdate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					if (!CommonUtil.isStringNullOrEmpty(oStatus.toString())) {
						sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_6,
								strStatement + "scan.status IN (?,?,?,?,?,?,?,?,?,?,?,?,?)");
					}
				}

				if (!CommonUtil.isObjectNull(oProcess) && !CommonUtil.isStringNullOrEmpty(oProcess.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "scan.process = ?");
				}

				if (!CommonUtil.isObjectNull(oCxScanId) && !CommonUtil.isStringNullOrEmpty(oCxScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_8, strStatement + "scan.cxandroidscanid = ?");
				}
				
				if (!CommonUtil.isObjectNull(oStartUrl) && !CommonUtil.isStringNullOrEmpty(oStartUrl.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_9, strStatement + "LOWER(vexsetting.starturl) LIKE ?");
				}
				
				String strStartTime= null;
				String strEndTime = null;
				
				if (!CommonUtil.isObjectNull(oScanStartTime) && !CommonUtil.isStringNullOrEmpty(oScanStartTime.toString())) {
					strStartTime = oScanStartTime.toString();
				}
				
				if (!CommonUtil.isObjectNull(oScanEndTime) && !CommonUtil.isStringNullOrEmpty(oScanEndTime.toString())) {
					strEndTime = oScanEndTime.toString();
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strStartTime) && CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_10, strStatement + "vexsetting.starttime >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strStartTime) && !CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_11, strStatement + "vexsetting.endtime <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strStartTime) && !CommonUtil.isStringNullOrEmpty(strEndTime)) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_10,
							strStatement + "vexsetting.starttime >= ? AND vexsetting.endtime <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oImplementationEnvironment) && !CommonUtil.isStringNullOrEmpty(oImplementationEnvironment.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "vexsetting.implementationenvironment = ?");
				}
			}

			for (int index = 1; index < 13; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (sql.contains(strPlaceHolder)) {
					sql = sql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("scanId", Type.LONG);
			sqlQuery.addScalar("projectName", Type.STRING);
			sqlQuery.addScalar("type_", Type.INTEGER);
			sqlQuery.addScalar("projectStatus", Type.INTEGER);
			sqlQuery.addScalar("fileName", Type.STRING);
			sqlQuery.addScalar("filePath", Type.STRING);
			sqlQuery.addScalar("hashValue", Type.STRING);
			sqlQuery.addScalar("scanManager", Type.STRING);
			sqlQuery.addScalar("scanRegistrationDate", Type.CALENDAR);
			sqlQuery.addScalar("scanStatus", Type.INTEGER);
			sqlQuery.addScalar("process", Type.INTEGER);
			sqlQuery.addScalar("highCount", Type.INTEGER);
			sqlQuery.addScalar("projectEndDate", Type.CALENDAR);
			sqlQuery.addScalar("cxAndroidScanId", Type.STRING);
			sqlQuery.addScalar("reviewFlag", Type.INTEGER);
			sqlQuery.addScalar("failedScanCause", Type.STRING);
			sqlQuery.addScalar("reportCount", Type.INTEGER);
			sqlQuery.addScalar("referenceCount", Type.INTEGER);
			sqlQuery.addScalar("startUrl", Type.STRING);
			sqlQuery.addScalar("authorizedPatrolUrl", Type.STRING);
			sqlQuery.addScalar("starttime", Type.CALENDAR);
			sqlQuery.addScalar("endtime", Type.CALENDAR);
			sqlQuery.addScalar("implementationenvironment", Type.INTEGER);
			sqlQuery.addScalar("setEmailNotification", Type.INTEGER);
			sqlQuery.addScalar("loginUrl", Type.STRING);
			sqlQuery.addScalar("protocol", Type.STRING);
			sqlQuery.addScalar("host", Type.STRING);
			sqlQuery.addScalar("port", Type.INTEGER);

		} catch (ORMException orme) {
			params.put("sql", sql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_SCAN_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}
}
