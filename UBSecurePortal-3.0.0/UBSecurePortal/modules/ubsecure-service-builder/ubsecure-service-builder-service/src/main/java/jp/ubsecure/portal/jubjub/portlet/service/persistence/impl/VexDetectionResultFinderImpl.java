package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.dao.orm.custom.sql.CustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResultItem;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultFinder;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * VexLoginSettingFinder Implementation
 */
public class VexDetectionResultFinderImpl extends VexDetectionResultFinderBaseImpl implements VexDetectionResultFinder {

	/** UBSPortalDebugger */ 
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexDetectionResultFinderImpl.class);

	/** GET_DETECTION_RESULT */
	private static final String GET_DETECTION_RESULT = VexDetectionResultFinder.class.getName() + ".getDetectionResultReview";
	
	/** GET_DETECTION_RESULT_COUNT */
	private static final String GET_DETECTION_RESULT_COUNT = VexDetectionResultFinder.class.getName() + ".getDetectionResultCount";
	
	/**
	 * Get Login Setting
	 * 
	 * @param scanId
	 *            unique id of the scan
	 * @return List of login setting
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getDetectionResult(long scanId, Map<String, Object> searchedDetectionResult, int start, String orderByCol, String orderByType) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> detectionResultList = null;
		List<Object> resultList = null;
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (scanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}
			//sql = CustomSQLUtil.get(getClass(), GET_DETECTION_RESULT);
			sqlQuery = this.generateVexDetectionResultSQLQuery(CustomSQLUtil.get(getClass(), GET_DETECTION_RESULT), searchedDetectionResult, orderByCol, orderByType, false);

			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(scanId);
			qPos = this.qPosHelper(qPos, searchedDetectionResult, scanId);
			qPos.add(start);
			qPos.add(PortalConstants.PAGINATION_DELTA);
			resultList = (List<Object>) sqlQuery.list();
			
			detectionResultList = this.getDetectionResultList(resultList);
		} catch (ORMException orme) {
			params.put("scanid", scanId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("scanid", scanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, pe);
			throw pe;
		} catch(Exception pe){
			params.put("scanid", scanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, pe);
		} finally {
			params.clear();
			params = null;
		}

		return detectionResultList;
	}
		
	@SuppressWarnings("unchecked")
	public int getDetectionResultsCount(long scanId, Map<String, Object> searchedDetectionResult) throws PortalException{
		int resultCount = 0;
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> resultList = null;
		try{
			//List<Object> detectionResultList = null;
			SQLQuery sqlQuery = null;
			QueryPos qPos = null;

			if (scanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}
			sqlQuery = this.generateVexDetectionResultSQLQuery(CustomSQLUtil.get(getClass(), GET_DETECTION_RESULT_COUNT), searchedDetectionResult, null, null, true);
	
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(scanId);
			qPos = this.qPosHelper(qPos, searchedDetectionResult, scanId);
			resultList = (List<Object>) sqlQuery.list();
			if(null != resultList && resultList.size() > 0){
				resultCount = resultList.size();
			}
		} catch (ORMException orme) {
			params.put("scanid", scanId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("scanid", scanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, pe);
			throw pe;
		} catch(Exception pe){
			params.put("scanid", scanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, pe);
		}  finally {
			params.clear();
			params = null;
		}
		return resultCount;
	}
	
	/**
	 * Generate Detection Result SQL query
	 * 
	 * @param sql
	 *            SQL
	 * @param searchedDetectionResult
	 *            a key-value parameter inputted by user during search
	 * @return SQLQUERY object
	 * @throws ORMException
	 */
	private SQLQuery generateVexDetectionResultSQLQuery(String sql, Map<String, Object> searchedDetectionResult, String orderByCol, String orderByType, boolean isCountResultOnly) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		try {
			if(!isCountResultOnly){
				if (!CommonUtil.isStringNullOrEmpty(orderByCol) && !CommonUtil.isStringNullOrEmpty(orderByType)) {
					if(orderByCol.equals(PortalConstants.PARAM_DETECTION_NUMBER) ){
						orderByCol = "scanresultid";
					}else if (orderByCol.equals(PortalConstants.PARAM_DETECTION_SCAN_ID)
							|| orderByCol.equals(PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX) || orderByCol.equals(PortalConstants.PARAM_DETECTION_RISK_LEVEL)) {
						//do nothing
					} else {
						orderByCol = "LOWER(" + orderByCol + ")";
					}
	
					if (sql.contains("{orderBy}")) {
						sql = sql.replace("{orderBy}", orderByCol + PortalConstants.STRING_BLANK_SPACE + orderByType);
					}
				} else {
					sql = sql.replace("{orderBy}", "vexscandetectionresult.scanresultid asc");
				}
			}
			if (!CommonUtil.isMapNullOrEmpty(searchedDetectionResult)) {
				Object oSearchedNumber = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_NUMBER);
				Object oSearchedDetectionResultId = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_RESULT_ID);
				Object oSearchedScanId = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_SCAN_ID);
				Object oSearchedRiskLevel = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_RISK_LEVEL);
				Object oSearchedCategory = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_CATEGORY);
				Object oSearchedOverview = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_OVERVIEW);
				Object oSearchedFunctionName = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_FUNCTION_NAME);
				Object oSearchedUrl = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_URL);
				Object oSearchedParameterName = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_PARAMETER_NAME);
				Object oSearchedDetectionJudgment = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_JUDGEMENT);
				Object oSearchedReviewComment = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_REVIEW_COMMENT);
				Object oSearchedIsResendInVex = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;
				

				if (!CommonUtil.isObjectNull(oSearchedNumber) && !CommonUtil.isStringNullOrEmpty(oSearchedNumber.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "vexscandetectionresult.scanresultid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oSearchedScanId) && !CommonUtil.isStringNullOrEmpty(oSearchedScanId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_2, strStatement + "vexscandetectionresult.scanid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oSearchedDetectionResultId) && !CommonUtil.isStringNullOrEmpty(oSearchedDetectionResultId.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_3, strStatement + "LOWER(vexscandetectionresult.detectionresultid) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oSearchedRiskLevel) && !CommonUtil.isStringNullOrEmpty(oSearchedRiskLevel.toString())) {

					boolean isAddSql = true;
					String[] strRiskLevelArr = oSearchedRiskLevel.toString().split(PortalConstants.COMMA);
					if (!CommonUtil.isObjectNull(strRiskLevelArr) && strRiskLevelArr.length > PortalConstants.INT_ZERO) {
						String[] iRiskLevelArr  = { "0", "0", "0", "0" };
						int index = PortalConstants.INT_ZERO;
						
						for (String riskLevel : strRiskLevelArr) {
							if (!CommonUtil.isStringNullOrEmpty(riskLevel)) {
								if(Validator.isNumber(riskLevel)){
									iRiskLevelArr[index] = riskLevel;
								}else{
									isAddSql = false;
								}
							}
							index++;
						}
					}
					if(isAddSql){
						sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_4, strStatement + "vexscandetectionresult.risklevel IN (?,?,?,?)");
						strStatement = PortalConstants.STRING_AND_STATEMENT;
					}
				}

				if (!CommonUtil.isObjectNull(oSearchedCategory) && !CommonUtil.isStringNullOrEmpty(oSearchedCategory.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_5, strStatement + "LOWER(vexscandetectionresult.category) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oSearchedOverview) && !CommonUtil.isStringNullOrEmpty(oSearchedOverview.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_6, strStatement + "LOWER(vexscandetectionresult.overview) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oSearchedFunctionName) && !CommonUtil.isStringNullOrEmpty(oSearchedFunctionName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "LOWER(vexscandetectionresult.functionname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oSearchedUrl) && !CommonUtil.isStringNullOrEmpty(oSearchedUrl.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_8, strStatement + "LOWER(vexscandetectionresult.url) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oSearchedParameterName) && !CommonUtil.isStringNullOrEmpty(oSearchedParameterName.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_9, strStatement + "LOWER(vexscandetectionresult.parametername) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oSearchedDetectionJudgment) && !CommonUtil.isStringNullOrEmpty(oSearchedDetectionJudgment.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_10, strStatement + "LOWER(vexscandetectionresult.detectionjudgment) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oSearchedReviewComment) && !CommonUtil.isStringNullOrEmpty(oSearchedReviewComment.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_11, strStatement + "LOWER(vexscandetectionresult.reviewcomment) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oSearchedIsResendInVex) && !CommonUtil.isStringNullOrEmpty(oSearchedIsResendInVex.toString())) {
					sql = sql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "vexscandetectionresult.isresendinvex = ?");
				}
				
			}

			for (int index = 1; index < 13; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index + PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (sql.contains(strPlaceHolder)) {
					sql = sql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}
			
			session = openSession();
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("scanresultid", Type.LONG);
			sqlQuery.addScalar("scanid", Type.LONG);
			sqlQuery.addScalar("detectionresultid", Type.STRING);
			sqlQuery.addScalar("risklevel", Type.INTEGER);
			sqlQuery.addScalar("category", Type.STRING);
			sqlQuery.addScalar("overview", Type.STRING);
			sqlQuery.addScalar("functionname", Type.STRING);
			sqlQuery.addScalar("url", Type.STRING);
			sqlQuery.addScalar("parametername", Type.STRING);
			sqlQuery.addScalar("detectionjudgment", Type.STRING);
			sqlQuery.addScalar("reviewcomment", Type.STRING);
			sqlQuery.addScalar("isresendinvex", Type.INTEGER);
			sqlQuery.addScalar("listofdetectedresult", Type.STRING);
			sqlQuery.addScalar("listofinspectiondatetime", Type.STRING);

		} catch (ORMException orme) {
			params.put("sql", sql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATEVEX__DETECTION_RESULT_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}
	
	private QueryPos qPosHelper(QueryPos qPos, Map<String, Object> searchedDetectionResult, long scanId){
		Map<String, Object> params = new HashMap<String, Object>();
		try{
			if (!CommonUtil.isMapNullOrEmpty(searchedDetectionResult)) {
				Object oSearchedNumber = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_NUMBER);
				Object oSearchedDetectionResultId = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_RESULT_ID);
				Object oSearchedScanId = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_SCAN_ID);
				Object oSearchedRiskLevel = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_RISK_LEVEL);
				Object oSearchedCategory = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_CATEGORY);
				Object oSearchedOverview = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_OVERVIEW);
				Object oSearchedFunctionName = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_FUNCTION_NAME);
				Object oSearchedUrl = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_URL);
				Object oSearchedParameterName = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_PARAMETER_NAME);
				Object oSearchedDetectionJudgment = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_JUDGEMENT);
				Object oSearchedReviewComment = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_REVIEW_COMMENT);
				Object oSearchedIsResendInVex = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX);
				//Object oSearchedIsDetected = searchedDetectionResult.get(PortalConstants.PARAM_DETECTION_IS_DETECTED);
				
				if (!CommonUtil.isObjectNull(oSearchedNumber)) {
					String strSearchedNumber = oSearchedNumber.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedNumber)) {
						qPos.add(Long.parseLong(strSearchedNumber));
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedScanId)) {
					String strSearchedScanId = oSearchedScanId.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedScanId)) {
						qPos.add(Long.parseLong(strSearchedScanId));
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedDetectionResultId)) {
					String strSearchedDetectionResultId= oSearchedDetectionResultId.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedDetectionResultId)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strSearchedDetectionResultId.toLowerCase() + PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				
				
				if (!CommonUtil.isObjectNull(oSearchedRiskLevel)) {
					String strSearchedRiskLevel = oSearchedRiskLevel.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedRiskLevel)) {
						String[] strRiskLevelArr = strSearchedRiskLevel.split(PortalConstants.COMMA);
						if (!CommonUtil.isObjectNull(strRiskLevelArr) && strRiskLevelArr.length > PortalConstants.INT_ZERO) {
							int[] iRiskLevelArr  = { 0, 0, 0, 0 };
							int index = PortalConstants.INT_ZERO;
							
							for (String riskLevel : strRiskLevelArr) {
								if (!CommonUtil.isStringNullOrEmpty(riskLevel)) {
									iRiskLevelArr[index] = Integer.parseInt(riskLevel) ;
								}
								index++;
							}
							qPos.add(iRiskLevelArr);
						}
							
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedCategory)) {
					String strSearchedCategory = oSearchedCategory.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedCategory)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strSearchedCategory.toLowerCase() + PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedOverview)) {
					String strSearchedOverview = oSearchedOverview.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedOverview)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strSearchedOverview.toLowerCase() + PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedFunctionName)) {
					String strSearchedFunctionName = oSearchedFunctionName.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedFunctionName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strSearchedFunctionName.toLowerCase() + PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedUrl)) {
					String strSearchedUrl = oSearchedUrl.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strSearchedUrl.toLowerCase() + PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedParameterName)) {
					String strSearchedParameterName = oSearchedParameterName.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedParameterName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strSearchedParameterName.toLowerCase() + PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedDetectionJudgment)) {
					String strSearchedDetectionJudgment = oSearchedDetectionJudgment.toString();
					
					if (!CommonUtil.isStringNullOrEmpty(strSearchedDetectionJudgment)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strSearchedDetectionJudgment.toLowerCase() + PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedReviewComment)) {
					String strSearchedReviewComment = oSearchedReviewComment.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedReviewComment)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strSearchedReviewComment.toLowerCase() + PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oSearchedIsResendInVex)) {
					String strSearchedIsResendInVex = oSearchedIsResendInVex.toString();
		
					if (!CommonUtil.isStringNullOrEmpty(strSearchedIsResendInVex)) {
						qPos.add(Integer.parseInt(strSearchedIsResendInVex));
					}
				}
			}
		}catch(Exception pe){
			params.put("scanid", scanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_QPOS_HELPER, params, pe);
		}
		
		return qPos;
	}
	
	private List<Object> getDetectionResultList(List<Object> resultList){
		List<Object> detectionResultList = null;
		String serializeString = null;
		JSONArray jsonArray = null;
		VexDetectionResultItem detectionResultItem = null;
		try{
			if (!CommonUtil.isListNullOrEmpty(resultList)) {
				detectionResultList = new ArrayList<Object>();
				for (Object item : resultList) {
					serializeString = JSONFactoryUtil.serialize(item);
					jsonArray = JSONFactoryUtil.createJSONArray(serializeString);
					detectionResultItem = new VexDetectionResultItem();
					detectionResultItem.setScanresultid(jsonArray.getLong(0));
					detectionResultItem.setScanid(jsonArray.getLong(1));
					detectionResultItem.setDetectionresultid(this.getNonNullResult(jsonArray.getString(2)));
					detectionResultItem.setRisklevel(jsonArray.getInt(3));
					detectionResultItem.setCategory(this.getNonNullResult(jsonArray.getString(4)));
					detectionResultItem.setOverview(this.getNonNullResult(jsonArray.getString(5)));
					detectionResultItem.setFunctionname(this.getNonNullResult(jsonArray.getString(6)));
					detectionResultItem.setUrl(this.getNonNullResult(jsonArray.getString(7)));
					detectionResultItem.setParametername(this.getNonNullResult(jsonArray.getString(8)));
					detectionResultItem.setDetectionjudgment(this.getNonNullResult((jsonArray.getString(9))));
					detectionResultItem.setReviewcomment(this.getNonNullResult(jsonArray.getString(10)));
					detectionResultItem.setIsresendinvex(jsonArray.getInt(11));
					detectionResultItem.setListofdetectedresult(this.getNonNullResult(jsonArray.getString(12)));
					detectionResultItem.setListofinspectiondatetime(this.getNonNullResult(jsonArray.getString(13)));
					detectionResultList.add(detectionResultItem);
				}
			}
		}catch(Exception pe){
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, null, pe);
		}
		return detectionResultList;
	}
	
	private String getNonNullResult(String strResult){
		if(!CommonUtil.isStringNullOrEmpty(strResult) && strResult.equals("null")){
			strResult = PortalConstants.STRING_EMPTY;
		}
		return strResult;
	}
	
	private String getStringRiskLevel(String strRiskLevel){
		String iRiskLevel = "0";
		switch(strRiskLevel){
			case "Low":
				iRiskLevel = "1";
				break;
			case "Medium":
				iRiskLevel = "2";
				break;
			case "High":
				iRiskLevel = "3";
				break;
			case "情報":
				iRiskLevel = "4";
				break;
		}
		return iRiskLevel;
	}
	
}