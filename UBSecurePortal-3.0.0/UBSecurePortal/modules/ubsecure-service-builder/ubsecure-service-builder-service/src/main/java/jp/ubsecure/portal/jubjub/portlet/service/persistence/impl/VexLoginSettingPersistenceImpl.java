/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexLoginSettingException;
import jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingImpl;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingModelImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexLoginSettingPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the vex login setting service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexLoginSettingPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.VexLoginSettingUtil
 * @generated
 */
@ProviderType
public class VexLoginSettingPersistenceImpl extends BasePersistenceImpl<VexLoginSetting>
	implements VexLoginSettingPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link VexLoginSettingUtil} to access the vex login setting persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = VexLoginSettingImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingModelImpl.FINDER_CACHE_ENABLED,
			VexLoginSettingImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingModelImpl.FINDER_CACHE_ENABLED,
			VexLoginSettingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID = new FinderPath(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingModelImpl.FINDER_CACHE_ENABLED,
			VexLoginSettingImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByScanId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID =
		new FinderPath(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingModelImpl.FINDER_CACHE_ENABLED,
			VexLoginSettingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByScanId",
			new String[] { Long.class.getName() },
			VexLoginSettingModelImpl.SCANID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SCANID = new FinderPath(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScanId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the vex login settings where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @return the matching vex login settings
	 */
	@Override
	public List<VexLoginSetting> findByScanId(long scanid) {
		return findByScanId(scanid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the vex login settings where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex login settings
	 * @param end the upper bound of the range of vex login settings (not inclusive)
	 * @return the range of matching vex login settings
	 */
	@Override
	public List<VexLoginSetting> findByScanId(long scanid, int start, int end) {
		return findByScanId(scanid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the vex login settings where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex login settings
	 * @param end the upper bound of the range of vex login settings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching vex login settings
	 */
	@Override
	public List<VexLoginSetting> findByScanId(long scanid, int start, int end,
		OrderByComparator<VexLoginSetting> orderByComparator) {
		return findByScanId(scanid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the vex login settings where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex login settings
	 * @param end the upper bound of the range of vex login settings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching vex login settings
	 */
	@Override
	public List<VexLoginSetting> findByScanId(long scanid, int start, int end,
		OrderByComparator<VexLoginSetting> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanid, start, end, orderByComparator };
		}

		List<VexLoginSetting> list = null;

		if (retrieveFromCache) {
			list = (List<VexLoginSetting>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (VexLoginSetting vexLoginSetting : list) {
					if ((scanid != vexLoginSetting.getScanid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_VEXLOGINSETTING_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(VexLoginSettingModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanid);

				if (!pagination) {
					list = (List<VexLoginSetting>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<VexLoginSetting>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first vex login setting in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching vex login setting
	 * @throws NoSuchVexLoginSettingException if a matching vex login setting could not be found
	 */
	@Override
	public VexLoginSetting findByScanId_First(long scanid,
		OrderByComparator<VexLoginSetting> orderByComparator)
		throws NoSuchVexLoginSettingException {
		VexLoginSetting vexLoginSetting = fetchByScanId_First(scanid,
				orderByComparator);

		if (vexLoginSetting != null) {
			return vexLoginSetting;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanid=");
		msg.append(scanid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVexLoginSettingException(msg.toString());
	}

	/**
	 * Returns the first vex login setting in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching vex login setting, or <code>null</code> if a matching vex login setting could not be found
	 */
	@Override
	public VexLoginSetting fetchByScanId_First(long scanid,
		OrderByComparator<VexLoginSetting> orderByComparator) {
		List<VexLoginSetting> list = findByScanId(scanid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last vex login setting in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching vex login setting
	 * @throws NoSuchVexLoginSettingException if a matching vex login setting could not be found
	 */
	@Override
	public VexLoginSetting findByScanId_Last(long scanid,
		OrderByComparator<VexLoginSetting> orderByComparator)
		throws NoSuchVexLoginSettingException {
		VexLoginSetting vexLoginSetting = fetchByScanId_Last(scanid,
				orderByComparator);

		if (vexLoginSetting != null) {
			return vexLoginSetting;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanid=");
		msg.append(scanid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVexLoginSettingException(msg.toString());
	}

	/**
	 * Returns the last vex login setting in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching vex login setting, or <code>null</code> if a matching vex login setting could not be found
	 */
	@Override
	public VexLoginSetting fetchByScanId_Last(long scanid,
		OrderByComparator<VexLoginSetting> orderByComparator) {
		int count = countByScanId(scanid);

		if (count == 0) {
			return null;
		}

		List<VexLoginSetting> list = findByScanId(scanid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the vex login settings where scanid = &#63; from the database.
	 *
	 * @param scanid the scanid
	 */
	@Override
	public void removeByScanId(long scanid) {
		for (VexLoginSetting vexLoginSetting : findByScanId(scanid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(vexLoginSetting);
		}
	}

	/**
	 * Returns the number of vex login settings where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @return the number of matching vex login settings
	 */
	@Override
	public int countByScanId(long scanid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SCANID;

		Object[] finderArgs = new Object[] { scanid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_VEXLOGINSETTING_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanid);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCANID_SCANID_2 = "vexLoginSetting.scanid = ?";

	public VexLoginSettingPersistenceImpl() {
		setModelClass(VexLoginSetting.class);
	}

	/**
	 * Caches the vex login setting in the entity cache if it is enabled.
	 *
	 * @param vexLoginSetting the vex login setting
	 */
	@Override
	public void cacheResult(VexLoginSetting vexLoginSetting) {
		entityCache.putResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingImpl.class, vexLoginSetting.getPrimaryKey(),
			vexLoginSetting);

		vexLoginSetting.resetOriginalValues();
	}

	/**
	 * Caches the vex login settings in the entity cache if it is enabled.
	 *
	 * @param vexLoginSettings the vex login settings
	 */
	@Override
	public void cacheResult(List<VexLoginSetting> vexLoginSettings) {
		for (VexLoginSetting vexLoginSetting : vexLoginSettings) {
			if (entityCache.getResult(
						VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
						VexLoginSettingImpl.class,
						vexLoginSetting.getPrimaryKey()) == null) {
				cacheResult(vexLoginSetting);
			}
			else {
				vexLoginSetting.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all vex login settings.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(VexLoginSettingImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the vex login setting.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(VexLoginSetting vexLoginSetting) {
		entityCache.removeResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingImpl.class, vexLoginSetting.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<VexLoginSetting> vexLoginSettings) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (VexLoginSetting vexLoginSetting : vexLoginSettings) {
			entityCache.removeResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
				VexLoginSettingImpl.class, vexLoginSetting.getPrimaryKey());
		}
	}

	/**
	 * Creates a new vex login setting with the primary key. Does not add the vex login setting to the database.
	 *
	 * @param scanid the primary key for the new vex login setting
	 * @return the new vex login setting
	 */
	@Override
	public VexLoginSetting create(long scanid) {
		VexLoginSetting vexLoginSetting = new VexLoginSettingImpl();

		vexLoginSetting.setNew(true);
		vexLoginSetting.setPrimaryKey(scanid);

		return vexLoginSetting;
	}

	/**
	 * Removes the vex login setting with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param scanid the primary key of the vex login setting
	 * @return the vex login setting that was removed
	 * @throws NoSuchVexLoginSettingException if a vex login setting with the primary key could not be found
	 */
	@Override
	public VexLoginSetting remove(long scanid)
		throws NoSuchVexLoginSettingException {
		return remove((Serializable)scanid);
	}

	/**
	 * Removes the vex login setting with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the vex login setting
	 * @return the vex login setting that was removed
	 * @throws NoSuchVexLoginSettingException if a vex login setting with the primary key could not be found
	 */
	@Override
	public VexLoginSetting remove(Serializable primaryKey)
		throws NoSuchVexLoginSettingException {
		Session session = null;

		try {
			session = openSession();

			VexLoginSetting vexLoginSetting = (VexLoginSetting)session.get(VexLoginSettingImpl.class,
					primaryKey);

			if (vexLoginSetting == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchVexLoginSettingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(vexLoginSetting);
		}
		catch (NoSuchVexLoginSettingException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected VexLoginSetting removeImpl(VexLoginSetting vexLoginSetting) {
		vexLoginSetting = toUnwrappedModel(vexLoginSetting);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(vexLoginSetting)) {
				vexLoginSetting = (VexLoginSetting)session.get(VexLoginSettingImpl.class,
						vexLoginSetting.getPrimaryKeyObj());
			}

			if (vexLoginSetting != null) {
				session.delete(vexLoginSetting);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (vexLoginSetting != null) {
			clearCache(vexLoginSetting);
		}

		return vexLoginSetting;
	}

	@Override
	public VexLoginSetting updateImpl(VexLoginSetting vexLoginSetting) {
		vexLoginSetting = toUnwrappedModel(vexLoginSetting);

		boolean isNew = vexLoginSetting.isNew();

		VexLoginSettingModelImpl vexLoginSettingModelImpl = (VexLoginSettingModelImpl)vexLoginSetting;

		Session session = null;

		try {
			session = openSession();

			if (vexLoginSetting.isNew()) {
				session.save(vexLoginSetting);

				vexLoginSetting.setNew(false);
			}
			else {
				vexLoginSetting = (VexLoginSetting)session.merge(vexLoginSetting);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !VexLoginSettingModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((vexLoginSettingModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						vexLoginSettingModelImpl.getOriginalScanid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);

				args = new Object[] { vexLoginSettingModelImpl.getScanid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);
			}
		}

		entityCache.putResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexLoginSettingImpl.class, vexLoginSetting.getPrimaryKey(),
			vexLoginSetting, false);

		vexLoginSetting.resetOriginalValues();

		return vexLoginSetting;
	}

	protected VexLoginSetting toUnwrappedModel(VexLoginSetting vexLoginSetting) {
		if (vexLoginSetting instanceof VexLoginSettingImpl) {
			return vexLoginSetting;
		}

		VexLoginSettingImpl vexLoginSettingImpl = new VexLoginSettingImpl();

		vexLoginSettingImpl.setNew(vexLoginSetting.isNew());
		vexLoginSettingImpl.setPrimaryKey(vexLoginSetting.getPrimaryKey());

		vexLoginSettingImpl.setScanid(vexLoginSetting.getScanid());
		vexLoginSettingImpl.setLoginurl(vexLoginSetting.getLoginurl());
		vexLoginSettingImpl.setParamname(vexLoginSetting.getParamname());
		vexLoginSettingImpl.setParamvalue(vexLoginSetting.getParamvalue());

		return vexLoginSettingImpl;
	}

	/**
	 * Returns the vex login setting with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the vex login setting
	 * @return the vex login setting
	 * @throws NoSuchVexLoginSettingException if a vex login setting with the primary key could not be found
	 */
	@Override
	public VexLoginSetting findByPrimaryKey(Serializable primaryKey)
		throws NoSuchVexLoginSettingException {
		VexLoginSetting vexLoginSetting = fetchByPrimaryKey(primaryKey);

		if (vexLoginSetting == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchVexLoginSettingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return vexLoginSetting;
	}

	/**
	 * Returns the vex login setting with the primary key or throws a {@link NoSuchVexLoginSettingException} if it could not be found.
	 *
	 * @param scanid the primary key of the vex login setting
	 * @return the vex login setting
	 * @throws NoSuchVexLoginSettingException if a vex login setting with the primary key could not be found
	 */
	@Override
	public VexLoginSetting findByPrimaryKey(long scanid)
		throws NoSuchVexLoginSettingException {
		return findByPrimaryKey((Serializable)scanid);
	}

	/**
	 * Returns the vex login setting with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the vex login setting
	 * @return the vex login setting, or <code>null</code> if a vex login setting with the primary key could not be found
	 */
	@Override
	public VexLoginSetting fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
				VexLoginSettingImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		VexLoginSetting vexLoginSetting = (VexLoginSetting)serializable;

		if (vexLoginSetting == null) {
			Session session = null;

			try {
				session = openSession();

				vexLoginSetting = (VexLoginSetting)session.get(VexLoginSettingImpl.class,
						primaryKey);

				if (vexLoginSetting != null) {
					cacheResult(vexLoginSetting);
				}
				else {
					entityCache.putResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
						VexLoginSettingImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
					VexLoginSettingImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return vexLoginSetting;
	}

	/**
	 * Returns the vex login setting with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param scanid the primary key of the vex login setting
	 * @return the vex login setting, or <code>null</code> if a vex login setting with the primary key could not be found
	 */
	@Override
	public VexLoginSetting fetchByPrimaryKey(long scanid) {
		return fetchByPrimaryKey((Serializable)scanid);
	}

	@Override
	public Map<Serializable, VexLoginSetting> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, VexLoginSetting> map = new HashMap<Serializable, VexLoginSetting>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			VexLoginSetting vexLoginSetting = fetchByPrimaryKey(primaryKey);

			if (vexLoginSetting != null) {
				map.put(primaryKey, vexLoginSetting);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
					VexLoginSettingImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (VexLoginSetting)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_VEXLOGINSETTING_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (VexLoginSetting vexLoginSetting : (List<VexLoginSetting>)q.list()) {
				map.put(vexLoginSetting.getPrimaryKeyObj(), vexLoginSetting);

				cacheResult(vexLoginSetting);

				uncachedPrimaryKeys.remove(vexLoginSetting.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(VexLoginSettingModelImpl.ENTITY_CACHE_ENABLED,
					VexLoginSettingImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the vex login settings.
	 *
	 * @return the vex login settings
	 */
	@Override
	public List<VexLoginSetting> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the vex login settings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex login settings
	 * @param end the upper bound of the range of vex login settings (not inclusive)
	 * @return the range of vex login settings
	 */
	@Override
	public List<VexLoginSetting> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the vex login settings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex login settings
	 * @param end the upper bound of the range of vex login settings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of vex login settings
	 */
	@Override
	public List<VexLoginSetting> findAll(int start, int end,
		OrderByComparator<VexLoginSetting> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the vex login settings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexLoginSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex login settings
	 * @param end the upper bound of the range of vex login settings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of vex login settings
	 */
	@Override
	public List<VexLoginSetting> findAll(int start, int end,
		OrderByComparator<VexLoginSetting> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<VexLoginSetting> list = null;

		if (retrieveFromCache) {
			list = (List<VexLoginSetting>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_VEXLOGINSETTING);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_VEXLOGINSETTING;

				if (pagination) {
					sql = sql.concat(VexLoginSettingModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<VexLoginSetting>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<VexLoginSetting>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the vex login settings from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (VexLoginSetting vexLoginSetting : findAll()) {
			remove(vexLoginSetting);
		}
	}

	/**
	 * Returns the number of vex login settings.
	 *
	 * @return the number of vex login settings
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_VEXLOGINSETTING);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return VexLoginSettingModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the vex login setting persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(VexLoginSettingImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_VEXLOGINSETTING = "SELECT vexLoginSetting FROM VexLoginSetting vexLoginSetting";
	private static final String _SQL_SELECT_VEXLOGINSETTING_WHERE_PKS_IN = "SELECT vexLoginSetting FROM VexLoginSetting vexLoginSetting WHERE scanid IN (";
	private static final String _SQL_SELECT_VEXLOGINSETTING_WHERE = "SELECT vexLoginSetting FROM VexLoginSetting vexLoginSetting WHERE ";
	private static final String _SQL_COUNT_VEXLOGINSETTING = "SELECT COUNT(vexLoginSetting) FROM VexLoginSetting vexLoginSetting";
	private static final String _SQL_COUNT_VEXLOGINSETTING_WHERE = "SELECT COUNT(vexLoginSetting) FROM VexLoginSetting vexLoginSetting WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "vexLoginSetting.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VexLoginSetting exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VexLoginSetting exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(VexLoginSettingPersistenceImpl.class);
}