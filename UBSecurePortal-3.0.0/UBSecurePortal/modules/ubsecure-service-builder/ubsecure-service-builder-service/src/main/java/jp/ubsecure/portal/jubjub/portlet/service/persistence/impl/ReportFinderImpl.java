package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.dao.orm.custom.sql.CustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.ReportItem;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ReportImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ReportFinder;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * ReportFinder Implementation
 */
public class ReportFinderImpl extends ReportFinderBaseImpl implements ReportFinder {

	/** UBSPortalDebugger */
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ReportFinderImpl.class);

	/** GET_REPORTS */
	private static final String GET_REPORTS = ReportFinder.class.getName() + ".getReports";
	
	/** GET_CSV_REPORTS_BY_PROJECT_TYPE_START_DATE_END_DATE */
	private static final String GET_CSV_REPORTS_BY_PROJECT_TYPE_START_DATE_END_DATE = ReportFinder.class.getName() + ".getCSVReportsByProjectTypeStartDateEndDate";
	
	/** GET_REFERENCES */
	private static final String GET_REFERENCES = ReportFinder.class.getName() + ".getReferences";
	
	/** GET_REPORTS_BY_REPORT_TYPE_PROJECT_TYPE_START_DATE_END_DATE */ 
	private static final String GET_REPORTS_BY_REPORT_TYPE_PROJECT_TYPE_START_DATE_END_DATE = ReportFinder.class.getName() + ".getReportsByReportTypeProjectTypeStartDateEndDate";
	
	/** GET_MAX_REPORT_ID */
	private static final String GET_MAX_REPORT_ID = ReportFinder.class.getName() + ".getMaxReportId";

	/**
	 * Get Reports
	 * 
	 * @param scanId
	 *            unique id of the scan
	 * @param reportTypeArr
	 *            arrays of report types
	 * @return List of reports
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Report> getReports(long scanId, int[] reportTypeArr, int status) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Report> reportList = new ArrayList<Report>();
		Session session = null;
		String sql = "";
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (scanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			if (reportTypeArr == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT_TYPE));
				throw new PortalException(PortalErrors.INVALID_REPORT_TYPE);
			}

			session = openSession();
			sql = CustomSQLUtil.get(getClass(), GET_REPORTS);

			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addEntity("UBS_Report", ReportImpl.class);

			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(scanId);
			qPos.add(reportTypeArr);

			reportList = (List<Report>) sqlQuery.list();
		} catch (ORMException orme) {
			params.put("scanId", scanId);
			params.put("reportTypeArr", reportTypeArr);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_REPORTS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("scanId", scanId);
			params.put("reportTypeArr", reportTypeArr);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_REPORTS, params, pe);
			throw pe;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return reportList;
	}

	/**
	 * Get Reports based from project type, start date and end date
	 * 
	 * @param projectType
	 *            a type of project(android, cxsuite, ios)
	 * @param calStartDate
	 *            project start date
	 * @param calEndDate
	 *            project end date
	 * @return List of Reports
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getCSVReportsByProjectTypeStartDateEndDate(int projectType, Calendar calStartDate,
			Calendar calEndDate) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		String sql = PortalConstants.STRING_EMPTY;
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		List<Object> reportList = null;
		String serializeString = null;
		JSONArray jsonArray = null;
		ReportItem item = null;
		Date dteReportDate = null;
		List<Object> resultList = new ArrayList<Object>();
		Map<Long, Integer> reportCountPerScan = new HashMap<Long, Integer>();

		try {
			session = openSession();
			sql = CustomSQLUtil.get(getClass(), GET_CSV_REPORTS_BY_PROJECT_TYPE_START_DATE_END_DATE);
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("reportId", Type.LONG);
			sqlQuery.addScalar("reportDate", Type.CALENDAR);
			sqlQuery.addScalar("reportBucketName", Type.STRING);
			sqlQuery.addScalar("reportType", Type.INTEGER);
			sqlQuery.addScalar("reportName", Type.STRING);
			sqlQuery.addScalar("cxReportId", Type.LONG);
			sqlQuery.addScalar("scanId", Type.LONG);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("caseNumber", Type.STRING);
			sqlQuery.addScalar("attribute", Type.INTEGER);
			sqlQuery.addScalar("process", Type.INTEGER);
			sqlQuery.addScalar("groupName", Type.STRING);
			sqlQuery.addScalar("cxAndroidProjectId", Type.STRING);
			sqlQuery.addScalar("cxAndroidScanId", Type.STRING);

			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(projectType);
			qPos.add(calStartDate.getTime());
			qPos.add(calEndDate.getTime());

			reportList = (List<Object>) sqlQuery.list();

			for (Object report : reportList) {
				serializeString = JSONFactoryUtil.serialize(report);
				jsonArray = JSONFactoryUtil.createJSONArray(serializeString);

				long lScanId = jsonArray.getLong(6);
				Object oReportCount = reportCountPerScan.get(lScanId);
				int iReportCount = 0;

				if (!CommonUtil.isObjectNull(oReportCount)) {
					iReportCount = Integer.valueOf(oReportCount.toString());
				}

				iReportCount++;

				reportCountPerScan.put(lScanId, iReportCount);

				if (iReportCount < 3) {
					item = new ReportItem();
					item.setReportId(jsonArray.getLong(0));
					dteReportDate = new Date();
					dteReportDate.setTime(jsonArray.getJSONObject(1).getJSONObject("serializable").getLong("time"));
					item.setReportDate(dteReportDate);
					item.setBucketName(jsonArray.getString(2));
					item.setReportType(jsonArray.getInt(3));
					item.setReportName(jsonArray.getString(4));
					item.setCxReportId(jsonArray.getLong(5));
					item.setScanId(lScanId);
					item.setProjectId(jsonArray.getLong(7));
					item.setCaseNumber(jsonArray.getString(8));
					item.setAttribute(jsonArray.getInt(9));
					item.setProcess(jsonArray.getInt(10));
					item.setGroupName(jsonArray.getString(11));
					item.setCxAndroidProjectId(jsonArray.getString(12));
					item.setCxAndroidScanId(jsonArray.getString(13));

					resultList.add(item);
				}
			}
		} catch (ORMException orme) {
			params.put("projectType", projectType);
			log.debug(PortalErrors.ORM_EXCEPTION,
					PortalConstants.METHOD_GET_CSV_REPORTS_BY_PROJECTTYPE_STARTDATE_ENDDATE, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("projectType", projectType);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_CSV_REPORTS_BY_PROJECTTYPE_STARTDATE_ENDDATE, params,
					pe);
			throw pe;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
			reportList = null;
		}

		return resultList;
	}

	/**
	 * Download PDF Report
	 * 
	 * @param scanId
	 *            unique id of the selected scan
	 * @param reportType
	 *            type of report
	 *            (PDF_REPORT,XML_REPORT,CSV_META_INFORMATION,CSV_VULNERABILITY,
	 *            ANALYZE_REPORT)
	 * @return Report
	 * @throws PortalException
	 */
	public Report getReferences(long scanId, int reportType, int status) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Report report = null;
		Session session = null;
		String sql = "";
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (scanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			if (reportType == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT_TYPE));
				throw new PortalException(PortalErrors.INVALID_REPORT_TYPE);
			}

			session = openSession();
			sql = CustomSQLUtil.get(getClass(), GET_REFERENCES);

			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addEntity("UBS_Report", ReportImpl.class);

			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(scanId);
			qPos.add(reportType);

			report = (Report) sqlQuery.uniqueResult();
		} catch (ORMException orme) {
			params.put("scanId", scanId);
			params.put("reportType", reportType);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_REFERENCES, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("scanId", scanId);
			params.put("reportType", reportType);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_REFERENCES, params, pe);
			throw pe;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return report;
	}

	/**
	 * Get reports base on report type, project type, start date and end date
	 * 
	 * @param reportType
	 *            type of report
	 *            (PDF_REPORT,XML_REPORT,CSV_META_INFORMATION,CSV_VULNERABILITY,
	 *            ANALYZE_REPORT)
	 * @param projectType
	 *            a type of a project(android, cxsuite, ios)
	 * @param calStartDate
	 *            project start date
	 * @param calEndDate
	 *            project end date
	 * @return List of reports
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getReportsByReportTypeProjectTypeStartDateEndDate(int reportType, int projectType,
			Calendar calStartDate, Calendar calEndDate) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		String sql = PortalConstants.STRING_EMPTY;
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		List<Object> reportList = null;
		String serializeString = null;
		JSONArray jsonArray = null;
		ReportItem item = null;
		Date dteReportDate = null;
		List<Object> resultList = new ArrayList<Object>();

		try {
			session = openSession();
			sql = CustomSQLUtil.get(getClass(), GET_REPORTS_BY_REPORT_TYPE_PROJECT_TYPE_START_DATE_END_DATE);
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("reportId", Type.LONG);
			sqlQuery.addScalar("reportDate", Type.CALENDAR);
			sqlQuery.addScalar("reportBucketName", Type.STRING);
			sqlQuery.addScalar("reportType", Type.INTEGER);
			sqlQuery.addScalar("reportName", Type.STRING);
			sqlQuery.addScalar("cxReportId", Type.LONG);
			sqlQuery.addScalar("scanId", Type.LONG);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("caseNumber", Type.STRING);
			sqlQuery.addScalar("attribute", Type.INTEGER);
			sqlQuery.addScalar("process", Type.INTEGER);
			sqlQuery.addScalar("groupName", Type.STRING);
			sqlQuery.addScalar("reviewFlag", Type.INTEGER);

			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(projectType);
			qPos.add(reportType);
			qPos.add(calStartDate.getTime());
			qPos.add(calEndDate.getTime());

			reportList = (List<Object>) sqlQuery.list();

			for (Object report : reportList) {
				serializeString = JSONFactoryUtil.serialize(report);
				jsonArray = JSONFactoryUtil.createJSONArray(serializeString);

				item = new ReportItem();
				item.setReportId(jsonArray.getLong(0));
				dteReportDate = new Date();
				dteReportDate.setTime(jsonArray.getJSONObject(1).getJSONObject("serializable").getLong("time"));
				item.setReportDate(dteReportDate);
				item.setBucketName(jsonArray.getString(2));
				item.setReportType(jsonArray.getInt(3));
				item.setReportName(jsonArray.getString(4));
				item.setCxReportId(jsonArray.getLong(5));
				item.setScanId(jsonArray.getLong(6));
				item.setProjectId(jsonArray.getLong(7));
				item.setCaseNumber(jsonArray.getString(8));
				item.setAttribute(jsonArray.getInt(9));
				item.setProcess(jsonArray.getInt(10));
				item.setGroupName(jsonArray.getString(11));
				item.setReviewFlag(jsonArray.getInt(12));

				resultList.add(item);
			}
		} catch (ORMException orme) {
			params.put("projectType", projectType);
			params.put("reportType", reportType);
			log.debug(PortalErrors.ORM_EXCEPTION,
					PortalConstants.METHOD_GET_REPORTS_BY_REPORTTYPE_PROJECTTYPE_STARTDATE_ENDDATE, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("projectType", projectType);
			params.put("reportType", reportType);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_REPORTS_BY_REPORTTYPE_PROJECTTYPE_STARTDATE_ENDDATE,
					params, pe);
			throw pe;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
			reportList = null;
		}

		return resultList;
	}

	/**
	 * Get Max Report ID
	 * 
	 * @return reportID
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public long getMaxReportId() throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		String sql = PortalConstants.STRING_EMPTY;
		SQLQuery sqlQuery = null;
		long reportId = PortalConstants.LONG_ZERO;
		List<Long> reportIdList = null;

		try {
			session = openSession();
			sql = CustomSQLUtil.get(getClass(), GET_MAX_REPORT_ID);
			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("reportId", Type.LONG);

			reportIdList = (List<Long>) sqlQuery.list();

			if (!CommonUtil.isListNullOrEmpty(reportIdList)) {
				for (long id : reportIdList) {
					reportId = id;
					break;
				}
			}
		} catch (ORMException orme) {
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_MAX_REPORT_ID, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} finally {
			closeSession(session);
			params.clear();
			params = null;
			reportIdList = null;
		}

		return reportId;
	}
}
