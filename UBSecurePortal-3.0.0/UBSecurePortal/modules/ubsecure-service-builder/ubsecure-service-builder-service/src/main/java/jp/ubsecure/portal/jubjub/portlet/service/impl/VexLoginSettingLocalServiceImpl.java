/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;

import aQute.bnd.annotation.ProviderType;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexLoginSettingImpl;
import jp.ubsecure.portal.jubjub.portlet.service.base.VexLoginSettingLocalServiceBaseImpl;

/**
 * The implementation of the vex login setting local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link jp.ubsecure.portal.jubjub.portlet.service.VexLoginSettingLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexLoginSettingLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.VexLoginSettingLocalServiceUtil
 */
@ProviderType
public class VexLoginSettingLocalServiceImpl extends VexLoginSettingLocalServiceBaseImpl {
	
	/** UBSPortalDebugger */
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexLoginSettingLocalServiceImpl.class);

	@Override
	public VexLoginSetting createVexLoginSettingObj() {
		return new VexLoginSettingImpl();
	}

	@Override
	public List<Object> getLoginSettings(long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> loginSettingList = null;

		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}
			loginSettingList = vexLoginSettingFinder.getLoginSettings(lScanId);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_LOGIN_SETTINGS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return loginSettingList;
	}
	
}