/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.Scan;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Scan in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Scan
 * @generated
 */
@ProviderType
public class ScanCacheModel implements CacheModel<Scan>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ScanCacheModel)) {
			return false;
		}

		ScanCacheModel scanCacheModel = (ScanCacheModel)obj;

		if (scanId == scanCacheModel.scanId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, scanId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{scanId=");
		sb.append(scanId);
		sb.append(", registrationDate=");
		sb.append(registrationDate);
		sb.append(", status=");
		sb.append(status);
		sb.append(", process=");
		sb.append(process);
		sb.append(", cxAndroidScanId=");
		sb.append(cxAndroidScanId);
		sb.append(", scanManager=");
		sb.append(scanManager);
		sb.append(", infoCount=");
		sb.append(infoCount);
		sb.append(", highCount=");
		sb.append(highCount);
		sb.append(", mediumCount=");
		sb.append(mediumCount);
		sb.append(", lowCount=");
		sb.append(lowCount);
		sb.append(", fileName=");
		sb.append(fileName);
		sb.append(", hashValue=");
		sb.append(hashValue);
		sb.append(", filePath=");
		sb.append(filePath);
		sb.append(", cxRunId=");
		sb.append(cxRunId);
		sb.append(", projectId=");
		sb.append(projectId);
		sb.append(", reviewFlag=");
		sb.append(reviewFlag);
		sb.append(", failedScanCause=");
		sb.append(failedScanCause);
		sb.append(", isDeletedInCx=");
		sb.append(isDeletedInCx);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", cxAndroidProjectId=");
		sb.append(cxAndroidProjectId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Scan toEntityModel() {
		ScanImpl scanImpl = new ScanImpl();

		scanImpl.setScanId(scanId);

		if (registrationDate == Long.MIN_VALUE) {
			scanImpl.setRegistrationDate(null);
		}
		else {
			scanImpl.setRegistrationDate(new Date(registrationDate));
		}

		scanImpl.setStatus(status);
		scanImpl.setProcess(process);

		if (cxAndroidScanId == null) {
			scanImpl.setCxAndroidScanId(StringPool.BLANK);
		}
		else {
			scanImpl.setCxAndroidScanId(cxAndroidScanId);
		}

		if (scanManager == null) {
			scanImpl.setScanManager(StringPool.BLANK);
		}
		else {
			scanImpl.setScanManager(scanManager);
		}

		scanImpl.setInfoCount(infoCount);
		scanImpl.setHighCount(highCount);
		scanImpl.setMediumCount(mediumCount);
		scanImpl.setLowCount(lowCount);

		if (fileName == null) {
			scanImpl.setFileName(StringPool.BLANK);
		}
		else {
			scanImpl.setFileName(fileName);
		}

		if (hashValue == null) {
			scanImpl.setHashValue(StringPool.BLANK);
		}
		else {
			scanImpl.setHashValue(hashValue);
		}

		if (filePath == null) {
			scanImpl.setFilePath(StringPool.BLANK);
		}
		else {
			scanImpl.setFilePath(filePath);
		}

		if (cxRunId == null) {
			scanImpl.setCxRunId(StringPool.BLANK);
		}
		else {
			scanImpl.setCxRunId(cxRunId);
		}

		scanImpl.setProjectId(projectId);
		scanImpl.setReviewFlag(reviewFlag);

		if (failedScanCause == null) {
			scanImpl.setFailedScanCause(StringPool.BLANK);
		}
		else {
			scanImpl.setFailedScanCause(failedScanCause);
		}

		if (isDeletedInCx == null) {
			scanImpl.setIsDeletedInCx(StringPool.BLANK);
		}
		else {
			scanImpl.setIsDeletedInCx(isDeletedInCx);
		}

		if (modifiedDate == Long.MIN_VALUE) {
			scanImpl.setModifiedDate(null);
		}
		else {
			scanImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (cxAndroidProjectId == null) {
			scanImpl.setCxAndroidProjectId(StringPool.BLANK);
		}
		else {
			scanImpl.setCxAndroidProjectId(cxAndroidProjectId);
		}

		scanImpl.resetOriginalValues();

		return scanImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		scanId = objectInput.readLong();
		registrationDate = objectInput.readLong();

		status = objectInput.readInt();

		process = objectInput.readInt();
		cxAndroidScanId = objectInput.readUTF();
		scanManager = objectInput.readUTF();

		infoCount = objectInput.readLong();

		highCount = objectInput.readLong();

		mediumCount = objectInput.readLong();

		lowCount = objectInput.readLong();
		fileName = objectInput.readUTF();
		hashValue = objectInput.readUTF();
		filePath = objectInput.readUTF();
		cxRunId = objectInput.readUTF();

		projectId = objectInput.readLong();

		reviewFlag = objectInput.readInt();
		failedScanCause = objectInput.readUTF();
		isDeletedInCx = objectInput.readUTF();
		modifiedDate = objectInput.readLong();
		cxAndroidProjectId = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(scanId);
		objectOutput.writeLong(registrationDate);

		objectOutput.writeInt(status);

		objectOutput.writeInt(process);

		if (cxAndroidScanId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cxAndroidScanId);
		}

		if (scanManager == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(scanManager);
		}

		objectOutput.writeLong(infoCount);

		objectOutput.writeLong(highCount);

		objectOutput.writeLong(mediumCount);

		objectOutput.writeLong(lowCount);

		if (fileName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fileName);
		}

		if (hashValue == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(hashValue);
		}

		if (filePath == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(filePath);
		}

		if (cxRunId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cxRunId);
		}

		objectOutput.writeLong(projectId);

		objectOutput.writeInt(reviewFlag);

		if (failedScanCause == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(failedScanCause);
		}

		if (isDeletedInCx == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isDeletedInCx);
		}

		objectOutput.writeLong(modifiedDate);

		if (cxAndroidProjectId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cxAndroidProjectId);
		}
	}

	public long scanId;
	public long registrationDate;
	public int status;
	public int process;
	public String cxAndroidScanId;
	public String scanManager;
	public long infoCount;
	public long highCount;
	public long mediumCount;
	public long lowCount;
	public String fileName;
	public String hashValue;
	public String filePath;
	public String cxRunId;
	public long projectId;
	public int reviewFlag;
	public String failedScanCause;
	public String isDeletedInCx;
	public long modifiedDate;
	public String cxAndroidProjectId;
}