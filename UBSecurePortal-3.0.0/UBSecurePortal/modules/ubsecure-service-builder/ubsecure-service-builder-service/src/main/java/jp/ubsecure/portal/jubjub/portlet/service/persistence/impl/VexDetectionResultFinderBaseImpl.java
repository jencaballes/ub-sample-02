/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;

import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPersistence;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class VexDetectionResultFinderBaseImpl extends BasePersistenceImpl<VexDetectionResult> {
	/**
	 * Returns the vex detection result persistence.
	 *
	 * @return the vex detection result persistence
	 */
	public VexDetectionResultPersistence getVexDetectionResultPersistence() {
		return vexDetectionResultPersistence;
	}

	/**
	 * Sets the vex detection result persistence.
	 *
	 * @param vexDetectionResultPersistence the vex detection result persistence
	 */
	public void setVexDetectionResultPersistence(
		VexDetectionResultPersistence vexDetectionResultPersistence) {
		this.vexDetectionResultPersistence = vexDetectionResultPersistence;
	}

	@BeanReference(type = VexDetectionResultPersistence.class)
	protected VexDetectionResultPersistence vexDetectionResultPersistence;
}