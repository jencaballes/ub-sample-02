/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionList;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;

import aQute.bnd.annotation.ProviderType;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ReportType;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ReportImpl;
import jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.base.ReportLocalServiceBaseImpl;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser;

/**
 * The implementation of the report local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link jp.ubsecure.portal.jubjub.portlet.service.ReportLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author ASI
 * @see ReportLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil
 */
@ProviderType
public class ReportLocalServiceImpl extends ReportLocalServiceBaseImpl {

	/** UBSPortalDebugger */
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ReportLocalServiceImpl.class);

	/**
	 * Get list of reports
	 * 
	 * @param lScanId
	 * @param reportTypeArr
	 *            a type of reports(PDF_REPORT(1), XML_REPORT(2),
	 *            CSV_META_INFORMATION(3), CSV_VULNERABILITY(4),
	 *            ANALYZE_REPORT(5);)
	 * @param type
	 *            type of report project(android, cxsuite, ios)
	 * @return List of Reports
	 * @throws PortalException
	 *             an exception thrown
	 */
	public List<Report> getReports(long lScanId, int[] reportTypeArr, int status, int type) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Report> reportList = new ArrayList<Report>();

		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			if (reportTypeArr.length == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT_ID));
				throw new PortalException(PortalErrors.INVALID_REPORT_ID);
			}

			if (type == PortalConstants.INT_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (status == PortalConstants.INT_ZERO && type == ProjectType.CX_SUITE.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_STATUS));
				throw new PortalException(PortalErrors.INVALID_SCAN_STATUS);
			}

			reportList = reportFinder.getReports(lScanId, reportTypeArr, status);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);
			params.put("reportTypeArr", reportTypeArr);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_REPORTS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return reportList;
	}

	/**
	 * Get CSV Reports
	 * 
	 * @param projectType
	 *            - a type of project(android, cxsuite, ios)
	 * @param calStartDate
	 *            - project start date
	 * @param calEndDate
	 *            - project end date
	 * @return reportList list of project reports
	 * @throws PortalException
	 *             an exception thrown
	 */
	public List<Object> getCSVReportsByProjectTypeStartDateEndDate(int projectType, Calendar calStartDate,
			Calendar calEndDate) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> reportList = null;

		try {
			if (CommonUtil.isOutOfRange(projectType, ProjectType.CX_SUITE.getInteger(), ProjectType.IOS.getInteger())) {
				params.put("projectType", projectType);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (CommonUtil.isObjectNull(calStartDate)) {
				params.put("calStartDate", calStartDate);
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new PortalException(PortalErrors.INVALID_REGISTRATION_DATE);
			}

			if (CommonUtil.isObjectNull(calEndDate)) {
				params.put("calEndDate", calEndDate);
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new PortalException(PortalErrors.INVALID_REGISTRATION_DATE);
			}

			reportList = reportFinder.getCSVReportsByProjectTypeStartDateEndDate(projectType, calStartDate, calEndDate);
		} catch (PortalException e) {
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_CSV_REPORTS_BY_PROJECTTYPE_STARTDATE_ENDDATE, params,
					e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}

		return reportList;
	}

	/**
	 * Get Reports base on scan id
	 * 
	 * @param lScanId
	 *            scan id of the scan reports
	 * @return List of reports
	 * @throws PortalException
	 */
	public List<Report> getReportByScanId(long lScanId) throws PortalException {
		List<Report> resultList = null;

		try {
			reportPersistence.clearCache();
			resultList = reportPersistence.findByScanId(lScanId);
		} catch (SystemException e) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_REPORT_BY_SCAN_ID, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		}

		return resultList;
	}

	/**
	 * Get Reports
	 * 
	 * @param lScanId
	 *            scan id of the scan reports
	 * @return List of reports
	 * @throws PortalException
	 */
	public List<Report> getReports(long lScanId) throws PortalException {
		List<Report> lstReport = new ArrayList<Report>();
		List<Object> lstResult = null;
		Report report = null;
		String serializeString = null;
		JSONArray jsonArray = null;
		DynamicQuery dynamicQuery = null;
		Map<String, Object> params = new HashMap<String, Object>();

		try {

			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			dynamicQuery = DynamicQueryFactoryUtil.forClass(Report.class, PortalClassLoaderUtil.getClassLoader());

			ProjectionList projectionList = ProjectionFactoryUtil.projectionList();
			projectionList.add(ProjectionFactoryUtil.property("reportId"));
			projectionList.add(ProjectionFactoryUtil.property("reportType"));
			projectionList.add(ProjectionFactoryUtil.property("cxReportId"));

			dynamicQuery.setProjection(projectionList);
			dynamicQuery.add(PropertyFactoryUtil.forName("scanId").eq(lScanId));

			lstResult = ReportLocalServiceUtil.dynamicQuery(dynamicQuery);

			if (lstResult != null && lstResult.size() > PortalConstants.REPORT_COUNT_LIMIT) {
				params.put("lstResult size", lstResult.size());
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT));
				throw new PortalException(PortalErrors.INVALID_REPORT);
			}

			if (!CommonUtil.isListNullOrEmpty(lstResult)) {
				for (Object scan : lstResult) {
					serializeString = JSONFactoryUtil.serialize(scan);
					jsonArray = JSONFactoryUtil.createJSONArray(serializeString);

					report = new ReportImpl();
					report.setReportId(jsonArray.getLong(0));
					report.setReportType(jsonArray.getInt(1));
					report.setCxReportId(jsonArray.getLong(2));

					lstReport.add(report);
				}
			}

		} catch (SystemException e) {
			params.put("dynamicQuery", dynamicQuery.toString());
			params.put("lScanId", lScanId);

			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_REPORTS, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION, e);
		} catch (JSONException e) {
			params.put("serializeString", serializeString);

			log.debug(PortalErrors.JSON_EXCEPTION, PortalConstants.METHOD_GET_REPORTS, params, e);
			throw new PortalException(PortalErrors.JSON_EXCEPTION, e);
		} catch (PortalException e) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_REPORTS, params, e);
			throw new PortalException(PortalErrors.PORTAL_EXCEPTION, e);
		} finally {
			lstResult = null;
			serializeString = null;
			jsonArray = null;
			dynamicQuery = null;
			params.clear();
			params = null;
		}

		return lstReport;
	}

	/**
	 * Get references
	 * 
	 * @param lScanId
	 *            id of the scan
	 * @param reportType
	 *            type of report
	 *            (PDF_REPORT,XML_REPORT,CSV_META_INFORMATION,CSV_VULNERABILITY,
	 *            ANALYZE_REPORT)
	 * @return Report
	 * @throws PortalException
	 */
	public Report getReferences(long lScanId, int reportType, int status) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Report report = null;

		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			if (reportType == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT_TYPE));
				throw new PortalException(PortalErrors.INVALID_REPORT_TYPE);
			}

			if (status == PortalConstants.INT_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_STATUS));
				throw new PortalException(PortalErrors.INVALID_SCAN_STATUS);
			}

			report = reportFinder.getReferences(lScanId, reportType, status);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);
			params.put("reportType", reportType);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_REFERENCES, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return report;
	}

	/**
	 * Get reports based on report type, project type, start date and end date
	 * 
	 * @param reportType
	 *            type of report
	 *            (PDF_REPORT,XML_REPORT,CSV_META_INFORMATION,CSV_VULNERABILITY,
	 *            ANALYZE_REPORT)
	 * @param projectType
	 *            a type of a project(android, cxsuite, ios)
	 * @param calStartDate
	 *            project start date
	 * @param calEndDate
	 *            project end date
	 * @return List of reports
	 * @throws PortalException
	 */
	public List<Object> getReportsByReportTypeProjectTypeStartDateEndDate(int reportType, int projectType,
			Calendar calStartDate, Calendar calEndDate) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> reportList = null;

		try {
			if (CommonUtil.isOutOfRange(reportType, ReportType.PDF_REPORT.getInteger(),
					ReportType.ANALYZE_REPORT.getInteger())) {
				params.put("reportType", reportType);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT_TYPE));
				throw new PortalException(PortalErrors.INVALID_REPORT_TYPE);
			}

			if (projectType==ProjectType.IOS.getInteger() && CommonUtil.isOutOfRange(projectType, ProjectType.CX_SUITE.getInteger(), ProjectType.IOS.getInteger())) {
				params.put("projectType", projectType);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}
			
			if (projectType==ProjectType.VEX.getInteger() && CommonUtil.isOutOfRange(projectType, ProjectType.CX_SUITE.getInteger(), ProjectType.VEX.getInteger())) {
				params.put("projectType", projectType);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (CommonUtil.isObjectNull(calStartDate)) {
				params.put("calStartDate", calStartDate);
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new PortalException(PortalErrors.INVALID_REGISTRATION_DATE);
			}

			if (CommonUtil.isObjectNull(calEndDate)) {
				params.put("calEndDate", calEndDate);
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new PortalException(PortalErrors.INVALID_REGISTRATION_DATE);
			}

			reportList = reportFinder.getReportsByReportTypeProjectTypeStartDateEndDate(reportType, projectType,
					calStartDate, calEndDate);
		} catch (PortalException e) {
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_REPORTS_BY_REPORTTYPE_PROJECTTYPE_STARTDATE_ENDDATE,
					params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}

		return reportList;
	}

	/**
	 * Register a report
	 * 
	 * @param lScanId
	 *            id of the scan
	 * @param iReportType
	 *            type of report
	 *            (PDF_REPORT,XML_REPORT,CSV_META_INFORMATION,CSV_VULNERABILITY,
	 *            ANALYZE_REPORT)
	 * @return Report
	 * @throws PortalException
	 */
	public Report addReport(long lScanId, int iReportType) throws PortalException {
		Report report = null;
		Map<String, Object> params = new HashMap<String, Object>();

		try {
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}
			if (CommonUtil.isOutOfRange(iReportType, 1, PortalConstants.REPORT_TYPE_LIMIT)) {
				params.put("iReportType", iReportType);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT_TYPE));
				throw new PortalException(PortalErrors.INVALID_REPORT_TYPE);
			}

			reportPersistence.clearCache();
			long lReportId = getMaxReportId() + 1;
			report = reportPersistence.create(lReportId);
			report.setScanId(lScanId);
			report.setReportType(iReportType);
			report.setReportBucketName(ConfigurationFileParser.getConfig(PortalConstants.YCLOUD_BUCKETNAME));
			report.setReportDate(new Date());
			super.addReport(report);

		} catch (PortalException e) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_REPORT, params, e);
			throw e;
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_REPORT, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION, e);
		} catch (UBSPortalException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_REPORT, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}

		return report;
	}

	/**
	 * Get Max Report ID
	 * 
	 * @return reportID
	 * @throws PortalException
	 */
	public long getMaxReportId() throws PortalException {
		long reportId = PortalConstants.LONG_ZERO;

		try {
			reportPersistence.clearCache();
			reportId = reportFinder.getMaxReportId();
		} catch (PortalException e) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_MAX_REPORT_ID, null, e);
			throw e;
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_MAX_REPORT_ID, null, e);
			throw new PortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		}

		return reportId;
	}
}