/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing VexScanSetting in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see VexScanSetting
 * @generated
 */
@ProviderType
public class VexScanSettingCacheModel implements CacheModel<VexScanSetting>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexScanSettingCacheModel)) {
			return false;
		}

		VexScanSettingCacheModel vexScanSettingCacheModel = (VexScanSettingCacheModel)obj;

		if (scanid == vexScanSettingCacheModel.scanid) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, scanid);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(71);

		sb.append("{scanid=");
		sb.append(scanid);
		sb.append(", starturl=");
		sb.append(starturl);
		sb.append(", authorizedpatrolurl=");
		sb.append(authorizedpatrolurl);
		sb.append(", unauthorizedpatrolurl=");
		sb.append(unauthorizedpatrolurl);
		sb.append(", starttime=");
		sb.append(starttime);
		sb.append(", endtime=");
		sb.append(endtime);
		sb.append(", setemailnotification=");
		sb.append(setemailnotification);
		sb.append(", selectedwebsignature=");
		sb.append(selectedwebsignature);
		sb.append(", getserverfiles=");
		sb.append(getserverfiles);
		sb.append(", getserversettings=");
		sb.append(getserversettings);
		sb.append(", selectedserverfilessignature=");
		sb.append(selectedserverfilessignature);
		sb.append(", selectedserversettingssignature=");
		sb.append(selectedserversettingssignature);
		sb.append(", maxnumdetectionlink=");
		sb.append(maxnumdetectionlink);
		sb.append(", maxnumdetectionlinkperpage=");
		sb.append(maxnumdetectionlinkperpage);
		sb.append(", detectionlinkdepthlimit=");
		sb.append(detectionlinkdepthlimit);
		sb.append(", waittime=");
		sb.append(waittime);
		sb.append(", numofthread=");
		sb.append(numofthread);
		sb.append(", timeouttime=");
		sb.append(timeouttime);
		sb.append(", setautopostform=");
		sb.append(setautopostform);
		sb.append(", setpostmethod=");
		sb.append(setpostmethod);
		sb.append(", requestheader=");
		sb.append(requestheader);
		sb.append(", loginstatusdetection=");
		sb.append(loginstatusdetection);
		sb.append(", setnotsendpassword=");
		sb.append(setnotsendpassword);
		sb.append(", paramnamesaspassword=");
		sb.append(paramnamesaspassword);
		sb.append(", numofretryaterror=");
		sb.append(numofretryaterror);
		sb.append(", sessionerrordetection=");
		sb.append(sessionerrordetection);
		sb.append(", screentransitionerrordetection=");
		sb.append(screentransitionerrordetection);
		sb.append(", implementationenvironment=");
		sb.append(implementationenvironment);
		sb.append(", setcrawlingonly=");
		sb.append(setcrawlingonly);
		sb.append(", excludepathinforegex=");
		sb.append(excludepathinforegex);
		sb.append(", excludeparameterdeletenameregex=");
		sb.append(excludeparameterdeletenameregex);
		sb.append(", excludeparameterdeleteparamregex=");
		sb.append(excludeparameterdeleteparamregex);
		sb.append(", extracturlregex=");
		sb.append(extracturlregex);
		sb.append(", notparseextension=");
		sb.append(notparseextension);
		sb.append(", notparsefilenameregex=");
		sb.append(notparsefilenameregex);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public VexScanSetting toEntityModel() {
		VexScanSettingImpl vexScanSettingImpl = new VexScanSettingImpl();

		vexScanSettingImpl.setScanid(scanid);

		if (starturl == null) {
			vexScanSettingImpl.setStarturl(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setStarturl(starturl);
		}

		if (authorizedpatrolurl == null) {
			vexScanSettingImpl.setAuthorizedpatrolurl(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setAuthorizedpatrolurl(authorizedpatrolurl);
		}

		if (unauthorizedpatrolurl == null) {
			vexScanSettingImpl.setUnauthorizedpatrolurl(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setUnauthorizedpatrolurl(unauthorizedpatrolurl);
		}

		if (starttime == Long.MIN_VALUE) {
			vexScanSettingImpl.setStarttime(null);
		}
		else {
			vexScanSettingImpl.setStarttime(new Date(starttime));
		}

		if (endtime == Long.MIN_VALUE) {
			vexScanSettingImpl.setEndtime(null);
		}
		else {
			vexScanSettingImpl.setEndtime(new Date(endtime));
		}

		vexScanSettingImpl.setSetemailnotification(setemailnotification);

		if (selectedwebsignature == null) {
			vexScanSettingImpl.setSelectedwebsignature(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setSelectedwebsignature(selectedwebsignature);
		}

		vexScanSettingImpl.setGetserverfiles(getserverfiles);
		vexScanSettingImpl.setGetserversettings(getserversettings);

		if (selectedserverfilessignature == null) {
			vexScanSettingImpl.setSelectedserverfilessignature(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setSelectedserverfilessignature(selectedserverfilessignature);
		}

		if (selectedserversettingssignature == null) {
			vexScanSettingImpl.setSelectedserversettingssignature(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setSelectedserversettingssignature(selectedserversettingssignature);
		}

		vexScanSettingImpl.setMaxnumdetectionlink(maxnumdetectionlink);
		vexScanSettingImpl.setMaxnumdetectionlinkperpage(maxnumdetectionlinkperpage);
		vexScanSettingImpl.setDetectionlinkdepthlimit(detectionlinkdepthlimit);
		vexScanSettingImpl.setWaittime(waittime);
		vexScanSettingImpl.setNumofthread(numofthread);
		vexScanSettingImpl.setTimeouttime(timeouttime);
		vexScanSettingImpl.setSetautopostform(setautopostform);
		vexScanSettingImpl.setSetpostmethod(setpostmethod);

		if (requestheader == null) {
			vexScanSettingImpl.setRequestheader(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setRequestheader(requestheader);
		}

		if (loginstatusdetection == null) {
			vexScanSettingImpl.setLoginstatusdetection(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setLoginstatusdetection(loginstatusdetection);
		}

		vexScanSettingImpl.setSetnotsendpassword(setnotsendpassword);

		if (paramnamesaspassword == null) {
			vexScanSettingImpl.setParamnamesaspassword(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setParamnamesaspassword(paramnamesaspassword);
		}

		vexScanSettingImpl.setNumofretryaterror(numofretryaterror);

		if (sessionerrordetection == null) {
			vexScanSettingImpl.setSessionerrordetection(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setSessionerrordetection(sessionerrordetection);
		}

		if (screentransitionerrordetection == null) {
			vexScanSettingImpl.setScreentransitionerrordetection(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setScreentransitionerrordetection(screentransitionerrordetection);
		}

		vexScanSettingImpl.setImplementationenvironment(implementationenvironment);
		vexScanSettingImpl.setSetcrawlingonly(setcrawlingonly);

		if (excludepathinforegex == null) {
			vexScanSettingImpl.setExcludepathinforegex(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setExcludepathinforegex(excludepathinforegex);
		}

		if (excludeparameterdeletenameregex == null) {
			vexScanSettingImpl.setExcludeparameterdeletenameregex(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setExcludeparameterdeletenameregex(excludeparameterdeletenameregex);
		}

		if (excludeparameterdeleteparamregex == null) {
			vexScanSettingImpl.setExcludeparameterdeleteparamregex(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setExcludeparameterdeleteparamregex(excludeparameterdeleteparamregex);
		}

		if (extracturlregex == null) {
			vexScanSettingImpl.setExtracturlregex(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setExtracturlregex(extracturlregex);
		}

		if (notparseextension == null) {
			vexScanSettingImpl.setNotparseextension(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setNotparseextension(notparseextension);
		}

		if (notparsefilenameregex == null) {
			vexScanSettingImpl.setNotparsefilenameregex(StringPool.BLANK);
		}
		else {
			vexScanSettingImpl.setNotparsefilenameregex(notparsefilenameregex);
		}

		vexScanSettingImpl.resetOriginalValues();

		return vexScanSettingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		scanid = objectInput.readLong();
		starturl = objectInput.readUTF();
		authorizedpatrolurl = objectInput.readUTF();
		unauthorizedpatrolurl = objectInput.readUTF();
		starttime = objectInput.readLong();
		endtime = objectInput.readLong();

		setemailnotification = objectInput.readInt();
		selectedwebsignature = objectInput.readUTF();

		getserverfiles = objectInput.readInt();

		getserversettings = objectInput.readInt();
		selectedserverfilessignature = objectInput.readUTF();
		selectedserversettingssignature = objectInput.readUTF();

		maxnumdetectionlink = objectInput.readInt();

		maxnumdetectionlinkperpage = objectInput.readInt();

		detectionlinkdepthlimit = objectInput.readInt();

		waittime = objectInput.readInt();

		numofthread = objectInput.readInt();

		timeouttime = objectInput.readInt();

		setautopostform = objectInput.readInt();

		setpostmethod = objectInput.readInt();
		requestheader = objectInput.readUTF();
		loginstatusdetection = objectInput.readUTF();

		setnotsendpassword = objectInput.readInt();
		paramnamesaspassword = objectInput.readUTF();

		numofretryaterror = objectInput.readInt();
		sessionerrordetection = objectInput.readUTF();
		screentransitionerrordetection = objectInput.readUTF();

		implementationenvironment = objectInput.readInt();

		setcrawlingonly = objectInput.readInt();
		excludepathinforegex = objectInput.readUTF();
		excludeparameterdeletenameregex = objectInput.readUTF();
		excludeparameterdeleteparamregex = objectInput.readUTF();
		extracturlregex = objectInput.readUTF();
		notparseextension = objectInput.readUTF();
		notparsefilenameregex = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(scanid);

		if (starturl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(starturl);
		}

		if (authorizedpatrolurl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(authorizedpatrolurl);
		}

		if (unauthorizedpatrolurl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(unauthorizedpatrolurl);
		}

		objectOutput.writeLong(starttime);
		objectOutput.writeLong(endtime);

		objectOutput.writeInt(setemailnotification);

		if (selectedwebsignature == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(selectedwebsignature);
		}

		objectOutput.writeInt(getserverfiles);

		objectOutput.writeInt(getserversettings);

		if (selectedserverfilessignature == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(selectedserverfilessignature);
		}

		if (selectedserversettingssignature == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(selectedserversettingssignature);
		}

		objectOutput.writeInt(maxnumdetectionlink);

		objectOutput.writeInt(maxnumdetectionlinkperpage);

		objectOutput.writeInt(detectionlinkdepthlimit);

		objectOutput.writeInt(waittime);

		objectOutput.writeInt(numofthread);

		objectOutput.writeInt(timeouttime);

		objectOutput.writeInt(setautopostform);

		objectOutput.writeInt(setpostmethod);

		if (requestheader == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(requestheader);
		}

		if (loginstatusdetection == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(loginstatusdetection);
		}

		objectOutput.writeInt(setnotsendpassword);

		if (paramnamesaspassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paramnamesaspassword);
		}

		objectOutput.writeInt(numofretryaterror);

		if (sessionerrordetection == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(sessionerrordetection);
		}

		if (screentransitionerrordetection == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(screentransitionerrordetection);
		}

		objectOutput.writeInt(implementationenvironment);

		objectOutput.writeInt(setcrawlingonly);

		if (excludepathinforegex == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(excludepathinforegex);
		}

		if (excludeparameterdeletenameregex == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(excludeparameterdeletenameregex);
		}

		if (excludeparameterdeleteparamregex == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(excludeparameterdeleteparamregex);
		}

		if (extracturlregex == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(extracturlregex);
		}

		if (notparseextension == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(notparseextension);
		}

		if (notparsefilenameregex == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(notparsefilenameregex);
		}
	}

	public long scanid;
	public String starturl;
	public String authorizedpatrolurl;
	public String unauthorizedpatrolurl;
	public long starttime;
	public long endtime;
	public int setemailnotification;
	public String selectedwebsignature;
	public int getserverfiles;
	public int getserversettings;
	public String selectedserverfilessignature;
	public String selectedserversettingssignature;
	public int maxnumdetectionlink;
	public int maxnumdetectionlinkperpage;
	public int detectionlinkdepthlimit;
	public int waittime;
	public int numofthread;
	public int timeouttime;
	public int setautopostform;
	public int setpostmethod;
	public String requestheader;
	public String loginstatusdetection;
	public int setnotsendpassword;
	public String paramnamesaspassword;
	public int numofretryaterror;
	public String sessionerrordetection;
	public String screentransitionerrordetection;
	public int implementationenvironment;
	public int setcrawlingonly;
	public String excludepathinforegex;
	public String excludeparameterdeletenameregex;
	public String excludeparameterdeleteparamregex;
	public String extracturlregex;
	public String notparseextension;
	public String notparsefilenameregex;
}