/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;

import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ReportPersistence;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ReportFinderBaseImpl extends BasePersistenceImpl<Report> {
	/**
	 * Returns the report persistence.
	 *
	 * @return the report persistence
	 */
	public ReportPersistence getReportPersistence() {
		return reportPersistence;
	}

	/**
	 * Sets the report persistence.
	 *
	 * @param reportPersistence the report persistence
	 */
	public void setReportPersistence(ReportPersistence reportPersistence) {
		this.reportPersistence = reportPersistence;
	}

	@BeanReference(type = ReportPersistence.class)
	protected ReportPersistence reportPersistence;
}