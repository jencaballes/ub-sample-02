/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;

import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ScanPersistence;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ScanFinderBaseImpl extends BasePersistenceImpl<Scan> {
	/**
	 * Returns the scan persistence.
	 *
	 * @return the scan persistence
	 */
	public ScanPersistence getScanPersistence() {
		return scanPersistence;
	}

	/**
	 * Sets the scan persistence.
	 *
	 * @param scanPersistence the scan persistence
	 */
	public void setScanPersistence(ScanPersistence scanPersistence) {
		this.scanPersistence = scanPersistence;
	}

	@BeanReference(type = ScanPersistence.class)
	protected ScanPersistence scanPersistence;
}