/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ScanImpl;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ScanModelImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ScanPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the scan service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ScanPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.ScanUtil
 * @generated
 */
@ProviderType
public class ScanPersistenceImpl extends BasePersistenceImpl<Scan>
	implements ScanPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ScanUtil} to access the scan persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ScanImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTID =
		new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByProjectId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID =
		new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByProjectId",
			new String[] { Long.class.getName() },
			ScanModelImpl.PROJECTID_COLUMN_BITMASK |
			ScanModelImpl.REGISTRATIONDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTID = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByProjectId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the scans where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @return the matching scans
	 */
	@Override
	public List<Scan> findByProjectId(long projectId) {
		return findByProjectId(projectId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the scans where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @return the range of matching scans
	 */
	@Override
	public List<Scan> findByProjectId(long projectId, int start, int end) {
		return findByProjectId(projectId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the scans where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching scans
	 */
	@Override
	public List<Scan> findByProjectId(long projectId, int start, int end,
		OrderByComparator<Scan> orderByComparator) {
		return findByProjectId(projectId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the scans where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching scans
	 */
	@Override
	public List<Scan> findByProjectId(long projectId, int start, int end,
		OrderByComparator<Scan> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID;
			finderArgs = new Object[] { projectId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTID;
			finderArgs = new Object[] { projectId, start, end, orderByComparator };
		}

		List<Scan> list = null;

		if (retrieveFromCache) {
			list = (List<Scan>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Scan scan : list) {
					if ((projectId != scan.getProjectId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SCAN_WHERE);

			query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ScanModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				if (!pagination) {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first scan in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching scan
	 * @throws NoSuchScanException if a matching scan could not be found
	 */
	@Override
	public Scan findByProjectId_First(long projectId,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = fetchByProjectId_First(projectId, orderByComparator);

		if (scan != null) {
			return scan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchScanException(msg.toString());
	}

	/**
	 * Returns the first scan in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching scan, or <code>null</code> if a matching scan could not be found
	 */
	@Override
	public Scan fetchByProjectId_First(long projectId,
		OrderByComparator<Scan> orderByComparator) {
		List<Scan> list = findByProjectId(projectId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last scan in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching scan
	 * @throws NoSuchScanException if a matching scan could not be found
	 */
	@Override
	public Scan findByProjectId_Last(long projectId,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = fetchByProjectId_Last(projectId, orderByComparator);

		if (scan != null) {
			return scan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchScanException(msg.toString());
	}

	/**
	 * Returns the last scan in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching scan, or <code>null</code> if a matching scan could not be found
	 */
	@Override
	public Scan fetchByProjectId_Last(long projectId,
		OrderByComparator<Scan> orderByComparator) {
		int count = countByProjectId(projectId);

		if (count == 0) {
			return null;
		}

		List<Scan> list = findByProjectId(projectId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the scans before and after the current scan in the ordered set where projectId = &#63;.
	 *
	 * @param scanId the primary key of the current scan
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next scan
	 * @throws NoSuchScanException if a scan with the primary key could not be found
	 */
	@Override
	public Scan[] findByProjectId_PrevAndNext(long scanId, long projectId,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = findByPrimaryKey(scanId);

		Session session = null;

		try {
			session = openSession();

			Scan[] array = new ScanImpl[3];

			array[0] = getByProjectId_PrevAndNext(session, scan, projectId,
					orderByComparator, true);

			array[1] = scan;

			array[2] = getByProjectId_PrevAndNext(session, scan, projectId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Scan getByProjectId_PrevAndNext(Session session, Scan scan,
		long projectId, OrderByComparator<Scan> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SCAN_WHERE);

		query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ScanModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(projectId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(scan);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Scan> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the scans where projectId = &#63; from the database.
	 *
	 * @param projectId the project ID
	 */
	@Override
	public void removeByProjectId(long projectId) {
		for (Scan scan : findByProjectId(projectId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(scan);
		}
	}

	/**
	 * Returns the number of scans where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @return the number of matching scans
	 */
	@Override
	public int countByProjectId(long projectId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTID;

		Object[] finderArgs = new Object[] { projectId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SCAN_WHERE);

			query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTID_PROJECTID_2 = "scan.projectId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RUNID = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByRunId",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNID = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByRunId",
			new String[] { String.class.getName() },
			ScanModelImpl.CXRUNID_COLUMN_BITMASK |
			ScanModelImpl.REGISTRATIONDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_RUNID = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByRunId",
			new String[] { String.class.getName() });

	/**
	 * Returns all the scans where cxRunId = &#63;.
	 *
	 * @param cxRunId the cx run ID
	 * @return the matching scans
	 */
	@Override
	public List<Scan> findByRunId(String cxRunId) {
		return findByRunId(cxRunId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the scans where cxRunId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cxRunId the cx run ID
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @return the range of matching scans
	 */
	@Override
	public List<Scan> findByRunId(String cxRunId, int start, int end) {
		return findByRunId(cxRunId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the scans where cxRunId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cxRunId the cx run ID
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching scans
	 */
	@Override
	public List<Scan> findByRunId(String cxRunId, int start, int end,
		OrderByComparator<Scan> orderByComparator) {
		return findByRunId(cxRunId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the scans where cxRunId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param cxRunId the cx run ID
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching scans
	 */
	@Override
	public List<Scan> findByRunId(String cxRunId, int start, int end,
		OrderByComparator<Scan> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNID;
			finderArgs = new Object[] { cxRunId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RUNID;
			finderArgs = new Object[] { cxRunId, start, end, orderByComparator };
		}

		List<Scan> list = null;

		if (retrieveFromCache) {
			list = (List<Scan>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Scan scan : list) {
					if (!Objects.equals(cxRunId, scan.getCxRunId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SCAN_WHERE);

			boolean bindCxRunId = false;

			if (cxRunId == null) {
				query.append(_FINDER_COLUMN_RUNID_CXRUNID_1);
			}
			else if (cxRunId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RUNID_CXRUNID_3);
			}
			else {
				bindCxRunId = true;

				query.append(_FINDER_COLUMN_RUNID_CXRUNID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ScanModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCxRunId) {
					qPos.add(cxRunId);
				}

				if (!pagination) {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first scan in the ordered set where cxRunId = &#63;.
	 *
	 * @param cxRunId the cx run ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching scan
	 * @throws NoSuchScanException if a matching scan could not be found
	 */
	@Override
	public Scan findByRunId_First(String cxRunId,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = fetchByRunId_First(cxRunId, orderByComparator);

		if (scan != null) {
			return scan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("cxRunId=");
		msg.append(cxRunId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchScanException(msg.toString());
	}

	/**
	 * Returns the first scan in the ordered set where cxRunId = &#63;.
	 *
	 * @param cxRunId the cx run ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching scan, or <code>null</code> if a matching scan could not be found
	 */
	@Override
	public Scan fetchByRunId_First(String cxRunId,
		OrderByComparator<Scan> orderByComparator) {
		List<Scan> list = findByRunId(cxRunId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last scan in the ordered set where cxRunId = &#63;.
	 *
	 * @param cxRunId the cx run ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching scan
	 * @throws NoSuchScanException if a matching scan could not be found
	 */
	@Override
	public Scan findByRunId_Last(String cxRunId,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = fetchByRunId_Last(cxRunId, orderByComparator);

		if (scan != null) {
			return scan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("cxRunId=");
		msg.append(cxRunId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchScanException(msg.toString());
	}

	/**
	 * Returns the last scan in the ordered set where cxRunId = &#63;.
	 *
	 * @param cxRunId the cx run ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching scan, or <code>null</code> if a matching scan could not be found
	 */
	@Override
	public Scan fetchByRunId_Last(String cxRunId,
		OrderByComparator<Scan> orderByComparator) {
		int count = countByRunId(cxRunId);

		if (count == 0) {
			return null;
		}

		List<Scan> list = findByRunId(cxRunId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the scans before and after the current scan in the ordered set where cxRunId = &#63;.
	 *
	 * @param scanId the primary key of the current scan
	 * @param cxRunId the cx run ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next scan
	 * @throws NoSuchScanException if a scan with the primary key could not be found
	 */
	@Override
	public Scan[] findByRunId_PrevAndNext(long scanId, String cxRunId,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = findByPrimaryKey(scanId);

		Session session = null;

		try {
			session = openSession();

			Scan[] array = new ScanImpl[3];

			array[0] = getByRunId_PrevAndNext(session, scan, cxRunId,
					orderByComparator, true);

			array[1] = scan;

			array[2] = getByRunId_PrevAndNext(session, scan, cxRunId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Scan getByRunId_PrevAndNext(Session session, Scan scan,
		String cxRunId, OrderByComparator<Scan> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SCAN_WHERE);

		boolean bindCxRunId = false;

		if (cxRunId == null) {
			query.append(_FINDER_COLUMN_RUNID_CXRUNID_1);
		}
		else if (cxRunId.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_RUNID_CXRUNID_3);
		}
		else {
			bindCxRunId = true;

			query.append(_FINDER_COLUMN_RUNID_CXRUNID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ScanModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindCxRunId) {
			qPos.add(cxRunId);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(scan);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Scan> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the scans where cxRunId = &#63; from the database.
	 *
	 * @param cxRunId the cx run ID
	 */
	@Override
	public void removeByRunId(String cxRunId) {
		for (Scan scan : findByRunId(cxRunId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(scan);
		}
	}

	/**
	 * Returns the number of scans where cxRunId = &#63;.
	 *
	 * @param cxRunId the cx run ID
	 * @return the number of matching scans
	 */
	@Override
	public int countByRunId(String cxRunId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_RUNID;

		Object[] finderArgs = new Object[] { cxRunId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SCAN_WHERE);

			boolean bindCxRunId = false;

			if (cxRunId == null) {
				query.append(_FINDER_COLUMN_RUNID_CXRUNID_1);
			}
			else if (cxRunId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RUNID_CXRUNID_3);
			}
			else {
				bindCxRunId = true;

				query.append(_FINDER_COLUMN_RUNID_CXRUNID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCxRunId) {
					qPos.add(cxRunId);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_RUNID_CXRUNID_1 = "scan.cxRunId IS NULL";
	private static final String _FINDER_COLUMN_RUNID_CXRUNID_2 = "scan.cxRunId = ?";
	private static final String _FINDER_COLUMN_RUNID_CXRUNID_3 = "(scan.cxRunId IS NULL OR scan.cxRunId = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANIDSCANSTATUS =
		new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByScanIdScanStatus",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDSCANSTATUS =
		new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByScanIdScanStatus",
			new String[] { Long.class.getName(), Integer.class.getName() },
			ScanModelImpl.SCANID_COLUMN_BITMASK |
			ScanModelImpl.STATUS_COLUMN_BITMASK |
			ScanModelImpl.REGISTRATIONDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SCANIDSCANSTATUS = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByScanIdScanStatus",
			new String[] { Long.class.getName(), Integer.class.getName() });

	/**
	 * Returns all the scans where scanId = &#63; and status = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @return the matching scans
	 */
	@Override
	public List<Scan> findByScanIdScanStatus(long scanId, int status) {
		return findByScanIdScanStatus(scanId, status, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the scans where scanId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @return the range of matching scans
	 */
	@Override
	public List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end) {
		return findByScanIdScanStatus(scanId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the scans where scanId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching scans
	 */
	@Override
	public List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end, OrderByComparator<Scan> orderByComparator) {
		return findByScanIdScanStatus(scanId, status, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the scans where scanId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching scans
	 */
	@Override
	public List<Scan> findByScanIdScanStatus(long scanId, int status,
		int start, int end, OrderByComparator<Scan> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDSCANSTATUS;
			finderArgs = new Object[] { scanId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANIDSCANSTATUS;
			finderArgs = new Object[] {
					scanId, status,
					
					start, end, orderByComparator
				};
		}

		List<Scan> list = null;

		if (retrieveFromCache) {
			list = (List<Scan>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Scan scan : list) {
					if ((scanId != scan.getScanId()) ||
							(status != scan.getStatus())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_SCAN_WHERE);

			query.append(_FINDER_COLUMN_SCANIDSCANSTATUS_SCANID_2);

			query.append(_FINDER_COLUMN_SCANIDSCANSTATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ScanModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanId);

				qPos.add(status);

				if (!pagination) {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first scan in the ordered set where scanId = &#63; and status = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching scan
	 * @throws NoSuchScanException if a matching scan could not be found
	 */
	@Override
	public Scan findByScanIdScanStatus_First(long scanId, int status,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = fetchByScanIdScanStatus_First(scanId, status,
				orderByComparator);

		if (scan != null) {
			return scan;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanId=");
		msg.append(scanId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchScanException(msg.toString());
	}

	/**
	 * Returns the first scan in the ordered set where scanId = &#63; and status = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching scan, or <code>null</code> if a matching scan could not be found
	 */
	@Override
	public Scan fetchByScanIdScanStatus_First(long scanId, int status,
		OrderByComparator<Scan> orderByComparator) {
		List<Scan> list = findByScanIdScanStatus(scanId, status, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last scan in the ordered set where scanId = &#63; and status = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching scan
	 * @throws NoSuchScanException if a matching scan could not be found
	 */
	@Override
	public Scan findByScanIdScanStatus_Last(long scanId, int status,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = fetchByScanIdScanStatus_Last(scanId, status,
				orderByComparator);

		if (scan != null) {
			return scan;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanId=");
		msg.append(scanId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchScanException(msg.toString());
	}

	/**
	 * Returns the last scan in the ordered set where scanId = &#63; and status = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching scan, or <code>null</code> if a matching scan could not be found
	 */
	@Override
	public Scan fetchByScanIdScanStatus_Last(long scanId, int status,
		OrderByComparator<Scan> orderByComparator) {
		int count = countByScanIdScanStatus(scanId, status);

		if (count == 0) {
			return null;
		}

		List<Scan> list = findByScanIdScanStatus(scanId, status, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the scans where scanId = &#63; and status = &#63; from the database.
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 */
	@Override
	public void removeByScanIdScanStatus(long scanId, int status) {
		for (Scan scan : findByScanIdScanStatus(scanId, status,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(scan);
		}
	}

	/**
	 * Returns the number of scans where scanId = &#63; and status = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param status the status
	 * @return the number of matching scans
	 */
	@Override
	public int countByScanIdScanStatus(long scanId, int status) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SCANIDSCANSTATUS;

		Object[] finderArgs = new Object[] { scanId, status };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_SCAN_WHERE);

			query.append(_FINDER_COLUMN_SCANIDSCANSTATUS_SCANID_2);

			query.append(_FINDER_COLUMN_SCANIDSCANSTATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanId);

				qPos.add(status);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCANIDSCANSTATUS_SCANID_2 = "scan.scanId = ? AND ";
	private static final String _FINDER_COLUMN_SCANIDSCANSTATUS_STATUS_2 = "scan.status = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANSTATUS =
		new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByScanStatus",
			new String[] {
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANSTATUS =
		new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, ScanImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByScanStatus",
			new String[] { Integer.class.getName() },
			ScanModelImpl.STATUS_COLUMN_BITMASK |
			ScanModelImpl.REGISTRATIONDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SCANSTATUS = new FinderPath(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScanStatus",
			new String[] { Integer.class.getName() });

	/**
	 * Returns all the scans where status = &#63;.
	 *
	 * @param status the status
	 * @return the matching scans
	 */
	@Override
	public List<Scan> findByScanStatus(int status) {
		return findByScanStatus(status, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the scans where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @return the range of matching scans
	 */
	@Override
	public List<Scan> findByScanStatus(int status, int start, int end) {
		return findByScanStatus(status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the scans where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching scans
	 */
	@Override
	public List<Scan> findByScanStatus(int status, int start, int end,
		OrderByComparator<Scan> orderByComparator) {
		return findByScanStatus(status, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the scans where status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param status the status
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching scans
	 */
	@Override
	public List<Scan> findByScanStatus(int status, int start, int end,
		OrderByComparator<Scan> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANSTATUS;
			finderArgs = new Object[] { status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANSTATUS;
			finderArgs = new Object[] { status, start, end, orderByComparator };
		}

		List<Scan> list = null;

		if (retrieveFromCache) {
			list = (List<Scan>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Scan scan : list) {
					if ((status != scan.getStatus())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SCAN_WHERE);

			query.append(_FINDER_COLUMN_SCANSTATUS_STATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ScanModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				if (!pagination) {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first scan in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching scan
	 * @throws NoSuchScanException if a matching scan could not be found
	 */
	@Override
	public Scan findByScanStatus_First(int status,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = fetchByScanStatus_First(status, orderByComparator);

		if (scan != null) {
			return scan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchScanException(msg.toString());
	}

	/**
	 * Returns the first scan in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching scan, or <code>null</code> if a matching scan could not be found
	 */
	@Override
	public Scan fetchByScanStatus_First(int status,
		OrderByComparator<Scan> orderByComparator) {
		List<Scan> list = findByScanStatus(status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last scan in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching scan
	 * @throws NoSuchScanException if a matching scan could not be found
	 */
	@Override
	public Scan findByScanStatus_Last(int status,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = fetchByScanStatus_Last(status, orderByComparator);

		if (scan != null) {
			return scan;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchScanException(msg.toString());
	}

	/**
	 * Returns the last scan in the ordered set where status = &#63;.
	 *
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching scan, or <code>null</code> if a matching scan could not be found
	 */
	@Override
	public Scan fetchByScanStatus_Last(int status,
		OrderByComparator<Scan> orderByComparator) {
		int count = countByScanStatus(status);

		if (count == 0) {
			return null;
		}

		List<Scan> list = findByScanStatus(status, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the scans before and after the current scan in the ordered set where status = &#63;.
	 *
	 * @param scanId the primary key of the current scan
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next scan
	 * @throws NoSuchScanException if a scan with the primary key could not be found
	 */
	@Override
	public Scan[] findByScanStatus_PrevAndNext(long scanId, int status,
		OrderByComparator<Scan> orderByComparator) throws NoSuchScanException {
		Scan scan = findByPrimaryKey(scanId);

		Session session = null;

		try {
			session = openSession();

			Scan[] array = new ScanImpl[3];

			array[0] = getByScanStatus_PrevAndNext(session, scan, status,
					orderByComparator, true);

			array[1] = scan;

			array[2] = getByScanStatus_PrevAndNext(session, scan, status,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Scan getByScanStatus_PrevAndNext(Session session, Scan scan,
		int status, OrderByComparator<Scan> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SCAN_WHERE);

		query.append(_FINDER_COLUMN_SCANSTATUS_STATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ScanModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(status);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(scan);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Scan> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the scans where status = &#63; from the database.
	 *
	 * @param status the status
	 */
	@Override
	public void removeByScanStatus(int status) {
		for (Scan scan : findByScanStatus(status, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(scan);
		}
	}

	/**
	 * Returns the number of scans where status = &#63;.
	 *
	 * @param status the status
	 * @return the number of matching scans
	 */
	@Override
	public int countByScanStatus(int status) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SCANSTATUS;

		Object[] finderArgs = new Object[] { status };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SCAN_WHERE);

			query.append(_FINDER_COLUMN_SCANSTATUS_STATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(status);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCANSTATUS_STATUS_2 = "scan.status = ?";

	public ScanPersistenceImpl() {
		setModelClass(Scan.class);
	}

	/**
	 * Caches the scan in the entity cache if it is enabled.
	 *
	 * @param scan the scan
	 */
	@Override
	public void cacheResult(Scan scan) {
		entityCache.putResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanImpl.class, scan.getPrimaryKey(), scan);

		scan.resetOriginalValues();
	}

	/**
	 * Caches the scans in the entity cache if it is enabled.
	 *
	 * @param scans the scans
	 */
	@Override
	public void cacheResult(List<Scan> scans) {
		for (Scan scan : scans) {
			if (entityCache.getResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
						ScanImpl.class, scan.getPrimaryKey()) == null) {
				cacheResult(scan);
			}
			else {
				scan.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all scans.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ScanImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the scan.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Scan scan) {
		entityCache.removeResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanImpl.class, scan.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Scan> scans) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Scan scan : scans) {
			entityCache.removeResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
				ScanImpl.class, scan.getPrimaryKey());
		}
	}

	/**
	 * Creates a new scan with the primary key. Does not add the scan to the database.
	 *
	 * @param scanId the primary key for the new scan
	 * @return the new scan
	 */
	@Override
	public Scan create(long scanId) {
		Scan scan = new ScanImpl();

		scan.setNew(true);
		scan.setPrimaryKey(scanId);

		return scan;
	}

	/**
	 * Removes the scan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param scanId the primary key of the scan
	 * @return the scan that was removed
	 * @throws NoSuchScanException if a scan with the primary key could not be found
	 */
	@Override
	public Scan remove(long scanId) throws NoSuchScanException {
		return remove((Serializable)scanId);
	}

	/**
	 * Removes the scan with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the scan
	 * @return the scan that was removed
	 * @throws NoSuchScanException if a scan with the primary key could not be found
	 */
	@Override
	public Scan remove(Serializable primaryKey) throws NoSuchScanException {
		Session session = null;

		try {
			session = openSession();

			Scan scan = (Scan)session.get(ScanImpl.class, primaryKey);

			if (scan == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchScanException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(scan);
		}
		catch (NoSuchScanException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Scan removeImpl(Scan scan) {
		scan = toUnwrappedModel(scan);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(scan)) {
				scan = (Scan)session.get(ScanImpl.class, scan.getPrimaryKeyObj());
			}

			if (scan != null) {
				session.delete(scan);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (scan != null) {
			clearCache(scan);
		}

		return scan;
	}

	@Override
	public Scan updateImpl(Scan scan) {
		scan = toUnwrappedModel(scan);

		boolean isNew = scan.isNew();

		ScanModelImpl scanModelImpl = (ScanModelImpl)scan;

		Session session = null;

		try {
			session = openSession();

			if (scan.isNew()) {
				session.save(scan);

				scan.setNew(false);
			}
			else {
				scan = (Scan)session.merge(scan);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ScanModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((scanModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						scanModelImpl.getOriginalProjectId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID,
					args);

				args = new Object[] { scanModelImpl.getProjectId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID,
					args);
			}

			if ((scanModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { scanModelImpl.getOriginalCxRunId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_RUNID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNID,
					args);

				args = new Object[] { scanModelImpl.getCxRunId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_RUNID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RUNID,
					args);
			}

			if ((scanModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDSCANSTATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						scanModelImpl.getOriginalScanId(),
						scanModelImpl.getOriginalStatus()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANIDSCANSTATUS,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDSCANSTATUS,
					args);

				args = new Object[] {
						scanModelImpl.getScanId(), scanModelImpl.getStatus()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANIDSCANSTATUS,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDSCANSTATUS,
					args);
			}

			if ((scanModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANSTATUS.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { scanModelImpl.getOriginalStatus() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANSTATUS, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANSTATUS,
					args);

				args = new Object[] { scanModelImpl.getStatus() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANSTATUS, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANSTATUS,
					args);
			}
		}

		entityCache.putResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
			ScanImpl.class, scan.getPrimaryKey(), scan, false);

		scan.resetOriginalValues();

		return scan;
	}

	protected Scan toUnwrappedModel(Scan scan) {
		if (scan instanceof ScanImpl) {
			return scan;
		}

		ScanImpl scanImpl = new ScanImpl();

		scanImpl.setNew(scan.isNew());
		scanImpl.setPrimaryKey(scan.getPrimaryKey());

		scanImpl.setScanId(scan.getScanId());
		scanImpl.setRegistrationDate(scan.getRegistrationDate());
		scanImpl.setStatus(scan.getStatus());
		scanImpl.setProcess(scan.getProcess());
		scanImpl.setCxAndroidScanId(scan.getCxAndroidScanId());
		scanImpl.setScanManager(scan.getScanManager());
		scanImpl.setInfoCount(scan.getInfoCount());
		scanImpl.setHighCount(scan.getHighCount());
		scanImpl.setMediumCount(scan.getMediumCount());
		scanImpl.setLowCount(scan.getLowCount());
		scanImpl.setFileName(scan.getFileName());
		scanImpl.setHashValue(scan.getHashValue());
		scanImpl.setFilePath(scan.getFilePath());
		scanImpl.setCxRunId(scan.getCxRunId());
		scanImpl.setProjectId(scan.getProjectId());
		scanImpl.setReviewFlag(scan.getReviewFlag());
		scanImpl.setFailedScanCause(scan.getFailedScanCause());
		scanImpl.setIsDeletedInCx(scan.getIsDeletedInCx());
		scanImpl.setModifiedDate(scan.getModifiedDate());
		scanImpl.setCxAndroidProjectId(scan.getCxAndroidProjectId());

		return scanImpl;
	}

	/**
	 * Returns the scan with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the scan
	 * @return the scan
	 * @throws NoSuchScanException if a scan with the primary key could not be found
	 */
	@Override
	public Scan findByPrimaryKey(Serializable primaryKey)
		throws NoSuchScanException {
		Scan scan = fetchByPrimaryKey(primaryKey);

		if (scan == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchScanException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return scan;
	}

	/**
	 * Returns the scan with the primary key or throws a {@link NoSuchScanException} if it could not be found.
	 *
	 * @param scanId the primary key of the scan
	 * @return the scan
	 * @throws NoSuchScanException if a scan with the primary key could not be found
	 */
	@Override
	public Scan findByPrimaryKey(long scanId) throws NoSuchScanException {
		return findByPrimaryKey((Serializable)scanId);
	}

	/**
	 * Returns the scan with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the scan
	 * @return the scan, or <code>null</code> if a scan with the primary key could not be found
	 */
	@Override
	public Scan fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
				ScanImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Scan scan = (Scan)serializable;

		if (scan == null) {
			Session session = null;

			try {
				session = openSession();

				scan = (Scan)session.get(ScanImpl.class, primaryKey);

				if (scan != null) {
					cacheResult(scan);
				}
				else {
					entityCache.putResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
						ScanImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
					ScanImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return scan;
	}

	/**
	 * Returns the scan with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param scanId the primary key of the scan
	 * @return the scan, or <code>null</code> if a scan with the primary key could not be found
	 */
	@Override
	public Scan fetchByPrimaryKey(long scanId) {
		return fetchByPrimaryKey((Serializable)scanId);
	}

	@Override
	public Map<Serializable, Scan> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Scan> map = new HashMap<Serializable, Scan>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Scan scan = fetchByPrimaryKey(primaryKey);

			if (scan != null) {
				map.put(primaryKey, scan);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
					ScanImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Scan)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_SCAN_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Scan scan : (List<Scan>)q.list()) {
				map.put(scan.getPrimaryKeyObj(), scan);

				cacheResult(scan);

				uncachedPrimaryKeys.remove(scan.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ScanModelImpl.ENTITY_CACHE_ENABLED,
					ScanImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the scans.
	 *
	 * @return the scans
	 */
	@Override
	public List<Scan> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the scans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @return the range of scans
	 */
	@Override
	public List<Scan> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the scans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of scans
	 */
	@Override
	public List<Scan> findAll(int start, int end,
		OrderByComparator<Scan> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the scans.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ScanModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of scans
	 * @param end the upper bound of the range of scans (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of scans
	 */
	@Override
	public List<Scan> findAll(int start, int end,
		OrderByComparator<Scan> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Scan> list = null;

		if (retrieveFromCache) {
			list = (List<Scan>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_SCAN);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SCAN;

				if (pagination) {
					sql = sql.concat(ScanModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Scan>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the scans from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Scan scan : findAll()) {
			remove(scan);
		}
	}

	/**
	 * Returns the number of scans.
	 *
	 * @return the number of scans
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SCAN);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ScanModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the scan persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ScanImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_SCAN = "SELECT scan FROM Scan scan";
	private static final String _SQL_SELECT_SCAN_WHERE_PKS_IN = "SELECT scan FROM Scan scan WHERE scanId IN (";
	private static final String _SQL_SELECT_SCAN_WHERE = "SELECT scan FROM Scan scan WHERE ";
	private static final String _SQL_COUNT_SCAN = "SELECT COUNT(scan) FROM Scan scan";
	private static final String _SQL_COUNT_SCAN_WHERE = "SELECT COUNT(scan) FROM Scan scan WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "scan.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Scan exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Scan exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ScanPersistenceImpl.class);
}