/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

import aQute.bnd.annotation.ProviderType;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersImpl;
import jp.ubsecure.portal.jubjub.portlet.service.base.ProjectUsersLocalServiceBaseImpl;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * The implementation of the project users local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author ASI
 * @see ProjectUsersLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil
 */
@ProviderType
public class ProjectUsersLocalServiceImpl extends ProjectUsersLocalServiceBaseImpl {

	/** UBSPortalDebugger */
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ProjectUsersLocalServiceImpl.class);

	/**
	 * Delete all user in project user table
	 * 
	 * @param userId
	 *            a user id to be deleted
	 * @return return true if delete transaction is successful
	 * @throws PortalException
	 *             - an exception thrown if transaction fails.
	 */
	public boolean deleteProjectUser(String userId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;

		try {
			if (CommonUtil.isStringNullOrEmpty(userId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
				throw new PortalException(PortalErrors.GET_USER_USER_ID_INVALID);
			} else {
				projectUsersPersistence.clearCache();
				projectUsersPersistence.removeByUserId(userId);
			}
		} catch (PortalException e) {
			params.put("userId", userId);
			log.debug(e.getMessage(), PortalConstants.METHOD_DELETE_PROJECT_USER, params, e);
			throw e;
		} catch (SystemException e) {
			params.put("userId", userId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_DELETE_PROJECT_USER, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} finally {
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Get all User of a certain project.
	 * 
	 * @param projectId
	 *            - the id of the project
	 * @return projectUsersList - List of Users of the project
	 * @throws PortalException
	 *             an exception thrown if transaction fails
	 */
	public List<ProjectUsers> getProjectUsers(long projectId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<ProjectUsers> projectUsersList = new ArrayList<ProjectUsers>();

		try {
			if (projectId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.GET_PROJECT_USERS_PROJECT_ID_INVALID);
			}

			projectUsersPersistence.clearCache();
			projectUsersList = projectUsersPersistence.findByProjectId(projectId);
		} catch (SystemException se) {
			params.put("projectId", projectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECT_USERS, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("projectId", projectId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECT_USERS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return projectUsersList;
	}

	/**
	 * Update the Project User table
	 * 
	 * @param projectId
	 *            - id of the project to be updated
	 * @param lUsersArr
	 *            - list of user to be assign on this project
	 * @return return true if transaction is successful
	 * @throws PortalException
	 *             an exception thrown if transaction fails
	 */
	public boolean updateProjectUsers(long projectId, long[] lUsersArr) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		int length = 0;
		User user = null;
		ProjectUsers projectUser = new ProjectUsersImpl();

		try {
			if (projectId == 0L) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.UPDATE_PROJECT_PROJECT_ID_INVALID);
			}

			if (lUsersArr == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED,
								PortalConstants.ERROR_LOG_PARAM_PROJECT_USER));
				throw new PortalException(PortalErrors.NO_PROJECT_USERS);
			}

			projectUsersPersistence.clearCache();
			projectUsersPersistence.removeByProjectId(projectId);

			length = lUsersArr.length;

			for (int i = 0; i < length; i++) {
				user = UserLocalServiceUtil.getUserById(lUsersArr[i]);

				if (user == null) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE,
							String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST,
									PortalConstants.ERROR_LOG_PARAM_USER));
					throw new PortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST);
				}
			}

			for (int i = 0; i < length; i++) {
				user = UserLocalServiceUtil.getUserById(lUsersArr[i]);

				if (user != null) {
					projectUser.setProjectId(projectId);
					projectUser.setUserId(user.getEmailAddress());

					projectUsersPersistence.clearCache();
					projectUsersPersistence.update(projectUser);

					user = null;
					projectUser = new ProjectUsersImpl();
				}
			}

			bSuccess = true;
		} catch (PortalException pe) {
			params.put("projectId", projectId);
			params.put("lUsersArr", lUsersArr);
			log.debug(pe.getMessage(), PortalConstants.METHOD_UPDATE_PROJECT_USERS, params, pe);
			throw pe;
		} catch (SystemException se) {
			params.put("projectId", projectId);
			params.put("lUsersArr", lUsersArr);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPDATE_PROJECT_USERS, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} finally {
			params.clear();
			params = null;
		}

		return bSuccess;
	}
}