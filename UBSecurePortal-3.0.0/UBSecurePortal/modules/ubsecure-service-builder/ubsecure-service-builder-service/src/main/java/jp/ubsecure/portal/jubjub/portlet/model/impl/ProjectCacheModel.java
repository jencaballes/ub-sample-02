/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.Project;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Project in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Project
 * @generated
 */
@ProviderType
public class ProjectCacheModel implements CacheModel<Project>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectCacheModel)) {
			return false;
		}

		ProjectCacheModel projectCacheModel = (ProjectCacheModel)obj;

		if (projectId == projectCacheModel.projectId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, projectId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{projectId=");
		sb.append(projectId);
		sb.append(", type=");
		sb.append(type);
		sb.append(", ownerGroup=");
		sb.append(ownerGroup);
		sb.append(", cxAndroidProjectId=");
		sb.append(cxAndroidProjectId);
		sb.append(", projectName=");
		sb.append(projectName);
		sb.append(", caseNumber=");
		sb.append(caseNumber);
		sb.append(", status=");
		sb.append(status);
		sb.append(", attribute=");
		sb.append(attribute);
		sb.append(", projectEndDate=");
		sb.append(projectEndDate);
		sb.append(", projectCreateDate=");
		sb.append(projectCreateDate);
		sb.append(", checklistFileName=");
		sb.append(checklistFileName);
		sb.append(", presetId=");
		sb.append(presetId);
		sb.append(", caseName=");
		sb.append(caseName);
		sb.append(", targetUrl=");
		sb.append(targetUrl);
		sb.append(", productionEnvironmentUrl=");
		sb.append(productionEnvironmentUrl);
		sb.append(", selectedWebSignature=");
		sb.append(selectedWebSignature);
		sb.append(", getServerFiles=");
		sb.append(getServerFiles);
		sb.append(", getServerSettings=");
		sb.append(getServerSettings);
		sb.append(", selectedServerFilesSignature=");
		sb.append(selectedServerFilesSignature);
		sb.append(", selectedServerSettingsSignature=");
		sb.append(selectedServerSettingsSignature);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Project toEntityModel() {
		ProjectImpl projectImpl = new ProjectImpl();

		projectImpl.setProjectId(projectId);
		projectImpl.setType(type);
		projectImpl.setOwnerGroup(ownerGroup);

		if (cxAndroidProjectId == null) {
			projectImpl.setCxAndroidProjectId(StringPool.BLANK);
		}
		else {
			projectImpl.setCxAndroidProjectId(cxAndroidProjectId);
		}

		if (projectName == null) {
			projectImpl.setProjectName(StringPool.BLANK);
		}
		else {
			projectImpl.setProjectName(projectName);
		}

		if (caseNumber == null) {
			projectImpl.setCaseNumber(StringPool.BLANK);
		}
		else {
			projectImpl.setCaseNumber(caseNumber);
		}

		projectImpl.setStatus(status);
		projectImpl.setAttribute(attribute);

		if (projectEndDate == Long.MIN_VALUE) {
			projectImpl.setProjectEndDate(null);
		}
		else {
			projectImpl.setProjectEndDate(new Date(projectEndDate));
		}

		if (projectCreateDate == Long.MIN_VALUE) {
			projectImpl.setProjectCreateDate(null);
		}
		else {
			projectImpl.setProjectCreateDate(new Date(projectCreateDate));
		}

		if (checklistFileName == null) {
			projectImpl.setChecklistFileName(StringPool.BLANK);
		}
		else {
			projectImpl.setChecklistFileName(checklistFileName);
		}

		projectImpl.setPresetId(presetId);

		if (caseName == null) {
			projectImpl.setCaseName(StringPool.BLANK);
		}
		else {
			projectImpl.setCaseName(caseName);
		}

		if (targetUrl == null) {
			projectImpl.setTargetUrl(StringPool.BLANK);
		}
		else {
			projectImpl.setTargetUrl(targetUrl);
		}

		if (productionEnvironmentUrl == null) {
			projectImpl.setProductionEnvironmentUrl(StringPool.BLANK);
		}
		else {
			projectImpl.setProductionEnvironmentUrl(productionEnvironmentUrl);
		}

		if (selectedWebSignature == null) {
			projectImpl.setSelectedWebSignature(StringPool.BLANK);
		}
		else {
			projectImpl.setSelectedWebSignature(selectedWebSignature);
		}

		projectImpl.setGetServerFiles(getServerFiles);
		projectImpl.setGetServerSettings(getServerSettings);

		if (selectedServerFilesSignature == null) {
			projectImpl.setSelectedServerFilesSignature(StringPool.BLANK);
		}
		else {
			projectImpl.setSelectedServerFilesSignature(selectedServerFilesSignature);
		}

		if (selectedServerSettingsSignature == null) {
			projectImpl.setSelectedServerSettingsSignature(StringPool.BLANK);
		}
		else {
			projectImpl.setSelectedServerSettingsSignature(selectedServerSettingsSignature);
		}

		projectImpl.resetOriginalValues();

		return projectImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		projectId = objectInput.readLong();

		type = objectInput.readInt();

		ownerGroup = objectInput.readLong();
		cxAndroidProjectId = objectInput.readUTF();
		projectName = objectInput.readUTF();
		caseNumber = objectInput.readUTF();

		status = objectInput.readInt();

		attribute = objectInput.readInt();
		projectEndDate = objectInput.readLong();
		projectCreateDate = objectInput.readLong();
		checklistFileName = objectInput.readUTF();

		presetId = objectInput.readLong();
		caseName = objectInput.readUTF();
		targetUrl = objectInput.readUTF();
		productionEnvironmentUrl = objectInput.readUTF();
		selectedWebSignature = objectInput.readUTF();

		getServerFiles = objectInput.readInt();

		getServerSettings = objectInput.readInt();
		selectedServerFilesSignature = objectInput.readUTF();
		selectedServerSettingsSignature = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(projectId);

		objectOutput.writeInt(type);

		objectOutput.writeLong(ownerGroup);

		if (cxAndroidProjectId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cxAndroidProjectId);
		}

		if (projectName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(projectName);
		}

		if (caseNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(caseNumber);
		}

		objectOutput.writeInt(status);

		objectOutput.writeInt(attribute);
		objectOutput.writeLong(projectEndDate);
		objectOutput.writeLong(projectCreateDate);

		if (checklistFileName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(checklistFileName);
		}

		objectOutput.writeLong(presetId);

		if (caseName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(caseName);
		}

		if (targetUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(targetUrl);
		}

		if (productionEnvironmentUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(productionEnvironmentUrl);
		}

		if (selectedWebSignature == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(selectedWebSignature);
		}

		objectOutput.writeInt(getServerFiles);

		objectOutput.writeInt(getServerSettings);

		if (selectedServerFilesSignature == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(selectedServerFilesSignature);
		}

		if (selectedServerSettingsSignature == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(selectedServerSettingsSignature);
		}
	}

	public long projectId;
	public int type;
	public long ownerGroup;
	public String cxAndroidProjectId;
	public String projectName;
	public String caseNumber;
	public int status;
	public int attribute;
	public long projectEndDate;
	public long projectCreateDate;
	public String checklistFileName;
	public long presetId;
	public String caseName;
	public String targetUrl;
	public String productionEnvironmentUrl;
	public String selectedWebSignature;
	public int getServerFiles;
	public int getServerSettings;
	public String selectedServerFilesSignature;
	public String selectedServerSettingsSignature;
}