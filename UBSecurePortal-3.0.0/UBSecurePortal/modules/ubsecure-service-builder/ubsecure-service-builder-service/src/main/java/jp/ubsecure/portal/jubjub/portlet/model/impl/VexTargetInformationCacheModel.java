/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VexTargetInformation in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see VexTargetInformation
 * @generated
 */
@ProviderType
public class VexTargetInformationCacheModel implements CacheModel<VexTargetInformation>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexTargetInformationCacheModel)) {
			return false;
		}

		VexTargetInformationCacheModel vexTargetInformationCacheModel = (VexTargetInformationCacheModel)obj;

		if (scanid == vexTargetInformationCacheModel.scanid) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, scanid);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(53);

		sb.append("{scanid=");
		sb.append(scanid);
		sb.append(", protocol=");
		sb.append(protocol);
		sb.append(", host=");
		sb.append(host);
		sb.append(", port=");
		sb.append(port);
		sb.append(", httpversion=");
		sb.append(httpversion);
		sb.append(", setkeepaliveconnection=");
		sb.append(setkeepaliveconnection);
		sb.append(", setresponsecontentlength=");
		sb.append(setresponsecontentlength);
		sb.append(", useacceptencodingheader=");
		sb.append(useacceptencodingheader);
		sb.append(", unzipresponse=");
		sb.append(unzipresponse);
		sb.append(", httpprotocol=");
		sb.append(httpprotocol);
		sb.append(", externalproxyhost=");
		sb.append(externalproxyhost);
		sb.append(", externalproxyport=");
		sb.append(externalproxyport);
		sb.append(", externalproxyauthid=");
		sb.append(externalproxyauthid);
		sb.append(", externalproxyauthpassword=");
		sb.append(externalproxyauthpassword);
		sb.append(", useclientcertificate=");
		sb.append(useclientcertificate);
		sb.append(", certificatefile=");
		sb.append(certificatefile);
		sb.append(", certificatefilepassword=");
		sb.append(certificatefilepassword);
		sb.append(", ntlmauthid=");
		sb.append(ntlmauthid);
		sb.append(", ntlmauthpassword=");
		sb.append(ntlmauthpassword);
		sb.append(", ntlmauthdomain=");
		sb.append(ntlmauthdomain);
		sb.append(", ntlmauthhost=");
		sb.append(ntlmauthhost);
		sb.append(", digestauthid=");
		sb.append(digestauthid);
		sb.append(", digestauthpassword=");
		sb.append(digestauthpassword);
		sb.append(", basicauthid=");
		sb.append(basicauthid);
		sb.append(", basicauthpassword=");
		sb.append(basicauthpassword);
		sb.append(", accessexclusionpath=");
		sb.append(accessexclusionpath);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public VexTargetInformation toEntityModel() {
		VexTargetInformationImpl vexTargetInformationImpl = new VexTargetInformationImpl();

		vexTargetInformationImpl.setScanid(scanid);

		if (protocol == null) {
			vexTargetInformationImpl.setProtocol(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setProtocol(protocol);
		}

		if (host == null) {
			vexTargetInformationImpl.setHost(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setHost(host);
		}

		vexTargetInformationImpl.setPort(port);
		vexTargetInformationImpl.setHttpversion(httpversion);
		vexTargetInformationImpl.setSetkeepaliveconnection(setkeepaliveconnection);
		vexTargetInformationImpl.setSetresponsecontentlength(setresponsecontentlength);
		vexTargetInformationImpl.setUseacceptencodingheader(useacceptencodingheader);
		vexTargetInformationImpl.setUnzipresponse(unzipresponse);

		if (httpprotocol == null) {
			vexTargetInformationImpl.setHttpprotocol(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setHttpprotocol(httpprotocol);
		}

		if (externalproxyhost == null) {
			vexTargetInformationImpl.setExternalproxyhost(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setExternalproxyhost(externalproxyhost);
		}

		vexTargetInformationImpl.setExternalproxyport(externalproxyport);

		if (externalproxyauthid == null) {
			vexTargetInformationImpl.setExternalproxyauthid(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setExternalproxyauthid(externalproxyauthid);
		}

		if (externalproxyauthpassword == null) {
			vexTargetInformationImpl.setExternalproxyauthpassword(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setExternalproxyauthpassword(externalproxyauthpassword);
		}

		vexTargetInformationImpl.setUseclientcertificate(useclientcertificate);

		if (certificatefile == null) {
			vexTargetInformationImpl.setCertificatefile(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setCertificatefile(certificatefile);
		}

		if (certificatefilepassword == null) {
			vexTargetInformationImpl.setCertificatefilepassword(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setCertificatefilepassword(certificatefilepassword);
		}

		if (ntlmauthid == null) {
			vexTargetInformationImpl.setNtlmauthid(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setNtlmauthid(ntlmauthid);
		}

		if (ntlmauthpassword == null) {
			vexTargetInformationImpl.setNtlmauthpassword(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setNtlmauthpassword(ntlmauthpassword);
		}

		if (ntlmauthdomain == null) {
			vexTargetInformationImpl.setNtlmauthdomain(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setNtlmauthdomain(ntlmauthdomain);
		}

		if (ntlmauthhost == null) {
			vexTargetInformationImpl.setNtlmauthhost(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setNtlmauthhost(ntlmauthhost);
		}

		if (digestauthid == null) {
			vexTargetInformationImpl.setDigestauthid(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setDigestauthid(digestauthid);
		}

		if (digestauthpassword == null) {
			vexTargetInformationImpl.setDigestauthpassword(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setDigestauthpassword(digestauthpassword);
		}

		if (basicauthid == null) {
			vexTargetInformationImpl.setBasicauthid(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setBasicauthid(basicauthid);
		}

		if (basicauthpassword == null) {
			vexTargetInformationImpl.setBasicauthpassword(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setBasicauthpassword(basicauthpassword);
		}

		if (accessexclusionpath == null) {
			vexTargetInformationImpl.setAccessexclusionpath(StringPool.BLANK);
		}
		else {
			vexTargetInformationImpl.setAccessexclusionpath(accessexclusionpath);
		}

		vexTargetInformationImpl.resetOriginalValues();

		return vexTargetInformationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		scanid = objectInput.readLong();
		protocol = objectInput.readUTF();
		host = objectInput.readUTF();

		port = objectInput.readInt();

		httpversion = objectInput.readInt();

		setkeepaliveconnection = objectInput.readInt();

		setresponsecontentlength = objectInput.readInt();

		useacceptencodingheader = objectInput.readInt();

		unzipresponse = objectInput.readInt();
		httpprotocol = objectInput.readUTF();
		externalproxyhost = objectInput.readUTF();

		externalproxyport = objectInput.readInt();
		externalproxyauthid = objectInput.readUTF();
		externalproxyauthpassword = objectInput.readUTF();

		useclientcertificate = objectInput.readInt();
		certificatefile = objectInput.readUTF();
		certificatefilepassword = objectInput.readUTF();
		ntlmauthid = objectInput.readUTF();
		ntlmauthpassword = objectInput.readUTF();
		ntlmauthdomain = objectInput.readUTF();
		ntlmauthhost = objectInput.readUTF();
		digestauthid = objectInput.readUTF();
		digestauthpassword = objectInput.readUTF();
		basicauthid = objectInput.readUTF();
		basicauthpassword = objectInput.readUTF();
		accessexclusionpath = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(scanid);

		if (protocol == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(protocol);
		}

		if (host == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(host);
		}

		objectOutput.writeInt(port);

		objectOutput.writeInt(httpversion);

		objectOutput.writeInt(setkeepaliveconnection);

		objectOutput.writeInt(setresponsecontentlength);

		objectOutput.writeInt(useacceptencodingheader);

		objectOutput.writeInt(unzipresponse);

		if (httpprotocol == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(httpprotocol);
		}

		if (externalproxyhost == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(externalproxyhost);
		}

		objectOutput.writeInt(externalproxyport);

		if (externalproxyauthid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(externalproxyauthid);
		}

		if (externalproxyauthpassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(externalproxyauthpassword);
		}

		objectOutput.writeInt(useclientcertificate);

		if (certificatefile == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(certificatefile);
		}

		if (certificatefilepassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(certificatefilepassword);
		}

		if (ntlmauthid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ntlmauthid);
		}

		if (ntlmauthpassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ntlmauthpassword);
		}

		if (ntlmauthdomain == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ntlmauthdomain);
		}

		if (ntlmauthhost == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ntlmauthhost);
		}

		if (digestauthid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(digestauthid);
		}

		if (digestauthpassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(digestauthpassword);
		}

		if (basicauthid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(basicauthid);
		}

		if (basicauthpassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(basicauthpassword);
		}

		if (accessexclusionpath == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(accessexclusionpath);
		}
	}

	public long scanid;
	public String protocol;
	public String host;
	public int port;
	public int httpversion;
	public int setkeepaliveconnection;
	public int setresponsecontentlength;
	public int useacceptencodingheader;
	public int unzipresponse;
	public String httpprotocol;
	public String externalproxyhost;
	public int externalproxyport;
	public String externalproxyauthid;
	public String externalproxyauthpassword;
	public int useclientcertificate;
	public String certificatefile;
	public String certificatefilepassword;
	public String ntlmauthid;
	public String ntlmauthpassword;
	public String ntlmauthdomain;
	public String ntlmauthhost;
	public String digestauthid;
	public String digestauthpassword;
	public String basicauthid;
	public String basicauthpassword;
	public String accessexclusionpath;
}