package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.dao.orm.custom.sql.CustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformationItem;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexTargetInformationFinder;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * VexTargetInformationFinder Implemtation
 */
public class VexTargetInformationFinderImpl extends VexTargetInformationFinderBaseImpl implements VexTargetInformationFinder {

	/** UBSPortalDebugger */ 
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexTargetInformationFinderImpl.class);

	/** GET_TARGET_INFORMATIONS */
	private static final String GET_TARGET_INFORMATIONS = VexTargetInformationFinder.class.getName() + ".getTargetInformations";
	
	/**
	 * Get Target Information
	 * 
	 * @param scanId
	 *            unique id of the scan
	 * @return List of vex target information
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getTargetInformations(long scanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> targetInformationList = null;
		List<Object> resultList = null;
		VexTargetInformationItem targetInformation = null;
		String serializeString = null;
		JSONArray jsonArray = null;
		Session session = null;
		String sql = "";
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (scanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}
			session = openSession();
			sql = CustomSQLUtil.get(getClass(), GET_TARGET_INFORMATIONS);

			sqlQuery = session.createSQLQuery(sql);
			sqlQuery.addScalar("scanid", Type.LONG);
			sqlQuery.addScalar("protocol", Type.STRING);
			sqlQuery.addScalar("host", Type.STRING);
			sqlQuery.addScalar("port", Type.INTEGER);
			sqlQuery.addScalar("httpversion", Type.INTEGER);
			sqlQuery.addScalar("setkeepaliveconnection", Type.INTEGER);
			sqlQuery.addScalar("setresponsecontentlength", Type.INTEGER);
			sqlQuery.addScalar("useacceptencodingheader", Type.INTEGER);
			sqlQuery.addScalar("unzipresponse", Type.INTEGER);
			sqlQuery.addScalar("httpprotocol", Type.STRING);
			sqlQuery.addScalar("externalproxyhost", Type.STRING);
			sqlQuery.addScalar("externalproxyport", Type.INTEGER);
			sqlQuery.addScalar("externalproxyauthid", Type.STRING);
			sqlQuery.addScalar("externalproxyauthpassword", Type.STRING);
			sqlQuery.addScalar("useclientcertificate", Type.INTEGER);
			sqlQuery.addScalar("certificatefile", Type.STRING);
			sqlQuery.addScalar("certificatefilepassword", Type.STRING);
			sqlQuery.addScalar("ntlmauthid", Type.STRING);
			sqlQuery.addScalar("ntlmauthpassword", Type.STRING);
			sqlQuery.addScalar("ntlmauthdomain", Type.STRING);
			sqlQuery.addScalar("ntlmauthhost", Type.STRING);
			sqlQuery.addScalar("digestauthid", Type.STRING);
			sqlQuery.addScalar("digestauthpassword", Type.STRING);
			sqlQuery.addScalar("basicauthid", Type.STRING);
			sqlQuery.addScalar("basicauthpassword", Type.STRING);
			sqlQuery.addScalar("accessexclusionpath", Type.STRING);

			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(scanId);
			
			resultList = (List<Object>) sqlQuery.list();
			
			if (!CommonUtil.isListNullOrEmpty(resultList)) {
				targetInformationList = new ArrayList<Object>();
				for (Object item : resultList) {
					serializeString = JSONFactoryUtil.serialize(item);
					jsonArray = JSONFactoryUtil.createJSONArray(serializeString);
					targetInformation = new VexTargetInformationItem();
					
					targetInformation.setScanid(jsonArray.getLong(0));
					targetInformation.setProtocol(jsonArray.getString(1));
					targetInformation.setHost(jsonArray.getString(2));
					targetInformation.setPort(String.valueOf(jsonArray.getInt(3)));
					targetInformation.setHttpversion(jsonArray.getInt(4));
					targetInformation.setSetkeepaliveconnection(jsonArray.getInt(5));
					targetInformation.setSetresponsecontentlength(jsonArray.getInt(6));
					targetInformation.setUseacceptencodingheader(jsonArray.getInt(7));
					targetInformation.setUnzipresponse(jsonArray.getInt(8));
					targetInformation.setHttpprotocol(jsonArray.getString(9));
					targetInformation.setExternalproxyhost(jsonArray.getString(10));
					targetInformation.setExternalproxyport(jsonArray.getInt(11));
					targetInformation.setExternalproxyauthid(jsonArray.getString(12));
					targetInformation.setExternalproxyauthpassword(jsonArray.getString(13));
					targetInformation.setUseclientcertificate(jsonArray.getInt(14));
					targetInformation.setCertificatefile(jsonArray.getString(15));
					targetInformation.setCertificatefilepassword(jsonArray.getString(16));
					targetInformation.setNtlmauthid(jsonArray.getString(17));
					targetInformation.setNtlmauthpassword(jsonArray.getString(18));
					targetInformation.setNtlmauthdomain(jsonArray.getString(19));
					targetInformation.setNtlmauthhost(jsonArray.getString(20));
					targetInformation.setDigestauthid(jsonArray.getString(21));
					targetInformation.setDigestauthpassword(jsonArray.getString(22));
					targetInformation.setBasicauthid(jsonArray.getString(23));
					targetInformation.setBasicauthpassword(jsonArray.getString(24));
					targetInformation.setAccessexclusionpath(jsonArray.getString(25));
					
					targetInformationList.add(targetInformation);
				}
			}
		} catch (ORMException orme) {
			params.put("scanid", scanId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_TARGET_INFORMATIONS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("scanid", scanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_TARGET_INFORMATIONS, params, pe);
			throw pe;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return targetInformationList;
	}
	
}