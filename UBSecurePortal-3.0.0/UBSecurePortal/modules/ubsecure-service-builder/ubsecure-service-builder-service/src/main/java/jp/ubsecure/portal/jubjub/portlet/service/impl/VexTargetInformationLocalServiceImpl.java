/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;

import aQute.bnd.annotation.ProviderType;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexTargetInformationImpl;
import jp.ubsecure.portal.jubjub.portlet.service.base.VexTargetInformationLocalServiceBaseImpl;

/**
 * The implementation of the vex target information local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link jp.ubsecure.portal.jubjub.portlet.service.VexTargetInformationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexTargetInformationLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.VexTargetInformationLocalServiceUtil
 */
@ProviderType
public class VexTargetInformationLocalServiceImpl extends VexTargetInformationLocalServiceBaseImpl {
	
	/** UBSPortalDebugger */
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexTargetInformationLocalServiceImpl.class);

	@Override
	public VexTargetInformation createVexTargetInformationObj() {
		return new VexTargetInformationImpl();
	}

	@Override
	public List<Object> getTargetInformations(long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> targetInformationList = null;

		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}
			targetInformationList = vexTargetInformationFinder.getTargetInformations(lScanId);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_TARGET_INFORMATIONS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return targetInformationList;
	}

}