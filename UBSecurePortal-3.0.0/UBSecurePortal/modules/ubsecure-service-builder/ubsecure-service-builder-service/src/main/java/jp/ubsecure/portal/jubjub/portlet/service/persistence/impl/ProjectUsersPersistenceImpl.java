/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectUsersException;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersImpl;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectUsersModelImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the project users service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProjectUsersPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersUtil
 * @generated
 */
@ProviderType
public class ProjectUsersPersistenceImpl extends BasePersistenceImpl<ProjectUsers>
	implements ProjectUsersPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProjectUsersUtil} to access the project users persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProjectUsersImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, ProjectUsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, ProjectUsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTID =
		new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, ProjectUsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByProjectId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID =
		new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, ProjectUsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByProjectId",
			new String[] { Long.class.getName() },
			ProjectUsersModelImpl.PROJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTID = new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByProjectId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the project userses where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @return the matching project userses
	 */
	@Override
	public List<ProjectUsers> findByProjectId(long projectId) {
		return findByProjectId(projectId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the project userses where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @return the range of matching project userses
	 */
	@Override
	public List<ProjectUsers> findByProjectId(long projectId, int start, int end) {
		return findByProjectId(projectId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project userses where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project userses
	 */
	@Override
	public List<ProjectUsers> findByProjectId(long projectId, int start,
		int end, OrderByComparator<ProjectUsers> orderByComparator) {
		return findByProjectId(projectId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project userses where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project userses
	 */
	@Override
	public List<ProjectUsers> findByProjectId(long projectId, int start,
		int end, OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID;
			finderArgs = new Object[] { projectId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTID;
			finderArgs = new Object[] { projectId, start, end, orderByComparator };
		}

		List<ProjectUsers> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectUsers>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectUsers projectUsers : list) {
					if ((projectId != projectUsers.getProjectId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTUSERS_WHERE);

			query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectUsersModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				if (!pagination) {
					list = (List<ProjectUsers>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectUsers>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project users in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project users
	 * @throws NoSuchProjectUsersException if a matching project users could not be found
	 */
	@Override
	public ProjectUsers findByProjectId_First(long projectId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException {
		ProjectUsers projectUsers = fetchByProjectId_First(projectId,
				orderByComparator);

		if (projectUsers != null) {
			return projectUsers;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectUsersException(msg.toString());
	}

	/**
	 * Returns the first project users in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project users, or <code>null</code> if a matching project users could not be found
	 */
	@Override
	public ProjectUsers fetchByProjectId_First(long projectId,
		OrderByComparator<ProjectUsers> orderByComparator) {
		List<ProjectUsers> list = findByProjectId(projectId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project users in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project users
	 * @throws NoSuchProjectUsersException if a matching project users could not be found
	 */
	@Override
	public ProjectUsers findByProjectId_Last(long projectId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException {
		ProjectUsers projectUsers = fetchByProjectId_Last(projectId,
				orderByComparator);

		if (projectUsers != null) {
			return projectUsers;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectUsersException(msg.toString());
	}

	/**
	 * Returns the last project users in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project users, or <code>null</code> if a matching project users could not be found
	 */
	@Override
	public ProjectUsers fetchByProjectId_Last(long projectId,
		OrderByComparator<ProjectUsers> orderByComparator) {
		int count = countByProjectId(projectId);

		if (count == 0) {
			return null;
		}

		List<ProjectUsers> list = findByProjectId(projectId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project userses before and after the current project users in the ordered set where projectId = &#63;.
	 *
	 * @param projectUsersPK the primary key of the current project users
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project users
	 * @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	 */
	@Override
	public ProjectUsers[] findByProjectId_PrevAndNext(
		ProjectUsersPK projectUsersPK, long projectId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException {
		ProjectUsers projectUsers = findByPrimaryKey(projectUsersPK);

		Session session = null;

		try {
			session = openSession();

			ProjectUsers[] array = new ProjectUsersImpl[3];

			array[0] = getByProjectId_PrevAndNext(session, projectUsers,
					projectId, orderByComparator, true);

			array[1] = projectUsers;

			array[2] = getByProjectId_PrevAndNext(session, projectUsers,
					projectId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectUsers getByProjectId_PrevAndNext(Session session,
		ProjectUsers projectUsers, long projectId,
		OrderByComparator<ProjectUsers> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTUSERS_WHERE);

		query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectUsersModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(projectId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectUsers);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectUsers> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project userses where projectId = &#63; from the database.
	 *
	 * @param projectId the project ID
	 */
	@Override
	public void removeByProjectId(long projectId) {
		for (ProjectUsers projectUsers : findByProjectId(projectId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectUsers);
		}
	}

	/**
	 * Returns the number of project userses where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @return the number of matching project userses
	 */
	@Override
	public int countByProjectId(long projectId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTID;

		Object[] finderArgs = new Object[] { projectId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTUSERS_WHERE);

			query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTID_PROJECTID_2 = "projectUsers.id.projectId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, ProjectUsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserId",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, ProjectUsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserId",
			new String[] { String.class.getName() },
			ProjectUsersModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { String.class.getName() });

	/**
	 * Returns all the project userses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching project userses
	 */
	@Override
	public List<ProjectUsers> findByUserId(String userId) {
		return findByUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project userses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @return the range of matching project userses
	 */
	@Override
	public List<ProjectUsers> findByUserId(String userId, int start, int end) {
		return findByUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project userses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project userses
	 */
	@Override
	public List<ProjectUsers> findByUserId(String userId, int start, int end,
		OrderByComparator<ProjectUsers> orderByComparator) {
		return findByUserId(userId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project userses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project userses
	 */
	@Override
	public List<ProjectUsers> findByUserId(String userId, int start, int end,
		OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<ProjectUsers> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectUsers>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectUsers projectUsers : list) {
					if (!Objects.equals(userId, projectUsers.getUserId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTUSERS_WHERE);

			boolean bindUserId = false;

			if (userId == null) {
				query.append(_FINDER_COLUMN_USERID_USERID_1);
			}
			else if (userId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERID_USERID_3);
			}
			else {
				bindUserId = true;

				query.append(_FINDER_COLUMN_USERID_USERID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectUsersModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUserId) {
					qPos.add(userId);
				}

				if (!pagination) {
					list = (List<ProjectUsers>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectUsers>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project users in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project users
	 * @throws NoSuchProjectUsersException if a matching project users could not be found
	 */
	@Override
	public ProjectUsers findByUserId_First(String userId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException {
		ProjectUsers projectUsers = fetchByUserId_First(userId,
				orderByComparator);

		if (projectUsers != null) {
			return projectUsers;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectUsersException(msg.toString());
	}

	/**
	 * Returns the first project users in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project users, or <code>null</code> if a matching project users could not be found
	 */
	@Override
	public ProjectUsers fetchByUserId_First(String userId,
		OrderByComparator<ProjectUsers> orderByComparator) {
		List<ProjectUsers> list = findByUserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project users in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project users
	 * @throws NoSuchProjectUsersException if a matching project users could not be found
	 */
	@Override
	public ProjectUsers findByUserId_Last(String userId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException {
		ProjectUsers projectUsers = fetchByUserId_Last(userId, orderByComparator);

		if (projectUsers != null) {
			return projectUsers;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectUsersException(msg.toString());
	}

	/**
	 * Returns the last project users in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project users, or <code>null</code> if a matching project users could not be found
	 */
	@Override
	public ProjectUsers fetchByUserId_Last(String userId,
		OrderByComparator<ProjectUsers> orderByComparator) {
		int count = countByUserId(userId);

		if (count == 0) {
			return null;
		}

		List<ProjectUsers> list = findByUserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project userses before and after the current project users in the ordered set where userId = &#63;.
	 *
	 * @param projectUsersPK the primary key of the current project users
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project users
	 * @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	 */
	@Override
	public ProjectUsers[] findByUserId_PrevAndNext(
		ProjectUsersPK projectUsersPK, String userId,
		OrderByComparator<ProjectUsers> orderByComparator)
		throws NoSuchProjectUsersException {
		ProjectUsers projectUsers = findByPrimaryKey(projectUsersPK);

		Session session = null;

		try {
			session = openSession();

			ProjectUsers[] array = new ProjectUsersImpl[3];

			array[0] = getByUserId_PrevAndNext(session, projectUsers, userId,
					orderByComparator, true);

			array[1] = projectUsers;

			array[2] = getByUserId_PrevAndNext(session, projectUsers, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectUsers getByUserId_PrevAndNext(Session session,
		ProjectUsers projectUsers, String userId,
		OrderByComparator<ProjectUsers> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTUSERS_WHERE);

		boolean bindUserId = false;

		if (userId == null) {
			query.append(_FINDER_COLUMN_USERID_USERID_1);
		}
		else if (userId.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_USERID_USERID_3);
		}
		else {
			bindUserId = true;

			query.append(_FINDER_COLUMN_USERID_USERID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectUsersModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUserId) {
			qPos.add(userId);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectUsers);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectUsers> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project userses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	@Override
	public void removeByUserId(String userId) {
		for (ProjectUsers projectUsers : findByUserId(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectUsers);
		}
	}

	/**
	 * Returns the number of project userses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching project userses
	 */
	@Override
	public int countByUserId(String userId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTUSERS_WHERE);

			boolean bindUserId = false;

			if (userId == null) {
				query.append(_FINDER_COLUMN_USERID_USERID_1);
			}
			else if (userId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERID_USERID_3);
			}
			else {
				bindUserId = true;

				query.append(_FINDER_COLUMN_USERID_USERID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUserId) {
					qPos.add(userId);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_1 = "projectUsers.id.userId IS NULL";
	private static final String _FINDER_COLUMN_USERID_USERID_2 = "projectUsers.id.userId = ?";
	private static final String _FINDER_COLUMN_USERID_USERID_3 = "(projectUsers.id.userId IS NULL OR projectUsers.id.userId = '')";

	public ProjectUsersPersistenceImpl() {
		setModelClass(ProjectUsers.class);
	}

	/**
	 * Caches the project users in the entity cache if it is enabled.
	 *
	 * @param projectUsers the project users
	 */
	@Override
	public void cacheResult(ProjectUsers projectUsers) {
		entityCache.putResult(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersImpl.class, projectUsers.getPrimaryKey(), projectUsers);

		projectUsers.resetOriginalValues();
	}

	/**
	 * Caches the project userses in the entity cache if it is enabled.
	 *
	 * @param projectUserses the project userses
	 */
	@Override
	public void cacheResult(List<ProjectUsers> projectUserses) {
		for (ProjectUsers projectUsers : projectUserses) {
			if (entityCache.getResult(
						ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
						ProjectUsersImpl.class, projectUsers.getPrimaryKey()) == null) {
				cacheResult(projectUsers);
			}
			else {
				projectUsers.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all project userses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProjectUsersImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the project users.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProjectUsers projectUsers) {
		entityCache.removeResult(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersImpl.class, projectUsers.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ProjectUsers> projectUserses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProjectUsers projectUsers : projectUserses) {
			entityCache.removeResult(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
				ProjectUsersImpl.class, projectUsers.getPrimaryKey());
		}
	}

	/**
	 * Creates a new project users with the primary key. Does not add the project users to the database.
	 *
	 * @param projectUsersPK the primary key for the new project users
	 * @return the new project users
	 */
	@Override
	public ProjectUsers create(ProjectUsersPK projectUsersPK) {
		ProjectUsers projectUsers = new ProjectUsersImpl();

		projectUsers.setNew(true);
		projectUsers.setPrimaryKey(projectUsersPK);

		return projectUsers;
	}

	/**
	 * Removes the project users with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param projectUsersPK the primary key of the project users
	 * @return the project users that was removed
	 * @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	 */
	@Override
	public ProjectUsers remove(ProjectUsersPK projectUsersPK)
		throws NoSuchProjectUsersException {
		return remove((Serializable)projectUsersPK);
	}

	/**
	 * Removes the project users with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the project users
	 * @return the project users that was removed
	 * @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	 */
	@Override
	public ProjectUsers remove(Serializable primaryKey)
		throws NoSuchProjectUsersException {
		Session session = null;

		try {
			session = openSession();

			ProjectUsers projectUsers = (ProjectUsers)session.get(ProjectUsersImpl.class,
					primaryKey);

			if (projectUsers == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProjectUsersException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(projectUsers);
		}
		catch (NoSuchProjectUsersException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProjectUsers removeImpl(ProjectUsers projectUsers) {
		projectUsers = toUnwrappedModel(projectUsers);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(projectUsers)) {
				projectUsers = (ProjectUsers)session.get(ProjectUsersImpl.class,
						projectUsers.getPrimaryKeyObj());
			}

			if (projectUsers != null) {
				session.delete(projectUsers);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (projectUsers != null) {
			clearCache(projectUsers);
		}

		return projectUsers;
	}

	@Override
	public ProjectUsers updateImpl(ProjectUsers projectUsers) {
		projectUsers = toUnwrappedModel(projectUsers);

		boolean isNew = projectUsers.isNew();

		ProjectUsersModelImpl projectUsersModelImpl = (ProjectUsersModelImpl)projectUsers;

		Session session = null;

		try {
			session = openSession();

			if (projectUsers.isNew()) {
				session.save(projectUsers);

				projectUsers.setNew(false);
			}
			else {
				projectUsers = (ProjectUsers)session.merge(projectUsers);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProjectUsersModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((projectUsersModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectUsersModelImpl.getOriginalProjectId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID,
					args);

				args = new Object[] { projectUsersModelImpl.getProjectId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID,
					args);
			}

			if ((projectUsersModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectUsersModelImpl.getOriginalUserId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { projectUsersModelImpl.getUserId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		entityCache.putResult(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
			ProjectUsersImpl.class, projectUsers.getPrimaryKey(), projectUsers,
			false);

		projectUsers.resetOriginalValues();

		return projectUsers;
	}

	protected ProjectUsers toUnwrappedModel(ProjectUsers projectUsers) {
		if (projectUsers instanceof ProjectUsersImpl) {
			return projectUsers;
		}

		ProjectUsersImpl projectUsersImpl = new ProjectUsersImpl();

		projectUsersImpl.setNew(projectUsers.isNew());
		projectUsersImpl.setPrimaryKey(projectUsers.getPrimaryKey());

		projectUsersImpl.setUserId(projectUsers.getUserId());
		projectUsersImpl.setProjectId(projectUsers.getProjectId());

		return projectUsersImpl;
	}

	/**
	 * Returns the project users with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the project users
	 * @return the project users
	 * @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	 */
	@Override
	public ProjectUsers findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProjectUsersException {
		ProjectUsers projectUsers = fetchByPrimaryKey(primaryKey);

		if (projectUsers == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProjectUsersException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return projectUsers;
	}

	/**
	 * Returns the project users with the primary key or throws a {@link NoSuchProjectUsersException} if it could not be found.
	 *
	 * @param projectUsersPK the primary key of the project users
	 * @return the project users
	 * @throws NoSuchProjectUsersException if a project users with the primary key could not be found
	 */
	@Override
	public ProjectUsers findByPrimaryKey(ProjectUsersPK projectUsersPK)
		throws NoSuchProjectUsersException {
		return findByPrimaryKey((Serializable)projectUsersPK);
	}

	/**
	 * Returns the project users with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the project users
	 * @return the project users, or <code>null</code> if a project users with the primary key could not be found
	 */
	@Override
	public ProjectUsers fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
				ProjectUsersImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ProjectUsers projectUsers = (ProjectUsers)serializable;

		if (projectUsers == null) {
			Session session = null;

			try {
				session = openSession();

				projectUsers = (ProjectUsers)session.get(ProjectUsersImpl.class,
						primaryKey);

				if (projectUsers != null) {
					cacheResult(projectUsers);
				}
				else {
					entityCache.putResult(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
						ProjectUsersImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProjectUsersModelImpl.ENTITY_CACHE_ENABLED,
					ProjectUsersImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return projectUsers;
	}

	/**
	 * Returns the project users with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param projectUsersPK the primary key of the project users
	 * @return the project users, or <code>null</code> if a project users with the primary key could not be found
	 */
	@Override
	public ProjectUsers fetchByPrimaryKey(ProjectUsersPK projectUsersPK) {
		return fetchByPrimaryKey((Serializable)projectUsersPK);
	}

	@Override
	public Map<Serializable, ProjectUsers> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ProjectUsers> map = new HashMap<Serializable, ProjectUsers>();

		for (Serializable primaryKey : primaryKeys) {
			ProjectUsers projectUsers = fetchByPrimaryKey(primaryKey);

			if (projectUsers != null) {
				map.put(primaryKey, projectUsers);
			}
		}

		return map;
	}

	/**
	 * Returns all the project userses.
	 *
	 * @return the project userses
	 */
	@Override
	public List<ProjectUsers> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project userses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @return the range of project userses
	 */
	@Override
	public List<ProjectUsers> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the project userses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of project userses
	 */
	@Override
	public List<ProjectUsers> findAll(int start, int end,
		OrderByComparator<ProjectUsers> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project userses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectUsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project userses
	 * @param end the upper bound of the range of project userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of project userses
	 */
	@Override
	public List<ProjectUsers> findAll(int start, int end,
		OrderByComparator<ProjectUsers> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProjectUsers> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectUsers>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PROJECTUSERS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROJECTUSERS;

				if (pagination) {
					sql = sql.concat(ProjectUsersModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProjectUsers>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectUsers>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the project userses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ProjectUsers projectUsers : findAll()) {
			remove(projectUsers);
		}
	}

	/**
	 * Returns the number of project userses.
	 *
	 * @return the number of project userses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROJECTUSERS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProjectUsersModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the project users persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ProjectUsersImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PROJECTUSERS = "SELECT projectUsers FROM ProjectUsers projectUsers";
	private static final String _SQL_SELECT_PROJECTUSERS_WHERE = "SELECT projectUsers FROM ProjectUsers projectUsers WHERE ";
	private static final String _SQL_COUNT_PROJECTUSERS = "SELECT COUNT(projectUsers) FROM ProjectUsers projectUsers";
	private static final String _SQL_COUNT_PROJECTUSERS_WHERE = "SELECT COUNT(projectUsers) FROM ProjectUsers projectUsers WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "projectUsers.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProjectUsers exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProjectUsers exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProjectUsersPersistenceImpl.class);
}