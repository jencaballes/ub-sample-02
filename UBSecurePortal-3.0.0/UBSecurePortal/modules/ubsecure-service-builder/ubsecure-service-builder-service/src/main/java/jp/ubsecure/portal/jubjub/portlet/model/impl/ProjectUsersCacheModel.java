/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectUsersPK;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ProjectUsers in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ProjectUsers
 * @generated
 */
@ProviderType
public class ProjectUsersCacheModel implements CacheModel<ProjectUsers>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectUsersCacheModel)) {
			return false;
		}

		ProjectUsersCacheModel projectUsersCacheModel = (ProjectUsersCacheModel)obj;

		if (projectUsersPK.equals(projectUsersCacheModel.projectUsersPK)) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, projectUsersPK);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{userId=");
		sb.append(userId);
		sb.append(", projectId=");
		sb.append(projectId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProjectUsers toEntityModel() {
		ProjectUsersImpl projectUsersImpl = new ProjectUsersImpl();

		if (userId == null) {
			projectUsersImpl.setUserId(StringPool.BLANK);
		}
		else {
			projectUsersImpl.setUserId(userId);
		}

		projectUsersImpl.setProjectId(projectId);

		projectUsersImpl.resetOriginalValues();

		return projectUsersImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		userId = objectInput.readUTF();

		projectId = objectInput.readLong();

		projectUsersPK = new ProjectUsersPK(userId, projectId);
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (userId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userId);
		}

		objectOutput.writeLong(projectId);
	}

	public String userId;
	public long projectId;
	public transient ProjectUsersPK projectUsersPK;
}