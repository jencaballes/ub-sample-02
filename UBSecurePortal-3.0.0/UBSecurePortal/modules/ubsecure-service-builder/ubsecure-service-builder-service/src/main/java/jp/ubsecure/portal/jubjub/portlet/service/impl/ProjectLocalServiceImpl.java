/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.StringUtil;

import aQute.bnd.annotation.ProviderType;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ScanItem;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectImpl;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.base.ProjectLocalServiceBaseImpl;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * The implementation of the project local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalService}
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author ASI
 * @see ProjectLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil
 */
@ProviderType
public class ProjectLocalServiceImpl extends ProjectLocalServiceBaseImpl {

	/** UBSPortalDebugger */
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ProjectLocalServiceImpl.class);

	/**
	 * Retrieve Project List by owner group.
	 * 
	 * @param ownerGroup
	 *            owner group
	 * @throws PortalException
	 *             Exception thrown
	 * @return List<Project> of Projects
	 */
	public List<Project> getProjectsByOwnerGroup(long ownerGroup) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Project> projectList = new ArrayList<Project>();

		try {
			projectPersistence.clearCache();
			projectList = projectPersistence.findByOwnerGroup(ownerGroup);
		} catch (SystemException e) {
			params.put("ownerGroup", ownerGroup);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS_BY_OWNER_GROUP, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} finally {
			params.clear();
			params = null;
		}

		return projectList;
	}

	/**
	 * Retrieve Project List by owner groups.
	 * 
	 * @param ownerGroupList
	 *            owner group list
	 * @throws PortalException
	 *             Exception thrown
	 * @return List<Project> of Projects
	 */
	public List<Project> getProjectsByOwnerGroupList(long[] ownerGroupList) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Project> tempProjectList = new ArrayList<Project>();
		List<Project> projectList = new ArrayList<Project>();

		try {
			projectPersistence.clearCache();
			for (long ownerGroup : ownerGroupList) {
				tempProjectList = projectPersistence.findByOwnerGroup(ownerGroup);
				for (Project project : tempProjectList) {
					if (!projectList.contains(project)) {
						projectList.add(project); 
		            }
				}
			}
		} catch (SystemException e) {
			params.put("ownerGroup", ownerGroupList);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS_BY_OWNER_GROUP, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} finally {
			params.clear();
			params = null;
		}

		return projectList;
	}
	
	/**
	 * Retrieve project list.
	 * 
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search.
	 * @param user
	 *            login user
	 * @param type
	 *            type of the project (android, cxsuite, ios)
	 * @param start
	 *            index on where the search begin
	 * @return projectList list of projects
	 * @throws PortalException
	 *             - exception thrown
	 */
	public List<Object> getProjects(Map<String, Object> searchedProject, User user, int type, int start)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Role> roleList = new ArrayList<Role>();
		int iUserRole = 0;
		String userId = null;
		String strGroupName = null;
		String strCaseNumber = null;
		String strProjectName = null;

		try {
			if (user == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}

			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			roleList = user.getRoles();

			for (Role r : roleList) {
				if (r.getName().equals(PortalConstants.SYSTEM_ADMINISTRATOR)) {
					iUserRole = UserRole.SYSTEM_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.OVERALL_ADMINISTRATOR)) {
					iUserRole = UserRole.OVERALL_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.GROUP_ADMINISTRATOR)) {
					iUserRole = UserRole.GROUP_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.GENERAL_USER)) {
					iUserRole = UserRole.GEN_USER.getInteger();
					break;
				}
			}

			if (iUserRole == UserRole.SYSTEM_ADMIN.getInteger() || iUserRole == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			} else {
				if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
					Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);

					if (!CommonUtil.isObjectNull(oGroupName)) {
						strGroupName = oGroupName.toString();

						if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
							strGroupName = StringUtil.replace(strGroupName, "\\", "\\\\");
							strGroupName = StringUtil.replace(strGroupName, "%", PortalConstants.PERCENT);
							searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
						}
					}

					Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);

					if (!CommonUtil.isObjectNull(oCaseNumber)) {
						strCaseNumber = oCaseNumber.toString();

						if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
							strCaseNumber = StringUtil.replace(strCaseNumber, "\\", "\\\\");
							strCaseNumber = StringUtil.replace(strCaseNumber, "%", PortalConstants.PERCENT);
							searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, strCaseNumber);
						}
					}

					Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);

					if (!CommonUtil.isObjectNull(oProjectName)) {
						strProjectName = oProjectName.toString();

						if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
							strProjectName = StringUtil.replace(strProjectName, "\\", "\\\\");
							strProjectName = StringUtil.replace(strProjectName, "%", PortalConstants.PERCENT);
							searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
						}
					}
				}

				if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
					projectList = getOverallAdminProjects(user.getUserId(), iUserRole, type, searchedProject, start,
							PortalConstants.PAGINATION_DELTA);
				} else {
					userId = user.getEmailAddress();

					projectList = getUserProjects(type, userId, searchedProject, start,
							PortalConstants.PAGINATION_DELTA);
				}
			}
		} catch (SystemException se) {
			params.put("searchedProject", searchedProject);
			params.put("user", user);
			params.put("type", type);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("searchedProject", searchedProject);
			params.put("user", user);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!roleList.isEmpty()) {
				roleList.clear();
				roleList = null;
			}

			params.clear();
			params = null;
		}

		return projectList;
	}

	/**
	 * Get the list of overall admin projects.
	 * 
	 * @param type
	 *            a type of a project(android, cxsuite, ios)
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search.
	 * @param start
	 *            index for the start position
	 * @param end
	 *            index for the end position
	 * @return projectList List of overall admin projects
	 * @throws PortalException
	 *             an exception thrown
	 */
	private List<Object> getOverallAdminProjects(long lUserId, int iUserRole, int type, Map<String, Object> searchedProject, int start, int end)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();

		try {
			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			projectList = projectFinder.getOverallAdminProjects(lUserId, iUserRole, type, searchedProject, start, end);
		} catch (PortalException pe) {
			params.put("type", type);
			params.put("searchedProject", searchedProject);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_OVERALL_ADMIN_PROJECTS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return projectList;
	}

	/**
	 * Get the list of User projects.
	 * 
	 * @param type
	 *            a type of a project(android, cxsuite, ios)
	 * @param userId
	 *            ID of the user of which the projects belong
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param start
	 *            index for the start position
	 * @param end
	 *            index for the end position
	 * @return projectList List of projects of the user
	 * @throws PortalException
	 *             an exception thrown
	 */
	private List<Object> getUserProjects(int type, String userId, Map<String, Object> searchedProject, int start,
			int end) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();

		try {
			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (userId == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}

			projectList = projectFinder.getUserProjects(type, userId, searchedProject, start, end);
		} catch (PortalException pe) {
			params.put("type", type);
			params.put("userId", userId);
			params.put("searchedProject", searchedProject);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_USER_PROJECTS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return projectList;
	}

	/**
	 * Get number of projects.
	 * 
	 * @param type
	 *            the type of project(android, cxsuite, ios)
	 * @return count - number of projects
	 * @throws PortalException
	 *             - an exception thrown
	 */
	public int getProjectsCount(int type) throws PortalException {
		int iCount = PortalConstants.INT_ZERO;

		try {
			iCount = projectFinder.getProjectsCount(type);
		} catch (PortalException e) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("type", type);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_PROJECTS_COUNT, params, e);
			throw e;
		}

		return iCount;
	}

	/**
	 * Get the number of projects per user
	 * 
	 * @param userId
	 *            - ID of the User
	 * @param type
	 *            - type of the project(android, cxsuite, ios)
	 * @return count - number of projects
	 * @throws PortalException
	 *             - an exception thrown
	 */
	public int getUserProjectsCount(String userId, int type) throws PortalException {
		int iCount = PortalConstants.INT_ZERO;

		try {
			iCount = projectFinder.getUserProjectsCount(userId, type);
		} catch (PortalException e) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			params.put("type", type);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_USER_PROJECTS_COUNT, params, e);
			throw e;
		}
		return iCount;
	}

	/**
	 * Get the number of projects
	 * 
	 * @param type
	 *            - type of the project(android, cxsuite, ios)
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @return count - number of projects
	 */
	public int getSearchedProjectsCount(int type, Map<String, Object> searchedProject, User user) throws PortalException {
		int iCount = PortalConstants.INT_ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		List<Role> roleList = new ArrayList<Role>();
		int iUserRole = 0;
		
		try {
			if (user == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}
			
			roleList = user.getRoles();

			for (Role r : roleList) {
				if (r.getName().equals(PortalConstants.SYSTEM_ADMINISTRATOR)) {
					iUserRole = UserRole.SYSTEM_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.OVERALL_ADMINISTRATOR)) {
					iUserRole = UserRole.OVERALL_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.GROUP_ADMINISTRATOR)) {
					iUserRole = UserRole.GROUP_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.GENERAL_USER)) {
					iUserRole = UserRole.GEN_USER.getInteger();
					break;
				}
			}

			if (iUserRole == UserRole.SYSTEM_ADMIN.getInteger() || iUserRole == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}
			
			iCount = projectFinder.getSearchedProjectsCount(user.getUserId(), iUserRole, type, searchedProject);
		} catch (PortalException e) {
			params.put("type", type);
			params.put("searchedProject", searchedProject);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_SEARCHED_PROJECTS_COUNT, params, e);
			throw e;
		}

		return iCount;
	}

	/**
	 * Get the number of projects
	 * 
	 * @param userId
	 *            ID of the user
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @return PortalException - an exception thrown
	 */
	public int getSearchedUserProjectsCount(String userId, int type, Map<String, Object> searchedProject)
			throws PortalException {
		int iCount = PortalConstants.INT_ZERO;

		try {
			iCount = projectFinder.getSearchedUserProjectsCount(userId, type, searchedProject);
		} catch (PortalException e) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			params.put("type", type);
			params.put("searchedProject", searchedProject);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_SEARCHED_USER_PROJECTS_COUNT, params, e);
			throw e;
		}

		return iCount;
	}

	/**
	 * Sort overall Projects
	 * 
	 * @param lUserId
	 user id
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param orderByCol
	 *            - a column to order
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @param start
	 *            index for the start position
	 * @param end
	 *            index for the end position
	 * @return projectList a sorted project list
	 * @throws PortalException
	 *             an exception thrown
	 */
	public List<Object> sortOverallProjects(long lUserId, int iUserRole, Map<String, Object> searchedProject, String orderByType, String orderByCol,
			int type, int start, int end) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Role> roleList = new ArrayList<Role>();
		String strGroupName = null;
		String strCaseNumber = null;
		String strProjectName = null;

		try {
			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);

				if (!CommonUtil.isObjectNull(oGroupName)) {
					strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						strGroupName = StringUtil.replace(strGroupName, "\\", "\\\\");
						strGroupName = StringUtil.replace(strGroupName, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					}
				}

				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						strCaseNumber = StringUtil.replace(strCaseNumber, "\\", "\\\\");
						strCaseNumber = StringUtil.replace(strCaseNumber, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, strCaseNumber);
					}
				}

				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);

				if (!CommonUtil.isObjectNull(oProjectName)) {
					strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						strProjectName = StringUtil.replace(strProjectName, "\\", "\\\\");
						strProjectName = StringUtil.replace(strProjectName, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					}
				}
			}

			projectList = projectFinder.sortOverallAdminProjects(lUserId, iUserRole, type, searchedProject, orderByCol, orderByType, start, end);

		} catch (PortalException pe) {
			params.put("searchedProject", searchedProject);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!roleList.isEmpty()) {
				roleList.clear();
				roleList = null;
			}

			params.clear();
			params = null;
		}

		return projectList;
	}

	/**
	 * Sort User Projects
	 * 
	 * @param strUserId
	 *            - User ID
	 * 
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param orderByCol
	 *            - a column to order
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @param start
	 *            index for the start position
	 * @param end
	 *            index for the end position
	 * @return projectList a sorted project list
	 * @throws PortalException
	 *             an exception thrown
	 */
	public List<Object> sortUserProjects(String strUserId, Map<String, Object> searchedProject, String orderByType,
			String orderByCol, int type, int start, int end) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Role> roleList = new ArrayList<Role>();
		String strGroupName = null;
		String strCaseNumber = null;
		String strProjectName = null;

		try {
			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);

				if (!CommonUtil.isObjectNull(oGroupName)) {
					strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						strGroupName = StringUtil.replace(strGroupName, "\\", "\\\\");
						strGroupName = StringUtil.replace(strGroupName, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					}
				}

				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						strCaseNumber = StringUtil.replace(strCaseNumber, "\\", "\\\\");
						strCaseNumber = StringUtil.replace(strCaseNumber, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, strCaseNumber);
					}
				}

				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);

				if (!CommonUtil.isObjectNull(oProjectName)) {
					strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						strProjectName = StringUtil.replace(strProjectName, "\\", "\\\\");
						strProjectName = StringUtil.replace(strProjectName, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					}
				}
			}

			projectList = projectFinder.sortUserProjects(type, strUserId, searchedProject, orderByType, orderByCol,
					start, end);

		} catch (PortalException pe) {
			params.put("searchedProject", searchedProject);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!roleList.isEmpty()) {
				roleList.clear();
				roleList = null;
			}

			params.clear();
			params = null;
		}

		return projectList;
	}

	/**
	 * Get all Overall Admin Projects
	 * 
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @return projectList a list of projects to be downloaded
	 * @throws PortalException
	 *             an exception thrown
	 */
	public List<Object> getOverallAdminProjectsToDownload(Map<String, Object> searchedProject, int type, User user)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Role> roleList = new ArrayList<Role>();
		int iUserRole = 0;
		String strGroupName = null;
		String strCaseNumber = null;
		String strProjectName = null;

		try {
			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}
			
			if (user == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}
			
			roleList = user.getRoles();

			for (Role r : roleList) {
				if (r.getName().equals(PortalConstants.SYSTEM_ADMINISTRATOR)) {
					iUserRole = UserRole.SYSTEM_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.OVERALL_ADMINISTRATOR)) {
					iUserRole = UserRole.OVERALL_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.GROUP_ADMINISTRATOR)) {
					iUserRole = UserRole.GROUP_ADMIN.getInteger();
					break;
				} else if (r.getName().equals(PortalConstants.GENERAL_USER)) {
					iUserRole = UserRole.GEN_USER.getInteger();
					break;
				}
			}

			if (iUserRole == UserRole.SYSTEM_ADMIN.getInteger() || iUserRole == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);

				if (!CommonUtil.isObjectNull(oGroupName)) {
					strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						strGroupName = StringUtil.replace(strGroupName, "\\", "\\\\");
						strGroupName = StringUtil.replace(strGroupName, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					}
				}

				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						strCaseNumber = StringUtil.replace(strCaseNumber, "\\", "\\\\");
						strCaseNumber = StringUtil.replace(strCaseNumber, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, strCaseNumber);
					}
				}

				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);

				if (!CommonUtil.isObjectNull(oProjectName)) {
					strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						strProjectName = StringUtil.replace(strProjectName, "\\", "\\\\");
						strProjectName = StringUtil.replace(strProjectName, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					}
				}
			}

			projectList = projectFinder.getOverallAdminProjectsToDownload(user.getUserId(), iUserRole, type, searchedProject);

		} catch (PortalException pe) {
			params.put("searchedProject", searchedProject);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!roleList.isEmpty()) {
				roleList.clear();
				roleList = null;
			}

			params.clear();
			params = null;
		}

		return projectList;
	}

	/**
	 * Get All User Projects
	 * 
	 * @param strUserId
	 *            - a User ID
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @return projectList a list of projects to be downloaded
	 * @throws PortalException
	 *             an exception thrown
	 */
	public List<Object> getUserProjectsToDownload(String strUserId, Map<String, Object> searchedProject, int type)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Role> roleList = new ArrayList<Role>();
		String strGroupName = null;
		String strCaseNumber = null;
		String strProjectName = null;

		try {
			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);

				if (!CommonUtil.isObjectNull(oGroupName)) {
					strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						strGroupName = StringUtil.replace(strGroupName, "\\", "\\\\");
						strGroupName = StringUtil.replace(strGroupName, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					}
				}

				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						strCaseNumber = StringUtil.replace(strCaseNumber, "\\", "\\\\");
						strCaseNumber = StringUtil.replace(strCaseNumber, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, strCaseNumber);
					}
				}

				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);

				if (!CommonUtil.isObjectNull(oProjectName)) {
					strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						strProjectName = StringUtil.replace(strProjectName, "\\", "\\\\");
						strProjectName = StringUtil.replace(strProjectName, "%", PortalConstants.PERCENT);
						searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					}
				}
			}

			projectList = projectFinder.getUserProjectsToDownload(type, strUserId, searchedProject);

		} catch (PortalException pe) {
			params.put("searchedProject", searchedProject);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!roleList.isEmpty()) {
				roleList.clear();
				roleList = null;
			}

			params.clear();
			params = null;
		}

		return projectList;
	}

	/**
	 * Check if a case number is valid. A case number should be unique
	 * 
	 * @param caseNumber
	 *            the case number to check
	 * @param projectId
	 *            a unique ID of the project
	 * @return return true if the case number is valid, otherwise return false
	 * @throws PortalException
	 *             an exception thrown
	 */
	public boolean isCaseNumberValid(String caseNumber, long projectId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Project> existingProjectList = new ArrayList<Project>();
		boolean bIsValid = false;

		try {
			if (caseNumber == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
				throw new PortalException(PortalErrors.INVALID_CASE_NUMBER);
			} else if (caseNumber.isEmpty()) {
				return true;
			}

			projectPersistence.clearCache();
			existingProjectList = projectPersistence.findByCaseNumber(caseNumber);

			if (existingProjectList.isEmpty()) {
				bIsValid = true;
			} else {
				for (Project project : existingProjectList) {
					if (project.getProjectId() == projectId) {
						bIsValid = true;
					}
					break;
				}
			}

		} catch (SystemException se) {
			params.put("caseNumber", caseNumber);
			params.put("projectId", projectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_IS_CASE_NUMBER_VALID, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("caseNumber", caseNumber);
			params.put("projectId", projectId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_IS_CASE_NUMBER_VALID, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return bIsValid;
	}

	/**
	 * Update certain record in DB
	 * 
	 * @param project
	 *            - the project to be updated
	 * @param lUsersArr
	 * @param bAdmin
	 * @return return true if update transaction is successful
	 * @throws PortalException
	 *             an exception thrown if the transaction fails.
	 */
	public boolean updateProject(String strSessionId, Project project, List<String> packageNamesList, File file,
			long[] lUsersArr, boolean bAdmin) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;

		try {
			if (project == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new PortalException(PortalErrors.INVALID_PROJECT);
			}

			if (lUsersArr == null && bAdmin) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED,
								PortalConstants.ERROR_LOG_PARAM_PROJECT_USER));
				throw new PortalException(PortalErrors.NO_PROJECT_USERS);
			}

			projectPersistence.update(project);
			bSuccess = true;

			if (bAdmin) {
				bSuccess = ProjectUsersLocalServiceUtil.updateProjectUsers(project.getProjectId(), lUsersArr);
			}
		} catch (PortalException pe) {
			params.put("strSessionId", strSessionId);
			params.put("project", project);
			params.put("packageNamesList", packageNamesList);
			params.put("file", file);
			params.put("lUsersArr", lUsersArr);
			log.debug(pe.getMessage(), PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
			throw pe;
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPDATE_PROJECT, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Delete Record Related to Project
	 * 
	 * @param projectId
	 *            - ID of the project
	 * @return return true if delete transaction is successful
	 * @throws PortalException
	 *             an exception thrown if the transaction fails.
	 */
	public boolean deleteRelatedToProject(long projectId, long lCxProjectId, int iType, String strSessionId)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		List<Object> scanList = new ArrayList<Object>();
		long lScanId = PortalConstants.LONG_ZERO;

		try {
			if (projectId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			scanList = ScanLocalServiceUtil.getScans(projectId, null, null, 0);

			if (!scanList.isEmpty()) {
				for (int i = 0; i < scanList.size(); i++) {

					ScanItem item = (ScanItem) scanList.get(i);
					lScanId = item.getScanId();
					reportPersistence.removeByScanId(lScanId);
				}
				scanPersistence.removeByProjectId(projectId);
			}

			projectUsersPersistence.removeByProjectId(projectId);
			projectPersistence.remove(projectId);

			bSuccess = true;
		} catch (NoSuchProjectException nspe) {
			params.put("projectId", projectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
					PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST,
					PortalConstants.METHOD_DELETE_RELATED_TO_PROJECT, params, nspe);
			throw new PortalException(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST);
		} catch (SystemException se) {
			params.put("projectId", projectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_DELETE_RELATED_TO_PROJECT, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("projectId", projectId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_DELETE_RELATED_TO_PROJECT, params, pe);
			throw pe;
		} finally {
			scanList = null;
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Set project to complete
	 * 
	 * @param projectId
	 *            - ID of the project to be completed
	 * @return return true if transaction is successful
	 * @throws PortalException
	 *             an exception thrown if the transaction fails.
	 */
	public boolean completeProject(long projectId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		Project project = null;
		Project updatedProject = null;

		try {
			if (projectId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID);
			}

			project = ProjectLocalServiceUtil.getProject(projectId);

			if (project == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST,
								PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new PortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST);
			}

			project.setStatus(ProjectStatus.COMPLETE.getInteger());

			updatedProject = projectPersistence.update(project);

			if (updatedProject == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						PortalConstants.ERROR_LOG_MESSAGE_COMPLETE_PROJECT_FAILED);
				throw new PortalException(PortalErrors.COMPLETE_PROJECT_FAILED);
			} else {
				bSuccess = true;
			}
		} catch (SystemException se) {
			params.put("projectId", projectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_COMPLETE_PROJECT, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("projectId", projectId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_COMPLETE_PROJECT, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Reopen completed project
	 * 
	 * @param project
	 *            - a project to be reopen
	 * @return return true if transaction is successful
	 * @throws PortalException
	 *             an exception thrown if the transaction fails.
	 */
	public boolean openProject(Project project) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		Project updatedProject = null;

		try {
			if (project == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST,
								PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new PortalException(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST);
			}

			project.setStatus(ProjectStatus.NOT_YET_COMPLETE.getInteger());

			updatedProject = projectPersistence.update(project);

			if (updatedProject == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_OPEN_PROJECT_FAILED);
				throw new PortalException(PortalErrors.OPEN_PROJECT_FAILED);
			} else {
				bSuccess = true;
			}
		} catch (SystemException se) {
			params.put("updatedProject", updatedProject);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_OPEN_PROJECT, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("project", project);
			log.debug(pe.getMessage(), PortalConstants.METHOD_OPEN_PROJECT, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Generate a unique project ID
	 * 
	 * @return return a unique Project ID
	 * @throws PortalException
	 *             an exception thrown
	 */
	public long createProjectId() throws PortalException {
		long lProjectId = 0L;

		try {
			lProjectId = (long) counterLocalService.increment(Project.class.getName());
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, "createProjectId", null, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION, e);
		}

		return lProjectId;
	}

	/**
	 * Create Project
	 * 
	 * @return return empty Project
	 */
	public Project createProjectObj() {
		return new ProjectImpl();
	}

	/**
	 * Clears the cache for all instances of this model.
	 */
	public void clearCache() throws PortalException {
		projectPersistence.clearCache();
	}
}