/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.StringUtil;

import aQute.bnd.annotation.ProviderType;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectException;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ScanImpl;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.base.ScanLocalServiceBaseImpl;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * The implementation of the scan local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link jp.ubsecure.portal.jubjub.portlet.service.ScanLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Razel
 * @see ScanLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil
 */
@ProviderType
public class ScanLocalServiceImpl extends ScanLocalServiceBaseImpl {

	/** UBSPortalDebugger */
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ScanLocalServiceImpl.class);

	/**
	 * Get List of Scans
	 * 
	 * @param lProjectId
	 *            - id of the project on which project scans should be retrieved
	 * @param searchedScan
	 *            - a key-value parameter inputted by user during search.
	 * @param iStart
	 *            - start index position
	 * @return scanList - list of Project scans
	 * @throws PortalException
	 *             an exception thrown if error occurs
	 */
	public List<Object> getScans(long lProjectId, User user, Map<String, Object> searchedScan, int iStart)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = new ArrayList<Object>();

		try {
			if (lProjectId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						strFileName = StringUtil.replace(strFileName, "\\", "\\\\");
						strFileName = StringUtil.replace(strFileName, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					}
				}

				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						strHashValue = StringUtil.replace(strHashValue, "\\", "\\\\");
						strHashValue = StringUtil.replace(strHashValue, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					}
				}

				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						strScanManager = StringUtil.replace(strScanManager, "\\", "\\\\");
						strScanManager = StringUtil.replace(strScanManager, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					}
				}

			}

			scanList = scanFinder.getScans(lProjectId, searchedScan, iStart);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("user", user);
			params.put("searchedScan", searchedScan);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCANS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return scanList;
	}

	/**
	 * Get All Scans
	 * 
	 * @param type
	 *            a type of the project (android, cxsuite, ios)
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search.
	 * @param start
	 *            a start index position
	 * @return scanList list of the entire scan
	 * @throws PortalException
	 *             an exception thrown if error occurs.
	 */
	public List<Object> getEntireScans(int type, Map<String, Object> searchedScan, int start) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = new ArrayList<Object>();

		try {
			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						strProjectName = StringUtil.replace(strProjectName, "\\", "\\\\");
						strProjectName = StringUtil.replace(strProjectName, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					}
				}

				Object oGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						strGroupName = StringUtil.replace(strGroupName, "\\", "\\\\");
						strGroupName = StringUtil.replace(strGroupName, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					}
				}

				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						strFileName = StringUtil.replace(strFileName, "\\", "\\\\");
						strFileName = StringUtil.replace(strFileName, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					}
				}

				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						strHashValue = StringUtil.replace(strHashValue, "\\", "\\\\");
						strHashValue = StringUtil.replace(strHashValue, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					}
				}

				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						strScanManager = StringUtil.replace(strScanManager, "\\", "\\\\");
						strScanManager = StringUtil.replace(strScanManager, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					}
				}

			}

			scanList = scanFinder.getEntireScans(type, searchedScan, start);
		} catch (PortalException pe) {
			params.put("type", type);
			params.put("searchedScan", searchedScan);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return scanList;
	}

	/**
	 * Get the total Number of All scans in all projects for each project type
	 * (android, cxsuite, ios)
	 * 
	 * @param type
	 *            a type of the project (android, cxsuite, ios)
	 * @param searchedScan
	 * 
	 * @return Number of scans
	 * @throws PortalException
	 *             an exception thrown if error occurs
	 */
	public int getEntireScansCount(int type, Map<String, Object> searchedScan) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = PortalConstants.INT_ZERO;

		try {
			if (type == PortalConstants.INT_ZERO) {
				params.put("type", type);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			count = scanFinder.getEntireScansCount(type, searchedScan);
		} catch (PortalException e) {
			params.put("type", type);
			params.put("searchedScan", searchedScan);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS_COUNT, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}

		return count;
	}

	/**
	 * Get the number of scans
	 * 
	 * @param lProjectId
	 *            - id of the project on which project scans to count
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @return Number of scans
	 * @throws PortalException
	 *             an exception thrown if error occurs
	 */
	public int getScansCount(long lProjectId, Map<String, Object> searchedScan) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = PortalConstants.INT_ZERO;

		try {
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put("lProjectId", lProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			count = scanFinder.getScansCount(lProjectId, searchedScan);
		} catch (PortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("searchedScan", searchedScan);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS_COUNT, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		return count;
	}

	/**
	 * Sort Entire Scans
	 * 
	 * @param type
	 *            a type of project (android, cxsuite, ios)
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search.
	 * @param orderByCol
	 *            a column to order
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param start
	 *            index for the start position
	 * @return scanList - sorted scan list
	 * @throws PortalException
	 */
	public List<Object> sortEntireScans(int type, Map<String, Object> searchedScan, String orderByCol,
			String orderByType, int start) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = new ArrayList<Object>();

		try {
			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						strProjectName = StringUtil.replace(strProjectName, "\\", "\\\\");
						strProjectName = StringUtil.replace(strProjectName, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					}
				}

				Object oGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						strGroupName = StringUtil.replace(strGroupName, "\\", "\\\\");
						strGroupName = StringUtil.replace(strGroupName, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					}
				}

				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						strFileName = StringUtil.replace(strFileName, "\\", "\\\\");
						strFileName = StringUtil.replace(strFileName, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					}
				}

				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						strHashValue = StringUtil.replace(strHashValue, "\\", "\\\\");
						strHashValue = StringUtil.replace(strHashValue, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					}
				}

				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						strScanManager = StringUtil.replace(strScanManager, "\\", "\\\\");
						strScanManager = StringUtil.replace(strScanManager, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					}
				}

			}

			scanList = scanFinder.sortEntireScans(type, searchedScan, orderByCol, orderByType, start);
		} catch (PortalException pe) {
			params.put("type", type);
			params.put("searchedScan", searchedScan);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return scanList;
	}

	/**
	 * Sort Scans
	 * 
	 * @param lProjectId
	 *            id of the project on which project scans to count
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @param orderByCol
	 *            a column to order
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param iStart
	 *            index for the start position
	 * @return scanList a sorted scan list
	 * @throws PortalException
	 */
	public List<Object> sortScans(long lProjectId, Map<String, Object> searchedScan, String orderByCol,
			String orderByType, int iStart) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = new ArrayList<Object>();

		try {
			if (lProjectId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
				Object oFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);

				if (!CommonUtil.isObjectNull(oFileName)) {
					String strFileName = oFileName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						strFileName = StringUtil.replace(strFileName, "\\", "\\\\");
						strFileName = StringUtil.replace(strFileName, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					}
				}

				Object oHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);

				if (!CommonUtil.isObjectNull(oHashValue)) {
					String strHashValue = oHashValue.toString();

					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						strHashValue = StringUtil.replace(strHashValue, "\\", "\\\\");
						strHashValue = StringUtil.replace(strHashValue, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					}
				}

				Object oScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);

				if (!CommonUtil.isObjectNull(oScanManager)) {
					String strScanManager = oScanManager.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanManager)) {
						strScanManager = StringUtil.replace(strScanManager, "\\", "\\\\");
						strScanManager = StringUtil.replace(strScanManager, "%", PortalConstants.PERCENT);
						searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					}
				}

			}

			scanList = scanFinder.sortScans(lProjectId, searchedScan, orderByCol, orderByType, iStart);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("searchedScan", searchedScan);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCANS, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return scanList;
	}

	/**
	 * Get the number of scans waiting
	 * 
	 * @param scanRegDate
	 *            the registration date of the scan
	 * @param type
	 *            a type of project(android, cxsuite, ios)
	 * @return Number of scans waiting
	 * @throws PortalException
	 */
	public int countScanWaiting(Date scanRegDate, int type) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = 0;
		String strErrorCode = null;
		try {
			if (scanRegDate == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
			}

			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			count = scanFinder.countScanWaiting(scanRegDate, type);
		} catch (PortalException pe) {
			params.put("scanRegDate", scanRegDate);
			params.put("type", type);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_COUNT_SCAN_WAITING, params, pe);
			throw pe;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return count;
	}
	
	/**
	 * Get the number of scans waiting
	 * 
	 * @param scanRegDate
	 *            the registration date of the scan
	 * @param type
	 *            a type of project(android, cxsuite, ios)
	 * @return Number of scans waiting
	 * @throws PortalException
	 */
	public int countCrawlWaiting(Date scanRegDate, int type) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = 0;
		String strErrorCode = null;
		try {
			if (scanRegDate == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new PortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
			}

			if (type == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			count = scanFinder.countCrawlWaiting(scanRegDate, type);
		} catch (PortalException pe) {
			params.put("scanRegDate", scanRegDate);
			params.put("type", type);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_COUNT_SCAN_WAITING, params, pe);
			throw pe;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return count;
	}

	/**
	 * Get Scans Per Project
	 * 
	 * @param projectId
	 *            id of the project on where to get the scans
	 * @return scanList list of scans
	 * @throws PortalException
	 */
	public List<Scan> getScansByProjectId(long projectId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Scan> scanList = new ArrayList<Scan>();
		String strErrorCode = null;
		try {
			if (projectId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			scanPersistence.clearCache();
			scanList = scanPersistence.findByProjectId(projectId);
		} catch (SystemException se) {
			params.put("projectId", projectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SCANS_BY_PROJECT_ID, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("projectId", projectId);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_GET_SCANS_BY_PROJECT_ID, params, pe);
			throw pe;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return scanList;
	}

	/**
	 * Update existing scans
	 * 
	 * @param iUserAction
	 *            action of the user
	 * @param scan
	 *            the scan to be updated
	 * @return return true if update transaction is successful
	 * @throws PortalException
	 *             an exception thrown if transaction fails
	 */
	public boolean updateScan(int iUserAction, Scan scan) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;

		try {
			if (scan == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new PortalException(PortalErrors.INVALID_SCAN);
			} else {
				if (scan.getScanId() == 0L) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
							PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));

					if (iUserAction == PortalConstants.USER_EVENT_STOP_SCAN) {
						throw new PortalException(PortalErrors.STOP_SCAN_SCAN_ID_INVALID);
					} else if (iUserAction == PortalConstants.USER_EVENT_REEXECUTE_SCAN) {
						throw new PortalException(PortalErrors.REEXECUTE_SCAN_SCAN_ID_INVALID);
					} else if (iUserAction == PortalConstants.USER_EVENT_REQUEST_REVIEW) {
						throw new PortalException(PortalErrors.REQUEST_REVIEW_SCAN_ID_INVALID);
					} else if (iUserAction == PortalConstants.USER_EVENT_CANCEL_REVIEW) {
						throw new PortalException(PortalErrors.CANCEL_REVIEW_SCAN_ID_INVALID);
					} else if (iUserAction == PortalConstants.USER_EVENT_REGENERATE_REPORT) {
						throw new PortalException(PortalErrors.REGENERATE_REPORT_SCAN_ID_INVALID);
					}
				} else {
					scan.setModifiedDate(new Date());
					scan = super.updateScan(scan);

					if (scan == null) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								PortalConstants.ERROR_LOG_MESSAGE_SCAN_CHANGE_FAILED);
						if (iUserAction == PortalConstants.USER_EVENT_STOP_SCAN) {
							throw new PortalException(PortalErrors.STOP_SCAN_FAILED);
						} else if (iUserAction == PortalConstants.USER_EVENT_REEXECUTE_SCAN) {
							throw new PortalException(PortalErrors.REEXECUTE_SCAN_FAILED);
						} else if (iUserAction == PortalConstants.USER_EVENT_REQUEST_REVIEW) {
							throw new PortalException(PortalErrors.REQUEST_REVIEW_FAILED);
						} else if (iUserAction == PortalConstants.USER_EVENT_CANCEL_REVIEW) {
							throw new PortalException(PortalErrors.CANCEL_REVIEW_FAILED);
						} else if (iUserAction == PortalConstants.USER_EVENT_REGENERATE_REPORT) {
							throw new PortalException(PortalErrors.REGENERATE_REPORT_FAILED);
						}
					} else {
						bSuccess = true;
					}
				}
			}
		} catch (NoSuchScanException e) {
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
					PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			if (iUserAction == PortalConstants.USER_EVENT_STOP_SCAN) {
				log.debug(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_SCAN, params, e);
				throw new PortalException(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST);
			} else if (iUserAction == PortalConstants.USER_EVENT_REEXECUTE_SCAN) {
				log.debug(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_SCAN, params,
						e);
				throw new PortalException(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST);
			} else if (iUserAction == PortalConstants.USER_EVENT_REQUEST_REVIEW) {
				log.debug(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_SCAN, params,
						e);
				throw new PortalException(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST);
			} else if (iUserAction == PortalConstants.USER_EVENT_CANCEL_REVIEW) {
				log.debug(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_SCAN, params,
						e);
				throw new PortalException(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST);
			} else if (iUserAction == PortalConstants.USER_EVENT_REGENERATE_REPORT) {
				log.debug(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_SCAN,
						params, e);
				throw new PortalException(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST);
			}
		} catch (PortalException e) {
			log.debug(e.getMessage(), PortalConstants.METHOD_UPDATE_SCAN, params, e);
			throw e;
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPDATE_SCAN, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} finally {
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Delete existing scan
	 * 
	 * @param lScanId
	 *            scan id to be deleted
	 * @return returns true if delete transaction is successful
	 * @throws PortalException
	 */
	public boolean deleteScan(int iType, long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		Scan scan = null;
		Scan removedScan = null;
		Project project = null;
		String strErrorCode = null;
		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			scanPersistence.clearCache();
			scan = scanPersistence.findByPrimaryKey(lScanId);

			if (scan == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new PortalException(PortalErrors.INVALID_SCAN);
			}

			bSuccess = deleteReport(lScanId);

			if (!bSuccess) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_REPORT_DELETION_FAILED);
				throw new PortalException(PortalErrors.DELETE_REPORT_FAILED);
			}

			removedScan = scanPersistence.remove(lScanId);

			if (removedScan == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_DELETION_FAILED);
				throw new PortalException(PortalErrors.DELETE_SCAN_FAILED);
			}
			bSuccess = true;

		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put("scan", scan);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
					PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params, nsse);
			throw new PortalException(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST);
		} catch (NoSuchProjectException nspe) {
			params.put("project", project);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
					PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params,
					nspe);
			throw new PortalException(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST);
		} catch (SystemException se) {
			params.put("lScanId", lScanId);
			params.put("scan", scan);
			params.put("project", project);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_DELETE_SCAN, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (NumberFormatException nfe) {
			log.debug(PortalErrors.NUMBER_FORMAT_EXCEPTION, PortalConstants.METHOD_DELETE_SCAN, params, nfe);
			throw new PortalException(PortalErrors.NUMBER_FORMAT_EXCEPTION);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);
			params.put("scan", scan);
			params.put("project", project);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_DELETE_SCAN, params, pe);
			throw pe;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Generate a unique scan ID
	 * 
	 * @return returns a unique scan id
	 * @throws PortalException
	 *             an exception thrown
	 */
	public long createScanId() throws PortalException {
		long lScanId = 0L;

		try {
			lScanId = (long) counterLocalService.increment(Scan.class.getName());
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, "createScanId", null, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION, e);
		}

		return lScanId;
	}

	/**
	 * Create empty scan object
	 * 
	 * @return scan object
	 */
	public Scan createScanObj() {
		return new ScanImpl();
	}

	/**
	 * Delete Scan Report
	 * 
	 * @param lScanId
	 *            scan id of the report to be deleted
	 * @return returns true if delete transaction is successful
	 * @throws PortalException
	 *             an exception thrown if transaction fails
	 */
	private boolean deleteReport(long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		String strErrorCode = null;
		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			reportPersistence.removeByScanId(lScanId);

			bSuccess = true;
		} catch (SystemException se) {
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_DELETE_REPORT, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_DELETE_REPORT, params, pe);
			throw pe;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Get File path
	 * 
	 * @param lScanId
	 *            scan ID
	 * @return filepath
	 * @throws PortalException
	 *             an exception thrown if error occurs
	 */
	public String getFilePath(long lScanId) throws PortalException {
		Scan scan = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String strErrorCode = null;

		try {

			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			scanPersistence.clearCache();
			scan = scanPersistence.findByPrimaryKey(lScanId);

		} catch (PortalException pe) {
			params.put("lScanId", lScanId);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_GET_FILE_PATH, params, pe);
			throw pe;
		} catch (SystemException e) {
			params.put("lScanId", lScanId);

			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_FILE_PATH, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION, e);
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return scan.getFilePath();
	}

	/**
	 * Get Scan
	 * 
	 * @param lScanId
	 *            id of the scan to retrieve
	 * @return Scan object
	 * @throws PortalException
	 *             an exception thrown
	 */
	public Scan retrieveScan(long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Scan scan = null;

		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			scanPersistence.clearCache();
			scan = getScan(lScanId);
		} catch (PortalException pe) {
			params.put("lProjectId", lScanId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCAN, params, pe);
			throw pe;
		} catch (SystemException e) {
			params.put("lProjectId", lScanId);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_SCAN, params, e);
		} finally {
			params.clear();
			params = null;
		}

		return scan;
	}

	/**
	 * Request for review
	 * 
	 * @param lScanId
	 *            id of the scan to be requested for review
	 * @return returns true if request review transaction is successful
	 * @throws PortalException
	 *             an exception thrown if error occurs
	 */
	public boolean requestReview(long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		Scan scan = null;
		Scan updatedScan = null;
		String strErrorCode = null;

		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			scanPersistence.clearCache();
			scan = scanPersistence.findByPrimaryKey(lScanId);

			if (scan == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new PortalException(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST);
			}

			scan.setStatus(ScanStatus.UNDER_REVIEW.getInteger());
			scan.setReviewFlag(PortalConstants.INT_ONE);
			scan.setModifiedDate(new Date());

			updatedScan = scanPersistence.update(scan);

			if (updatedScan == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_CHANGE_FAILED);
				throw new PortalException(PortalErrors.REQUEST_REVIEW_FAILED);
			}

			bSuccess = true;
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put("scan", scan);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
					PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_REQUEST_REVIEW, params,
					nsse);
			throw new PortalException(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST);
		} catch (SystemException se) {
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_REQUEST_REVIEW, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_REQUEST_REVIEW, params, pe);
			throw pe;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Cancel a review request
	 * 
	 * @param lScanId
	 *            id of scan to cancel the request review
	 * @return returns true if cancel transaction is successful
	 * @throws PortalException
	 *             an exception thrown
	 */
	public boolean cancelReview(long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		Scan scan = null;
		Scan updatedScan = null;
		String strErrorCode = null;

		try {
			if (lScanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			scanPersistence.clearCache();
			scan = scanPersistence.findByPrimaryKey(lScanId);

			if (scan == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new PortalException(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST);
			}

			scan.setStatus(ScanStatus.COMPLETE.getInteger());
			scan.setReviewFlag(PortalConstants.INT_ZERO);
			scan.setModifiedDate(new Date());

			updatedScan = scanPersistence.update(scan);

			if (updatedScan == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_CHANGE_FAILED);
				throw new PortalException(PortalErrors.CANCEL_REVIEW_FAILED);
			}

			bSuccess = true;
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put("scan", scan);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
					PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_CANCEL_REVIEW, params,
					nsse);
			throw new PortalException(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST);
		} catch (SystemException se) {
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_CANCEL_REVIEW, params, se);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_CANCEL_REVIEW, params, pe);
			throw pe;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return bSuccess;
	}

	/**
	 * Get Scan Status
	 * 
	 * @param lScanId
	 *            id of the scan
	 * @return returns a status of the scan
	 * @throws PortalException
	 *             an exception thrown
	 */
	public int getScanStatus(long lScanId) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int iStatus = 0;
		String strErrorCode = null;
		try {

			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}
			iStatus = scanFinder.getScanStatus(lScanId);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);

			if (pe.getMessage().startsWith("S.") || pe.getMessage().startsWith("U.")) {
				strErrorCode = pe.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_GET_SCAN_STATUS, params, pe);
			throw pe;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return iStatus;
	}

	/**
	 * Check if scan is deleted in cx server
	 * 
	 * @param scanId
	 *            id of the scan to be checked
	 * @return return true if the scan is deleted in cx scan server, else return
	 *         false
	 * @throws PortalException
	 *             an exception thrown
	 */
	public boolean isDeletedScanInCxServer(long scanId) throws PortalException {
		Scan scanItem = null;
		boolean isDeleted = false;
		String isDeletedInCx = null;

		try {
			scanPersistence.clearCache();
			scanItem = scanPersistence.fetchByPrimaryKey(scanId);
			isDeletedInCx = scanItem.getIsDeletedInCx();
			if (scanItem != null) {
				if (!CommonUtil.isStringNullOrEmpty(isDeletedInCx)
						&& isDeletedInCx.equals(PortalConstants.DELETED_FLAG)) {
					isDeleted = true;
				}
			}
		} catch (SystemException e) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("lScanId", scanId);
			params.put("isDeleted", isDeleted);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_IS_DELETED_SCAN_IN_CX, params, e);
			throw new PortalException(PortalErrors.SYSTEM_EXCEPTION);
		}

		return isDeleted;
	}

	/**
	 * Retrieve the scan registragion date
	 * 
	 * @param lScanId
	 *            id of the scan
	 * @return Date
	 */
	public Date getScanRegistrationDate(long lScanId) {
		Map<String, Object> params = new HashMap<String, Object>();
		Date dteRegistrationDate = null;
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Scan.class,
				PortalClassLoaderUtil.getClassLoader());
		Projection projection = ProjectionFactoryUtil.distinct(ProjectionFactoryUtil.property("registrationDate"));
		dynamicQuery.setProjection(projection).addOrder(OrderFactoryUtil.desc("registrationDate"));
		dynamicQuery.add(PropertyFactoryUtil.forName("scanId").eq(lScanId));
		List<Object> registrationDateList = null;
		try {
			registrationDateList = ScanLocalServiceUtil.dynamicQuery(dynamicQuery);
			if (registrationDateList != null && registrationDateList.size() == 1) {
				for (Object regDate : registrationDateList) {
					dteRegistrationDate = (Date) regDate;
					break;
				}
			} else {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
								PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
			}
		} catch (SystemException e) {
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SCAN_REGISTRATION_DATE, params, e);
		} catch (UBSPortalException e) {
			params.put("lScanId", lScanId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_SCAN_REGISTRATION_DATE, params, e);
		} finally {
			params.clear();
			params = null;
		}

		return dteRegistrationDate;
	}

	/**
	 * Update Android scan status
	 * 
	 * @param lScanId
	 *            id of the scan to be updated
	 * @param iStatus
	 *            the status to be set
	 * @return returns the updated scan
	 * @throws PortalException
	 *             an exception thrown if error occurs
	 */
	public Scan updateAndroidScanStatus(long lScanId, int iStatus) throws PortalException {
		Scan scan = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String strErrorCode = null;

		try {

			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}
			if (CommonUtil.isOutOfRange(iStatus, 1, PortalConstants.SCAN_STATUS_LIMIT)) {
				params.put("iStatus", iStatus);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_STATUS));
				throw new PortalException(PortalErrors.INVALID_SCAN_STATUS);
			}

			scanPersistence.clearCache();
			scan = scanPersistence.findByPrimaryKey(lScanId);
			scan.setStatus(iStatus);
			scan.setModifiedDate(new Date());
			scan = super.updateScan(scan);

		} catch (NoSuchScanException e) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
					PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_UPDATE_ANDROID_SCAN_STATUS, params, e);
			throw new PortalException(PortalErrors.PORTAL_EXCEPTION, e);
		} catch (SystemException e) {
			params.put("lScanId", lScanId);
			params.put("scan", scan);

			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPDATE_ANDROID_SCAN_STATUS, params, e);
			throw new PortalException(PortalErrors.PORTAL_EXCEPTION, e);
		} catch (PortalException e) {
			params.put("lScanId", lScanId);

			if (e.getMessage().startsWith("S.") || e.getMessage().startsWith("U.")) {
				strErrorCode = e.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}

			log.debug(strErrorCode, PortalConstants.METHOD_UPDATE_ANDROID_SCAN_STATUS, params, e);
			throw e;
		} finally {
			strErrorCode = null;
			params.clear();
			params = null;
		}

		return scan;
	}
}