/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.Report;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Report in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Report
 * @generated
 */
@ProviderType
public class ReportCacheModel implements CacheModel<Report>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReportCacheModel)) {
			return false;
		}

		ReportCacheModel reportCacheModel = (ReportCacheModel)obj;

		if (reportId == reportCacheModel.reportId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, reportId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{reportId=");
		sb.append(reportId);
		sb.append(", reportDate=");
		sb.append(reportDate);
		sb.append(", reportBucketName=");
		sb.append(reportBucketName);
		sb.append(", reportType=");
		sb.append(reportType);
		sb.append(", reportName=");
		sb.append(reportName);
		sb.append(", cxReportId=");
		sb.append(cxReportId);
		sb.append(", scanId=");
		sb.append(scanId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Report toEntityModel() {
		ReportImpl reportImpl = new ReportImpl();

		reportImpl.setReportId(reportId);

		if (reportDate == Long.MIN_VALUE) {
			reportImpl.setReportDate(null);
		}
		else {
			reportImpl.setReportDate(new Date(reportDate));
		}

		if (reportBucketName == null) {
			reportImpl.setReportBucketName(StringPool.BLANK);
		}
		else {
			reportImpl.setReportBucketName(reportBucketName);
		}

		reportImpl.setReportType(reportType);

		if (reportName == null) {
			reportImpl.setReportName(StringPool.BLANK);
		}
		else {
			reportImpl.setReportName(reportName);
		}

		reportImpl.setCxReportId(cxReportId);
		reportImpl.setScanId(scanId);

		reportImpl.resetOriginalValues();

		return reportImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		reportId = objectInput.readLong();
		reportDate = objectInput.readLong();
		reportBucketName = objectInput.readUTF();

		reportType = objectInput.readInt();
		reportName = objectInput.readUTF();

		cxReportId = objectInput.readLong();

		scanId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(reportId);
		objectOutput.writeLong(reportDate);

		if (reportBucketName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reportBucketName);
		}

		objectOutput.writeInt(reportType);

		if (reportName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reportName);
		}

		objectOutput.writeLong(cxReportId);

		objectOutput.writeLong(scanId);
	}

	public long reportId;
	public long reportDate;
	public String reportBucketName;
	public int reportType;
	public String reportName;
	public long cxReportId;
	public long scanId;
}