package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ibm.wsdl.util.StringUtils;
import com.liferay.portal.dao.orm.custom.sql.CustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectItem;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ProjectImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ProjectFinder;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * ProjectFinder Implementation
 */
public class ProjectFinderImpl extends ProjectFinderBaseImpl implements ProjectFinder {

	/** UBSPortalDebugger */ 
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ProjectFinderImpl.class);

	/** GET_OVERALL_ADMIN_PROJECTS */
	private static final String GET_OVERALL_ADMIN_PROJECTS = ProjectFinder.class.getName() + ".getOverallAdminProjects";
	
	/** GET_PROJECTS_COUNT */
	private static final String GET_PROJECTS_COUNT = ProjectFinder.class.getName() + ".getProjectsCount";
	
	/** GET_SEARCHED_PROJECTS_COUNT */
	private static final String GET_SEARCHED_PROJECTS_COUNT = ProjectFinder.class.getName() + ".getSearchedProjectsCount";
	
	/** SORT_OVERALL_ADMIN_PROJECTS */ 
	private static final String SORT_OVERALL_ADMIN_PROJECTS = ProjectFinder.class.getName() + ".sortOverallAdminProjects";
	
	/** GET_OVERALL_ADMIN_PROJECTS_TO_DOWNLOAD */ 
	private static final String GET_OVERALL_ADMIN_PROJECTS_TO_DOWNLOAD = ProjectFinder.class.getName() + ".getOverallAdminProjectsToDownload";

	/** GET_USER_PROJECTS */ 
	private static final String GET_USER_PROJECTS = ProjectFinder.class.getName() + ".getUserProjects";
	
	/** GET_USER_PROJECTS_COUNT */ 
	private static final String GET_USER_PROJECTS_COUNT = ProjectFinder.class.getName() + ".getUserProjectsCount";
	
	/** GET_SEARCHED_USER_PROJECTS_COUNT */
	private static final String GET_SEARCHED_USER_PROJECTS_COUNT = ProjectFinder.class.getName() + ".getSearchedUserProjectsCount";
	
	/** SORT_USER_PROJECTS */ 
	private static final String SORT_USER_PROJECTS = ProjectFinder.class.getName() + ".sortUserProjects";
	
	/** GET_USER_PROJECTS_TO_DOWNLOAD */
	private static final String GET_USER_PROJECTS_TO_DOWNLOAD = ProjectFinder.class.getName() + ".getUserProjectsToDownload";

	/**
	 * Get Overall Admin Projects
	 * 
	 * @param iType
	 *            a type of a project(android, cxsuite, ios)
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search.
	 * @param iStart
	 *            index for the start position
	 * @param iEnd
	 *            the last index position
	 * @return List of overall admin projects
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getOverallAdminProjects(long lUserId, int iUserRole, int iType, Map<String, Object> searchedProject, int iStart, int iEnd)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		long[] userOrgIdList = null;

		try {
			if (iType == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			sqlQuery = this.generateOverallAdminSQLQuery(lUserId, iUserRole, CustomSQLUtil.get(getClass(), GET_OVERALL_ADMIN_PROJECTS), searchedProject);
			qPos = QueryPos.getInstance(sqlQuery);

			qPos.add(iType);
			
			if(iUserRole == UserRole.GROUP_ADMIN.getInteger() && lUserId != PortalConstants.LONG_ZERO){
				User user = UserLocalServiceUtil.getUser(lUserId);
				userOrgIdList = user.getOrganizationIds();
				
				if(!CommonUtil.isObjectNull(userOrgIdList) && userOrgIdList.length > PortalConstants.INT_ZERO){
					long[] userOrgIdArr = new long[userOrgIdList.length];
					int index = PortalConstants.INT_ZERO;
					for (long userOrgId : userOrgIdList) {
						if (userOrgId != PortalConstants.LONG_ZERO) {
							userOrgIdArr[index] = userOrgId;
							index++;
						}
					}
					qPos.add(userOrgIdArr);
				}
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
								
				if (!CommonUtil.isObjectNull(oProjectId)) {
					String strProjectId = oProjectId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
						long lProjectId = Long.parseLong(strProjectId);

						qPos.add(lProjectId);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					String strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseNumber.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Calendar dteProjectEndDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					try {
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					try {
						dteProjectEndDateHigh = Calendar.getInstance();
						dteProjectEndDateHigh.setTime(format.parse(strProjectEndDateHigh));
						dteProjectEndDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteProjectEndDateHigh.set(Calendar.MINUTE, 59);
						dteProjectEndDateHigh.set(Calendar.SECOND, 59);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteProjectEndDateLow) && !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateLow);
					qPos.add(dteProjectEndDateHigh.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					qPos.add(dteProjectEndDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateHigh.getTime());
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						qPos.add(Integer.parseInt(strStatus));
					}
				}

				if (!CommonUtil.isObjectNull(oScanCount)) {
					String strScanCount = oScanCount.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
						qPos.add(Integer.parseInt(strScanCount));
					}
				}
				
				if (!CommonUtil.isObjectNull(oCaseName)) {
					String strCaseName = oCaseName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl)) {
					String strTargetUrl = oTargetUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strTargetUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strTargetUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl)) {
					String strProductionEnvironmentUrl = oProductionEnvironmentUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProductionEnvironmentUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProductionEnvironmentUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup)) {
					String strWebInspectionSignatureSetGroup = oWebInspectionSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strWebInspectionSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strWebInspectionSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
								
				if (!CommonUtil.isObjectNull(oServerFiles)) {
					String strServerFiles = oServerFiles.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFiles)) {
						long lServerFiles = Long.parseLong(strServerFiles);

						qPos.add(lServerFiles);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings)) {
					String strServerSettings = oServerSettings.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettings)) {
						long lServerSettings = Long.parseLong(strServerSettings);

						qPos.add(lServerSettings);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup)) {
					String strServerFilesSignatureSetGroup = oServerFilesSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFilesSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerFilesSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup)) {
					String strServerSettingsSignatureSetGroup = oServerSettingsSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettingsSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerSettingsSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
			}

			qPos.add(iStart);
			qPos.add(iEnd);

			projectList = sqlQuery.list();

			resultList = this.getOverallAdminResults(projectList);
		} catch (ORMException orme) {
			params.put("iType", iType);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_OVERALL_ADMIN_PROJECTS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("iType", iType);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_OVERALL_ADMIN_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!projectList.isEmpty()) {
				projectList.clear();
				projectList = null;
			}
			params.clear();
			params = null;
		}

		return resultList;
	}
	
	/**
	 * Get the list of user projects
	 * 
	 * @param iType
	 *            a type of a project(android, cxsuite, ios)
	 * @param strUserId
	 *            ID of the user of which the projects belong
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search.
	 * @param iStart
	 *            first index position
	 * @param iEnd
	 *            last index position
	 * @return List of projects of the user
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getUserProjects(int iType, String strUserId, Map<String, Object> searchedProject, int iStart,
			int iEnd) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (iType == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (CommonUtil.isStringNullOrEmpty(strUserId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}

			sqlQuery = this.generateUserSQLQuery(CustomSQLUtil.get(getClass(), GET_USER_PROJECTS), searchedProject);
			qPos = QueryPos.getInstance(sqlQuery);

			qPos.add(iType);
			qPos.add(strUserId);

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);

				if (!CommonUtil.isObjectNull(oProjectId)) {
					String strProjectId = oProjectId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
						long lProjectId = Long.parseLong(strProjectId);

						qPos.add(lProjectId);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					String strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseNumber.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Date dteProjectEndDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					try {
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					try {
						dteProjectEndDateHigh = format.parse(strProjectEndDateHigh);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteProjectEndDateLow) && !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateLow);
					qPos.add(dteProjectEndDateHigh);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					qPos.add(dteProjectEndDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateHigh);
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						int iStatus = Integer.parseInt(strStatus);

						qPos.add(iStatus);
					}
				}

				if (!CommonUtil.isObjectNull(oScanCount)) {
					String strScanCount = oScanCount.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
						int iScanCount = Integer.parseInt(strScanCount);

						qPos.add(iScanCount);
					}
				}
				
				if (!CommonUtil.isObjectNull(oCaseName)) {
					String strCaseName = oCaseName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl)) {
					String strTargetUrl = oTargetUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strTargetUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strTargetUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl)) {
					String strProductionEnvironmentUrl = oProductionEnvironmentUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProductionEnvironmentUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProductionEnvironmentUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup)) {
					String strWebInspectionSignatureSetGroup = oWebInspectionSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strWebInspectionSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strWebInspectionSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles)) {
					String strServerFiles = oServerFiles.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFiles)) {
						long lServerFiles = Long.parseLong(strServerFiles);

						qPos.add(lServerFiles);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings)) {
					String strServerSettings = oServerSettings.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettings)) {
						long lServerSettings = Long.parseLong(strServerSettings);

						qPos.add(lServerSettings);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup)) {
					String strServerFilesSignatureSetGroup = oServerFilesSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFilesSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerFilesSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup)) {
					String strServerSettingsSignatureSetGroup = oServerSettingsSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettingsSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerSettingsSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
			}
			qPos.add(iStart);
			qPos.add(iEnd);

			projectList = sqlQuery.list();

			resultList = this.getUserResults(projectList);
		} catch (ORMException orme) {
			params.put("iType", iType);
			params.put("strUserId", strUserId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_USER_PROJECTS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("iType", iType);
			params.put("strUserId", strUserId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_USER_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!projectList.isEmpty()) {
				projectList.clear();
				projectList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Get number of projects.
	 * 
	 * @param type
	 *            the type of project(android, cxsuite, ios)
	 * @return returns the number of porjects
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public int getProjectsCount(int type) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = 0;
		List<Project> projectList = new ArrayList<Project>();
		String strSql = null;
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		Session session = null;

		try {
			if (type == PortalConstants.INT_ZERO) {
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			session = openSession();
			strSql = CustomSQLUtil.get(getClass(), GET_PROJECTS_COUNT);
			sqlQuery = session.createSQLQuery(strSql);
			sqlQuery.setCacheable(false);
			sqlQuery.addEntity("Project", ProjectImpl.class);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(type);
			projectList = sqlQuery.list();

			count = projectList.size();
		} catch (ORMException orme) {
			params.put("type", type);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS_COUNT, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECTS_COUNT, params, pe);
			throw pe;
		} finally {
			closeSession(session);

			if (!CommonUtil.isListNullOrEmpty(projectList)) {
				projectList.clear();
				projectList = null;
			}

			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}

		return count;
	}

	/**
	 * Get User Projects Count
	 * 
	 * @param userId
	 *            id of the user
	 * @param type
	 *            the type of the project(android, cxsuite, ios)
	 * @return Number of users project
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public int getUserProjectsCount(String userId, int type) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = 0;
		List<Project> projectList = new ArrayList<Project>();
		String strSql = null;
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		Session session = null;

		try {
			if (CommonUtil.isStringNullOrEmpty(userId)) {
				throw new PortalException(PortalErrors.INVALID_USER);
			}

			if (type == PortalConstants.INT_ZERO) {
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			session = openSession();
			strSql = CustomSQLUtil.get(getClass(), GET_USER_PROJECTS_COUNT);
			sqlQuery = session.createSQLQuery(strSql);
			sqlQuery.setCacheable(false);
			sqlQuery.addEntity("Project", ProjectImpl.class);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(type);
			qPos.add(userId);
			projectList = sqlQuery.list();

			count = projectList.size();
		} catch (ORMException orme) {
			params.put("userId", userId);
			params.put("type", type);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_USER_PROJECTS_COUNT, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("userId", userId);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_USER_PROJECTS_COUNT, params, pe);
			throw pe;
		} finally {
			closeSession(session);

			if (!CommonUtil.isListNullOrEmpty(projectList)) {
				projectList.clear();
				projectList = null;
			}

			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}

		return count;
	}

	/**
	 * Get the number of Searched Projects
	 * 
	 * @param type
	 *            type of the project(android, cxsuite, ios)
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @return Number of searched projects
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public int getSearchedProjectsCount(long lUserId, int iUserRole, int type, Map<String, Object> searchedProject) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = 0;
		List<Project> projectList = new ArrayList<Project>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		long[] userOrgIdList = null;
		
		try {
			if (type == PortalConstants.INT_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			sqlQuery = this.generateOverallAdminSQLQuery(lUserId, iUserRole, CustomSQLUtil.get(getClass(), GET_SEARCHED_PROJECTS_COUNT),
					searchedProject);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(type);
			
			if(iUserRole == UserRole.GROUP_ADMIN.getInteger() && lUserId != PortalConstants.LONG_ZERO){
				User user = UserLocalServiceUtil.getUser(lUserId);
				userOrgIdList = user.getOrganizationIds();
				
				if(!CommonUtil.isObjectNull(userOrgIdList) && userOrgIdList.length > PortalConstants.INT_ZERO){
					long[] userOrgIdArr = new long[userOrgIdList.length];
					int index = PortalConstants.INT_ZERO;
					for (long userOrgId : userOrgIdList) {
						if (userOrgId != PortalConstants.LONG_ZERO) {
							userOrgIdArr[index] = userOrgId;
							index++;
						}
					}
					qPos.add(userOrgIdArr);
				}
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
					
				if (!CommonUtil.isObjectNull(oProjectId)) {
					String strProjectId = oProjectId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
						long lProjectId = Long.parseLong(strProjectId);

						qPos.add(lProjectId);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					String strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseNumber.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Date dteProjectEndDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					try {
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					try {
						dteProjectEndDateHigh = format.parse(strProjectEndDateHigh);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteProjectEndDateLow) && !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateLow);
					qPos.add(dteProjectEndDateHigh);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					qPos.add(dteProjectEndDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateHigh);
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						int iStatus = Integer.parseInt(strStatus);

						qPos.add(iStatus);
					}
				}

				if (!CommonUtil.isObjectNull(oScanCount)) {
					String strScanCount = oScanCount.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
						int iScanCount = Integer.parseInt(strScanCount);

						qPos.add(iScanCount);
					}
				}
				
				if (!CommonUtil.isObjectNull(oCaseName)) {
					String strCaseName = oCaseName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl)) {
					String strTargetUrl = oTargetUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strTargetUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strTargetUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl)) {
					String strProductionEnvironmentUrl = oProductionEnvironmentUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProductionEnvironmentUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProductionEnvironmentUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup)) {
					String strWebInspectionSignatureSetGroup = oWebInspectionSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strWebInspectionSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strWebInspectionSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles)) {
					String strServerFiles = oServerFiles.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFiles)) {
						long lServerFiles = Long.parseLong(strServerFiles);

						qPos.add(lServerFiles);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings)) {
					String strServerSettings = oServerSettings.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettings)) {
						long lServerSettings = Long.parseLong(strServerSettings);

						qPos.add(lServerSettings);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup)) {
					String strServerFilesSignatureSetGroup = oServerFilesSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFilesSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerFilesSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup)) {
					String strServerSettingsSignatureSetGroup = oServerSettingsSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettingsSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerSettingsSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
			}

			projectList = sqlQuery.list();

			count = projectList.size();
		} catch (ORMException orme) {
			params.put("type", type);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_SEARCHED_PROJECTS_COUNT, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SEARCHED_PROJECTS_COUNT, params, pe);
			throw pe;
		} finally {
			if (!CommonUtil.isListNullOrEmpty(projectList)) {
				projectList.clear();
				projectList = null;
			}

			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}

		return count;
	}

	/**
	 * Get Number of Searched User Projects
	 * 
	 * @param userId
	 *            id of the user
	 * @param type
	 *            a type of a project(android, cxsuite, ios)
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @return Number of searched user projects
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public int getSearchedUserProjectsCount(String userId, int type, Map<String, Object> searchedProject)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = 0;
		List<Project> projectList = new ArrayList<Project>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (CommonUtil.isStringNullOrEmpty(userId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}

			if (type == PortalConstants.INT_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			sqlQuery = this.generateUserSQLQuery(CustomSQLUtil.get(getClass(), GET_SEARCHED_USER_PROJECTS_COUNT),
					searchedProject);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(type);
			qPos.add(userId);

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
			
				if (!CommonUtil.isObjectNull(oProjectId)) {
					String strProjectId = oProjectId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
						long lProjectId = Long.parseLong(strProjectId);

						qPos.add(lProjectId);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					String strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseNumber.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Date dteProjectEndDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					try {
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					try {
						dteProjectEndDateHigh = format.parse(strProjectEndDateHigh);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteProjectEndDateLow) && !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateLow);
					qPos.add(dteProjectEndDateHigh);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					qPos.add(dteProjectEndDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateHigh);
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						int iStatus = Integer.parseInt(strStatus);

						qPos.add(iStatus);
					}
				}

				if (!CommonUtil.isObjectNull(oScanCount)) {
					String strScanCount = oScanCount.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
						int iScanCount = Integer.parseInt(strScanCount);

						qPos.add(iScanCount);
					}
				}
				
				if (!CommonUtil.isObjectNull(oCaseName)) {
					String strCaseName = oCaseName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl)) {
					String strTargetUrl = oTargetUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strTargetUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strTargetUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl)) {
					String strProductionEnvironmentUrl = oProductionEnvironmentUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProductionEnvironmentUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProductionEnvironmentUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup)) {
					String strWebInspectionSignatureSetGroup = oWebInspectionSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strWebInspectionSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strWebInspectionSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles)) {
					String strServerFiles = oServerFiles.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFiles)) {
						long lServerFiles = Long.parseLong(strServerFiles);

						qPos.add(lServerFiles);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings)) {
					String strServerSettings = oServerSettings.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettings)) {
						long lServerSettings = Long.parseLong(strServerSettings);

						qPos.add(lServerSettings);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup)) {
					String strServerFilesSignatureSetGroup = oServerFilesSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFilesSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerFilesSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup)) {
					String strServerSettingsSignatureSetGroup = oServerSettingsSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettingsSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerSettingsSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
			}

			projectList = sqlQuery.list();

			count = projectList.size();
		} catch (ORMException orme) {
			params.put("userId", userId);
			params.put("type", type);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_SEARCHED_USER_PROJECTS_COUNT, params,
					orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION);
		} catch (PortalException pe) {
			params.put("userId", userId);
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SEARCHED_USER_PROJECTS_COUNT, params, pe);
			throw pe;
		} finally {
			if (!CommonUtil.isListNullOrEmpty(projectList)) {
				projectList.clear();
				projectList = null;
			}

			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}

		return count;
	}

	/**
	 * Sort overall admin projects
	 * 
	 * @param lUserId
	 *            user id
	 * @param iType
	 *            a type of the project(android, cxsuite, ios)
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param orderByCol
	 *            a column to order
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param iStart
	 *            first index position
	 * @param iEnd
	 *            last index position
	 * @return projectList a sorted project list
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> sortOverallAdminProjects(long lUserId, int iUserRole, int iType, Map<String, Object> searchedProject, String orderByCol,
			String orderByType, int iStart, int iEnd) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		long[] userOrgIdList = null;

		try {
			if (iType == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			sqlQuery = this.generateSortOverallAdminSQLQuery(lUserId, iUserRole, CustomSQLUtil.get(getClass(), SORT_OVERALL_ADMIN_PROJECTS),
					searchedProject, orderByType, orderByCol);
			qPos = QueryPos.getInstance(sqlQuery);

			qPos.add(iType);
			
			if(iUserRole == UserRole.GROUP_ADMIN.getInteger() && lUserId != PortalConstants.LONG_ZERO){
				User user = UserLocalServiceUtil.getUser(lUserId);
				userOrgIdList = user.getOrganizationIds();
				
				if(!CommonUtil.isObjectNull(userOrgIdList) && userOrgIdList.length > PortalConstants.INT_ZERO){
					long[] userOrgIdArr = new long[userOrgIdList.length];
					int index = PortalConstants.INT_ZERO;
					for (long userOrgId : userOrgIdList) {
						if (userOrgId != PortalConstants.LONG_ZERO) {
							userOrgIdArr[index] = userOrgId;
							index++;
						}
					}
					qPos.add(userOrgIdArr);
				}
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
				
				if (!CommonUtil.isObjectNull(oProjectId)) {
					String strProjectId = oProjectId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
						long lProjectId = Long.parseLong(strProjectId);

						qPos.add(lProjectId);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					String strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseNumber.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Calendar dteProjectEndDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					try {
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					try {
						dteProjectEndDateHigh = Calendar.getInstance();
						dteProjectEndDateHigh.setTime(format.parse(strProjectEndDateHigh));
						dteProjectEndDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteProjectEndDateHigh.set(Calendar.MINUTE, 59);
						dteProjectEndDateHigh.set(Calendar.SECOND, 59);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteProjectEndDateLow) && !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateLow);
					qPos.add(dteProjectEndDateHigh.getTime());
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					qPos.add(dteProjectEndDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateHigh.getTime());
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						qPos.add(Integer.parseInt(strStatus));
					}
				}

				if (!CommonUtil.isObjectNull(oScanCount)) {
					String strScanCount = oScanCount.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
						qPos.add(Integer.parseInt(strScanCount));
					}
				}
				
				if (!CommonUtil.isObjectNull(oCaseName)) {
					String strCaseName = oCaseName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl)) {
					String strTargetUrl = oTargetUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strTargetUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strTargetUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl)) {
					String strProductionEnvironmentUrl = oProductionEnvironmentUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProductionEnvironmentUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProductionEnvironmentUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup)) {
					String strWebInspectionSignatureSetGroup = oWebInspectionSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strWebInspectionSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strWebInspectionSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles)) {
					String strServerFiles = oServerFiles.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFiles)) {
						long lServerFiles = Long.parseLong(strServerFiles);

						qPos.add(lServerFiles);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings)) {
					String strServerSettings = oServerSettings.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettings)) {
						long lServerSettings = Long.parseLong(strServerSettings);

						qPos.add(lServerSettings);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup)) {
					String strServerFilesSignatureSetGroup = oServerFilesSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFilesSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerFilesSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup)) {
					String strServerSettingsSignatureSetGroup = oServerSettingsSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettingsSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerSettingsSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
			}

			qPos.add(iStart);
			qPos.add(iEnd);

			projectList = sqlQuery.list();

			resultList = this.getOverallAdminResults(projectList);
		} catch (ORMException orme) {
			params.put("iType", iType);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_OVERALL_ADMIN_PROJECTS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("iType", iType);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_OVERALL_ADMIN_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!projectList.isEmpty()) {
				projectList.clear();
				projectList = null;
			}
			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Sort User Projects
	 * 
	 * @param iType
	 *            a type of the project(android, cxsuite, ios)
	 * @param strUserId
	 *            id of the User
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param orderByType
	 *            the type of order(ascending, descending)
	 * @param orderByCol
	 *            a column to order
	 * @param iStart
	 *            index for the start position
	 * @param iEnd
	 *            index for the last position
	 * @return projectList a sorted project list
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> sortUserProjects(int iType, String strUserId, Map<String, Object> searchedProject,
			String orderByType, String orderByCol, int iStart, int iEnd) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			if (iType == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			if (CommonUtil.isStringNullOrEmpty(strUserId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String
						.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new PortalException(PortalErrors.INVALID_USER);
			}

			sqlQuery = this.generateSortUserSQLQuery(CustomSQLUtil.get(getClass(), SORT_USER_PROJECTS), searchedProject,
					orderByType, orderByCol);
			qPos = QueryPos.getInstance(sqlQuery);

			qPos.add(iType);
			qPos.add(strUserId);

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
				
				if (!CommonUtil.isObjectNull(oProjectId)) {
					String strProjectId = oProjectId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
						long lProjectId = Long.parseLong(strProjectId);

						qPos.add(lProjectId);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					String strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseNumber.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Date dteProjectEndDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					try {
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					try {
						dteProjectEndDateHigh = format.parse(strProjectEndDateHigh);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteProjectEndDateLow) && !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateLow);
					qPos.add(dteProjectEndDateHigh);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					qPos.add(dteProjectEndDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateHigh);
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						int iStatus = Integer.parseInt(strStatus);

						qPos.add(iStatus);
					}
				}

				if (!CommonUtil.isObjectNull(oScanCount)) {
					String strScanCount = oScanCount.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
						int iScanCount = Integer.parseInt(strScanCount);

						qPos.add(iScanCount);
					}
				}
				
				if (!CommonUtil.isObjectNull(oCaseName)) {
					String strCaseName = oCaseName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl)) {
					String strTargetUrl = oTargetUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strTargetUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strTargetUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl)) {
					String strProductionEnvironmentUrl = oProductionEnvironmentUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProductionEnvironmentUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProductionEnvironmentUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup)) {
					String strWebInspectionSignatureSetGroup = oWebInspectionSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strWebInspectionSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strWebInspectionSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles)) {
					String strServerFiles = oServerFiles.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFiles)) {
						long lServerFiles = Long.parseLong(strServerFiles);

						qPos.add(lServerFiles);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings)) {
					String strServerSettings = oServerSettings.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettings)) {
						long lServerSettings = Long.parseLong(strServerSettings);

						qPos.add(lServerSettings);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup)) {
					String strServerFilesSignatureSetGroup = oServerFilesSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFilesSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerFilesSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup)) {
					String strServerSettingsSignatureSetGroup = oServerSettingsSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettingsSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerSettingsSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
			}
			qPos.add(iStart);
			qPos.add(iEnd);

			projectList = sqlQuery.list();

			resultList = this.getUserResults(projectList);
		} catch (ORMException orme) {
			params.put("iType", iType);
			params.put("strUserId", strUserId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_USER_PROJECTS, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("iType", iType);
			params.put("strUserId", strUserId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_USER_PROJECTS, params, pe);
			throw pe;
		} finally {
			if (!projectList.isEmpty()) {
				projectList.clear();
				projectList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Get Downloadable overall admin projects
	 * 
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @return List of overall admin projects
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getOverallAdminProjectsToDownload(long lUserId, int iUserRole, int type, Map<String, Object> searchedProject)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		List<Object> resultList = new ArrayList<Object>();
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;
		long[] userOrgIdList = null;
		
		try {
			if (type == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new PortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}

			sqlQuery = this.generateOverallAdminSQLQuery(lUserId, iUserRole, CustomSQLUtil.get(getClass(), GET_OVERALL_ADMIN_PROJECTS_TO_DOWNLOAD), searchedProject);
			qPos = QueryPos.getInstance(sqlQuery);
			qPos.add(type);

			if(iUserRole == UserRole.GROUP_ADMIN.getInteger() && lUserId != PortalConstants.LONG_ZERO){
				User user = UserLocalServiceUtil.getUser(lUserId);
				userOrgIdList = user.getOrganizationIds();
				
				if(!CommonUtil.isObjectNull(userOrgIdList) && userOrgIdList.length > PortalConstants.INT_ZERO){
					long[] userOrgIdArr = new long[userOrgIdList.length];
					int index = PortalConstants.INT_ZERO;
					for (long userOrgId : userOrgIdList) {
						if (userOrgId != PortalConstants.LONG_ZERO) {
							userOrgIdArr[index] = userOrgId;
							index++;
						}
					}
					qPos.add(userOrgIdArr);
				}
			}
			
			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
				
				if (!CommonUtil.isObjectNull(oProjectId)) {
					String strProjectId = oProjectId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
						long lProjectId = Long.parseLong(strProjectId);

						qPos.add(lProjectId);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					String strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseNumber.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Date dteProjectEndDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					try {
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					try {
						dteProjectEndDateHigh = format.parse(strProjectEndDateHigh);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteProjectEndDateLow) && !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateLow);
					qPos.add(dteProjectEndDateHigh);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					qPos.add(dteProjectEndDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateHigh);
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						qPos.add(Integer.parseInt(strStatus));
					}
				}

				if (!CommonUtil.isObjectNull(oScanCount)) {
					String strScanCount = oScanCount.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
						qPos.add(Integer.parseInt(strScanCount));
					}
				}
				
				if (!CommonUtil.isObjectNull(oCaseName)) {
					String strCaseName = oCaseName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl)) {
					String strTargetUrl = oTargetUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strTargetUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strTargetUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl)) {
					String strProductionEnvironmentUrl = oProductionEnvironmentUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProductionEnvironmentUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProductionEnvironmentUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup)) {
					String strWebInspectionSignatureSetGroup = oWebInspectionSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strWebInspectionSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strWebInspectionSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles)) {
					String strServerFiles = oServerFiles.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFiles)) {
						long lServerFiles = Long.parseLong(strServerFiles);

						qPos.add(lServerFiles);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings)) {
					String strServerSettings = oServerSettings.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettings)) {
						long lServerSettings = Long.parseLong(strServerSettings);

						qPos.add(lServerSettings);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup)) {
					String strServerFilesSignatureSetGroup = oServerFilesSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFilesSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerFilesSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup)) {
					String strServerSettingsSignatureSetGroup = oServerSettingsSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettingsSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerSettingsSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
			}

			projectList = sqlQuery.list();

			resultList = this.getOverallAdminResults(projectList);
		} catch (ORMException orme) {
			params.put("type", type);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_OVERALL_ADMIN_PROJECTS_TO_DOWNLOAD, params,
					orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("type", type);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_OVERALL_ADMIN_PROJECTS_TO_DOWNLOAD, params, pe);
			throw pe;
		} catch (Exception e) {
			params.put("iType", type);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_OVERALL_ADMIN_PROJECTS_TO_DOWNLOAD,
					params, e);
			throw new PortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		} finally {
			if (!projectList.isEmpty()) {
				projectList.clear();
				projectList = null;
			}
			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Get Downloadable user porjects
	 * 
	 * @param type
	 *            a type of the project(android, cxsuite, ios)
	 * @param userId
	 *            id of the user
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @return List of user projects
	 * @throws PortalException
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getUserProjectsToDownload(int type, String userId, Map<String, Object> searchedProject)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = null;
		List<Object> resultList = null;
		SQLQuery sqlQuery = null;
		QueryPos qPos = null;

		try {
			sqlQuery = this.generateUserSQLQuery(CustomSQLUtil.get(getClass(), GET_USER_PROJECTS_TO_DOWNLOAD),
					searchedProject);
			qPos = QueryPos.getInstance(sqlQuery);

			qPos.add(type);
			qPos.add(userId);

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
				
				if (!CommonUtil.isObjectNull(oProjectId)) {
					String strProjectId = oProjectId.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
						long lProjectId = Long.parseLong(strProjectId);

						qPos.add(lProjectId);
					}
				}

				if (!CommonUtil.isObjectNull(oGroupName)) {
					String strGroupName = oGroupName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strGroupName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oCaseNumber)) {
					String strCaseNumber = oCaseNumber.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseNumber.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				if (!CommonUtil.isObjectNull(oProjectName)) {
					String strProjectName = oProjectName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProjectName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Date dteProjectEndDateHigh = null;

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					try {
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					try {
						dteProjectEndDateHigh = format.parse(strProjectEndDateHigh);
					} catch (ParseException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new PortalException(PortalErrors.INVALID_PROJECT_END_DATE);
					}
				}

				if (!CommonUtil.isObjectNull(dteProjectEndDateLow) && !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateLow);
					qPos.add(dteProjectEndDateHigh);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					qPos.add(dteProjectEndDateLow);
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					qPos.add(dteProjectEndDateHigh);
				}

				if (!CommonUtil.isObjectNull(oStatus)) {
					String strStatus = oStatus.toString();

					if (!CommonUtil.isStringNullOrEmpty(strStatus)) {
						int iStatus = Integer.parseInt(strStatus);

						qPos.add(iStatus);
					}
				}

				if (!CommonUtil.isObjectNull(oScanCount)) {
					String strScanCount = oScanCount.toString();

					if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
						int iScanCount = Integer.parseInt(strScanCount);

						qPos.add(iScanCount);
					}
				}
				
				if (!CommonUtil.isObjectNull(oCaseName)) {
					String strCaseName = oCaseName.toString();

					if (!CommonUtil.isStringNullOrEmpty(strCaseName)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strCaseName.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl)) {
					String strTargetUrl = oTargetUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strTargetUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strTargetUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl)) {
					String strProductionEnvironmentUrl = oProductionEnvironmentUrl.toString();

					if (!CommonUtil.isStringNullOrEmpty(strProductionEnvironmentUrl)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strProductionEnvironmentUrl.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup)) {
					String strWebInspectionSignatureSetGroup = oWebInspectionSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strWebInspectionSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strWebInspectionSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles)) {
					String strServerFiles = oServerFiles.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFiles)) {
						long lServerFiles = Long.parseLong(strServerFiles);

						qPos.add(lServerFiles);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings)) {
					String strServerSettings = oServerSettings.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettings)) {
						long lServerSettings = Long.parseLong(strServerSettings);

						qPos.add(lServerSettings);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup)) {
					String strServerFilesSignatureSetGroup = oServerFilesSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerFilesSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerFilesSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup)) {
					String strServerSettingsSignatureSetGroup = oServerSettingsSignatureSetGroup.toString();

					if (!CommonUtil.isStringNullOrEmpty(strServerSettingsSignatureSetGroup)) {
						qPos.add(PortalConstants.SQL_WILDCARD_PERCENT + strServerSettingsSignatureSetGroup.toLowerCase()
								+ PortalConstants.SQL_WILDCARD_PERCENT);
					}
				}
			}

			projectList = sqlQuery.list();

			resultList = this.getUserResults(projectList);
		} catch (ORMException orme) {
			params.put("iType", type);
			params.put("strUserId", userId);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_USER_PROJECTS_TO_DOWNLOAD, params, orme);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, orme);
		} catch (PortalException pe) {
			params.put("iType", type);
			params.put("strUserId", userId);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_USER_PROJECTS_TO_DOWNLOAD, params, pe);
			throw pe;
		} catch (Exception e) {
			params.put("iType", type);
			params.put("strUserId", userId);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_USER_PROJECTS_TO_DOWNLOAD, params,
					e);
			throw new PortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		} finally {
			if (!projectList.isEmpty()) {
				projectList.clear();
				projectList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Generate sql query for overall admin
	 * 
	 * @param strSql
	 *            Sql
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @return sql query object
	 * @throws ORMException
	 */
	private SQLQuery generateOverallAdminSQLQuery(long lUserId, int iUserRole, String strSql, Map<String, Object> searchedProject)
			throws ORMException, PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		long[] userOrgIdList = null;
		
		try {
			
			if (iUserRole == UserRole.GROUP_ADMIN.getInteger()){
				User user = UserLocalServiceUtil.getUser(lUserId);
				userOrgIdList = user.getOrganizationIds();
				
				if(!CommonUtil.isObjectNull(userOrgIdList) && userOrgIdList.length > PortalConstants.INT_ZERO){
					String placeHolders = PortalConstants.STRING_EMPTY;
					for (int i = PortalConstants.INT_ZERO; i < userOrgIdList.length; i++){
						placeHolders += PortalConstants.STRING_QUESTION_MARK;
						if (i < userOrgIdList.length - PortalConstants.INT_ONE){
							placeHolders += PortalConstants.COMMA;
						}
					}
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_0, 
							PortalConstants.STRING_AND_STATEMENT + " project.ownergroup IN (" + placeHolders + ")");
				}
			}
			
			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
				String strStatement = PortalConstants.STRING_WHERE_STATEMENT;
				
				if (!CommonUtil.isObjectNull(oProjectId) && !CommonUtil.isStringNullOrEmpty(oProjectId.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "result.projectid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oGroupName) && !CommonUtil.isStringNullOrEmpty(oGroupName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_2,
							strStatement + "LOWER(result.groupname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oCaseNumber) && !CommonUtil.isStringNullOrEmpty(oCaseNumber.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_3,
							strStatement + "LOWER(result.casenumber) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oProjectName)
						&& !CommonUtil.isStringNullOrEmpty(oProjectName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_4,
							strStatement + "LOWER(result.projectname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate >= ? AND result.projectenddate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oStatus) && !CommonUtil.isStringNullOrEmpty(oStatus.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_6, strStatement + "result.status = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oScanCount) && !CommonUtil.isStringNullOrEmpty(oScanCount.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "result.scancount = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oCaseName) && !CommonUtil.isStringNullOrEmpty(oCaseName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_8,
							strStatement + "LOWER(result.casename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl) && !CommonUtil.isStringNullOrEmpty(oTargetUrl.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_9,
							strStatement + "LOWER(result.targeturl) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl) && !CommonUtil.isStringNullOrEmpty(oProductionEnvironmentUrl.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_10,
							strStatement + "LOWER(result.productionenvironmenturl) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oWebInspectionSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_11,
							strStatement + "LOWER(result.selectedwebsignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles) && !CommonUtil.isStringNullOrEmpty(oServerFiles.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "result.getserverfiles = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings) && !CommonUtil.isStringNullOrEmpty(oServerSettings.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_13, strStatement + "result.getserversettings = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oServerFilesSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_14,
							strStatement + "LOWER(result.selectedserverfilessignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oServerSettingsSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_15,
							strStatement + "LOWER(result.selectedserversettingssignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
			}

			for (int index = 0; index < 16; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (strSql.contains(strPlaceHolder)) {
					strSql = strSql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(strSql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("groupId", Type.LONG);
			sqlQuery.addScalar("cxAndroidProjectId", Type.STRING);
			sqlQuery.addScalar("projectName", Type.STRING);
			sqlQuery.addScalar("caseNumber", Type.STRING);
			sqlQuery.addScalar("status", Type.INTEGER);
			sqlQuery.addScalar("attribute", Type.INTEGER);
			sqlQuery.addScalar("projectEndDate", Type.DATE);
			sqlQuery.addScalar("projectCreateDate", Type.DATE);
			sqlQuery.addScalar("checklistFileName", Type.STRING);
			sqlQuery.addScalar("scanCount", Type.INTEGER);
			sqlQuery.addScalar("groupname", Type.STRING);
			sqlQuery.addScalar("casename", Type.STRING);
			sqlQuery.addScalar("targeturl", Type.STRING);
			sqlQuery.addScalar("productionenvironmenturl", Type.STRING);
			sqlQuery.addScalar("selectedwebsignature", Type.STRING);
			sqlQuery.addScalar("getserverfiles", Type.INTEGER);
			sqlQuery.addScalar("getserversettings", Type.INTEGER);
			sqlQuery.addScalar("selectedserverfilessignature", Type.STRING);
			sqlQuery.addScalar("selectedserversettingssignature", Type.STRING);
		} catch (ORMException | PortalException orme) {
			params.put("strSql", strSql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_OVERALL_ADMIN_SQL_QUERY, params,
					orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}

	/**
	 * Generate sql query for user
	 * 
	 * @param strSql
	 *            SQL
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @return sql query object
	 * @throws ORMException
	 */
	private SQLQuery generateUserSQLQuery(String strSql, Map<String, Object> searchedProject) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		try {
			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;

				if (!CommonUtil.isObjectNull(oProjectId) && !CommonUtil.isStringNullOrEmpty(oProjectId.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "result.projectid = ?");
				}

				if (!CommonUtil.isObjectNull(oGroupName) && !CommonUtil.isStringNullOrEmpty(oGroupName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_2,
							strStatement + "LOWER(result.groupname) LIKE ?");
				}

				if (!CommonUtil.isObjectNull(oCaseNumber) && !CommonUtil.isStringNullOrEmpty(oCaseNumber.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_3,
							strStatement + "LOWER(result.casenumber) LIKE ?");
				}

				if (!CommonUtil.isObjectNull(oProjectName)
						&& !CommonUtil.isStringNullOrEmpty(oProjectName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_4,
							strStatement + "LOWER(result.projectname) LIKE ?");
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate >= ?");
				} else if (CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate <= ?");
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate >= ? AND result.projectenddate <= ?");
				}

				if (!CommonUtil.isObjectNull(oStatus) && !CommonUtil.isStringNullOrEmpty(oStatus.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_6, strStatement + "result.status = ?");
				}

				if (!CommonUtil.isObjectNull(oScanCount) && !CommonUtil.isStringNullOrEmpty(oScanCount.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "result.scancount = ?");
				}
				
				if (!CommonUtil.isObjectNull(oCaseName) && !CommonUtil.isStringNullOrEmpty(oCaseName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_8,
							strStatement + "LOWER(result.casename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl) && !CommonUtil.isStringNullOrEmpty(oTargetUrl.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_9,
							strStatement + "LOWER(result.targeturl) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl) && !CommonUtil.isStringNullOrEmpty(oProductionEnvironmentUrl.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_10,
							strStatement + "LOWER(result.productionenvironmenturl) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oWebInspectionSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_11,
							strStatement + "LOWER(result.selectedwebsignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles) && !CommonUtil.isStringNullOrEmpty(oServerFiles.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "result.getserverfiles = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings) && !CommonUtil.isStringNullOrEmpty(oServerSettings.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_13, strStatement + "result.getserversettings = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oServerFilesSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_14,
							strStatement + "LOWER(result.selectedserverfilessignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oServerSettingsSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_15,
							strStatement + "LOWER(result.selectedserversettingssignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
			}

			for (int index = 1; index < 16; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (strSql.contains(strPlaceHolder)) {
					strSql = strSql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(strSql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("groupId", Type.LONG);
			sqlQuery.addScalar("cxAndroidProjectId", Type.STRING);
			sqlQuery.addScalar("projectName", Type.STRING);
			sqlQuery.addScalar("caseNumber", Type.STRING);
			sqlQuery.addScalar("status", Type.INTEGER);
			sqlQuery.addScalar("attribute", Type.INTEGER);
			sqlQuery.addScalar("projectEndDate", Type.DATE);
			sqlQuery.addScalar("projectCreateDate", Type.DATE);
			sqlQuery.addScalar("checklistFileName", Type.STRING);
			sqlQuery.addScalar("scanCount", Type.INTEGER);
			sqlQuery.addScalar("groupName", Type.STRING);
			sqlQuery.addScalar("caseName", Type.STRING);
			sqlQuery.addScalar("targetUrl", Type.STRING);
			sqlQuery.addScalar("productionEnvironmentUrl", Type.STRING);
			sqlQuery.addScalar("selectedwebsignature", Type.STRING);
			sqlQuery.addScalar("getserverfiles", Type.INTEGER);
			sqlQuery.addScalar("getserversettings", Type.INTEGER);
			sqlQuery.addScalar("selectedserverfilessignature", Type.STRING);
			sqlQuery.addScalar("selectedserversettingssignature", Type.STRING);
		} catch (ORMException orme) {
			params.put("strSql", strSql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_USER_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}

	/**
	 * Creates Overall Admin Projects Data
	 * 
	 * @param projectList
	 *            List of projects
	 * @return List of overall Projects
	 * @throws PortalException
	 */
	private List<Object> getOverallAdminResults(List<Object> projectList) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> resultList = new ArrayList<Object>();
		Date date = null;
		String serializeString = null;
		JSONArray jsonArray = null;
		ProjectItem item = null;

		try {
			for (Object project : projectList) {
				serializeString = JSONFactoryUtil.serialize(project);
				jsonArray = JSONFactoryUtil.createJSONArray(serializeString);
				item = new ProjectItem();

				item.setProjectId(jsonArray.getLong(0));
				item.setOwnerGroupId(jsonArray.getInt(1));
				item.setCxAndroidProjectId(jsonArray.getString(2));
				item.setProjectName(jsonArray.getString(3));
				item.setCaseNumber(jsonArray.getString(4));
				item.setStatus(jsonArray.getInt(5));
				item.setAttribute(jsonArray.getInt(6));
				date = new Date();
				date.setTime(jsonArray.getJSONObject(7).getLong("time"));
				item.setProjectEndDate(date);
				date = new Date();
				date.setTime(jsonArray.getJSONObject(8).getLong("time"));
				item.setProjectCreateDate(date);
				item.setChecklistFileName(jsonArray.getString(9));
				item.setScanCount(jsonArray.getInt(10));
				item.setOwnerGroupName(jsonArray.getString(11));
				item.setCaseName(jsonArray.getString(12));
				item.setTargetUrl(jsonArray.getString(13));
				item.setProductionEnvironmentUrl(jsonArray.getString(14));
				item.setSelectedWebSignature(jsonArray.getString(15));
				item.setGetServerFiles(jsonArray.getInt(16));
				item.setGetServerSettings(jsonArray.getInt(17));
				item.setSelectedServerFilesSignature(jsonArray.getString(18));
				item.setSelectedServerSettingsSignature(jsonArray.getString(19));
				resultList.add(item);
			}
		} catch (JSONException jsone) {
			params.put("projectList", projectList);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_OVERALL_ADMIN_RESULTS, params, jsone);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, jsone);
		} finally {
			if (!projectList.isEmpty()) {
				projectList.clear();
				projectList = null;
			}

			params.clear();
			params = null;
		}

		return resultList;
	}

	/**
	 * Create User Data
	 * 
	 * @param projectList
	 *            list of projects
	 * @return List of User
	 * @throws PortalException
	 */
	private List<Object> getUserResults(List<Object> projectList) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> resultList = new ArrayList<Object>();
		Date date = null;
		String serializeString = null;
		JSONArray jsonArray = null;
		ProjectItem item = null;

		try {
			for (Object project : projectList) {
				serializeString = JSONFactoryUtil.serialize(project);
				jsonArray = JSONFactoryUtil.createJSONArray(serializeString);
				item = new ProjectItem();

				item.setProjectId(jsonArray.getLong(0));
				item.setOwnerGroupId(jsonArray.getInt(1));
				item.setCxAndroidProjectId(jsonArray.getString(2));
				item.setProjectName(jsonArray.getString(3));
				item.setCaseNumber(jsonArray.getString(4));
				item.setStatus(jsonArray.getInt(5));
				item.setAttribute(jsonArray.getInt(6));
				date = new Date();
				date.setTime(jsonArray.getJSONObject(7).getLong("time"));
				item.setProjectEndDate(date);
				date = new Date();
				date.setTime(jsonArray.getJSONObject(8).getLong("time"));
				item.setProjectCreateDate(date);
				item.setChecklistFileName(jsonArray.getString(9));
				item.setScanCount(jsonArray.getInt(10));
				item.setOwnerGroupName(jsonArray.getString(11));
				item.setCaseName(jsonArray.getString(12));
				item.setTargetUrl(jsonArray.getString(13));
				item.setProductionEnvironmentUrl(jsonArray.getString(14));
				item.setSelectedWebSignature(jsonArray.getString(15));
				item.setGetServerFiles(jsonArray.getInt(16));
				item.setGetServerSettings(jsonArray.getInt(17));
				item.setSelectedServerFilesSignature(jsonArray.getString(18));
				item.setSelectedServerSettingsSignature(jsonArray.getString(19));
				resultList.add(item);
			}
		} catch (JSONException jsone) {
			params.put("projectList", projectList);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_USER_RESULTS, params, jsone);
			throw new PortalException(PortalErrors.ORM_EXCEPTION, jsone);
		} finally {
			if (!CommonUtil.isListNullOrEmpty(projectList)) {
				projectList.clear();
				projectList = null;
			}

			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}

		return resultList;
	}

	/**
	 * Generate SQL query for overall admin
	 * 
	 * @param strSql
	 *            SQL
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param orderByCol
	 *            a column to order
	 * @return SQLQUERY object
	 * @throws ORMException
	 */
	private SQLQuery generateSortOverallAdminSQLQuery(long lUserId, int iUserRole, String strSql, Map<String, Object> searchedProject,
			String orderByType, String orderByCol) throws ORMException, PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		long[] userOrgIdList = null;
		
		try {
			if (!CommonUtil.isStringNullOrEmpty(orderByCol) && !CommonUtil.isStringNullOrEmpty(orderByType)) {
				if (orderByCol.equals("groupName")) {
					orderByCol = "LOWER(org.name)";
				} else if (orderByCol.equals("scanCount")) {
					orderByCol = "count(scan.scanid)";
				} else if (orderByCol.equals("projectEndDate") || orderByCol.equals("projectId")
						|| orderByCol.equals("status")) {

				} else {
					orderByCol = "LOWER(" + orderByCol + ")";
				}

				if (strSql.contains("{orderBy}")) {
					strSql = strSql.replace("{orderBy}", orderByCol + PortalConstants.STRING_BLANK_SPACE + orderByType);
				}
			} else {
				strSql = strSql.replace("{orderBy}", PortalConstants.STRING_EMPTY);
			}
						
			if (iUserRole == UserRole.GROUP_ADMIN.getInteger()){
				User user = UserLocalServiceUtil.getUser(lUserId);
				userOrgIdList = user.getOrganizationIds();
				
				if(!CommonUtil.isObjectNull(userOrgIdList) && userOrgIdList.length > PortalConstants.INT_ZERO){
					String placeHolders = PortalConstants.STRING_EMPTY;
					for (int i = PortalConstants.INT_ZERO; i < userOrgIdList.length; i++){
						placeHolders += PortalConstants.STRING_QUESTION_MARK;
						if (i < userOrgIdList.length - PortalConstants.INT_ONE){
							placeHolders += PortalConstants.COMMA;
						}
					}
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_0, 
							PortalConstants.STRING_AND_STATEMENT + " project.ownergroup IN (" + placeHolders + ")");
				}
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
				String strStatement = PortalConstants.STRING_WHERE_STATEMENT;

				if (!CommonUtil.isObjectNull(oProjectId) && !CommonUtil.isStringNullOrEmpty(oProjectId.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "result.projectid = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oGroupName) && !CommonUtil.isStringNullOrEmpty(oGroupName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_2,
							strStatement + "LOWER(result.groupname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oCaseNumber) && !CommonUtil.isStringNullOrEmpty(oCaseNumber.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_3,
							strStatement + "LOWER(result.casenumber) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oProjectName)
						&& !CommonUtil.isStringNullOrEmpty(oProjectName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_4,
							strStatement + "LOWER(result.projectname) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate >= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate >= ? AND result.projectenddate <= ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oStatus) && !CommonUtil.isStringNullOrEmpty(oStatus.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_6, strStatement + "result.status = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}

				if (!CommonUtil.isObjectNull(oScanCount) && !CommonUtil.isStringNullOrEmpty(oScanCount.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "result.scancount = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oCaseName) && !CommonUtil.isStringNullOrEmpty(oCaseName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_8,
							strStatement + "LOWER(result.casename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl) && !CommonUtil.isStringNullOrEmpty(oTargetUrl.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_9,
							strStatement + "LOWER(result.targeturl) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl) && !CommonUtil.isStringNullOrEmpty(oProductionEnvironmentUrl.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_10,
							strStatement + "LOWER(result.productionenvironmenturl) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oWebInspectionSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_11,
							strStatement + "LOWER(result.selectedwebsignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles) && !CommonUtil.isStringNullOrEmpty(oServerFiles.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "result.getserverfiles = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings) && !CommonUtil.isStringNullOrEmpty(oServerSettings.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_13, strStatement + "result.getserversettings = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oServerFilesSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_14,
							strStatement + "LOWER(result.selectedserverfilessignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oServerSettingsSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_15,
							strStatement + "LOWER(result.selectedserversettingssignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
			}

			for (int index = 0; index < 16; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (strSql.contains(strPlaceHolder)) {
					strSql = strSql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(strSql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("groupId", Type.LONG);
			sqlQuery.addScalar("cxAndroidProjectId", Type.STRING);
			sqlQuery.addScalar("projectName", Type.STRING);
			sqlQuery.addScalar("caseNumber", Type.STRING);
			sqlQuery.addScalar("status", Type.INTEGER);
			sqlQuery.addScalar("attribute", Type.INTEGER);
			sqlQuery.addScalar("projectEndDate", Type.DATE);
			sqlQuery.addScalar("projectCreateDate", Type.DATE);
			sqlQuery.addScalar("checklistFileName", Type.STRING);
			sqlQuery.addScalar("scanCount", Type.INTEGER);
			sqlQuery.addScalar("groupname", Type.STRING);
			sqlQuery.addScalar("casename", Type.STRING);
			sqlQuery.addScalar("targeturl", Type.STRING);
			sqlQuery.addScalar("productionenvironmenturl", Type.STRING);
			sqlQuery.addScalar("selectedwebsignature", Type.STRING);
			sqlQuery.addScalar("getserverfiles", Type.INTEGER);
			sqlQuery.addScalar("getserversettings", Type.INTEGER);
			sqlQuery.addScalar("selectedserverfilessignature", Type.STRING);
			sqlQuery.addScalar("selectedserversettingssignature", Type.STRING);
		} catch (ORMException | PortalException orme) {
			params.put("strSql", strSql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_OVERALL_ADMIN_SQL_QUERY, params,
					orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}

	/**
	 * Generate sql query for sorting a user
	 * 
	 * @param strSql
	 *            SQL
	 * @param searchedProject
	 *            a key-value parameter inputted by user during search
	 * @param orderByType
	 *            type of order(ascending, descending)
	 * @param orderByCol
	 *            a column to order
	 * @return SQLQUERY object
	 * @throws ORMException
	 */
	private SQLQuery generateSortUserSQLQuery(String strSql, Map<String, Object> searchedProject, String orderByType,
			String orderByCol) throws ORMException {
		Map<String, Object> params = new HashMap<String, Object>();
		Session session = null;
		SQLQuery sqlQuery = null;
		try {
			if (!CommonUtil.isStringNullOrEmpty(orderByCol) && !CommonUtil.isStringNullOrEmpty(orderByType)) {
				if (orderByCol.equals("projectEndDate") || orderByCol.equals("projectId") || orderByCol.equals("status")
						|| orderByCol.equals("scanCount") || orderByCol.equals("groupName")) {

				} else {
					orderByCol = "LOWER(" + orderByCol + ")";
				}

				if (strSql.contains("{orderBy}")) {
					strSql = strSql.replace("{orderBy}", orderByCol + PortalConstants.STRING_BLANK_SPACE + orderByType);
				}
			} else {
				strSql = strSql.replace("{orderBy}", PortalConstants.STRING_EMPTY);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
				Object oGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
				Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
				Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
				Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
				Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
				Object oStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
				Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
				Object oCaseName = searchedProject.get(PortalConstants.PARAM_CASE_NAME);
				Object oTargetUrl = searchedProject.get(PortalConstants.PARAM_TARGET_URL);
				Object oProductionEnvironmentUrl = searchedProject.get(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL);
				Object oWebInspectionSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				Object oServerFiles = searchedProject.get(PortalConstants.PARAM_SERVER_FILES);
				Object oServerSettings = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS);
				Object oServerFilesSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				Object oServerSettingsSignatureSetGroup = searchedProject.get(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
				String strStatement = PortalConstants.STRING_AND_STATEMENT;

				if (!CommonUtil.isObjectNull(oProjectId) && !CommonUtil.isStringNullOrEmpty(oProjectId.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_1, strStatement + "result.projectid = ?");
				}

				if (!CommonUtil.isObjectNull(oGroupName) && !CommonUtil.isStringNullOrEmpty(oGroupName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_2,
							strStatement + "LOWER(result.groupname) LIKE ?");
				}

				if (!CommonUtil.isObjectNull(oCaseNumber) && !CommonUtil.isStringNullOrEmpty(oCaseNumber.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_3,
							strStatement + "LOWER(result.casenumber) LIKE ?");
				}

				if (!CommonUtil.isObjectNull(oProjectName)
						&& !CommonUtil.isStringNullOrEmpty(oProjectName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_4,
							strStatement + "LOWER(result.projectname) LIKE ?");
				}

				String strProjectEndDateLow = null;
				String strProjectEndDateHigh = null;

				if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
					strProjectEndDateLow = oProjectEndDateLow.toString();
				}

				if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
					strProjectEndDateHigh = oProjectEndDateHigh.toString();
				}

				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate >= ?");
				} else if (CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate <= ?");
				} else if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)
						&& !CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_5,
							strStatement + "result.projectenddate >= ? AND result.projectenddate <= ?");
				}

				if (!CommonUtil.isObjectNull(oStatus) && !CommonUtil.isStringNullOrEmpty(oStatus.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_6, strStatement + "result.status = ?");
				}

				if (!CommonUtil.isObjectNull(oScanCount) && !CommonUtil.isStringNullOrEmpty(oScanCount.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_7, strStatement + "result.scancount = ?");
				}
				
				if (!CommonUtil.isObjectNull(oCaseName) && !CommonUtil.isStringNullOrEmpty(oCaseName.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_8,
							strStatement + "LOWER(result.casename) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oTargetUrl) && !CommonUtil.isStringNullOrEmpty(oTargetUrl.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_9,
							strStatement + "LOWER(result.targeturl) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oProductionEnvironmentUrl) && !CommonUtil.isStringNullOrEmpty(oProductionEnvironmentUrl.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_10,
							strStatement + "LOWER(result.productionenvironmenturl) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oWebInspectionSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oWebInspectionSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_11,
							strStatement + "LOWER(result.selectedwebsignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerFiles) && !CommonUtil.isStringNullOrEmpty(oServerFiles.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_12, strStatement + "result.getserverfiles = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerSettings) && !CommonUtil.isStringNullOrEmpty(oServerSettings.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_13, strStatement + "result.getserversettings = ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerFilesSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oServerFilesSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_14,
							strStatement + "LOWER(result.selectedserverfilessignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
				
				if (!CommonUtil.isObjectNull(oServerSettingsSignatureSetGroup) && !CommonUtil.isStringNullOrEmpty(oServerSettingsSignatureSetGroup.toString())) {
					strSql = strSql.replace(PortalConstants.SQL_PLACEHOLDER_15,
							strStatement + "LOWER(result.selectedserversettingssignature) LIKE ?");
					strStatement = PortalConstants.STRING_AND_STATEMENT;
				}
			}

			for (int index = 1; index < 16; index++) {
				String strPlaceHolder = PortalConstants.STRING_OPEN_CURLY_BRACE + index
						+ PortalConstants.STRING_CLOSE_CURLY_BRACE;
				if (strSql.contains(strPlaceHolder)) {
					strSql = strSql.replace(strPlaceHolder, PortalConstants.STRING_EMPTY);
				}
			}

			session = openSession();
			sqlQuery = session.createSQLQuery(strSql);
			sqlQuery.setCacheable(false);
			sqlQuery.addScalar("projectId", Type.LONG);
			sqlQuery.addScalar("groupId", Type.LONG);
			sqlQuery.addScalar("cxAndroidProjectId", Type.STRING);
			sqlQuery.addScalar("projectName", Type.STRING);
			sqlQuery.addScalar("caseNumber", Type.STRING);
			sqlQuery.addScalar("status", Type.INTEGER);
			sqlQuery.addScalar("attribute", Type.INTEGER);
			sqlQuery.addScalar("projectEndDate", Type.DATE);
			sqlQuery.addScalar("projectCreateDate", Type.DATE);
			sqlQuery.addScalar("checklistFileName", Type.STRING);
			sqlQuery.addScalar("scanCount", Type.INTEGER);
			sqlQuery.addScalar("groupName", Type.STRING);
			sqlQuery.addScalar("casename", Type.STRING);
			sqlQuery.addScalar("targeturl", Type.STRING);
			sqlQuery.addScalar("productionenvironmenturl", Type.STRING);
			sqlQuery.addScalar("selectedwebsignature", Type.STRING);
			sqlQuery.addScalar("getserverfiles", Type.INTEGER);
			sqlQuery.addScalar("getserversettings", Type.INTEGER);
			sqlQuery.addScalar("selectedserverfilessignature", Type.STRING);
			sqlQuery.addScalar("selectedserversettingssignature", Type.STRING);
		} catch (ORMException orme) {
			params.put("strSql", strSql);
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GENERATE_USER_SQL_QUERY, params, orme);
			throw orme;
		} finally {
			closeSession(session);
			params.clear();
			params = null;
		}

		return sqlQuery;
	}
}
