/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;
import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResultModel;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the VexDetectionResult service. Represents a row in the &quot;UBS_VexDetectionResult&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link VexDetectionResultModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link VexDetectionResultImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResultImpl
 * @see VexDetectionResult
 * @see VexDetectionResultModel
 * @generated
 */
@ProviderType
public class VexDetectionResultModelImpl extends BaseModelImpl<VexDetectionResult>
	implements VexDetectionResultModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a vex detection result model instance should use the {@link VexDetectionResult} interface instead.
	 */
	public static final String TABLE_NAME = "UBS_VexDetectionResult";
	public static final Object[][] TABLE_COLUMNS = {
			{ "scanresultid", Types.BIGINT },
			{ "scanid", Types.BIGINT },
			{ "detectionresultid", Types.VARCHAR },
			{ "risklevel", Types.INTEGER },
			{ "category", Types.VARCHAR },
			{ "overview", Types.VARCHAR },
			{ "functionname", Types.VARCHAR },
			{ "url", Types.VARCHAR },
			{ "parametername", Types.VARCHAR },
			{ "detectionjudgment", Types.VARCHAR },
			{ "reviewcomment", Types.VARCHAR },
			{ "isresendinvex", Types.INTEGER },
			{ "listofdetectedresult", Types.VARCHAR },
			{ "listofinspectiondatetime", Types.VARCHAR }
		};
	public static final Map<String, Integer> TABLE_COLUMNS_MAP = new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("scanresultid", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("scanid", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("detectionresultid", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("risklevel", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("category", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("overview", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("functionname", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("url", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("parametername", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("detectionjudgment", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("reviewcomment", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("isresendinvex", Types.INTEGER);
		TABLE_COLUMNS_MAP.put("listofdetectedresult", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("listofinspectiondatetime", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE = "create table UBS_VexDetectionResult (scanresultid LONG not null,scanid LONG not null,detectionresultid VARCHAR(75) null,risklevel INTEGER,category VARCHAR(75) null,overview VARCHAR(75) null,functionname VARCHAR(75) null,url VARCHAR(75) null,parametername VARCHAR(75) null,detectionjudgment VARCHAR(75) null,reviewcomment VARCHAR(75) null,isresendinvex INTEGER,listofdetectedresult VARCHAR(75) null,listofinspectiondatetime VARCHAR(75) null,primary key (scanresultid, scanid))";
	public static final String TABLE_SQL_DROP = "drop table UBS_VexDetectionResult";
	public static final String ORDER_BY_JPQL = " ORDER BY vexDetectionResult.id.scanresultid ASC, vexDetectionResult.id.scanid ASC";
	public static final String ORDER_BY_SQL = " ORDER BY UBS_VexDetectionResult.scanresultid ASC, UBS_VexDetectionResult.scanid ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(jp.ubsecure.portal.jubjub.portlet.service.util.ServiceProps.get(
				"value.object.entity.cache.enabled.jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(jp.ubsecure.portal.jubjub.portlet.service.util.ServiceProps.get(
				"value.object.finder.cache.enabled.jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(jp.ubsecure.portal.jubjub.portlet.service.util.ServiceProps.get(
				"value.object.column.bitmask.enabled.jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult"),
			true);
	public static final long SCANID_COLUMN_BITMASK = 1L;
	public static final long SCANRESULTID_COLUMN_BITMASK = 2L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(jp.ubsecure.portal.jubjub.portlet.service.util.ServiceProps.get(
				"lock.expiration.time.jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult"));

	public VexDetectionResultModelImpl() {
	}

	@Override
	public VexDetectionResultPK getPrimaryKey() {
		return new VexDetectionResultPK(_scanresultid, _scanid);
	}

	@Override
	public void setPrimaryKey(VexDetectionResultPK primaryKey) {
		setScanresultid(primaryKey.scanresultid);
		setScanid(primaryKey.scanid);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new VexDetectionResultPK(_scanresultid, _scanid);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((VexDetectionResultPK)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return VexDetectionResult.class;
	}

	@Override
	public String getModelClassName() {
		return VexDetectionResult.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("scanresultid", getScanresultid());
		attributes.put("scanid", getScanid());
		attributes.put("detectionresultid", getDetectionresultid());
		attributes.put("risklevel", getRisklevel());
		attributes.put("category", getCategory());
		attributes.put("overview", getOverview());
		attributes.put("functionname", getFunctionname());
		attributes.put("url", getUrl());
		attributes.put("parametername", getParametername());
		attributes.put("detectionjudgment", getDetectionjudgment());
		attributes.put("reviewcomment", getReviewcomment());
		attributes.put("isresendinvex", getIsresendinvex());
		attributes.put("listofdetectedresult", getListofdetectedresult());
		attributes.put("listofinspectiondatetime", getListofinspectiondatetime());

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long scanresultid = (Long)attributes.get("scanresultid");

		if (scanresultid != null) {
			setScanresultid(scanresultid);
		}

		Long scanid = (Long)attributes.get("scanid");

		if (scanid != null) {
			setScanid(scanid);
		}

		String detectionresultid = (String)attributes.get("detectionresultid");

		if (detectionresultid != null) {
			setDetectionresultid(detectionresultid);
		}

		Integer risklevel = (Integer)attributes.get("risklevel");

		if (risklevel != null) {
			setRisklevel(risklevel);
		}

		String category = (String)attributes.get("category");

		if (category != null) {
			setCategory(category);
		}

		String overview = (String)attributes.get("overview");

		if (overview != null) {
			setOverview(overview);
		}

		String functionname = (String)attributes.get("functionname");

		if (functionname != null) {
			setFunctionname(functionname);
		}

		String url = (String)attributes.get("url");

		if (url != null) {
			setUrl(url);
		}

		String parametername = (String)attributes.get("parametername");

		if (parametername != null) {
			setParametername(parametername);
		}

		String detectionjudgment = (String)attributes.get("detectionjudgment");

		if (detectionjudgment != null) {
			setDetectionjudgment(detectionjudgment);
		}

		String reviewcomment = (String)attributes.get("reviewcomment");

		if (reviewcomment != null) {
			setReviewcomment(reviewcomment);
		}

		Integer isresendinvex = (Integer)attributes.get("isresendinvex");

		if (isresendinvex != null) {
			setIsresendinvex(isresendinvex);
		}

		String listofdetectedresult = (String)attributes.get(
				"listofdetectedresult");

		if (listofdetectedresult != null) {
			setListofdetectedresult(listofdetectedresult);
		}

		String listofinspectiondatetime = (String)attributes.get(
				"listofinspectiondatetime");

		if (listofinspectiondatetime != null) {
			setListofinspectiondatetime(listofinspectiondatetime);
		}
	}

	@Override
	public long getScanresultid() {
		return _scanresultid;
	}

	@Override
	public void setScanresultid(long scanresultid) {
		_scanresultid = scanresultid;
	}

	@Override
	public long getScanid() {
		return _scanid;
	}

	@Override
	public void setScanid(long scanid) {
		_columnBitmask |= SCANID_COLUMN_BITMASK;

		if (!_setOriginalScanid) {
			_setOriginalScanid = true;

			_originalScanid = _scanid;
		}

		_scanid = scanid;
	}

	public long getOriginalScanid() {
		return _originalScanid;
	}

	@Override
	public String getDetectionresultid() {
		if (_detectionresultid == null) {
			return StringPool.BLANK;
		}
		else {
			return _detectionresultid;
		}
	}

	@Override
	public void setDetectionresultid(String detectionresultid) {
		_detectionresultid = detectionresultid;
	}

	@Override
	public int getRisklevel() {
		return _risklevel;
	}

	@Override
	public void setRisklevel(int risklevel) {
		_risklevel = risklevel;
	}

	@Override
	public String getCategory() {
		if (_category == null) {
			return StringPool.BLANK;
		}
		else {
			return _category;
		}
	}

	@Override
	public void setCategory(String category) {
		_category = category;
	}

	@Override
	public String getOverview() {
		if (_overview == null) {
			return StringPool.BLANK;
		}
		else {
			return _overview;
		}
	}

	@Override
	public void setOverview(String overview) {
		_overview = overview;
	}

	@Override
	public String getFunctionname() {
		if (_functionname == null) {
			return StringPool.BLANK;
		}
		else {
			return _functionname;
		}
	}

	@Override
	public void setFunctionname(String functionname) {
		_functionname = functionname;
	}

	@Override
	public String getUrl() {
		if (_url == null) {
			return StringPool.BLANK;
		}
		else {
			return _url;
		}
	}

	@Override
	public void setUrl(String url) {
		_url = url;
	}

	@Override
	public String getParametername() {
		if (_parametername == null) {
			return StringPool.BLANK;
		}
		else {
			return _parametername;
		}
	}

	@Override
	public void setParametername(String parametername) {
		_parametername = parametername;
	}

	@Override
	public String getDetectionjudgment() {
		if (_detectionjudgment == null) {
			return StringPool.BLANK;
		}
		else {
			return _detectionjudgment;
		}
	}

	@Override
	public void setDetectionjudgment(String detectionjudgment) {
		_detectionjudgment = detectionjudgment;
	}

	@Override
	public String getReviewcomment() {
		if (_reviewcomment == null) {
			return StringPool.BLANK;
		}
		else {
			return _reviewcomment;
		}
	}

	@Override
	public void setReviewcomment(String reviewcomment) {
		_reviewcomment = reviewcomment;
	}

	@Override
	public int getIsresendinvex() {
		return _isresendinvex;
	}

	@Override
	public void setIsresendinvex(int isresendinvex) {
		_isresendinvex = isresendinvex;
	}

	@Override
	public String getListofdetectedresult() {
		if (_listofdetectedresult == null) {
			return StringPool.BLANK;
		}
		else {
			return _listofdetectedresult;
		}
	}

	@Override
	public void setListofdetectedresult(String listofdetectedresult) {
		_listofdetectedresult = listofdetectedresult;
	}

	@Override
	public String getListofinspectiondatetime() {
		if (_listofinspectiondatetime == null) {
			return StringPool.BLANK;
		}
		else {
			return _listofinspectiondatetime;
		}
	}

	@Override
	public void setListofinspectiondatetime(String listofinspectiondatetime) {
		_listofinspectiondatetime = listofinspectiondatetime;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public VexDetectionResult toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (VexDetectionResult)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		VexDetectionResultImpl vexDetectionResultImpl = new VexDetectionResultImpl();

		vexDetectionResultImpl.setScanresultid(getScanresultid());
		vexDetectionResultImpl.setScanid(getScanid());
		vexDetectionResultImpl.setDetectionresultid(getDetectionresultid());
		vexDetectionResultImpl.setRisklevel(getRisklevel());
		vexDetectionResultImpl.setCategory(getCategory());
		vexDetectionResultImpl.setOverview(getOverview());
		vexDetectionResultImpl.setFunctionname(getFunctionname());
		vexDetectionResultImpl.setUrl(getUrl());
		vexDetectionResultImpl.setParametername(getParametername());
		vexDetectionResultImpl.setDetectionjudgment(getDetectionjudgment());
		vexDetectionResultImpl.setReviewcomment(getReviewcomment());
		vexDetectionResultImpl.setIsresendinvex(getIsresendinvex());
		vexDetectionResultImpl.setListofdetectedresult(getListofdetectedresult());
		vexDetectionResultImpl.setListofinspectiondatetime(getListofinspectiondatetime());

		vexDetectionResultImpl.resetOriginalValues();

		return vexDetectionResultImpl;
	}

	@Override
	public int compareTo(VexDetectionResult vexDetectionResult) {
		VexDetectionResultPK primaryKey = vexDetectionResult.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexDetectionResult)) {
			return false;
		}

		VexDetectionResult vexDetectionResult = (VexDetectionResult)obj;

		VexDetectionResultPK primaryKey = vexDetectionResult.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return ENTITY_CACHE_ENABLED;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return FINDER_CACHE_ENABLED;
	}

	@Override
	public void resetOriginalValues() {
		VexDetectionResultModelImpl vexDetectionResultModelImpl = this;

		vexDetectionResultModelImpl._originalScanid = vexDetectionResultModelImpl._scanid;

		vexDetectionResultModelImpl._setOriginalScanid = false;

		vexDetectionResultModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<VexDetectionResult> toCacheModel() {
		VexDetectionResultCacheModel vexDetectionResultCacheModel = new VexDetectionResultCacheModel();

		vexDetectionResultCacheModel.vexDetectionResultPK = getPrimaryKey();

		vexDetectionResultCacheModel.scanresultid = getScanresultid();

		vexDetectionResultCacheModel.scanid = getScanid();

		vexDetectionResultCacheModel.detectionresultid = getDetectionresultid();

		String detectionresultid = vexDetectionResultCacheModel.detectionresultid;

		if ((detectionresultid != null) && (detectionresultid.length() == 0)) {
			vexDetectionResultCacheModel.detectionresultid = null;
		}

		vexDetectionResultCacheModel.risklevel = getRisklevel();

		vexDetectionResultCacheModel.category = getCategory();

		String category = vexDetectionResultCacheModel.category;

		if ((category != null) && (category.length() == 0)) {
			vexDetectionResultCacheModel.category = null;
		}

		vexDetectionResultCacheModel.overview = getOverview();

		String overview = vexDetectionResultCacheModel.overview;

		if ((overview != null) && (overview.length() == 0)) {
			vexDetectionResultCacheModel.overview = null;
		}

		vexDetectionResultCacheModel.functionname = getFunctionname();

		String functionname = vexDetectionResultCacheModel.functionname;

		if ((functionname != null) && (functionname.length() == 0)) {
			vexDetectionResultCacheModel.functionname = null;
		}

		vexDetectionResultCacheModel.url = getUrl();

		String url = vexDetectionResultCacheModel.url;

		if ((url != null) && (url.length() == 0)) {
			vexDetectionResultCacheModel.url = null;
		}

		vexDetectionResultCacheModel.parametername = getParametername();

		String parametername = vexDetectionResultCacheModel.parametername;

		if ((parametername != null) && (parametername.length() == 0)) {
			vexDetectionResultCacheModel.parametername = null;
		}

		vexDetectionResultCacheModel.detectionjudgment = getDetectionjudgment();

		String detectionjudgment = vexDetectionResultCacheModel.detectionjudgment;

		if ((detectionjudgment != null) && (detectionjudgment.length() == 0)) {
			vexDetectionResultCacheModel.detectionjudgment = null;
		}

		vexDetectionResultCacheModel.reviewcomment = getReviewcomment();

		String reviewcomment = vexDetectionResultCacheModel.reviewcomment;

		if ((reviewcomment != null) && (reviewcomment.length() == 0)) {
			vexDetectionResultCacheModel.reviewcomment = null;
		}

		vexDetectionResultCacheModel.isresendinvex = getIsresendinvex();

		vexDetectionResultCacheModel.listofdetectedresult = getListofdetectedresult();

		String listofdetectedresult = vexDetectionResultCacheModel.listofdetectedresult;

		if ((listofdetectedresult != null) &&
				(listofdetectedresult.length() == 0)) {
			vexDetectionResultCacheModel.listofdetectedresult = null;
		}

		vexDetectionResultCacheModel.listofinspectiondatetime = getListofinspectiondatetime();

		String listofinspectiondatetime = vexDetectionResultCacheModel.listofinspectiondatetime;

		if ((listofinspectiondatetime != null) &&
				(listofinspectiondatetime.length() == 0)) {
			vexDetectionResultCacheModel.listofinspectiondatetime = null;
		}

		return vexDetectionResultCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{scanresultid=");
		sb.append(getScanresultid());
		sb.append(", scanid=");
		sb.append(getScanid());
		sb.append(", detectionresultid=");
		sb.append(getDetectionresultid());
		sb.append(", risklevel=");
		sb.append(getRisklevel());
		sb.append(", category=");
		sb.append(getCategory());
		sb.append(", overview=");
		sb.append(getOverview());
		sb.append(", functionname=");
		sb.append(getFunctionname());
		sb.append(", url=");
		sb.append(getUrl());
		sb.append(", parametername=");
		sb.append(getParametername());
		sb.append(", detectionjudgment=");
		sb.append(getDetectionjudgment());
		sb.append(", reviewcomment=");
		sb.append(getReviewcomment());
		sb.append(", isresendinvex=");
		sb.append(getIsresendinvex());
		sb.append(", listofdetectedresult=");
		sb.append(getListofdetectedresult());
		sb.append(", listofinspectiondatetime=");
		sb.append(getListofinspectiondatetime());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(46);

		sb.append("<model><model-name>");
		sb.append("jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>scanresultid</column-name><column-value><![CDATA[");
		sb.append(getScanresultid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>scanid</column-name><column-value><![CDATA[");
		sb.append(getScanid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>detectionresultid</column-name><column-value><![CDATA[");
		sb.append(getDetectionresultid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>risklevel</column-name><column-value><![CDATA[");
		sb.append(getRisklevel());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>category</column-name><column-value><![CDATA[");
		sb.append(getCategory());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>overview</column-name><column-value><![CDATA[");
		sb.append(getOverview());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>functionname</column-name><column-value><![CDATA[");
		sb.append(getFunctionname());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>url</column-name><column-value><![CDATA[");
		sb.append(getUrl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>parametername</column-name><column-value><![CDATA[");
		sb.append(getParametername());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>detectionjudgment</column-name><column-value><![CDATA[");
		sb.append(getDetectionjudgment());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>reviewcomment</column-name><column-value><![CDATA[");
		sb.append(getReviewcomment());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isresendinvex</column-name><column-value><![CDATA[");
		sb.append(getIsresendinvex());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>listofdetectedresult</column-name><column-value><![CDATA[");
		sb.append(getListofdetectedresult());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>listofinspectiondatetime</column-name><column-value><![CDATA[");
		sb.append(getListofinspectiondatetime());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static final ClassLoader _classLoader = VexDetectionResult.class.getClassLoader();
	private static final Class<?>[] _escapedModelInterfaces = new Class[] {
			VexDetectionResult.class
		};
	private long _scanresultid;
	private long _scanid;
	private long _originalScanid;
	private boolean _setOriginalScanid;
	private String _detectionresultid;
	private int _risklevel;
	private String _category;
	private String _overview;
	private String _functionname;
	private String _url;
	private String _parametername;
	private String _detectionjudgment;
	private String _reviewcomment;
	private int _isresendinvex;
	private String _listofdetectedresult;
	private String _listofinspectiondatetime;
	private long _columnBitmask;
	private VexDetectionResult _escapedModel;
}