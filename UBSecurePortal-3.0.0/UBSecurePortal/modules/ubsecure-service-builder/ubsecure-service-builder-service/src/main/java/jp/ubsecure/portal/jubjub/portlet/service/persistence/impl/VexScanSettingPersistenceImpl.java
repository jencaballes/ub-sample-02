/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexScanSettingException;
import jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexScanSettingImpl;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexScanSettingModelImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexScanSettingPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the vex scan setting service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexScanSettingPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.VexScanSettingUtil
 * @generated
 */
@ProviderType
public class VexScanSettingPersistenceImpl extends BasePersistenceImpl<VexScanSetting>
	implements VexScanSettingPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link VexScanSettingUtil} to access the vex scan setting persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = VexScanSettingImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingModelImpl.FINDER_CACHE_ENABLED,
			VexScanSettingImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingModelImpl.FINDER_CACHE_ENABLED,
			VexScanSettingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID = new FinderPath(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingModelImpl.FINDER_CACHE_ENABLED,
			VexScanSettingImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByScanId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID =
		new FinderPath(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingModelImpl.FINDER_CACHE_ENABLED,
			VexScanSettingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByScanId",
			new String[] { Long.class.getName() },
			VexScanSettingModelImpl.SCANID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SCANID = new FinderPath(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScanId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the vex scan settings where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @return the matching vex scan settings
	 */
	@Override
	public List<VexScanSetting> findByScanId(long scanid) {
		return findByScanId(scanid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the vex scan settings where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex scan settings
	 * @param end the upper bound of the range of vex scan settings (not inclusive)
	 * @return the range of matching vex scan settings
	 */
	@Override
	public List<VexScanSetting> findByScanId(long scanid, int start, int end) {
		return findByScanId(scanid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the vex scan settings where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex scan settings
	 * @param end the upper bound of the range of vex scan settings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching vex scan settings
	 */
	@Override
	public List<VexScanSetting> findByScanId(long scanid, int start, int end,
		OrderByComparator<VexScanSetting> orderByComparator) {
		return findByScanId(scanid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the vex scan settings where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex scan settings
	 * @param end the upper bound of the range of vex scan settings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching vex scan settings
	 */
	@Override
	public List<VexScanSetting> findByScanId(long scanid, int start, int end,
		OrderByComparator<VexScanSetting> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanid, start, end, orderByComparator };
		}

		List<VexScanSetting> list = null;

		if (retrieveFromCache) {
			list = (List<VexScanSetting>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (VexScanSetting vexScanSetting : list) {
					if ((scanid != vexScanSetting.getScanid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_VEXSCANSETTING_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(VexScanSettingModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanid);

				if (!pagination) {
					list = (List<VexScanSetting>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<VexScanSetting>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first vex scan setting in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching vex scan setting
	 * @throws NoSuchVexScanSettingException if a matching vex scan setting could not be found
	 */
	@Override
	public VexScanSetting findByScanId_First(long scanid,
		OrderByComparator<VexScanSetting> orderByComparator)
		throws NoSuchVexScanSettingException {
		VexScanSetting vexScanSetting = fetchByScanId_First(scanid,
				orderByComparator);

		if (vexScanSetting != null) {
			return vexScanSetting;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanid=");
		msg.append(scanid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVexScanSettingException(msg.toString());
	}

	/**
	 * Returns the first vex scan setting in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching vex scan setting, or <code>null</code> if a matching vex scan setting could not be found
	 */
	@Override
	public VexScanSetting fetchByScanId_First(long scanid,
		OrderByComparator<VexScanSetting> orderByComparator) {
		List<VexScanSetting> list = findByScanId(scanid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last vex scan setting in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching vex scan setting
	 * @throws NoSuchVexScanSettingException if a matching vex scan setting could not be found
	 */
	@Override
	public VexScanSetting findByScanId_Last(long scanid,
		OrderByComparator<VexScanSetting> orderByComparator)
		throws NoSuchVexScanSettingException {
		VexScanSetting vexScanSetting = fetchByScanId_Last(scanid,
				orderByComparator);

		if (vexScanSetting != null) {
			return vexScanSetting;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanid=");
		msg.append(scanid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVexScanSettingException(msg.toString());
	}

	/**
	 * Returns the last vex scan setting in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching vex scan setting, or <code>null</code> if a matching vex scan setting could not be found
	 */
	@Override
	public VexScanSetting fetchByScanId_Last(long scanid,
		OrderByComparator<VexScanSetting> orderByComparator) {
		int count = countByScanId(scanid);

		if (count == 0) {
			return null;
		}

		List<VexScanSetting> list = findByScanId(scanid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the vex scan settings where scanid = &#63; from the database.
	 *
	 * @param scanid the scanid
	 */
	@Override
	public void removeByScanId(long scanid) {
		for (VexScanSetting vexScanSetting : findByScanId(scanid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(vexScanSetting);
		}
	}

	/**
	 * Returns the number of vex scan settings where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @return the number of matching vex scan settings
	 */
	@Override
	public int countByScanId(long scanid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SCANID;

		Object[] finderArgs = new Object[] { scanid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_VEXSCANSETTING_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanid);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCANID_SCANID_2 = "vexScanSetting.scanid = ?";

	public VexScanSettingPersistenceImpl() {
		setModelClass(VexScanSetting.class);
	}

	/**
	 * Caches the vex scan setting in the entity cache if it is enabled.
	 *
	 * @param vexScanSetting the vex scan setting
	 */
	@Override
	public void cacheResult(VexScanSetting vexScanSetting) {
		entityCache.putResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingImpl.class, vexScanSetting.getPrimaryKey(),
			vexScanSetting);

		vexScanSetting.resetOriginalValues();
	}

	/**
	 * Caches the vex scan settings in the entity cache if it is enabled.
	 *
	 * @param vexScanSettings the vex scan settings
	 */
	@Override
	public void cacheResult(List<VexScanSetting> vexScanSettings) {
		for (VexScanSetting vexScanSetting : vexScanSettings) {
			if (entityCache.getResult(
						VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
						VexScanSettingImpl.class, vexScanSetting.getPrimaryKey()) == null) {
				cacheResult(vexScanSetting);
			}
			else {
				vexScanSetting.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all vex scan settings.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(VexScanSettingImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the vex scan setting.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(VexScanSetting vexScanSetting) {
		entityCache.removeResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingImpl.class, vexScanSetting.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<VexScanSetting> vexScanSettings) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (VexScanSetting vexScanSetting : vexScanSettings) {
			entityCache.removeResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
				VexScanSettingImpl.class, vexScanSetting.getPrimaryKey());
		}
	}

	/**
	 * Creates a new vex scan setting with the primary key. Does not add the vex scan setting to the database.
	 *
	 * @param scanid the primary key for the new vex scan setting
	 * @return the new vex scan setting
	 */
	@Override
	public VexScanSetting create(long scanid) {
		VexScanSetting vexScanSetting = new VexScanSettingImpl();

		vexScanSetting.setNew(true);
		vexScanSetting.setPrimaryKey(scanid);

		return vexScanSetting;
	}

	/**
	 * Removes the vex scan setting with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param scanid the primary key of the vex scan setting
	 * @return the vex scan setting that was removed
	 * @throws NoSuchVexScanSettingException if a vex scan setting with the primary key could not be found
	 */
	@Override
	public VexScanSetting remove(long scanid)
		throws NoSuchVexScanSettingException {
		return remove((Serializable)scanid);
	}

	/**
	 * Removes the vex scan setting with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the vex scan setting
	 * @return the vex scan setting that was removed
	 * @throws NoSuchVexScanSettingException if a vex scan setting with the primary key could not be found
	 */
	@Override
	public VexScanSetting remove(Serializable primaryKey)
		throws NoSuchVexScanSettingException {
		Session session = null;

		try {
			session = openSession();

			VexScanSetting vexScanSetting = (VexScanSetting)session.get(VexScanSettingImpl.class,
					primaryKey);

			if (vexScanSetting == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchVexScanSettingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(vexScanSetting);
		}
		catch (NoSuchVexScanSettingException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected VexScanSetting removeImpl(VexScanSetting vexScanSetting) {
		vexScanSetting = toUnwrappedModel(vexScanSetting);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(vexScanSetting)) {
				vexScanSetting = (VexScanSetting)session.get(VexScanSettingImpl.class,
						vexScanSetting.getPrimaryKeyObj());
			}

			if (vexScanSetting != null) {
				session.delete(vexScanSetting);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (vexScanSetting != null) {
			clearCache(vexScanSetting);
		}

		return vexScanSetting;
	}

	@Override
	public VexScanSetting updateImpl(VexScanSetting vexScanSetting) {
		vexScanSetting = toUnwrappedModel(vexScanSetting);

		boolean isNew = vexScanSetting.isNew();

		VexScanSettingModelImpl vexScanSettingModelImpl = (VexScanSettingModelImpl)vexScanSetting;

		Session session = null;

		try {
			session = openSession();

			if (vexScanSetting.isNew()) {
				session.save(vexScanSetting);

				vexScanSetting.setNew(false);
			}
			else {
				vexScanSetting = (VexScanSetting)session.merge(vexScanSetting);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !VexScanSettingModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((vexScanSettingModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						vexScanSettingModelImpl.getOriginalScanid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);

				args = new Object[] { vexScanSettingModelImpl.getScanid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);
			}
		}

		entityCache.putResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
			VexScanSettingImpl.class, vexScanSetting.getPrimaryKey(),
			vexScanSetting, false);

		vexScanSetting.resetOriginalValues();

		return vexScanSetting;
	}

	protected VexScanSetting toUnwrappedModel(VexScanSetting vexScanSetting) {
		if (vexScanSetting instanceof VexScanSettingImpl) {
			return vexScanSetting;
		}

		VexScanSettingImpl vexScanSettingImpl = new VexScanSettingImpl();

		vexScanSettingImpl.setNew(vexScanSetting.isNew());
		vexScanSettingImpl.setPrimaryKey(vexScanSetting.getPrimaryKey());

		vexScanSettingImpl.setScanid(vexScanSetting.getScanid());
		vexScanSettingImpl.setStarturl(vexScanSetting.getStarturl());
		vexScanSettingImpl.setAuthorizedpatrolurl(vexScanSetting.getAuthorizedpatrolurl());
		vexScanSettingImpl.setUnauthorizedpatrolurl(vexScanSetting.getUnauthorizedpatrolurl());
		vexScanSettingImpl.setStarttime(vexScanSetting.getStarttime());
		vexScanSettingImpl.setEndtime(vexScanSetting.getEndtime());
		vexScanSettingImpl.setSetemailnotification(vexScanSetting.getSetemailnotification());
		vexScanSettingImpl.setSelectedwebsignature(vexScanSetting.getSelectedwebsignature());
		vexScanSettingImpl.setGetserverfiles(vexScanSetting.getGetserverfiles());
		vexScanSettingImpl.setGetserversettings(vexScanSetting.getGetserversettings());
		vexScanSettingImpl.setSelectedserverfilessignature(vexScanSetting.getSelectedserverfilessignature());
		vexScanSettingImpl.setSelectedserversettingssignature(vexScanSetting.getSelectedserversettingssignature());
		vexScanSettingImpl.setMaxnumdetectionlink(vexScanSetting.getMaxnumdetectionlink());
		vexScanSettingImpl.setMaxnumdetectionlinkperpage(vexScanSetting.getMaxnumdetectionlinkperpage());
		vexScanSettingImpl.setDetectionlinkdepthlimit(vexScanSetting.getDetectionlinkdepthlimit());
		vexScanSettingImpl.setWaittime(vexScanSetting.getWaittime());
		vexScanSettingImpl.setNumofthread(vexScanSetting.getNumofthread());
		vexScanSettingImpl.setTimeouttime(vexScanSetting.getTimeouttime());
		vexScanSettingImpl.setSetautopostform(vexScanSetting.getSetautopostform());
		vexScanSettingImpl.setSetpostmethod(vexScanSetting.getSetpostmethod());
		vexScanSettingImpl.setRequestheader(vexScanSetting.getRequestheader());
		vexScanSettingImpl.setLoginstatusdetection(vexScanSetting.getLoginstatusdetection());
		vexScanSettingImpl.setSetnotsendpassword(vexScanSetting.getSetnotsendpassword());
		vexScanSettingImpl.setParamnamesaspassword(vexScanSetting.getParamnamesaspassword());
		vexScanSettingImpl.setNumofretryaterror(vexScanSetting.getNumofretryaterror());
		vexScanSettingImpl.setSessionerrordetection(vexScanSetting.getSessionerrordetection());
		vexScanSettingImpl.setScreentransitionerrordetection(vexScanSetting.getScreentransitionerrordetection());
		vexScanSettingImpl.setImplementationenvironment(vexScanSetting.getImplementationenvironment());
		vexScanSettingImpl.setSetcrawlingonly(vexScanSetting.getSetcrawlingonly());
		vexScanSettingImpl.setExcludepathinforegex(vexScanSetting.getExcludepathinforegex());
		vexScanSettingImpl.setExcludeparameterdeletenameregex(vexScanSetting.getExcludeparameterdeletenameregex());
		vexScanSettingImpl.setExcludeparameterdeleteparamregex(vexScanSetting.getExcludeparameterdeleteparamregex());
		vexScanSettingImpl.setExtracturlregex(vexScanSetting.getExtracturlregex());
		vexScanSettingImpl.setNotparseextension(vexScanSetting.getNotparseextension());
		vexScanSettingImpl.setNotparsefilenameregex(vexScanSetting.getNotparsefilenameregex());

		return vexScanSettingImpl;
	}

	/**
	 * Returns the vex scan setting with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the vex scan setting
	 * @return the vex scan setting
	 * @throws NoSuchVexScanSettingException if a vex scan setting with the primary key could not be found
	 */
	@Override
	public VexScanSetting findByPrimaryKey(Serializable primaryKey)
		throws NoSuchVexScanSettingException {
		VexScanSetting vexScanSetting = fetchByPrimaryKey(primaryKey);

		if (vexScanSetting == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchVexScanSettingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return vexScanSetting;
	}

	/**
	 * Returns the vex scan setting with the primary key or throws a {@link NoSuchVexScanSettingException} if it could not be found.
	 *
	 * @param scanid the primary key of the vex scan setting
	 * @return the vex scan setting
	 * @throws NoSuchVexScanSettingException if a vex scan setting with the primary key could not be found
	 */
	@Override
	public VexScanSetting findByPrimaryKey(long scanid)
		throws NoSuchVexScanSettingException {
		return findByPrimaryKey((Serializable)scanid);
	}

	/**
	 * Returns the vex scan setting with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the vex scan setting
	 * @return the vex scan setting, or <code>null</code> if a vex scan setting with the primary key could not be found
	 */
	@Override
	public VexScanSetting fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
				VexScanSettingImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		VexScanSetting vexScanSetting = (VexScanSetting)serializable;

		if (vexScanSetting == null) {
			Session session = null;

			try {
				session = openSession();

				vexScanSetting = (VexScanSetting)session.get(VexScanSettingImpl.class,
						primaryKey);

				if (vexScanSetting != null) {
					cacheResult(vexScanSetting);
				}
				else {
					entityCache.putResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
						VexScanSettingImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
					VexScanSettingImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return vexScanSetting;
	}

	/**
	 * Returns the vex scan setting with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param scanid the primary key of the vex scan setting
	 * @return the vex scan setting, or <code>null</code> if a vex scan setting with the primary key could not be found
	 */
	@Override
	public VexScanSetting fetchByPrimaryKey(long scanid) {
		return fetchByPrimaryKey((Serializable)scanid);
	}

	@Override
	public Map<Serializable, VexScanSetting> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, VexScanSetting> map = new HashMap<Serializable, VexScanSetting>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			VexScanSetting vexScanSetting = fetchByPrimaryKey(primaryKey);

			if (vexScanSetting != null) {
				map.put(primaryKey, vexScanSetting);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
					VexScanSettingImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (VexScanSetting)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_VEXSCANSETTING_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (VexScanSetting vexScanSetting : (List<VexScanSetting>)q.list()) {
				map.put(vexScanSetting.getPrimaryKeyObj(), vexScanSetting);

				cacheResult(vexScanSetting);

				uncachedPrimaryKeys.remove(vexScanSetting.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(VexScanSettingModelImpl.ENTITY_CACHE_ENABLED,
					VexScanSettingImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the vex scan settings.
	 *
	 * @return the vex scan settings
	 */
	@Override
	public List<VexScanSetting> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the vex scan settings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex scan settings
	 * @param end the upper bound of the range of vex scan settings (not inclusive)
	 * @return the range of vex scan settings
	 */
	@Override
	public List<VexScanSetting> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the vex scan settings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex scan settings
	 * @param end the upper bound of the range of vex scan settings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of vex scan settings
	 */
	@Override
	public List<VexScanSetting> findAll(int start, int end,
		OrderByComparator<VexScanSetting> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the vex scan settings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexScanSettingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex scan settings
	 * @param end the upper bound of the range of vex scan settings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of vex scan settings
	 */
	@Override
	public List<VexScanSetting> findAll(int start, int end,
		OrderByComparator<VexScanSetting> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<VexScanSetting> list = null;

		if (retrieveFromCache) {
			list = (List<VexScanSetting>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_VEXSCANSETTING);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_VEXSCANSETTING;

				if (pagination) {
					sql = sql.concat(VexScanSettingModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<VexScanSetting>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<VexScanSetting>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the vex scan settings from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (VexScanSetting vexScanSetting : findAll()) {
			remove(vexScanSetting);
		}
	}

	/**
	 * Returns the number of vex scan settings.
	 *
	 * @return the number of vex scan settings
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_VEXSCANSETTING);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return VexScanSettingModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the vex scan setting persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(VexScanSettingImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_VEXSCANSETTING = "SELECT vexScanSetting FROM VexScanSetting vexScanSetting";
	private static final String _SQL_SELECT_VEXSCANSETTING_WHERE_PKS_IN = "SELECT vexScanSetting FROM VexScanSetting vexScanSetting WHERE scanid IN (";
	private static final String _SQL_SELECT_VEXSCANSETTING_WHERE = "SELECT vexScanSetting FROM VexScanSetting vexScanSetting WHERE ";
	private static final String _SQL_COUNT_VEXSCANSETTING = "SELECT COUNT(vexScanSetting) FROM VexScanSetting vexScanSetting";
	private static final String _SQL_COUNT_VEXSCANSETTING_WHERE = "SELECT COUNT(vexScanSetting) FROM VexScanSetting vexScanSetting WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "vexScanSetting.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VexScanSetting exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VexScanSetting exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(VexScanSettingPersistenceImpl.class);
}