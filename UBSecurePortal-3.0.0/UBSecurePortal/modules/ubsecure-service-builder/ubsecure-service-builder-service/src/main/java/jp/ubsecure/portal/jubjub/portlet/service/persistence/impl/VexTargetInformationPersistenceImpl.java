/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexTargetInformationException;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexTargetInformationImpl;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexTargetInformationModelImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexTargetInformationPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the vex target information service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexTargetInformationPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.VexTargetInformationUtil
 * @generated
 */
@ProviderType
public class VexTargetInformationPersistenceImpl extends BasePersistenceImpl<VexTargetInformation>
	implements VexTargetInformationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link VexTargetInformationUtil} to access the vex target information persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = VexTargetInformationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationModelImpl.FINDER_CACHE_ENABLED,
			VexTargetInformationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationModelImpl.FINDER_CACHE_ENABLED,
			VexTargetInformationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID = new FinderPath(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationModelImpl.FINDER_CACHE_ENABLED,
			VexTargetInformationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByScanId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID =
		new FinderPath(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationModelImpl.FINDER_CACHE_ENABLED,
			VexTargetInformationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByScanId",
			new String[] { Long.class.getName() },
			VexTargetInformationModelImpl.SCANID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SCANID = new FinderPath(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScanId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the vex target informations where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @return the matching vex target informations
	 */
	@Override
	public List<VexTargetInformation> findByScanId(long scanid) {
		return findByScanId(scanid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the vex target informations where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex target informations
	 * @param end the upper bound of the range of vex target informations (not inclusive)
	 * @return the range of matching vex target informations
	 */
	@Override
	public List<VexTargetInformation> findByScanId(long scanid, int start,
		int end) {
		return findByScanId(scanid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the vex target informations where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex target informations
	 * @param end the upper bound of the range of vex target informations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching vex target informations
	 */
	@Override
	public List<VexTargetInformation> findByScanId(long scanid, int start,
		int end, OrderByComparator<VexTargetInformation> orderByComparator) {
		return findByScanId(scanid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the vex target informations where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex target informations
	 * @param end the upper bound of the range of vex target informations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching vex target informations
	 */
	@Override
	public List<VexTargetInformation> findByScanId(long scanid, int start,
		int end, OrderByComparator<VexTargetInformation> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanid, start, end, orderByComparator };
		}

		List<VexTargetInformation> list = null;

		if (retrieveFromCache) {
			list = (List<VexTargetInformation>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (VexTargetInformation vexTargetInformation : list) {
					if ((scanid != vexTargetInformation.getScanid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_VEXTARGETINFORMATION_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(VexTargetInformationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanid);

				if (!pagination) {
					list = (List<VexTargetInformation>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<VexTargetInformation>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first vex target information in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching vex target information
	 * @throws NoSuchVexTargetInformationException if a matching vex target information could not be found
	 */
	@Override
	public VexTargetInformation findByScanId_First(long scanid,
		OrderByComparator<VexTargetInformation> orderByComparator)
		throws NoSuchVexTargetInformationException {
		VexTargetInformation vexTargetInformation = fetchByScanId_First(scanid,
				orderByComparator);

		if (vexTargetInformation != null) {
			return vexTargetInformation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanid=");
		msg.append(scanid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVexTargetInformationException(msg.toString());
	}

	/**
	 * Returns the first vex target information in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching vex target information, or <code>null</code> if a matching vex target information could not be found
	 */
	@Override
	public VexTargetInformation fetchByScanId_First(long scanid,
		OrderByComparator<VexTargetInformation> orderByComparator) {
		List<VexTargetInformation> list = findByScanId(scanid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last vex target information in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching vex target information
	 * @throws NoSuchVexTargetInformationException if a matching vex target information could not be found
	 */
	@Override
	public VexTargetInformation findByScanId_Last(long scanid,
		OrderByComparator<VexTargetInformation> orderByComparator)
		throws NoSuchVexTargetInformationException {
		VexTargetInformation vexTargetInformation = fetchByScanId_Last(scanid,
				orderByComparator);

		if (vexTargetInformation != null) {
			return vexTargetInformation;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanid=");
		msg.append(scanid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVexTargetInformationException(msg.toString());
	}

	/**
	 * Returns the last vex target information in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching vex target information, or <code>null</code> if a matching vex target information could not be found
	 */
	@Override
	public VexTargetInformation fetchByScanId_Last(long scanid,
		OrderByComparator<VexTargetInformation> orderByComparator) {
		int count = countByScanId(scanid);

		if (count == 0) {
			return null;
		}

		List<VexTargetInformation> list = findByScanId(scanid, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the vex target informations where scanid = &#63; from the database.
	 *
	 * @param scanid the scanid
	 */
	@Override
	public void removeByScanId(long scanid) {
		for (VexTargetInformation vexTargetInformation : findByScanId(scanid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(vexTargetInformation);
		}
	}

	/**
	 * Returns the number of vex target informations where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @return the number of matching vex target informations
	 */
	@Override
	public int countByScanId(long scanid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SCANID;

		Object[] finderArgs = new Object[] { scanid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_VEXTARGETINFORMATION_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanid);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCANID_SCANID_2 = "vexTargetInformation.scanid = ?";

	public VexTargetInformationPersistenceImpl() {
		setModelClass(VexTargetInformation.class);
	}

	/**
	 * Caches the vex target information in the entity cache if it is enabled.
	 *
	 * @param vexTargetInformation the vex target information
	 */
	@Override
	public void cacheResult(VexTargetInformation vexTargetInformation) {
		entityCache.putResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationImpl.class,
			vexTargetInformation.getPrimaryKey(), vexTargetInformation);

		vexTargetInformation.resetOriginalValues();
	}

	/**
	 * Caches the vex target informations in the entity cache if it is enabled.
	 *
	 * @param vexTargetInformations the vex target informations
	 */
	@Override
	public void cacheResult(List<VexTargetInformation> vexTargetInformations) {
		for (VexTargetInformation vexTargetInformation : vexTargetInformations) {
			if (entityCache.getResult(
						VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
						VexTargetInformationImpl.class,
						vexTargetInformation.getPrimaryKey()) == null) {
				cacheResult(vexTargetInformation);
			}
			else {
				vexTargetInformation.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all vex target informations.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(VexTargetInformationImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the vex target information.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(VexTargetInformation vexTargetInformation) {
		entityCache.removeResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationImpl.class, vexTargetInformation.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<VexTargetInformation> vexTargetInformations) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (VexTargetInformation vexTargetInformation : vexTargetInformations) {
			entityCache.removeResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
				VexTargetInformationImpl.class,
				vexTargetInformation.getPrimaryKey());
		}
	}

	/**
	 * Creates a new vex target information with the primary key. Does not add the vex target information to the database.
	 *
	 * @param scanid the primary key for the new vex target information
	 * @return the new vex target information
	 */
	@Override
	public VexTargetInformation create(long scanid) {
		VexTargetInformation vexTargetInformation = new VexTargetInformationImpl();

		vexTargetInformation.setNew(true);
		vexTargetInformation.setPrimaryKey(scanid);

		return vexTargetInformation;
	}

	/**
	 * Removes the vex target information with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param scanid the primary key of the vex target information
	 * @return the vex target information that was removed
	 * @throws NoSuchVexTargetInformationException if a vex target information with the primary key could not be found
	 */
	@Override
	public VexTargetInformation remove(long scanid)
		throws NoSuchVexTargetInformationException {
		return remove((Serializable)scanid);
	}

	/**
	 * Removes the vex target information with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the vex target information
	 * @return the vex target information that was removed
	 * @throws NoSuchVexTargetInformationException if a vex target information with the primary key could not be found
	 */
	@Override
	public VexTargetInformation remove(Serializable primaryKey)
		throws NoSuchVexTargetInformationException {
		Session session = null;

		try {
			session = openSession();

			VexTargetInformation vexTargetInformation = (VexTargetInformation)session.get(VexTargetInformationImpl.class,
					primaryKey);

			if (vexTargetInformation == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchVexTargetInformationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(vexTargetInformation);
		}
		catch (NoSuchVexTargetInformationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected VexTargetInformation removeImpl(
		VexTargetInformation vexTargetInformation) {
		vexTargetInformation = toUnwrappedModel(vexTargetInformation);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(vexTargetInformation)) {
				vexTargetInformation = (VexTargetInformation)session.get(VexTargetInformationImpl.class,
						vexTargetInformation.getPrimaryKeyObj());
			}

			if (vexTargetInformation != null) {
				session.delete(vexTargetInformation);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (vexTargetInformation != null) {
			clearCache(vexTargetInformation);
		}

		return vexTargetInformation;
	}

	@Override
	public VexTargetInformation updateImpl(
		VexTargetInformation vexTargetInformation) {
		vexTargetInformation = toUnwrappedModel(vexTargetInformation);

		boolean isNew = vexTargetInformation.isNew();

		VexTargetInformationModelImpl vexTargetInformationModelImpl = (VexTargetInformationModelImpl)vexTargetInformation;

		Session session = null;

		try {
			session = openSession();

			if (vexTargetInformation.isNew()) {
				session.save(vexTargetInformation);

				vexTargetInformation.setNew(false);
			}
			else {
				vexTargetInformation = (VexTargetInformation)session.merge(vexTargetInformation);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !VexTargetInformationModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((vexTargetInformationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						vexTargetInformationModelImpl.getOriginalScanid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);

				args = new Object[] { vexTargetInformationModelImpl.getScanid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);
			}
		}

		entityCache.putResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
			VexTargetInformationImpl.class,
			vexTargetInformation.getPrimaryKey(), vexTargetInformation, false);

		vexTargetInformation.resetOriginalValues();

		return vexTargetInformation;
	}

	protected VexTargetInformation toUnwrappedModel(
		VexTargetInformation vexTargetInformation) {
		if (vexTargetInformation instanceof VexTargetInformationImpl) {
			return vexTargetInformation;
		}

		VexTargetInformationImpl vexTargetInformationImpl = new VexTargetInformationImpl();

		vexTargetInformationImpl.setNew(vexTargetInformation.isNew());
		vexTargetInformationImpl.setPrimaryKey(vexTargetInformation.getPrimaryKey());

		vexTargetInformationImpl.setScanid(vexTargetInformation.getScanid());
		vexTargetInformationImpl.setProtocol(vexTargetInformation.getProtocol());
		vexTargetInformationImpl.setHost(vexTargetInformation.getHost());
		vexTargetInformationImpl.setPort(vexTargetInformation.getPort());
		vexTargetInformationImpl.setHttpversion(vexTargetInformation.getHttpversion());
		vexTargetInformationImpl.setSetkeepaliveconnection(vexTargetInformation.getSetkeepaliveconnection());
		vexTargetInformationImpl.setSetresponsecontentlength(vexTargetInformation.getSetresponsecontentlength());
		vexTargetInformationImpl.setUseacceptencodingheader(vexTargetInformation.getUseacceptencodingheader());
		vexTargetInformationImpl.setUnzipresponse(vexTargetInformation.getUnzipresponse());
		vexTargetInformationImpl.setHttpprotocol(vexTargetInformation.getHttpprotocol());
		vexTargetInformationImpl.setExternalproxyhost(vexTargetInformation.getExternalproxyhost());
		vexTargetInformationImpl.setExternalproxyport(vexTargetInformation.getExternalproxyport());
		vexTargetInformationImpl.setExternalproxyauthid(vexTargetInformation.getExternalproxyauthid());
		vexTargetInformationImpl.setExternalproxyauthpassword(vexTargetInformation.getExternalproxyauthpassword());
		vexTargetInformationImpl.setUseclientcertificate(vexTargetInformation.getUseclientcertificate());
		vexTargetInformationImpl.setCertificatefile(vexTargetInformation.getCertificatefile());
		vexTargetInformationImpl.setCertificatefilepassword(vexTargetInformation.getCertificatefilepassword());
		vexTargetInformationImpl.setNtlmauthid(vexTargetInformation.getNtlmauthid());
		vexTargetInformationImpl.setNtlmauthpassword(vexTargetInformation.getNtlmauthpassword());
		vexTargetInformationImpl.setNtlmauthdomain(vexTargetInformation.getNtlmauthdomain());
		vexTargetInformationImpl.setNtlmauthhost(vexTargetInformation.getNtlmauthhost());
		vexTargetInformationImpl.setDigestauthid(vexTargetInformation.getDigestauthid());
		vexTargetInformationImpl.setDigestauthpassword(vexTargetInformation.getDigestauthpassword());
		vexTargetInformationImpl.setBasicauthid(vexTargetInformation.getBasicauthid());
		vexTargetInformationImpl.setBasicauthpassword(vexTargetInformation.getBasicauthpassword());
		vexTargetInformationImpl.setAccessexclusionpath(vexTargetInformation.getAccessexclusionpath());

		return vexTargetInformationImpl;
	}

	/**
	 * Returns the vex target information with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the vex target information
	 * @return the vex target information
	 * @throws NoSuchVexTargetInformationException if a vex target information with the primary key could not be found
	 */
	@Override
	public VexTargetInformation findByPrimaryKey(Serializable primaryKey)
		throws NoSuchVexTargetInformationException {
		VexTargetInformation vexTargetInformation = fetchByPrimaryKey(primaryKey);

		if (vexTargetInformation == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchVexTargetInformationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return vexTargetInformation;
	}

	/**
	 * Returns the vex target information with the primary key or throws a {@link NoSuchVexTargetInformationException} if it could not be found.
	 *
	 * @param scanid the primary key of the vex target information
	 * @return the vex target information
	 * @throws NoSuchVexTargetInformationException if a vex target information with the primary key could not be found
	 */
	@Override
	public VexTargetInformation findByPrimaryKey(long scanid)
		throws NoSuchVexTargetInformationException {
		return findByPrimaryKey((Serializable)scanid);
	}

	/**
	 * Returns the vex target information with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the vex target information
	 * @return the vex target information, or <code>null</code> if a vex target information with the primary key could not be found
	 */
	@Override
	public VexTargetInformation fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
				VexTargetInformationImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		VexTargetInformation vexTargetInformation = (VexTargetInformation)serializable;

		if (vexTargetInformation == null) {
			Session session = null;

			try {
				session = openSession();

				vexTargetInformation = (VexTargetInformation)session.get(VexTargetInformationImpl.class,
						primaryKey);

				if (vexTargetInformation != null) {
					cacheResult(vexTargetInformation);
				}
				else {
					entityCache.putResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
						VexTargetInformationImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
					VexTargetInformationImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return vexTargetInformation;
	}

	/**
	 * Returns the vex target information with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param scanid the primary key of the vex target information
	 * @return the vex target information, or <code>null</code> if a vex target information with the primary key could not be found
	 */
	@Override
	public VexTargetInformation fetchByPrimaryKey(long scanid) {
		return fetchByPrimaryKey((Serializable)scanid);
	}

	@Override
	public Map<Serializable, VexTargetInformation> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, VexTargetInformation> map = new HashMap<Serializable, VexTargetInformation>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			VexTargetInformation vexTargetInformation = fetchByPrimaryKey(primaryKey);

			if (vexTargetInformation != null) {
				map.put(primaryKey, vexTargetInformation);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
					VexTargetInformationImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (VexTargetInformation)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_VEXTARGETINFORMATION_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (VexTargetInformation vexTargetInformation : (List<VexTargetInformation>)q.list()) {
				map.put(vexTargetInformation.getPrimaryKeyObj(),
					vexTargetInformation);

				cacheResult(vexTargetInformation);

				uncachedPrimaryKeys.remove(vexTargetInformation.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(VexTargetInformationModelImpl.ENTITY_CACHE_ENABLED,
					VexTargetInformationImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the vex target informations.
	 *
	 * @return the vex target informations
	 */
	@Override
	public List<VexTargetInformation> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the vex target informations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex target informations
	 * @param end the upper bound of the range of vex target informations (not inclusive)
	 * @return the range of vex target informations
	 */
	@Override
	public List<VexTargetInformation> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the vex target informations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex target informations
	 * @param end the upper bound of the range of vex target informations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of vex target informations
	 */
	@Override
	public List<VexTargetInformation> findAll(int start, int end,
		OrderByComparator<VexTargetInformation> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the vex target informations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexTargetInformationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex target informations
	 * @param end the upper bound of the range of vex target informations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of vex target informations
	 */
	@Override
	public List<VexTargetInformation> findAll(int start, int end,
		OrderByComparator<VexTargetInformation> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<VexTargetInformation> list = null;

		if (retrieveFromCache) {
			list = (List<VexTargetInformation>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_VEXTARGETINFORMATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_VEXTARGETINFORMATION;

				if (pagination) {
					sql = sql.concat(VexTargetInformationModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<VexTargetInformation>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<VexTargetInformation>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the vex target informations from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (VexTargetInformation vexTargetInformation : findAll()) {
			remove(vexTargetInformation);
		}
	}

	/**
	 * Returns the number of vex target informations.
	 *
	 * @return the number of vex target informations
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_VEXTARGETINFORMATION);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return VexTargetInformationModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the vex target information persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(VexTargetInformationImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_VEXTARGETINFORMATION = "SELECT vexTargetInformation FROM VexTargetInformation vexTargetInformation";
	private static final String _SQL_SELECT_VEXTARGETINFORMATION_WHERE_PKS_IN = "SELECT vexTargetInformation FROM VexTargetInformation vexTargetInformation WHERE scanid IN (";
	private static final String _SQL_SELECT_VEXTARGETINFORMATION_WHERE = "SELECT vexTargetInformation FROM VexTargetInformation vexTargetInformation WHERE ";
	private static final String _SQL_COUNT_VEXTARGETINFORMATION = "SELECT COUNT(vexTargetInformation) FROM VexTargetInformation vexTargetInformation";
	private static final String _SQL_COUNT_VEXTARGETINFORMATION_WHERE = "SELECT COUNT(vexTargetInformation) FROM VexTargetInformation vexTargetInformation WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "vexTargetInformation.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VexTargetInformation exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VexTargetInformation exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(VexTargetInformationPersistenceImpl.class);
}