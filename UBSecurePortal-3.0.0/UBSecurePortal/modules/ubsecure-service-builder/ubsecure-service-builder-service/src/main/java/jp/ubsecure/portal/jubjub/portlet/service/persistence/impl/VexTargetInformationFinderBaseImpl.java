/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;

import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexTargetInformationPersistence;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class VexTargetInformationFinderBaseImpl extends BasePersistenceImpl<VexTargetInformation> {
	/**
	 * Returns the vex target information persistence.
	 *
	 * @return the vex target information persistence
	 */
	public VexTargetInformationPersistence getVexTargetInformationPersistence() {
		return vexTargetInformationPersistence;
	}

	/**
	 * Sets the vex target information persistence.
	 *
	 * @param vexTargetInformationPersistence the vex target information persistence
	 */
	public void setVexTargetInformationPersistence(
		VexTargetInformationPersistence vexTargetInformationPersistence) {
		this.vexTargetInformationPersistence = vexTargetInformationPersistence;
	}

	@BeanReference(type = VexTargetInformationPersistence.class)
	protected VexTargetInformationPersistence vexTargetInformationPersistence;
}