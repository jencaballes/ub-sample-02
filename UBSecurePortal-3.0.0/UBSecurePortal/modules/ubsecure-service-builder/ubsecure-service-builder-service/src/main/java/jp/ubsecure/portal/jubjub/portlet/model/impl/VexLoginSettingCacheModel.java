/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing VexLoginSetting in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see VexLoginSetting
 * @generated
 */
@ProviderType
public class VexLoginSettingCacheModel implements CacheModel<VexLoginSetting>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VexLoginSettingCacheModel)) {
			return false;
		}

		VexLoginSettingCacheModel vexLoginSettingCacheModel = (VexLoginSettingCacheModel)obj;

		if (scanid == vexLoginSettingCacheModel.scanid) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, scanid);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{scanid=");
		sb.append(scanid);
		sb.append(", loginurl=");
		sb.append(loginurl);
		sb.append(", paramname=");
		sb.append(paramname);
		sb.append(", paramvalue=");
		sb.append(paramvalue);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public VexLoginSetting toEntityModel() {
		VexLoginSettingImpl vexLoginSettingImpl = new VexLoginSettingImpl();

		vexLoginSettingImpl.setScanid(scanid);

		if (loginurl == null) {
			vexLoginSettingImpl.setLoginurl(StringPool.BLANK);
		}
		else {
			vexLoginSettingImpl.setLoginurl(loginurl);
		}

		if (paramname == null) {
			vexLoginSettingImpl.setParamname(StringPool.BLANK);
		}
		else {
			vexLoginSettingImpl.setParamname(paramname);
		}

		if (paramvalue == null) {
			vexLoginSettingImpl.setParamvalue(StringPool.BLANK);
		}
		else {
			vexLoginSettingImpl.setParamvalue(paramvalue);
		}

		vexLoginSettingImpl.resetOriginalValues();

		return vexLoginSettingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		scanid = objectInput.readLong();
		loginurl = objectInput.readUTF();
		paramname = objectInput.readUTF();
		paramvalue = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(scanid);

		if (loginurl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(loginurl);
		}

		if (paramname == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paramname);
		}

		if (paramvalue == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paramvalue);
		}
	}

	public long scanid;
	public String loginurl;
	public String paramname;
	public String paramvalue;
}