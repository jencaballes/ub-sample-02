/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchVexDetectionResultException;
import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultImpl;
import jp.ubsecure.portal.jubjub.portlet.model.impl.VexDetectionResultModelImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the vex detection result service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResultPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultUtil
 * @generated
 */
@ProviderType
public class VexDetectionResultPersistenceImpl extends BasePersistenceImpl<VexDetectionResult>
	implements VexDetectionResultPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link VexDetectionResultUtil} to access the vex detection result persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = VexDetectionResultImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultModelImpl.FINDER_CACHE_ENABLED,
			VexDetectionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultModelImpl.FINDER_CACHE_ENABLED,
			VexDetectionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID = new FinderPath(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultModelImpl.FINDER_CACHE_ENABLED,
			VexDetectionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByScanId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID =
		new FinderPath(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultModelImpl.FINDER_CACHE_ENABLED,
			VexDetectionResultImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByScanId",
			new String[] { Long.class.getName() },
			VexDetectionResultModelImpl.SCANID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SCANID = new FinderPath(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScanId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the vex detection results where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @return the matching vex detection results
	 */
	@Override
	public List<VexDetectionResult> findByScanId(long scanid) {
		return findByScanId(scanid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the vex detection results where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex detection results
	 * @param end the upper bound of the range of vex detection results (not inclusive)
	 * @return the range of matching vex detection results
	 */
	@Override
	public List<VexDetectionResult> findByScanId(long scanid, int start, int end) {
		return findByScanId(scanid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the vex detection results where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex detection results
	 * @param end the upper bound of the range of vex detection results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching vex detection results
	 */
	@Override
	public List<VexDetectionResult> findByScanId(long scanid, int start,
		int end, OrderByComparator<VexDetectionResult> orderByComparator) {
		return findByScanId(scanid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the vex detection results where scanid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanid the scanid
	 * @param start the lower bound of the range of vex detection results
	 * @param end the upper bound of the range of vex detection results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching vex detection results
	 */
	@Override
	public List<VexDetectionResult> findByScanId(long scanid, int start,
		int end, OrderByComparator<VexDetectionResult> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanid, start, end, orderByComparator };
		}

		List<VexDetectionResult> list = null;

		if (retrieveFromCache) {
			list = (List<VexDetectionResult>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (VexDetectionResult vexDetectionResult : list) {
					if ((scanid != vexDetectionResult.getScanid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_VEXDETECTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(VexDetectionResultModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanid);

				if (!pagination) {
					list = (List<VexDetectionResult>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<VexDetectionResult>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first vex detection result in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching vex detection result
	 * @throws NoSuchVexDetectionResultException if a matching vex detection result could not be found
	 */
	@Override
	public VexDetectionResult findByScanId_First(long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator)
		throws NoSuchVexDetectionResultException {
		VexDetectionResult vexDetectionResult = fetchByScanId_First(scanid,
				orderByComparator);

		if (vexDetectionResult != null) {
			return vexDetectionResult;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanid=");
		msg.append(scanid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVexDetectionResultException(msg.toString());
	}

	/**
	 * Returns the first vex detection result in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching vex detection result, or <code>null</code> if a matching vex detection result could not be found
	 */
	@Override
	public VexDetectionResult fetchByScanId_First(long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator) {
		List<VexDetectionResult> list = findByScanId(scanid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last vex detection result in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching vex detection result
	 * @throws NoSuchVexDetectionResultException if a matching vex detection result could not be found
	 */
	@Override
	public VexDetectionResult findByScanId_Last(long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator)
		throws NoSuchVexDetectionResultException {
		VexDetectionResult vexDetectionResult = fetchByScanId_Last(scanid,
				orderByComparator);

		if (vexDetectionResult != null) {
			return vexDetectionResult;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanid=");
		msg.append(scanid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVexDetectionResultException(msg.toString());
	}

	/**
	 * Returns the last vex detection result in the ordered set where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching vex detection result, or <code>null</code> if a matching vex detection result could not be found
	 */
	@Override
	public VexDetectionResult fetchByScanId_Last(long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator) {
		int count = countByScanId(scanid);

		if (count == 0) {
			return null;
		}

		List<VexDetectionResult> list = findByScanId(scanid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the vex detection results before and after the current vex detection result in the ordered set where scanid = &#63;.
	 *
	 * @param vexDetectionResultPK the primary key of the current vex detection result
	 * @param scanid the scanid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next vex detection result
	 * @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	 */
	@Override
	public VexDetectionResult[] findByScanId_PrevAndNext(
		VexDetectionResultPK vexDetectionResultPK, long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator)
		throws NoSuchVexDetectionResultException {
		VexDetectionResult vexDetectionResult = findByPrimaryKey(vexDetectionResultPK);

		Session session = null;

		try {
			session = openSession();

			VexDetectionResult[] array = new VexDetectionResultImpl[3];

			array[0] = getByScanId_PrevAndNext(session, vexDetectionResult,
					scanid, orderByComparator, true);

			array[1] = vexDetectionResult;

			array[2] = getByScanId_PrevAndNext(session, vexDetectionResult,
					scanid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected VexDetectionResult getByScanId_PrevAndNext(Session session,
		VexDetectionResult vexDetectionResult, long scanid,
		OrderByComparator<VexDetectionResult> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_VEXDETECTIONRESULT_WHERE);

		query.append(_FINDER_COLUMN_SCANID_SCANID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(VexDetectionResultModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(scanid);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(vexDetectionResult);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<VexDetectionResult> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the vex detection results where scanid = &#63; from the database.
	 *
	 * @param scanid the scanid
	 */
	@Override
	public void removeByScanId(long scanid) {
		for (VexDetectionResult vexDetectionResult : findByScanId(scanid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(vexDetectionResult);
		}
	}

	/**
	 * Returns the number of vex detection results where scanid = &#63;.
	 *
	 * @param scanid the scanid
	 * @return the number of matching vex detection results
	 */
	@Override
	public int countByScanId(long scanid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SCANID;

		Object[] finderArgs = new Object[] { scanid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_VEXDETECTIONRESULT_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanid);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCANID_SCANID_2 = "vexDetectionResult.id.scanid = ?";

	public VexDetectionResultPersistenceImpl() {
		setModelClass(VexDetectionResult.class);
	}

	/**
	 * Caches the vex detection result in the entity cache if it is enabled.
	 *
	 * @param vexDetectionResult the vex detection result
	 */
	@Override
	public void cacheResult(VexDetectionResult vexDetectionResult) {
		entityCache.putResult(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultImpl.class, vexDetectionResult.getPrimaryKey(),
			vexDetectionResult);

		vexDetectionResult.resetOriginalValues();
	}

	/**
	 * Caches the vex detection results in the entity cache if it is enabled.
	 *
	 * @param vexDetectionResults the vex detection results
	 */
	@Override
	public void cacheResult(List<VexDetectionResult> vexDetectionResults) {
		for (VexDetectionResult vexDetectionResult : vexDetectionResults) {
			if (entityCache.getResult(
						VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
						VexDetectionResultImpl.class,
						vexDetectionResult.getPrimaryKey()) == null) {
				cacheResult(vexDetectionResult);
			}
			else {
				vexDetectionResult.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all vex detection results.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(VexDetectionResultImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the vex detection result.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(VexDetectionResult vexDetectionResult) {
		entityCache.removeResult(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultImpl.class, vexDetectionResult.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<VexDetectionResult> vexDetectionResults) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (VexDetectionResult vexDetectionResult : vexDetectionResults) {
			entityCache.removeResult(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
				VexDetectionResultImpl.class, vexDetectionResult.getPrimaryKey());
		}
	}

	/**
	 * Creates a new vex detection result with the primary key. Does not add the vex detection result to the database.
	 *
	 * @param vexDetectionResultPK the primary key for the new vex detection result
	 * @return the new vex detection result
	 */
	@Override
	public VexDetectionResult create(VexDetectionResultPK vexDetectionResultPK) {
		VexDetectionResult vexDetectionResult = new VexDetectionResultImpl();

		vexDetectionResult.setNew(true);
		vexDetectionResult.setPrimaryKey(vexDetectionResultPK);

		return vexDetectionResult;
	}

	/**
	 * Removes the vex detection result with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param vexDetectionResultPK the primary key of the vex detection result
	 * @return the vex detection result that was removed
	 * @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	 */
	@Override
	public VexDetectionResult remove(VexDetectionResultPK vexDetectionResultPK)
		throws NoSuchVexDetectionResultException {
		return remove((Serializable)vexDetectionResultPK);
	}

	/**
	 * Removes the vex detection result with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the vex detection result
	 * @return the vex detection result that was removed
	 * @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	 */
	@Override
	public VexDetectionResult remove(Serializable primaryKey)
		throws NoSuchVexDetectionResultException {
		Session session = null;

		try {
			session = openSession();

			VexDetectionResult vexDetectionResult = (VexDetectionResult)session.get(VexDetectionResultImpl.class,
					primaryKey);

			if (vexDetectionResult == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchVexDetectionResultException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(vexDetectionResult);
		}
		catch (NoSuchVexDetectionResultException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected VexDetectionResult removeImpl(
		VexDetectionResult vexDetectionResult) {
		vexDetectionResult = toUnwrappedModel(vexDetectionResult);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(vexDetectionResult)) {
				vexDetectionResult = (VexDetectionResult)session.get(VexDetectionResultImpl.class,
						vexDetectionResult.getPrimaryKeyObj());
			}

			if (vexDetectionResult != null) {
				session.delete(vexDetectionResult);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (vexDetectionResult != null) {
			clearCache(vexDetectionResult);
		}

		return vexDetectionResult;
	}

	@Override
	public VexDetectionResult updateImpl(VexDetectionResult vexDetectionResult) {
		vexDetectionResult = toUnwrappedModel(vexDetectionResult);

		boolean isNew = vexDetectionResult.isNew();

		VexDetectionResultModelImpl vexDetectionResultModelImpl = (VexDetectionResultModelImpl)vexDetectionResult;

		Session session = null;

		try {
			session = openSession();

			if (vexDetectionResult.isNew()) {
				session.save(vexDetectionResult);

				vexDetectionResult.setNew(false);
			}
			else {
				vexDetectionResult = (VexDetectionResult)session.merge(vexDetectionResult);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !VexDetectionResultModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((vexDetectionResultModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						vexDetectionResultModelImpl.getOriginalScanid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);

				args = new Object[] { vexDetectionResultModelImpl.getScanid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);
			}
		}

		entityCache.putResult(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
			VexDetectionResultImpl.class, vexDetectionResult.getPrimaryKey(),
			vexDetectionResult, false);

		vexDetectionResult.resetOriginalValues();

		return vexDetectionResult;
	}

	protected VexDetectionResult toUnwrappedModel(
		VexDetectionResult vexDetectionResult) {
		if (vexDetectionResult instanceof VexDetectionResultImpl) {
			return vexDetectionResult;
		}

		VexDetectionResultImpl vexDetectionResultImpl = new VexDetectionResultImpl();

		vexDetectionResultImpl.setNew(vexDetectionResult.isNew());
		vexDetectionResultImpl.setPrimaryKey(vexDetectionResult.getPrimaryKey());

		vexDetectionResultImpl.setScanresultid(vexDetectionResult.getScanresultid());
		vexDetectionResultImpl.setScanid(vexDetectionResult.getScanid());
		vexDetectionResultImpl.setDetectionresultid(vexDetectionResult.getDetectionresultid());
		vexDetectionResultImpl.setRisklevel(vexDetectionResult.getRisklevel());
		vexDetectionResultImpl.setCategory(vexDetectionResult.getCategory());
		vexDetectionResultImpl.setOverview(vexDetectionResult.getOverview());
		vexDetectionResultImpl.setFunctionname(vexDetectionResult.getFunctionname());
		vexDetectionResultImpl.setUrl(vexDetectionResult.getUrl());
		vexDetectionResultImpl.setParametername(vexDetectionResult.getParametername());
		vexDetectionResultImpl.setDetectionjudgment(vexDetectionResult.getDetectionjudgment());
		vexDetectionResultImpl.setReviewcomment(vexDetectionResult.getReviewcomment());
		vexDetectionResultImpl.setIsresendinvex(vexDetectionResult.getIsresendinvex());
		vexDetectionResultImpl.setListofdetectedresult(vexDetectionResult.getListofdetectedresult());
		vexDetectionResultImpl.setListofinspectiondatetime(vexDetectionResult.getListofinspectiondatetime());

		return vexDetectionResultImpl;
	}

	/**
	 * Returns the vex detection result with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the vex detection result
	 * @return the vex detection result
	 * @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	 */
	@Override
	public VexDetectionResult findByPrimaryKey(Serializable primaryKey)
		throws NoSuchVexDetectionResultException {
		VexDetectionResult vexDetectionResult = fetchByPrimaryKey(primaryKey);

		if (vexDetectionResult == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchVexDetectionResultException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return vexDetectionResult;
	}

	/**
	 * Returns the vex detection result with the primary key or throws a {@link NoSuchVexDetectionResultException} if it could not be found.
	 *
	 * @param vexDetectionResultPK the primary key of the vex detection result
	 * @return the vex detection result
	 * @throws NoSuchVexDetectionResultException if a vex detection result with the primary key could not be found
	 */
	@Override
	public VexDetectionResult findByPrimaryKey(
		VexDetectionResultPK vexDetectionResultPK)
		throws NoSuchVexDetectionResultException {
		return findByPrimaryKey((Serializable)vexDetectionResultPK);
	}

	/**
	 * Returns the vex detection result with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the vex detection result
	 * @return the vex detection result, or <code>null</code> if a vex detection result with the primary key could not be found
	 */
	@Override
	public VexDetectionResult fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
				VexDetectionResultImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		VexDetectionResult vexDetectionResult = (VexDetectionResult)serializable;

		if (vexDetectionResult == null) {
			Session session = null;

			try {
				session = openSession();

				vexDetectionResult = (VexDetectionResult)session.get(VexDetectionResultImpl.class,
						primaryKey);

				if (vexDetectionResult != null) {
					cacheResult(vexDetectionResult);
				}
				else {
					entityCache.putResult(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
						VexDetectionResultImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(VexDetectionResultModelImpl.ENTITY_CACHE_ENABLED,
					VexDetectionResultImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return vexDetectionResult;
	}

	/**
	 * Returns the vex detection result with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param vexDetectionResultPK the primary key of the vex detection result
	 * @return the vex detection result, or <code>null</code> if a vex detection result with the primary key could not be found
	 */
	@Override
	public VexDetectionResult fetchByPrimaryKey(
		VexDetectionResultPK vexDetectionResultPK) {
		return fetchByPrimaryKey((Serializable)vexDetectionResultPK);
	}

	@Override
	public Map<Serializable, VexDetectionResult> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, VexDetectionResult> map = new HashMap<Serializable, VexDetectionResult>();

		for (Serializable primaryKey : primaryKeys) {
			VexDetectionResult vexDetectionResult = fetchByPrimaryKey(primaryKey);

			if (vexDetectionResult != null) {
				map.put(primaryKey, vexDetectionResult);
			}
		}

		return map;
	}

	/**
	 * Returns all the vex detection results.
	 *
	 * @return the vex detection results
	 */
	@Override
	public List<VexDetectionResult> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the vex detection results.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex detection results
	 * @param end the upper bound of the range of vex detection results (not inclusive)
	 * @return the range of vex detection results
	 */
	@Override
	public List<VexDetectionResult> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the vex detection results.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex detection results
	 * @param end the upper bound of the range of vex detection results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of vex detection results
	 */
	@Override
	public List<VexDetectionResult> findAll(int start, int end,
		OrderByComparator<VexDetectionResult> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the vex detection results.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VexDetectionResultModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of vex detection results
	 * @param end the upper bound of the range of vex detection results (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of vex detection results
	 */
	@Override
	public List<VexDetectionResult> findAll(int start, int end,
		OrderByComparator<VexDetectionResult> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<VexDetectionResult> list = null;

		if (retrieveFromCache) {
			list = (List<VexDetectionResult>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_VEXDETECTIONRESULT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_VEXDETECTIONRESULT;

				if (pagination) {
					sql = sql.concat(VexDetectionResultModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<VexDetectionResult>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<VexDetectionResult>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the vex detection results from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (VexDetectionResult vexDetectionResult : findAll()) {
			remove(vexDetectionResult);
		}
	}

	/**
	 * Returns the number of vex detection results.
	 *
	 * @return the number of vex detection results
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_VEXDETECTIONRESULT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return VexDetectionResultModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the vex detection result persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(VexDetectionResultImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_VEXDETECTIONRESULT = "SELECT vexDetectionResult FROM VexDetectionResult vexDetectionResult";
	private static final String _SQL_SELECT_VEXDETECTIONRESULT_WHERE = "SELECT vexDetectionResult FROM VexDetectionResult vexDetectionResult WHERE ";
	private static final String _SQL_COUNT_VEXDETECTIONRESULT = "SELECT COUNT(vexDetectionResult) FROM VexDetectionResult vexDetectionResult";
	private static final String _SQL_COUNT_VEXDETECTIONRESULT_WHERE = "SELECT COUNT(vexDetectionResult) FROM VexDetectionResult vexDetectionResult WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "vexDetectionResult.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VexDetectionResult exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VexDetectionResult exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(VexDetectionResultPersistenceImpl.class);
}