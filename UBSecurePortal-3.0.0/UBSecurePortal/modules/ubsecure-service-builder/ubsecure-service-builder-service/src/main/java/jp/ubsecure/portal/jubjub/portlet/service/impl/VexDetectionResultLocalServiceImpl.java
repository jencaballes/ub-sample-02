/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;

import aQute.bnd.annotation.ProviderType;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.service.base.VexDetectionResultLocalServiceBaseImpl;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

/**
 * The implementation of the vex detection result local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link jp.ubsecure.portal.jubjub.portlet.service.VexDetectionResultLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VexDetectionResultLocalServiceBaseImpl
 * @see jp.ubsecure.portal.jubjub.portlet.service.VexDetectionResultLocalServiceUtil
 */
@ProviderType
public class VexDetectionResultLocalServiceImpl
	extends VexDetectionResultLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link jp.ubsecure.portal.jubjub.portlet.service.VexDetectionResultLocalServiceUtil} to access the vex detection result local service.
	 */
	
	/** UBSPortalDebugger */
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexDetectionResultLocalServiceImpl.class);
	
	/**
	 * Get List of Detection Result
	 * 
	 * @param lscanId
	 *            - id of the scan on which detection results should be retrieved
	 * @param searchedDetectionResult
	 *            - a key-value parameter inputted by user during search.
	 * @throws PortalException
	 *             an exception thrown if error occurs
	 */
	public List<Object> getVexDetectionResultReviews(long lscanId, User user, Map<String, Object> searchedDetectionResult, int start, String orderByCol, String orderByType)
			throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> detectionResultList = new ArrayList<Object>();

		try {
			if (lscanId == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new PortalException(PortalErrors.INVALID_SCAN_ID);
			}

			if (!CommonUtil.isMapNullOrEmpty(searchedDetectionResult)) {
				
			}
			detectionResultList =   vexDetectionResultFinder.getDetectionResult(lscanId, searchedDetectionResult, start, orderByCol, orderByType) ;
		} catch (PortalException pe) {
			params.put("lScanId", lscanId);
			params.put("user", user);
			params.put("searchedDetectionResult", searchedDetectionResult);
			log.debug(pe.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, pe);
			throw pe;
		} finally {
			params.clear();
			params = null;
		}

		return detectionResultList;
	}
	
	/**
	 * Get the number of scans
	 * 
	 * @param lProjectId
	 *            - id of the project on which project scans to count
	 * @param searchedScan
	 *            a key-value parameter inputted by user during search
	 * @return Number of scans
	 * @throws PortalException
	 *             an exception thrown if error occurs
	 */
	public int getDetectionResultsCount(long lscanId, Map<String, Object> searchedDetectionResult) throws PortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		int count = PortalConstants.INT_ZERO;

		try {
			if (lscanId == PortalConstants.LONG_ZERO) {
				params.put("lscanId", lscanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new PortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			count = vexDetectionResultFinder.getDetectionResultsCount(lscanId, searchedDetectionResult);
		} catch (PortalException e) {
			params.put("lscanId", lscanId);
			params.put("searchedDetectionResult", searchedDetectionResult);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_COUNT, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		return count;
	}
	
	
}