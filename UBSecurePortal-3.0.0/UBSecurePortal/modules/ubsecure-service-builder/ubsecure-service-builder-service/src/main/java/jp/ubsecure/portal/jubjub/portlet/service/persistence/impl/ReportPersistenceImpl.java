/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package jp.ubsecure.portal.jubjub.portlet.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchReportException;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ReportImpl;
import jp.ubsecure.portal.jubjub.portlet.model.impl.ReportModelImpl;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.ReportPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the report service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ReportPersistence
 * @see jp.ubsecure.portal.jubjub.portlet.service.persistence.ReportUtil
 * @generated
 */
@ProviderType
public class ReportPersistenceImpl extends BasePersistenceImpl<Report>
	implements ReportPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ReportUtil} to access the report persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ReportImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, ReportImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, ReportImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID = new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, ReportImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByScanId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID =
		new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, ReportImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByScanId",
			new String[] { Long.class.getName() },
			ReportModelImpl.SCANID_COLUMN_BITMASK |
			ReportModelImpl.REPORTDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SCANID = new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScanId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the reports where scanId = &#63;.
	 *
	 * @param scanId the scan ID
	 * @return the matching reports
	 */
	@Override
	public List<Report> findByScanId(long scanId) {
		return findByScanId(scanId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reports where scanId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @return the range of matching reports
	 */
	@Override
	public List<Report> findByScanId(long scanId, int start, int end) {
		return findByScanId(scanId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the reports where scanId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching reports
	 */
	@Override
	public List<Report> findByScanId(long scanId, int start, int end,
		OrderByComparator<Report> orderByComparator) {
		return findByScanId(scanId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the reports where scanId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching reports
	 */
	@Override
	public List<Report> findByScanId(long scanId, int start, int end,
		OrderByComparator<Report> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANID;
			finderArgs = new Object[] { scanId, start, end, orderByComparator };
		}

		List<Report> list = null;

		if (retrieveFromCache) {
			list = (List<Report>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Report report : list) {
					if ((scanId != report.getScanId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REPORT_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReportModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanId);

				if (!pagination) {
					list = (List<Report>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Report>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first report in the ordered set where scanId = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching report
	 * @throws NoSuchReportException if a matching report could not be found
	 */
	@Override
	public Report findByScanId_First(long scanId,
		OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = fetchByScanId_First(scanId, orderByComparator);

		if (report != null) {
			return report;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanId=");
		msg.append(scanId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportException(msg.toString());
	}

	/**
	 * Returns the first report in the ordered set where scanId = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching report, or <code>null</code> if a matching report could not be found
	 */
	@Override
	public Report fetchByScanId_First(long scanId,
		OrderByComparator<Report> orderByComparator) {
		List<Report> list = findByScanId(scanId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last report in the ordered set where scanId = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching report
	 * @throws NoSuchReportException if a matching report could not be found
	 */
	@Override
	public Report findByScanId_Last(long scanId,
		OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = fetchByScanId_Last(scanId, orderByComparator);

		if (report != null) {
			return report;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanId=");
		msg.append(scanId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportException(msg.toString());
	}

	/**
	 * Returns the last report in the ordered set where scanId = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching report, or <code>null</code> if a matching report could not be found
	 */
	@Override
	public Report fetchByScanId_Last(long scanId,
		OrderByComparator<Report> orderByComparator) {
		int count = countByScanId(scanId);

		if (count == 0) {
			return null;
		}

		List<Report> list = findByScanId(scanId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the reports before and after the current report in the ordered set where scanId = &#63;.
	 *
	 * @param reportId the primary key of the current report
	 * @param scanId the scan ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next report
	 * @throws NoSuchReportException if a report with the primary key could not be found
	 */
	@Override
	public Report[] findByScanId_PrevAndNext(long reportId, long scanId,
		OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = findByPrimaryKey(reportId);

		Session session = null;

		try {
			session = openSession();

			Report[] array = new ReportImpl[3];

			array[0] = getByScanId_PrevAndNext(session, report, scanId,
					orderByComparator, true);

			array[1] = report;

			array[2] = getByScanId_PrevAndNext(session, report, scanId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Report getByScanId_PrevAndNext(Session session, Report report,
		long scanId, OrderByComparator<Report> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REPORT_WHERE);

		query.append(_FINDER_COLUMN_SCANID_SCANID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReportModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(scanId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(report);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Report> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the reports where scanId = &#63; from the database.
	 *
	 * @param scanId the scan ID
	 */
	@Override
	public void removeByScanId(long scanId) {
		for (Report report : findByScanId(scanId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(report);
		}
	}

	/**
	 * Returns the number of reports where scanId = &#63;.
	 *
	 * @param scanId the scan ID
	 * @return the number of matching reports
	 */
	@Override
	public int countByScanId(long scanId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SCANID;

		Object[] finderArgs = new Object[] { scanId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REPORT_WHERE);

			query.append(_FINDER_COLUMN_SCANID_SCANID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCANID_SCANID_2 = "report.scanId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REPORTTYPE =
		new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, ReportImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByReportType",
			new String[] {
				Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTTYPE =
		new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, ReportImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByReportType",
			new String[] { Integer.class.getName() },
			ReportModelImpl.REPORTTYPE_COLUMN_BITMASK |
			ReportModelImpl.REPORTDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REPORTTYPE = new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByReportType",
			new String[] { Integer.class.getName() });

	/**
	 * Returns all the reports where reportType = &#63;.
	 *
	 * @param reportType the report type
	 * @return the matching reports
	 */
	@Override
	public List<Report> findByReportType(int reportType) {
		return findByReportType(reportType, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reports where reportType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reportType the report type
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @return the range of matching reports
	 */
	@Override
	public List<Report> findByReportType(int reportType, int start, int end) {
		return findByReportType(reportType, start, end, null);
	}

	/**
	 * Returns an ordered range of all the reports where reportType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reportType the report type
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching reports
	 */
	@Override
	public List<Report> findByReportType(int reportType, int start, int end,
		OrderByComparator<Report> orderByComparator) {
		return findByReportType(reportType, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the reports where reportType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param reportType the report type
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching reports
	 */
	@Override
	public List<Report> findByReportType(int reportType, int start, int end,
		OrderByComparator<Report> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTTYPE;
			finderArgs = new Object[] { reportType };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REPORTTYPE;
			finderArgs = new Object[] { reportType, start, end, orderByComparator };
		}

		List<Report> list = null;

		if (retrieveFromCache) {
			list = (List<Report>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Report report : list) {
					if ((reportType != report.getReportType())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REPORT_WHERE);

			query.append(_FINDER_COLUMN_REPORTTYPE_REPORTTYPE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReportModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(reportType);

				if (!pagination) {
					list = (List<Report>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Report>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first report in the ordered set where reportType = &#63;.
	 *
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching report
	 * @throws NoSuchReportException if a matching report could not be found
	 */
	@Override
	public Report findByReportType_First(int reportType,
		OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = fetchByReportType_First(reportType, orderByComparator);

		if (report != null) {
			return report;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("reportType=");
		msg.append(reportType);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportException(msg.toString());
	}

	/**
	 * Returns the first report in the ordered set where reportType = &#63;.
	 *
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching report, or <code>null</code> if a matching report could not be found
	 */
	@Override
	public Report fetchByReportType_First(int reportType,
		OrderByComparator<Report> orderByComparator) {
		List<Report> list = findByReportType(reportType, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last report in the ordered set where reportType = &#63;.
	 *
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching report
	 * @throws NoSuchReportException if a matching report could not be found
	 */
	@Override
	public Report findByReportType_Last(int reportType,
		OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = fetchByReportType_Last(reportType, orderByComparator);

		if (report != null) {
			return report;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("reportType=");
		msg.append(reportType);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportException(msg.toString());
	}

	/**
	 * Returns the last report in the ordered set where reportType = &#63;.
	 *
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching report, or <code>null</code> if a matching report could not be found
	 */
	@Override
	public Report fetchByReportType_Last(int reportType,
		OrderByComparator<Report> orderByComparator) {
		int count = countByReportType(reportType);

		if (count == 0) {
			return null;
		}

		List<Report> list = findByReportType(reportType, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the reports before and after the current report in the ordered set where reportType = &#63;.
	 *
	 * @param reportId the primary key of the current report
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next report
	 * @throws NoSuchReportException if a report with the primary key could not be found
	 */
	@Override
	public Report[] findByReportType_PrevAndNext(long reportId, int reportType,
		OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = findByPrimaryKey(reportId);

		Session session = null;

		try {
			session = openSession();

			Report[] array = new ReportImpl[3];

			array[0] = getByReportType_PrevAndNext(session, report, reportType,
					orderByComparator, true);

			array[1] = report;

			array[2] = getByReportType_PrevAndNext(session, report, reportType,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Report getByReportType_PrevAndNext(Session session,
		Report report, int reportType,
		OrderByComparator<Report> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REPORT_WHERE);

		query.append(_FINDER_COLUMN_REPORTTYPE_REPORTTYPE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReportModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(reportType);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(report);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Report> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the reports where reportType = &#63; from the database.
	 *
	 * @param reportType the report type
	 */
	@Override
	public void removeByReportType(int reportType) {
		for (Report report : findByReportType(reportType, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(report);
		}
	}

	/**
	 * Returns the number of reports where reportType = &#63;.
	 *
	 * @param reportType the report type
	 * @return the number of matching reports
	 */
	@Override
	public int countByReportType(int reportType) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REPORTTYPE;

		Object[] finderArgs = new Object[] { reportType };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REPORT_WHERE);

			query.append(_FINDER_COLUMN_REPORTTYPE_REPORTTYPE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(reportType);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REPORTTYPE_REPORTTYPE_2 = "report.reportType = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANIDREPORTTYPE =
		new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, ReportImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByScanIdReportType",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDREPORTTYPE =
		new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, ReportImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByScanIdReportType",
			new String[] { Long.class.getName(), Integer.class.getName() },
			ReportModelImpl.SCANID_COLUMN_BITMASK |
			ReportModelImpl.REPORTTYPE_COLUMN_BITMASK |
			ReportModelImpl.REPORTDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SCANIDREPORTTYPE = new FinderPath(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByScanIdReportType",
			new String[] { Long.class.getName(), Integer.class.getName() });

	/**
	 * Returns all the reports where scanId = &#63; and reportType = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @return the matching reports
	 */
	@Override
	public List<Report> findByScanIdReportType(long scanId, int reportType) {
		return findByScanIdReportType(scanId, reportType, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reports where scanId = &#63; and reportType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @return the range of matching reports
	 */
	@Override
	public List<Report> findByScanIdReportType(long scanId, int reportType,
		int start, int end) {
		return findByScanIdReportType(scanId, reportType, start, end, null);
	}

	/**
	 * Returns an ordered range of all the reports where scanId = &#63; and reportType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching reports
	 */
	@Override
	public List<Report> findByScanIdReportType(long scanId, int reportType,
		int start, int end, OrderByComparator<Report> orderByComparator) {
		return findByScanIdReportType(scanId, reportType, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the reports where scanId = &#63; and reportType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching reports
	 */
	@Override
	public List<Report> findByScanIdReportType(long scanId, int reportType,
		int start, int end, OrderByComparator<Report> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDREPORTTYPE;
			finderArgs = new Object[] { scanId, reportType };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SCANIDREPORTTYPE;
			finderArgs = new Object[] {
					scanId, reportType,
					
					start, end, orderByComparator
				};
		}

		List<Report> list = null;

		if (retrieveFromCache) {
			list = (List<Report>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Report report : list) {
					if ((scanId != report.getScanId()) ||
							(reportType != report.getReportType())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_REPORT_WHERE);

			query.append(_FINDER_COLUMN_SCANIDREPORTTYPE_SCANID_2);

			query.append(_FINDER_COLUMN_SCANIDREPORTTYPE_REPORTTYPE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ReportModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanId);

				qPos.add(reportType);

				if (!pagination) {
					list = (List<Report>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Report>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first report in the ordered set where scanId = &#63; and reportType = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching report
	 * @throws NoSuchReportException if a matching report could not be found
	 */
	@Override
	public Report findByScanIdReportType_First(long scanId, int reportType,
		OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = fetchByScanIdReportType_First(scanId, reportType,
				orderByComparator);

		if (report != null) {
			return report;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanId=");
		msg.append(scanId);

		msg.append(", reportType=");
		msg.append(reportType);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportException(msg.toString());
	}

	/**
	 * Returns the first report in the ordered set where scanId = &#63; and reportType = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching report, or <code>null</code> if a matching report could not be found
	 */
	@Override
	public Report fetchByScanIdReportType_First(long scanId, int reportType,
		OrderByComparator<Report> orderByComparator) {
		List<Report> list = findByScanIdReportType(scanId, reportType, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last report in the ordered set where scanId = &#63; and reportType = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching report
	 * @throws NoSuchReportException if a matching report could not be found
	 */
	@Override
	public Report findByScanIdReportType_Last(long scanId, int reportType,
		OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = fetchByScanIdReportType_Last(scanId, reportType,
				orderByComparator);

		if (report != null) {
			return report;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("scanId=");
		msg.append(scanId);

		msg.append(", reportType=");
		msg.append(reportType);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchReportException(msg.toString());
	}

	/**
	 * Returns the last report in the ordered set where scanId = &#63; and reportType = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching report, or <code>null</code> if a matching report could not be found
	 */
	@Override
	public Report fetchByScanIdReportType_Last(long scanId, int reportType,
		OrderByComparator<Report> orderByComparator) {
		int count = countByScanIdReportType(scanId, reportType);

		if (count == 0) {
			return null;
		}

		List<Report> list = findByScanIdReportType(scanId, reportType,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the reports before and after the current report in the ordered set where scanId = &#63; and reportType = &#63;.
	 *
	 * @param reportId the primary key of the current report
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next report
	 * @throws NoSuchReportException if a report with the primary key could not be found
	 */
	@Override
	public Report[] findByScanIdReportType_PrevAndNext(long reportId,
		long scanId, int reportType, OrderByComparator<Report> orderByComparator)
		throws NoSuchReportException {
		Report report = findByPrimaryKey(reportId);

		Session session = null;

		try {
			session = openSession();

			Report[] array = new ReportImpl[3];

			array[0] = getByScanIdReportType_PrevAndNext(session, report,
					scanId, reportType, orderByComparator, true);

			array[1] = report;

			array[2] = getByScanIdReportType_PrevAndNext(session, report,
					scanId, reportType, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Report getByScanIdReportType_PrevAndNext(Session session,
		Report report, long scanId, int reportType,
		OrderByComparator<Report> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_REPORT_WHERE);

		query.append(_FINDER_COLUMN_SCANIDREPORTTYPE_SCANID_2);

		query.append(_FINDER_COLUMN_SCANIDREPORTTYPE_REPORTTYPE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ReportModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(scanId);

		qPos.add(reportType);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(report);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Report> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the reports where scanId = &#63; and reportType = &#63; from the database.
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 */
	@Override
	public void removeByScanIdReportType(long scanId, int reportType) {
		for (Report report : findByScanIdReportType(scanId, reportType,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(report);
		}
	}

	/**
	 * Returns the number of reports where scanId = &#63; and reportType = &#63;.
	 *
	 * @param scanId the scan ID
	 * @param reportType the report type
	 * @return the number of matching reports
	 */
	@Override
	public int countByScanIdReportType(long scanId, int reportType) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SCANIDREPORTTYPE;

		Object[] finderArgs = new Object[] { scanId, reportType };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_REPORT_WHERE);

			query.append(_FINDER_COLUMN_SCANIDREPORTTYPE_SCANID_2);

			query.append(_FINDER_COLUMN_SCANIDREPORTTYPE_REPORTTYPE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(scanId);

				qPos.add(reportType);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCANIDREPORTTYPE_SCANID_2 = "report.scanId = ? AND ";
	private static final String _FINDER_COLUMN_SCANIDREPORTTYPE_REPORTTYPE_2 = "report.reportType = ?";

	public ReportPersistenceImpl() {
		setModelClass(Report.class);
	}

	/**
	 * Caches the report in the entity cache if it is enabled.
	 *
	 * @param report the report
	 */
	@Override
	public void cacheResult(Report report) {
		entityCache.putResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportImpl.class, report.getPrimaryKey(), report);

		report.resetOriginalValues();
	}

	/**
	 * Caches the reports in the entity cache if it is enabled.
	 *
	 * @param reports the reports
	 */
	@Override
	public void cacheResult(List<Report> reports) {
		for (Report report : reports) {
			if (entityCache.getResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
						ReportImpl.class, report.getPrimaryKey()) == null) {
				cacheResult(report);
			}
			else {
				report.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all reports.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ReportImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the report.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Report report) {
		entityCache.removeResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportImpl.class, report.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Report> reports) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Report report : reports) {
			entityCache.removeResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
				ReportImpl.class, report.getPrimaryKey());
		}
	}

	/**
	 * Creates a new report with the primary key. Does not add the report to the database.
	 *
	 * @param reportId the primary key for the new report
	 * @return the new report
	 */
	@Override
	public Report create(long reportId) {
		Report report = new ReportImpl();

		report.setNew(true);
		report.setPrimaryKey(reportId);

		return report;
	}

	/**
	 * Removes the report with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param reportId the primary key of the report
	 * @return the report that was removed
	 * @throws NoSuchReportException if a report with the primary key could not be found
	 */
	@Override
	public Report remove(long reportId) throws NoSuchReportException {
		return remove((Serializable)reportId);
	}

	/**
	 * Removes the report with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the report
	 * @return the report that was removed
	 * @throws NoSuchReportException if a report with the primary key could not be found
	 */
	@Override
	public Report remove(Serializable primaryKey) throws NoSuchReportException {
		Session session = null;

		try {
			session = openSession();

			Report report = (Report)session.get(ReportImpl.class, primaryKey);

			if (report == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchReportException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(report);
		}
		catch (NoSuchReportException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Report removeImpl(Report report) {
		report = toUnwrappedModel(report);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(report)) {
				report = (Report)session.get(ReportImpl.class,
						report.getPrimaryKeyObj());
			}

			if (report != null) {
				session.delete(report);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (report != null) {
			clearCache(report);
		}

		return report;
	}

	@Override
	public Report updateImpl(Report report) {
		report = toUnwrappedModel(report);

		boolean isNew = report.isNew();

		ReportModelImpl reportModelImpl = (ReportModelImpl)report;

		Session session = null;

		try {
			session = openSession();

			if (report.isNew()) {
				session.save(report);

				report.setNew(false);
			}
			else {
				report = (Report)session.merge(report);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ReportModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((reportModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { reportModelImpl.getOriginalScanId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);

				args = new Object[] { reportModelImpl.getScanId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANID,
					args);
			}

			if ((reportModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTTYPE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						reportModelImpl.getOriginalReportType()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_REPORTTYPE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTTYPE,
					args);

				args = new Object[] { reportModelImpl.getReportType() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_REPORTTYPE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REPORTTYPE,
					args);
			}

			if ((reportModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDREPORTTYPE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						reportModelImpl.getOriginalScanId(),
						reportModelImpl.getOriginalReportType()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANIDREPORTTYPE,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDREPORTTYPE,
					args);

				args = new Object[] {
						reportModelImpl.getScanId(),
						reportModelImpl.getReportType()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_SCANIDREPORTTYPE,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SCANIDREPORTTYPE,
					args);
			}
		}

		entityCache.putResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
			ReportImpl.class, report.getPrimaryKey(), report, false);

		report.resetOriginalValues();

		return report;
	}

	protected Report toUnwrappedModel(Report report) {
		if (report instanceof ReportImpl) {
			return report;
		}

		ReportImpl reportImpl = new ReportImpl();

		reportImpl.setNew(report.isNew());
		reportImpl.setPrimaryKey(report.getPrimaryKey());

		reportImpl.setReportId(report.getReportId());
		reportImpl.setReportDate(report.getReportDate());
		reportImpl.setReportBucketName(report.getReportBucketName());
		reportImpl.setReportType(report.getReportType());
		reportImpl.setReportName(report.getReportName());
		reportImpl.setCxReportId(report.getCxReportId());
		reportImpl.setScanId(report.getScanId());

		return reportImpl;
	}

	/**
	 * Returns the report with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the report
	 * @return the report
	 * @throws NoSuchReportException if a report with the primary key could not be found
	 */
	@Override
	public Report findByPrimaryKey(Serializable primaryKey)
		throws NoSuchReportException {
		Report report = fetchByPrimaryKey(primaryKey);

		if (report == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchReportException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return report;
	}

	/**
	 * Returns the report with the primary key or throws a {@link NoSuchReportException} if it could not be found.
	 *
	 * @param reportId the primary key of the report
	 * @return the report
	 * @throws NoSuchReportException if a report with the primary key could not be found
	 */
	@Override
	public Report findByPrimaryKey(long reportId) throws NoSuchReportException {
		return findByPrimaryKey((Serializable)reportId);
	}

	/**
	 * Returns the report with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the report
	 * @return the report, or <code>null</code> if a report with the primary key could not be found
	 */
	@Override
	public Report fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
				ReportImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Report report = (Report)serializable;

		if (report == null) {
			Session session = null;

			try {
				session = openSession();

				report = (Report)session.get(ReportImpl.class, primaryKey);

				if (report != null) {
					cacheResult(report);
				}
				else {
					entityCache.putResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
						ReportImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
					ReportImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return report;
	}

	/**
	 * Returns the report with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param reportId the primary key of the report
	 * @return the report, or <code>null</code> if a report with the primary key could not be found
	 */
	@Override
	public Report fetchByPrimaryKey(long reportId) {
		return fetchByPrimaryKey((Serializable)reportId);
	}

	@Override
	public Map<Serializable, Report> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Report> map = new HashMap<Serializable, Report>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Report report = fetchByPrimaryKey(primaryKey);

			if (report != null) {
				map.put(primaryKey, report);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
					ReportImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Report)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_REPORT_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Report report : (List<Report>)q.list()) {
				map.put(report.getPrimaryKeyObj(), report);

				cacheResult(report);

				uncachedPrimaryKeys.remove(report.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ReportModelImpl.ENTITY_CACHE_ENABLED,
					ReportImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the reports.
	 *
	 * @return the reports
	 */
	@Override
	public List<Report> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the reports.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @return the range of reports
	 */
	@Override
	public List<Report> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the reports.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of reports
	 */
	@Override
	public List<Report> findAll(int start, int end,
		OrderByComparator<Report> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the reports.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of reports
	 * @param end the upper bound of the range of reports (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of reports
	 */
	@Override
	public List<Report> findAll(int start, int end,
		OrderByComparator<Report> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Report> list = null;

		if (retrieveFromCache) {
			list = (List<Report>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_REPORT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_REPORT;

				if (pagination) {
					sql = sql.concat(ReportModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Report>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Report>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the reports from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Report report : findAll()) {
			remove(report);
		}
	}

	/**
	 * Returns the number of reports.
	 *
	 * @return the number of reports
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_REPORT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ReportModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the report persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ReportImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_REPORT = "SELECT report FROM Report report";
	private static final String _SQL_SELECT_REPORT_WHERE_PKS_IN = "SELECT report FROM Report report WHERE reportId IN (";
	private static final String _SQL_SELECT_REPORT_WHERE = "SELECT report FROM Report report WHERE ";
	private static final String _SQL_COUNT_REPORT = "SELECT COUNT(report) FROM Report report";
	private static final String _SQL_COUNT_REPORT_WHERE = "SELECT COUNT(report) FROM Report report WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "report.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Report exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Report exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ReportPersistenceImpl.class);
}