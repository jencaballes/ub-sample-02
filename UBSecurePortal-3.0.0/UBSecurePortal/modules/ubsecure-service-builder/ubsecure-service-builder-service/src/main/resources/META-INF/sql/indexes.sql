create index IX_F7192CB3 on UBS_Project (caseNumber[$COLUMN_LENGTH:75$]);
create index IX_7D3768E6 on UBS_Project (ownerGroup);
create index IX_AB67342F on UBS_Project (type_);

create index IX_FBE12776 on UBS_ProjectUsers (projectId);
create index IX_7B7AE2C8 on UBS_ProjectUsers (userId[$COLUMN_LENGTH:75$]);

create index IX_377A5DD5 on UBS_Report (reportType);
create index IX_980262C1 on UBS_Report (scanId, reportType);

create index IX_8BF0BDE5 on UBS_Scan (cxRunId[$COLUMN_LENGTH:75$]);
create index IX_CEE77928 on UBS_Scan (projectId);
create index IX_FD326EE on UBS_Scan (scanId, status);
create index IX_E9942502 on UBS_Scan (status);

create index IX_3A3D12C4 on UBS_VexDetectionResult (scanid);

create index IX_D69B18C9 on UBS_VexLoginSetting (scanid);

create index IX_2C598B95 on UBS_VexScanSetting (scanid);

create index IX_587FDB5D on UBS_VexTargetInformation (scanid);