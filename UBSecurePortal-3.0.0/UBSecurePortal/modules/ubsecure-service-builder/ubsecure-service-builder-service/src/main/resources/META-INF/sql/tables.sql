create table UBS_Project (
	projectId LONG not null primary key,
	type_ INTEGER,
	ownerGroup LONG,
	cxAndroidProjectId VARCHAR(75) null,
	projectName VARCHAR(75) null,
	caseNumber VARCHAR(75) null,
	status INTEGER,
	attribute INTEGER,
	projectEndDate DATE null,
	projectCreateDate DATE null,
	checklistFileName VARCHAR(75) null,
	presetId LONG,
	caseName VARCHAR(75) null,
	targetUrl VARCHAR(75) null,
	productionEnvironmentUrl VARCHAR(75) null,
	selectedWebSignature VARCHAR(75) null,
	getServerFiles INTEGER,
	getServerSettings INTEGER,
	selectedServerFilesSignature VARCHAR(75) null,
	selectedServerSettingsSignature VARCHAR(75) null
);

create table UBS_ProjectUsers (
	userId VARCHAR(75) not null,
	projectId LONG not null,
	primary key (userId, projectId)
);

create table UBS_Report (
	reportId LONG not null primary key,
	reportDate DATE null,
	reportBucketName VARCHAR(75) null,
	reportType INTEGER,
	reportName VARCHAR(75) null,
	cxReportId LONG,
	scanId LONG
);

create table UBS_Scan (
	scanId LONG not null primary key,
	registrationDate DATE null,
	status INTEGER,
	process INTEGER,
	cxAndroidScanId VARCHAR(75) null,
	scanManager VARCHAR(75) null,
	infoCount LONG,
	highCount LONG,
	mediumCount LONG,
	lowCount LONG,
	fileName VARCHAR(75) null,
	hashValue VARCHAR(75) null,
	filePath VARCHAR(75) null,
	cxRunId VARCHAR(75) null,
	projectId LONG,
	reviewFlag INTEGER,
	failedScanCause VARCHAR(75) null,
	isDeletedInCx VARCHAR(75) null,
	modifiedDate DATE null,
	cxAndroidProjectId VARCHAR(75) null
);

alter table ubs_project alter column projectName type text;
alter table ubs_project alter column checklistFileName type text;
alter table ubs_projectusers alter column userId type text;
alter table ubs_scan alter column scanManager type text;
alter table ubs_scan alter column fileName type text;
alter table ubs_scan alter column hashValue type text;
alter table ubs_scan alter column filePath type text;
alter table contact_ alter column username type text;
alter table ubs_project add presetId LONG;
alter table ubs_scan add failedScanCause text;
alter table ubs_scan add isDeletedInCx text;
alter table ubs_scan add modifiedDate DATE;

create table UBS_VexDetectionResult (
	scanresultid LONG not null,
	scanid LONG not null,
	detectionresultid VARCHAR(75) null,
	risklevel INTEGER,
	category VARCHAR(75) null,
	overview VARCHAR(75) null,
	functionname VARCHAR(75) null,
	url VARCHAR(75) null,
	parametername VARCHAR(75) null,
	detectionjudgment VARCHAR(75) null,
	reviewcomment VARCHAR(75) null,
	isresendinvex INTEGER,
	listofdetectedresult VARCHAR(75) null,
	listofinspectiondatetime VARCHAR(75) null,
	primary key (scanresultid, scanid)
);

create table UBS_VexLoginSetting (
	scanid LONG not null primary key,
	loginurl VARCHAR(75) null,
	paramname VARCHAR(75) null,
	paramvalue VARCHAR(75) null
);

create table UBS_VexScanSetting (
	scanid LONG not null primary key,
	starturl VARCHAR(75) null,
	authorizedpatrolurl VARCHAR(75) null,
	unauthorizedpatrolurl VARCHAR(75) null,
	starttime DATE null,
	endtime DATE null,
	setemailnotification INTEGER,
	selectedwebsignature VARCHAR(75) null,
	getserverfiles INTEGER,
	getserversettings INTEGER,
	selectedserverfilessignature VARCHAR(75) null,
	selectedserversettingssignature VARCHAR(75) null,
	maxnumdetectionlink INTEGER,
	maxnumdetectionlinkperpage INTEGER,
	detectionlinkdepthlimit INTEGER,
	waittime INTEGER,
	numofthread INTEGER,
	timeouttime INTEGER,
	setautopostform INTEGER,
	setpostmethod INTEGER,
	requestheader VARCHAR(75) null,
	loginstatusdetection VARCHAR(75) null,
	setnotsendpassword INTEGER,
	paramnamesaspassword VARCHAR(75) null,
	numofretryaterror INTEGER,
	sessionerrordetection VARCHAR(75) null,
	screentransitionerrordetection VARCHAR(75) null,
	implementationenvironment INTEGER,
	setcrawlingonly INTEGER,
	excludepathinforegex VARCHAR(75) null,
	excludeparameterdeletenameregex VARCHAR(75) null,
	excludeparameterdeleteparamregex VARCHAR(75) null,
	extracturlregex VARCHAR(75) null,
	notparseextension VARCHAR(75) null,
	notparsefilenameregex VARCHAR(75) null
);

create table UBS_VexTargetInformation (
	scanid LONG not null primary key,
	protocol VARCHAR(75) null,
	host VARCHAR(75) null,
	port INTEGER,
	httpversion INTEGER,
	setkeepaliveconnection INTEGER,
	setresponsecontentlength INTEGER,
	useacceptencodingheader INTEGER,
	unzipresponse INTEGER,
	httpprotocol VARCHAR(75) null,
	externalproxyhost VARCHAR(75) null,
	externalproxyport INTEGER,
	externalproxyauthid VARCHAR(75) null,
	externalproxyauthpassword VARCHAR(75) null,
	useclientcertificate INTEGER,
	certificatefile VARCHAR(75) null,
	certificatefilepassword VARCHAR(75) null,
	ntlmauthid VARCHAR(75) null,
	ntlmauthpassword VARCHAR(75) null,
	ntlmauthdomain VARCHAR(75) null,
	ntlmauthhost VARCHAR(75) null,
	digestauthid VARCHAR(75) null,
	digestauthpassword VARCHAR(75) null,
	basicauthid VARCHAR(75) null,
	basicauthpassword VARCHAR(75) null,
	accessexclusionpath VARCHAR(75) null
);