<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List"%>

<%@ page import="javax.portlet.PortletSession"%>

<%@ page import="jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.ApiSignatureSetInfo" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Scan" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformationItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="com.liferay.portal.kernel.model.Organization" %>
<%@ page import="com.liferay.portal.kernel.model.User" %>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >

<portlet:defineObjects />

<!-- Vex -->

<%
//Previous and current screen
	PortletSession pSession = renderRequest.getPortletSession();
	Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
	String strCurrScreen = PortalConstants.STRING_EMPTY;
	Object oProjectId = pSession.getAttribute(PortalConstants.PARAM_PROJECT_ID);
	Object oScanId = pSession.getAttribute(PortalConstants.PARAM_SCAN_ID);
	
	long lProjectId = PortalConstants.LONG_ZERO;
	long lScanId = PortalConstants.LONG_ZERO;
	
	if (!CommonUtil.isObjectNull(oCurrScreen)) {
		strCurrScreen = oCurrScreen.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
			pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
		}
	}
	
	if (!CommonUtil.isObjectNull(oProjectId)) {
		lProjectId = Long.parseLong(oProjectId.toString());
	}
	
	if (!CommonUtil.isObjectNull(oScanId)) {
		lScanId = Long.parseLong(oScanId.toString());
	}
	
	pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION);
	// End
	
	// Show error indicators
	boolean bShowDBConnError = false;
	
	// Get project details
	Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);
	// End

	String projectName = PortalConstants.STRING_EMPTY;
	int status = PortalConstants.INT_ZERO;
	
	if (!CommonUtil.isObjectNull(project)) {
		projectName = project.getProjectName();
		status = project.getStatus();
		lProjectId = project.getProjectId();
	}

	String portletNamespace = null;
	int iUserAction = PortalConstants.INT_ZERO;
	portletNamespace = renderResponse.getNamespace();
	
/* 	List<ApiSignatureSetInfo> webInspectionSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST);
	List<ApiSignatureSetInfo> serverFilesSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST);
	List<ApiSignatureSetInfo> serverSettingsSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST);
 */
/* 	Scan scan = (Scan) renderRequest.getAttribute(PortalConstants.PARAM_SCAN);
	VexScanSetting scanSetting = (VexScanSetting) renderRequest.getAttribute(PortalConstants.PARAM_SCAN_SETTING);
	List<VexLoginSettingItem> loginSettingList = (List<VexLoginSettingItem>) renderRequest.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
	 */
	String startTimeFormat = PortalConstants.STRING_EMPTY;
	String endTimeFormat = PortalConstants.STRING_EMPTY;
	
	DateFormat formatter = new SimpleDateFormat(PortalConstants.TIME_FORMAT_HH_MM);
	
	/* if (null != scanSetting) {
		if (null != scanSetting.getStarttime() && null != scanSetting.getEndtime()) {
			startTimeFormat = formatter.format(scanSetting.getStarttime());
			endTimeFormat = formatter.format(scanSetting.getEndtime());
		}
	} */
	
	String paramStartTime = renderRequest.getParameter(PortalConstants.PARAM_START_TIME);
	String paramEndTime = renderRequest.getParameter(PortalConstants.PARAM_END_TIME);
	String paramStartURL = (String) renderRequest.getAttribute(PortalConstants.PARAM_START_URL);
	String paramAuthorizedPatrolURL = (String) renderRequest.getAttribute(PortalConstants.PARAM_AUTHORIZED_PATROL_URL);
	
	if (null == paramStartTime) {
		paramStartTime = PortalConstants.STRING_EMPTY;
	}
	
	if (null == paramEndTime) {
		paramEndTime = PortalConstants.STRING_EMPTY;
	}
	
	if (null == paramStartURL) {
		paramStartURL = PortalConstants.STRING_EMPTY;
	}
	
	if (null == paramAuthorizedPatrolURL) {
		paramAuthorizedPatrolURL = PortalConstants.STRING_EMPTY;
	}
	
	String strScanName = PortalConstants.STRING_EMPTY;
	//Object oImplementationEnvironment = renderRequest.getAttribute(PortalConstants.PARAM_IMPLEMENTATION_ENVIRONMENT);

	/* if(!CommonUtil.isObjectNull(scan)){
		strScanName = scan.getFileName();
	}else{
		strScanName = renderRequest.getAttribute(PortalConstants.PARAM_SCAN_NAME).toString();
	} */
	
	List<VexTargetInformationItem> targetInformationList = (List<VexTargetInformationItem>)pSession.getAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
		
	//String strProtocol = (String) renderRequest.getAttribute(PortalConstants.PARAM_TARGET_INFO_PROTOCOL);
	
	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	httpSession.setAttribute("Current_screen", "VEX");
	int iUserRole = 0;
	long lUserId = 0L;
	boolean bUseVEX = false;
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	
	if (!CommonUtil.isObjectNull(oUserId)) {
		lUserId = Long.parseLong(oUserId.toString());
	}
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
		bUseVEX = true;
	} else {
		Object oUseVEX = httpSession.getAttribute(PortalConstants.USE_VEX);
		
		if (!CommonUtil.isObjectNull(oUseVEX)) {
			bUseVEX = Boolean.parseBoolean(oUseVEX.toString());
		}
	}
	
	Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
		strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
	}

	Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
	String strCxServerErrorMsg = PortalConstants.STRING_EMPTY;
	
	if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
		strCxServerErrorMsg = oCxServerErrorMsg.toString();
	}
	
	pSession.removeAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	
	if (bUseVEX && (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger() || iUserRole == UserRole.GEN_USER.getInteger())) {
		boolean bErrorFromCxSuite = SessionErrors.contains(renderRequest, PortalMessages.VEX_REGISTER_SCAN_FAILED);
%>

<portlet:actionURL name="viewScanList" var="cancelURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<portlet:param name="type" value="<%= String.valueOf(ProjectType.VEX.getInteger()) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanAdvancedSetting" var="viewRegisterScanAdvancedSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<portlet:param name="<%=PortalConstants.PARAM_SCAN_NAME %>" value="<%= String.valueOf(strScanName) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanSimpleSetting" var="viewRegisterScanSimpleSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="registerScanTargetInformation" var="registerScanTargetInformationURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<portlet:param name="scanId" value="<%= String.valueOf(lScanId) %>" />
</portlet:actionURL>

<div style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px">
	<liferay-ui:message key="header-project-registration" />
</div>

<div style="height: 14px;">
	<hr style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>


<style>
.targetInfo_hideBtn{
	width: 57% !important;
    margin-top: 0px !important;
    margin-right: 12px !important;
}

.targetInfo_deleteBtn{
	width: 35% !important;
}

.targetInfo_hide{
   /* position: absolute !important;
   top: -9999px !important;
   left: -9999px !important; */
   visibility: collapse;
}
</style>
<div style="width: 100%; position: relative;">
	<label id="button-add" style="display:none"><liferay-ui:message key="button-add" /></label>
	<label id="button-delete" style="display:none"><liferay-ui:message key="button-delete" /></label>
	<label id="button-hide-advanced-setting" style="display:none"><liferay-ui:message key="button-hide-advanced-setting"/></label>
	<label id="label-websocket-keep-alive-text" style="display:none"><liferay-ui:message key="label-websocket-keep-alive-text"/></label>
	<label id="label-http-version-colon" style="display:none"><liferay-ui:message key="label-http-version-colon" /></label>
	
	<label id="label-keep-alive-connection-colon" style="display:none"><liferay-ui:message key="label-keep-alive-connection-colon" /></label>
	<label id="label-use" style="display:none"><liferay-ui:message key="label-use" /></label>
	<label id="label-do-not-use" style="display:none"><liferay-ui:message key="label-do-not-use" /></label>
	
	<label id="label-response-content-length-colon" style="display:none"><liferay-ui:message key="label-response-content-length-colon"/></label>
	<label id="label-ignore" style="display:none"><liferay-ui:message key="label-ignore" /></label>
	<label id="label-do-not-ignore" style="display:none"><liferay-ui:message key="label-do-not-ignore" /></label>
	
	<label id="label-accept-encoding-header-colon" style="display:none"><liferay-ui:message key="label-accept-encoding-header-colon" /></label>
	<label id="label-delete-encoding" style="display:none"><liferay-ui:message key="label-delete-encoding"/></label>
	<label id="label-do-not-delete-encoding" style="display:none"><liferay-ui:message key="label-do-not-delete-encoding"/></label>
	
	<label id="label-response-unzip-colon" style="display:none"><liferay-ui:message key="label-response-unzip-colon" /></label>
	<label id="label-do-response" style="display:none"><liferay-ui:message key="label-do-response" /></label>
	<label id="label-do-not-do-response" style="display:none"><liferay-ui:message key="label-do-not-do-response" /></label>
	
	<label id="label-http-protocol-colon" style="display:none"><liferay-ui:message key="label-http-protocol-colon" /></label>
	<label id="label-automatic" style="display:none"><liferay-ui:message key="label-automatic" /></label>
	
	<label id="label-external-proxy-colon" style="display:none"><liferay-ui:message key="label-external-proxy-colon"/></label>
	<label id="label-host-colon" style="display:none"><liferay-ui:message key="label-host-colon"/></label>
	<label id="label-port-colon" style="display:none"><liferay-ui:message key="label-port-colon"/></label>
	<label id="label-authentication-ID" style="display:none"><liferay-ui:message key="label-authentication-ID"/></label>
	<label id="label-authentication-password" style="display:none"><liferay-ui:message key="label-authentication-password"/></label>
	
	<label id="label-client-certificate-colon" style="display:none"><liferay-ui:message key="label-client-certificate-colon"/></label>
	<label id="label-reference-colon" style="display:none"><liferay-ui:message key="label-reference-colon"/></label>
	<label id="label-file-not-selected-text" style="display:none"><liferay-ui:message key="label-file-not-selected-text"/></label>
	<label id="label-passphrase-colon" style="display:none"><liferay-ui:message key="label-passphrase-colon"/></label>
	<label id="label-certificate-colon" style="display:none"><liferay-ui:message key="label-certificate-colon"/></label>
	
	<label id="label-ntlm-authentication-colon" style="display:none"><liferay-ui:message key="label-ntlm-authentication-colon"/></label>
	<label id="label-username-colon" style="display:none"><liferay-ui:message key='label-username-colon'/></label>
	<label id="label-password-colon" style="display:none"><liferay-ui:message key='label-password-colon'/></label>
	<label id="label-domain-colon" style="display:none"><liferay-ui:message key='label-domain-colon'/></label>
	<label id="label-hostname-colon" style="display:none"><liferay-ui:message key='label-hostname-colon'/></label>
	
	<label id="label-basic-authentication-colon" style="display:none"><liferay-ui:message key='label-basic-authentication-colon'/></label>
	<label id="label-automatic-patrol-text" style="display:none"><liferay-ui:message key='label-automatic-patrol-text'/></label>
	
	<label id="label-digest-authentication-colon" style="display:none"><liferay-ui:message key='label-digest-authentication-colon'/></label>
	<label id="label-access-execution-path-colon" style="display:none"><liferay-ui:message key="label-access-execution-path-colon"/></label>
	<label id="label-access-execution-placeholder" style="display:none"><liferay-ui:message key="label-access-execution-placeholder"/></label>
	
	<label id="button-hide-advanced-setting" style="display:none"><liferay-ui:message key="button-hide-advanced-setting"/></label>
	<label id="button-open-advanced-setting" style="display:none"><liferay-ui:message key="button-open-advanced-setting"/></label>
	
	<div class="tabbable-content instantiated" id="portlet-set-properties">
		<ul class="lfr-nav nav nav-tabs nav-tabs-default " role="tablist">
			<li class="tab yui3-widget" role="presentation"><a href="<%= viewRegisterScanSimpleSettingURL %>" role="tab" tabindex="0"><liferay-ui:message key="button-simple-setting" /></a></li>
			<li class="tab yui3-widget tab-focused active tab-selected" role="presentation"><a href="#" role="tab" tabindex="-1"><liferay-ui:message key="button-advanced-setting" /></a></li>
		</ul>
	</div>
	
	<div id="userIdError" class="alert alert-danger hideError">
		<liferay-ui:message key="error-search-user-id-too-long" />
	</div>
	
	<div id="userNameError" class="alert alert-danger hideError">
		<liferay-ui:message key="error-search-username-too-long" />
	</div>
	
	<div id="userDoesNotExistError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	</div>
	
	<div id="userDoesNotBelongToGroupError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP %>" />
	</div>
	
	<div id="dbConnError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.ORM_EXCEPTION %>" />
	</div>
	
	<div id="userDoesNotExistError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	</div>
	
	<div id="dbConnError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.ORM_EXCEPTION %>" />
	</div>
	<div>
		<%
		if (bErrorFromCxSuite) {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.VEX_UPDATE_PROJECT_FAILED %>" /><liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
			</div>
			<%
		}
		%>
		
		<liferay-ui:success key="<%= PortalConstants.REGISTER_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.REGISTER_SCAN_SUCCESSFUL %>" />
	
		<liferay-ui:error key="<%= PortalMessages.PROJECT_ID_INVALID %>" message="<%= PortalMessages.PROJECT_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" message="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" />
		
	    <liferay-ui:error key="<%= PortalMessages.ADD_SCAN_FAILED %>" message="<%= PortalMessages.ADD_SCAN_FAILED %>" />
		
		<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
		
		<liferay-ui:error key="<%= PortalErrors.CX_SERVER_ERROR %>" message="<%= strCxServerErrorMsg %>" />
		<liferay-ui:error key="<%= PortalMessages.CX_SESSION_ID_INVALID %>" message="<%= PortalMessages.CX_SESSION_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.VEX_HOST_INVALID %>" message="<%= PortalMessages.VEX_HOST_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PRESET_ID_INVALID %>" message="<%= PortalMessages.PRESET_ID_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.SCAN_NAME_INVALID %>" message="<%= PortalMessages.SCAN_NAME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_SCAN_NAME %>" message="<%= PortalMessages.NO_SCAN_NAME %>" />
		<liferay-ui:error key="<%= PortalMessages.START_TIME_AND_END_TIME_INVALID %>" message="<%= PortalMessages.START_TIME_AND_END_TIME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.START_TIME_INVALID %>" message="<%= PortalMessages.START_TIME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.END_TIME_INVALID %>" message="<%= PortalMessages.END_TIME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.START_URL_INVALID %>" message="<%= PortalMessages.START_URL_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.AUTHORIZED_PATROL_URL_INVALID %>" message="<%= PortalMessages.AUTHORIZED_PATROL_URL_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.LOGIN_URL_INVALID %>" message="<%= PortalMessages.LOGIN_URL_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PARAMETER_NAME_ID_INVALID %>" message="<%= PortalMessages.PARAMETER_NAME_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PARAMETER_NAME_PASS_INVALID %>" message="<%= PortalMessages.PARAMETER_NAME_PASS_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_START_URL %>" message="<%= PortalMessages.NO_START_URL %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_AUTHORIZED_PATROL_URL %>" message="<%= PortalMessages.NO_AUTHORIZED_PATROL_URL %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_LOGIN_URL %>" message="<%= PortalMessages.NO_LOGIN_URL %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_PARAMETER_NAME_ID %>" message="<%= PortalMessages.NO_PARAMETER_NAME_ID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_PARAMETER_NAME_PASS %>" message="<%= PortalMessages.NO_PARAMETER_NAME_PASS %>" />
		<liferay-ui:error key="<%= PortalMessages.PROTOCOL_INVALID %>" message="<%= PortalMessages.PROTOCOL_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.HOST_INVALID %>" message="<%= PortalMessages.HOST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.HOST_LOCALHOST_INVALID %>" message="<%= PortalMessages.HOST_LOCALHOST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PORT_INVALID %>" message="<%= PortalMessages.PORT_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_HOST %>" message="<%= PortalMessages.NO_HOST %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_PORT %>" message="<%= PortalMessages.NO_PORT %>" />
		<liferay-ui:error key="<%= PortalMessages.TARGET_INFORMATION_LIST_INVALID %>" message="<%= PortalMessages.TARGET_INFORMATION_LIST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_TARGET_INFORMATION_LIST %>" message="<%= PortalMessages.NO_TARGET_INFORMATION_LIST %>" />
		<liferay-ui:error key="<%= PortalMessages.ADD_SCAN_FAILED %>" message="<%= PortalMessages.ADD_SCAN_FAILED %>" />
	
		<liferay-ui:error key="<%= PortalMessages.FILE_NOT_FOUND_EXCEPTION %>" message="<%= PortalMessages.FILE_NOT_FOUND_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.IO_EXCEPTION %>" message="<%= PortalMessages.IO_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.FILE_INVALID %>" message="<%= PortalMessages.FILE_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.NO_EXTERNAL_PROXY_HOST %>" message="<%= PortalMessages.NO_EXTERNAL_PROXY_HOST %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_EXTERNAL_PROXY_PORT %>" message="<%= PortalMessages.NO_EXTERNAL_PROXY_PORT %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_EXTERNAL_PROXY_AUTH_ID %>" message="<%= PortalMessages.NO_EXTERNAL_PROXY_AUTH_ID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_EXTERNAL_PROXY_AUTH_PASSWORD %>" message="<%= PortalMessages.NO_EXTERNAL_PROXY_AUTH_PASSWORD %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_USE_CLIENT_CERTIFICATE %>" message="<%= PortalMessages.NO_USE_CLIENT_CERTIFICATE %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_CERTIFICATE_FILE %>" message="<%= PortalMessages.NO_CERTIFICATE_FILE %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_CERTIFICATE_PASSWORD %>" message="<%= PortalMessages.NO_CERTIFICATE_PASSWORD %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_NTLM_AUTH_ID %>" message="<%= PortalMessages.NO_NTLM_AUTH_ID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_NTLM_AUTH_PASSWORD %>" message="<%= PortalMessages.NO_NTLM_AUTH_PASSWORD %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_NTLM_AUTH_DOMAIN %>" message="<%= PortalMessages.NO_NTLM_AUTH_DOMAIN %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_NTLM_AUTH_HOST %>" message="<%= PortalMessages.NO_NTLM_AUTH_HOST %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_DIGEST_AUTH_ID %>" message="<%= PortalMessages.NO_DIGEST_AUTH_ID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_DIGEST_AUTH_PASSWORD %>" message="<%= PortalMessages.NO_DIGEST_AUTH_PASSWORD %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_BASIC_AUTH_ID %>" message="<%= PortalMessages.NO_BASIC_AUTH_ID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_BASIC_AUTH_PASSWORD %>" message="<%= PortalMessages.NO_BASIC_AUTH_PASSWORD %>" />
		
		<liferay-ui:error key="<%= PortalMessages.EXTERNAL_PROXY_HOST_INVALID %>" message="<%= PortalMessages.EXTERNAL_PROXY_HOST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.EXTERNAL_PROXY_HOST_LOCALHOST_INVALID %>" message="<%= PortalMessages.EXTERNAL_PROXY_HOST_LOCALHOST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.EXTERNAL_PROXY_PORT_INVALID %>" message="<%= PortalMessages.EXTERNAL_PROXY_PORT_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.EXTERNAL_PROXY_AUTH_ID_INVALID %>" message="<%= PortalMessages.EXTERNAL_PROXY_AUTH_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.EXTERNAL_PROXY_AUTH_PASSWORD_INVALID %>" message="<%= PortalMessages.EXTERNAL_PROXY_AUTH_PASSWORD_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.USE_CLIENT_CERTIFICATE_INVALID %>" message="<%= PortalMessages.USE_CLIENT_CERTIFICATE_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.CERTIFICATE_FILE_INVALID %>" message="<%= PortalMessages.CERTIFICATE_FILE_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.CERTIFICATE_PASSWORD_INVALID %>" message="<%= PortalMessages.CERTIFICATE_PASSWORD_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NTLM_AUTH_ID_INVALID %>" message="<%= PortalMessages.NTLM_AUTH_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NTLM_AUTH_PASSWORD_INVALID %>" message="<%= PortalMessages.NTLM_AUTH_PASSWORD_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NTLM_AUTH_DOMAIN_INVALID %>" message="<%= PortalMessages.NTLM_AUTH_DOMAIN_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NTLM_AUTH_DOMAIN_LOCALHOST_INVALID %>" message="<%= PortalMessages.NTLM_AUTH_DOMAIN_LOCALHOST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NTLM_AUTH_HOST_INVALID %>" message="<%= PortalMessages.NTLM_AUTH_HOST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NTLM_AUTH_HOST_LOCALHOST_INVALID %>" message="<%= PortalMessages.NTLM_AUTH_HOST_LOCALHOST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.DIGEST_AUTH_ID_INVALID %>" message="<%= PortalMessages.DIGEST_AUTH_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.DIGEST_AUTH_PASSWORD_INVALID %>" message="<%= PortalMessages.DIGEST_AUTH_PASSWORD_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.BASIC_AUTH_ID_INVALID %>" message="<%= PortalMessages.BASIC_AUTH_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.BASIC_AUTH_PASSWORD_INVALID %>" message="<%= PortalMessages.BASIC_AUTH_PASSWORD_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.ACCESS_EXCLUSION_PATH_INVALID %>" message="<%= PortalMessages.ACCESS_EXCLUSION_PATH_INVALID %>" />
	</div>

	<form action="<%= registerScanTargetInformationURL %>" method="post" id="fm" class="edit-project-form" accept-charset="UTF-8" enctype="multipart/form-data">
		<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; ">
			<input type="hidden" id="portletNamespace" value="<%=portletNamespace %>"/>
			<input type="hidden" id="<%=PortalConstants.PARAM_SCAN_NAME %>" value="<%=String.valueOf(strScanName) %>"/>
			<tbody>
				<tr>
					<td>
					</td>
					<td style="float:right">
						<a onclick="addRow('targetInfoTable')" class="btn btn-default" style="width:100%;margin-top: 0 !important;" id="button-add"><span class='lfr-btn-label'>
							<liferay-ui:message key="button-add" />
						</span></a>		
					</td>
				</tr>
				
				<tr class="input-row">
					<td colspan="2">
						<% 
							String numOfRowIndexes = PortalConstants.STRING_EMPTY;
									if (!CommonUtil.isListNullOrEmpty(targetInformationList) && targetInformationList.size() > 0) {
										for (int i = 0; i < targetInformationList.size(); i++) {
											numOfRowIndexes = numOfRowIndexes.concat(i + ",");
										}
									}
						%>
						<input type="hidden" id="<%= portletNamespace %>rowIndexes" name="<%= portletNamespace %>rowIndexes" value="<%= ((null != targetInformationList && targetInformationList.size() > 0) ? numOfRowIndexes : 0) %>"/>
						<input type="hidden" id="projectId" name="projectId" value="<%= lProjectId %>" />
						<input type="hidden" id="loggedInUser" name="loggedInUser" value="<%= lUserId %>" />
						<input type="hidden" id="<%= portletNamespace %>currentListTableSize" name="<%= portletNamespace %>currentListTableSize" value="<%= ((null != targetInformationList && targetInformationList.size() > 0) ? targetInformationList.size() : 1) %>" />
						<input type="hidden" id="<%= portletNamespace %>listTableSize" name="<%= portletNamespace %>listTableSize" value="<%= ((null != targetInformationList && targetInformationList.size() > 0) ? targetInformationList.size() : 1) %>" />
						
						<table class="table table-bordered table-hover table-striped" id="targetInfoTable" width="350px" border="1">
							<thead class="table-columns"> 
								<tr>
									<th><liferay-ui:message key="label-protocol" /><span style="color: red;">*</span></th>
									<th><liferay-ui:message key="label-host" /><span style="color: red;">*</span></th>
									<th><liferay-ui:message key="label-port" /><span style="color: red;">*</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody class="table-data">
								<% 
									if (!CommonUtil.isListNullOrEmpty(targetInformationList) && targetInformationList.size() > 0) {
										for (int i = 0; i < targetInformationList.size(); i++) {
											VexTargetInformationItem item = targetInformationList.get(i);
								%>
										<tr>
											<td>
												<input type="hidden" name="rowIndex" value="<%= i %>"/>
												<select class="field form-control" style="width: auto !important" label="" name="<%= portletNamespace %>protocolGroup<%= i %>" >
												<% if (item.getProtocol().equals("http://")) { %>
													<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">http://</option>
													<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">https://</option>
												<% } else if (item.getProtocol().equals("https://")) { %>
													<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">http://</option>
													<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">https://</option>
												<% } else { %>
													<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">http://</option>
													<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">https://</option>
												<% } %>
												</select>
											</td>
											<td><input type="text" name="<%= portletNamespace %>host<%= i %>" class="field form-control" style="width:100%;" placeholder="example.com" value="<%= HtmlUtil.escape(item.getHost()) %>" /></td>
											<td>
											<%
												String getPort = "";
												if(item!=null && !CommonUtil.isStringNullOrEmpty(item.getPort())){
													getPort=String.valueOf(item.getPort());
												}
											%>
											<input type="text" name="<%= portletNamespace %>port<%= i %>" class="field form-control" 
												style="width:80px;" placeholder="80" value="<%= HtmlUtil.escape(getPort) %>" />
											</td>
											<td>
												<button class="btn user-filter-btn targetInfo_hideBtn" style="width:100%; margin-top: 0 !important;" id="hideSettingBtn<%= i %>" name="hideSettingBtn<%= i %>" onclick="hideSetting('hideSettingBtn<%= i %>','table_<%=i%>_<%=i%>')" type="button">
											    <liferay-ui:message key="button-hide-advanced-setting"/></button>
											    
											    <a name="<%= portletNamespace %>btnDelete<%= i %>" id="<%= portletNamespace %>btnDelete<%= i %>" 
												onclick="deleteRow(this)" class="btn btn-default targetInfo_deleteBtn" style="width:100%;margin-top:0px;"><span class='lfr-btn-label'><liferay-ui:message key="button-delete" /></span></a>
											</td>
										</tr>	
										<tr>
											<td colspan="4">
												<table style="width: 100%;max-width: 100%;" id="table_<%=i%>_<%=i%>">
													<tbody>
														<tr>
															<td colspan="2"><span style="color: red;"><liferay-ui:message key="label-websocket-keep-alive-text"/></span></td>
														</tr>
														<tr>
															<td><div style="margin-right:90px;"><span><liferay-ui:message key="label-http-version-colon"/></span></div></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>httpversion<%= i %>" value="1" id="httpversion_1" 
																	<% if(item == null || item.getHttpversion() == 1){%> checked <%} %>>1.0
																<input type="radio" name="<%= portletNamespace %>httpversion<%= i %>" value="2" id="httpversion_2" 
																	<% if( item != null && item.getHttpversion() == 2){%> checked <%} %> style="margin-left:40px">1.1
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-keep-alive-connection-colon"/></span></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>setkeepaliveconnection<%= i %>" value="1" id="setkeepaliveconnection_1" 
																	<% if(item == null || item.getSetkeepaliveconnection() == 1){%> checked <%} %>><span><liferay-ui:message key="label-use"/></span>
																<input type="radio" name="<%= portletNamespace %>setkeepaliveconnection<%= i %>" value="0" id="setkeepaliveconnection_0" 
																	style="margin-left:40px" <% if( item != null && item.getSetkeepaliveconnection() == 0){%> checked <%} %>><span><liferay-ui:message key="label-do-not-use"/></span>
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-response-content-length-colon"/></span></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>setresponsecontentlength<%= i %>" value="1" id="setresponsecontentlength_1" 
																	<% if(item == null || item.getSetresponsecontentlength() == 1){%> checked <%} %>><span><liferay-ui:message key="label-ignore"/></span>
																<input type="radio" name="<%= portletNamespace %>setresponsecontentlength<%= i %>" value="0" id="setresponsecontentlength_0" 
																	style="margin-left:40px" <% if( item != null && item.getSetresponsecontentlength() == 0){%> checked <%} %>><span><liferay-ui:message key="label-do-not-ignore"/></span>
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-accept-encoding-header-colon"/></span></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>useacceptencodingheader<%= i %>" value="1" id="useacceptencodingheader_1" 
																	<% if(item == null || item.getUseacceptencodingheader() == 1){%> checked <%} %>><span><liferay-ui:message key="label-delete-encoding"/></span>
																<input type="radio" name="<%= portletNamespace %>useacceptencodingheader<%= i %>" value="0" id="useacceptencodingheader_0" 
																	style="margin-left:40px" <% if( item != null && item.getUseacceptencodingheader() == 0){%> checked <%} %>><span><liferay-ui:message key="label-do-not-delete-encoding"/></span>
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-response-unzip-colon"/></span></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>unzipresponse<%= i %>" value="1" id="unzipresponse_1" 
																	<% if(item == null || item.getUnzipresponse() == 1){%> checked <%} %>><span><liferay-ui:message key="label-do-response"/></span>
																<input type="radio" name="<%= portletNamespace %>unzipresponse<%= i %>" value="0" id="unzipresponse_0" 
																	style="margin-left:40px" <% if( item != null && item.getUnzipresponse() == 0){%> checked <%} %>><span><liferay-ui:message key="label-do-not-do-response"/></span>
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-http-protocol-colon"/></span></td>
															<td>
																<select name="<%= portletNamespace %>httpprotocol<%= i %>" style="width: auto !important;">

																<% if (!CommonUtil.isStringNullOrEmpty(item.getHttpprotocol())) {
																		if (item.getHttpprotocol().equals("auto")) { %>
																		<option value="auto" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true"><liferay-ui:message key="label-automatic"/></option>
																	<%} else{%>
																		<option value="auto" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true"><liferay-ui:message key="label-automatic"/></option>
																	<%} } else { %>
																		<option value="auto" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true"><liferay-ui:message key="label-automatic"/></option>
                                                                  <%  } %>
																</select>
															</td>
														</tr>
														
														<!-- External proxy -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-external-proxy-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-host-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getExternalproxyhost="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getExternalproxyhost()) && item !=null){
																						getExternalproxyhost=item.getExternalproxyhost();
																				}%>
																				<input type="text" name="<%= portletNamespace %>externalproxyhost<%= i %>" class="field form-control" style="width:300px;" placeholder="example.com" 
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getExternalproxyhost())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getExternalproxyhost)) %>" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-port-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getExternalproxyport="";
																					if(item.getExternalproxyport() != 0 && item !=null){
																						getExternalproxyport=String.valueOf(item.getExternalproxyport());
																				}%>
																				<input type="text" name="<%= portletNamespace %>externalproxyport<%= i %>" class="field form-control" style="width:100px;" placeholder="80"  
																					value="<%= ((CommonUtil.isStringNullOrEmpty(getExternalproxyport)) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getExternalproxyport)) %>"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-authentication-ID"/></span>
																			</td>
																			<td>
																				<% 
																					String getExternalproxyauthid="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getExternalproxyauthid()) && item !=null){
																						getExternalproxyauthid=item.getExternalproxyauthid();
																				}%>
																				<input type="text" name="<%= portletNamespace %>externalproxyauthid<%= i %>" class="field form-control" placeholder="id" 
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getExternalproxyauthid())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getExternalproxyauthid)) %>"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-authentication-password"/></span>
																			</td>
																			<td>
																				<% 
																					String getExternalproxyauthpassword="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getExternalproxyauthpassword()) && item !=null){
																						getExternalproxyauthpassword=item.getExternalproxyauthpassword();
																				}%>
																				<input type="text" name="<%= portletNamespace %>externalproxyauthpassword<%= i %>" class="field form-control" placeholder="password"  
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getExternalproxyauthpassword())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getExternalproxyauthpassword)) %>"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
														<!-- Client Certification -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-client-certificate-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td colspan="2">
																				<input type="checkbox" id="checkbox_<%= i %>" onchange="toggleCheckbox(this,'<%= portletNamespace %>useclientcertificate<%= i %>')" 
																				<% if( item != null && item.getUseclientcertificate() == 1){%> checked="true" <%} %>/><liferay-ui:message key="label-use"/>
																				<input type="hidden" id="<%= portletNamespace %>useclientcertificate<%= i %>" name="<%= portletNamespace %>useclientcertificate<%= i %>" 
																				value="<%=item.getUseclientcertificate()%>"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-certificate-colon"/></span>
																			</td>
																			<td>
																				<button class="btn user-filter-btn" style="width:100px;" id="fileBtn<%= i %>" name="" onclick="clickFile('<%= portletNamespace %>certificatefile<%= i %>')" type="button">
																					<liferay-ui:message key="label-reference-colon"/>
																				</button>&nbsp;&nbsp;<span id="strtextfile<%= i %>">
																				<%  
																				if(item == null || CommonUtil.isStringNullOrEmpty(item.getCertificatefile())) {%>
																					<liferay-ui:message key="label-file-not-selected-text"/>
																				<%}else{
																					String certificateFileName = item.getCertificatefile().replace("\\", "/");
																					int certificateIndex = certificateFileName.lastIndexOf("/");
																					certificateFileName= certificateFileName.substring(certificateIndex + 1);
																				%>
																				<span><%=certificateFileName%></span>
																				<%}%>
																				</span>
																			</td>
																			<td>
																				<div class="hideInput" style="display:none;"> <div class="form-group input-text-wrapper"> 
																				<input class="field form-control" id="<%= portletNamespace %>certificatefile<%= i %>" name="<%= portletNamespace %>certificatefile<%= i %>" 
																				type="file" value="<%=item.getCertificatefile()%>" accept="<%=PortalConstants.CONTENT_TYPE_PKCS12 %>" onchange="sendValue('<%= portletNamespace %>certificatefile<%= i %>','strtextfile<%= i %>')"> </div> </div>
																				<script type="text/javascript">
																				<%-- sendValue("<%= portletNamespace %>certificatefile<%= i %>","strtextfile<%= i %>"); --%>
																				</script>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-passphrase-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getCertificatefilepassword="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getCertificatefilepassword()) && item !=null){
																						getCertificatefilepassword=item.getCertificatefilepassword();
																				}%>
																				<input type="text" name="<%= portletNamespace %>certificatefilepassword<%= i %>" class="field form-control" 
																					placeholder="password" value="<%= ((CommonUtil.isStringNullOrEmpty(item.getCertificatefilepassword())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getCertificatefilepassword)) %>"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															  </td>
														 </tr>
														 
														 <!-- NTLM Authorization -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-ntlm-authentication-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-username-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getNtlmauthid="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getNtlmauthid()) && item !=null){
																						getNtlmauthid=item.getNtlmauthid();
																				}%>
																				<input type="text" name="<%= portletNamespace %>ntlmauthid<%= i %>" class="field form-control" placeholder="id" 
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getNtlmauthid())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getNtlmauthid)) %>" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-password-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getNtlmauthpassword="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getNtlmauthpassword()) && item !=null){
																						getNtlmauthpassword=item.getNtlmauthpassword();
																				}%>
																				<input type="text" name="<%= portletNamespace %>ntlmauthpassword<%= i %>" class="field form-control" placeholder="password"  
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getNtlmauthpassword())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getNtlmauthpassword)) %>"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-domain-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getNtlmauthdomain="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getNtlmauthdomain()) && item !=null){
																						getNtlmauthdomain=item.getNtlmauthdomain();
																				}%>
																				<input type="text" name="<%= portletNamespace %>ntlmauthdomain<%= i %>" class="field form-control" placeholder="domain" 
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getNtlmauthdomain())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getNtlmauthdomain)) %>"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-hostname-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getNtlmauthhost="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getNtlmauthhost()) && item !=null){
																						getNtlmauthhost=item.getNtlmauthhost();
																				}%>
																				<input type="text" name="<%= portletNamespace %>ntlmauthhost<%= i %>" class="field form-control" placeholder="host"  
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getNtlmauthhost())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getNtlmauthhost)) %>"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
														<!-- Digest Authorization -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-digest-authentication-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-username-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getDigestauthid="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getDigestauthid()) && item !=null){
																						getDigestauthid=item.getDigestauthid();
																				}%>
																				<input type="text" name="<%= portletNamespace %>digestauthid<%= i %>" class="field form-control"  placeholder="id" 
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getDigestauthid())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getDigestauthid)) %>" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-password-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getDigestauthpassword="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getDigestauthpassword()) && item !=null){
																						getDigestauthpassword=item.getDigestauthpassword();
																				}%>
																				<input type="text" name="<%= portletNamespace %>digestauthpassword<%= i %>" class="field form-control"  placeholder="password"  
																					value="<%= ((CommonUtil.isStringNullOrEmpty(item.getDigestauthpassword())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getDigestauthpassword)) %>"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
														<!-- Basic Authorization -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-basic-authentication-colon"/></span>
																<br><span style="color: red;"><liferay-ui:message key="label-automatic-patrol-text"/></span>
															</td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-username-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getBasicauthid="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getBasicauthid()) && item !=null){
																						getBasicauthid=item.getBasicauthid();
																				}%>
																				<input type="text" name="<%= portletNamespace %>basicauthid<%= i %>" class="field form-control" placeholder="id" 
																				value="<%= ((CommonUtil.isStringNullOrEmpty(item.getBasicauthid())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getBasicauthid)) %>"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-password-colon"/></span>
																			</td>
																			<td>
																				<% 
																					String getBasicauthpassword="";
																					if(!CommonUtil.isStringNullOrEmpty(item.getBasicauthpassword()) && item !=null){
																						getBasicauthpassword=item.getBasicauthpassword();
																				}%>
																				<input type="text" name="<%= portletNamespace %>basicauthpassword<%= i %>" class="field form-control" placeholder="password" 
																				value="<%= ((CommonUtil.isStringNullOrEmpty(item.getBasicauthpassword())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(getBasicauthpassword)) %>"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
														<!-- Access Exclusion path -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-access-execution-path-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<textarea class="field form-control" style="resize:none;height:100px; width:615px !important;" name="<%= portletNamespace %>accessexclusionpath<%= i %>"
																				 placeholder="<%= portletNamespace %>targetInfo_textarea"><%= ((CommonUtil.isStringNullOrEmpty(item.getAccessexclusionpath())) ? PortalConstants.STRING_EMPTY : HtmlUtil.escape(item.getAccessexclusionpath())) %></textarea>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
															
								<% 		}
									} else { %>
										<tr>
											<input type="hidden" name="rowIndex" value="0"/>
											<td>						
												<select class="field form-control" style="width: auto !important" label="" name="<%= portletNamespace %>protocolGroup0" onchange="changePortNumbers(this, 0)" >	
													<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">http://</option>
													<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">https://</option>
												</select>
											</td>
											<td><input type="text" id="<%= portletNamespace %>host0" name="<%= portletNamespace %>host0" class="field form-control" style="width:100%;" placeholder="example.com" /></td>
											<td><input type="text" id="<%= portletNamespace %>port0" name="<%= portletNamespace %>port0" class="field form-control" style="width:80px;" placeholder="80" value="80"/></td>
											<td>
												<button class="btn user-filter-btn targetInfo_hideBtn" style="width:100%" id="hideSettingBtn0" name="hideSettingBtn0" onclick="hideSetting('hideSettingBtn0','table_0_0')" type="button">
											  	  <liferay-ui:message key="button-hide-advanced-setting"/></button>
												<a name="<%= portletNamespace %>btnDelete0" id="<%= portletNamespace %>btnDelete0" onclick="deleteRow(this)" class="btn btn-default targetInfo_deleteBtn" style="width:100%;margin-top:0px;"><span class='lfr-btn-label'><liferay-ui:message key="button-delete" /></span></a>
											</td>
										</tr>
										<tr>
											<td colspan="4">
												<table style="width: 100%;max-width: 100%;" id="table_0_0">
													<tbody>
														<tr>
															<td colspan="2"><span style="color: red;"><liferay-ui:message key="label-websocket-keep-alive-text"/></span></td>
														</tr>
														<tr>
															<td><div style="margin-right:90px;"><span><liferay-ui:message key="label-http-version-colon"/></span></div></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>httpversion0" value="1" id="httpversion_1" checked="">1.0
																<input type="radio" name="<%= portletNamespace %>httpversion0" value="2" id="httpversion_2" style="margin-left:40px">1.1
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-keep-alive-connection-colon"/></span></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>setkeepaliveconnection0" value="1" id="setkeepaliveconnection_1"><span><liferay-ui:message key="label-use"/></span>
																<input type="radio" name="<%= portletNamespace %>setkeepaliveconnection0" value="0" id="setkeepaliveconnection_0" style="margin-left:40px" checked=""><span><liferay-ui:message key="label-do-not-use"/></span>
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-response-content-length-colon"/></span></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>setresponsecontentlength0" value="1" id="setresponsecontentlength_1"  checked=""><span><liferay-ui:message key="label-ignore"/></span>
																<input type="radio" name="<%= portletNamespace %>setresponsecontentlength0" value="0" id="setresponsecontentlength_0" style="margin-left:40px"><span><liferay-ui:message key="label-do-not-ignore"/></span>
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-accept-encoding-header-colon"/></span></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>useacceptencodingheader0" value="1" id="useacceptencodingheader_1"  checked=""><span><liferay-ui:message key="label-delete-encoding"/></span>
																<input type="radio" name="<%= portletNamespace %>useacceptencodingheader0" value="0" id="useacceptencodingheader_0" style="margin-left:40px"><span><liferay-ui:message key="label-do-not-delete-encoding"/></span>
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-response-unzip-colon"/></span></td>
															<td>
																<input type="radio" name="<%= portletNamespace %>unzipresponse0" value="1" id="unzipresponse_1"  checked="">
																	<span><liferay-ui:message key="label-do-response"/></span>
																<input type="radio" name="<%= portletNamespace %>unzipresponse0" value="0" id="unzipresponse_0" style="margin-left:40px">
																	<span><liferay-ui:message key="label-do-not-do-response"/></span>
															</td>
														</tr>
														<tr>
															<td><span><liferay-ui:message key="label-http-protocol-colon"/></span></td>
															<td>
																<select name="<%= portletNamespace %>httpprotocol0" style="width: auto !important;">
																		<option value="auto" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="selected"><liferay-ui:message key="label-automatic"/></option>
																</select>
															</td>
														</tr>
														
														<!-- External proxy -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-external-proxy-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-host-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>externalproxyhost0" class="field form-control" style="width:300px;" placeholder="example.com" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-port-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>externalproxyport0" class="field form-control" style="width:100px;" placeholder="80" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-authentication-ID"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>externalproxyauthid0" class="field form-control" placeholder="id" />
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-authentication-password"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>externalproxyauthpassword0" class="field form-control" placeholder="password" />
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
														<!-- Client Certification -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-client-certificate-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td colspan="2">
																				<input type="checkbox" id="checkbox_0" onchange="toggleCheckbox(this,'<%= portletNamespace %>useclientcertificate0')"/><liferay-ui:message key="label-use"/>
																				<input type="hidden" id="<%= portletNamespace %>useclientcertificate0" name="<%= portletNamespace %>useclientcertificate0" value="0"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-certificate-colon"/></span>
																			</td>
																			<td>
																				<button class="btn user-filter-btn" style="width:100px;" id="fileBtn0" name="" onclick="clickFile('<%= portletNamespace %>certificatefile0')" type="button">
																					<liferay-ui:message key="label-reference-colon"/>
																				</button>&nbsp;&nbsp;<span id="strtextfile0"><liferay-ui:message key="label-file-not-selected-text"/></span>
																			</td>
																			<td>
																				<div class="hideInput" style="display:none;"> <div class="form-group input-text-wrapper"> 
																				<input class="field form-control" id="<%= portletNamespace %>certificatefile0" name="<%= portletNamespace %>certificatefile0" 
																				type="file" accept="<%=PortalConstants.CONTENT_TYPE_PKCS12 %>" onchange="sendValue('<%= portletNamespace %>certificatefile0','strtextfile0')"> </div> </div>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-passphrase-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>certificatefilepassword0" class="field form-control" placeholder="password" />
																			</td>
																		</tr>
																	</tbody>
																</table>
															  </td>
														 </tr>
														 
														  <!-- NTLM Authorization -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-ntlm-authentication-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-username-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>ntlmauthid0" class="field form-control" placeholder="id"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-password-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>ntlmauthpassword0" class="field form-control" placeholder="password"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-domain-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>ntlmauthdomain0" class="field form-control" placeholder="domain"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-hostname-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>ntlmauthhost0" class="field form-control" placeholder="host"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
														<!-- Digest Authorization -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-digest-authentication-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-username-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>digestauthid0" class="field form-control" placeholder="id"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-password-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>digestauthpassword0" class="field form-control" placeholder="password"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
														<!-- Basic Authorization -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-basic-authentication-colon"/></span>
																<br><span style="color: red;"><liferay-ui:message key="label-automatic-patrol-text"/></span>
															</td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<span style="display: inline-block;width: 100px;"><liferay-ui:message key="label-username-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>basicauthid0" class="field form-control" placeholder="id"/>
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<span><liferay-ui:message key="label-password-colon"/></span>
																			</td>
																			<td>
																				<input type="text" name="<%= portletNamespace %>basicauthpassword0" class="field form-control" placeholder="password"/>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
														<!-- Access Exclusion path -->
														<tr>
															<td style="vertical-align:top"><span><liferay-ui:message key="label-access-execution-path-colon"/></span></td>
															<td>
																<table style="border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;">
																	<tbody>
																		<tr>
																			<td>
																				<textarea class="field form-control" style="resize:none; width:615px !important; height:100px;" name="<%= portletNamespace %>accessexclusionpath0"
																				  placeholder="<%= portletNamespace %>targetInfo_textarea"></textarea>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
														
													</tbody>
												</table>
											</td>
										</tr>
								<% } %>
								<tr>
									<td colspan="4">
										<a onclick="addRow('targetInfoTable')" class="btn btn-default" style="width:100%;margin-top: 0px;" id="button-add"><span class='lfr-btn-label'><liferay-ui:message key="button-add" /></span></a>
									</td>
								</tr>
							<tbody>
						</table>
					</td>
				</tr>
					
			</tbody>
		</table>
		<br>
		<div style="padding-top: 10px;">
			<a id="backBtn" href="<%= viewRegisterScanAdvancedSettingURL %>"><span id="backBtn" class="btn cancel-btn" style="margin-right: 20px;"><liferay-ui:message key="label-back" /></span></a>
			<button type="submit" class="btn" style="margin-right: 20px;"><liferay-ui:message key="button-next-page" /></button>
			<a id="cancelBtn" href="<%= cancelURL %>"><span class="btn cancel-btn" style="width:auto; margin-right: 20px;"><liferay-ui:message key="button-cancel" /></span></a>
		</div>
	</form>
</div>
<%
	} else {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="message-no-access-rights" />
		</div>
		<%
	}
%>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/vex/register_scan_target_info.js"></script>
<script>
	define.amd = define._amd;
</script>
