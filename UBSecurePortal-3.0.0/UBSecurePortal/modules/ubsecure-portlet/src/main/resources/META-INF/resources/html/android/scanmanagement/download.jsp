<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResultItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem" %>

<%
ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
if (row.getObject() instanceof ScanItem) {
	ScanItem scan = (ScanItem) row.getObject();
%>

	<portlet:resourceURL var="scanDownloadReportURL">
		<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_DOWNLOAD_REPORT) %>" />
		<portlet:param name="projectId" value="<%= String.valueOf(scan.getProjectId()) %>" />
		<portlet:param name="scanId" value="<%= String.valueOf(scan.getScanId()) %>" />
	</portlet:resourceURL>
		
	<liferay-ui:icon-menu message="button-action">

	<%
	String downloadReference = "downloadReport('" + scanDownloadReportURL.toString() + "'); return false;";
		
	if (scan.getScanStatus() == ScanStatus.COMPLETE.getInteger()) {
		%>
		<liferay-ui:icon message="label-download" url="#" onClick="<%= downloadReference %>" />
		<%
	}
	%>
	</liferay-ui:icon-menu>
	
	<%
} else {
	ResultItem scan = (ResultItem) row.getObject();
	%>
	
	<portlet:resourceURL var="entireScanDownloadReportURL">
		<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_DOWNLOAD_REPORT) %>" />
		<portlet:param name="projectId" value="<%= String.valueOf(scan.getProjectId()) %>" />
		<portlet:param name="scanId" value="<%= String.valueOf(scan.getScanId()) %>" />
	</portlet:resourceURL>
		
	<liferay-ui:icon-menu message="button-action">
		<% 
		String downloadReference = "downloadReport('" + entireScanDownloadReportURL.toString() + "'); return false;";
		
		if (scan.getReportCount() > 0) {
			%>
			<liferay-ui:icon message="label-download" url="#" onClick="<%= downloadReference %>" />
			<%
		}
		%>
	</liferay-ui:icon-menu>
	
	<%
}
%>