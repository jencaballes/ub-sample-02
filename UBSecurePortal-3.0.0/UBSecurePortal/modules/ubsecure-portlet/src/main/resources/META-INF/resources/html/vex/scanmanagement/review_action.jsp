<%@page import="jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Scan" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResultItem"%>
<portlet:defineObjects />

<!-- Vex -->

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	VexDetectionResultItem detectionResultItem = (VexDetectionResultItem) row.getObject();
	
	long scanId = detectionResultItem.getScanid();
	String detectionResultId = detectionResultItem.getDetectionresultid();
	long number = detectionResultItem.getScanresultid();
	Scan scan = ScanLocalServiceUtil.getScan(scanId);
	Project project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
	String strDetectionJudgement = detectionResultItem.getDetectionjudgment();
	
	HttpSession httpSession = null;

	if (renderRequest != null) {
		httpSession = ControllerHelper.getHttpSession(renderRequest);
	} else if (resourceRequest != null) {
		httpSession = ControllerHelper.getHttpSession(resourceRequest);
	} else {
		httpSession = request.getSession();
	}
	
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	int iUserRole = PortalConstants.INT_ZERO;

	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	
	String strDisable = PortalConstants.STRING_EMPTY;
	//If judgement is not detection, disable the resend button.
	if(!strDetectionJudgement.equals(PortalConstants.STR_DETECTION_JUDGEMENT)){
		strDisable = "disabled";
	}
%>

	<% 
	String buttonId = "resendButton_"+number;
	/* Resend Review Detection Result - Complete status */
	if (scanId > PortalConstants.LONG_ZERO) { %>
		<portlet:actionURL name="resendDetectionResult" var="resendDetectionResultURL">
			<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_DETECTION_RESULT_REVIEW) %>" />
	        <portlet:param name="number" value="<%= String.valueOf(number) %>" />
	        <portlet:param name="detectionresultid" value="<%= String.valueOf(detectionResultId) %>" />
	    </portlet:actionURL>
			
			<%

			String onClickResend = PortalConstants.STRING_EMPTY;
			String onClickDownArrow = "javascript: showOrHideResendResultTable("+ number +")";
			int iIsResend = detectionResultItem.getIsresendinvex();
			if(!strDisable.equals("disabled")){
				onClickResend = "confirmResendResult('"+resendDetectionResultURL.toString()+"',"+iIsResend+")";
			}
			%>
		<div style="display: flex !important; align-items: flex-end !important;">
			<button type="button" name="resendDetectionResultURL" id="resendDetectionResultURL" class="btn <%=strDisable %>" value="button-scan" onClick="<%=onClickResend %>" 
			style="width:auto; color:#404040; margin-top: 0 !important; margin-right: 5px;"> <liferay-ui:message key="button-resend" /></button>
			
			<%if(!CommonUtil.isStringNullOrEmpty(detectionResultItem.getListofinspectiondatetime()) && !detectionResultItem.getListofinspectiondatetime().equals("null")){ %>
				<a href="<%=onClickDownArrow %>"><img src="<%=PortalConstants.DETECTION_ARROW_ICON_PATH%>" /></a>
			<%} %>
			
		</div>
		<table style="display:none;" id="resend_result_table_<%=String.valueOf(number) %>" class="table table-bordered table-hover table-striped detection-resend-result-table">
			<thead>
				<tr>
					<th><liferay-ui:message key="header-detection-inspection-date" /></th>
					<th><liferay-ui:message key="header-detection-resend-result" /></th> 
				</tr>
			</thead>
			<tbody>
				<%
					String inspectionDateTimeList[] = detectionResultItem.getListofinspectiondatetime().toString().split(";");
					String resendResultList[] = detectionResultItem.getListofdetectedresult().toString().split(";");
					for (int x = 0 ; x < inspectionDateTimeList.length ; x++){
						String resendResult = "label-no-vulnerability-detected";
						if(resendResultList[x].equals("0")){
							resendResult = "label-vulnerability-detected-again";
						}
					%>	
					<tr>
						<td><span><%=inspectionDateTimeList[x] %></span></td>
						<td><span><liferay-ui:message key="<%=resendResult %>" /></span></td>
					</tr>
				<%	} %>
				
			</tbody>
		</table>
	<%}%>