$(document).ready(function() {
	var portletNamespace = $("#portletNamespace").val()+"targetInfo_textarea";
	var label_access_execution_placeholder = $('#label-access-execution-placeholder').text();
	var html = document.getElementsByTagName('html')[0];
	html.innerHTML = html.innerHTML.replace(portletNamespace,label_access_execution_placeholder);
});

function changePortNumbers(select, index) {
	var portletNamespace = $("#portletNamespace").val();
	
	if(select.value == 1){
		document.getElementById(portletNamespace+"port"+index).value = "80"; 
		document.getElementById(portletNamespace+"port"+index).innerText = "80"; 
		
	}else if(select.value == 2 ){
		document.getElementById(portletNamespace+"port"+index).value = "443"; 
		document.getElementById(portletNamespace+"port"+index).innerText = "443"; 
	}
}

function submitForm() {
	$("#saveBtn").prop("disabled", true);
	$("#cancelBtn").prop("disabled", true);
	$("#cancelBtn").removeAttr("onclick");
	$("#saveBtn").removeAttr("onclick");
	
	document.getElementById("fm").submit();
}

function addRow(tableID) {
	var portletNamespace = $("#portletNamespace").val();

	var table = document.getElementById(tableID);

	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount-1);
	var rowIndex = document.getElementById(portletNamespace+"listTableSize").value;
	row.id="row_"+rowIndex;
	
	var cell1 = row.insertCell(0);
	var element1 = document.createElement("select");
	element1.name = portletNamespace+"protocolGroup"+rowIndex;
	element1.setAttribute('style','width: auto !important');
	element1.setAttribute('class',"field form-control");
	element1.setAttribute("onchange", "changePortNumbers(this,"+rowIndex+")");
	element1.id = portletNamespace+"select"+rowIndex;		
		var option1 = document.createElement("option");
			option1.text = "http://";
			option1.value = 1;
		element1.add(option1);
		var option2 = document.createElement("option");
			option2.text = "https://";
		option2.value = 2;
		element1.add(option2);
	cell1.appendChild(element1);

	var cell2 = row.insertCell(1);
	var element2 = document.createElement("input");
	element2.type = "text";
	element2.id = portletNamespace+"host"+rowIndex;
	element2.name = portletNamespace+"host"+rowIndex;
	element2.setAttribute('class',"field form-control");
	element2.setAttribute('placeholder', 'example.com');
	element2.setAttribute('style','width:300px;');
	cell2.appendChild(element2);

	var cell3 = row.insertCell(2);
	var element3 = document.createElement("input");
	element3.type = "text";
	element3.id = portletNamespace+"port"+rowIndex;
	element3.name = portletNamespace+"port"+rowIndex;
	element3.setAttribute('class',"field form-control");
	element3.setAttribute('placeholder', '80');
	element3.setAttribute('value', '80');
	element3.setAttribute('style','width:80px;');
	cell3.appendChild(element3);
	
	var cell4 = row.insertCell(3);
	
	//Hide advanced setting button
	var element4a = document.createElement("input");
	element4a.type = "button";
	element4a.name = portletNamespace+"btnHideSetting"+rowIndex;
	element4a.id="hideSettingBtn"+rowIndex;
	element4a.setAttribute('onclick',"hideSetting('hideSettingBtn"+rowIndex+"','table_"+rowIndex+"_"+rowIndex+"')");
	element4a.setAttribute('class',"btn btn-default targetInfo_hideBtn");
	element4a.setAttribute('style','width:100%;margin-top:0px;');
		
	// Delete button
	var element4 = document.createElement("input");
	element4.type = "button";
	element4.name = portletNamespace+"btnDelete"+rowIndex;
	element4.setAttribute('onclick',"deleteRow(this)");
	element4.setAttribute('class',"btn btn-default targetInfo_deleteBtn");
	element4.setAttribute('style','width:100%;margin-top:0 !important;');
	
	var buttonDeleteVal = $('#button-delete').text();
	var buttonHideAdvancedVal = $('#button-hide-advanced-setting').text();
	var buttonOpenAdvancedVal = $('#button-open-advanced-setting').text();
	
	element4a.value = buttonOpenAdvancedVal;
	element4.value = buttonDeleteVal;

	element4a.id = "hideSettingBtn"+ rowIndex;
	cell4.appendChild(element4a);
	
	element4.id = portletNamespace+"btnDelete"+ rowIndex;
	cell4.appendChild(element4);
	
	var currentSize = document.getElementById(portletNamespace+"currentListTableSize").value;
	
	document.getElementById(portletNamespace+"listTableSize").value = ++rowIndex;
	document.getElementById(portletNamespace+"currentListTableSize").value = ++currentSize;
	
	//Append target info row
	table = document.getElementById(tableID);
	rowCount = table.rows.length;
	var targetInforRow = table.insertRow(rowCount-1);
	rowIndex = rowIndex - 1;
	targetInforRow.id="row_"+rowIndex+"_"+rowIndex;

	appendTargetInfoHTMLString(rowIndex, portletNamespace);
	getRowIndexes(portletNamespace);
}

function deleteRow(button) {
	var portletNamespace = $("#portletNamespace").val();
	var rowCount = document.getElementById(portletNamespace+"currentListTableSize").value;
	var i = button.parentNode.parentNode.rowIndex;
	if (rowCount > 1) {
		//Remove basic setting row
		document.getElementById("targetInfoTable").deleteRow(i);
		//Remove detail setting row
		document.getElementById("targetInfoTable").deleteRow(i);
		document.getElementById(portletNamespace+"currentListTableSize").value = --rowCount;
	}
	getRowIndexes(portletNamespace);
}

function getRowIndexes(portletNameSpace){
	var eles = [];
	var inputs = document.getElementsByName("rowIndex");
	var strIndexes = "";
	for(var i = 0; i < inputs.length; i++) {
        strIndexes = strIndexes + inputs[i].value;
        if(i + 1 < inputs.length){
           strIndexes = strIndexes  +",";
       }
	}
	document.getElementById(portletNameSpace+"rowIndexes").value = strIndexes;
} 

function isBrowserIE () {
	var browser = navigator.userAgent;
	
	return (browser.indexOf("MSIE", 0) > -1 || (browser.indexOf("Firefox", 0) == -1 && browser.indexOf("Chrome", 0) == -1));
}

function appendTargetInfoHTMLString(rowIndex, portletNamespace){
	var label_websocket_keep_alive_text = $('#label-websocket-keep-alive-text').text();
	var label_http_version_colon= $('#label-http-version-colon').text();
	
	var label_keep_alive_connection_colon= $('#label-keep-alive-connection-colon').text();
	var label_use = $('#label-use').text();
	var label_do_not_use = $('#label-do-not-use').text();
	
	var label_response_content_length_colon =  $('#label-response-content-length-colon').text();
	var label_ignore =  $('#label-ignore').text();
	var label_do_not_ignore =  $('#label-do-not-ignore').text();
	
	var label_accept_encoding_header_colon =  $('#label-accept-encoding-header-colon').text();
	var label_delete_encoding =  $('#label-delete-encoding').text();
	var label_do_not_delete_encoding =$('#label-do-not-delete-encoding').text();
	
	var label_response_unzip_colon = $('#label-response-unzip-colon').text();
	var label_do_response = $('#label-do-response').text();
	var label_do_not_do_response = $('#label-do-not-do-response').text();
	
	var label_http_protocol_colon = $('#label-http-protocol-colon').text();
	var label_automatic = $('#label-automatic').text();

	var label_external_proxy_colon = $('#label-external-proxy-colon').text(); 
	var label_host_colon = $('#label-host-colon').text(); 
	var label_port_colon = $('#label-port-colon').text(); 
	var label_authentication_ID = $('#label-authentication-ID').text(); 
	var label_authentication_password = $('#label-authentication-password').text(); 

	var tableId = "table_"+rowIndex+"_"+rowIndex;
	var targetInfo = "<td colspan='4'><table style='width: 100%;max-width: 100%;' id='"+tableId+"' class='targetInfo_hide'><tbody>";
	
	//Set row index
	targetInfo = targetInfo + "<input type='hidden' name='rowIndex' value='"+rowIndex+"'/>";
	
	//Web socket red Text
	targetInfo = targetInfo + "<tr><td colspan='2'><span style='color: red;'>"+label_websocket_keep_alive_text+"</span></td></tr>";
	
	//HTTP version
	targetInfo = targetInfo + "<tr><td><span style='margin-right:90px;'>"+label_http_version_colon+"</span></td>"
	targetInfo = targetInfo + "<td><input type='radio' name='"+portletNamespace+"httpversion"+rowIndex+"' value='1' id='httpversion_1' checked=''>1.0 " +
			                      "<input type='radio' name='"+portletNamespace+"httpversion"+rowIndex+"' value='2' id='httpversion_2' style='margin-left:40px'>1.1 </td></tr>";
	
	//Keep-Alive connection
	targetInfo = targetInfo +"<tr><td><span>"+label_keep_alive_connection_colon+"</span></td>"
	targetInfo = targetInfo + "<td><input type='radio' name='"+portletNamespace+"setkeepaliveconnection"+rowIndex+"' value='1' id='setkeepaliveconnection_1'>"+label_use +
	 							   "<input type='radio' name='"+portletNamespace+"setkeepaliveconnection"+rowIndex+"' value='0' id='setkeepaliveconnection_0' style='margin-left:40px' checked=''>"+label_do_not_use+"</td></tr>";
	
	//Response content-length
	targetInfo = targetInfo +"<tr><td><span>"+label_response_content_length_colon+"</span></td>"
	targetInfo = targetInfo + "<td><input type='radio' name='"+portletNamespace+"setresponsecontentlength"+rowIndex+"' value='1' id='setresponsecontentlength_1' checked=''>"+label_ignore +
	 							   "<input type='radio' name='"+portletNamespace+"setresponsecontentlength"+rowIndex+"' value='0' id='setresponsecontentlength_0' style='margin-left:40px'>"+label_do_not_ignore+"</td></tr>";
	
	//Accept-Encoding header
	targetInfo = targetInfo +"<tr><td><span>"+label_accept_encoding_header_colon+"</span></td>"
	targetInfo = targetInfo + "<td><input type='radio' name='"+portletNamespace+"useacceptencodingheader"+rowIndex+"' value='1' id='useacceptencodingheader_1' checked=''>"+label_delete_encoding+
	 							   "<input type='radio' name='"+portletNamespace+"useacceptencodingheader"+rowIndex+"' value='0' id='useacceptencodingheader_0' style='margin-left:40px'>"+label_do_not_delete_encoding+"</td></tr>";
	
	//Response unzip
	targetInfo = targetInfo +"<tr><td><span>"+label_response_unzip_colon+"</span></td>"
	targetInfo = targetInfo + "<td><input type='radio' name='"+portletNamespace+"unzipresponse"+rowIndex+"' value='1' id='unzipresponse_1' checked=''>"+label_do_response +
	 							  "<input type='radio' name='"+portletNamespace+"unzipresponse"+rowIndex+"' value='0' id='unzipresponse_0' style='margin-left:40px'>"+label_do_not_do_response+"</td></tr>";

	//HTTPS protocol
	targetInfo = targetInfo +"<tr><td><span>"+label_http_protocol_colon+"</span></td>"
	targetInfo = targetInfo + "<td><select name='"+portletNamespace+"httpprotocol"+rowIndex+"' style='width: auto !important;'>"+ 
							"<option value='auto' style='white-space: nowrap; text-overflow: ellipsis; overflow: hidden;' selected='selected'>"+label_automatic+"</option>"+"</td></tr>";
	
	//External proxy
	targetInfo = targetInfo +"<tr><td style='vertical-align:top'><span>"+label_external_proxy_colon+"</span></td>";
	targetInfo = targetInfo +"<td><table style='border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate;margin-bottom: 10px;'><tbody><tr>";
	targetInfo = targetInfo +"<td><span style='display: inline-block;width: 100px;'>"+label_host_colon+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"externalproxyhost"+rowIndex+"' class='field form-control' style='width:300px;' placeholder='example.com' /></td></tr>";
	targetInfo = targetInfo +"<tr><td><span>"+label_port_colon+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"externalproxyport"+rowIndex+"' class='field form-control' style='width:100px;' placeholder='80' /></td></tr>";
	targetInfo = targetInfo +"<tr><td><span>"+label_authentication_ID+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"externalproxyauthid"+rowIndex+"' class='field form-control' placeholder='id' /></td><tr>";
	targetInfo = targetInfo +"<td><span>"+label_authentication_password+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"externalproxyauthpassword"+rowIndex+"' class='field form-control' placeholder='password' /></td></tr></tbody></table></td></tr>";
	
	//Client Certificate
	var label_client_certificate_colon = $('#label-client-certificate-colon').text(); 
	var label_certificate_colon =  $('#label-certificate-colon').text(); 
	var label_use = $('#label-use').text(); 
	var label_reference_colon = $('#label-reference-colon').text();
	var label_file_not_selected_text = $('#label-file-not-selected-text').text();
	var label_passphrase_colon = $('#label-passphrase-colon').text();
	var clickName = portletNamespace+"certificatefile"+rowIndex;
	var onclickFunction = "onclick='clickFile('"+clickName+"')'";
	
	targetInfo = targetInfo +"<tr><td style='vertical-align:top'><span>"+label_client_certificate_colon+"</span></td><td>";
	targetInfo = targetInfo +"<table style='border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;'>";
	targetInfo = targetInfo +"<tbody><tr><td colspan='2'><input type='checkbox' id='checkbox_"+rowIndex+"'/>"+label_use;
	targetInfo = targetInfo +"<input type='hidden' id='"+portletNamespace+"useclientcertificate"+rowIndex+"' name='"+portletNamespace+"useclientcertificate"+rowIndex+"' value='0'/>";
	targetInfo = targetInfo +"</td></tr><tr><td><span style='display: inline-block;width: 100px;'>"+label_certificate_colon+"</span></td>";
	targetInfo = targetInfo +"<td><button class='btn user-filter-btn' style='width:100px;' id='fileBtn"+rowIndex+"' name='' type='button'>";
	targetInfo = targetInfo +label_reference_colon+"</button>&nbsp;&nbsp;<span id='strtextfile"+rowIndex+"'>"+label_file_not_selected_text+"</span>";
	targetInfo = targetInfo +"</td><td><div class='hideInput' style='display:none;'> <div class='form-group input-text-wrapper'> ";
	targetInfo = targetInfo +"<input class='field form-control' id='"+portletNamespace+"certificatefile"+rowIndex+"' name='"+portletNamespace+"certificatefile"+rowIndex+"' type='file' accept='.p12, .pfx'> </div> </div>";
	targetInfo = targetInfo +"</td></tr><tr><td><span>"+label_passphrase_colon+"</span></td><td>";
	targetInfo = targetInfo +"<input type='text' name='"+portletNamespace+"certificatefilepassword"+rowIndex+"' class='field form-control' placeholder='password' /></td></tr></tbody></table> </td> </tr>";
	
	//NTLM Authorization
	var label_ntlm_authentication_colon = $('#label-ntlm-authentication-colon').text();
	var label_username_colon = $('#label-username-colon').text();
	var label_password_colon = $('#label-password-colon').text();
	var label_domain_colon = $('#label-domain-colon').text();
	var label_hostname_colon = $('#label-hostname-colon').text();
			
	targetInfo = targetInfo +"<tr><td style='vertical-align:top'><span>"+label_ntlm_authentication_colon+"</span></td>";
	targetInfo = targetInfo +"<td><table style='border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate;margin-bottom: 10px;'><tbody>";
	targetInfo = targetInfo +"<tr><td><span style='display: inline-block;width: 100px;'>"+label_username_colon+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"ntlmauthid"+rowIndex+"' class='field form-control' placeholder='id'/>";
	targetInfo = targetInfo +"</td></tr><tr><td><span>"+label_password_colon+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"ntlmauthpassword"+rowIndex+"' class='field form-control' placeholder='password'/>";
	targetInfo = targetInfo +"</td></tr><tr><td><span>"+label_domain_colon+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"ntlmauthdomain"+rowIndex+"' class='field form-control' placeholder='domain'/></td></tr>";
	targetInfo = targetInfo +"<tr><td><span>"+label_hostname_colon+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"ntlmauthhost"+rowIndex+"' class='field form-control' placeholder='host'/></td></tr></tbody></table></td></tr>";
	
	//Digest Authorization
	var label_digest_authentication_colon = $('#label-digest-authentication-colon').text();
	
	targetInfo = targetInfo +"<tr><td style='vertical-align:top'>"+label_digest_authentication_colon+"</span></td><td>";
	targetInfo = targetInfo +"<table style='border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate;margin-bottom: 10px;'><tbody>";
	targetInfo = targetInfo +"<tr><td><span style='display: inline-block;width: 100px;'>"+label_username_colon+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"digestauthid"+rowIndex+"' class='field form-control' placeholder='id'/></td></tr>";
	targetInfo = targetInfo +"<tr><td><span>"+label_password_colon+"</span></td><td>";
	targetInfo = targetInfo +"<input type='text' name='"+portletNamespace+"digestauthpassword"+rowIndex+"' class='field form-control' placeholder='password'/></td>";
	targetInfo = targetInfo +"</tr></tbody></table></td></tr>";
	
	//Basic Authorization
	var label_basic_authentication_colon = $('#label-basic-authentication-colon').text();
	var label_automatic_patrol_text = $('#label-automatic-patrol-text').text();
	
	targetInfo = targetInfo +"<tr><td style='vertical-align:top'><span>"+label_basic_authentication_colon+"</span>";
	targetInfo = targetInfo +"<br><span style='color: red;'>"+label_automatic_patrol_text+"</span>";
	targetInfo = targetInfo +"</td><td><table style='border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate; margin-bottom: 10px;'>";
	targetInfo = targetInfo +"<tbody><tr><td><span style='display: inline-block;width: 100px;'>"+label_username_colon+"</span></td>";
	targetInfo = targetInfo +"<td><input type='text' name='"+portletNamespace+"basicauthid"+rowIndex+"' class='field form-control' placeholder='id'/>";
	targetInfo = targetInfo +"</td></tr><tr><td><span>"+label_password_colon+"</span></td><td>";
	targetInfo = targetInfo +"<input type='text' name='"+portletNamespace+"basicauthpassword"+rowIndex+"' class='field form-control' placeholder='password'/>";
	targetInfo = targetInfo +"</td></tr></tbody></table></td></tr>";
	
	var label_access_execution_path_colon = $('#label-access-execution-path-colon').text();
	var label_access_execution_placeholder = $('#label-access-execution-placeholder').text();
	
	//Access Exclusion path
	targetInfo = targetInfo +"<tr><td style='vertical-align:top'><span>"+label_access_execution_path_colon+"</span></td>";
	targetInfo = targetInfo +"<td><table style='border: 1px solid #ddd !important; border-spacing:10px;border-collapse:separate;margin-bottom: 10px;'><tbody>";
	targetInfo = targetInfo +"<tr><td><textarea class='field form-control' style='resize:none;height:100px;width:615px !important;' name='"+portletNamespace+"accessexclusionpath"+rowIndex+"' placeholder='"+label_access_execution_placeholder+"'></textarea>";
	targetInfo = targetInfo +"</td></tr></tbody></table></td></tr>";
	
    targetInfo = targetInfo + "</tbody></table></td>";
    
    $('#row_'+rowIndex+'_'+rowIndex+'').append(targetInfo);

    var toggleCheckBox = "this, '"+portletNamespace+"useclientcertificate"+rowIndex+"'";
    $('#'+"checkbox_"+rowIndex+'').attr('onchange',"toggleCheckbox("+toggleCheckBox+")"); 
    $('#fileBtn'+rowIndex+'').attr('onClick', "clickFile('"+clickName+"')");
    var sendValue = "'"+portletNamespace+"certificatefile"+rowIndex+"','strtextfile"+rowIndex+"'";
    $('#'+portletNamespace+'certificatefile'+rowIndex+'').attr('onchange',"sendValue("+sendValue+")");
}

// Function that hides setting row after pressing the hide setting button.
function hideSetting (buttonId, rowId){
	var isHidden = $("#"+rowId).hasClass("targetInfo_hide");
	var btnHideText= $('#button-hide-advanced-setting').text();
	var btnShowText= $('#button-open-advanced-setting').text();
	
	if(isHidden){
		document.getElementById(buttonId).innerText = btnHideText;
		document.getElementById(buttonId).value = btnHideText;
		$("#"+rowId).slideUp(0,function(){;
			$("#"+rowId).removeClass('targetInfo_hide').slideDown('fast'); 
		});

	}else{
		document.getElementById(buttonId).innerText = btnShowText;
		document.getElementById(buttonId).value = btnShowText;
		$("#"+rowId).slideUp('fast',function(){
			$("#"+rowId).addClass('targetInfo_hide').slideDown(0);
		 });
		
	}
	
}

function sendValue(id, textFile){
	var path = $("#"+id).val();
	var fileName = path.split("\\");
	var file = fileName[fileName.length - 1];
	
	if (file == null || file == "") {
		var label_file_not_selected_text = $('#label-file-not-selected-text').text();
		$("#"+textFile).html(label_file_not_selected_text);
	} else {
		$("#"+textFile).text(file);
	}
}

function clickFile(id){
	$("#"+id).trigger('click');
}

function toggleCheckbox(element,id){
	var elem = document.getElementById(id);
	
	if(element.checked){
		elem.setAttribute("value","1");
	}else{
		elem.setAttribute("value","0");
	}
	
}
