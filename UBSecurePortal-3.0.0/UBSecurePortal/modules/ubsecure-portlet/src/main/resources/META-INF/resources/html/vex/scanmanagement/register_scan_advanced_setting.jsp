<%@page import="com.liferay.portal.kernel.util.Portal"%>
<%@page import="jp.ubsecure.portal.jubjub.portlet.model.VexScanSettingItem"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List"%>

<%@ page import="javax.portlet.PortletSession"%>

<%@ page import="jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.ApiSignatureSetInfo" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Scan" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformationItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexScanSettingItem" %>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >

<portlet:defineObjects />

<!-- Vex -->

<%
	//Previous and current screen
	PortletSession pSession = renderRequest.getPortletSession();
	Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
	String strCurrScreen = PortalConstants.STRING_EMPTY;
	Object oProjectId = pSession.getAttribute(PortalConstants.PARAM_PROJECT_ID);
	Object oScanId = pSession.getAttribute(PortalConstants.PARAM_SCAN_ID);
	
	long lProjectId = PortalConstants.LONG_ZERO;
	long lScanId = PortalConstants.LONG_ZERO;
	
	if (!CommonUtil.isObjectNull(oCurrScreen)) {
		strCurrScreen = oCurrScreen.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
			pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
		}
	}
	
	if (!CommonUtil.isObjectNull(oProjectId)) {
		lProjectId = Long.parseLong(oProjectId.toString());
	}
	
	if (!CommonUtil.isObjectNull(oScanId)) {
		lScanId = Long.parseLong(oScanId.toString());
	}
	
	pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_ADVANCED_SETTING_REGISTRATION);
	
	// End
	
	// Show error indicators
	boolean bShowDBConnError = false;
	
	// Get project details
	Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);
	// End

	String projectName = PortalConstants.STRING_EMPTY;
	int status = PortalConstants.INT_ZERO;
	
	if (!CommonUtil.isObjectNull(project)) {
		projectName = project.getProjectName();
		status = project.getStatus();
		lProjectId = project.getProjectId();
	}
	
	String portletNamespace = null;
	int iUserAction = PortalConstants.INT_ZERO;
	portletNamespace = renderResponse.getNamespace();
	
//	List<ApiSignatureSetInfo> webInspectionSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST);
//	List<ApiSignatureSetInfo> serverFilesSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST);
//	List<ApiSignatureSetInfo> serverSettingsSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST);

	Scan scan = (Scan) renderRequest.getAttribute(PortalConstants.PARAM_SCAN);
	VexScanSetting scanSetting = (VexScanSetting) renderRequest.getAttribute(PortalConstants.PARAM_SCAN_SETTING);
	List<VexTargetInformationItem> targetInformationList = (List<VexTargetInformationItem>) renderRequest.getAttribute(PortalConstants.PARAM_TARGET_INFORMATION_LIST);
	List<VexLoginSettingItem> loginSettingList = (List<VexLoginSettingItem>) renderRequest.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
	
	String scanName = (String)renderRequest.getAttribute(PortalConstants.PARAM_SCAN_NAME);
	if(!CommonUtil.isObjectNull(scan)){
		scanName = scan.getFileName();
	}
	
	VexScanSettingItem scanAdvancedSetting = (VexScanSettingItem) pSession.getAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
	
	String startTimeFormat = PortalConstants.STRING_EMPTY;
	String endTimeFormat = PortalConstants.STRING_EMPTY;
	
	DateFormat formatter = new SimpleDateFormat(PortalConstants.TIME_FORMAT_HH_MM);
	
	if (null != scanSetting) {
		if (null != scanAdvancedSetting.getStarttime() && null != scanAdvancedSetting.getEndtime()) {
			startTimeFormat = formatter.format(scanAdvancedSetting.getStarttime());
			endTimeFormat = formatter.format(scanAdvancedSetting.getEndtime());
		}
	}
	
	String paramStartTime = PortalConstants.STRING_EMPTY;
	String paramEndTime = PortalConstants.STRING_EMPTY;
	
	try{
		paramStartTime = formatter.format(scanAdvancedSetting.getStarttime());
	}catch(Exception e){
		//do nothing
	}
	
	try{
		paramEndTime = formatter.format(scanAdvancedSetting.getEndtime());
	}catch(Exception e){
		//do nothing
	}
	
	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	httpSession.setAttribute("Current_screen", "VEX");
	int iUserRole = 0;
	long lUserId = 0L;
	boolean bUseVEX = false;
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	
	if (!CommonUtil.isObjectNull(oUserId)) {
		lUserId = Long.parseLong(oUserId.toString());
	}
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
		bUseVEX = true;
	} else {
		Object oUseVEX = httpSession.getAttribute(PortalConstants.USE_VEX);
		
		if (!CommonUtil.isObjectNull(oUseVEX)) {
			bUseVEX = Boolean.parseBoolean(oUseVEX.toString());
		}
	}
	
	Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
		strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
	}

	Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
	String strCxServerErrorMsg = PortalConstants.STRING_EMPTY;
	
	if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
		strCxServerErrorMsg = oCxServerErrorMsg.toString();
	}
	
	pSession.removeAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	
	if (bUseVEX && (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger() || iUserRole == UserRole.GEN_USER.getInteger())) {
		boolean bErrorFromCxSuite = SessionErrors.contains(renderRequest, PortalMessages.VEX_REGISTER_SCAN_FAILED);
%>

<portlet:actionURL name="viewScanList" var="cancelURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<portlet:param name="type" value="<%= String.valueOf(ProjectType.VEX.getInteger()) %>" />
</portlet:actionURL>


<portlet:actionURL name="viewRegisterScanAdvancedSetting" var="viewRegisterScanAdvancedSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanSimpleSetting" var="viewRegisterScanSimpleSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="registerScanSimpleSetting" var="registerScanSimpleSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="registerScanAdvancedSetting" var="registerScanAdvancedSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<portlet:param name="scanId" value="<%= String.valueOf(lScanId) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanTargetInformation" var="viewRegisterScanTargetInformationURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<div style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px">
	<liferay-ui:message key="header-project-registration" />
</div>

<div style="height: 14px;">
	<hr style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>


<div style="width: 100%; position: relative;">
	<div class="tabbable-content instantiated" id="portlet-set-properties">
		<ul class="lfr-nav nav nav-tabs nav-tabs-default " role="tablist">
			<li class="tab yui3-widget" role="presentation"><a href="<%= viewRegisterScanSimpleSettingURL %>" role="tab" tabindex="0"><liferay-ui:message key="button-simple-setting" /></a></li>
			<li class="tab yui3-widget tab-focused active tab-selected" role="presentation"><a href="#" role="tab" tabindex="-1"><liferay-ui:message key="button-advanced-setting" /></a></li>
		</ul>
	</div>
	<div id="userIdError" class="alert alert-danger hideError">
		<liferay-ui:message key="error-search-user-id-too-long" />
	</div>
	
	<div id="userNameError" class="alert alert-danger hideError">
		<liferay-ui:message key="error-search-username-too-long" />
	</div>
	
	<div id="userDoesNotExistError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	</div>
	
	<div id="userDoesNotBelongToGroupError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP %>" />
	</div>
	
	<div id="dbConnError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.ORM_EXCEPTION %>" />
	</div>
	
	<div id="userDoesNotExistError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	</div>
	
	<div id="dbConnError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.ORM_EXCEPTION %>" />
	</div>
	
	<style>
		#confirm {
	   display: none;
	   background-color: #FFFFFF;
	   border: 1px solid #aaa;
	   position: fixed;
	   width: 400px;
	   height: auto;
	   left: 35%;
	   z-index: 9999;
	   padding: 6px 8px 8px;
	   box-sizing: border-box;
	   text-align: center;
	   top: 0;
	}
	#confirm button {
	   background-color: #489de5;
	   display: inline-block;
	   border-radius: 5px;
	   border: 1px solid #aaa;
	   padding: 0px;
	   text-align: center;
	   width: 80px;
	   cursor: pointer;
	   color:white;
	   font-size: 13px !important;
	}
	#confirm button {
	   background-color: #489de5;
	   display: inline-block;
	   border-radius: 5px;
	   border: 1px solid #aaa;
	   padding: 0px;
	   text-align: center;
	   width: 80px;
	   cursor: pointer;
	   color:white;
	   font-size: 13px !important;
	}
	#confirm .message {
	   text-align: left;
	   font-size: 13px;
	   margin-bottom: 15px;
	   margin-top: 15px;
	}
	
	#confirm .hostMessage{
	   text-align: left;
	   font-size: 13px;
	   margin-left:10px;
	   margin-bottom: 10px;
	}
	
	#confirm .no{
		background-color: #FFFFFF;
	    color: black;
	    margin-left: 10px;
	    width: 85px;
	}
	
	#confirmBoxTable{
		width: 100%;
	}
	
	.confirmButtons{
		text-align: right;
	}
	
	#overlay {
	     visibility: hidden;
	     position: absolute;
	     left: 0px;
	     top: 0px;
	     width:100%;
	     height:100%;
	     text-align:center;
	     z-index: 9998;
	     background-image:url(background-trans.png);
	}
	
	#overlay div {
	     width:300px;
	     margin: 100px auto;
	     background-color: #fff;
	     border:1px solid #000;
	     padding:15px;
	     text-align:center;
	}
	.custom-tooltiptext{
		width: 169% !important;
		left: -20% !important;
	}
	</style>

	<div>
		<%
		if (bErrorFromCxSuite) {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.VEX_REGISTER_SCAN_FAILED %>" /><liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
			</div>
			<%
		}
		%>
		
		<liferay-ui:success key="<%= PortalConstants.REGISTER_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.REGISTER_SCAN_SUCCESSFUL %>" />
	
		<liferay-ui:error key="<%= PortalMessages.PROJECT_ID_INVALID %>" message="<%= PortalMessages.PROJECT_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" message="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" />
		
	    <liferay-ui:error key="<%= PortalMessages.ADD_SCAN_FAILED %>" message="<%= PortalMessages.ADD_SCAN_FAILED %>" />
		
		<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
		
		<liferay-ui:error key="<%= PortalErrors.CX_SERVER_ERROR %>" message="<%= strCxServerErrorMsg %>" />
		<liferay-ui:error key="<%= PortalMessages.CX_SESSION_ID_INVALID %>" message="<%= PortalMessages.CX_SESSION_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.VEX_HOST_INVALID %>" message="<%= PortalMessages.VEX_HOST_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PRESET_ID_INVALID %>" message="<%= PortalMessages.PRESET_ID_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.SCAN_NAME_INVALID %>" message="<%= PortalMessages.SCAN_NAME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_SCAN_NAME %>" message="<%= PortalMessages.NO_SCAN_NAME %>" />
		<liferay-ui:error key="<%= PortalMessages.START_TIME_AND_END_TIME_INVALID %>" message="<%= PortalMessages.START_TIME_AND_END_TIME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.START_TIME_INVALID %>" message="<%= PortalMessages.START_TIME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.END_TIME_INVALID %>" message="<%= PortalMessages.END_TIME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.START_URL_INVALID %>" message="<%= PortalMessages.START_URL_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.AUTHORIZED_PATROL_URL_INVALID %>" message="<%= PortalMessages.AUTHORIZED_PATROL_URL_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.LOGIN_URL_INVALID %>" message="<%= PortalMessages.LOGIN_URL_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PARAMETER_NAME_ID_INVALID %>" message="<%= PortalMessages.PARAMETER_NAME_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PARAMETER_NAME_PASS_INVALID %>" message="<%= PortalMessages.PARAMETER_NAME_PASS_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_START_URL %>" message="<%= PortalMessages.NO_START_URL %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_AUTHORIZED_PATROL_URL %>" message="<%= PortalMessages.NO_AUTHORIZED_PATROL_URL %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_LOGIN_URL %>" message="<%= PortalMessages.NO_LOGIN_URL %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_PARAMETER_NAME_ID %>" message="<%= PortalMessages.NO_PARAMETER_NAME_ID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_PARAMETER_NAME_PASS %>" message="<%= PortalMessages.NO_PARAMETER_NAME_PASS %>" />
		<liferay-ui:error key="<%= PortalMessages.PROTOCOL_INVALID %>" message="<%= PortalMessages.PROTOCOL_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.HOST_INVALID %>" message="<%= PortalMessages.HOST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PORT_INVALID %>" message="<%= PortalMessages.PORT_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_HOST %>" message="<%= PortalMessages.NO_HOST %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_PORT %>" message="<%= PortalMessages.NO_PORT %>" />
		<liferay-ui:error key="<%= PortalMessages.TARGET_INFORMATION_LIST_INVALID %>" message="<%= PortalMessages.TARGET_INFORMATION_LIST_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_TARGET_INFORMATION_LIST %>" message="<%= PortalMessages.NO_TARGET_INFORMATION_LIST %>" />
		<liferay-ui:error key="<%= PortalMessages.ADD_SCAN_FAILED %>" message="<%= PortalMessages.ADD_SCAN_FAILED %>" />
	
		<liferay-ui:error key="<%= PortalMessages.FILE_NOT_FOUND_EXCEPTION %>" message="<%= PortalMessages.FILE_NOT_FOUND_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.IO_EXCEPTION %>" message="<%= PortalMessages.IO_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_WEB_INSPECTION_SIGNATURE_SET_GROUP %>" message="<%= PortalMessages.NO_WEB_INSPECTION_SIGNATURE_SET_GROUP %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_SERVER_FILE_SIGNATURE_SET_GROUP %>" message="<%= PortalMessages.NO_SERVER_FILE_SIGNATURE_SET_GROUP %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_SERVER_SETTING_SIGNATURE_SET_GROUP %>" message="<%= PortalMessages.NO_SERVER_SETTING_SIGNATURE_SET_GROUP %>" />
		<liferay-ui:error key="<%= PortalMessages.START_TIME_END_TIME_DIFFERENCE %>" message="<%= PortalMessages.START_TIME_END_TIME_DIFFERENCE %>" />
	
	</div>
	<form action="<%= registerScanAdvancedSettingURL %>" method="post" id="fm" class="edit-scan-advanced-setting-form">
		<table>
			<input type="hidden" id="scanSettingStartTime" value="<%= startTimeFormat %>" />
			<input type="hidden" id="scanSettingEndTime" value="<%= endTimeFormat %>" />
			<input type="hidden" id="paramStartTime" value="<%= paramStartTime %>" />
			<input type="hidden" id="paramEndTime" value="<%= paramEndTime %>" />
			<input type="hidden" id="scanId" value="<%= lScanId %>" />
			<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
			<tbody>				
				<tr class="input-row">
					<td><liferay-ui:message key="label-project-name" />:</td>
					<td><span style="margin-left: 20px;"><%= HtmlUtil.escapeAttribute(projectName) %></span></td>
					<td><span style="text-decoration: underline;margin-left: 150px;"><liferay-ui:message key="label-timer-setting" /></span></td>
				</tr>
			
				<tr class="input-row">
					<td><span><liferay-ui:message key="label-scan-name" />:</span><span style="color: red;">*</span></td>
					<td>
						<div class="form-group input-text-wrapper" style="margin-left: 20px;">
							<% if (null != scan || null != scanName) {  %>	
								<aui:input type="text" style="width: 100%;" name="<%=PortalConstants.PARAM_SCAN_NAME %>" value="<%= scanName %>" label="" />
							<% } else { %>
								<aui:input type="text" style="width: 100%;" name="<%=PortalConstants.PARAM_SCAN_NAME %>" value="" label="" />
							<% } %>
						</div>
					</td>
					<td><span style="margin-left: 150px;"><liferay-ui:message key="label-start-time" /></span></td>
					<td>
						<% 
							String[] arrTime;
							int hourValue = 0;
							int minuteValue = 0;
							
							if (paramStartTime.length() > 0) {
								arrTime = paramStartTime.split(":");
								hourValue = Integer.parseInt(arrTime[0]);
								minuteValue = Integer.parseInt(arrTime[1]);
							}
							
							if (startTimeFormat.length() > 0) {
								arrTime = startTimeFormat.split(":");
								hourValue = Integer.parseInt(arrTime[0]);
								minuteValue = Integer.parseInt(arrTime[1]);
							}
						
							if (paramStartTime.length() > 0 || startTimeFormat.length() > 0) {
						%>	
							<liferay-ui:input-time name="startTime" amPmParam="" hourParam="hour" hourValue="<%= hourValue %>" minuteParam="minute" minuteValue="<%= minuteValue %>" minuteInterval="1" timeFormat="24-hour" />
						<% } else { %>
							<liferay-ui:input-time name="startTime" amPmParam="" hourParam="hour" hourValue="0" minuteParam="minute" minuteValue="0" minuteInterval="1" timeFormat="24-hour" />
						<% } %>
					</td>
				</tr>

				<tr class="input-row">
					<td><span><liferay-ui:message key="label-implementation-environment" /></span></td>
					<td>
						<div class="form-group input-text-wrapper" style="margin-left: 20px;">
							<div style="display:inline-block;margin-right: 10px;"> 
								<label style="vertical-align: bottom;font-weight:normal;"><liferay-ui:message key="label-production-environment" />:</label>
								<aui:input type="radio" name="implementationEnvironment" value="0" style="vertical-align: top;"
									checked="<%= (null == scanAdvancedSetting || scanAdvancedSetting.getImplementationenvironment() == 0) %>" label="" />
							</div>
							<div style="display:inline-block"> 
								<label style="vertical-align: bottom;font-weight:normal;"><liferay-ui:message key="label-verification-environment" />:</label>
								<aui:input type="radio" name="implementationEnvironment" value="1" style="vertical-align: top;" 
									checked="<%= (null != scanAdvancedSetting && scanAdvancedSetting.getImplementationenvironment() == 1) %>" label="" />
							</div>
						</div>
					</td>
					<td><span style="margin-left: 150px;"><liferay-ui:message key="label-end-time" /></span></td>
					<td>
						<% 
							if (paramEndTime.length() > 0) {
								arrTime = paramEndTime.split(":");
								hourValue = Integer.parseInt(arrTime[0]);
								minuteValue = Integer.parseInt(arrTime[1]);
							}
						
							if (endTimeFormat.length() > 0) {
								arrTime = endTimeFormat.split(":");
								hourValue = Integer.parseInt(arrTime[0]);
								minuteValue = Integer.parseInt(arrTime[1]);
							}
						
							if (paramEndTime.length() > 0 || endTimeFormat.length() > 0) {
						%>	
							<liferay-ui:input-time name="endTime" amPmParam="" hourParam="hour" hourValue="<%= hourValue %>" minuteParam="minute" minuteValue="<%= minuteValue %>" minuteInterval="1" timeFormat="24-hour" />
						<% } else { %>
							<liferay-ui:input-time name="endTime" amPmParam="" hourParam="hour" hourValue="0" minuteParam="minute" minuteValue="0" minuteInterval="1" timeFormat="24-hour" />
						<% } %>
					</td>
				</tr>
				
				<tr class="input-row">
					<%-- <td><span style="text-decoration: underline;"><liferay-ui:message key="label-web-inspection-signature-set" /></span></td>
					<td></td> --%>
					<td><span><liferay-ui:message key="label-email-notification" /></span></td>
					<td>
						<div class="form-group input-text-wrapper" style="margin-left: 20px;">
							<div style="display:inline-block;margin-right: 10px;">
								<label style="vertical-align: bottom;font-weight:normal">ON : &nbsp;</label>
								<aui:input type="radio" name="emailNotification" value="1" style="vertical-align: top;" 
									checked="<%= (null == scanAdvancedSetting || scanAdvancedSetting.getSetemailnotification() == 1) %>" label="" />
							</div>
							<div style="display:inline-block">
								<label style="vertical-align: bottom;font-weight:normal">OFF : &nbsp;</label>
								<aui:input type="radio" name="emailNotification" value="0" style="vertical-align: top;" 
									checked="<%= (null != scanAdvancedSetting && scanAdvancedSetting.getSetemailnotification() == 0) %>" label="" />
							</div>
						</div>
					</td>
					<%-- <td>
						<div style="margin-left: 150px;display: -webkit-box;">
							<div><span><liferay-ui:message key="label-crawling-only" /></span></div>
							<div class="form-group input-text-wrapper vextooltip" style="width:100%;opacity:unset; margin-top: 0px;display: block;">
								<img src="<%=PortalConstants.FAILED_INFO_ICON_PATH%>"/> 
								<span class="tooltiptext custom-tooltiptext">
									<liferay-ui:message key="label-crawling-only-info" />
								</span>
							</div>
						</div>
					</td> --%>
					<%-- <td>
						<div class="form-group input-text-wrapper" style="margin-left: 80px;">
							<div style="display:inline-block;margin-right: 10px;">
								<label style="vertical-align: bottom;font-weight:normal">ON : &nbsp;</label>
								<aui:input type="radio" name="setCrawlingOnly" value="1" style="vertical-align: top;" 
									checked="<%= (null != scanAdvancedSetting && scanAdvancedSetting.getSetcrawlingonly() == 1) %>" label="" />
							</div>
							<div style="display:inline-block">
								<label style="vertical-align: bottom;font-weight:normal">OFF : &nbsp;</label>
								<aui:input type="radio" name="setCrawlingOnly" value="0" style="vertical-align: top;" 
									checked="<%= (null == scanAdvancedSetting || scanAdvancedSetting.getSetcrawlingonly() == 0) %>" label="" />
							</div>
						</div>
					</td> --%>
				</tr>
				
				<%-- <tr class="input-row">
					<td colspan="4">
						<aui:select name="webInspectionSignatureSetGroup" style="width: auto !important;" label="">
							<% 
							if (!CommonUtil.isListNullOrEmpty(webInspectionSignatureSetList) && webInspectionSignatureSetList.size() > 0) {
								for (ApiSignatureSetInfo signature : webInspectionSignatureSetList) {  
									if (null != scanAdvancedSetting) {
							%>
										<aui:option value="<%= signature.getSignatureSetId() %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" 
											selected="<%= scanAdvancedSetting.getSelectedwebsignature().equals(signature.getSignatureSetId()) %>">
											<liferay-ui:message key="<%= signature.getSignatureSetName() %>" />
										</aui:option>
								<% 	} else { %>
										<aui:option value="<%= signature.getSignatureSetId() %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">
											<liferay-ui:message key="<%= signature.getSignatureSetName() %>" />
										</aui:option>
								<% } 
								} 
							 } %>
						</aui:select>
					</td>	
				</tr>

				<tr class="input-row">
					<td><span style="text-decoration: underline;"><liferay-ui:message key="label-server-inspection-settings" /></span></td>
				</tr>

				<tr class="input-row">
					<td><span>Server Files</span></td>
					<td>
						<div class="form-group input-text-wrapper" style="margin-left: 20px;">
							<div style="display:inline-block;margin-right: 10px;"> 
								<label style="vertical-align: bottom;font-weight:normal;">ON : </label>
								<aui:input type="radio" name="serverFiles" value="1" style="vertical-align: top;" 
									checked="<%= (null == scanAdvancedSetting || scanAdvancedSetting.getGetserverfiles() == 1) %>" label="" />
							</div>
							<div style="display:inline-block"> 
								<label style="vertical-align: bottom;font-weight:normal;">OFF : </label>
								<aui:input type="radio" name="serverFiles" value="0" style="vertical-align: top;" 
									checked="<%= (null != scanAdvancedSetting && scanAdvancedSetting.getGetserverfiles() == 0) %>" label="" />
							</div>
						</div>
					</td>
				</tr>

				<tr class="input-row">
					<td><span>Server Settings</span></td>
					<td>
						<div class="form-group input-text-wrapper" style="margin-left: 20px;">
							<div style="display:inline-block;margin-right: 10px;"> 
								<label style="vertical-align: bottom;font-weight:normal;">ON : </label>
								<aui:input type="radio" name="serverSettings" value="1" style="vertical-align: top;" 
									checked="<%= (null == scanAdvancedSetting || scanAdvancedSetting.getGetserversettings() == 1) %>" label="" />
							</div>
							<div style="display:inline-block"> 
								<label style="vertical-align: bottom;font-weight:normal;">OFF : </label>
								<aui:input type="radio" name="serverSettings" value="0" style="vertical-align: top;" 
									checked="<%= (null != scanAdvancedSetting && scanAdvancedSetting.getGetserversettings() == 0) %>" label="" />
							</div>
						</div>
					</td>
				</tr>

				<tr class="input-row">
					<td><span style="text-decoration: underline;"><liferay-ui:message key="label-server-files-signature-set" /></span></td>
				</tr>

				<tr class="input-row">
					<td colspan="4">
						<aui:select name="serverFilesSignatureSetGroup" style="width: auto !important;" label="">
							<% 
								if (!CommonUtil.isListNullOrEmpty(serverFilesSignatureSetList) && serverFilesSignatureSetList.size() > 0) {
									for (ApiSignatureSetInfo signature : serverFilesSignatureSetList) {  
										if (null != scanAdvancedSetting) {
							%>
										<aui:option value="<%= signature.getSignatureSetId() %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" 
										selected="<%= scanAdvancedSetting.getSelectedserverfilessignature().equals(signature.getSignatureSetId()) %>">
											<liferay-ui:message key="<%= signature.getSignatureSetName() %>" />
										</aui:option>
									<% 	} else { %>
										<aui:option value="<%= signature.getSignatureSetId() %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">
											<liferay-ui:message key="<%= signature.getSignatureSetName() %>" />
										</aui:option>
									<% 	} %>
								<% } %>
							<% } %>
						</aui:select>
					</td>	
				</tr>

				<tr class="input-row">
					<td><span style="text-decoration: underline;"><liferay-ui:message key="label-server-settings-signature-set" /></span></td>
				</tr>

				<tr class="input-row">
					<td colspan="4">
						<aui:select name="serverSettingsSignatureSetGroup" style="width: auto !important;" label="">
							<% 
								if (!CommonUtil.isListNullOrEmpty(serverSettingsSignatureSetList) && serverSettingsSignatureSetList.size() > 0) {
									for (ApiSignatureSetInfo signature : serverSettingsSignatureSetList) {  
										if (null != scanAdvancedSetting) {
							%>
										<aui:option value="<%= signature.getSignatureSetId() %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" 
											selected="<%= scanAdvancedSetting.getSelectedserversettingssignature().equals(signature.getSignatureSetId()) %>">
											<liferay-ui:message key="<%= signature.getSignatureSetName() %>" />
										</aui:option>
									<% 	} else { %>
										<aui:option value="<%= signature.getSignatureSetId() %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">
											<liferay-ui:message key="<%= signature.getSignatureSetName() %>" />
										</aui:option>
									<% 	} %>
								<% } %>
							<% } %>
						</aui:select>
					</td>	
				</tr> --%>
			</tbody>
		</table>
		<br>
		<div>
			<button type="submit" class="btn" style="margin-right: 30px;"><liferay-ui:message key="button-next-page" /></button>
			<%-- <a onclick="submitConfirmFormAdvancedSetting()" style="margin-right: 30px;"><span class="btn"><liferay-ui:message key="button-next-page" /></span></a> --%>
		<%-- 	<a onclick="submitProjectForm()" style="margin-right: 30px;"><span class="btn" style="width:auto;"><liferay-ui:message key="button-next-page" /></span></a> --%>
			<a href="<%= cancelURL %>"><span class="btn cancel-btn"><liferay-ui:message key="button-cancel" /></span></a>
		</div>
		 <div id="confirm">
	         <table id ="confirmBoxTable">
	         	<tr>
	         		 <div class="hostMessage">Host name.</div>
	         	</tr>
	         	<tr>
	         		<td>
	         		   <img id="confirmBoxImage" src = "/o/ubsecure-theme/images/common/warning.png" width="35" height="35"/>
	         		</td>
	         		<td>
	         		  <div class="message">This is a warning message.</div>
	         		</td>
	         	</tr>
	         	<tr>
	         		<td></td>
	         		<td>
	         			<div class="confirmButtons">
	         				<button type="submit">OK</button>
        	 				<button type='button' class="no" onclick="hideConfirmBox();"><liferay-ui:message key="button-cancel" /></button>
        	 				<%-- <a onclick="hideConfirmBox();"><span class="btn cancel-btn" ><liferay-ui:message key="button-cancel" /></span></a> --%>
        	 			</div>
	         		</td>
	         	</tr>
	         </table>
	         
	        
     	 </div>
	</form>
</div>
<%
	} else {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="message-no-access-rights" />
		</div>
		<%
	}
%>
<style>
#_jp_ubsecure_portal_jubjub_portlet_VexPortlet_endTime{
	margin-left: 80px;
}
#_jp_ubsecure_portal_jubjub_portlet_VexPortlet_startTime{
	margin-left: 80px;
}

</style>
<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/vex/register_scan_simple_setting.js"></script>
<script>
	define.amd = define._amd;
</script>