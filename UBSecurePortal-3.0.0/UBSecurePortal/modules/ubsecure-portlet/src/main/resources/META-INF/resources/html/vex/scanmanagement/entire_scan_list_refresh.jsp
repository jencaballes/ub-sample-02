<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.TimeZone" %>

<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="javax.portlet.PortletURL" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResultItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@page import="jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil"%>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >


<portlet:defineObjects />

<%
// Data from resourceRequest
List<ResultItem> scanList = (List<ResultItem>) resourceRequest.getAttribute(PortalConstants.PARAM_SCAN_LIST);
Map<String, Object> searchedScan = (Map<String, Object>) resourceRequest.getAttribute(PortalConstants.PARAM_SCAN);
// End

// Pagination variables
PortletURL paginationURL = resourceResponse.createRenderURL();
paginationURL.setParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_ENTIRE_SCAN_LIST_JSP);
// End

Object oCxDoesNotExistErrorMsg = resourceRequest.getAttribute(PortalConstants.PARAM_CX_DOES_NOT_EXIST_ERROR_MSG);
String strCxDoesNotExistErrorMsg = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oCxDoesNotExistErrorMsg)) {
	strCxDoesNotExistErrorMsg = oCxDoesNotExistErrorMsg.toString();

	if (!strCxDoesNotExistErrorMsg.equals(PortalConstants.CX_REGENERATE_REPORT_FAILED)) {
		strCxDoesNotExistErrorMsg = PortalConstants.CX_REGENERATE_REPORT_FAILED + "(" + strCxDoesNotExistErrorMsg + ")";
	}
}

//Sorting variables
String orderByCol = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_COL);
String orderByType = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_TYPE);

PortletSession pSession = resourceRequest.getPortletSession();

if (orderByType == null || orderByType.isEmpty()) {
	Object oOrderByType = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
	
	if (oOrderByType != null) {
		orderByType = oOrderByType.toString();
		
		if (orderByType.isEmpty()) {
			orderByType = PortalConstants.SORT_ASCENDING;
		}
	} else {
		orderByType = PortalConstants.SORT_ASCENDING;
	}
	
	Object oOrderByCol = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_COL);
	
	if (oOrderByCol != null) {
		orderByCol = oOrderByCol.toString();
		
		if (!orderByCol.isEmpty()) {
			pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
		}
	}
} else {
	pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_TYPE, orderByType);
	pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
}

int totalSize = 0;

if (!CommonUtil.isListNullOrEmpty(scanList)) {
	totalSize = ControllerHelper.getEntireScansCount(ProjectType.VEX.getInteger(), searchedScan);
}
//End
%>

<input id="orderByCol" type="hidden" value="<%= orderByCol %>"/>
<input id="orderByType" type="hidden" value="<%= HtmlUtil.escape(orderByType) %>"/>
<input id="screenList" type="hidden" value="entire_scan_list" />
<input id="screenNo" type="hidden" value="<%= PortalConstants.SCREEN_ENTIRE_SCAN_LIST %>" />
<input type="hidden" name="cxErrorMessage" id="cxErrorMessage" value="<%= HtmlUtil.escape(strCxDoesNotExistErrorMsg) %>" />

<liferay-ui:search-container emptyResultsMessage="message-no-scans-to-display" orderByType="<%= HtmlUtil.escape(orderByType) %>" orderByCol="<%= orderByCol %>" iteratorURL="<%=paginationURL%>" total="<%= totalSize %>">
	<liferay-ui:search-container-results>
		<%
			int curPage = searchContainer.getCur();	
			int start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
			int end = curPage * PortalConstants.PAGINATION_DELTA;
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				int size = scanList.size();
				
				if (size == 0 && !CommonUtil.isListNullOrEmpty(scanList)) {
					--curPage;
					start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
					end = curPage * PortalConstants.PAGINATION_DELTA;
					request.setAttribute("index-overlap", "");
				}
			
				results = scanList;
			
				pageContext.setAttribute("results", results);
			}
			
			try {
				Integer.parseInt(searchContainer.getCurParam());
				
				paginationURL.setParameter("cur", searchContainer.getCurParam());
			} catch (NumberFormatException e) {
				
			}
		%>
	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row
		className="jp.ubsecure.portal.jubjub.portlet.model.ResultItem"
		keyProperty="scanId" modelVar="scan" escapedModel="<%=true%>">
		<%
			String strScanId = String.format("%05d", scan.getScanId());
			
		%>
		<liferay-ui:search-container-column-text
			name="label-id"
			orderable="<%= true %>"
			orderableProperty="scanId"
			value="<%= strScanId %>" />
		
		<liferay-ui:search-container-column-text
			name="label-project-name"
			orderable="<%= true %>"
			orderableProperty="projectName"
			value="<%= HtmlUtil.escapeAttribute(scan.getProjectName()) %>" />
		
		<liferay-ui:search-container-column-text
			name="label-group-name"
			orderable="<%= true %>"
			orderableProperty="groupName"
			value="<%= HtmlUtil.escapeAttribute(scan.getGroupName()) %>" />

		<liferay-ui:search-container-column-text
			name="label-scan-name"
			orderable="<%= true %>"
			orderableProperty="fileName"
			value="<%=HtmlUtil.escapeAttribute(scan.getFileName())%>" />

		<%
		String username = scan.getScanManager();
		%>

		<liferay-ui:search-container-column-text
			name="label-scan-manager"
			orderable="<%= true %>"
			orderableProperty="scanManager"
			value="<%=HtmlUtil.escapeAttribute(username)%>" />

		<%
			DateFormat formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
			formatter.setTimeZone(TimeZone.getTimeZone(PortalConstants.TIMEZONE_TOKYO));
			
			String date = formatter.format(scan.getScanRegistrationDate());
		%>

		<liferay-ui:search-container-column-text
			name="label-scan-registration-date"
			orderable="<%= true %>"
			orderableProperty="scanRegistrationDate"
			value="<%=date%>" />

		
		<%
				int iImplementation = scan.getImplementationEnvironment();
							String strImplementation = LanguageUtil.get(request,
									PortalConstants.KEY_SCAN_PRODUCTION_ENVIRONMENT);
							if (iImplementation == 1) {
								strImplementation = LanguageUtil.get(request,
										PortalConstants.KEY_SCAN_VERIFICATION_ENVIRONMENT);
							}
							
				String stringUrls = scan.getScanStartURL();
				stringUrls = HtmlUtil.escapeAttribute(stringUrls.replaceAll(";"," ; "));
				stringUrls = stringUrls.replaceAll("&#x20;&#x3b;&#x20;","<br/>");
			
			%>
			<liferay-ui:search-container-column-text
				name="label-implementation-environment" orderable="<%=true%>"
				orderableProperty="implementationEnvironment"
				value="<%=HtmlUtil.escapeAttribute(strImplementation)%>" />
				
			<liferay-ui:search-container-column-text name="label-scan-start-url"
				orderable="<%=true%>" orderableProperty="startUrl"
				value="<%=HtmlUtil.escapeAttribute(stringUrls)%>" />
			<%
				DateFormat startEndTimeFormat = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
				String startTime = PortalConstants.STRING_EMPTY;
							if (!CommonUtil.isObjectNull(scan.getScanStartTime())) {
								startTime = startEndTimeFormat.format(scan.getScanStartTime());
							}
			%>
			<liferay-ui:search-container-column-text name="label-start-time"
				orderable="<%=true%>" orderableProperty="startTime"
				value="<%=startTime%>" />
			<%
				String endTime = PortalConstants.STRING_EMPTY;
							if (!CommonUtil.isObjectNull(scan.getScanEndTime())) {
								endTime = startEndTimeFormat.format(scan.getScanEndTime());
							}
			%>
			<liferay-ui:search-container-column-text name="label-end-time"
				orderable="<%=true%>" orderableProperty="endTime"
				value="<%=endTime%>" />
			<%
				int iStatus = scan.getStatus();
				String strStatus = PortalConstants.STRING_EMPTY;
				String strCause = PortalConstants.STRING_EMPTY;
				int iWaitingCount = PortalConstants.INT_ZERO;
				int iCrawlWaitingCount =  PortalConstants.INT_ZERO;
				
				if (PortletCommonUtil.isDBConnected()) {
					iWaitingCount = ScanLocalServiceUtil.countScanWaiting(scan.getScanRegistrationDate(),
							ProjectType.VEX.getInteger());
				}

				if (iStatus == ScanStatus.SCAN_WAITING.getInteger()) {
					strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_WAITING);

					if (iWaitingCount > 0) {
						strStatus += LanguageUtil.format(request,
								PortalConstants.KEY_SCAN_STATUS_WAITING_PRIORITY_NUMBER, iWaitingCount, false);
						iCrawlWaitingCount = ScanLocalServiceUtil.countCrawlWaiting(scan.getScanRegistrationDate(),
								ProjectType.VEX.getInteger());
					}
				} else  {
					switch(iStatus){
						case PortalConstants.SCANNING:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_ONGOING);
						break;
						
						case PortalConstants.REPORT_MAKING:
						case PortalConstants.REGENERATING:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_REPORT_MAKING);
						break;
						
						case PortalConstants.COMPLETE:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_COMPLETE);
						break;
						
						case PortalConstants.FAILURE:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_FAILURE);
							strCause = scan.getFailedScanCause();
						break;
						
						case PortalConstants.CRAWLING_WAITING:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_WAITING);
							if (iCrawlWaitingCount > 0) {
								strStatus += LanguageUtil.format(request,
										PortalConstants.KEY_CRAWLING_STATUS_WAITING_PRIORITY_NUMBER, iCrawlWaitingCount, false);
							} 
						break;
						
						case PortalConstants.CRAWLING:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_ONGOING);
						break;
						
						case PortalConstants.CRAWLING_INTERRUPTED:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_INTERRUPTED);
						break;
						
						case PortalConstants.CRAWLING_FAILURE:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_FAILURE);
						break;
						
						case PortalConstants.SCAN_INTERRUPTED:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_INTERRUPTED);
						break;
						
						case PortalConstants.CRAWLING_COMPLETED:
							strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_COMPLETED);
						break;
				}
			
			}
				
			%>
			
		<liferay-ui:search-container-column-text
			name="label-status"
			orderable="<%= true %>"
			orderableProperty="status" >
			<liferay-ui:message key="<%= strStatus %>" />
	
				<%
					/* Modified this block of code for SWP-156.
					if ((iStatus == ScanStatus.FAILURE.getInteger() || iStatus == ScanStatus.CRAWLING_FAILURE.getInteger()) && !CommonUtil.isStringNullOrEmpty(strCause)) { */
					if ((iStatus == ScanStatus.FAILURE.getInteger() || iStatus == ScanStatus.CRAWLING_FAILURE.getInteger())) {
						%>
						<%-- <img src="<%= PortalConstants.FAILED_INFO_ICON_PATH %>" title="<%= strCause %>" /> --%>
						<img src="<%= PortalConstants.FAILED_INFO_ICON_PATH %>" />
						<%
					}
				%>
		</liferay-ui:search-container-column-text>
			<liferay-ui:search-container-column-jsp name="label-screen-transition-diagram" align="right" path="<%=PortalConstants.VEX_DOWNLOAD_REFERENCE_JSP%>" />
			<liferay-ui:search-container-column-jsp name="label-report-docx" align="right" path="<%=PortalConstants.VEX_DOWNLOAD_JSP%>" />
			<liferay-ui:search-container-column-jsp name="button-action" align="right" path="<%=PortalConstants.VEX_ENTIRE_SCAN_ACTION_JSP%>" />
		</liferay-ui:search-container-row>
	<liferay-ui:search-iterator />
</liferay-ui:search-container>