/*** Vex */

function submitFormGetParams() {
	var portletNamespace = $("#portletNamespace").val();
	var loginFormPath = document.getElementById(portletNamespace+"loginformpath").value.trim();
	if(loginFormPath == ""){
		alert("Please enter login form path.");
	}else{
		var msg = "対象のURLにアクセスします。\n\nよろしいですか？";//"Are you sure to access target URL?";
			
		if (confirm(msg)) {
			if(loginFormPath.charAt(0) != "/"){
				document.getElementById(portletNamespace+"loginformpath").value = "/" + loginFormPath;
			}else{
				document.getElementById(portletNamespace+"loginformpath").value = loginFormPath;
			}
				document.getElementById("fmGetLoginInfo").submit();
		} else {
			// do nothing	
		}
	}	
}

function addLoginForm() {
	$("#loginParamValueInvalidError").hide();
	$("#unusableCharacterInputError").hide();
	var portletNamespace = $("#portletNamespace").val();
	var parentForm = document.getElementById("parentLoginForm");
	var table = document.getElementById("container");
	var lastRowItemIndex = table.rows.length-1;
	var rowCount = table.rows.length;

	// Form Container -- clones on last form -- 
	var childForm = table.rows[lastRowItemIndex].cells[0].getElementsByTagName('div')[0];
	var childFormId = table.rows[lastRowItemIndex].cells[0].getElementsByTagName('div')[0].getAttribute('id');

	var clone = childForm.cloneNode(true);
	var numIndex = childFormId.replace( /^\D+/g, ''); 

	var row = table.insertRow(rowCount);

	var cell1 = row.insertCell(0);
	clone.id = "childForm"+(++numIndex);

	// Parameters Table Form
	var tableForm = document.getElementById("inputFormFieldTable");
	var tableFormRows = tableForm.rows.length-1; // the number of parameters from getLoginInfo
	var paramIndex = document.getElementById("totalParams").value-1;  //Get total number of parameters as index - 1
	var newtotalParams = parseInt(document.getElementById("totalParams").value)+parseInt(tableFormRows); //Expected total number of parameters with clone
	document.getElementById("totalParams").value = newtotalParams;

	var newParamIndex = newtotalParams-1; //Get expected total number of parameters with clone as index - 1
	var isLastFormDeleted = false;

	for (var i = 0, index = paramIndex; i < tableForm.rows.length-1; i++, index--) { 
         if(isLastFormDeleted == true){ 
         	    i = 0; 
         	    //get the latest existing paramIndex
         	    var allInput = clone.querySelectorAll("input");
         	    var lastRowID = allInput[allInput.length-1].getAttribute('id');
         	    var index = lastRowID.replace( /^\D+/g, ''); 
         	    isLastFormDeleted = false;
         }
        
    	var labelIDParameterName = '[id=' + portletNamespace + 'parameterName' + index +']';
		var labelIDCheckItem = '[id=checkItem' + index + ']';
		var labelNameCheckItem = '[name=' + portletNamespace + 'checkItem' + index + ']';
		var labelIDParameterValue = '[id=' + portletNamespace + 'parameterValue' + index + ']';
		var labelNameParameterName = '[name=' + portletNamespace + 'parameterName' + index +']';
		var labelNameParameterValue = '[name=' + portletNamespace + 'parameterValue' + index + ']';

		try{
			clone.querySelectorAll(labelIDParameterName)[0].id = portletNamespace+"parameterName"+newParamIndex;
			clone.querySelectorAll(labelNameParameterName)[0].name = portletNamespace+"parameterName"+newParamIndex;
			clone.querySelectorAll(labelIDCheckItem)[0].id = "checkItem"+newParamIndex;
			clone.querySelectorAll(labelNameCheckItem)[0].name = portletNamespace+"checkItem"+newParamIndex;
			clone.querySelectorAll(labelIDParameterValue)[0].id = portletNamespace+"parameterValue"+newParamIndex;
			clone.querySelectorAll(labelNameParameterValue)[0].name = portletNamespace+"parameterValue"+newParamIndex;
			newParamIndex--;
		}catch(err){
			// fix when last form is deleted (cloning fix)
            isLastFormDeleted = true;
            continue;
		}
	}

	lastRowItemIndex++;
	var newClone = clone;

	cell1.appendChild(newClone);
}

function deleteRow(button) {
	$("#loginParamValueInvalidError").hide();
	$("#unusableCharacterInputError").hide();
	var portletNamespace = $("#portletNamespace").val();
	var table = document.getElementById("container");
	var tableForm = document.getElementById("inputFormFieldTable");
    var rowCount = table.rows.length;

	var i = button.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex; // table: container rowIndex

	if(i != 0 || rowCount != 1)
		table.deleteRow(i);
}

function disableField(checkbox) {
	var portletNamespace = $("#portletNamespace").val();
	var numIndex = checkbox.getAttribute('id').replace( /^\D+/g, ''); 
	var inputNameFieldId = "#"+portletNamespace+"parameterName"+numIndex;
	var inputValueFieldId = "#"+portletNamespace+"parameterValue"+numIndex;

	if(checkbox.value == 0){
		checkbox.value = 1;
		$(inputNameFieldId).prop("readonly", "");
		$(inputValueFieldId).prop("readonly", "");
		$(checkbox).prop("checked", true);
	}
	else{
		checkbox.value = 0;
		$(inputNameFieldId).prop("readonly", "readonly");
		$(inputValueFieldId).prop("readonly", "readonly");
		$(checkbox).prop("checked", false);
	}
}

function cancelAddUpdateScan () {
	$("#saveHostList").prop("disabled", true);
	$("#cancelBtn").addClass("disabled");
	$("#saveHostList").removeAttr("onclick");
}

function isBrowserIE () {
	var browser = navigator.userAgent;
	
	return (browser.indexOf("MSIE", 0) > -1 || (browser.indexOf("Firefox", 0) == -1 && browser.indexOf("Chrome", 0) == -1));
}

