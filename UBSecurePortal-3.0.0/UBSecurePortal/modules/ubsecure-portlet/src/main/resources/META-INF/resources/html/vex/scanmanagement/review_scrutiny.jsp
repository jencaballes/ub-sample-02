<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResultItem"%>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >

<portlet:defineObjects />

<!-- Vex -->

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	VexDetectionResultItem detectionResultItem = (VexDetectionResultItem) row.getObject();
	
	long scanId = detectionResultItem.getScanid();
	long number = detectionResultItem.getScanresultid();
	
	HttpSession httpSession = null;

	if (renderRequest != null) {
		httpSession = ControllerHelper.getHttpSession(renderRequest);
	} else if (resourceRequest != null) {
		httpSession = ControllerHelper.getHttpSession(resourceRequest);
	} else {
		httpSession = request.getSession();
	}
	
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	int iUserRole = PortalConstants.INT_ZERO;

	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	
	String inputName = PortalConstants.PARAM_DETECTION_REVIEW_COMMENT + "_" + number;
%>
	
	<%-- <aui:input type="text" name="<%=inputName%>" id="<%=inputName%>" label="" value="" style="display:none;" /> --%>
	<% 
	
	if (scanId > PortalConstants.LONG_ZERO) { %>
		
		<%-- if(detectionResultItem.getDetectionjudgment().equals(PortalConstants.STR_DETECTION_JUDGEMENT_OVER_DETECTION)){%> --%>
			<span><%=detectionResultItem.getReviewcomment()%></span>
		<%-- <% }%> --%>
			
	<%}%>