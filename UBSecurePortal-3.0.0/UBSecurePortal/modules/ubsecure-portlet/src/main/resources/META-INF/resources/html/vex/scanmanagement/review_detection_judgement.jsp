<%@page import="jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Scan" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResultItem"%>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >

<portlet:defineObjects />

<!-- Vex -->

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	VexDetectionResultItem detectionResultItem = (VexDetectionResultItem) row.getObject();
	
	long scanId = detectionResultItem.getScanid();
	String detectionResultId = detectionResultItem.getDetectionresultid();
	long number = detectionResultItem.getScanresultid();
	Scan scan = ScanLocalServiceUtil.getScan(scanId);
	Project project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
	
	HttpSession httpSession = null;

	if (renderRequest != null) {
		httpSession = ControllerHelper.getHttpSession(renderRequest);
	} else if (resourceRequest != null) {
		httpSession = ControllerHelper.getHttpSession(resourceRequest);
	} else {
		httpSession = request.getSession();
	}
	
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	int iUserRole = PortalConstants.INT_ZERO;

	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
%>

	<% 
	
	if (scanId > PortalConstants.LONG_ZERO) { %>
		<portlet:actionURL name="confirmDetectionResult" var="confirmDetectionResultURL">
			<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
		    <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
		    <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_DETECTION_RESULT_REVIEW) %>" />
		    <portlet:param name="confirmNumber" value="<%= String.valueOf(number) %>" />
		    <portlet:param name="confirmDetectionresultid" value="<%= String.valueOf(detectionResultId) %>" />
		    <portlet:param name="fileName" value="<%= String.valueOf(scan.getFileName()) %>" />
		</portlet:actionURL>
	
	<%
		boolean isDetection = false;
		boolean isOverDetection = false;
		if(detectionResultItem.getDetectionjudgment().equals(PortalConstants.STR_DETECTION_JUDGEMENT_OVER_DETECTION)){
			isOverDetection = true;
		}else{
			isDetection = true;
		}
		String selectorName = PortalConstants.PARAM_DETECTION_JUDGEMENT +"_" + number;
		String enableReviewComment = "enableReviewComment(this,'"+confirmDetectionResultURL.toString()+"')";
	%>
		<aui:select name="<%=selectorName %>" style="width: auto !important;" label="" onChange="<%=enableReviewComment%>" >
			<aui:option value="0" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="<%= isDetection %>" >
				<liferay-ui:message key="<%= PortalConstants.STR_DETECTION_JUDGEMENT %>" />
			</aui:option>
		
			<aui:option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="<%= isOverDetection %>" >
				<liferay-ui:message key="<%= PortalConstants.STR_DETECTION_JUDGEMENT_OVER_DETECTION %>" />
			</aui:option>
		</aui:select>
	<%}%>
		


