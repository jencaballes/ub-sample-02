<%@page import="java.text.ParseException"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.liferay.portal.kernel.model.User"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="java.util.Date"%>
<%@ page import="javax.portlet.PortletSession"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResponseModel"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil"%>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >
<portlet:defineObjects />
<!-- Vex -->
<%
// Data from session
HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
httpSession.setAttribute("Current_screen", "VEX");
Object downloadFrom = httpSession.getAttribute("download_from");
String strDownloadFrom = PortalConstants.STRING_EMPTY;
Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
long lUserId = PortalConstants.LONG_ZERO;
Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
int iUserRole = PortalConstants.INT_ZERO;
Object oUseVEX = httpSession.getAttribute(PortalConstants.USE_VEX);
boolean bUseVEX = false;
boolean bHasSearchInput = false;

if (!CommonUtil.isObjectNull(oUserId)) {
	lUserId = Long.parseLong(oUserId.toString());
}

if (!CommonUtil.isObjectNull(oUserRole)) {
	iUserRole = Integer.parseInt(oUserRole.toString());
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
		bUseVEX = true;
	} else {
		if (!CommonUtil.isObjectNull(oUseVEX)) {
			bUseVEX = Boolean.parseBoolean(oUseVEX.toString());
		}
	}
}
// End

// Data from renderRequest
PortletSession pSession = renderRequest.getPortletSession();
Object oProjectId = pSession.getAttribute(PortalConstants.PARAM_PROJECT_ID);
long lProjectId = PortalConstants.LONG_ZERO;
Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
String strCxServerErrorMsg = PortalConstants.STRING_EMPTY;
Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
boolean bIsFromSearch = false;
Object oUserAction = pSession.getAttribute(PortalConstants.PARAM_USER_ACTION);
String strUserAction = PortalConstants.STRING_EMPTY;
Object oScanId = pSession.getAttribute(PortalConstants.PARAM_SCAN_ID);
String strScanId = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oProjectId)) {
	lProjectId = Long.parseLong(oProjectId.toString());
}

if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
	strCxServerErrorMsg = oCxServerErrorMsg.toString();
}

if (!CommonUtil.isObjectNull(oIsFromSearch)) {
	bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
	
	if (bIsFromSearch) {
		httpSession.setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
	}
}

if (!CommonUtil.isObjectNull(oUserAction)) {
	strUserAction = oUserAction.toString();
}

if (!CommonUtil.isObjectNull(oScanId)) {
	strScanId = oScanId.toString();
}

Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
	strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
}

pSession.removeAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
// End

// Show error indicators
boolean bShowDBConnError = false;
boolean bShowPaginationError = false;
// End

//Previous and current screen
Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
String strCurrScreen = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oCurrScreen)) {
	strCurrScreen = oCurrScreen.toString();
	
	if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
		pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
	}
}

pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_LIST);
// End

// Search scan attributes
Map<String, Object> searchedScan = (Map<String, Object>) pSession.getAttribute(PortalConstants.PARAM_SCAN);
String strSearchedScanId = PortalConstants.STRING_EMPTY;
String strSearchedFileName = PortalConstants.STRING_EMPTY;
String strSearchedHashValue = PortalConstants.STRING_EMPTY;
String strSearchedScanManager = PortalConstants.STRING_EMPTY;
String strSearchedScanRegDateLow = PortalConstants.STRING_EMPTY;
String strSearchedScanRegDateHigh = PortalConstants.STRING_EMPTY;
String strSearchedStatus = PortalConstants.STRING_EMPTY;
String strSearchedCxScanId = PortalConstants.STRING_EMPTY;
String strSearchedImplementationEnvironment = PortalConstants.STRING_EMPTY;;
String strSearchedStartUrl = PortalConstants.STRING_EMPTY;
String strSearchedStartTime = PortalConstants.STRING_EMPTY;
String strSearchedEndTime = PortalConstants.STRING_EMPTY;
Calendar dteSearchedScanStartTime = null;
Calendar dteSearchedScanEndTime = null;
boolean bProductionSelected = false;
boolean bVerificationSelected = false;
boolean bCrawlWaitingSelected = false;
boolean bCrawlingSelected = false;
boolean bCrawlingInterruptedSelected = false;
boolean bCrawlingFailedSelected = false;
boolean bScanWaitingSelected = false;
boolean bScanningSelected = false;
boolean bScanningInterruptedSelected = false;
boolean bReportMakingSelected = false;
boolean bCompleteSelected = false;
boolean bFailureSelected = false;
boolean bCrawlingCompletedSelected = false;
Calendar dteSearchedScanRegDateLow = null;
Calendar dteSearchedScanRegDateHigh = null;
Map<String, Object> searchedScanWithError = (Map<String, Object>) httpSession.getAttribute(PortalConstants.PARAM_SCAN);
String strSearchInfo = PortalConstants.STRING_EMPTY;
String strSearchInfoConnector = PortalConstants.STRING_EMPTY;
String strSearchInfoComma = LanguageUtil.get(request, PortalConstants.KEY_FILTER_LIST_COMMA);

if (bIsFromSearch && !CommonUtil.isMapNullOrEmpty(searchedScan)) {
	Object oSearchedScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
	Object oSearchedFileName = searchedScan.get(PortalConstants.PARAM_FILE_NAME);
	Object oSearchedHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
	Object oSearchedScanManager = searchedScan.get(PortalConstants.PARAM_SCAN_MANAGER);
	Object oSearchedScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
	Object oSearchedScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
	Object oSearchedStatus = searchedScan.get(PortalConstants.PARAM_STATUS);
	Object oSearchedCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
	Object oSearchedStartUrl = searchedScan.get(PortalConstants.PARAM_SCAN_START_URL);
	Object oSearchedImplementationEnvironment = searchedScan.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
	Object oSearchedScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
	Object oSearchedScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
	DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
	DateFormat datetimeFormat = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
	
	if (!CommonUtil.isObjectNull(oSearchedScanId)) {
		strSearchedScanId = String.valueOf(oSearchedScanId);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedScanId)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_SCAN_ID, strSearchedScanId, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedFileName)) {
		strSearchedFileName = String.valueOf(oSearchedFileName);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedFileName)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_FILENAME, strSearchedFileName, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedHashValue)) {
		strSearchedHashValue = String.valueOf(oSearchedHashValue);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedHashValue)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_HASH_VALUE, strSearchedHashValue, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedScanManager)) {
		strSearchedScanManager = String.valueOf(oSearchedScanManager);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedScanManager)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_SCAN_MANAGER, strSearchedScanManager, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedScanRegDateLow)) {
		strSearchedScanRegDateLow = String.valueOf(oSearchedScanRegDateLow);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateLow)
				&& !strSearchedScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
			dteSearchedScanRegDateLow = Calendar.getInstance();
			dteSearchedScanRegDateLow.setTime(format.parse(strSearchedScanRegDateLow));
		}
			
	}
	
	if (!CommonUtil.isObjectNull(oSearchedScanRegDateHigh)) {
		strSearchedScanRegDateHigh = String.valueOf(oSearchedScanRegDateHigh);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateHigh)
				&& !strSearchedScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
			dteSearchedScanRegDateHigh = Calendar.getInstance();
			dteSearchedScanRegDateHigh.setTime(format.parse(strSearchedScanRegDateHigh));
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedImplementationEnvironment)) {
		strSearchedImplementationEnvironment = oSearchedImplementationEnvironment.toString(); 
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedImplementationEnvironment)) {
			String strEnvironment = "検証環境";
			if(strSearchedImplementationEnvironment.equals("1")){
				bVerificationSelected = true;
			} else {
				strEnvironment = "本番環境";
				bProductionSelected = true;
			}
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_IMPLEMENTATION_ENVIRONMENT, strEnvironment, false);
			
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedStartUrl)) {
		strSearchedStartUrl = String.valueOf(oSearchedStartUrl);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedStartUrl)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_START_URL, strSearchedStartUrl, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedScanStartTime)) {
		strSearchedStartTime= String.valueOf(oSearchedScanStartTime);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedStartTime) && !strSearchedStartTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
			dteSearchedScanStartTime = Calendar.getInstance();
			try{
			dteSearchedScanStartTime.setTime(datetimeFormat.parse(strSearchedStartTime));
			
			}catch (ParseException pe) {
				//do nothing
			}
		}
	}
	
	if (!CommonUtil.isStringNullOrEmpty(strSearchedStartTime)) {
		bHasSearchInput = true;
		strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_STARTTIME, strSearchedStartTime, false);
		strSearchInfoConnector = strSearchInfoComma;
	}
	
	if (!CommonUtil.isObjectNull(oSearchedScanEndTime)) {
		strSearchedEndTime= String.valueOf(oSearchedScanEndTime);
		if (!CommonUtil.isStringNullOrEmpty(strSearchedEndTime) && !strSearchedEndTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
			dteSearchedScanEndTime = Calendar.getInstance();
			try{
			dteSearchedScanEndTime.setTime(datetimeFormat.parse(strSearchedEndTime));
			
			}catch (ParseException pe) {
				//do nothing
			}
		}
	} 
	
	if (!CommonUtil.isStringNullOrEmpty(strSearchedEndTime)) {
		bHasSearchInput = true;
		strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_ENDTIME, strSearchedEndTime, false);
		strSearchInfoConnector = strSearchInfoComma;
	} 
	
	
	if (!CommonUtil.isObjectNull(oSearchedScanRegDateHigh)) {
		strSearchedScanRegDateHigh = String.valueOf(oSearchedScanRegDateHigh);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateHigh)
				&& !strSearchedScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
			dteSearchedScanRegDateHigh = Calendar.getInstance();
			dteSearchedScanRegDateHigh.setTime(format.parse(strSearchedScanRegDateHigh));
		}
	}
	
	if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateLow)
			|| !CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateHigh)) {
		bHasSearchInput = true;
		strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_SCAN_REGISTRATION_DATE, strSearchedScanRegDateLow + PortalConstants.STRING_TILDE + strSearchedScanRegDateHigh, false);
		strSearchInfoConnector = strSearchInfoComma;
	}
	
	if (!CommonUtil.isObjectNull(oSearchedStatus)) {
		strSearchedStatus = oSearchedStatus.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedStatus)) {
			String [] strSearchedStatusArr = strSearchedStatus.split(PortalConstants.COMMA);

			if (!CommonUtil.isObjectNull(strSearchedStatusArr)) {
				bHasSearchInput = true;
				
				String strSearchStatus = PortalConstants.STRING_EMPTY;
				String strInfoConnector = PortalConstants.STRING_EMPTY;
				for (String strStatus : strSearchedStatusArr) {
					if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING_WAITING.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_WAITING);
						bFailureSelected = true;
					}
					else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_ONGOING);
						bFailureSelected = true;
					}
					else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING_INTERRUPTED.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_INTERRUPTED);
						bFailureSelected = true;
					}
					else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING_FAILURE.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_FAILURE);
						bFailureSelected = true;
					}
					else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.SCAN_WAITING.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_WAITING);
						bScanWaitingSelected = true;
					} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.SCANNING.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_ONGOING);
						bScanningSelected = true;
					}
					else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.SCAN_INTERRUPTED.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_INTERRUPTED);
						bFailureSelected = true;
					} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.REPORT_MAKING.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_REPORT_MAKING);
						bReportMakingSelected = true;
					} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.COMPLETE.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_COMPLETE);
						bCompleteSelected = true;
					} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.FAILURE.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_FAILURE);
						bFailureSelected = true;
					} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING_COMPLETED.getInteger()))) {
						strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_COMPLETED);
						bCrawlingCompletedSelected = true;
					}
					strInfoConnector = strSearchInfoComma;
				}
				
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_STATUS, strSearchStatus, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedCxScanId)) {
		strSearchedCxScanId = String.valueOf(oSearchedCxScanId);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedCxScanId)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_CX_SCAN_ID, strSearchedCxScanId, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
} else {
	Object oIsSearch = httpSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
	
	if (!CommonUtil.isObjectNull(oIsSearch)) {
		String strIsSearch = oIsSearch.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strIsSearch)) {
			bIsFromSearch = Boolean.parseBoolean(strIsSearch);
		}
	}
	
	if (bIsFromSearch && CommonUtil.isMapNullOrEmpty(searchedScan) && !CommonUtil.isMapNullOrEmpty(searchedScanWithError)) {
		Object oSearchedScanId = searchedScanWithError.get(PortalConstants.PARAM_SCAN_ID);
		Object oSearchedFileName = searchedScanWithError.get(PortalConstants.PARAM_FILE_NAME);
		Object oSearchedHashValue = searchedScanWithError.get(PortalConstants.PARAM_HASH_VALUE);
		Object oSearchedScanManager = searchedScanWithError.get(PortalConstants.PARAM_SCAN_MANAGER);
		Object oSearchedScanRegDateLow = searchedScanWithError.get(PortalConstants.PARAM_REG_DATE_LOW);
		Object oSearchedScanRegDateHigh = searchedScanWithError.get(PortalConstants.PARAM_REG_DATE_HIGH);
		Object oSearchedStatus = searchedScanWithError.get(PortalConstants.PARAM_STATUS);
		Object oSearchedCxScanId = searchedScanWithError.get(PortalConstants.PARAM_CX_SCAN_ID);
		Object oSearchedStartUrl = searchedScanWithError.get(PortalConstants.PARAM_SCAN_START_URL);
		Object oSearchedImplementationEnvironment = searchedScanWithError.get(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
		Object oSearchedScanStartTime= searchedScanWithError.get(PortalConstants.PARAM_SCAN_START_TIME);
		Object oSearchedScanEndTime = searchedScanWithError.get(PortalConstants.PARAM_SCAN_END_TIME);
		DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
		DateFormat datetimeFormat = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
		
		if (!CommonUtil.isObjectNull(oSearchedScanId)) {
			strSearchedScanId = String.valueOf(oSearchedScanId);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedScanId)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_SCAN_ID, strSearchedScanId, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedFileName)) {
			strSearchedFileName = String.valueOf(oSearchedFileName);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedFileName)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_FILENAME, strSearchedFileName, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedHashValue)) {
			strSearchedHashValue = String.valueOf(oSearchedHashValue);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedHashValue)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_HASH_VALUE, strSearchedHashValue, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedScanManager)) {
			strSearchedScanManager = String.valueOf(oSearchedScanManager);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedScanManager)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_SCAN_MANAGER, strSearchedScanManager, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedScanRegDateLow)) {
			strSearchedScanRegDateLow = String.valueOf(oSearchedScanRegDateLow);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateLow)
					&& !strSearchedScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				dteSearchedScanRegDateLow = Calendar.getInstance();
				try {
					dteSearchedScanRegDateLow.setTime(format.parse(strSearchedScanRegDateLow));
				} catch (ParseException pe) {
					//do nothing
				}
				
			}
				
		}
		
		if (!CommonUtil.isObjectNull(oSearchedScanRegDateHigh)) {
			strSearchedScanRegDateHigh = String.valueOf(oSearchedScanRegDateHigh);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateHigh)
					&& !strSearchedScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				dteSearchedScanRegDateHigh = Calendar.getInstance();
				try {
					dteSearchedScanRegDateHigh.setTime(format.parse(strSearchedScanRegDateHigh));
				} catch (ParseException pe) {
					//do nothing
				}
				
			}
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateLow)
				|| !CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateHigh)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_SCAN_REGISTRATION_DATE, strSearchedScanRegDateLow + PortalConstants.STRING_TILDE + strSearchedScanRegDateHigh, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
		
		if (!CommonUtil.isObjectNull(oSearchedStatus)) {
			strSearchedStatus = oSearchedStatus.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedStatus)) {
				String [] strSearchedStatusArr = strSearchedStatus.split(PortalConstants.COMMA);

				if (!CommonUtil.isObjectNull(strSearchedStatusArr)) {
					bHasSearchInput = true;
					
					String strSearchStatus = PortalConstants.STRING_EMPTY;
					String strInfoConnector = PortalConstants.STRING_EMPTY;
					for (String strStatus : strSearchedStatusArr) {
						if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING_WAITING.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_WAITING);
							bCrawlWaitingSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_ONGOING);
							bCrawlingSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING_INTERRUPTED.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_INTERRUPTED);
							bCrawlingInterruptedSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING_FAILURE.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_FAILURE);
							bCrawlingFailedSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.SCAN_WAITING.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_WAITING);
							bScanWaitingSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.SCANNING.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_ONGOING);
							bScanningSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.REPORT_MAKING.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_REPORT_MAKING);
							bReportMakingSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.COMPLETE.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_COMPLETE);
							bCompleteSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.FAILURE.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_FAILURE);
							bFailureSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.SCAN_INTERRUPTED.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_INTERRUPTED);
							bScanningInterruptedSelected = true;
						} else if (strStatus.equalsIgnoreCase(String.valueOf(ScanStatus.CRAWLING_COMPLETED.getInteger()))) {
							strSearchStatus += strInfoConnector + LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_COMPLETED);
							bCrawlingCompletedSelected = true;
						}
						
						strInfoConnector = strSearchInfoComma;
					}
					
					strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_STATUS, strSearchStatus, false);
					strSearchInfoConnector = strSearchInfoComma;
				}
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedCxScanId)) {
			strSearchedCxScanId = String.valueOf(oSearchedCxScanId);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedCxScanId)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_CX_SCAN_ID, strSearchedCxScanId, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedImplementationEnvironment)) {
			strSearchedImplementationEnvironment = oSearchedImplementationEnvironment.toString(); 
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedImplementationEnvironment)) {
				String strEnvironment = "検証環境";
				if(strSearchedImplementationEnvironment.equals("1")){
					bVerificationSelected = true;
				} else {
					strEnvironment = "本番環境";
					bProductionSelected = true;
				}
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_IMPLEMENTATION_ENVIRONMENT, strEnvironment, false);
				
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedStartUrl)) {
			strSearchedStartUrl = String.valueOf(oSearchedStartUrl);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedScanManager)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_START_URL, strSearchedStartUrl, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedScanStartTime)) {
			strSearchedStartTime= String.valueOf(oSearchedScanStartTime);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedStartTime) && !strSearchedStartTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				dteSearchedScanStartTime = Calendar.getInstance();
				try{
				dteSearchedScanStartTime.setTime(datetimeFormat.parse(strSearchedStartTime));
				
				}catch (ParseException pe) {
					//do nothing
				}
			}
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedStartTime)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_STARTTIME, strSearchedStartTime, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
		
		if (!CommonUtil.isObjectNull(oSearchedScanEndTime)) {
			strSearchedEndTime= String.valueOf(oSearchedScanEndTime);
			if (!CommonUtil.isStringNullOrEmpty(strSearchedEndTime) && !strSearchedEndTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				dteSearchedScanEndTime = Calendar.getInstance();
				try{
				dteSearchedScanEndTime.setTime(datetimeFormat.parse(strSearchedEndTime));
				
				}catch (ParseException pe) {
					//do nothing
				}
			}
		} 
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedEndTime)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_ENDTIME, strSearchedEndTime, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
}

httpSession.removeAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);

searchedScan = new HashMap<String, Object>();
searchedScan.put(PortalConstants.PARAM_SCAN_ID, strSearchedScanId);
searchedScan.put(PortalConstants.PARAM_FILE_NAME, strSearchedFileName);
searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strSearchedHashValue);
searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strSearchedScanManager);
searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strSearchedScanRegDateLow);
searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strSearchedScanRegDateHigh);
searchedScan.put(PortalConstants.PARAM_STATUS, strSearchedStatus);
searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strSearchedCxScanId);
searchedScan.put(PortalConstants.PARAM_SCAN_START_URL, strSearchedStartUrl);
searchedScan.put(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT, strSearchedImplementationEnvironment);
searchedScan.put(PortalConstants.PARAM_SCAN_START_TIME, strSearchedStartTime);
searchedScan.put(PortalConstants.PARAM_SCAN_END_TIME, strSearchedEndTime);

// End

// Pagination variables
Object curPageObj = null;
int curPage = PortalConstants.INT_ONE;	
int start = PortalConstants.INT_ZERO;
int end = PortalConstants.PAGINATION_DELTA;
// End

// Get project details
ResponseModel rModel = null;
Project project = null;

if (lProjectId != PortalConstants.LONG_ZERO) {
	rModel = ControllerHelper.getProject(lProjectId);
	
	if (!CommonUtil.isObjectNull(rModel)) {
		if (rModel.getStatus() && rModel.getData() != null) {
			project = (Project) rModel.getData();
		} else {
			if (rModel.getMessage().equals(PortalConstants.ORM_EXCEPTION)) {
				if (SessionMessages.isEmpty(renderRequest) && SessionErrors.isEmpty(renderRequest)) {
					bShowDBConnError = true;
				}
			}
		}
	}
}
// End

List<ScanItem> scanList = null;

//Get logged in user details
User user = null;

if (!bShowDBConnError) {
	try {
		user = ControllerHelper.getUser(lUserId);
	} catch (UBSPortalException e) {
		if (e.getErrorCode().equalsIgnoreCase(PortalErrors.ORM_EXCEPTION)) {
			if (SessionErrors.isEmpty(renderRequest) && SessionMessages.isEmpty(renderRequest)) {
				SessionErrors.add(request, PortalMessages.ORM_EXCEPTION);
			}
		}
	}
}
//End

//Get scans
if (bUseVEX && !CommonUtil.isObjectNull(user) && !CommonUtil.isObjectNull(project)) {
	curPageObj = request.getParameter("cur");
	if (Validator.isNotNull(curPageObj)) {
		curPage = Integer.valueOf(String.valueOf(curPageObj));
		start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
		end = curPage * PortalConstants.PAGINATION_DELTA;
	}
	
	int size = 0;
	
	if (!CommonUtil.isListNullOrEmpty(scanList)) {
		size = scanList.size();
	}
	
	if (size == 0 && !CommonUtil.isListNullOrEmpty(scanList)) {
		--curPage;
		start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
		end = curPage * PortalConstants.PAGINATION_DELTA;
		request.setAttribute("index-overlap", "");
		
		if (SessionErrors.isEmpty(renderRequest) && SessionMessages.isEmpty(renderRequest)) {
			bShowPaginationError = true;
		}
	}
	
	ResponseModel rModelScan = null;
	
	if (!bShowDBConnError) {
		rModelScan = ControllerHelper.getScans(project.getProjectId(), user, searchedScan, start);
	}
	
	if (!CommonUtil.isObjectNull(rModelScan)) {
		if (rModelScan.getStatus() && rModel.getData() != null) {
			scanList = (List<ScanItem>) rModelScan.getData();
		} else {
			if (rModelScan.getMessage().equals(PortalConstants.ORM_EXCEPTION)) {
				if (SessionMessages.isEmpty(renderRequest) && SessionErrors.isEmpty(renderRequest)) {
					bShowDBConnError = true;
				}
			}
		}
	}
}
// End

// Check user access
boolean bCanAccess = false;

if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
	bCanAccess = true;
} else {
	bCanAccess = ControllerHelper.canAccessScanList(user.getEmailAddress(), lProjectId);
}
// End

// Sorting variables
String orderByCol = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_COL);
String orderByType = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_TYPE);

if (orderByType == null || orderByType.isEmpty()) {
	Object oOrderByType = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
	
	if (oOrderByType != null) {
		orderByType = oOrderByType.toString();
		
		if (orderByType.isEmpty()) {
			orderByType = PortalConstants.SORT_ASCENDING;
		}
	} else {
		orderByType = PortalConstants.SORT_ASCENDING;
	}
	
	Object oOrderByCol = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_COL);
	
	if (oOrderByCol != null) {
		orderByCol = oOrderByCol.toString();
		
		if (!orderByCol.isEmpty()) {
			pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
		}
	}
} else {
	pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_TYPE, orderByType);
	pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
}

List<ScanItem> scansList = new ArrayList<ScanItem>();
List<Object> list = null;

if (!CommonUtil.isStringNullOrEmpty(orderByCol)
		&& !CommonUtil.isStringNullOrEmpty(orderByType)) {
	list = ControllerHelper.sortScans(lProjectId, user, searchedScan, orderByCol, orderByType, ProjectType.VEX.getInteger(), start);
}

if (!CommonUtil.isListNullOrEmpty(list)) {
	scanList = new ArrayList<ScanItem>();
	for (Object item : list) {
		scanList.add((ScanItem) item);
	}
}


// End

// Show scan list
if (bUseVEX && bCanAccess) {
		int year = 0;
		int month = -1;
		int day = 0;
		
		PortletURL paginationURL = renderResponse.createRenderURL();
		paginationURL.setParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_SCAN_LIST_JSP);
		
		boolean bErrorFromCxSuite = false;
		String strPortalMessage = PortalConstants.STRING_EMPTY;
		
		if (SessionErrors.contains(renderRequest, PortalMessages.VEX_REEXECUTE_SCAN_FAILED)) {
			strPortalMessage = PortalMessages.VEX_REEXECUTE_SCAN_FAILED;
			bErrorFromCxSuite = true;
		} else if (SessionErrors.contains(renderRequest, PortalMessages.VEX_CANCEL_SCAN_FAILED)) {
			strPortalMessage = PortalMessages.VEX_CANCEL_SCAN_FAILED;
			bErrorFromCxSuite = true;
		} else if (SessionErrors.contains(renderRequest, PortalMessages.VEX_REGENERATE_REPORT_FAILED)) {
			strPortalMessage = PortalMessages.VEX_REGENERATE_REPORT_FAILED;
			bErrorFromCxSuite = true;
		} else if (SessionErrors.contains(renderRequest, PortalMessages.VEX_DELETE_SCAN_FAILED)) {
			strPortalMessage = PortalMessages.VEX_DELETE_SCAN_FAILED;
			bErrorFromCxSuite = true;
		}

Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
String strManualDownloadError = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oManualDownloadError)) {
	strManualDownloadError = oManualDownloadError.toString();
}

if (!CommonUtil.isObjectNull(downloadFrom)){
	strDownloadFrom = downloadFrom.toString();
}
%>
<div
	style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px;">
	<liferay-ui:message key="header-scan-list" />
</div>
<div style="height: 14px;">
	<hr
		style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>
<div class="errorMsgDiv">
	<%
	if (bShowPaginationError) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="<%= PortalMessages.NEXT_PAGINATION_ERROR %>" />
	</div>
	<%
	} else if (bShowDBConnError) {
		%>
	<div id="dbConnError" class="alert alert-danger">
		<liferay-ui:message key="error-orm-exception" />
	</div>
	<%
	} else if ((project == null || project.getProjectId() == 0) && SessionErrors.isEmpty(renderRequest)) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="error-project-does-not-exist" />
	</div>
	<%
	} else if (bErrorFromCxSuite) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="<%= strPortalMessage %>" />
		<liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
	</div>
	<%
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("VEX")) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
	</div>
	<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("VEX")) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
	</div>
	<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	}
	
	Set<String> keys = SessionMessages.keySet(renderRequest);
	Iterator<String> it = keys.iterator();
	boolean bHasSessionMessage = false;
	
	while (it.hasNext()) {
		String key = it.next();
		if (!key.contains(SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE)
				&& !key.contains(SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE)) {
			bHasSessionMessage = true;
			break;
		}
	}
	
	if (bHasSessionMessage) {
		%>
	<liferay-ui:success key="<%= PortalConstants.ADD_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.ADD_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.COMPLETE_PROJECT_SUCCESSFUL %>" message="<%= PortalMessages.COMPLETE_PROJECT_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.STOP_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.STOP_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.REEXECUTE_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.REEXECUTE_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.REGENERATE_REPORT_SUCCESSFUL %>" message="<%= PortalMessages.REGENERATE_REPORT_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.DELETE_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.DELETE_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.UPDATE_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.UPDATE_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.OPEN_PROJECT_SUCCESSFUL %>" message="<%= PortalMessages.OPEN_PROJECT_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.RECRAWL_SUCCESSFUL %>" message="<%= PortalMessages.RECRAWL_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.ABORT_CRAWL_SUCCESSFUL %>" message="<%= PortalMessages.ABORT_CRAWL_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.INTERRUPTED_CRAWL_SUCCESSFUL %>" message="<%= PortalMessages.INTERRUPTED_CRAWL_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.CANCEL_CRAWL_SUCCESSFUL %>" message="<%= PortalMessages.CANCEL_CRAWL_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.RESTART_CRAWL_SUCCESSFUL %>" message="<%= PortalMessages.RESTART_CRAWL_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.COPY_CRAWL_SETTING_SUCCESSFUL %>" message="<%= PortalMessages.COPY_CRAWL_SETTING_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.INTERRUPTED_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.INTERRUPTED_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.RESTART_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.RESTART_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.COPY_CRAWL_RESULT_AND_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.COPY_CRAWL_RESULT_AND_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.CANCEL_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.CANCEL_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.REGISTER_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.REGISTER_SCAN_SUCCESSFUL %>" />
	<liferay-ui:success key="<%= PortalConstants.IMPORT_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.IMPORT_SCAN_SUCCESSFUL%>" />
	<%-- <liferay-ui:success key="<%= PortalConstants.AUTO_CRAWL_SUCCESSFUL %>" message="<%= PortalMessages.AUTO_CRAWL_SUCCESSFUL%>" /> --%>
	<%
	} else {
	%>
	<liferay-ui:error key="<%= PortalMessages.AUTO_CRAWL_FAILED %>" message="<%= PortalMessages.AUTO_CRAWL_FAILED%>" />
	<liferay-ui:error key="<%= PortalMessages.ADD_VEX_SCAN_FAILED %>" message="<%= PortalMessages.ADD_VEX_SCAN_FAILED%>" />
	<liferay-ui:error key="<%= PortalMessages.AUTO_CRAWL_FAILED %>" message="<%= PortalMessages.AUTO_CRAWL_FAILED%>" />
	<liferay-ui:error key="<%= PortalMessages.USER_ID_INVALID %>" message="<%= PortalMessages.USER_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_INVALID %>" message="<%= PortalMessages.USER_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_NO_RIGHTS_ACTION %>" message="<%= PortalMessages.USER_NO_RIGHTS_ACTION %>" />
	<liferay-ui:error key="<%= PortalMessages.SCAN_ID_INVALID %>" message="<%= PortalMessages.SCAN_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.HASH_VALUE_INVALID %>" message="<%= PortalMessages.HASH_VALUE_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.SCAN_REGISTRATION_DATE_INVALID %>" message="<%= PortalMessages.SCAN_REGISTRATION_DATE_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.CX_SCAN_ID_INVALID %>" message="<%= PortalMessages.CX_SCAN_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.SCAN_DOES_NOT_EXIST %>" message="<%= PortalMessages.SCAN_DOES_NOT_EXIST %>" />
	<liferay-ui:error key="<%= PortalMessages.SCAN_INVALID %>" message="<%= PortalMessages.SCAN_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.STOP_SCAN_FAILED %>" message="<%= PortalMessages.STOP_SCAN_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.SCAN_CANNOT_BE_STOPPED %>" message="<%= PortalMessages.SCAN_CANNOT_BE_STOPPED %>" />
	<liferay-ui:error key="<%= PortalMessages.REEXECUTE_SCAN_FAILED %>" message="<%= PortalMessages.REEXECUTE_SCAN_FAILED %>" />
	<liferay-ui:error key="<%=PortalMessages.REGENERATE_REPORT_FAILED%>" message="<%=PortalMessages.REGENERATE_REPORT_FAILED%>" />
	<liferay-ui:error key="<%= PortalMessages.REPORT_CANNOT_BE_REGENERATED %>" message="<%= PortalMessages.REPORT_CANNOT_BE_REGENERATED %>" />
	<liferay-ui:error key="<%=PortalMessages.NO_REPORT%>" message="<%=PortalMessages.NO_REPORT%>" />
	<liferay-ui:error key="<%=PortalMessages.NO_DESCRIPTION%>" message="<%=PortalMessages.NO_DESCRIPTION%>" />
	<liferay-ui:error key="<%=PortalMessages.DELETE_SCAN_FAILED%>" message="<%=PortalMessages.DELETE_SCAN_FAILED%>" />
	<liferay-ui:error key="<%=PortalMessages.COMPLETE_PROJECT_FAILED%>" message="<%=PortalMessages.COMPLETE_PROJECT_FAILED%>" />
	<liferay-ui:error key="<%=PortalMessages.OPEN_PROJECT_FAILED%>" message="<%=PortalMessages.OPEN_PROJECT_FAILED%>" />
	<liferay-ui:error key="<%=PortalMessages.CX_SESSION_ID_INVALID%>" message="<%=PortalMessages.CX_SESSION_ID_INVALID%>" />
	<liferay-ui:error key="<%=PortalMessages.SYSTEM_EXCEPTION%>" message="<%=PortalMessages.SYSTEM_EXCEPTION%>" />
	<liferay-ui:error key="<%=PortalMessages.PORTAL_EXCEPTION%>" message="<%=PortalMessages.PORTAL_EXCEPTION%>" />
	<liferay-ui:error key="<%=PortalMessages.ORM_EXCEPTION%>" message="<%=PortalMessages.ORM_EXCEPTION%>" />
	<liferay-ui:error key="<%=PortalMessages.COMMON_EXCEPTION%>" message="<%=PortalMessages.COMMON_EXCEPTION%>" />
	<liferay-ui:error key="<%=PortalErrors.CX_PROCESSING_EXCEPTION%>" message="<%=PortalMessages.REGENERATE_REPORT_FAILED%>" />
	<liferay-ui:error key="<%=PortalErrors.CX_SERVER_ERROR%>" message="<%= strCxServerErrorMsg %>" />
	<liferay-ui:error key="<%= PortalMessages.START_TIME_INVALID %>" message="<%= PortalMessages.START_TIME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.RECRAWL_FAILED %>" message="<%= PortalMessages.RECRAWL_FAILED%>" />
	<liferay-ui:error key="<%= PortalMessages.ABORT_CRAWL_FAILED %>" message="<%= PortalMessages.ABORT_CRAWL_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.INTERRUPTED_CRAWL_FAILED %>" message="<%= PortalMessages.INTERRUPTED_CRAWL_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.CANCEL_CRAWL_FAILED %>" message="<%= PortalMessages.CANCEL_CRAWL_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.RESTART_CRAWL_FAILED %>" message="<%= PortalMessages.RESTART_CRAWL_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.COPY_CRAWL_SETTING_FAILED %>" message="<%= PortalMessages.COPY_CRAWL_SETTING_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.INTERRUPTED_SCAN_FAILED %>" message="<%= PortalMessages.INTERRUPTED_SCAN_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.RESTART_SCAN_FAILED %>" message="<%= PortalMessages.RESTART_SCAN_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.COPY_CRAWL_RESULT_AND_SCAN_FAILED %>" message="<%= PortalMessages.COPY_CRAWL_RESULT_AND_SCAN_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.CANCEL_SCAN_FAILED %>" message="<%= PortalMessages.CANCEL_SCAN_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.END_TIME_INVALID %>" message="<%= PortalMessages.END_TIME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.VEX_REGISTER_SCAN_FAILED %>" message="<%= PortalMessages.VEX_REGISTER_SCAN_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.START_TIME_AND_END_TIME_INVALID %>" message="<%= PortalMessages.START_TIME_AND_END_TIME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.IMPORT_SCAN_FAILED %>" message="<%= PortalMessages.IMPORT_SCAN_FAILED%>" />
	<%
		}
	%>
</div>
<portlet:actionURL name="viewEditProject" var="viewProjectChangeURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="userAction" value="<%=String.valueOf(PortalConstants.USER_EVENT_VIEW_PROJECT_CHANGE)%>" />
	<portlet:param name="isClicked" value="<%=String.valueOf(true)%>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanSimpleSetting" var="viewEditScanURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="userAction" value="<%=String.valueOf(PortalConstants.USER_EVENT_VIEW_CREATE_SCAN)%>" />
</portlet:actionURL>

<portlet:actionURL name="viewScanRegistration" var="viewImportScanURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="userAction" value="<%=String.valueOf(PortalConstants.USER_EVENT_VIEW_IMPORT_SCAN)%>" />
</portlet:actionURL>

<portlet:actionURL name="searchScans" var="searchScansURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
</portlet:actionURL>

<portlet:actionURL name="completeProject" var="completeProjectURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="regDateLow" value="<%=null%>" />
	<portlet:param name="regDateHigh" value="<%=null%>" />
	<portlet:param name="status" value="<%=null%>" />
</portlet:actionURL>

<portlet:actionURL name="openProject" var="openProjectURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="regDateLow" value="<%=null%>" />
	<portlet:param name="regDateHigh" value="<%=null%>" />
	<portlet:param name="status" value="<%=null%>" />
</portlet:actionURL>

<portlet:resourceURL var="updateActionURL">
	<portlet:param name="userAction" value="<%=String.valueOf(PortalConstants.USER_EVENT_UPDATE_ACTION)%>" />
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="<%=PortalConstants.PARAM_SCAN_ID%>" value="<%=strSearchedScanId%>" />
	<portlet:param name="<%=PortalConstants.PARAM_FILE_NAME%>" value="<%=strSearchedFileName%>" />
	<portlet:param name="<%=PortalConstants.PARAM_HASH_VALUE%>" value="<%=strSearchedHashValue%>" />
	<portlet:param name="<%=PortalConstants.PARAM_SCAN_MANAGER%>" value="<%=strSearchedScanManager%>" />
	<portlet:param name="<%=PortalConstants.PARAM_REG_DATE_LOW%>" value="<%=strSearchedScanRegDateLow%>" />
	<portlet:param name="<%=PortalConstants.PARAM_REG_DATE_HIGH%>" value="<%=strSearchedScanRegDateHigh%>" />
	<portlet:param name="<%=PortalConstants.PARAM_STATUS%>" value="<%=strSearchedStatus%>" />
	<portlet:param name="<%=PortalConstants.PARAM_CX_SCAN_ID%>" value="<%=strSearchedCxScanId%>" />
	<portlet:param name="<%=PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT%>" value="<%=strSearchedImplementationEnvironment%>" />
	<portlet:param name="<%=PortalConstants.PARAM_SCAN_START_URL%>" value="<%=strSearchedStartUrl%>" />
	<portlet:param name="<%=PortalConstants.PARAM_SCAN_START_TIME%>" value="<%=strSearchedStartTime%>" />
	<portlet:param name="<%=PortalConstants.PARAM_SCAN_END_TIME%>" value="<%=strSearchedEndTime%>" />
	<portlet:param name="orderByCol" value="<%=orderByCol%>" />
	<portlet:param name="orderByType" value="<%=orderByType%>" />
	<portlet:param name="userId" value="<%=String.valueOf(lUserId)%>" />
	<portlet:param name="screenNo" value="<%=String.valueOf(PortalConstants.SCREEN_SCAN_LIST)%>" />
	<portlet:param name="start" value="<%=String.valueOf(start)%>" />
</portlet:resourceURL>

<portlet:resourceURL var="downloadVulnURL">
	<portlet:param name="userAction" value="<%=String.valueOf(PortalConstants.USER_EVENT_DOWNLOAD_VULN)%>" />
</portlet:resourceURL>

<input id="downloadVuln" type="hidden" value="<%=downloadVulnURL.toString()%>" />
	
<portlet:actionURL name="viewProjectList" var="viewProjectListURL">
	<portlet:param name="type" value="<%=String.valueOf(ProjectType.VEX.getInteger())%>" />
</portlet:actionURL>

<portlet:actionURL name="clearFilterScans" var="clearFilterScansURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="type" value="<%=String.valueOf(ProjectType.VEX.getInteger())%>" />
	<portlet:param name="start" value="<%=String.valueOf(start)%>" />
	<portlet:param name="screenNo" value="<%=String.valueOf(PortalConstants.SCREEN_SCAN_LIST)%>" />
</portlet:actionURL>

<portlet:actionURL name="viewScanList" var="viewScanListURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="start" value="<%=String.valueOf(PortalConstants.INT_ZERO)%>" />
</portlet:actionURL>

<portlet:actionURL name="copyCrawlSetting" var="copyCrawlSettingURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="<%=PortalConstants.PARAM_SCAN_ID%>" value="<%=strSearchedScanId%>" />
</portlet:actionURL>

<input id="screenName" type="hidden" value="refresh_page" />
<input id="screenNo" type="hidden" value="<%=PortalConstants.SCREEN_SCAN_LIST%>" />
<input id="updateActionURL" type="hidden" value="<%=updateActionURL%>" />
<input id="viewProjectListURL" type="hidden" value="<%=viewProjectListURL.toString()%>" />
<input id="viewScanListURL" type="hidden" value="<%=viewScanListURL.toString()%>" />

<div style="float: left;">
	<table>
		<tr>
			<%
				String projectName = PortalConstants.STRING_EMPTY;
					int status = PortalConstants.INT_ZERO;

					if (!CommonUtil.isObjectNull(project)) {
						projectName = project.getProjectName();
						status = project.getStatus();
					}
			%>
			<td style="padding-right: 20px;"><liferay-ui:message
					key="label-project-colon" /><%=HtmlUtil.escapeAttribute(projectName)%></td>
			<%
				if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
			%>
			<td style="padding-right: 20px;"><aui:button
					name="projectChange" value="button-project-change"
					href="<%=viewProjectChangeURL.toString()%>"
					style="width:150px; color:#404040; " /></td>
			<%
				} else if (iUserRole == UserRole.GEN_USER.getInteger()) {
			%>
			<td style="padding-right: 20px;"><aui:button
					name="projectChange" value="button-project-change" disabled="true"
					style="width:150px; color:#404040; " /></td>
			<%
				}
			%>
		</tr>
	</table>
</div>
<div style="position: relative; float: right;">
	<%
		if (status == ProjectStatus.COMPLETE.getInteger()) {
			if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
				String strConfirmOpenProject = "javascript:confirmOpenProject(" + lProjectId + ")";
				%>
				<aui:button name="projectOpen" value="button-project-open"
					onclick="<%=strConfirmOpenProject%>"
					style="width:150px; color:#404040; " />
				<input type="hidden" name="openURL" id="openURL"
					value="<%=openProjectURL.toString()%>" />
				<%
			} else {
				%>
				<aui:button name="projectCompletion" value="button-project-open"
					disabled="true" style="width:150px; color:#404040;" />
				<%
			}
		} else {
			String strConfirmCompleteProject = "javascript:confirmCompleteProject(" + lProjectId + ")";
			%>
			<aui:button name="projectCompletion" value="button-project-completion"
				onclick="<%=strConfirmCompleteProject%>"
				style="width:150px; color:#404040;" />
			<input type="hidden" name="completeURL" id="completeURL"
				value="<%=completeProjectURL.toString()%>" />
			<%
		}
	%>
</div>
<%
	if (status != ProjectStatus.COMPLETE.getInteger()) {
%>
<div
	style="position: relative; float: right; margin-top: 5px; clear: both;">
	<span style="color: red;"><liferay-ui:message
			key="message-reminder-complete-project" /></span>
</div>
<%
	}
%>
<br />
<br />
<aui:button-row>
	<%
		if (status == ProjectStatus.COMPLETE.getInteger()) {
			%>
			<a style="float:left !important; width:160px; color:#404040; margin-right: 5px;" class="btn" disabled="true">
				<liferay-ui:message key="button-register-scan-simple-setting"/>
			</a>
			
			<%if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {%>
				<a style="float:left !important; width:160px; color:#404040; margin-right: 5px;" class="btn" disabled="true">
					<liferay-ui:message key="button-import-scan"/>
				</a>
			<%}%>
			<%
		} else {
			%>
			<a href="<%= viewEditScanURL.toString() %>" style="float:left !important; width:160px; color:#404040; margin-right: 5px;" class="btn">
				<liferay-ui:message key="button-register-scan-simple-setting"/>
			</a>
			
			<%if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {%>
				<a href="<%=viewImportScanURL.toString()%>" style="float:left !important; width:160px; color:#404040; margin-right: 5px;" class="btn">
					<liferay-ui:message key="button-import-scan"/>
				</a>
			<%}%>
			<%
		}
	%>
</aui:button-row>

<div style="height: 15px; padding-top: -10px;">
	<hr
		style="height: 1px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>
<aui:button-row>
	<%
		if (bIsFromSearch && bHasSearchInput) {
	%>
	<aui:button style="width:150px; color: #404040; float:right;"
		value="button-clear-filter" href="<%=clearFilterScansURL%>" />
	<%
		} else {
	%>
	<aui:button style="width:150px; color: #404040; float:right;"
		value="button-clear-filter" disabled="true" />
	<%
		}
	%>
	<button data-toggle="modal" data-target="#scanFilterModal"
		style="width: 150px; color: #404040; float: right; margin-right: 5px;"
		class="btn">
		<liferay-ui:message key="button-filter" />
	</button>
</aui:button-row>
<input type="hidden" id="orderByCol" value="<%=orderByCol%>" />
<input type="hidden" id="orderByType" value="<%=HtmlUtil.escape(orderByType)%>" />

<!-- Modal -->
<div class="modal fade" id="scanFilterModal" role="dialog"
	aria-hidden="true" data-backdrop="static" data-keyboard="false"
	style="width: auto; display: none;">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<liferay-ui:message key="header-scan-filter" />
			</div>
			<form action="<%=searchScansURL.toString()%>" method="post"
				name="fm" style="margin: 0;">
				<div class="modal-body">
					<table>
						<tr>
							<td><liferay-ui:message key="label-id" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_SCAN_ID%>" label=""
									value="<%=strSearchedScanId%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-scan-name" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_FILE_NAME%>" label=""
									value="<%=strSearchedFileName%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-scan-manager" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_SCAN_MANAGER%>" label=""
									value="<%=strSearchedScanManager%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-scan-registration-date" /></td>
							<td>
								<div class="form-group date-input-text">
									<%
										year = 0;
											month = -1;
											day = 0;

											if (dteSearchedScanRegDateLow != null) {
												year = dteSearchedScanRegDateLow.get(Calendar.YEAR);
												month = dteSearchedScanRegDateLow.get(Calendar.MONTH);
												day = dteSearchedScanRegDateLow.get(Calendar.DAY_OF_MONTH);
											}
									%>
									<liferay-ui:input-date
										name="<%=PortalConstants.PARAM_REG_DATE_LOW%>"
										yearValue="<%=year%>" monthValue="<%=month%>"
										dayValue="<%=day%>" />
								</div>
							</td>
							<td class="date-hyphen">~</td>
							<td>
								<div class="form-group date-input-text">
									<%
										year = 0;
											month = -1;
											day = 0;

											if (dteSearchedScanRegDateHigh != null) {
												year = dteSearchedScanRegDateHigh.get(Calendar.YEAR);
												month = dteSearchedScanRegDateHigh.get(Calendar.MONTH);
												day = dteSearchedScanRegDateHigh.get(Calendar.DAY_OF_MONTH);
											}
									%>
									<liferay-ui:input-date
										name="<%=PortalConstants.PARAM_REG_DATE_HIGH%>"
										yearValue="<%=year%>" monthValue="<%=month%>"
										dayValue="<%=day%>" />
								</div>
							</td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-implementation-environment" /></td>
							<td>
								<aui:select
									name="<%=PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT%>"
									label="" multiple="true" cssClass="multiple-select height40px">
									<aui:option value="0" selected="<%=bProductionSelected%>">
										<liferay-ui:message
											key="<%=PortalConstants.KEY_SCAN_PRODUCTION_ENVIRONMENT%>" />
									</aui:option>
									<aui:option value="1" selected="<%=bVerificationSelected%>">
										<liferay-ui:message
											key="<%=PortalConstants.KEY_SCAN_VERIFICATION_ENVIRONMENT%>" />
									</aui:option>
								</aui:select></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-scan-start-url" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_SCAN_START_URL%>" label=""
									value="<%=strSearchedStartUrl%>" /></td>
						</tr>
						<tr>		
							<td><liferay-ui:message key="label-start-time" /> ~ <liferay-ui:message key="label-end-time" /></td>
							<td>
								<div class="form-group date-input-text" style="display:flex; width: auto !important; border:none;">
									<%
									int hour = 0;
									int minute=0;
										year = 0;
											month = -1;
											day = 0;
											if (dteSearchedScanStartTime != null) {
												year = dteSearchedScanStartTime.get(Calendar.YEAR);
												month = dteSearchedScanStartTime.get(Calendar.MONTH);
												day = dteSearchedScanStartTime.get(Calendar.DAY_OF_MONTH);
												hour = dteSearchedScanStartTime.get(Calendar.HOUR_OF_DAY);
												minute = dteSearchedScanStartTime.get(Calendar.MINUTE);
											}
									%>
									<liferay-ui:input-date
										name="<%=PortalConstants.PARAM_SCAN_START_TIME%>"
										yearValue="<%=year%>" monthValue="<%=month%>"
										dayValue="<%=day%>" cssClass="datePicker-start-end"/>
									<liferay-ui:input-time name="startHourTime" amPmParam=""  hourParam = "hour" minuteParam="min"
									hourValue="<%=hour%>" minuteValue="<%=minute%>" minuteInterval="1" cssClass="timePicker-start-end" timeFormat="24-hour"/>
								</div>
							</td>
							<td class="date-hyphen">~</td>
							<td>
								<div class="form-group date-input-text" style="display:flex; width: auto !important; border:none;">
									<%
										year = 0;
											month = -1;
											day = 0;

											if (dteSearchedScanEndTime != null) {
												year = dteSearchedScanEndTime.get(Calendar.YEAR);
												month = dteSearchedScanEndTime.get(Calendar.MONTH);
												day = dteSearchedScanEndTime.get(Calendar.DAY_OF_MONTH);
												hour = dteSearchedScanEndTime.get(Calendar.HOUR_OF_DAY);
												minute = dteSearchedScanEndTime.get(Calendar.MINUTE);
											}
									%>
									<liferay-ui:input-date
										name="<%=PortalConstants.PARAM_SCAN_END_TIME%>"
										yearValue="<%=year%>" monthValue="<%=month%>"
										dayValue="<%=day%>" cssClass="datePicker-start-end" />
									<liferay-ui:input-time name="endHourTime" amPmParam="" hourParam = "hour" minuteParam="min"
											hourValue="<%=hour%>" minuteValue="<%=minute%>" minuteInterval="1" cssClass="timePicker-start-end" timeFormat="24-hour"/>
								</div>
							</td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-status" /></td>
							<td><aui:select name="<%=PortalConstants.PARAM_STATUS%>"
									label="" multiple="true" cssClass="multiple-select">
									<aui:option value="<%=ScanStatus.SCAN_WAITING.getInteger()%>" selected="<%=bScanWaitingSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_SCAN_STATUS_WAITING%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.SCANNING.getInteger()%>" selected="<%=bScanningSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_SCAN_STATUS_ONGOING%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.COMPLETE.getInteger()%>" selected="<%=bCompleteSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_SCAN_STATUS_COMPLETE%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.REPORT_MAKING.getInteger()%>" selected="<%=bReportMakingSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_SCAN_STATUS_REPORT_MAKING%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.FAILURE.getInteger()%>" selected="<%=bFailureSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_SCAN_STATUS_FAILURE%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.SCAN_INTERRUPTED.getInteger()%>" selected="<%=bScanningInterruptedSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_SCAN_STATUS_INTERRUPTED%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.CRAWLING_WAITING.getInteger()%>" selected="<%=bCrawlWaitingSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_CRAWLING_STATUS_WAITING%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.CRAWLING.getInteger()%>" selected="<%=bCrawlingSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_CRAWLING_STATUS_ONGOING%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.CRAWLING_INTERRUPTED.getInteger()%>" selected="<%=bCrawlingInterruptedSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_CRAWLING_STATUS_INTERRUPTED%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.CRAWLING_FAILURE.getInteger()%>" selected="<%=bCrawlingFailedSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_CRAWLING_STATUS_FAILURE%>" />
									</aui:option>
									<aui:option value="<%=ScanStatus.CRAWLING_COMPLETED.getInteger()%>" selected="<%=bCrawlingFailedSelected%>">
										<liferay-ui:message key="<%=PortalConstants.KEY_CRAWLING_STATUS_COMPLETED%>" />
									</aui:option>
								</aui:select></td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn">
						<liferay-ui:message key="button-filter" />
					</button>
					<button type="button" class="btn" data-dismiss="modal">
						<liferay-ui:message key="button-cancel" />
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Copy Crawling Setting and Scan Modal -->
 <div class="modal fade" id="copyCrawlSettingModal" role="dialog"
	aria-hidden="true" data-backdrop="static" data-keyboard="false"
	style="width: auto; display: none;">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<liferay-ui:message key="header-copy-crawl-setting" />
			</div>
			<form action="" method="post" id="formCopyCrawlSettingModal" name="fm" style="margin: 0;">
				<div class="modal-body">
					<table>
						<tr>
							<td><liferay-ui:message key="label-scan-name" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_FILE_NAME%>" label=""
									value="<%=strSearchedFileName%>" /></td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn">
						<liferay-ui:message key="button-create-scan" />
					</button>
					<button type="button" class="btn" data-dismiss="modal">
						<liferay-ui:message key="button-cancel" />
					</button>
				</div>
			</form>
		</div>
	</div>
</div> 
<%
	if (bIsFromSearch && !CommonUtil.isStringNullOrEmpty(strSearchInfo)) {
%>
<div class="alert alert-info">
	<liferay-ui:message arguments="<%=HtmlUtil.escape(strSearchInfo)%>"
		key="<%=PortalConstants.KEY_FILTER_LIST%>"
		localizeKey="<%=false%>" />
</div>
<%
	}

		int totalSize = 0;

		if (!CommonUtil.isListNullOrEmpty(scanList)) {
			totalSize = ControllerHelper.getScansCount(lProjectId, searchedScan);
		}
%>
<div class="search-div"> <liferay-ui:search-container emptyResultsMessage="message-no-scans-to-display" orderByType="<%=HtmlUtil.escape(orderByType)%>" orderByCol="<%=orderByCol%>" iteratorURL="<%=paginationURL%>" total="<%=totalSize%>">
		<liferay-ui:search-container-results>
			<%
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
								results = scanList;

								pageContext.setAttribute("results", results);
							}

							try {
								Integer.parseInt(searchContainer.getCurParam());

								paginationURL.setParameter("cur", searchContainer.getCurParam());
							} catch (NumberFormatException e) {

							}
							scanList = null;
			%>
		</liferay-ui:search-container-results>
		<liferay-ui:search-container-row
			className="jp.ubsecure.portal.jubjub.portlet.model.ScanItem"
			keyProperty="scanId" modelVar="scan" escapedModel="<%=true%>">
			<%
				strScanId = String.format("%05d", scan.getScanId());
			%>
			<liferay-ui:search-container-column-text 
				name="label-id"
				orderable="<%=true%>"
				orderableProperty="scanId"
				value="<%=strScanId%>" />
			<liferay-ui:search-container-column-text 
				name="label-scan-name"
				orderable="<%=true%>" 
				orderableProperty="fileName"
				value="<%=HtmlUtil.escapeAttribute(scan.getFileName())%>" />
			<%
				String username = scan.getScanManager();
			%>
			<liferay-ui:search-container-column-text 
				name="label-scan-manager"
				orderable="<%=true%>" 
				orderableProperty="scanManager"
				value="<%=username%>" />
			<%
				DateFormat formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
				formatter.setTimeZone(TimeZone.getTimeZone(PortalConstants.TIMEZONE_TOKYO));
	
				String date = formatter.format(scan.getScanRegistrationDate());
			%>
			<liferay-ui:search-container-column-text
				name="label-scan-registration-date" 
				orderable="<%=true%>"
				orderableProperty="scanRegistrationDate" 
				value="<%=date%>" />
			<%
				int iImplementation = scan.getImplementationEnvironment();
							String strImplementation = LanguageUtil.get(request,
									PortalConstants.KEY_SCAN_PRODUCTION_ENVIRONMENT);
							if (iImplementation == 1) {
								strImplementation = LanguageUtil.get(request,
										PortalConstants.KEY_SCAN_VERIFICATION_ENVIRONMENT);
							}
							
				String stringUrls = scan.getScanStartURL();
				stringUrls = HtmlUtil.escapeAttribute(stringUrls.replaceAll(";"," ; "));
				stringUrls = stringUrls.replaceAll("&#x20;&#x3b;&#x20;","<br/>");
			%>
			<liferay-ui:search-container-column-text
				name="label-implementation-environment" 
				orderable="<%=true%>"
				orderableProperty="implementationEnvironment"
				value="<%=strImplementation%>" />
			<liferay-ui:search-container-column-text 
				name="label-scan-start-url"
				orderable="<%=true%>" 
				orderableProperty="startUrl"
				value="<%=stringUrls%>" />
			
			<%
							int iStatus = scan.getScanStatus();
							String strStatus = PortalConstants.STRING_EMPTY;
							String strCause = PortalConstants.STRING_EMPTY;
							int iWaitingCount = PortalConstants.INT_ZERO;
							int iCrawlWaitingCount =  PortalConstants.INT_ZERO;

							if (PortletCommonUtil.isDBConnected()) {
								iWaitingCount = ScanLocalServiceUtil.countScanWaiting(scan.getScanRegistrationDate(),
										ProjectType.VEX.getInteger());
								
								iCrawlWaitingCount = ScanLocalServiceUtil.countCrawlWaiting(scan.getScanRegistrationDate(),
										ProjectType.VEX.getInteger());
							}

							if (iStatus == ScanStatus.SCAN_WAITING.getInteger()) {
								strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_WAITING);

								if (iWaitingCount > 0) {
									strStatus += LanguageUtil.format(request,
											PortalConstants.KEY_SCAN_STATUS_WAITING_PRIORITY_NUMBER, iWaitingCount, false);
								} 
							} else  {
								switch(iStatus){
									case PortalConstants.SCANNING:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_ONGOING);
									break;
									
									case PortalConstants.REPORT_MAKING:
									case PortalConstants.REGENERATING:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_REPORT_MAKING);
									break;
									
									case PortalConstants.COMPLETE:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_COMPLETE);
									break;
									
									case PortalConstants.FAILURE:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_FAILURE);
										strCause = (scan.getFailedScanCause() == null || scan.getFailedScanCause().equals("null"))? PortalConstants.STRING_EMPTY : scan.getFailedScanCause();
										
									break;
									
									case PortalConstants.CRAWLING_WAITING:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_WAITING);
										if (iCrawlWaitingCount > 0) {
											strStatus += LanguageUtil.format(request,
													PortalConstants.KEY_CRAWLING_STATUS_WAITING_PRIORITY_NUMBER, iCrawlWaitingCount, false);
										} 
									break;
									
									case PortalConstants.CRAWLING:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_ONGOING);
									break;
									
									case PortalConstants.CRAWLING_INTERRUPTED:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_INTERRUPTED);
									break;
									
									case PortalConstants.CRAWLING_FAILURE:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_FAILURE);
										strCause =  (scan.getFailedScanCause() == null || scan.getFailedScanCause().equals("null"))? PortalConstants.STRING_EMPTY : scan.getFailedScanCause();
										
									break;
									
									case PortalConstants.SCAN_INTERRUPTED:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_INTERRUPTED);
									break;
									
									case PortalConstants.CRAWLING_COMPLETED:
										strStatus = LanguageUtil.get(request, PortalConstants.KEY_CRAWLING_STATUS_COMPLETED);
									break;
							}
						
						}
			%>
			<liferay-ui:search-container-column-text 
				name="label-status"
				orderable="<%=true%>" 
				orderableProperty="scanStatus"
				>
				
				<liferay-ui:message key="<%=strStatus%>" />
				<%
					if ((iStatus == ScanStatus.FAILURE.getInteger() || iStatus == ScanStatus.CRAWLING_FAILURE.getInteger()) && !CommonUtil.isStringNullOrEmpty(strCause)) {
				%>
						<img src="<%=PortalConstants.FAILED_INFO_ICON_PATH%>" title="<%=strCause%>" />
				<%
					} else if ((iStatus == ScanStatus.FAILURE.getInteger()) && CommonUtil.isStringNullOrEmpty(strCause)) {
				%>
						<img src="<%=PortalConstants.FAILED_INFO_ICON_PATH%>" title="<%=PortalMessages.VEX_ERROR_MESSAGE_SCANNING_FAILURE %>" />
				<%
					} else if ((iStatus == ScanStatus.CRAWLING_FAILURE.getInteger()) && CommonUtil.isStringNullOrEmpty(strCause)) {
				%>
						<img src="<%=PortalConstants.FAILED_INFO_ICON_PATH%>" title="<%=PortalMessages.VEX_ERROR_MESSAGE_CRAWLING_FAILURE %>" />
				<%
					}
				%>
					
			</liferay-ui:search-container-column-text>
				<liferay-ui:search-container-column-jsp name="label-details-header" align="right" path="<%=PortalConstants.VEX_SCAN_DETAILS_JSP%>" />
				<liferay-ui:search-container-column-jsp name="label-screen-transition-diagram" align="right" path="<%=PortalConstants.VEX_DOWNLOAD_REFERENCE_JSP%>" />
				<liferay-ui:search-container-column-jsp name="label-report-docx" align="right" path="<%=PortalConstants.VEX_DOWNLOAD_JSP%>" />
				<liferay-ui:search-container-column-jsp name="button-action" align="right" path="<%=PortalConstants.VEX_SCAN_ACTION_JSP%>" /> 
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator />
	</liferay-ui:search-container>
</div>
<%
	} else {
%>
<div class="alert alert-danger">
	<liferay-ui:message key="message-no-access-rights" />
</div>
<%
	}
%>
<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.fileDownload.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/vex/scan_list.js"></script>
<script>
	define.amd = define._amd;
</script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#<portlet:namespace />regDateLow").click(function() {
			changeZIndex();
		});
	
		$("#<portlet:namespace />regDateHigh").click(function() {
			changeZIndex();
		});
		
		$("#<portlet:namespace />startTime").click(function() {
			changeZIndex();
		});
		
		$("#<portlet:namespace />endTime").click(function() {
			changeZIndex();
		});
		
		$("#<portlet:namespace />startHourTime").click(function() {
			changeZIndex();
		});
		
		$("#<portlet:namespace />endHourTime").click(function() {
			changeZIndex();
		});
		
		function changeZIndex () {
			var datePicker = document.getElementsByClassName("datepicker-popover");
			var timePicker = document.getElementsByClassName("timepicker-popover");
			if (datePicker != null) {
				for (var index = 0; index < datePicker.length; index++) {
					$(datePicker[index]).css("z-index", 2000);
				}
			}
			
			if (timePicker != null) {
				for (var index = 0; index < timePicker.length; index++) {
					$(timePicker[index]).css("z-index", 2000);
				}
			}
		}
		
		$('body').removeClass('modal-open');
		$('body').removeClass('modal-backdrop');
		$('.modal-backdrop').remove();
	});
</script>