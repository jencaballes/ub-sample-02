<%@page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil"%>
<%@page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ReportType"%>
<%@page import="jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="java.util.List"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResultItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Report" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ReportType" %>
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	List<Report> reportList = null;
	if (row.getObject() instanceof ScanItem) {
		ScanItem scan = (ScanItem) row.getObject();
		
		//Get reports related to scan
		try{
			int reportTypeArr[] = {ReportType.XLS_REPORT.getInteger(), 0};
			reportList = ControllerHelper.getReports(scan.getScanId(), reportTypeArr, ProjectType.VEX.getInteger());
			if(reportList.isEmpty() || CommonUtil.isStringNullOrEmpty(reportList.get(0).getReportName()) ){
				 reportList = null;
			} 
		} catch(Exception e){
			//do nothing
		}
%>

<portlet:resourceURL var="scanDownloadTransitionDiagramURL">
	<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_DOWNLOAD_SCREEN_TRANSITION_DIAGRAM) %>" />
	<portlet:param name="projectId" value="<%= String.valueOf(scan.getProjectId()) %>" />
	<portlet:param name="scanId" value="<%= String.valueOf(scan.getScanId()) %>" />
	<portlet:param name="status" value="<%= String.valueOf(scan.getScanStatus()) %>" /> 
</portlet:resourceURL>
	
<liferay-ui:icon-menu message="button-action">
	
	<% 
		String downloadReference = "downloadTransitionDiagram('" + scanDownloadTransitionDiagramURL.toString() + "'); return false;";
		
		if ((scan.getScanStatus() == ScanStatus.CRAWLING.getInteger() || 
			 scan.getScanStatus() == ScanStatus.CRAWLING_INTERRUPTED.getInteger() || 
			 scan.getScanStatus() == ScanStatus.CRAWLING_WAITING.getInteger() || 
			 scan.getScanStatus() == ScanStatus.CRAWLING_FAILURE.getInteger())) {
		}else {
			%>
			<!-- Vex Scan List -->
			<liferay-ui:icon message="label-download" url="#" onClick="<%= downloadReference %>" />
			<%
		}
	%>   
	 
</liferay-ui:icon-menu>

<%	} else {
		ResultItem scan = (ResultItem) row.getObject();
					
		//Get reports related to scan
		try{
			int reportTypeArr[] = {ReportType.XLS_REPORT.getInteger(), 0};
			reportList = ControllerHelper.getReports(scan.getScanId(), reportTypeArr, ProjectType.VEX.getInteger());
			if(reportList.isEmpty() || CommonUtil.isStringNullOrEmpty(reportList.get(0).getReportName()) ){
				 reportList = null;
			} 
		} catch(Exception e){
			//do nothing
		}
%>

<portlet:resourceURL var="entireScanDownloadTransitionDiagramURL">
	<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_DOWNLOAD_SCREEN_TRANSITION_DIAGRAM) %>" />
	<portlet:param name="projectId" value="<%= String.valueOf(scan.getProjectId()) %>" />
	<portlet:param name="scanId" value="<%= String.valueOf(scan.getScanId()) %>" />
	<portlet:param name="status" value="<%= String.valueOf(scan.getStatus()) %>" /> 
</portlet:resourceURL>
	
<liferay-ui:icon-menu message="button-action">
	
	<% 
		String downloadReference = "downloadTransitionDiagram('" + entireScanDownloadTransitionDiagramURL.toString() + "'); return false;";
		if ((scan.getStatus() == ScanStatus.CRAWLING.getInteger() ||
				scan.getStatus() == ScanStatus.CRAWLING_INTERRUPTED.getInteger() ||
				scan.getStatus() == ScanStatus.CRAWLING_WAITING.getInteger() || 
				scan.getStatus() == ScanStatus.CRAWLING_FAILURE.getInteger())) {			
		}else{
			%>
			<!-- Vex Entire Scan List -->
			<liferay-ui:icon message="label-download" url="#" onClick="<%= downloadReference %>" />
			<%
		}
	%>
	    
</liferay-ui:icon-menu>

<%	}%>