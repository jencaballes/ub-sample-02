<%@page import="javax.swing.text.html.HTML"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.model.User" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Scan" %>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >


<portlet:defineObjects />

<!-- Vex -->

<%
	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	httpSession.setAttribute("Current_screen", "VEX");
	Object downloadFrom = httpSession.getAttribute("download_from");
	String strDownloadFrom = PortalConstants.STRING_EMPTY;
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	int iUserRole = PortalConstants.INT_ZERO;
	
	//Previous and current screen
	PortletSession pSession = renderRequest.getPortletSession();
	Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
	String strCurrScreen = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oCurrScreen)) {
		strCurrScreen = oCurrScreen.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
			pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
		}
	}

	pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_REGISTRATION);
	// End
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	
	Object oUseVEX = httpSession.getAttribute(PortalConstants.USE_VEX);
	boolean bUseVEX = false;
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
		bUseVEX = true;
	} else {
		if (!CommonUtil.isObjectNull(oUseVEX)) {
			bUseVEX = Boolean.parseBoolean(oUseVEX.toString());
		}
	}
	
	Object oFieldNumber = renderRequest.getAttribute(PortalConstants.PARAM_FIELD_NUMBER);
	int iFieldNumber = PortalConstants.INT_ZERO;
	
	Scan scan = (Scan)renderRequest.getAttribute(PortalConstants.PARAM_SCAN);
	
	if (!CommonUtil.isObjectNull(oFieldNumber)) {
		iFieldNumber = Integer.parseInt(oFieldNumber.toString());
	}
	
	if(!CommonUtil.isObjectNull(scan)){
		pSession.setAttribute(PortalConstants.PARAM_SCAN, scan);
	}
	
	Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
	String strCxServerErrorMsg = null;

	if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
		strCxServerErrorMsg = oCxServerErrorMsg.toString();
	}
	
	Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
		strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
	}

	pSession.removeAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	
	Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);
	
	String strFunctionName = PortalConstants.STRING_EMPTY;
	String strHeaderName = PortalConstants.STRING_EMPTY;
	long lProjectId = PortalConstants.LONG_ZERO;
	long lScanId = PortalConstants.LONG_ZERO;
	String strProjectName = PortalConstants.STRING_EMPTY;
	
	if (!CommonUtil.isObjectNull(project)) {
		lProjectId = project.getProjectId();
		strProjectName = project.getProjectName();
	}
	
	if (!CommonUtil.isObjectNull(scan)) {
		lScanId = scan.getScanId();
	}
	
	
	Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
	long lUserId = PortalConstants.LONG_ZERO;

	if (!CommonUtil.isObjectNull(oUserId)) {
		lUserId = Long.parseLong(oUserId.toString());
	}

	User user = null;

	if (lUserId != PortalConstants.LONG_ZERO) {
		try {
			user = ControllerHelper.getUser(lUserId);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equalsIgnoreCase(PortalErrors.ORM_EXCEPTION)) {
				if (SessionErrors.isEmpty(request) && SessionMessages.isEmpty(request)) {
					SessionErrors.add(request, PortalMessages.ORM_EXCEPTION);
				}
			}
		}
	}

	String emailAddress = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(user)) {
		emailAddress = user.getEmailAddress();
	}


	boolean bCanAccess = false;
	bCanAccess = ControllerHelper.canAccessScanList(emailAddress, lProjectId);

	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
		bCanAccess = true;
	}
	
	Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
	String strManualDownloadError = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oManualDownloadError)) {
		strManualDownloadError = oManualDownloadError.toString();
	}

	if (!CommonUtil.isObjectNull(downloadFrom)){
		strDownloadFrom = downloadFrom.toString();
	}
	
	if (bUseVEX && bCanAccess) {
		boolean bErrorFromCxSuite = false;
		String strPortalMessage = PortalConstants.STRING_EMPTY;
		
		if (SessionErrors.contains(renderRequest, PortalMessages.VEX_REGISTER_SCAN_FAILED)) {
			strPortalMessage = PortalMessages.VEX_REGISTER_SCAN_FAILED;
			bErrorFromCxSuite = true;
		}
%>

<div
	style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px;">
	<liferay-ui:message key="header-scan-registration" />
</div>

<div style="height: 14px;">
	<hr
		style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>

<div>
	<%
	if (bErrorFromCxSuite) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="<%= HtmlUtil.escape(strPortalMessage) %>" /><liferay-ui:message key="<%= HtmlUtil.escape(strCxAPICallErrorMsg) %>" />
		</div>	
		<%
	}  else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("VEX")) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
		</div>
	<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("VEX")) {
		%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
			</div>
		<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	}
	
	%>
	<liferay-ui:error key="<%= PortalMessages.USER_ID_INVALID %>" message="<%= PortalMessages.USER_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_INVALID %>" message="<%= PortalMessages.USER_INVALID %>" />
	
	<liferay-ui:error key="<%= PortalMessages.PROJECT_INVALID %>" message="<%= PortalMessages.PROJECT_INVALID %>" />
	
	<liferay-ui:error key="<%= PortalMessages.FILE_INVALID %>" message="<%= PortalMessages.FILE_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.UPLOAD_FILE_ALREADY_UPLOADED %>" message="<%= PortalMessages.UPLOAD_FILE_ALREADY_UPLOADED %>" />
	<liferay-ui:error key="<%= PortalMessages.EXP_FILE_SIZE_TOO_BIG_VEX %>" message="<%= PortalMessages.EXP_FILE_SIZE_TOO_BIG_VEX %>" />
	<liferay-ui:error key="<%= PortalMessages.SCAN_NAME_INVALID %>" message="<%= PortalMessages.SCAN_NAME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_SCAN_NAME %>" message="<%= PortalMessages.NO_SCAN_NAME %>" />
	<liferay-ui:error key="<%= PortalMessages.UPLOAD_SCAN_FAILED %>" message="<%= PortalMessages.UPLOAD_SCAN_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.IMPORT_SCAN_NO_INSPECTION_LOG %>" message="<%= PortalMessages.IMPORT_SCAN_NO_INSPECTION_LOG %>" />
	<liferay-ui:error key="<%= PortalErrors.CX_SERVER_ERROR %>" message="<%= strCxServerErrorMsg %>" />
	<liferay-ui:error key="<%= PortalMessages.PROJECT_INVALID %>" message="<%= PortalMessages.PROJECT_INVALID %>" />
	<liferay-ui:success key="<%= PortalConstants.UPLOAD_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.UPLOAD_SCAN_SUCCESSFUL %>"/>
</div>

<portlet:actionURL name="uploadScan" var="uploadScanURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<%if(scan !=null){%>
		<portlet:param name="scanId" value="<%= String.valueOf(lScanId) %>" />
		<portlet:param name="status" value="<%= String.valueOf(scan.getStatus()) %>" />
		<portlet:param name="filePath" value="<%= scan.getFilePath() %>" />
	<%}%>
</portlet:actionURL>

<portlet:actionURL name="importScan" var="importScanURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<%if(scan !=null){%>
	<portlet:param name="<%=PortalConstants.PARAM_FILE_NAME %>" value="<%= scan.getFileName() %>" />
	<portlet:param name="status" value="<%= String.valueOf(scan.getStatus()) %>" />
	<portlet:param name="scanId" value="<%= String.valueOf(lScanId) %>" />
	<portlet:param name="filePath" value="<%= scan.getFilePath() %>" />
	<%}%>
</portlet:actionURL>

<portlet:actionURL name="viewScanList" var="viewScanListURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>
<div>
	<div style="display:none;" class="alert alert-info" id="message-reminder-upload-large-files"><liferay-ui:message key="message-reminder-upload-large-files"/></div>
	<div style="display:none;" class="alert alert-info" id="message-reminder-import-large-files"><liferay-ui:message key="message-reminder-import-large-files"/></div>
	</div>
<form action="<%= HtmlUtil.escape(uploadScanURL.toString()) %>" method="post" id="uploadScanForm" enctype="multipart/form-data" class="edit-scan-form">
	<input type="hidden" id="fieldNumber" value="<%= iFieldNumber %>" />
	<table>
		<tr >
			<td style="padding: 20px 60px 10px 0px"><liferay-ui:message key="label-project-colon" /></td>
			<td style="padding: 20px 0px 10px 0px"><%= HtmlUtil.escapeAttribute(strProjectName) %></td>
		</tr>
		<tr>
			<td><span><liferay-ui:message key="label-scan-name" />:</span><span style="color: red;">*</span></td>
			<td>
				<%
					String scanName = "";
					if(!CommonUtil.isObjectNull(scan)){
						scanName = scan.getFileName();
				}%>
				<aui:input type="text" style="width: 215px;" name="<%=PortalConstants.PARAM_SCAN_NAME %>" label=""  value="<%=HtmlUtil.escape(scanName)%>" /></td>
		</tr>
		<tr>
			<td><liferay-ui:message key="label-import-file-colon" /><span style="color: red;">*</span></td>
			
			<td>
				<button type="button" name="browseBtn" id="browseBtn" class="btn" value="button-browse" id="btn-file" onclick="clickFile()"
				style="margin-top: 0;"><liferay-ui:message key="button-browse" /></button>&nbsp;&nbsp;
				<%
					String strFile = "";
					String fileName="";
					
					if(scan !=null){
						strFile = scan.getFilePath();
						
						strFile = strFile.replace("\\", "/");
						int index = strFile.lastIndexOf("/");
						fileName= strFile.substring(index + 1);
					}
				%>
					<span id="strFile" name="strFile" value="<%=HtmlUtil.escape(strFile)%>" >
						<% if(scan == null){%> 
							<liferay-ui:message key="message-file-not-selected" /> 
						<%}else{%>
							<%=HtmlUtil.escape(fileName) %>
						<%}%>
					</span>
				<%
				%>
			</td>
			
			<td>
				<div class="hideInput" style="display:none;">
				<% 
					String filePath="";
					if(scan != null){
						filePath = scan.getFilePath();
					}
				%>
					<aui:input type="file" id="inspectionFile" name="inspectionFile" accept="<%=PortalConstants.CONTENT_TYPE_EXP %>" onchange="sendValue(this)"  value="<%=HtmlUtil.escape(filePath)%>" />
				</div>
			</td>
			
		</tr>
	</table>
	<aui:button-row>
		<button type="button" name="saveBtn" id="saveBtn" class="btn" value="button-scan" onClick="confirmUploadScan()" style="width:120px; color:#404040;">
			<liferay-ui:message key="button-start-uploading" />
		</button>
	</aui:button-row>
	<aui:button-row>
		 <input type="hidden" name="importScanURL" id="importScanURL" value="<%= HtmlUtil.escape(importScanURL.toString()) %>" />
		 <input type="hidden" name="viewScanListURL" id="viewScanListURL" value="<%= HtmlUtil.escape(viewScanListURL.toString()) %>" />
		<% 
			String disabled="";
			String onClickImportScan="";
			if(scan == null){
				disabled = "disabled";
			}
			else{
				onClickImportScan="confirmImportScan()";
			}
		%>
		 
		<button type="button" name="saveBtn" id="saveBtn" class="btn  <%=disabled%>" value="button-scan" onClick="<%=onClickImportScan %>" style="width:120px; color:#404040; margin-right: 15px;">
		<liferay-ui:message key="button-scan" /></button>
		<a id="cancelBtnHref" href="<%= HtmlUtil.escapeHREF(viewScanListURL.toString()) %>" ><span id="cancelBtn" class="btn" onClick="cancelAddScan()" style="width:94px; color:#404040;">
		<liferay-ui:message key="button-cancel" /></span></a>
	</aui:button-row>
	
	<span style="color: red;">
		<liferay-ui:message key="message-reminder-vex-import-1" /><br/>
		<liferay-ui:message key="message-reminder-vex-import-2" />
	</span>
</form>
<script type="text/javascript" src="<%= HtmlUtil.escape(request.getContextPath()) %>/js/vex/scan_list.js"></script>
<script type="text/javascript">
	
	
	function sendValue(){
		var path = $("#<portlet:namespace />inspectionFile").val();
		var fileName = path.split("\\");
		var file = fileName[fileName.length - 1];

		if (file == null || file == "") {
			$("#strFile").html("<liferay-ui:message key='message-file-not-selected' />");
			document.getElementById("uploadScanForm").action = "${uploadScanURL}&hasFileUpload=" + false;
		} else {
			$("#strFile").text(file);
			document.getElementById("uploadScanForm").action = "${uploadScanURL}&hasFileUpload=" + true;
		}
	}
	
	
	function clickFile(){
		$("#<portlet:namespace />inspectionFile").trigger('click');
	}
</script>

<%
	} else {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="message-no-access-rights" />
		</div>
		<%
	}
%>