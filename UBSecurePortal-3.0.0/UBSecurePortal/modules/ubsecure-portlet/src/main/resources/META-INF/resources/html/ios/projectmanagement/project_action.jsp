<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ProjectItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<portlet:defineObjects />

<!-- iOS -->
<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	ProjectItem project = (ProjectItem) row.getObject();
	
	long projectId = project.getProjectId();
	String name = ProjectItem.class.getName();
	
	String redirect = PortalConstants.STRING_EMPTY;
	
	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	int iUserRole = PortalConstants.INT_ZERO;
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
%>
<liferay-ui:icon-menu message="button-action" icon="/o/ubsecure-theme/images/common/tool.png" cssClass="portlet-action">
	<portlet:actionURL name="viewScanList" var="viewScanListURL">
		<portlet:param name="projectId" value="<%= String.valueOf(projectId) %>" />
		<portlet:param name="redirect" value="<%= redirect %>" />
		<portlet:param name="start" value="<%= String.valueOf(PortalConstants.INT_ZERO) %>" />
	</portlet:actionURL>

	<liferay-ui:icon image="list" message="button-scan-list" url="<%= viewScanListURL.toString() %>" cssClass="portlet-action-icon" />

	<%
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
	%>
	<portlet:actionURL name="deleteProject" var="deleteProjectURL">
        <portlet:param name="projectId" value="<%= String.valueOf(projectId) %>" />
        <portlet:param name="type" value="<%= String.valueOf(ProjectType.IOS.getInteger()) %>" />
        <portlet:param name="redirect" value="<%= redirect %>" />
    </portlet:actionURL>

	<%
	String deleteConfirm = "javascript:confirmDeleteProject(" + projectId + ")";
	String strDelete = "deleteURL_" + projectId;
	%>
	<input type="hidden" name="deleteURL" id="<%= strDelete %>" value="<%= deleteProjectURL.toString() %>" />
    <liferay-ui:icon image="trash" message="button-delete" url="<%= deleteConfirm %>" cssClass="portlet-action-icon" />
    <%
    }
    %>
</liferay-ui:icon-menu>