<%@page import="java.text.ParseException"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.liferay.portal.kernel.model.User"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="java.util.Date"%>
<%@ page import="javax.portlet.PortletSession"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResponseModel"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResultItem"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.VexDetectionResultLocalServiceUtil"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil"%>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >
<portlet:defineObjects />
<!-- Vex -->
<%
// Data from session
HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
httpSession.setAttribute("Current_screen", "VEX");
Object downloadFrom = httpSession.getAttribute("download_from");
String strDownloadFrom = PortalConstants.STRING_EMPTY;
Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
long lUserId = PortalConstants.LONG_ZERO;
Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
int iUserRole = PortalConstants.INT_ZERO;
Object oUseVEX = httpSession.getAttribute(PortalConstants.USE_VEX);
boolean bUseVEX = false;
boolean bHasSearchInput = false;

if (!CommonUtil.isObjectNull(oUserId)) {
	lUserId = Long.parseLong(oUserId.toString());
}

if (!CommonUtil.isObjectNull(oUserRole)) {
	iUserRole = Integer.parseInt(oUserRole.toString());
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
		bUseVEX = true;
	} else {
		if (!CommonUtil.isObjectNull(oUseVEX)) {
			bUseVEX = Boolean.parseBoolean(oUseVEX.toString());
		}
	}
}
// End

// Data from renderRequest
PortletSession pSession = renderRequest.getPortletSession();
Object oProjectId = pSession.getAttribute(PortalConstants.PARAM_PROJECT_ID);
long lProjectId = PortalConstants.LONG_ZERO;
Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
String strCxServerErrorMsg = PortalConstants.STRING_EMPTY;
Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
boolean bIsFromSearch = false;
Object oUserAction = pSession.getAttribute(PortalConstants.PARAM_USER_ACTION);
String strUserAction = PortalConstants.STRING_EMPTY;
Object oScanId = pSession.getAttribute(PortalConstants.PARAM_SCAN_ID);
String strScanId = PortalConstants.STRING_EMPTY;
Object oScanName = pSession.getAttribute(PortalConstants.PARAM_FILE_NAME);
String strScanName = PortalConstants.STRING_EMPTY;
long lScanId = PortalConstants.LONG_ZERO;

if (!CommonUtil.isObjectNull(oProjectId)) {
	lProjectId = Long.parseLong(oProjectId.toString());
}

if (!CommonUtil.isObjectNull(oScanName)) {
	strScanName = oScanName.toString();
}

if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
	strCxServerErrorMsg = oCxServerErrorMsg.toString();
}

if (!CommonUtil.isObjectNull(oIsFromSearch)) {
	bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
	
	if (bIsFromSearch) {
		httpSession.setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
	}
}

if (!CommonUtil.isObjectNull(oUserAction)) {
	strUserAction = oUserAction.toString();
}

if (!CommonUtil.isObjectNull(oScanId)) {
	strScanId = oScanId.toString();
	lScanId = Long.parseLong(strScanId);
}

String portletNamespace = renderResponse.getNamespace();
Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
	strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
}

pSession.removeAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
// End

// Show error indicators
boolean bShowDBConnError = false;
boolean bShowPaginationError = false;
// End

//Previous and current screen
Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
String strCurrScreen = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oCurrScreen)) {
	strCurrScreen = oCurrScreen.toString();
	
	if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
		pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
	}
}

pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_DETECTION_RESULT_REVIEW);
// End

// Search scan attributes
Map<String, Object> searchedDetectionResultReview = (Map<String, Object>) pSession.getAttribute(PortalConstants.PARAM_DETECTION_RESULT);
String strSearchedDetectionResultId = PortalConstants.STRING_EMPTY;
String strSearchedScanId = PortalConstants.STRING_EMPTY;
String strSearchedRiskLevel = PortalConstants.STRING_EMPTY;
String strSearchedCategory = PortalConstants.STRING_EMPTY;
String strSearchedOverview = PortalConstants.STRING_EMPTY;
String strSearchedFunctionName = PortalConstants.STRING_EMPTY;
String strSearchedUrl = PortalConstants.STRING_EMPTY;
String strSearchedParameterName = PortalConstants.STRING_EMPTY;
String strSearchedDetectionJudgment = PortalConstants.STRING_EMPTY;;
String strSearchedReviewComment = PortalConstants.STRING_EMPTY;
String strSearchedIsResendInVex = PortalConstants.STRING_EMPTY;
String strSearchedNumber = PortalConstants.STRING_EMPTY;
boolean bLowSelected = false;
boolean bMediumSelected = false;
boolean bHighSelected = false;
boolean bInfoSelected = false;
boolean bDetectedSelected = false;
boolean bOverDetectionSelected = false;
boolean bFixedSelected = false;
Map<String, Object> searchedDetectionResultReviewWithError = (Map<String, Object>) httpSession.getAttribute(PortalConstants.PARAM_DETECTION_RESULT);
String strSearchInfo = PortalConstants.STRING_EMPTY;
String strSearchInfoConnector = PortalConstants.STRING_EMPTY;
String strSearchInfoComma = LanguageUtil.get(request, PortalConstants.KEY_FILTER_LIST_COMMA);

if (bIsFromSearch && !CommonUtil.isMapNullOrEmpty(searchedDetectionResultReview)) {
	Object oSearchedDetectionResultId = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_RESULT_ID);
	Object oSearchedScanId = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_SCAN_ID);
	Object oSearchedRiskLevel = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_RISK_LEVEL);
	Object oSearchedCategory = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_CATEGORY);
	Object oSearchedOverview = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_OVERVIEW);
	Object oSearchedFunctionName = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_FUNCTION_NAME);
	Object oSearchedUrl = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_URL);
	Object oSearchedParameterName = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_PARAMETER_NAME);
	Object oSearchedDetectionJudgment = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_JUDGEMENT);
	Object oSearchedReviewComment = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_REVIEW_COMMENT);
	Object oSearchedIsResendInVex = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX);
	Object oSearchedNumber = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_NUMBER);

	if (!CommonUtil.isObjectNull(oSearchedDetectionResultId)) {
		strSearchedDetectionResultId = String.valueOf(oSearchedDetectionResultId);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedDetectionResultId)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_RESULT_ID, strSearchedDetectionResultId, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedScanId)) {
		strSearchedScanId = String.valueOf(oSearchedScanId);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedScanId)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_SCAN_ID, strSearchedScanId, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedRiskLevel)) {
		strSearchedRiskLevel = String.valueOf(oSearchedRiskLevel);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedRiskLevel)) {
			String [] strSearchedRiskLevelArr = strSearchedRiskLevel.split(PortalConstants.COMMA);
			String strRiskLevelList = PortalConstants.STRING_EMPTY;
			for(int x = 0; x < strSearchedRiskLevelArr.length ; x++){
				String strSelectedRiskLevel = strSearchedRiskLevelArr[x];
				switch(strSelectedRiskLevel){
				case "1":
					bLowSelected = true;
					strRiskLevelList +=PortalConstants.SEVERITY_LOW_EN.toString();
					break;
				case "2":
					bMediumSelected = true;
					strRiskLevelList +=PortalConstants.SEVERITY_MID_EN.toString();
					break;
				case "3":
					bHighSelected = true;
					strRiskLevelList +=PortalConstants.SEVERITY_HIGH_EN.toString();
					break;
				case "4":
					bInfoSelected = true;
					strRiskLevelList +=PortalConstants.SEVERITY_REMARKS_JP.toString();
					break;
				}
				if(x+1 < strSearchedRiskLevelArr.length){
					strRiskLevelList += ", ";
				}
			}
			
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_RISK_LEVEL, strRiskLevelList, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedCategory)) {
		strSearchedCategory = String.valueOf(oSearchedCategory);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedCategory)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_CATEGORY, strSearchedCategory, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedOverview)) {
		strSearchedOverview = String.valueOf(oSearchedOverview);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedOverview)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_OVERVIEW, strSearchedOverview, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedFunctionName)) {
		strSearchedFunctionName = String.valueOf(oSearchedFunctionName);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedFunctionName)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_FUNCTION_NAME, strSearchedFunctionName, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
		
	if (!CommonUtil.isObjectNull(oSearchedUrl)) {
		strSearchedUrl = String.valueOf(oSearchedUrl);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedUrl)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_URL, strSearchedUrl, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}

	if (!CommonUtil.isObjectNull(oSearchedParameterName)) {
		strSearchedParameterName = String.valueOf(oSearchedParameterName);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedParameterName)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_PARAMETER_NAME, strSearchedParameterName, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}

	if (!CommonUtil.isObjectNull(oSearchedDetectionJudgment)) {
		strSearchedDetectionJudgment = String.valueOf(oSearchedDetectionJudgment);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedDetectionJudgment)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_JUDGEMENT, strSearchedDetectionJudgment, false);
			strSearchInfoConnector = strSearchInfoComma;
			
			if(strSearchedDetectionJudgment.equals(PortalConstants.STR_DETECTION_JUDGEMENT)){
				bDetectedSelected = true;
			}else if (strSearchedDetectionJudgment.equals(PortalConstants.STR_DETECTION_JUDGEMENT_OVER_DETECTION)){
				bOverDetectionSelected = true;
			}else{
				bFixedSelected = true;
			}
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedReviewComment)) {
		strSearchedReviewComment = String.valueOf(oSearchedReviewComment);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedReviewComment)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_REVIEW_COMMENT, strSearchedReviewComment, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}

	if (!CommonUtil.isObjectNull(oSearchedIsResendInVex)) {
		strSearchedIsResendInVex = String.valueOf(oSearchedIsResendInVex);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedIsResendInVex)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_IS_RESEND_IN_VEX, strSearchedIsResendInVex, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	if (!CommonUtil.isObjectNull(oSearchedNumber)) {
		strSearchedNumber = String.valueOf(oSearchedNumber);
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedNumber)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_NUMBER, strSearchedNumber, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
} else {
	Object oIsSearch = httpSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
	
	if (!CommonUtil.isObjectNull(oIsSearch)) {
		String strIsSearch = oIsSearch.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strIsSearch)) {
			bIsFromSearch = Boolean.parseBoolean(strIsSearch);
		}
	}
	
	if (bIsFromSearch && CommonUtil.isMapNullOrEmpty(searchedDetectionResultReview) && !CommonUtil.isMapNullOrEmpty(searchedDetectionResultReviewWithError)) {
		Object oSearchedDetectionResultId = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_RESULT_ID);
		Object oSearchedScanId = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_SCAN_ID);
		Object oSearchedRiskLevel = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_RISK_LEVEL);
		Object oSearchedCategory = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_CATEGORY);
		Object oSearchedOverview = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_OVERVIEW);
		Object oSearchedFunctionName = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_FUNCTION_NAME);
		Object oSearchedUrl = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_URL);
		Object oSearchedParameterName = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_PARAMETER_NAME);
		Object oSearchedDetectionJudgment = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_JUDGEMENT);
		Object oSearchedReviewComment = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_REVIEW_COMMENT);
		Object oSearchedIsResendInVex = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX);
		Object oSearchedNumber = searchedDetectionResultReview.get(PortalConstants.PARAM_DETECTION_NUMBER);

		if (!CommonUtil.isObjectNull(oSearchedDetectionResultId)) {
			strSearchedDetectionResultId = String.valueOf(oSearchedDetectionResultId);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedDetectionResultId)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_RESULT_ID, strSearchedDetectionResultId, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedScanId)) {
			strSearchedScanId = String.valueOf(oSearchedScanId);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedScanId)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_SCAN_ID, strSearchedScanId, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedRiskLevel)) {
			strSearchedRiskLevel = String.valueOf(oSearchedRiskLevel);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedRiskLevel)) {
				String [] strSearchedRiskLevelArr = strSearchedRiskLevel.split(PortalConstants.COMMA);
				String strRiskLevelList = PortalConstants.STRING_EMPTY;
				for(int x = 0; x < strSearchedRiskLevelArr.length ; x++){
					String strSelectedRiskLevel = strSearchedRiskLevelArr[x];
					switch(strSelectedRiskLevel){
						case "1":
							bLowSelected = true;
							strRiskLevelList +=PortalConstants.SEVERITY_LOW_EN.toString();
							break;
						case "2":
							bMediumSelected = true;
							strRiskLevelList +=PortalConstants.SEVERITY_MID_EN.toString();
							break;
						case "3":
							bHighSelected = true;
							strRiskLevelList +=PortalConstants.SEVERITY_HIGH_EN.toString();
							break;
						case "4":
							bInfoSelected = true;
							strRiskLevelList +=PortalConstants.SEVERITY_REMARKS_JP.toString();
							break;
					}
					if(x+1 < strSearchedRiskLevelArr.length){
						strRiskLevelList += ", ";
					}
				}
				
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_RISK_LEVEL, strRiskLevelList, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedCategory)) {
			strSearchedCategory = String.valueOf(oSearchedCategory);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedCategory)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_CATEGORY, strSearchedCategory, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedOverview)) {
			strSearchedOverview = String.valueOf(oSearchedOverview);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedOverview)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_OVERVIEW, strSearchedOverview, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedFunctionName)) {
			strSearchedFunctionName = String.valueOf(oSearchedFunctionName);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedFunctionName)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_FUNCTION_NAME, strSearchedFunctionName, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
			
		if (!CommonUtil.isObjectNull(oSearchedUrl)) {
			strSearchedUrl = String.valueOf(oSearchedUrl);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedUrl)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_URL, strSearchedUrl, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}

		if (!CommonUtil.isObjectNull(oSearchedParameterName)) {
			strSearchedParameterName = String.valueOf(oSearchedParameterName);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedParameterName)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_PARAMETER_NAME, strSearchedParameterName, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}

		if (!CommonUtil.isObjectNull(oSearchedDetectionJudgment)) {
			strSearchedDetectionJudgment = String.valueOf(oSearchedDetectionJudgment);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedDetectionJudgment)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_JUDGEMENT, strSearchedDetectionJudgment, false);
				strSearchInfoConnector = strSearchInfoComma;
				
				if(strSearchedDetectionJudgment.equals(PortalConstants.STR_DETECTION_JUDGEMENT)){
					bDetectedSelected = true;
				}else if (strSearchedDetectionJudgment.equals(PortalConstants.STR_DETECTION_JUDGEMENT_OVER_DETECTION)){
					bOverDetectionSelected = true;
				}else{
					bFixedSelected = true;
				}
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedReviewComment)) {
			strSearchedReviewComment = String.valueOf(oSearchedReviewComment);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedReviewComment)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_REVIEW_COMMENT, strSearchedReviewComment, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}

		if (!CommonUtil.isObjectNull(oSearchedIsResendInVex)) {
			strSearchedIsResendInVex = String.valueOf(oSearchedIsResendInVex);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedIsResendInVex)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_IS_RESEND_IN_VEX, strSearchedIsResendInVex, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedNumber)) {
			strSearchedNumber = String.valueOf(oSearchedNumber);
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedNumber)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_DETECTION_NUMBER, strSearchedNumber, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
	}
}

httpSession.removeAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);

searchedDetectionResultReview = new HashMap<String, Object>();

searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_RESULT_ID, strSearchedDetectionResultId);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_SCAN_ID, strSearchedScanId);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_RISK_LEVEL, strSearchedRiskLevel);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_CATEGORY, strSearchedCategory);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_OVERVIEW, strSearchedOverview);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_FUNCTION_NAME, strSearchedFunctionName);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_URL, strSearchedUrl);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_PARAMETER_NAME, strSearchedParameterName);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_JUDGEMENT, strSearchedDetectionJudgment);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_REVIEW_COMMENT, strSearchedReviewComment);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX, strSearchedIsResendInVex);
searchedDetectionResultReview.put(PortalConstants.PARAM_DETECTION_NUMBER, strSearchedNumber);
// End

// Pagination variables
Object curPageObj = null;
int curPage = PortalConstants.INT_ONE;	
int start = PortalConstants.INT_ZERO;
int end = PortalConstants.PAGINATION_DELTA;
// End

// Get project details
ResponseModel rModel = null;
Project project = null;

if (lProjectId != PortalConstants.LONG_ZERO) {
	rModel = ControllerHelper.getProject(lProjectId);
	
	if (!CommonUtil.isObjectNull(rModel)) {
		if (rModel.getStatus() && rModel.getData() != null) {
			project = (Project) rModel.getData();
		} else {
			if (rModel.getMessage().equals(PortalConstants.ORM_EXCEPTION)) {
				if (SessionMessages.isEmpty(renderRequest) && SessionErrors.isEmpty(renderRequest)) {
					bShowDBConnError = true;
				}
			}
		}
	}
}
// End

List<VexDetectionResultItem> detectionResultList = null;

//Get logged in user details
User user = null;

if (!bShowDBConnError) {
	try {
		user = ControllerHelper.getUser(lUserId);
	} catch (UBSPortalException e) {
		if (e.getErrorCode().equalsIgnoreCase(PortalErrors.ORM_EXCEPTION)) {
			if (SessionErrors.isEmpty(renderRequest) && SessionMessages.isEmpty(renderRequest)) {
				SessionErrors.add(request, PortalMessages.ORM_EXCEPTION);
			}
		}
	}
}
//End

//Get detection results
if (bUseVEX && !CommonUtil.isObjectNull(user) && !CommonUtil.isObjectNull(project)) {
	curPageObj = request.getParameter("cur");
	if (Validator.isNotNull(curPageObj)) {
		curPage = Integer.valueOf(String.valueOf(curPageObj));
		start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
		end = curPage * PortalConstants.PAGINATION_DELTA;
	}
	
	int size = 0;
	
	if (!CommonUtil.isListNullOrEmpty(detectionResultList)) {
		size = detectionResultList.size();
	}
	
	if (size == 0 && !CommonUtil.isListNullOrEmpty(detectionResultList)) {
		--curPage;
		start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
		end = curPage * PortalConstants.PAGINATION_DELTA;
		request.setAttribute("index-overlap", "");
		
		if (SessionErrors.isEmpty(renderRequest) && SessionMessages.isEmpty(renderRequest)) {
			bShowPaginationError = true;
		}
	}
	
	ResponseModel rModelDetectionResults = null;
	
	if (!bShowDBConnError) {
		rModelDetectionResults = ControllerHelper.getDetectionResults(lScanId, user, searchedDetectionResultReview, start, null, null);
	}
	
	if (!CommonUtil.isObjectNull(rModelDetectionResults)) {
		if (rModel.getData() != null) {
			detectionResultList = (List<VexDetectionResultItem>) rModelDetectionResults.getData();
		} else {
			if (rModelDetectionResults.getMessage().equals(PortalConstants.ORM_EXCEPTION)) {
				if (SessionMessages.isEmpty(renderRequest) && SessionErrors.isEmpty(renderRequest)) {
					bShowDBConnError = true;
				}
			}
		}
	}
}
// End

// Check user access
boolean bCanAccess = false;

if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
	bCanAccess = true;
} else {
	bCanAccess = ControllerHelper.canAccessScanList(user.getEmailAddress(), lProjectId);
}
// End

// Sorting variables
String orderByCol = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_COL);
String orderByType = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_TYPE);

if (orderByType == null || orderByType.isEmpty()) {
	Object oOrderByType = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
	
	if (oOrderByType != null) {
		orderByType = oOrderByType.toString();
		
		if (orderByType.isEmpty()) {
			orderByType = PortalConstants.SORT_ASCENDING;
		}
	} else {
		orderByType = PortalConstants.SORT_ASCENDING;
	}
	
	Object oOrderByCol = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_COL);
	
	if (oOrderByCol != null) {
		orderByCol = oOrderByCol.toString();
		
		if (!orderByCol.isEmpty()) {
			pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
		}
	}
} else {
	pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_TYPE, orderByType);
	pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
}

List<ScanItem> scansList = new ArrayList<ScanItem>();
List<Object> list = null;

if (!CommonUtil.isStringNullOrEmpty(orderByCol) && !CommonUtil.isStringNullOrEmpty(orderByType)) {
	list = ControllerHelper.sortDetectionResults(lScanId, user, searchedDetectionResultReview, start, orderByCol, orderByType);
}

if (!CommonUtil.isListNullOrEmpty(list)) {
	detectionResultList = new ArrayList<VexDetectionResultItem>();
	for (Object item : list) {
		detectionResultList.add((VexDetectionResultItem)item);
	}
}


// End

// Show detection results
if (bUseVEX && bCanAccess) {
		int year = 0;
		int month = -1;
		int day = 0;
		
		PortletURL paginationURL = renderResponse.createRenderURL();
		paginationURL.setParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REVIEW_DETECTION_RESULT_JSP);
		
		boolean bErrorFromCxSuite = false;
		String strPortalMessage = PortalConstants.STRING_EMPTY;
		
		if (SessionErrors.contains(renderRequest, PortalMessages.RESEND_FAILED)) {
			strPortalMessage = PortalMessages.RESEND_FAILED;
			bErrorFromCxSuite = true;
		} else if (SessionErrors.contains(renderRequest, PortalMessages.REVIEW_COMMENT_INVALID)) {
			strPortalMessage = PortalMessages.REVIEW_COMMENT_INVALID;
			bErrorFromCxSuite = true;
		} else if (SessionErrors.contains(renderRequest, PortalMessages.REVIEW_COMMENT_EMPTY)) {
			strPortalMessage = PortalMessages.REVIEW_COMMENT_EMPTY;
			bErrorFromCxSuite = true;
		}

Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
String strManualDownloadError = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oManualDownloadError)) {
	strManualDownloadError = oManualDownloadError.toString();
}

if (!CommonUtil.isObjectNull(downloadFrom)){
	strDownloadFrom = downloadFrom.toString();
}
%>
<div
	style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px;">
	<liferay-ui:message key="header-review-detection-result" />
</div>
<div style="height: 14px;">
	<hr
		style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>
<div class="errorMsgDiv">
	<%
	if (bShowPaginationError) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="<%= PortalMessages.NEXT_PAGINATION_ERROR %>" />
	</div>
	<%
	} else if (bShowDBConnError) {
		%>
	<div id="dbConnError" class="alert alert-danger">
		<liferay-ui:message key="error-orm-exception" />
	</div>
	<%
	} else if ((project == null || project.getProjectId() == 0) && SessionErrors.isEmpty(renderRequest)) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="error-project-does-not-exist" />
	</div>
	<%
	} else if (bErrorFromCxSuite) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="<%= strPortalMessage %>" />
		<liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
	</div>
	<%
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("VEX")) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
	</div>
	<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("VEX")) {
		%>
	<div class="alert alert-danger">
		<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
	</div>
	<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	}
	
	Set<String> keys = SessionMessages.keySet(renderRequest);
	Iterator<String> it = keys.iterator();
	boolean bHasSessionMessage = false;
	
	while (it.hasNext()) {
		String key = it.next();
		if (!key.contains(SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE)
				&& !key.contains(SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE)) {
			bHasSessionMessage = true;
			break;
		}
	}
	
	if (bHasSessionMessage) {
		%>
	
	<liferay-ui:success key="<%= PortalConstants.RESEND_SUCCESSFUL %>" message="<%= PortalMessages.RESEND_SUCCESSFUL%>" />
	<liferay-ui:success key="<%= PortalConstants.ADD_REVIEW_SCRUTINY_SUCCESSFUL %>" message="<%= PortalMessages.ADD_REVIEW_SCRUTINY_SUCCESSFUL%>" />
	<%
	} else {
	%>
	<liferay-ui:success key="<%= PortalMessages.ADD_REVIEW_SCRUTINY_FAILED%>" message="<%= PortalMessages.ADD_REVIEW_SCRUTINY_FAILED%>" />
	<liferay-ui:success key="<%= PortalMessages.REVIEW_COMMENT_EMPTY %>" message="<%= PortalMessages.REVIEW_COMMENT_EMPTY%>" />
	<liferay-ui:success key="<%= PortalMessages.REVIEW_COMMENT_INVALID %>" message="<%= PortalMessages.REVIEW_COMMENT_INVALID%>" />
	<liferay-ui:success key="<%= PortalMessages.REVIEW_COMMENT_TOO_LONG %>" message="<%= PortalMessages.REVIEW_COMMENT_TOO_LONG%>" />
	<liferay-ui:success key="<%= PortalMessages.RESEND_FAILED%>" message="<%= PortalMessages.RESEND_FAILED%>" />
	<liferay-ui:error key="<%= PortalMessages.USER_ID_INVALID %>" message="<%= PortalMessages.USER_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_INVALID %>" message="<%= PortalMessages.USER_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_NO_RIGHTS_ACTION %>" message="<%= PortalMessages.USER_NO_RIGHTS_ACTION %>" />
	<liferay-ui:error key="<%= PortalMessages.SCAN_ID_INVALID %>" message="<%= PortalMessages.SCAN_ID_INVALID %>" />
	<liferay-ui:error key="<%=PortalMessages.CX_SESSION_ID_INVALID%>" message="<%=PortalMessages.CX_SESSION_ID_INVALID%>" />
	<liferay-ui:error key="<%=PortalMessages.SYSTEM_EXCEPTION%>" message="<%=PortalMessages.SYSTEM_EXCEPTION%>" />
	<liferay-ui:error key="<%=PortalMessages.PORTAL_EXCEPTION%>" message="<%=PortalMessages.PORTAL_EXCEPTION%>" />
	<liferay-ui:error key="<%=PortalMessages.ORM_EXCEPTION%>" message="<%=PortalMessages.ORM_EXCEPTION%>" />
	<liferay-ui:error key="<%=PortalMessages.COMMON_EXCEPTION%>" message="<%=PortalMessages.COMMON_EXCEPTION%>" />
	<liferay-ui:error key="<%=PortalErrors.CX_PROCESSING_EXCEPTION%>" message="<%=PortalMessages.REGENERATE_REPORT_FAILED%>" />
	<liferay-ui:error key="<%=PortalErrors.CX_SERVER_ERROR%>" message="<%= strCxServerErrorMsg %>" />

	<%
		}
	%>
</div>

<portlet:actionURL name="searchDetectionResult" var="searchDetectionResultURL">
	<portlet:param name="scanId" value="<%=String.valueOf(lScanId)%>" />
	<portlet:param name="fileName" value="<%= String.valueOf(strScanName) %>" />
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
</portlet:actionURL>

<portlet:actionURL name="clearFilterDetectionResult" var="clearFilterDetectionResultURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="type" value="<%=String.valueOf(ProjectType.VEX.getInteger())%>" />
	<portlet:param name="scanId" value="<%=String.valueOf(lScanId)%>" />
	<portlet:param name="fileName" value="<%= String.valueOf(strScanName) %>" />
	<portlet:param name="start" value="<%=String.valueOf(start)%>" />
</portlet:actionURL>

<portlet:actionURL name="viewDetectionResult" var="viewDetectionResultURL">
	<portlet:param name="projectId"	value="<%= String.valueOf(lProjectId) %>" />
    <portlet:param name="scanId" value="<%= String.valueOf(lScanId) %>" />
    <portlet:param name="fileName" value="<%= String.valueOf(strScanName) %>" />
    <portlet:param name="type" value="<%=String.valueOf(ProjectType.VEX.getInteger())%>" />
    <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_DETECTION_RESULT_REVIEW) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewScanList" var="viewScanListURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="start" value="<%=String.valueOf(PortalConstants.INT_ZERO)%>" />
</portlet:actionURL>
	    
<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
<input id="screenName" type="hidden" value="refresh_page" />
<input id="screenNo" type="hidden" value="<%=PortalConstants.SCREEN_SCAN_DETECTION_RESULT_REVIEW%>" />
<input id="viewScanListURL" type="hidden" value="<%=viewScanListURL.toString()%>" />
<input id="viewDetectionResultURL" type="hidden" value="<%=viewDetectionResultURL.toString()%>" />
<style>
	.detection-resend-result-table{
		z-index: 100;
	    position: absolute;
	    margin-left: -180px;
	    width: auto !important;
	    border-width: initial !important;
	}
	.detection-fixed-row{
		background-color: darkgray !important;
	}
</style>
<div style="float: left;">
	<table>
		<tr>
			<%
				String projectName = PortalConstants.STRING_EMPTY;

					if (!CommonUtil.isObjectNull(project)) {
						projectName = project.getProjectName();
					}
			%>
			<tr>
			<td style="padding-right: 20px;"><liferay-ui:message key="label-project-colon" /><%=HtmlUtil.escapeAttribute(projectName)%></td>
			</tr>
			<tr>
			<td style="padding-right: 20px;"><liferay-ui:message key="label-scanname-colon" /><%=HtmlUtil.escapeAttribute(strScanName)%></td>	
			</tr>
		</tr>
	</table>
</div>
<br />
<br />

<div style="height: 15px; padding-top: -10px;">
	<hr
		style="height: 1px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>
<aui:button-row>
	<%
		if (bIsFromSearch && bHasSearchInput) {
	%>
	<aui:button style="width:150px; color: #404040; float:right;"
		value="button-clear-filter" href="<%=clearFilterDetectionResultURL%>" />
	<%
		} else {
	%>
	<aui:button style="width:150px; color: #404040; float:right;"
		value="button-clear-filter" disabled="true" />
	<%
		}
	%>
	<button data-toggle="modal" data-target="#detectionResultFilterModal"
		style="width: 150px; color: #404040; float: right; margin-right: 5px;"
		class="btn">
		<liferay-ui:message key="button-filter" />
	</button>
	<button id="confirmationCommentBtn" data-toggle="modal" data-target="#confirmationCommentModal"
		style="display:none !important;"
		class="btn">
	</button>
</aui:button-row>
<input type="hidden" id="orderByCol" value="<%=orderByCol%>" />
<input type="hidden" id="orderByType" value="<%=HtmlUtil.escape(orderByType)%>" />
<!-- Modal -->
<div class="modal fade" id="detectionResultFilterModal" role="dialog"
	aria-hidden="true" data-backdrop="static" data-keyboard="false"
	style="width: auto; display: none;">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<liferay-ui:message key="header-review-detection-result-filter" />
			</div>
			<form action="<%=searchDetectionResultURL.toString()%>" method="post"
				name="fm" style="margin: 0;">
				<div class="modal-body">
					<table>
						<tr>
							<td><liferay-ui:message key="label-detection-result-id" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_DETECTION_RESULT_ID%>" label=""
									value="<%=strSearchedDetectionResultId%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-degree-of-risk" /></td>
							<td><aui:select name="<%=PortalConstants.PARAM_DETECTION_RISK_LEVEL%>" label="" multiple="true" cssClass="multiple-select">
									<aui:option value="1" selected="<%=bLowSelected%>">
										<liferay-ui:message key="<%=PortalConstants.SEVERITY_LOW_EN%>" />
									</aui:option>
									<aui:option value="2" selected="<%=bMediumSelected%>">
										<liferay-ui:message key="<%=PortalConstants.SEVERITY_MID_EN%>"/>
									</aui:option>
									<aui:option value="3" selected="<%=bHighSelected%>">
										<liferay-ui:message key="<%=PortalConstants.SEVERITY_HIGH_EN%>" />
									</aui:option>
									<aui:option value="4" selected="<%=bInfoSelected%>">
										<liferay-ui:message key="<%=PortalConstants.SEVERITY_REMARKS_JP%>" />
									</aui:option>
								</aui:select>
							</td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-category" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_DETECTION_CATEGORY%>" label=""
									value="<%=strSearchedCategory%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-overview-of-vulnerability" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_DETECTION_OVERVIEW%>" label=""
									value="<%=strSearchedOverview%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-function-name" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_DETECTION_FUNCTION_NAME%>" label=""
									value="<%=strSearchedFunctionName%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-url" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_DETECTION_URL%>" label=""
									value="<%=strSearchedUrl%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-parameter-name" /></td>
							<td><aui:input type="text"
									name="<%=PortalConstants.PARAM_DETECTION_PARAMETER_NAME%>" label=""
									value="<%=strSearchedParameterName%>" /></td>
						</tr>
						<tr>
							<td><liferay-ui:message key="label-detection-judgement" /></td>
							<td>
								<aui:select name="<%=PortalConstants.PARAM_DETECTION_JUDGEMENT%>" label="" multiple="false" cssClass="select">
									<aui:option value=""></aui:option>
									<aui:option value="<%=PortalConstants.STR_DETECTION_JUDGEMENT%>" selected="<%=bDetectedSelected%>">
										<liferay-ui:message key="<%=PortalConstants.STR_DETECTION_JUDGEMENT%>" />
									</aui:option>
									<aui:option value="<%=PortalConstants.STR_DETECTION_JUDGEMENT_OVER_DETECTION%>" selected="<%=bOverDetectionSelected%>">
										<liferay-ui:message key="<%=PortalConstants.STR_DETECTION_JUDGEMENT_OVER_DETECTION%>" />
									</aui:option>
									<aui:option value="<%=PortalConstants.STR_DETECTION_JUDGEMENT_FIXED%>" selected="<%=bFixedSelected%>">
										<liferay-ui:message key="<%=PortalConstants.STR_DETECTION_JUDGEMENT_FIXED%>" />
									</aui:option>
								</aui:select>
							</td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn">
						<liferay-ui:message key="button-filter" />
					</button>
					<button type="button" class="btn" data-dismiss="modal">
						<liferay-ui:message key="button-cancel" />
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="confirmationCommentModal" role="dialog"
	aria-hidden="true" data-backdrop="static" data-keyboard="false"
	style="width: auto; display: none;">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<liferay-ui:message key="label-review-scrutiny" />
			</div>
			<input type="hidden" id="selectedDetectionJudgementRow" value=""/>
 			<form id="confirmationCommentForm" action="" method="post"
				name="fm" style="margin: 0;">
				<div class="modal-body">
					<table> 
						<tr>
							<td><liferay-ui:message key="label-over-detection-reason" /></td>
							<td><aui:input type="text" name="<%=PortalConstants.PARAM_DETECTION_REVIEW_COMMENT%>" label="" value="" /></td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn">
						<liferay-ui:message key="button-confirm" />
					</button>
					<button type="button" class="btn" data-dismiss="modal" onclick="resetOption()">
						<liferay-ui:message key="button-cancel" />
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<%
	if (bIsFromSearch && !CommonUtil.isStringNullOrEmpty(strSearchInfo)) {
%>
<div class="alert alert-info">
	<liferay-ui:message arguments="<%=HtmlUtil.escape(strSearchInfo)%>"
		key="<%=PortalConstants.KEY_FILTER_LIST%>"
		localizeKey="<%=false%>" />
</div>
<%
	}

		int totalSize = 0;

		if (!CommonUtil.isListNullOrEmpty(detectionResultList)) {
			totalSize = ControllerHelper.getDetectionResultsCount(lScanId, searchedDetectionResultReview);
		}
%>
<div class="search-div"> <liferay-ui:search-container emptyResultsMessage="message-no-results-to-display" orderByType="<%=HtmlUtil.escape(orderByType)%>" orderByCol="<%=orderByCol%>" iteratorURL="<%=paginationURL%>" total="<%=totalSize%>">
		<liferay-ui:search-container-results>
			<%
				if (!CommonUtil.isListNullOrEmpty(detectionResultList)) {
					results = detectionResultList;

					pageContext.setAttribute("results", results);
				}

				try {
					Integer.parseInt(searchContainer.getCurParam());

					paginationURL.setParameter("cur", searchContainer.getCurParam());
				} catch (NumberFormatException e) {

				}
				detectionResultList = null;
					
							
			%>
		</liferay-ui:search-container-results>
		<liferay-ui:search-container-row
			className="jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResultItem"
			keyProperty="detectionresultid" modelVar="detectionResult" escapedModel="<%=true%>">
			<%
				Object oDetectionResultId = detectionResult.getDetectionresultid();
			 	Object oNumber = detectionResult.getScanresultid();
				
			 	Object oDetectionReviewComment = detectionResult.getReviewcomment();
				String strDetectionReviewComment= PortalConstants.STRING_EMPTY;
				String cssClassName = PortalConstants.STRING_EMPTY;
				
				if(detectionResult.getDetectionjudgment().equals(PortalConstants.STR_DETECTION_JUDGEMENT_FIXED)){
					cssClassName = "detection-fixed-row";
				}
				if(!CommonUtil.isObjectNull(oDetectionReviewComment)) {
					strDetectionReviewComment = oDetectionReviewComment.toString();
				}
				
						
			%>
			<liferay-ui:search-container-column-text 
				name="label-number"
				orderable="<%=true%>"
				orderableProperty="number"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(oNumber.toString())%>" />
			<liferay-ui:search-container-column-text 
				name="label-detection-result-id"
				orderable="<%=true%>" 
				orderableProperty="detectionresultid"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(oDetectionResultId.toString())%>" />
				
				<% Object oRiskLevel = detectionResult.getRisklevel();
				   String strRiskLevel = oRiskLevel.toString();
					switch(strRiskLevel){
						case "1":
							strRiskLevel = PortalConstants.SEVERITY_LOW_EN;
							break;
						case "2":
							strRiskLevel = PortalConstants.SEVERITY_MID_EN;
							break;
						case "3":
							strRiskLevel = PortalConstants.SEVERITY_HIGH_EN;
							break;
						case "4":
							strRiskLevel = PortalConstants.SEVERITY_REMARKS_JP;
							break;
						default:
							strRiskLevel = "";
					};
				%>
			<liferay-ui:search-container-column-text 
				name="label-degree-of-risk"
				orderable="<%=true%>" 
				orderableProperty="risklevel"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(strRiskLevel)%>" />
			<liferay-ui:search-container-column-text 
				name="label-category"
				orderable="<%=true%>" 
				orderableProperty="category"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(detectionResult.getCategory())%>" />
			<liferay-ui:search-container-column-text 
				name="label-overview-of-vulnerability"
				orderable="<%=true%>" 
				orderableProperty="overview"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(detectionResult.getOverview())%>" />
			<liferay-ui:search-container-column-text 
				name="label-function-name"
				orderable="<%=true%>" 
				orderableProperty="functionname"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(detectionResult.getFunctionname())%>" />
				<%
					String strUrl = detectionResult.getUrl();
					strUrl = HtmlUtil.escapeAttribute(strUrl.replaceAll(";"," ; "));
					strUrl = strUrl.replaceAll("&#x20;&#x3b;&#x20;","<br/>");
				%>
			<liferay-ui:search-container-column-text 
				name="label-url"
				orderable="<%=true%>" 
				orderableProperty="url"
				cssClass="<%=cssClassName %>"
				value="<%=strUrl%>" />
			<liferay-ui:search-container-column-text 
				name="label-parameter-name"
				orderable="<%=true%>" 
				orderableProperty="parametername"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(detectionResult.getParametername())%>" />
				
			<%/*If detection judgement is fixed, show text only. Else show dropdown button */
			if(detectionResult.getDetectionjudgment().equals(PortalConstants.STR_DETECTION_JUDGEMENT)) {%>
			<liferay-ui:search-container-column-jsp name="label-detection-judgement" align="right" path="<%=PortalConstants.VEX_REVIEW_DETECTION_DETECTION_JUDGEMENT_JSP %>" />
			<%}else{ %>
			<liferay-ui:search-container-column-text 
				name="label-detection-judgement"
				orderable="<%=true%>" 
				orderableProperty="detectionjudgment"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(detectionResult.getDetectionjudgment())%>" /> 
			<%} %>
				
			<liferay-ui:search-container-column-text 
				name="label-review-scrutiny"
				orderable="<%=false%>" 
				orderableProperty="reviewcomment"
				cssClass="<%=cssClassName %>"
				value="<%=HtmlUtil.escapeAttribute(strDetectionReviewComment)%>" />
			
			<%-- <liferay-ui:search-container-column-jsp name="label-detection-judgement" align="right" path="<%=PortalConstants.VEX_REVIEW_DETECTION_DETECTION_JUDGEMENT_JSP %>" />
			<liferay-ui:search-container-column-jsp name="label-review-scrutiny" align="right" path="<%=PortalConstants.VEX_REVIEW_DETECTION_REVIEW_SCRUTINY_JSP %>" /> --%>
			<liferay-ui:search-container-column-jsp name="label-resend" cssClass="<%=cssClassName %>" align="right" path="<%=PortalConstants.VEX_REVIEW_DETECTION_RESULT_ACTION_JSP %>" />
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator />
	</liferay-ui:search-container>
</div>
<%
	} else {
%>
<div class="alert alert-danger">
	<liferay-ui:message key="message-no-access-rights" />
</div>
<%
	}
%>
<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery.fileDownload.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/vex/detection_result.js"></script>
<script>
	define.amd = define._amd;
</script>
<script type="text/javascript">
	$('.modal-backdrop').remove();
</script>