<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@  taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResultItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil" %>

<portlet:defineObjects />

<!-- Android -->

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	ResultItem scan = (ResultItem) row.getObject();
	Project project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
	
	long scanId = scan.getScanId();
	String name = ResultItem.class.getName();
	int status = scan.getStatus();
%>
<portlet:actionURL name="viewEntireScanList" var="viewEntireScanListURL">
	
</portlet:actionURL>

<input type="hidden" id="url" value="<%= viewEntireScanListURL.toString() %>" />
<liferay-ui:icon-menu message="button-action" icon="/o/ubsecure-theme/images/common/tool.png" cssClass="portlet-action">
	<%
	if (status == ScanStatus.SCAN_WAITING.getInteger() || status == ScanStatus.SCANNING.getInteger()) {
		String confirmStop = "javascript:confirmStopScan(" + scanId + ")";
		%>
		<portlet:actionURL name="stopScan" var="stopScanURL">
			<portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
			<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			<portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
		</portlet:actionURL>
		<% String strStop = "stopURL_" + scanId; %>
		<input type="hidden" name="stopURL" id="<%= strStop %>" value="<%= stopScanURL.toString() %>" />
		<liferay-ui:icon image="stop" message="button-stop" url="<%= confirmStop %>" cssClass="portlet-action-icon" />
		<%
	}
	
	if (status == ScanStatus.SCAN_WAITING.getInteger() || status == ScanStatus.SCANNING.getInteger() || status == ScanStatus.FAILURE.getInteger()) {
		String confirmReexecute = "javascript:confirmReexecuteScan(" + scanId + ")";
		%>
		<portlet:actionURL name="reexecuteScan" var="reexecuteScanURL">
	        <portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
			<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			<portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
	    </portlet:actionURL>
	    
	    <% String strReexecute = "reexecuteURL_" + scanId; %>
		<input type="hidden" name="reexecuteURL" id="<%= strReexecute %>" value="<%= reexecuteScanURL.toString() %>" />
	    <liferay-ui:icon image="reexecute" message="button-reexecute" url="<%= confirmReexecute %>" cssClass="portlet-action-icon" />
		<%
	}
	String confirmDelete = "javascript:confirmDeleteScan(" + scanId + ")";
	%>
	<portlet:actionURL name="deleteScan" var="deleteScanURL">
        <portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
		<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
		<portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
		<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
    </portlet:actionURL>
    <% String strDelete = "deleteURL_" + scanId; %>
    <input type="hidden" name="deleteURL" id="<%= strDelete %>" value="<%= deleteScanURL.toString() %>" />
    <liferay-ui:icon image="trash" message="button-delete" url="<%= confirmDelete %>" cssClass="portlet-action-icon" />
</liferay-ui:icon-menu>