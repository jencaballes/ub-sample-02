<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser" %>

<portlet:defineObjects />

<!-- iOS -->

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	ScanItem scan = (ScanItem) row.getObject();
	Project project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
	
	long scanId = scan.getScanId();
	String name = ScanItem.class.getName();
	
	HttpSession httpSession = null;

	if (renderRequest != null) {
		httpSession = ControllerHelper.getHttpSession(renderRequest);
	} else if (resourceRequest != null) {
		httpSession = ControllerHelper.getHttpSession(resourceRequest);
	} else {
		httpSession = request.getSession();
	}
	
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	int iUserRole = PortalConstants.INT_ZERO;

	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
%>
<portlet:actionURL name="viewScanList" var="viewScanListURL">
	<portlet:param name="projectId" value="<%= String.valueOf(scan.getProjectId()) %>" />
</portlet:actionURL>

<input type="hidden" id="url" value="<%= viewScanListURL.toString() %>" />

<liferay-ui:icon-menu message="button-action" icon="/o/ubsecure-theme/images/common/tool.png" cssClass="portlet-action">
	<%
	if (scan.getScanStatus() == ScanStatus.SCAN_WAITING.getInteger() || scan.getScanStatus() == ScanStatus.SCANNING.getInteger()) {
		String confirmStop = "javascript:confirmStopScan(" + scanId + ")";
		%>
		<portlet:actionURL name="stopScan" var="stopScanURL">
			<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
			<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			<portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
		</portlet:actionURL>
	
		<% String strStop = "stopURL_" + scanId; %>
		<input type="hidden" name="stopURL" id="<%= strStop %>" value="<%= stopScanURL.toString() %>" />
		<liferay-ui:icon image="stop" message="button-stop" url="<%= confirmStop %>" cssClass="portlet-action-icon" />
		<%
	}
	
	if (scan.getScanStatus() == ScanStatus.SCAN_WAITING.getInteger() || scan.getScanStatus() == ScanStatus.SCANNING.getInteger()) {
		String confirmReexecute = "javascript:confirmReexecuteScan(" + scanId + ")";
		%>
		<portlet:actionURL name="reexecuteScan" var="reexecuteScanURL">
	        <portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
	    </portlet:actionURL>
		<% String strReexecute = "reexecuteURL_" + scanId; %>
	    <input type="hidden" name="reexecuteURL" id="<%= strReexecute %>" value="<%= reexecuteScanURL.toString() %>" />
		<liferay-ui:icon image="reexecute" message="button-reexecute" url="<%= confirmReexecute %>" cssClass="portlet-action-icon" />
		<%
	}
	
	if ((scan.getScanStatus() == ScanStatus.UNDER_REVIEW.getInteger()
			|| (scan.getScanStatus() == ScanStatus.FAILURE.getInteger() && scan.getReportCount() > 0 && scan.getReferenceCount() > 0))
			&& iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
		String confirmRegenerate = "javascript:confirmRegenerateReport(" + scanId + ")";
		%>
		<portlet:actionURL name="regenerateReport" var="regenerateReportURL">
			<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
	    </portlet:actionURL>
		<% String strRegenerate = "regenerateURL_" + scanId; %>
	    <input type="hidden" name="regenerateURL" id="<%= strRegenerate %>" value="<%= regenerateReportURL.toString() %>" />
		<liferay-ui:icon image="regenerate" message="button-regenerate-report" url="<%= confirmRegenerate %>" cssClass="portlet-action-icon" />
		<%
	}
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
		String confirmDeleteScan = "javascript:confirmDeleteScan(" + scanId + ")";
		%>
		<portlet:actionURL name="deleteScan" var="deleteScanURL">
			<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
	    </portlet:actionURL>
		<% String strDelete = "deleteURL_" + scanId; %>
	    <input type="hidden" name="deleteURL" id="<%= strDelete %>" value="<%= deleteScanURL.toString() %>" />
		<liferay-ui:icon image="trash" message="button-delete" url="<%= confirmDeleteScan %>" cssClass="portlet-action-icon" />
		<%
	}
	%>
</liferay-ui:icon-menu>