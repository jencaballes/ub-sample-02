<%@ page import="java.text.ParseException"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.net.URL"%>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.VexScanMgmtController" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ProjectItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResponseModel" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformationItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexScanSettingItem" %>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >

<portlet:defineObjects />

<!-- Vex -->

<%
	//Previous and current screen
	PortletSession pSession = renderRequest.getPortletSession();
	Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
	String strCurrScreen = PortalConstants.STRING_EMPTY;
	
	if (!CommonUtil.isObjectNull(oCurrScreen)) {
		strCurrScreen = oCurrScreen.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
			pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
		}
	}
	
	pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_AUTOCRAWLING_SETTING_REGISTRATION);
	// End

    Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);
	VexScanSettingItem autoCrawlingSetting = (VexScanSettingItem) pSession.getAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING);
	VexScanSettingItem scanAdvancedSetting = (VexScanSettingItem) pSession.getAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
	if(CommonUtil.isObjectNull(autoCrawlingSetting))
		autoCrawlingSetting = VexScanMgmtController.setAutoCrawlDefaultValuesForDetailSetting(autoCrawlingSetting, project);

	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	httpSession.setAttribute("Current_screen", "Vex");
	Object downloadFrom = httpSession.getAttribute("download_from");
	String strDownloadFrom = PortalConstants.STRING_EMPTY;
	int iUserRole = PortalConstants.INT_ZERO;
	long lUserId = PortalConstants.LONG_ZERO;
	boolean bUseVex = PortalConstants.FALSE;
	Object oUseVex = httpSession.getAttribute(PortalConstants.USE_VEX);
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
	
	String portletNamespace = null;
	
	int iUserAction = PortalConstants.USER_EVENT_REGISTER_SCAN_ADVANCED_SETTING;
	
	long lProjectId = PortalConstants.LONG_ZERO;
	
	portletNamespace = renderResponse.getNamespace();

	if (!CommonUtil.isObjectNull(project)) {
		lProjectId = project.getProjectId();
	}
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	
	if (!CommonUtil.isObjectNull(oUserId)) {
		lUserId = Long.parseLong(oUserId.toString());
	}
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger() ) {
		bUseVex = true;
	} else {
		if (!CommonUtil.isObjectNull(oUseVex)) {
			bUseVex = Boolean.parseBoolean(oUseVex.toString());
		}
	}
	
	Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
	String strManualDownloadError = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oManualDownloadError)) {
		strManualDownloadError = oManualDownloadError.toString();
	}

	if (!CommonUtil.isObjectNull(downloadFrom)){
		strDownloadFrom = downloadFrom.toString();
	}
	
	Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
	String strCxServerErrorMsg = null;

	if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
		strCxServerErrorMsg = oCxServerErrorMsg.toString();
	}
	
	Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
		strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
	}
	
	if (bUseVex) {
		boolean bErrorFromVex = false;
		String strPortalMessage = PortalConstants.STRING_EMPTY;
		
		if (SessionErrors.contains(renderRequest, PortalMessages.VEX_REGISTER_SCAN_FAILED)) {
			strPortalMessage = PortalMessages.VEX_REGISTER_SCAN_FAILED;
			bErrorFromVex = true;
		}
%>

<portlet:actionURL name="viewScanList" var="viewScanListURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanAutoCrawlingSetting" var="viewRegisterScanAutoCrawlingSettingURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanTargetInformation" var="viewRegisterScanTargetInformationURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanLoginForm" var="viewRegisterScanLoginFormURL" windowState="<%=LiferayWindowState.POP_UP.toString() %>">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanSimpleSetting" var="viewRegisterScanSimpleSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="cancelRegisterScanDetailSetting" var="cancelRegisterScanDetailSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="acquireLoginInformation" var="acquireLoginInformationURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="addLoginInformation" var="addLoginInformationURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="editLoginInformation" var="editLoginInformationURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="registerScanDetailSetting" var="registerScanDetailSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<portlet:param name="userAction" value="<%= String.valueOf(iUserAction) %>" />
</portlet:actionURL>

<div style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px">
	<liferay-ui:message key="header-scan-registration" />
</div>
<div style="height: 14px;">
	<hr style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>

<style>
	.custom-tooltiptext-1{
		width: 200% !important;
		left: 0% !important;
	}
	.custom-tooltiptext-2{
		width: 200% !important;
		left: 0% !important;
	}
	#confirm {
	   display: none;
	   background-color: #FFFFFF;
	   border: 1px solid #aaa;
	   position: fixed;
	   width: 400px;
	   height: auto;
	   left: 35%;
	   z-index: 9999;
	   padding: 6px 8px 8px;
	   box-sizing: border-box;
	   text-align: center;
	   top: 0;
	}
	#confirm button {
	   background-color: #489de5;
	   display: inline-block;
	   border-radius: 5px;
	   border: 1px solid #aaa;
	   padding: 0px;
	   text-align: center;
	   width: 80px;
	   cursor: pointer;
	   color:white;
	   font-size: 13px !important;
	}
	#confirm button {
	   background-color: #489de5;
	   display: inline-block;
	   border-radius: 5px;
	   border: 1px solid #aaa;
	   padding: 0px;
	   text-align: center;
	   width: 80px;
	   cursor: pointer;
	   color:white;
	   font-size: 13px !important;
	}
	#confirm .message {
	   text-align: left;
	   font-size: 13px;
	   margin-bottom: 15px;
	   margin-top: 15px;
	}
	
	#confirm .hostMessage{
	   text-align: left;
	   font-size: 13px;
	   margin-left:10px;
	   margin-bottom: 10px;
	}
	
	#confirm .no{
		background-color: #FFFFFF;
	    color: black;
	    margin-left: 10px;
	    width: 85px;
	}
	
	#confirmBoxTable{
		width: 100%;
	}
	
	.confirmButtons{
		text-align: right;
	}
	
	#overlay {
	     visibility: hidden;
	     position: absolute;
	     left: 0px;
	     top: 0px;
	     width:100%;
	     height:100%;
	     text-align:center;
	     z-index: 9998;
	     background-image:url(background-trans.png);
	}
	
	#overlay div {
	     width:300px;
	     margin: 100px auto;
	     background-color: #fff;
	     border:1px solid #000;
	     padding:15px;
	     text-align:center;
	}
</style>

<div>
	<%
	if (bErrorFromVex) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="<%= strPortalMessage %>" /><liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
		</div>	
		<%
	}  else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("Vex")) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
		</div>
	<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("Vex")) {
		%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
			</div>
		<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	}	%>

<div class="tabbable-content instantiated" id="portlet-set-properties">
	<ul class="lfr-nav nav nav-tabs nav-tabs-default " role="tablist">
		<li class="tab yui3-widget" role="presentation"><a href="<%= viewRegisterScanSimpleSettingURL %>" role="tab" tabindex="-1"><liferay-ui:message key="button-simple-setting" /></a></li>
		<li class="tab yui3-widget tab-focused active tab-selected" role="presentation"><a href="#" role="tab" tabindex="0"><liferay-ui:message key="button-advanced-setting" /></a></li>
	</ul>
</div>
	<liferay-ui:success key="<%= PortalConstants.REGISTER_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.REGISTER_SCAN_SUCCESSFUL %>" />

	<liferay-ui:error key="<%= PortalMessages.PROJECT_ID_INVALID %>" message="<%= PortalMessages.PROJECT_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" message="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" />
	<liferay-ui:success key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	
    <liferay-ui:error key="<%= PortalMessages.ADD_SCAN_FAILED %>" message="<%= PortalMessages.ADD_SCAN_FAILED %>" />
    <liferay-ui:error key="<%= PortalMessages.ADD_VEX_SCAN_FAILED %>" message="<%= PortalMessages.ADD_VEX_SCAN_FAILED %>" />
    <liferay-ui:error key="<%= PortalMessages.AUTO_CRAWL_FAILED %>" message="<%= PortalMessages.AUTO_CRAWL_FAILED %>" />
    <liferay-ui:error key="<%= PortalMessages.UPLOAD_CERTIFICATE_FAILED %>" message="<%= PortalMessages.UPLOAD_CERTIFICATE_FAILED %>" />
	
	<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
	
	<liferay-ui:error key="<%= PortalErrors.CX_SERVER_ERROR %>" message="<%= strCxServerErrorMsg %>" />
	<liferay-ui:error key="<%= PortalMessages.CX_SESSION_ID_INVALID %>" message="<%= PortalMessages.CX_SESSION_ID_INVALID %>" />	
	<liferay-ui:error key="<%= PortalMessages.PRESET_ID_INVALID %>" message="<%= PortalMessages.PRESET_ID_INVALID %>" />

	<liferay-ui:error key="<%= PortalMessages.SESSION_GET_SCAN_NAME_FAILED %>" message="<%=  PortalMessages.SESSION_GET_SCAN_NAME_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.SESSION_GET_SCAN_SETTING_FAILED %>" message="<%= PortalMessages.SESSION_GET_SCAN_SETTING_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.SESSION_GET_TARGET_INFORMATION_LIST_FAILED %>" message="<%= PortalMessages.SESSION_GET_TARGET_INFORMATION_LIST_FAILED %>" />

	<liferay-ui:error key="<%= PortalMessages.START_URL_INVALID %>" message="<%= PortalMessages.START_URL_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.AUTHORIZED_PATROL_URL_INVALID %>" message="<%= PortalMessages.AUTHORIZED_PATROL_URL_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.START_URL_LOCALHOST_INVALID %>" message="<%= PortalMessages.START_URL_LOCALHOST_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.AUTHORIZED_PATROL_URL_LOCALHOST_INVALID %>" message="<%= PortalMessages.AUTHORIZED_PATROL_URL_LOCALHOST_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.UNAUTHORIZED_PATROL_URL_INVALID %>" message="<%= PortalMessages.UNAUTHORIZED_PATROL_URL_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.PARAMETER_NAME_AS_PASSWORD_INVALID %>" message="<%= PortalMessages.PARAMETER_NAME_AS_PASSWORD_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.MAX_NUM_DETECTION_LINK_INVALID %>" message="<%= PortalMessages.MAX_NUM_DETECTION_LINK_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.MAX_NUM_DETECTION_LINK_PER_PAGE_INVALID %>" message="<%= PortalMessages.MAX_NUM_DETECTION_LINK_PER_PAGE_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.DETECTION_LINK_DEPTH_LIMIT_INVALID %>" message="<%= PortalMessages.DETECTION_LINK_DEPTH_LIMIT_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.WAIT_TIME_INVALID %>" message="<%= PortalMessages.WAIT_TIME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.NUM_OF_THREAD_INVALID %>" message="<%= PortalMessages.NUM_OF_THREAD_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.TIME_OUT_TIME_INVALID %>" message="<%= PortalMessages.TIME_OUT_TIME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.REQUEST_HEADER_INVALID %>" message="<%= PortalMessages.REQUEST_HEADER_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.NUM_OF_RETRY_AT_ERROR_INVALID %>" message="<%= PortalMessages.NUM_OF_RETRY_AT_ERROR_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.LOGIN_STATUS_DETECTION_INVALID %>" message="<%= PortalMessages.LOGIN_STATUS_DETECTION_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.SESSION_ERROR_DETECTION_INVALID %>" message="<%= PortalMessages.SESSION_ERROR_DETECTION_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.SCREEN_TRANSITION_ERROR_DETECTION_INVALID %>" message="<%= PortalMessages.SCREEN_TRANSITION_ERROR_DETECTION_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.REGULAR_EXPRESSION_INVALID %>" message="<%= PortalMessages.REGULAR_EXPRESSION_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.REGULAR_EXPRESSION_GROUPING_INVALID %>" message="<%= PortalMessages.REGULAR_EXPRESSION_GROUPING_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.LOGIN_PARAM_VALUE_INVALID%>" message="<%= PortalMessages.LOGIN_PARAM_VALUE_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.UNUSABLE_CHARACTER_INPUT%>" message="<%= PortalMessages.UNUSABLE_CHARACTER_INPUT %>" />
	<liferay-ui:error key="<%= PortalMessages.INVALID_CHARACTER_INPUT%>" message="<%= PortalMessages.INVALID_CHARACTER_INPUT %>" />
	
	<liferay-ui:error key="<%= PortalMessages.NO_START_URL %>" message="<%= PortalMessages.NO_START_URL %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_AUTHORIZED_PATROL_URL %>" message="<%= PortalMessages.NO_AUTHORIZED_PATROL_URL %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_UNAUTHORIZED_PATROL_URL %>" message="<%= PortalMessages.NO_UNAUTHORIZED_PATROL_URL %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_MAX_NUM_DETECTION_LINK %>" message="<%= PortalMessages.NO_MAX_NUM_DETECTION_LINK %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_MAX_NUM_DETECTION_LINK_PER_PAGE %>" message="<%= PortalMessages.NO_MAX_NUM_DETECTION_LINK_PER_PAGE %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_DETECTION_LINK_DEPTH_LIMIT %>" message="<%= PortalMessages.NO_DETECTION_LINK_DEPTH_LIMIT %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_WAIT_TIME %>" message="<%= PortalMessages.NO_WAIT_TIME %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_NUM_OF_THREAD %>" message="<%= PortalMessages.NO_NUM_OF_THREAD %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_TIME_OUT_TIME %>" message="<%= PortalMessages.NO_TIME_OUT_TIME %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_REQUEST_HEADER %>" message="<%= PortalMessages.NO_REQUEST_HEADER %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_NUM_OF_RETRY_AT_ERROR %>" message="<%= PortalMessages.NO_NUM_OF_RETRY_AT_ERROR %>" />

	<liferay-ui:error key="<%= PortalMessages.FILE_NOT_FOUND_EXCEPTION %>" message="<%= PortalMessages.FILE_NOT_FOUND_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.IO_EXCEPTION %>" message="<%= PortalMessages.IO_EXCEPTION %>" />
<div>
	<span id="labelAutoCrawlingSetting" style="text-decoration: underline;"><liferay-ui:message key="label-autocrawling-setting" /></span>
</div>
	
<div class="autocrawling-form">	
<form action="<%= registerScanDetailSettingURL.toString() %>" method="post" id="fm">
	<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
	<input type="hidden" id="projectId" name="projectId" value="<%= lProjectId %>" />
	<input type="hidden" id="loggedInUser" name="loggedInUser" value="<%= lUserId %>" />
	<input type="hidden" id="goToTargetInfo" value="<%= viewRegisterScanTargetInformationURL %>" />
	<aui:input type="hidden" name="setCrawlingOnly" value=""/>
	<!-- 1st division -->
	<table class="first-division-table">
		<tr>
            <td>
		        <table class="first-division-table-child-first full-width with-border">
                <tbody>
    		    <tr class="input-row">
    		     	<td class="form-label"><liferay-ui:message key="label-start-URL" /><span class="required">*</span></td>
    		     	<td><div class="with-margin-top-10 vextooltip" style="width:100%;opacity:unset;">
							<aui:input type="textarea" name="startURL" value="<%= autoCrawlingSetting.getStarturl() %>" label="" cssClass="field form-control form-textarea"/>
							<span class="tooltiptext"><liferay-ui:message key="message-reminder-start-url-1" /><br>
								<liferay-ui:message key="message-reminder-start-url-2" /><br><br>
								<liferay-ui:message key="message-reminder-start-url-3" /><br>
								<liferay-ui:message key="message-reminder-start-url-4" />
							</span>
							<!-- <span class="tooltiptext">Assign crawling Start URL.<br><br>
								Start URL has to be included to the authorized URL for crawling.<br><br>
								You need to use line break to separate multiple value.<br>
								This is case sensitive.
							</span> -->
						</div>
    		     	</td>
    		    </tr>
    		    <tr class="input-row">
    		     	<td class="form-label"><liferay-ui:message key="label-authorized-patrol-URL" /><span class="required">*</span></td>
    		     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
    		     			<aui:input type="textarea" name="authorizedPatrolURL" value="<%= autoCrawlingSetting.getAuthorizedpatrolurl() %>" label="" cssClass="field form-control form-textarea"/>
    		     			<span class="tooltiptext"><liferay-ui:message key="message-reminder-authorized-url-1" /><br><br>
								<liferay-ui:message key="message-reminder-authorized-url-2" /><br>
								<liferay-ui:message key="message-reminder-authorized-url-3" /><br>
								<liferay-ui:message key="message-reminder-authorized-url-4" /><br><br>
								<liferay-ui:message key="message-reminder-authorized-url-5" /><br>
								<liferay-ui:message key="message-reminder-authorized-url-6" />
							</span> 
    		     			<!-- <span class="tooltiptext">Assign possible crawling range.<br><br>
								You can use [*] as a wild card.<br>
								You need to use line break to separate multiple value.<br>
								This is not case sensitive.<br><br>

								Ex) If you authorize all URL starting with http://www.ubsecure.jp/ , you can write http://www.ubsecure.jp/*
							</span> -->
						</div>
    		     	</td>
    		    </tr>
    		    <tr class="input-row">
    		     	<td class="form-label"><liferay-ui:message key="label-unauthorized-patrol-URL" /></td>
    		     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
							<aui:input type="textarea" name="unauthorizedpatrolURL" value="<%= autoCrawlingSetting.getUnauthorizedpatrolurl() %>" label="" cssClass="field form-control form-textarea"/>
							<span class="tooltiptext">
								<liferay-ui:message key="message-reminder-unauthorized-url-1" /><br><br>
								<liferay-ui:message key="message-reminder-unauthorized-url-2" /><br>
								<liferay-ui:message key="message-reminder-unauthorized-url-3" /><br>
								<liferay-ui:message key="message-reminder-unauthorized-url-4" /><br><br>
								<liferay-ui:message key="message-reminder-unauthorized-url-5" /><br>
								<liferay-ui:message key="message-reminder-unauthorized-url-6" />
							</span>
						</div>
    		     	</td>
    		    </tr>
    	        </tbody>
                </table>
		    </td>
		</tr>
		<tr><td><div style="height:50px"></div>
				<table class="first-division-table-child-first full-width">
                <tbody id="first-division-table-child-first-tbody" style="height:775px">
				    <tr><td><liferay-ui:message key="label-login" /></td></tr>
      	        	<tr>
      	        	    <td>
						    <table class="inline-table with-border">
                                <tbody>
      	                        	<tr class="input-row">
      	                        		<td class="form-label"><liferay-ui:message key="label-login-status-detection" /></td>
      	                        		<td><div class="vextooltip" style="width:100%;opacity:unset;">
												<aui:input type="textarea" name="loginstatusdetection" value="<%= autoCrawlingSetting.getLoginstatusdetection() %>" label="" cssClass="field form-control form-textarea" style="margin-right: 15px;"/>
												<span class="tooltiptext">
													<liferay-ui:message key="message-reminder-login-status-detection-1" /><br>
													<liferay-ui:message key="message-reminder-login-status-detection-2" /><br>
													<liferay-ui:message key="message-reminder-login-status-detection-3" /><br><br>
													<liferay-ui:message key="message-reminder-login-status-detection-4" /><br>
													<liferay-ui:message key="message-reminder-login-status-detection-5" /><br>
													<liferay-ui:message key="message-reminder-login-status-detection-6" />
												</span>
											</div>
										</td>
      	                        	</tr>
      	                        	<tr class="input-row">
                                   	<td class="form-label"><liferay-ui:message key="label-set-not-send-password" /></td>
                                   	<td>
                                   		<div class="">
                                   		    <table>
                                   			<tr class="input-row">
                                   				<td class="form-field">
												<% if(autoCrawlingSetting.getSetnotsendpassword() == PortalConstants.INT_ZERO){ %>
                     							<input class="checkbox" id="setnotsendpassword" name="<%= portletNamespace %>setnotsendpassword" type="checkbox" value="0" onclick="changeValue(this)">
                     							<% } else { %>
                     							<input class="checkbox" id="setnotsendpassword" name="<%= portletNamespace %>setnotsendpassword" type="checkbox" value="1" checked onclick="changeValue(this)">
                     							<% } %>
                                   				</td>
                                   				<td><div class="form-group with-border" id="form-textarea-not-send-password" ><liferay-ui:message key="long-text-not-send-password" /></div></td>
                                   			</tr>
                                   			</table>
                                   		</div>
                                   	</td>
									</tr>
      	                        	<tr class="input-row">
      	                        		<td class="form-label"><liferay-ui:message key="label-parameter-name-as-password" /></td>
      	                        		<td><div class="vextooltip" style="width:100%;opacity:unset;">
												<aui:input type="textarea" name="parameternameaspassword" value="<%= autoCrawlingSetting.getParamnamesaspassword() %>" label="" cssClass="form-textarea" style="margin-right: 15px;" />
												<span class="tooltiptext">
													<liferay-ui:message key="message-reminder-parameter-name-as-password-1" /><br>
													<liferay-ui:message key="message-reminder-parameter-name-as-password-2" /><br>
													<liferay-ui:message key="message-reminder-parameter-name-as-password-3" /><br>
													<liferay-ui:message key="message-reminder-parameter-name-as-password-4" /><br><br>
													<liferay-ui:message key="message-reminder-parameter-name-as-password-5" /><br>
													<liferay-ui:message key="message-reminder-parameter-name-as-password-6" /><br>
													<liferay-ui:message key="message-reminder-parameter-name-as-password-7" />
												</span>
											</div>
										</td>
									</tr>
      	                        </tbody>
                            </table>
						</td>
      	        	</tr>
      	        	<tr>
      	        	    <td>
      	        	     	<div id="loginSettingContainer1">
      	        	     	<%
      	        	     	List<VexTargetInformationItem> targetInformationList = (List<VexTargetInformationItem>) pSession.getAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
							List <ArrayList<VexLoginSettingItem>> loginSettingList = (ArrayList<ArrayList<VexLoginSettingItem>>) pSession.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
	   							int totalNumOfParams = (int) pSession.getAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
	   						%>
	   						</div>
							<table>
			                    <tr>
			                        <td id="login-label"><liferay-ui:message key="label-login-setting" /></td>
			                        <td >
			                    	<div class="button-holder ">
										<button data-toggle="modal" data-target="#loginSettingModal" id="<%= portletNamespace %>loginSettingModal" class="btn btn-default" type="button"><liferay-ui:message key="button-create-new" /></button>
										<!-- Modal -->
										<% String modalLoginFormUrl = viewRegisterScanLoginFormURL.toString();  %>
											<aui:script position="inline" use="liferay-util-window">
													var addloginSettingModal = A.one('#<portlet:namespace />loginSettingModal');
													addloginSettingModal.on('click', function(){
																Liferay.Util.openWindow({
																	dialog: {
																		destroyOnHide:true,
																		height:600,
																		width:600,
																		resizable: false
																	},
																	id: 'open_get_login_form',
																	uri: '<%=modalLoginFormUrl %>',
																	title: Liferay.Language.get('header-login-settings')
																});
															});
											</aui:script>				
											<!-- For closing -->
											<aui:script>
													Liferay.provide(window, 'close_get_login_form',
																function(dialogId) {
																	var A=AUI();
																	console.log(dialogId);
																	var dialog= Liferay.Util.getWindow(dialogId);
																	dialog.destroy();
																	//Liferay.fire('closeWindow', {
																	//	id:dialogId
																	//});
																},
																['aui-base', 'liferay-util-window']
															);
											</aui:script>
											<!-- For refresh -->
											<aui:script position="inline" use="liferay-util-window">
											Liferay.provide(
       												 window,
        											'refreshPortlet',
        											function() {
           											var curPortletBoundaryId = '#p_p_id<portlet:namespace />';

            											Liferay.Portlet.refresh(curPortletBoundaryId);
       												 },
        											['aui-dialog','aui-dialog-iframe', 'liferay-util-window']
   													 );
											</aui:script>
										<input type="hidden" id="<%= portletNamespace %>totalNumOfParams" name="<%= portletNamespace %>totalNumOfParams" value="<%= totalNumOfParams %>" />
								    </div>
									</td>
			                    </tr>
			                </table>
						</td>
      	        	</tr>
      	        	<tr>
      	        	    <td>
      	        	     	<div id="loginSettingContainer2">
							<table class="table table-bordered scroll" style="width:98%" id="parentLoginSettingList">
           	                        <thead class="table-columns full-width">
           	                        	<tr class="full-width">
           	                        		<th class="table-first-header" id="">
           	                        			<div style="text-decoration-line: underline;">
           	                        				<liferay-ui:message key="label-target-information" />
           	                        			</div>
           	                        		</th>
           	                        		<th>
           	                        			<div style="text-decoration-line: underline;">
           	                        				<liferay-ui:message key="label-login-form-path" />
           	                        			</div>
           	                        		</th>
           	                        		<th class="table-last-header">
           	                        		</th>
           	                        	</tr>
           	                        </thead>
           	                        <tbody class="table-data">
           	                        		<%  String editLoginFormUrl = modalLoginFormUrl;
           	                        		    String deleteLoginFormUrl = PortalConstants.STRING_EMPTY;
												if (!CommonUtil.isListNullOrEmpty(loginSettingList) && loginSettingList.size() > 0) {
													for (ArrayList<VexLoginSettingItem> sessionItem : loginSettingList) {
														URL loginURL = null;
												%> 	<tr> 
														<portlet:actionURL name="deleteLoginInformation" var="vardeleteLoginInformationURL">
															<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
															<portlet:param name="rowIndex" value="<%= String.valueOf(loginSettingList.indexOf(sessionItem)) %>" />
														</portlet:actionURL>

														<portlet:actionURL name="viewEditLoginInformation" var="viewEditLoginInformationURL" windowState="<%=LiferayWindowState.POP_UP.toString() %>">
															<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
															<portlet:param name="rowIndex" value="<%= String.valueOf(loginSettingList.indexOf(sessionItem)) %>" />
														</portlet:actionURL>

														<% 
														int rowIndex = loginSettingList.indexOf(sessionItem);
														editLoginFormUrl = viewEditLoginInformationURL.toString();
														deleteLoginFormUrl = vardeleteLoginInformationURL.toString();
														for(VexLoginSettingItem item : sessionItem){
															loginURL = new URL(item.getLoginurl());
													 } %>
													 	<td><%=loginURL.getProtocol() + "://" + loginURL.getAuthority() %></td>
														<td><%=loginURL.getPath()%></td>
														<td style="padding:5px;" id="button-holder-login">
														<input type="hidden" id="editLoginFormUrl<%= rowIndex %>" value="<%= editLoginFormUrl %>" />
														<input type="hidden" id="deleteLoginFormUrl<%= rowIndex %>" value="<%= deleteLoginFormUrl %>" />
														<input type="hidden" id="rowIndex<%= rowIndex %>" name="<%= portletNamespace %>rowIndex<%= rowIndex %>" value="<%= rowIndex %>" />
														<button data-toggle="modal" data-target="#editloginSettingModal" id="<%= portletNamespace %>editloginSettingModal<%= rowIndex %>" class="btn" type="button" onclick="editLoginSetting(this)" style="width:80px;margin-top:0px;"><liferay-ui:message key="button-edit" /></button>
														<button type="button" class="btn" id="<%= portletNamespace %>deleteloginSetting<%= rowIndex %>" onclick="deleteLoginSetting(this)" style="width:80px;margin-top:0px;" ><liferay-ui:message key="button-delete" /></button>
														</td>
												</tr>
           	                        		<% } }else { %>
           	                        		       	<tr>
													<td></td>
													<td></td>
														<td style="padding:5px;">
														<a data-toggle="modal" data-target="" id="" class="btn btn-default" type="button" style="display:none"><liferay-ui:message key="button-edit" /></a>
														<a type="button" class="btn" onclick="" style="display:none"><liferay-ui:message key="button-delete" /></a>
														</td>
													</tr>
           	                        		<% }  %>
									</tbody>
								</table>
           	              	</div>
						</td>
      	        	</tr>																			
      	        </tbody>
                </table>
		</td></tr>
	</table>
	<!-- 2nd division -->
	<table class="second-division-table">
		<tr>
			<td>
		        <table class="second-division-table-child-first full-width with-border">
                    <tbody>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-maxnum-detection-link" /><span class="required">*</span></td>
                     		<td class="form-field">
								<div class="vextooltip" style="width:100%;opacity:unset;">
									<aui:input type="text" name="maxnumdetectionlink" value="<%= ((autoCrawlingSetting.getMaxnumdetectionlink() == 0)  ? 500 : autoCrawlingSetting.getMaxnumdetectionlink()) %>" label="" cssClass="field form-control"/>
									<span class="tooltiptext">
										<liferay-ui:message key="message-reminder-max-num-detection-link" />
									</span>
								</div>
							</td>
                     	</tr>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-maxnum-detection-link-per-page" /><span class="required">*</span></td>
							<td class="form-field">
								<div class="vextooltip" style="width:100%;opacity:unset;">
									<aui:input type="text" name="maxnumdetectionlinkperpage" value="<%= ((autoCrawlingSetting.getMaxnumdetectionlinkperpage() == 0)  ? 100 : autoCrawlingSetting.getMaxnumdetectionlinkperpage()) %>" label="" cssClass="field form-control"/>
									<span class="tooltiptext">
										<liferay-ui:message key="message-reminder-max-num-detection-link-per-page" />
									</span>
								</div>
							</td>
                     	</tr>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-detection-link-depth-limit" /><span class="required">*</span></td>
                     		<td class="form-field">
								<div class="vextooltip" style="width:100%;opacity:unset;">
									<aui:input type="text" name="detectionlinkdepthlimit" value="<%= ((autoCrawlingSetting.getDetectionlinkdepthlimit() == 0)  ? 20 : autoCrawlingSetting.getDetectionlinkdepthlimit()) %>" label="" cssClass="field form-control"/>
									<span class="tooltiptext">
										<liferay-ui:message key="message-reminder-detection-link-depth-limit" />
									</span>
								</div>
							</td>
                     	</tr>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-wait-time" /><span class="required">*</span></td>
                     		<td class="form-field">
								<div class="vextooltip" style="width:100%;opacity:unset;">
									<aui:input type="text" name="waittime" value="<%= autoCrawlingSetting.getWaittime() %>" label="" cssClass="field form-control"/>
									<span class="tooltiptext">
										<liferay-ui:message key="message-reminder-wait-time" />
									</span>
								</div>
							</td>
                     	</tr>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-num-Of-thread" /><span class="required">*</span></td>
                     		<td>
								<div class="form-group">
                     			<select name="<%= portletNamespace %>numofthreadgroup" class="field form-control">
                     				<% if(autoCrawlingSetting.getNumofthread() == 1 || autoCrawlingSetting.getNumofthread() == 0){ %>
                     				<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">1</option>
                     				<% } else {  %>
                     			    <option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">1</option>		
									<% } %>
									<% if(autoCrawlingSetting.getNumofthread() == 2){ %>
                     				<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">2</option>
                     				<% } else {  %>
                     			    <option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">2</option>		
									<% } %>
									<% if(autoCrawlingSetting.getNumofthread() == 3){ %>
                     				<option value="3" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">3</option>
                     				<% } else {  %>
                     			    <option value="3" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">3</option>		
									<% } %>
									<% if(autoCrawlingSetting.getNumofthread() == 4){ %>
                     				<option value="4" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">4</option>
                     				<% } else {  %>
                     			    <option value="4" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">4</option>		
									<% } %>
                     				<% if(autoCrawlingSetting.getNumofthread() == 5){ %>
                     				<option value="5" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">5</option>
                     				<% } else {  %>
                     			    <option value="5" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">5</option>		
									<% } %>
									<% if(autoCrawlingSetting.getNumofthread() == 6){ %>
                     				<option value="6" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">6</option>
                     				<% } else {  %>
                     			    <option value="6" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">6</option>		
									<% } %>
									<% if(autoCrawlingSetting.getNumofthread() == 7){ %>
                     				<option value="7" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">7</option>
                     				<% } else {  %>
                     			    <option value="7" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">7</option>		
									<% } %>
									<% if(autoCrawlingSetting.getNumofthread() == 8){ %>
                     				<option value="8" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">8</option>
                     				<% } else {  %>
                     			    <option value="8" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">8</option>		
									<% } %>
									<% if(autoCrawlingSetting.getNumofthread() == 9){ %>
                     				<option value="9" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">9</option>
                     				<% } else {  %>
                     			    <option value="9" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">9</option>		
									<% } %>
									<% if(autoCrawlingSetting.getNumofthread() == 10){ %>
                     				<option value="10" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">10</option>
                     				<% } else {  %>
                     			    <option value="10" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">10</option>		
									<% } %>
                     			</select>
								</div>
                     		</td>
                     	</tr>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-time-out-time" /><span class="required">*</span></td>
                     		<td class="form-field">
								<div class="vextooltip" style="width:100%;opacity:unset;">
									<aui:input type="text" name="timeouttime" value="<%= ((autoCrawlingSetting.getTimeouttime() == 0)  ? 60000 : autoCrawlingSetting.getTimeouttime()) %>" label="" cssClass="field form-control"/>
									<span class="tooltiptext">
										<liferay-ui:message key="message-reminder-time-out-time" />
									</span>
								</div>
							</td>
                     	</tr>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-set-auto-post-form" /></td>
                     		<td>
                     			<table>
                     			<tr class="input-row">
                     				<td class="form-field">
                     			  	<% if(autoCrawlingSetting.getSetautopostform() == PortalConstants.INT_ZERO){ %>
                     				<input class="checkbox" id="setautopostform" name="<%= portletNamespace %>setautopostform" type="checkbox" value="0" onclick="changeValue(this)">
                     				<% } else { %>
                     				<input class="checkbox" id="setautopostform" name="<%= portletNamespace %>setautopostform" type="checkbox" value="1" checked onclick="changeValue(this)">
                     				<% } %>
                     				</td>
                     				<td><div class="form-group with-border" id="form-textarea-auto-post-form" style="height:65px" ><liferay-ui:message key="long-text-auto-post-form" /></div></td>
                     			</tr>
                     			</table>
                     		</td>
                     	</tr>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-set-post-method" /></td>
                     		<td>
                     			<table>
                     			<tr class="input-row">
                     				<td class="form-field">
									<% if(autoCrawlingSetting.getSetpostmethod() == PortalConstants.INT_ZERO){ %>
                     				<input class="checkbox" id="setpostmethod" name="<%= portletNamespace %>setpostmethod" type="checkbox" value="0" onclick="changeValue(this)">
                     				<% } else { %>
                     				<input class="checkbox" id="setpostmethod" name="<%= portletNamespace %>setpostmethod" type="checkbox" value="1" checked onclick="changeValue(this)">
                     				<% } %>
                     			    </td>
                     				<td><div class="form-group with-border" id="form-textarea-post-method" ><liferay-ui:message key="long-text-post-method" /></div></td>
                     			</tr>
                     			</table>
                     		</td>
                     	</tr>
                     	<tr class="input-row">
                     		<td class="form-label"><liferay-ui:message key="label-request-header" /></td>
                     		<td><div class="vextooltip" style="width:100%;opacity:unset;">
									<aui:input type="textarea" name="requestheader" value="<%= autoCrawlingSetting.getRequestheader() %>" label="" cssClass="field form-control form-textarea" style="white-space: nowrap;"/>
									<span class="tooltiptext">
										<liferay-ui:message key="message-reminder-request-header-1" /><br>
										<liferay-ui:message key="message-reminder-request-header-2" />
									</span>
								</div>
							</td>
                     	</tr>
                    </tbody>
				</table>
			</td>
		</tr>
		<tr><td><div style="height:20px"></div></td></tr>
		<tr>
			<td>
				<table class="second-division-table-child-last full-width with-border">
                <tbody>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-num-of-retry-at-error" /><span class="required">*</span></td>
                     	<td class="form-field">
							<div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="text" name="numofretryaterror" value="<%= ((CommonUtil.isStringNullOrEmpty(autoCrawlingSetting.getNumofretryaterror()))  ? 1 : autoCrawlingSetting.getNumofretryaterror()) %>" label="" cssClass="field form-control"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-num-of-retry-at-error" />
								</span>
							</div>
						</td>
                    </tr>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-session-error-detection" /></td>
                     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="textarea" name="sessionerrordetection" value="<%= autoCrawlingSetting.getSessionerrordetection() %>" label="" cssClass="field form-control form-textarea"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-session-error-detection-1" /><br><br>
									<liferay-ui:message key="message-reminder-session-error-detection-2" /><br>
									<liferay-ui:message key="message-reminder-session-error-detection-3" /><br>
									<liferay-ui:message key="message-reminder-session-error-detection-4" />
								</span>
							</div>
						</td>
                     </tr>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-screen-transition-error-detection" /></td>
                     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="textarea" name="screentransitionerrordetection" value="<%= autoCrawlingSetting.getScreentransitionerrordetection() %>" label="" cssClass="field form-control form-textarea"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-screen-transition-error-detection-1" /><br><br>
									<liferay-ui:message key="message-reminder-screen-transition-error-detection-2" /><br>
									<liferay-ui:message key="message-reminder-screen-transition-error-detection-3" /><br>
									<liferay-ui:message key="message-reminder-screen-transition-error-detection-4" />
								</span>
							</div>
						</td>
                    </tr>
      	        </tbody>
                </table>
			</td>
		</tr>
		<tr><td><div style="height:20px"></div></td></tr>
		<tr>
			<td>
				<table class="second-division-table-child-last full-width with-border">
                <tbody>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-path-ignore-value-change" /></td>
                     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="textarea" name="excludepathinforegex" value="<%= autoCrawlingSetting.getExcludepathinforegex() %>" label="" cssClass="field form-control form-textarea"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-exclude-path-info-regex-1" /><br>
									<liferay-ui:message key="message-reminder-exclude-path-info-regex-2" /><br>
									<liferay-ui:message key="message-reminder-exclude-path-info-regex-3" /><br>
									<liferay-ui:message key="message-reminder-exclude-path-info-regex-4" />
								</span>
							</div>
						</td>
                     </tr>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-param-name-ignore-value-change" /></td>
                     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="textarea" name="excludeparameterdeletenameregex" value="<%= autoCrawlingSetting.getExcludeparameterdeletenameregex() %>" label="" cssClass="field form-control form-textarea"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-name-regex-1" /><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-name-regex-2" /><br><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-name-regex-3" /><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-name-regex-4" /><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-name-regex-5" /><br><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-name-regex-6" /><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-name-regex-7" />
								</span>
							</div>
						</td>
                     </tr>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-param-name-ignore-name-and-value-change" /></td>
                     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="textarea" name="excludeparameterdeleteparamregex" value="<%= autoCrawlingSetting.getExcludeparameterdeleteparamregex() %>" label="" cssClass="field form-control form-textarea"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-param-regex-1" /><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-param-regex-2" /><br><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-param-regex-3" /><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-param-regex-4" /><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-param-regex-5" /><br><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-param-regex-6" /><br>
									<liferay-ui:message key="message-reminder-exclude-parameter-delete-param-regex-7" />
								</span>
							</div>
						</td>
                    </tr>
      	        </tbody>
                </table>
			</td>
		</tr>
		<tr><td><div style="height:20px"></div></td></tr>
		<tr>
			<td>
				<table class="second-division-table-child-last full-width with-border">
                <tbody>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-link-extract" /></td>
                     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="textarea" name="extracturlregex" value="<%= autoCrawlingSetting.getExtracturlregex() %>" label="" cssClass="field form-control form-textarea"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-extracturlregex-1" /><br>
									<liferay-ui:message key="message-reminder-extracturlregex-2" /><br><br>
									<liferay-ui:message key="message-reminder-extracturlregex-3" /><br>
									<liferay-ui:message key="message-reminder-extracturlregex-4" /><br>
									<liferay-ui:message key="message-reminder-extracturlregex-5" /><br>
									<liferay-ui:message key="message-reminder-extracturlregex-6" /><br><br>
									<liferay-ui:message key="message-reminder-extracturlregex-7" /><br>
									<liferay-ui:message key="message-reminder-extracturlregex-8" />
								</span>
							</div>
						</td>
                    </tr>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-extension-do-not-link-detect" /></td>
                     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="textarea" name="notparseextension" value="<%= autoCrawlingSetting.getNotparseextension() %>" label="" cssClass="field form-control form-textarea"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-not-parse-extension-1" /><br>
									<liferay-ui:message key="message-reminder-not-parse-extension-2" /><br><br>
									<liferay-ui:message key="message-reminder-not-parse-extension-3" />
								</span>
							</div>
						</td>
					</tr>
      	        	<tr class="input-row">
                     	<td class="form-label"><liferay-ui:message key="label-filename-do-not-link-detect" /></td>
                     	<td><div class="vextooltip" style="width:100%;opacity:unset;">
								<aui:input type="textarea" name="notparsefilenameregex" value="<%= autoCrawlingSetting.getNotparsefilenameregex() %>" label="" cssClass="field form-control form-textarea"/>
								<span class="tooltiptext">
									<liferay-ui:message key="message-reminder-not-parse-file-name-regex-1" /><br>
									<liferay-ui:message key="message-reminder-not-parse-file-name-regex-2" /><br>
									<liferay-ui:message key="message-reminder-not-parse-file-name-regex-3" /><br><br>
									<liferay-ui:message key="message-reminder-not-parse-file-name-regex-4" /><br>
									<liferay-ui:message key="message-reminder-not-parse-file-name-regex-5" />
								</span>
							</div>
						</td>
                    </tr>
      	        </tbody>
                </table>
			</td>
		</tr>
    </table>
    <div class="form-group input-text-wrapper" style="display:none">
		<div style="display:inline-block;margin-right: 10px;"> 
			<label style="vertical-align: bottom;font-weight:normal;"><liferay-ui:message key="label-production-environment" />:</label>
			<aui:input type="radio" name="implementationEnvironment" value="0" style="vertical-align: top;"
				checked="<%= (null == scanAdvancedSetting || scanAdvancedSetting.getImplementationenvironment() == 0) %>" label="" />
		</div>
		<div style="display:inline-block"> 
			<label style="vertical-align: bottom;font-weight:normal;"><liferay-ui:message key="label-verification-environment" />:</label>
			<aui:input type="radio" name="implementationEnvironment" value="1" style="vertical-align: top;" 
				checked="<%= (null != scanAdvancedSetting && scanAdvancedSetting.getImplementationenvironment() == 1) %>" label="" />
		</div>
	</div>
    <div id="confirm">
        <table id ="confirmBoxTable">
        	<tr>
        		<div class="hostMessage">Host name.</div>
        	</tr>
        	<tr>
        		<td>
        		   <img id="confirmBoxImage" src = "/o/ubsecure-theme/images/common/warning.png" width="35" height="35"/>
        		</td>
        		<td>
        		  <div class="message">This is a warning message.</div>
        		</td>
        	</tr>
        	<tr>
        		<td></td>
        		<td>
        			<div class="confirmButtons">
        				<button type="submit">OK</button>
      	 				<button type='button' class="no" onclick="hideConfirmBox();"><liferay-ui:message key="button-cancel" /></button>
      	 				<%-- <a onclick="hideConfirmBox();"><span class="btn cancel-btn" ><liferay-ui:message key="button-cancel" /></span></a> --%>
      	 			</div>
        		</td>
        	</tr>
        </table>
   	</div>
</form>
</div>
		<br>
		<div style="padding-top: 10px;">
		<aui:button name="previous" value="label-back" onClick="goToTargetInfo()" style="width:120px; color:#404040; " />
		<div class="vextooltip" style="padding-left:10px;">
			<%-- <aui:button name="previous" value="button-automatic-patrol" onClick="submitPatrolOnlyConfirmForm()" style="width:120px; color:#404040; " /> --%>
			<aui:button name="previous" value="button-automatic-patrol" onClick="submitPatrolOnlyConfirmForm()" style="width:120px; color:#404040; " />
			<span class="tooltiptext custom-tooltiptext-1"><liferay-ui:message key="message-mouseover-automatic-patrol" /></span>
		</div>
		<div class="vextooltip" style="padding-left:10px;padding-right:10px;">
			<aui:button name="next" value="button-scan" onClick="submitConfirmForm()" style="width:120px; color:#404040; " id="saveHostList"/>
			<span class="tooltiptext custom-tooltiptext-2"><liferay-ui:message key="message-button-mouseover-scan" /></span>
		</div>
			<a id="cancelBtnHref" href="<%= cancelRegisterScanDetailSettingURL.toString() %>" ><span id="cancelBtn" class="btn" style="width:94px; color:#404040;"><liferay-ui:message key="button-cancel" /></span></a>
		</div>
   
<%
	} else {
	 %>
		<div class="alert alert-danger">
			<liferay-ui:message key="message-no-access-rights" />
		</div>
		<%
	}
%>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/vex/register_scan_autocrawling_setting.js"></script>

<script>
	define.amd = define._amd;
</script>
