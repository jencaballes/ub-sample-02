<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.TimeZone" %>

<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="javax.portlet.PortletURL" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanProcess" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResultItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@page import="jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil"%>

<portlet:defineObjects />

<%
// Data from resourceRequest
List<ResultItem> scanList = (List<ResultItem>) resourceRequest.getAttribute(PortalConstants.PARAM_SCAN_LIST);
Map<String, Object> searchedScan = (Map<String, Object>) resourceRequest.getAttribute(PortalConstants.PARAM_SCAN);
// End

// Pagination variables
PortletURL paginationURL = resourceResponse.createRenderURL();
paginationURL.setParameter(PortalConstants.MVC_PATH, PortalConstants.CX_ENTIRE_SCAN_LIST_JSP);
// End

Object oCxDoesNotExistErrorMsg = resourceRequest.getAttribute(PortalConstants.PARAM_CX_DOES_NOT_EXIST_ERROR_MSG);
String strCxDoesNotExistErrorMsg = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oCxDoesNotExistErrorMsg)) {
	strCxDoesNotExistErrorMsg = oCxDoesNotExistErrorMsg.toString();

	if (!strCxDoesNotExistErrorMsg.equals(PortalConstants.CX_REGENERATE_REPORT_FAILED)) {
		strCxDoesNotExistErrorMsg = PortalConstants.CX_REGENERATE_REPORT_FAILED + "(" + strCxDoesNotExistErrorMsg + ")";
	}
}

//Sorting variables
String orderByCol = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_COL);
String orderByType = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_TYPE);

PortletSession pSession = resourceRequest.getPortletSession();

if (orderByType == null || orderByType.isEmpty()) {
	Object oOrderByType = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
	
	if (oOrderByType != null) {
		orderByType = oOrderByType.toString();
		
		if (orderByType.isEmpty()) {
			orderByType = PortalConstants.SORT_ASCENDING;
		}
	} else {
		orderByType = PortalConstants.SORT_ASCENDING;
	}
	
	Object oOrderByCol = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_COL);
	
	if (oOrderByCol != null) {
		orderByCol = oOrderByCol.toString();
		
		if (!orderByCol.isEmpty()) {
			pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
		}
	}
} else {
	pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_TYPE, orderByType);
	pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
}

int totalSize = 0;

if (!CommonUtil.isListNullOrEmpty(scanList)) {
	totalSize = ControllerHelper.getEntireScansCount(ProjectType.CX_SUITE.getInteger(), searchedScan);
}
//End
%>

<input id="orderByCol" type="hidden" value="<%= orderByCol %>"/>
<input id="orderByType" type="hidden" value="<%= orderByType %>"/>
<input id="screenList" type="hidden" value="entire_scan_list" />
<input id="screenNo" type="hidden" value="<%= PortalConstants.SCREEN_ENTIRE_SCAN_LIST %>" />
<input type="hidden" name="cxErrorMessage" id="cxErrorMessage" value="<%= strCxDoesNotExistErrorMsg %>" />

<liferay-ui:search-container emptyResultsMessage="message-no-scans-to-display" orderByType="<%= orderByType %>" orderByCol="<%= orderByCol %>" iteratorURL="<%=paginationURL%>" total="<%= totalSize %>">
	<liferay-ui:search-container-results>
		<%
			int curPage = searchContainer.getCur();	
			int start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
			int end = curPage * PortalConstants.PAGINATION_DELTA;
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				int size = scanList.size();
				
				if (size == 0 && !CommonUtil.isListNullOrEmpty(scanList)) {
					--curPage;
					start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
					end = curPage * PortalConstants.PAGINATION_DELTA;
					request.setAttribute("index-overlap", "");
				}
			
				results = scanList;
			
				pageContext.setAttribute("results", results);
			}
			
			try {
				Integer.parseInt(searchContainer.getCurParam());
				
				paginationURL.setParameter("cur", searchContainer.getCurParam());
			} catch (NumberFormatException e) {
				
			}
		%>
	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row
		className="jp.ubsecure.portal.jubjub.portlet.model.ResultItem"
		keyProperty="scanId" modelVar="scan" escapedModel="<%=true%>">
		<%
			long scanId = scan.getScanId();
			String strScanId = String.format("%05d", scanId);
			
		%>
		<liferay-ui:search-container-column-text
			name="label-id"
			orderable="<%= true %>"
			orderableProperty="scanId"
			value="<%= strScanId %>" />
		
		<liferay-ui:search-container-column-text
			name="label-project-name"
			orderable="<%= true %>"
			orderableProperty="projectName"
			value="<%= HtmlUtil.escapeAttribute(scan.getProjectName()) %>" />
		
		<liferay-ui:search-container-column-text
			name="label-group-name"
			orderable="<%= true %>"
			orderableProperty="groupName"
			value="<%= HtmlUtil.escapeAttribute(scan.getGroupName()) %>" />

		<liferay-ui:search-container-column-text
			name="label-file-name"
			orderable="<%= true %>"
			orderableProperty="fileName"
			value="<%=HtmlUtil.escapeAttribute(scan.getFileName())%>" />

		<liferay-ui:search-container-column-text
			name="label-hash-value"
			orderable="<%= true %>"
			orderableProperty="hashValue"
			value="<%=scan.getHashValue()%>" />

		<%
		String username = scan.getScanManager();
		%>

		<liferay-ui:search-container-column-text
			name="label-scan-manager"
			orderable="<%= true %>"
			orderableProperty="scanManager"
			value="<%=username%>" />

		<%
			DateFormat formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
			formatter.setTimeZone(TimeZone.getTimeZone(PortalConstants.TIMEZONE_TOKYO));
			
			String date = formatter.format(scan.getScanRegistrationDate());
		%>

		<liferay-ui:search-container-column-text
			name="label-scan-registration-date"
			orderable="<%= true %>"
			orderableProperty="scanRegistrationDate"
			value="<%=date%>" />

		<%
			int iStatus = scan.getStatus();
			String strScanStatus = PortalConstants.STRING_EMPTY;
			String strCause = PortalConstants.STRING_EMPTY;
			int iWaitingCount = PortalConstants.INT_ZERO;
			
			if (PortletCommonUtil.isDBConnected()) {
				iWaitingCount = ScanLocalServiceUtil.countScanWaiting(scan.getScanRegistrationDate(), ProjectType.CX_SUITE.getInteger());
			}
			
			if (iStatus == ScanStatus.SCAN_WAITING.getInteger()) {
				strScanStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_WAITING);
				
				if(iWaitingCount > 0) {
					strScanStatus += LanguageUtil.format(request, PortalConstants.KEY_SCAN_STATUS_WAITING_PRIORITY_NUMBER, iWaitingCount, false);
				}
			} else if (iStatus == ScanStatus.SCANNING.getInteger()) {
				strScanStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_ONGOING);
			} else if (iStatus == ScanStatus.REPORT_MAKING.getInteger()
					|| iStatus == ScanStatus.REGENERATING.getInteger()) {
				strScanStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_REPORT_MAKING);
			} else if (iStatus == ScanStatus.COMPLETE.getInteger()) {
				strScanStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_COMPLETE);
				
				if (scan.getReviewFlag() == 1) {
					strScanStatus += LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_REVIEW_DONE);
				}
			} else if (iStatus == ScanStatus.UNDER_REVIEW.getInteger()) {
				strScanStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_UNDER_REVIEW);
			} else if (iStatus == ScanStatus.FAILURE.getInteger()) {
				strScanStatus = LanguageUtil.get(request, PortalConstants.KEY_SCAN_STATUS_FAILURE);
				strCause = scan.getFailedScanCause();
			}
		%>
		
		<liferay-ui:search-container-column-text
			name="label-status"
			orderable="<%= true %>"
			orderableProperty="status" >
			<liferay-ui:message key="<%= strScanStatus %>" />
	
				<%
					if (iStatus == ScanStatus.FAILURE.getInteger() && !CommonUtil.isStringNullOrEmpty(strCause)) {
						%>
						<img src="<%= PortalConstants.FAILED_INFO_ICON_PATH %>" title="<%= strCause %>" />
						<%
					}
				%>
		</liferay-ui:search-container-column-text>
			
		<%
			int process = scan.getProcess();
			String strProcess = PortalConstants.STRING_EMPTY;
			
			switch (process) {
				case 1:
					strProcess = LanguageUtil.get(request, PortalConstants.KEY_SCAN_PROCESS_UNIT_TEST);
					break;
					
				case 2:
					strProcess = LanguageUtil.get(request, PortalConstants.KEY_SCAN_PROCESS_SYSTEM_TEST);
					break;
					
				case 3:
					strProcess = LanguageUtil.get(request, PortalConstants.KEY_SCAN_PROCESS_REGULAR);
					break;
					
				default:
					break;
			}
		%>
		
		<liferay-ui:search-container-column-text
			name="label-process"
			orderable="<%= true %>"
			orderableProperty="process"
			value="<%= strProcess %>" />

		<liferay-ui:search-container-column-jsp align="right" path="<%=PortalConstants.CX_DOWNLOAD_JSP%>" />
		<liferay-ui:search-container-column-jsp name="label-reference-information" align="right" path="<%=PortalConstants.CX_DOWNLOAD_REFERENCE_JSP%>" />
		
		<%
			String strCxScanId = scan.getCxAndroidScanId();
		
			if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
				try {
					if (Long.parseLong(strCxScanId) <= PortalConstants.LONG_ZERO) {
						strCxScanId = PortalConstants.STRING_EMPTY;
					}
				} catch (NumberFormatException e) {
					strCxScanId = PortalConstants.STRING_EMPTY;
				}
			}
		%>
		
		<liferay-ui:search-container-column-text 
			name="label-cx-scan-id"
			orderable="<%= true %>"
			orderableProperty="cxAndroidScanId"
			value="<%= strCxScanId %>" />
		
		<liferay-ui:search-container-column-jsp name="button-action" align="right" path="<%=PortalConstants.CX_ENTIRE_SCAN_ACTION_JSP%>" />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator />
</liferay-ui:search-container>