<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.model.User" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectAttribute" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanProcess" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Scan" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<portlet:defineObjects />

<!-- CxSuite -->

<%
	PortletSession pSession = renderRequest.getPortletSession();
	Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);
	Scan scan = (Scan) renderRequest.getAttribute(PortalConstants.PARAM_SCAN);
	Map<String, String> processMap = (Map<String, String>) renderRequest.getAttribute(PortalConstants.PARAM_PROCESS_MAP);
	Object oScreenNo = (Object) renderRequest.getAttribute(PortalConstants.PARAM_SCREEN_NO);
	Object oFieldNumber = renderRequest.getAttribute(PortalConstants.PARAM_FIELD_NUMBER);
	Object oAttribute = renderRequest.getAttribute(PortalConstants.PARAM_ATTRIBUTE);
	Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
	String strCxServerErrorMsg = null;
	
	if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
		strCxServerErrorMsg = oCxServerErrorMsg.toString();
	}
	
	String strFunctionName = PortalConstants.STRING_EMPTY;
	String strHeaderName = PortalConstants.STRING_EMPTY;
	String strFileName = PortalConstants.STRING_EMPTY;
	String strButtonName = PortalConstants.STRING_EMPTY;
	String strConfirmFunctionName = PortalConstants.STRING_EMPTY;
	long lScanId = PortalConstants.LONG_ZERO;
	int iFieldNumber = PortalConstants.INT_ZERO;
	int iAttribute = PortalConstants.INT_ZERO;
	int iScanProcess = PortalConstants.INT_ZERO;
	
	if (CommonUtil.isObjectNull(scan)) {
		strFunctionName = "addScan";
		strButtonName = "button-scan";
		strHeaderName = "header-scan-registration";
		strConfirmFunctionName = "confirmAddScan()";
	} else {
		strFileName = scan.getFileName();
		lScanId = scan.getScanId();
		iScanProcess = scan.getProcess();
		
		if (lScanId == 0) {
			strFunctionName = "addScan";
			strButtonName = "button-scan";
			strHeaderName = "header-scan-registration";
			strConfirmFunctionName = "confirmAddScan()";
		} else {
			strFunctionName = "updateScan";
			strButtonName = "button-change";
			strHeaderName = "header-scan-change";
			strConfirmFunctionName = "confirmUpdateScan()";
		}
	}
	
	if (!CommonUtil.isObjectNull(oFieldNumber)) {
		iFieldNumber = Integer.parseInt(oFieldNumber.toString());
	}
	
	if (!CommonUtil.isObjectNull(oAttribute)) {
		iAttribute = Integer.parseInt(oAttribute.toString());
	}
	
	int iScreenNo = PortalConstants.INT_ZERO;
	
	if (!CommonUtil.isObjectNull(oScreenNo)) {
		iScreenNo = Integer.parseInt(oScreenNo.toString());
	}
	
	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	httpSession.setAttribute("Current_screen", "CxSuite");
	Object downloadFrom = httpSession.getAttribute("download_from");
	String strDownloadFrom = PortalConstants.STRING_EMPTY;
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	int iUserRole = PortalConstants.INT_ZERO;
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	boolean bUseCxSuite = false;
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
		bUseCxSuite = true;
	} else {
		Object oUseCxSuite = httpSession.getAttribute(PortalConstants.USE_CXSUITE);
		
		if (!CommonUtil.isObjectNull(oUseCxSuite)) {
			bUseCxSuite = Boolean.parseBoolean(oUseCxSuite.toString());
		}
	}
	
	String portletNamespace = null;
	portletNamespace = renderResponse.getNamespace();
	
	Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
	long lUserId = PortalConstants.LONG_ZERO;

	if (!CommonUtil.isObjectNull(oUserId)) {
		lUserId = Long.parseLong(oUserId.toString());
	}

	User user = null;
	
	try {
		user = ControllerHelper.getUser(lUserId);
	} catch (UBSPortalException e) {
		if (e.getErrorCode().equalsIgnoreCase(PortalErrors.ORM_EXCEPTION)) {
			if (SessionErrors.isEmpty(request) && SessionMessages.isEmpty(request)) {
				SessionErrors.add(request, PortalMessages.ORM_EXCEPTION);
			}
		}
	}
	
	String emailAddress = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(user)) {
		emailAddress = user.getEmailAddress();
	}

	long lProjectId = PortalConstants.LONG_ZERO;

	if (!CommonUtil.isObjectNull(project)) {
		lProjectId = project.getProjectId();
	}

	boolean bCanAccess = false;
	bCanAccess = ControllerHelper.canAccessScanList(emailAddress, lProjectId);

	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
		bCanAccess = true;
	}
	
	Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
		strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
	}
	
	//Previous and current screen
	Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
	String strCurrScreen = PortalConstants.STRING_EMPTY;
	
	if (!CommonUtil.isObjectNull(oCurrScreen)) {
		strCurrScreen = oCurrScreen.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
			pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
		}
	}
	
	pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_REGISTRATION);
	// End
	
	pSession.removeAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	
	Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
	String strManualDownloadError = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oManualDownloadError)) {
		strManualDownloadError = oManualDownloadError.toString();
	}

	if (!CommonUtil.isObjectNull(downloadFrom)){
		strDownloadFrom = downloadFrom.toString();
	}
		
	if (bUseCxSuite && bCanAccess) {
		boolean bErrorFromCxSuite = SessionErrors.contains(renderRequest, PortalMessages.CX_REGISTER_SCAN_FAILED);
%>

<div
	style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px;">
	<liferay-ui:message key="<%= strHeaderName %>" />
</div>

<div style="height: 14px;">
	<hr
		style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>

<div>
	<%
	if (bErrorFromCxSuite) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="<%= PortalMessages.CX_REGISTER_SCAN_FAILED %>" /><liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
		</div>
		<%
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("CxSuite")) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
		</div>
	<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("CxSuite")) {
		%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
			</div>
		<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	}
	%>
	
	<liferay-ui:error key="<%= PortalMessages.USER_ID_INVALID %>" message="<%= PortalMessages.USER_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	<liferay-ui:error key="<%= PortalMessages.USER_INVALID %>" message="<%= PortalMessages.USER_INVALID %>" />
	
	<liferay-ui:error key="<%= PortalMessages.PROJECT_INVALID %>" message="<%= PortalMessages.PROJECT_INVALID %>" />
	
	<liferay-ui:error key="<%= PortalMessages.FILE_INVALID %>" message="<%= PortalMessages.FILE_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.ZIP_FILE_SIZE_TOO_BIG %>" message="<%= PortalMessages.ZIP_FILE_SIZE_TOO_BIG %>" />
	
	<liferay-ui:error key="<%= PortalMessages.NO_PROCESS %>" message="<%= PortalMessages.NO_PROCESS %>" />
	
	<liferay-ui:error key="<%= PortalMessages.ADD_SCAN_FAILED %>" message="<%= PortalMessages.ADD_SCAN_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.UPDATE_SCAN_FAILED %>" message="<%= PortalMessages.UPDATE_SCAN_FAILED %>" />
	
	<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
	
	<liferay-ui:error key="<%= PortalErrors.CX_SERVER_ERROR %>" message="<%= strCxServerErrorMsg %>" />
	<%-- <liferay-ui:error key="<%= PortalMessages.CX_REGISTER_SCAN_FAILED %>" message="<%= PortalMessages.CX_REGISTER_SCAN_FAILED %>" /> --%>
	<liferay-ui:error key="<%= PortalMessages.CX_SESSION_ID_INVALID %>" message="<%= PortalMessages.CX_SESSION_ID_INVALID %>" />
</div>

<%
String projectName = PortalConstants.STRING_EMPTY;

if (project != null) {
	lProjectId = project.getProjectId();
	projectName = project.getProjectName();
	iAttribute = project.getAttribute();
}
%>
<portlet:actionURL name="<%= strFunctionName %>" var="updateScanURL">
	<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	<portlet:param name="scanId" value="<%= String.valueOf(lScanId) %>" />
	<portlet:param name="screenNo" value="<%= String.valueOf(iScreenNo) %>" />
	<portlet:param name="attribute"	value="<%= String.valueOf(iAttribute) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewScanList" var="viewScanListURL">
	<portlet:param name="projectId"
		value="<%=String.valueOf(lProjectId)%>" />
</portlet:actionURL>

<form action="<%= updateScanURL.toString() %>" method="post" id="fm" enctype="multipart/form-data" class="edit-scan-form">
	<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
	<input type="hidden" id="fieldNumber" value="<%= iFieldNumber %>" />
	<table>
		<tr>
			<td style="padding: 20px 20px 10px 0px"><liferay-ui:message key="label-project-colon" /></td>
			<td style="padding: 20px 0px 10px 0px"><%= HtmlUtil.escapeAttribute(projectName) %></td>
		</tr>

		<tr>
			<td style="padding: 0px 20px 10px 0px"><liferay-ui:message key="label-inspection-file" /><span style="color: red;">*</span></td>
			<% if (scan == null || lScanId == 0) { %>
				<td style="padding: 0px 0px 10px 0px">
					<button type="button" name="browseBtn" id="browseBtn" class="btn" value="button-browse" id="btn-file" onclick="clickFile()" style="margin-top: 0;">
						<liferay-ui:message key="button-browse" />
						</button>&nbsp;&nbsp;
					<%
					if (strFileName != null && !strFileName.equals("")) {
						%>
						<aui:input type="hidden" id="fileName" name="fileName" value="<%= strFileName %>" />
						<span id="strFile" ><%= strFileName %></span>
						<%
					} else {
						%>
						<span id="strFile" ><liferay-ui:message key="message-file-not-selected" /> </span>
						<%
					}
				%>			
				
				</td>
				
				<td>
					<div class="hideInput" style="display:none;">
						<aui:input type="file" name="inspectionFile" value="<%=strFileName%>" accept="<%=PortalConstants.CONTENT_TYPE_ZIP%>" onchange="sendValue(this)" />
					</div>
				</td>
			<% } else { %>
				<td><%= scan.getFileName() %></td>
			<% } %>
		</tr>
	</table>

	<div style="position: relative; float: left; width: 102px; padding: 0px 0px 10px 0px;">
		<liferay-ui:message key="label-process" /><span style="color: red;">*</span>
	</div>

	<%
		List<Integer> process = new ArrayList<Integer>();
		List<String> processList = new ArrayList<String>();
		List<String> processExplanation = new ArrayList<String>();
		
		
		
		if (iAttribute == ProjectAttribute.NEW.getInteger()) {
			process.add(ScanProcess.UNIT_TEST.getInteger());
			processList.add(processMap.get("cxsuite.processUT.name"));
			processExplanation.add(processMap.get("cxsuite.processUT.description"));
			process.add(ScanProcess.SYSTEM_TEST.getInteger());
			processList.add(processMap.get("cxsuite.processST.name"));
			processExplanation.add(processMap.get("cxsuite.processST.description"));
		} else if (iAttribute == ProjectAttribute.REGULAR.getInteger()) {
			process.add(ScanProcess.REGULAR.getInteger());
			processList.add(processMap.get("cxsuite.processPD.name"));
			processExplanation.add(processMap.get("cxsuite.processPD.description"));
		}
		
		int counter = PortalConstants.INT_ZERO;
	%>

	<div style="position: relative; float: left; margin-top: -20px; padding: 0px 0px 10px 0px;" class="process">
		<liferay-ui:search-container delta="100">
			<liferay-ui:search-container-results results="<%= processList %>" />
			<liferay-ui:search-container-row className="String"
				escapedModel="<%=true%>">

				<liferay-ui:search-container-column-text>
					<%
						boolean bIsChecked = false;
					
						if (scan != null && scan.getProcess() == process.get(counter) || iAttribute == ProjectAttribute.REGULAR.getInteger()) {
							bIsChecked = true;
						}
						
						String selectProcessFunction = "selectProcess(" + process.get(counter) + ")";
					%>
					<aui:input type="radio" id="process" name="process" value="<%= String.valueOf(process.get(counter)) %>" label="" checked="<%= bIsChecked %>" onChange="<%= selectProcessFunction %>" />
				</liferay-ui:search-container-column-text>

				<liferay-ui:search-container-column-text name="label-process"
					value="<%=processList.get(counter)%>" />

				<liferay-ui:search-container-column-text
					name="label-process-description"
					value="<%=processExplanation.get(counter)%>" />
				<%
					counter++;
				%>
			</liferay-ui:search-container-row>

			<liferay-ui:search-iterator />
		</liferay-ui:search-container>
	</div>

	<aui:button-row>
		<button type="button" name="saveBtn" id="saveBtn" class="btn" value="<%= strButtonName %>" onClick="<%= strConfirmFunctionName %>" style="width:120px; color:#404040;" ><liferay-ui:message key="<%= strButtonName %>" /></button>&nbsp;&nbsp;
		<a id="cancelBtnHref" href="<%= viewScanListURL.toString() %>" ><span id="cancelBtn" class="btn" onClick="cancelAddUpdateScan()" style="width:94px; color:#404040;"><liferay-ui:message key="button-cancel" /></span></a>
	</aui:button-row>
	
	<span style="color: red;"><liferay-ui:message key="message-reminder-200MB" /></span>
	<br />
	<span style="color: red;"><liferay-ui:message key="message-reminder-scan-reservation" /></span>
	<br />
	<span style="color: red;"><liferay-ui:message key="message-reminder-zip-file-only" /></span>
	<br />
	<span style="color: red;"><liferay-ui:message key="message-no-of-lines-per-scan-1" /></span>
	<br />
	<span style="color: red;"><liferay-ui:message key="message-no-of-lines-per-scan-2" /></span>
</form>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/cxsuite/scan_list.js"></script>
<script>
	define.amd = define._amd;
</script>
<script type="text/javascript">
	function sendValue(){
		var path = $("#<portlet:namespace />inspectionFile").val();
		var fileName = path.split("\\");
		var file = fileName[fileName.length - 1];
		var formAction = document.getElementById("fm").action;
		var scanProcess = <%= iScanProcess %>;
		var scanProcessAction = null;
		
		if (formAction.indexOf("hasFileUpload") > -1) {
			if (formAction.indexOf("process") < 0 && scanProcess != 0) {
				scanProcessAction = "&process=" + scanProcess;
			} else if (formAction.indexOf("process") > -1) {
				scanProcessAction = "&process=" + formAction.split("&process=")[1];
				
				if (scanProcessAction.indexOf("hasFileUpload") > -1) {
					scanProcessAction = scanProcessAction.split("&hasFileUpload")[0];
				}
			}
			
			formAction = formAction.split("&hasFileUpload")[0];
		} else {
			if (formAction.indexOf("process") < 0) {
				scanProcessAction = "&process=" + scanProcess;
			}
		}
		
		if (formAction.indexOf("process") < 0 && scanProcessAction != null) {
			formAction = formAction + scanProcessAction;
		}
		
		if (file == null || file == "") {
			$("#strFile").html("<liferay-ui:message key='message-file-not-selected' />");
			document.getElementById("fm").action = formAction + "&hasFileUpload=" + false;
		} else {
			$("#strFile").text(file);
			document.getElementById("fm").action = formAction + "&hasFileUpload=" + true;
		}
	}
	
	function clickFile(){
		$("#<portlet:namespace />inspectionFile").trigger('click');
	}
	
	function selectProcess (scanProcess) {
		var formAction = document.getElementById("fm").action;
		var formActionArr = null;
		var actionHasFileUpload = null;
		var actionToAppend = "";
		
		if (formAction.indexOf("process") > -1) {
			formActionArr = formAction.split("&process");
			
			formAction = formActionArr[0];
			
			if (formActionArr != null) {
				if (formActionArr[0].indexOf("hasFileUpload") > -1) {
					formAction = formActionArr[0].split("&hasFileUpload")[0];
					actionHasFileUpload = formActionArr[0].split("&hasFileUpload")[1];
				} else if (formActionArr[1].indexOf("hasFileUpload") > -1) {
					formAction = formActionArr[0].split("&hasFileUpload")[0];
					actionHasFileUpload = formActionArr[1].split("&hasFileUpload")[1];
				}
				
				if (actionHasFileUpload != null) {
					actionToAppend += "&hasFileUpload" + actionHasFileUpload;
				}
				
				actionToAppend += "&process=" + scanProcess;
			}
		} else {
			actionToAppend += "&process=" + scanProcess;
		}
		
		document.getElementById("fm").action = formAction + actionToAppend;
	}
</script>

<%
	} else {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="message-no-access-rights" />
		</div>
		<%
	}
%>