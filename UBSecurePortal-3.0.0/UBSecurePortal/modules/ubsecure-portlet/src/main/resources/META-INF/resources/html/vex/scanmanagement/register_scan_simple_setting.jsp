<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List"%>

<%@ page import="javax.portlet.PortletSession"%>

<%@ page import="jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.ApiSignatureSetInfo" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Scan" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformationItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >

<portlet:defineObjects />

<!-- Vex -->

<%
	//Previous and current screen
	PortletSession pSession = renderRequest.getPortletSession();
	Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
	String strCurrScreen = PortalConstants.STRING_EMPTY;
	Object oProjectId = pSession.getAttribute(PortalConstants.PARAM_PROJECT_ID);
	Object oScanId = pSession.getAttribute(PortalConstants.PARAM_SCAN_ID);

	long lProjectId = PortalConstants.LONG_ZERO;
	long lScanId = PortalConstants.LONG_ZERO;
	
	if (!CommonUtil.isObjectNull(oCurrScreen)) {
		strCurrScreen = oCurrScreen.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
			pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
		}
	}
	
	if (!CommonUtil.isObjectNull(oProjectId)) {
		lProjectId = Long.parseLong(oProjectId.toString());
	}
	
	if (!CommonUtil.isObjectNull(oScanId)) {
		lScanId = Long.parseLong(oScanId.toString());
	}
	
	pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_SIMPLE_SETTING_REGISTRATION);
	// End
	
	// Show error indicators
	boolean bShowDBConnError = false;
	
	// Get project details
	Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);
	// End
	
	String projectName = PortalConstants.STRING_EMPTY;
	int status = PortalConstants.INT_ZERO;
	
	if (!CommonUtil.isObjectNull(project)) {
		projectName = project.getProjectName();
		status = project.getStatus();
	}

	String portletNamespace = null;
	int iUserAction = PortalConstants.INT_ZERO;
	portletNamespace = renderResponse.getNamespace();
	
	iUserAction = PortalConstants.USER_EVENT_REGISTER_SCAN_SIMPLE_SETTING;
	//TODO..Removed
// 	List<ApiSignatureSetInfo> webInspectionSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST);
//	List<ApiSignatureSetInfo> serverFilesSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST);
//	List<ApiSignatureSetInfo> serverSettingsSignatureSetList = (List<ApiSignatureSetInfo>) renderRequest.getAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST);
 
	Scan scan = (Scan) renderRequest.getAttribute(PortalConstants.PARAM_SCAN);
	VexScanSetting scanSetting = (VexScanSetting) renderRequest.getAttribute(PortalConstants.PARAM_SCAN_SETTING);
	List<VexTargetInformationItem> targetInformationList = (List<VexTargetInformationItem>) renderRequest.getAttribute(PortalConstants.PARAM_TARGET_INFORMATION_LIST);
	List<VexLoginSettingItem> loginSettingList = (List<VexLoginSettingItem>) renderRequest.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
	
	String startTimeFormat = PortalConstants.STRING_EMPTY;
	String endTimeFormat = PortalConstants.STRING_EMPTY;
	
	DateFormat formatter = new SimpleDateFormat(PortalConstants.TIME_FORMAT_HH_MM);
	
	if (null != scanSetting) {
		if (null != scanSetting.getStarttime() && null != scanSetting.getEndtime()) {
			startTimeFormat = formatter.format(scanSetting.getStarttime());
			endTimeFormat = formatter.format(scanSetting.getEndtime());
		}
	}
	
	String paramStartTime = renderRequest.getParameter(PortalConstants.PARAM_START_TIME);
	String paramEndTime = renderRequest.getParameter(PortalConstants.PARAM_END_TIME);
	String paramStartURL = (String) renderRequest.getAttribute(PortalConstants.PARAM_START_URL);
	String paramAuthorizedPatrolURL = (String) renderRequest.getAttribute(PortalConstants.PARAM_AUTHORIZED_PATROL_URL);
	
	if (null == paramStartTime) {
		paramStartTime = PortalConstants.STRING_EMPTY;
	}
	
	if (null == paramEndTime) {
		paramEndTime = PortalConstants.STRING_EMPTY;
	}
	
	if (null == paramStartURL) {
		paramStartURL = PortalConstants.STRING_EMPTY;
	}
	
	if (null == paramAuthorizedPatrolURL) {
		paramAuthorizedPatrolURL = PortalConstants.STRING_EMPTY;
	}
	
	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	httpSession.setAttribute("Current_screen", "VEX");
	int iUserRole = 0;
	long lUserId = 0L;
	boolean bUseVEX = false;
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
	String labelParameterName  = "parameterName";
	String labelParameterValue = "parameterValue";
	String checkItem = "checkItem";
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	
	if (!CommonUtil.isObjectNull(oUserId)) {
		lUserId = Long.parseLong(oUserId.toString());
	}
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
		bUseVEX = true;
	} else {
		Object oUseVEX = httpSession.getAttribute(PortalConstants.USE_VEX);
		
		if (!CommonUtil.isObjectNull(oUseVEX)) {
			bUseVEX = Boolean.parseBoolean(oUseVEX.toString());
		}
	}
	
	Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
		strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
	}

	Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
	String strCxServerErrorMsg = PortalConstants.STRING_EMPTY;
	
	if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
		strCxServerErrorMsg = oCxServerErrorMsg.toString();
	}
	
	pSession.removeAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	
	if (bUseVEX && (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger() || iUserRole == UserRole.GEN_USER.getInteger())) {
		boolean bErrorFromVex = SessionErrors.contains(renderRequest, PortalMessages.VEX_REGISTER_SCAN_FAILED);
%>

<portlet:actionURL name="viewScanList" var="cancelURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<portlet:param name="type" value="<%= String.valueOf(ProjectType.VEX.getInteger()) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanAdvancedSetting" var="viewRegisterScanAdvancedSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewRegisterScanSimpleSetting" var="viewRegisterScanSimpleSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<portlet:actionURL name="registerScanSimpleSetting" var="registerScanSimpleSettingURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	<portlet:param name="userAction" value="<%= String.valueOf(iUserAction) %>" />
</portlet:actionURL>

<portlet:actionURL name="acquireLoginInformationSimpleSetting" var="acquireLoginInformationURL">
	<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
</portlet:actionURL>

<div style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px">
	<liferay-ui:message key="header-scan-registration" />
</div>
<style>
#confirm {
   display: none;
   background-color: #FFFFFF;
   border: 1px solid #aaa;
   position: fixed;
   width: 400px;
   height: auto;
   left: 35%;
   z-index: 9999;
   padding: 6px 8px 8px;
   box-sizing: border-box;
   text-align: center;
   top: 0;
}
#confirm button {
   background-color: #489de5;
   display: inline-block;
   border-radius: 5px;
   border: 1px solid #aaa;
   padding: 0px;
   text-align: center;
   width: 80px;
   cursor: pointer;
   color:white;
   font-size: 13px !important;
}
#confirm button {
   background-color: #489de5;
   display: inline-block;
   border-radius: 5px;
   border: 1px solid #aaa;
   padding: 0px;
   text-align: center;
   width: 80px;
   cursor: pointer;
   color:white;
   font-size: 13px !important;
}
#confirm .message {
   text-align: left;
   font-size: 13px;
   margin-bottom: 15px;
   margin-top: 15px;
}

#confirm .hostMessage{
   text-align: left;
   font-size: 13px;
   margin-left:10px;
   margin-bottom: 10px;
}

#confirm .no{
	background-color: #FFFFFF;
    color: black;
    margin-left: 10px;
    width: 85px;
}

#confirmBoxTable{
	width: 100%;
}

.confirmButtons{
	text-align: right;
}

#overlay {
     visibility: hidden;
     position: absolute;
     left: 0px;
     top: 0px;
     width:100%;
     height:100%;
     text-align:center;
     z-index: 9998;
     background-image:url(background-trans.png);
}

#overlay div {
     width:300px;
     margin: 100px auto;
     background-color: #fff;
     border:1px solid #000;
     padding:15px;
     text-align:center;
}
.custom-tooltiptext-1{
	width: 200% !important;
	left: 40% !important;
}
.custom-tooltiptext-2{
	width: 200% !important;
	left: 25% !important;
}
</style>

<div style="height: 14px;">
	<hr style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>

	<div class="tabbable-content instantiated" id="portlet-set-properties">
		<ul class="lfr-nav nav nav-tabs nav-tabs-default " role="tablist">
			<li class="tab yui3-widget tab-focused active tab-selected" role="presentation"><a href="#" role="tab" tabindex="0"><liferay-ui:message key="button-simple-setting" /></a></li>
		    <li class="tab yui3-widget" role="presentation"><a href="<%=viewRegisterScanAdvancedSettingURL %>" role="tab" tabindex="-1"><liferay-ui:message key="button-advanced-setting" /></a></li>
			<%--<li class="tab yui3-widget" role="presentation"><a href="" role="tab" tabindex="-1"><liferay-ui:message key="button-advanced-setting" /></a></li>--%>
		</ul>
	</div>

<div>

	<%
	/* if (bErrorFromVex) { */
		%>
		<%-- <div class="alert alert-danger">
			<liferay-ui:message key="<%= PortalMessages.VEX_REGISTER_SCAN_FAILED %>" /><liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
		</div> --%>
		<%
	/* } */
	%>
	
	<liferay-ui:success key="<%= PortalConstants.REGISTER_SCAN_SUCCESSFUL %>" message="<%= PortalMessages.REGISTER_SCAN_SUCCESSFUL %>" />

	<liferay-ui:error key="<%= PortalMessages.PROJECT_ID_INVALID %>" message="<%= PortalMessages.PROJECT_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" message="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" />
	<liferay-ui:success key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	
    <liferay-ui:error key="<%= PortalMessages.ADD_SCAN_FAILED %>" message="<%= PortalMessages.ADD_SCAN_FAILED %>" />
    <liferay-ui:error key="<%= PortalMessages.ADD_VEX_SCAN_FAILED %>" message="<%= PortalMessages.ADD_VEX_SCAN_FAILED %>" />
    <liferay-ui:error key="<%= PortalMessages.AUTO_CRAWL_FAILED %>" message="<%= PortalMessages.AUTO_CRAWL_FAILED %>" />
	
	<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
	
	<liferay-ui:error key="<%= PortalErrors.CX_SERVER_ERROR %>" message="<%= strCxServerErrorMsg %>" />
	<liferay-ui:error key="<%= PortalMessages.CX_SESSION_ID_INVALID %>" message="<%= PortalMessages.CX_SESSION_ID_INVALID %>" />
	
	<liferay-ui:error key="<%= PortalMessages.PRESET_ID_INVALID %>" message="<%= PortalMessages.PRESET_ID_INVALID %>" />
	
	<liferay-ui:error key="<%= PortalMessages.SCAN_NAME_INVALID %>" message="<%= PortalMessages.SCAN_NAME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_SCAN_NAME %>" message="<%= PortalMessages.NO_SCAN_NAME %>" />
	<liferay-ui:error key="<%= PortalMessages.START_TIME_AND_END_TIME_INVALID %>" message="<%= PortalMessages.START_TIME_AND_END_TIME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.START_TIME_INVALID %>" message="<%= PortalMessages.START_TIME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.END_TIME_INVALID %>" message="<%= PortalMessages.END_TIME_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.START_URL_INVALID %>" message="<%= PortalMessages.START_URL_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.AUTHORIZED_PATROL_URL_INVALID %>" message="<%= PortalMessages.AUTHORIZED_PATROL_URL_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.LOGIN_URL_INVALID %>" message="<%= PortalMessages.LOGIN_URL_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.START_URL_LOCALHOST_INVALID %>" message="<%= PortalMessages.START_URL_LOCALHOST_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.AUTHORIZED_PATROL_URL_LOCALHOST_INVALID %>" message="<%= PortalMessages.AUTHORIZED_PATROL_URL_LOCALHOST_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.LOGIN_URL_LOCALHOST_INVALID %>" message="<%= PortalMessages.LOGIN_URL_LOCALHOST_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.PARAMETER_NAME_ID_INVALID %>" message="<%= PortalMessages.PARAMETER_NAME_ID_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.PARAMETER_NAME_PASS_INVALID %>" message="<%= PortalMessages.PARAMETER_NAME_PASS_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_START_URL %>" message="<%= PortalMessages.NO_START_URL %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_AUTHORIZED_PATROL_URL %>" message="<%= PortalMessages.NO_AUTHORIZED_PATROL_URL %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_LOGIN_URL %>" message="<%= PortalMessages.NO_LOGIN_URL %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_PARAMETER_NAME_ID %>" message="<%= PortalMessages.NO_PARAMETER_NAME_ID %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_PARAMETER_NAME_PASS %>" message="<%= PortalMessages.NO_PARAMETER_NAME_PASS %>" />
	<liferay-ui:error key="<%= PortalMessages.PROTOCOL_INVALID %>" message="<%= PortalMessages.PROTOCOL_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.HOST_INVALID %>" message="<%= PortalMessages.HOST_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.HOST_LOCALHOST_INVALID %>" message="<%= PortalMessages.HOST_LOCALHOST_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.PORT_INVALID %>" message="<%= PortalMessages.PORT_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_HOST %>" message="<%= PortalMessages.NO_HOST %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_PORT %>" message="<%= PortalMessages.NO_PORT %>" />
	<liferay-ui:error key="<%= PortalMessages.TARGET_INFORMATION_LIST_INVALID %>" message="<%= PortalMessages.TARGET_INFORMATION_LIST_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_TARGET_INFORMATION_LIST %>" message="<%= PortalMessages.NO_TARGET_INFORMATION_LIST %>" />

	<liferay-ui:error key="<%= PortalMessages.FILE_NOT_FOUND_EXCEPTION %>" message="<%= PortalMessages.FILE_NOT_FOUND_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.IO_EXCEPTION %>" message="<%= PortalMessages.IO_EXCEPTION %>" />
	<liferay-ui:error key="<%= PortalMessages.MALFORMEDURL_EXCEPTION %>" message="<%= PortalMessages.MALFORMEDURL_EXCEPTION %>" />

	<liferay-ui:error key="<%= PortalMessages.GET_CRAWL_LOGIN_INFO_FAILED %>" message="<%= PortalMessages.GET_CRAWL_LOGIN_INFO_FAILED %>" />
	<liferay-ui:error key="<%= PortalMessages.LOGIN_URL_NOT_FOUND_IN_TARGET_INFORMATION_LIST %>" message="<%= PortalMessages.LOGIN_URL_NOT_FOUND_IN_TARGET_INFORMATION_LIST %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_LOGIN_PARAMETER_VALUE %>" message="<%= PortalMessages.NO_LOGIN_PARAMETER_VALUE %>" />
	<liferay-ui:error key="<%= PortalMessages.LOGIN_PARAM_VALUE_INVALID%>" message="<%= PortalMessages.LOGIN_PARAM_VALUE_INVALID %>" />
	<liferay-ui:error key="<%= PortalMessages.UNUSABLE_CHARACTER_INPUT%>" message="<%= PortalMessages.UNUSABLE_CHARACTER_INPUT %>" />
	<liferay-ui:error key="<%= PortalMessages.INVALID_CHARACTER_INPUT%>" message="<%= PortalMessages.INVALID_CHARACTER_INPUT %>" />
	
	<liferay-ui:error key="<%= PortalMessages.NO_WEB_INSPECTION_SIGNATURE_SET_GROUP %>" message="<%= PortalMessages.NO_WEB_INSPECTION_SIGNATURE_SET_GROUP %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_SERVER_FILE_SIGNATURE_SET_GROUP %>" message="<%= PortalMessages.NO_SERVER_FILE_SIGNATURE_SET_GROUP %>" />
	<liferay-ui:error key="<%= PortalMessages.NO_SERVER_SETTING_SIGNATURE_SET_GROUP %>" message="<%= PortalMessages.NO_SERVER_SETTING_SIGNATURE_SET_GROUP %>" />
	<liferay-ui:error key="<%= PortalMessages.START_TIME_END_TIME_DIFFERENCE %>" message="<%= PortalMessages.START_TIME_END_TIME_DIFFERENCE %>" />

</div>
<div style="width: 100%; position: relative;">
	<form action="<%= registerScanSimpleSettingURL %>" method="post" id="fm" class="edit-scan-advanced-setting-form">
		<input type="hidden" id="screenURL" name="screenURL" value="<%= viewRegisterScanSimpleSettingURL %>" />
		<input type="hidden" id="scanSettingStartTime" value="<%= startTimeFormat %>" />
		<input type="hidden" id="scanSettingEndTime" value="<%= endTimeFormat %>" />
		<input type="hidden" id="paramStartTime" value="<%= paramStartTime %>" />
		<input type="hidden" id="paramEndTime" value="<%= paramEndTime %>" />
		<input type="hidden" id="scanId" value="<%= lScanId %>" />
		<aui:input type="hidden" name="setCrawlingOnly" value=""/>
							
		<table>
			<tbody>
				<tr class="input-row">
					<td style="width: 150px;"><span style="text-decoration: underline;"><liferay-ui:message key="header-scan-registration" /></span></td>
					<td></td>
					<td style="padding-left: 100px;"><span style="text-decoration: underline;"><liferay-ui:message key="label-timer-setting" /></span></td>
					</tr>
				<tr class="input-row">
					<td><span id="labelProjectName"><liferay-ui:message key="label-project-name" /></span>:</td>
					<td><%= HtmlUtil.escapeAttribute(projectName) %></td>
					<td style="padding-left: 100px;"><liferay-ui:message key="label-start-time" /></td>
					<td>
						<% 
							String[] arrTime;
							int hourValue = 0;
							int minuteValue = 0;
							
							if (paramStartTime.length() > 0) {
								arrTime = paramStartTime.split(":");
								hourValue = Integer.parseInt(arrTime[0]);
								minuteValue = Integer.parseInt(arrTime[1]);
							}
							
							if (startTimeFormat.length() > 0) {
								arrTime = startTimeFormat.split(":");
								hourValue = Integer.parseInt(arrTime[0]);
								minuteValue = Integer.parseInt(arrTime[1]);
							}
						
							if (paramStartTime.length() > 0 || startTimeFormat.length() > 0) {
						%>	
							<liferay-ui:input-time name="startTime" amPmParam="" hourParam="hour" hourValue="<%= hourValue %>" minuteParam="minute" minuteValue="<%= minuteValue %>" minuteInterval="1" timeFormat="24-hour" />
						<% } else { %>
							<liferay-ui:input-time name="startTime" amPmParam="" hourParam="hour" hourValue="0" minuteParam="minute" minuteValue="0" minuteInterval="1" timeFormat="24-hour" />
						<% } %>
					
					</td>
				</tr>
				<tr class="input-row">
					<td><span><liferay-ui:message key="label-scan-name" />:</span><span style="color: red;">*</span></td>
					<td>
						<% if (null != scan) {  %>	
							<aui:input type="text" style="width: 100%;" name="scanName" value="<%= scan.getFileName() %>" label="" />
						<% } else { %>
							<aui:input type="text" style="width: 100%;" name="scanName" value="" label="" />
						<% } %>
					</td>
					<td style="padding-left: 100px;"><liferay-ui:message key="label-end-time" /></td>
					<td>
						<% 
							if (paramEndTime.length() > 0) {
								arrTime = paramEndTime.split(":");
								hourValue = Integer.parseInt(arrTime[0]);
								minuteValue = Integer.parseInt(arrTime[1]);
							}
						
							if (endTimeFormat.length() > 0) {
								arrTime = endTimeFormat.split(":");
								hourValue = Integer.parseInt(arrTime[0]);
								minuteValue = Integer.parseInt(arrTime[1]);
							}
						
							if (paramEndTime.length() > 0 || endTimeFormat.length() > 0) {
						%>	
							<liferay-ui:input-time name="endTime" amPmParam="" hourParam="hour" hourValue="<%= hourValue %>" minuteParam="minute" minuteValue="<%= minuteValue %>" minuteInterval="1" timeFormat="24-hour" />
						<% } else { %>
							<liferay-ui:input-time name="endTime" amPmParam="" hourParam="hour" hourValue="0" minuteParam="minute" minuteValue="0" minuteInterval="1" timeFormat="24-hour" />
						<% } %>
					</td>
				</tr>

				<tr class="input-row">
					<td><span style="text-decoration: underline;"><liferay-ui:message key="label-target-information" /></span></td>
					<td></td>
					<td style="vertical-align: top;padding-top: 7px;padding-left: 100px; padding-right:50px;"><span><liferay-ui:message key="label-email-notification" /></span><span style="color: red;">*</span></td>
					<td style="vertical-align: top;">
						<div class="form-group input-text-wrapper">
							<div style="display:inline-block;margin-right: 10px;">
								<label style="vertical-align: bottom;font-weight:normal">ON : &nbsp;</label>
								<aui:input type="radio" name="emailNotification" value="1" style="vertical-align: top;" checked="<%= (null == scanSetting || scanSetting.getSetemailnotification() == 1) %>" label="" />
							</div>
							<div style="display:inline-block">
								<label style="vertical-align: bottom;font-weight:normal">OFF : &nbsp;</label>
								<aui:input type="radio" name="emailNotification" value="0" style="vertical-align: top;" checked="<%= (null != scanSetting && scanSetting.getSetemailnotification() == 0) %>" label="" />
							</div>
						</div>
					</td>
				</tr>

				<tr class="input-row">
					<td><span><liferay-ui:message key="label-implementation-environment" /></span><span style="color: red;">*</span></td>
					<td>
						<div class="form-group input-text-wrapper">
							<div style="display:inline-block;margin-right: 10px;"> 
								<label style="vertical-align: bottom;font-weight:normal;"><liferay-ui:message key="label-production-environment" />:</label>
								<aui:input type="radio" name="implementationEnvironment" value="0" style="vertical-align: top;" checked="<%= (null == scanSetting || scanSetting.getImplementationenvironment() == 0) %>" label="" />
							</div>
							<div style="display:inline-block"> 
								<label style="vertical-align: bottom;font-weight:normal;"><liferay-ui:message key="label-verification-environment" />:</label>
								<aui:input type="radio" name="implementationEnvironment" value="1" style="vertical-align: top;" checked="<%= (null != scanSetting && scanSetting.getImplementationenvironment() == 1) %>" label="" />
							</div>
						</div>
					</td>
					<%-- <td style="vertical-align: top;padding-top: 7px;padding-left: 100px; padding-right:50px;">
						<div style="display: -webkit-box;">
							<div><span><liferay-ui:message key="label-crawling-only" /></span></div>
							<div class="form-group input-text-wrapper vextooltip" style="width:100%;opacity:unset; margin-top: 0px;display: block;">
								<img src="<%=PortalConstants.FAILED_INFO_ICON_PATH%>"/> 
								<span class="tooltiptext custom-tooltiptext">
									<liferay-ui:message key="label-crawling-only-info" />
								</span>
							</div>
						</div>
					</td>
					<td style="vertical-align: top;">
						<div class="form-group input-text-wrapper">
							<div style="display:inline-block;margin-right: 10px;">
								<label style="vertical-align: bottom;font-weight:normal">ON : &nbsp;</label>
								<aui:input type="radio" name="setCrawlingOnly" value="1" style="vertical-align: top;" checked="<%= (null != scanSetting && scanSetting.getSetcrawlingonly()== 1) %>" label="" />
							</div>
							<div style="display:inline-block">
								<label style="vertical-align: bottom;font-weight:normal">OFF : &nbsp;</label>
								<aui:input type="radio" name="setCrawlingOnly" value="0" style="vertical-align: top;" checked="<%= (null == scanSetting || scanSetting.getSetcrawlingonly() == 0) %>" label="" />
							</div>
						</div>
					</td> --%>
				</tr>

				<tr class="input-row">
					<td colspan="2">
						<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
						<input type="hidden" id="projectId" name="projectId" value="<%= lProjectId %>" />
						<input type="hidden" id="loggedInUser" name="loggedInUser" value="<%= lUserId %>" />
						
						<% if (!CommonUtil.isListNullOrEmpty(targetInformationList) && targetInformationList.size() > 0) { %>
							<input type="hidden" id="<%= portletNamespace %>currentListTableSize" name="<%= portletNamespace %>currentListTableSize" value="<%= targetInformationList.size()%>" />
						<%} else { %>
							<input type="hidden" id="<%= portletNamespace %>currentListTableSize" name="<%= portletNamespace %>currentListTableSize" value="1" />
						<% } %>
						
						<input type="hidden" id="<%= portletNamespace %>listTableSize" name="<%= portletNamespace %>listTableSize" value="<%= ((null != targetInformationList && targetInformationList.size() > 0) ? targetInformationList.size() : 1) %>" />
						
						<table class="table table-bordered table-hover table-striped" id="targetInfoTable" width="350px" border="1">
							<thead class="table-columns">
								<tr>
									<th><liferay-ui:message key="label-protocol" /><span style="color: red;">*</span></th>
									<th><liferay-ui:message key="label-host" /><span style="color: red;">*</span></th>
									<th><liferay-ui:message key="label-port" /><span style="color: red;">*</span></th>
									<th></th>
								</tr>
							</thead>
							<tbody class="table-data">
								<% 
									if (!CommonUtil.isListNullOrEmpty(targetInformationList) && targetInformationList.size() > 0) {
										for (int i = 0; i < targetInformationList.size(); i++) {
											VexTargetInformationItem item = targetInformationList.get(i);
								%>
										<tr>
											<td>
												<select class="field form-control" style="width: auto !important" label="" id="<%= portletNamespace %>protocolGroup<%= i %>" name="<%= portletNamespace %>protocolGroup<%= i %>"  onchange="changePortNumbers(this, <%= i %>)">
												<% if (item.getProtocol().equals("1")) { %>
													<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">http://</option>
													<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">https://</option>
												<% } else if (item.getProtocol().equals("2")) { %>
													<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">http://</option>
													<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true">https://</option>
												<% } else { %>
													<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">http://</option>
													<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">https://</option>
												<% } %>
												</select>
											</td>
											<td><input type="text" id="<%= portletNamespace %>host<%= i %>" name="<%= portletNamespace %>host<%= i %>" class="field form-control" style="width:300px;" placeholder="example.com" value="<%= HtmlUtil.escape(item.getHost()) %>" /></td>
											<td><input type="text" id="<%= portletNamespace %>port<%= i %>" name="<%= portletNamespace %>port<%= i %>" class="field form-control" style="width:80px;" placeholder="80" value="<%= HtmlUtil.escape(item.getPort()) %>" /></td>
											<td><a name="<%= portletNamespace %>btnDelete<%= i %>" id="<%= portletNamespace %>btnDelete<%= i %>" onclick="deleteRow(this)" class="btn btn-default" style="width:100%;margin-top:0px;"><span class='lfr-btn-label'><liferay-ui:message key="button-delete" /></span></a></td>
										</tr>									
								<% 		}
									} else { %>
										<tr>
											<td>
												<select class="field form-control" style="width: auto !important" label="" id="<%= portletNamespace %>protocolGroup0" name="<%= portletNamespace %>protocolGroup0" onchange="changePortNumbers(this, 0)" >	
													<option value="1" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">http://</option>
													<option value="2" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;">https://</option>
												</select>
											</td>
											<td><input type="text" id="<%= portletNamespace %>host0"  name="<%= portletNamespace %>host0" class="field form-control" style="width:300px;" placeholder="example.com" /></td>
											<td><input type="text" id="<%= portletNamespace %>port0" name="<%= portletNamespace %>port0" class="field form-control" style="width:80px;" placeholder="80" value="80"/></td>
											<td><a name="<%= portletNamespace %>btnDelete0" id="<%= portletNamespace %>btnDelete0" onclick="deleteRow(this)" class="btn btn-default" style="width:100%;margin-top:0px;"><span class='lfr-btn-label'><liferay-ui:message key="button-delete" /></span></a></td>
										</tr>
								<% } %>
								<tr>
									<td colspan="4">
										<a onclick="addRow('targetInfoTable')" class="btn btn-default" style="width:100%;margin-top: 0px;" id="button-add"><span class='lfr-btn-label'><liferay-ui:message key="button-add" /></span></a>
									</td>
								</tr>
							<tbody>
						</table>
					</td>
				</tr>

				<tr class="input-row">
					<td><span style="text-decoration: underline;"><liferay-ui:message key="label-cycling-range" /></span></td>
					<td></td>
					
				</tr>

				<tr class="input-row">
					<td style="vertical-align:top"><liferay-ui:message key="label-start-url" /><span style="color: red;">*</span></td>
					<td>
						<div class="form-group input-text-wrapper vextooltip" style="width:100%;opacity:unset;">
							<% if (null != scanSetting) {  %>
								<textarea class="field form-control"  id="startURLtextArea" name="startURL" style="resize:none;height: 100px;"><%= HtmlUtil.escape(scanSetting.getStarturl()) %></textarea>
							<% } else { %>
								<textarea class="field form-control" id="startURLtextArea" name="startURL" style="resize:none;height: 100px;"><%= HtmlUtil.escape(paramStartURL) %></textarea>
							<% } %>
							<span class="tooltiptext"><liferay-ui:message key="message-reminder-start-url-1" /><br>
								<liferay-ui:message key="message-reminder-start-url-2" /><br><br>
								<liferay-ui:message key="message-reminder-start-url-3" /><br>
								<liferay-ui:message key="message-reminder-start-url-4" />
							</span>
							<!-- <span class="tooltiptext">Assign crawling Start URL.<br><br>
								Start URL has to be included to the authorized URL for crawling.<br><br>
								You need to use line break to separate multiple value.<br>
								This is case sensitive.
							</span> -->
							<a onclick="updateStartUrlTextArea('targetInfoTable', 'startURLtextArea')" class="btn btn-default" style="width:30%;margin-top: 2%; margin-left:70%" id="btn-startUrl"><span class='lfr-btn-label'><liferay-ui:message key="button-vex-target-iinformation" /></span></a>
						</div>
					</td>
				</tr>

				<tr class="input-row">
					<td style="vertical-align:top"><liferay-ui:message key="label-authorized-patrol-url" /><span style="color: red;">*</span></td>
					<td>
						<div class="form-group input-text-wrapper vextooltip" style="width:100%;opacity:unset;">
							<% if (null != scanSetting) {  %>
								<textarea class="field form-control" id="authURLtextArea" name="authorizedPatrolURL" style="resize:none;height: 100px;"><%= HtmlUtil.escape(scanSetting.getAuthorizedpatrolurl()) %></textarea>
							<% } else { %>
								<textarea class="field form-control" id="authURLtextArea" name="authorizedPatrolURL" style="resize:none;height: 100px;"><%= HtmlUtil.escape(paramAuthorizedPatrolURL) %></textarea>
							<% } %>
							<span class="tooltiptext"><liferay-ui:message key="message-reminder-authorized-url-1" /><br><br>
								<liferay-ui:message key="message-reminder-authorized-url-2" /><br>
								<liferay-ui:message key="message-reminder-authorized-url-3" /><br>
								<liferay-ui:message key="message-reminder-authorized-url-4" /><br><br>
								<liferay-ui:message key="message-reminder-authorized-url-5" /><br>
								<liferay-ui:message key="message-reminder-authorized-url-6" />
							</span> 
							<!-- <span class="tooltiptext">Assign possible crawling range.<br><br>
									You can use [*] as a wild card.<br>
									You need to use line break to separate multiple value.<br>
									This is not case sensitive.<br><br>
									Ex) If you authorize all URL starting with http://www.ubsecure.jp/ , <br>
									you can write http://www.ubsecure.jp/*
							</span> -->
							<a onclick="updateStartUrlTextArea('targetInfoTable', 'authURLtextArea')" class="btn btn-default" style="width:30%; margin-top: 2%; margin-left: 70%; " id="btn-startUrl"><span class='lfr-btn-label'><liferay-ui:message key="button-vex-target-iinformation" /></span></a>
						</div>
					</td>
					
				</tr>

				<tr class="input-row">
					<td><span style="text-decoration: underline;"><liferay-ui:message key="label-login-information" /></span></td>
				</tr>
					
				<tr class="input-row">
				<tr class="input-row">
					<input type="hidden" id="acquireLoginInformationURL" name="acquireLoginInformationURL" value="<%= acquireLoginInformationURL %>" />
					<input type="hidden" id="userAction" name="userAction" value="<%= PortalConstants.USER_EVENT_GET_LOGIN_INFORMATION %>" /> 

					<td><span><liferay-ui:message key="label-login-url" />:</span></td>
					<td>
						<% if (!CommonUtil.isListNullOrEmpty(loginSettingList) && loginSettingList.size() > 0) { %>
							<aui:input name="loginURL" type="text" value="<%= loginSettingList.get(0).getLoginurl() %>" style="width: 100%;" label="" />
						<% } else { %>
							<aui:input name="loginURL" type="text" value="" style="width: 100%;" label="" />
						<% } %>
					</td>
				</tr>
				<tr class="input-row">
					<td colspan="2">
						<button type="button" class="btn" id="btnAcquireInformation" onclick="submitFormGetLoginInfo()" style="width: 130px;float:right"><liferay-ui:message key="button-acquire-information" /></button>
					</td>
				</tr>

			<div id="loginSettingContainer">
				<!-- Visible when Acquire Information is executed -->
				  		<% 
					   		boolean isFormParamsVisible = (Boolean) renderRequest.getAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS); 
					   		List<VexLoginSettingItem> loginParams = (List<VexLoginSettingItem>) renderRequest.getAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS);

					   		int i = 0;

					   		if(isFormParamsVisible && !CommonUtil.isListNullOrEmpty(loginParams)){
					   	%>
					<table class="table table-bordered table-hover table-striped" border="1" style="width: 583px; margin-top: 10px;">
						<thead>
							<th style="padding-left:20px;"><liferay-ui:message key="label-parameter-name" /></th>
							<th style="padding-left:15px;"><liferay-ui:message key="label-this-is-an-account-information" /></th>
							<th style="padding-left:15px;"><liferay-ui:message key="label-parameter-value" /></th>
						</thead>
					<tbody>
						<% 
							if(!CommonUtil.isListNullOrEmpty(loginParams) && loginParams.size() > 0) {
								for(VexLoginSettingItem item : loginParams){
									String parameterName = item.getParamname();
									String parameterValue = item.getParamvalue();
								%>
						<tr>
							<td style="padding-left: 20px;width: 115px;"><%=parameterName%>
								<input type="hidden" id="<%= portletNamespace+labelParameterName+i %>" name="<%= portletNamespace+labelParameterName+i %>" value="<%=parameterName%>" />
							</td>
							<td><center>
									<% if(item.getChecked() != PortalConstants.INT_ZERO){ %>
										<input type="checkbox" id="<%=checkItem+i%>" name="<%=portletNamespace+checkItem+i%>" onclick="disableField(this)" checked value="1"/></center></td>
									<%} else{ %>
										<input type="checkbox" id="<%=checkItem+i%>" name="<%=portletNamespace+checkItem+i%>" onclick="disableField(this)" value="0"/></center></td>
									<% } %>
							<td style="padding:5px;">
								<% if(item.getChecked() != PortalConstants.INT_ZERO) { %>
									<input type="text" name="<%= portletNamespace+labelParameterValue+i %>" id="<%= portletNamespace+labelParameterValue+i %>" value="<%= parameterValue %>" class="field form-control" style="width: 100%;"/>
								<%} else {%>	
									<input type="text" name="<%= portletNamespace+labelParameterValue+i %>" id="<%= portletNamespace+labelParameterValue+i %>" value="" class="field form-control" readonly style="width: 100%;"/>
								<% } %>
							</td>
						</tr>
						<% i++; }} %>
					</tbody>
					</table>
					<input type="hidden" id="totalParams" name="<%= portletNamespace %>totalNumOfParams" value="<%=i%>" />
				<% }else { %>
					<table class="table table-bordered table-hover table-striped" border="1" style="width: 583px; margin-top: 10px;">
						<thead>
							<th style="padding-left:20px;"><liferay-ui:message key="label-parameter-name" /></th>
							<th style="padding-left:15px;"><liferay-ui:message key="label-this-is-an-account-information" /></th>
							<th style="padding-left:15px;"><liferay-ui:message key="label-parameter-value" /></th>
						</thead>
					<tbody>
						<tr><td colspan=3></td></tr>
					</tbody>
					</table>
					<input type="hidden" id="totalParams" name="<%= portletNamespace %>totalNumOfParams" value="0" />
				<% } %>
			</div>
			</tbody>
		</table>
		<br>
		<div>
			<div class="vextooltip">
				<a id="savePatrolOnlyBtn" onclick="submitPatrolOnlyConfirmForm()" style="margin-right: 10px;"><span class="btn" style="width:auto;"><liferay-ui:message key="button-automatic-patrol" /></span></a>
				<span class="tooltiptext custom-tooltiptext-1"><liferay-ui:message key="message-mouseover-automatic-patrol" /></span>
			</div>
			<div class="vextooltip">
				<a id="saveBtn" onclick="submitConfirmForm()" style="margin-right: 10px;"><span class="btn" style="width:auto;"><liferay-ui:message key="button-scan" /></span></a>
				<span class="tooltiptext custom-tooltiptext-2"><liferay-ui:message key="message-button-mouseover-scan" /></span>
			</div>
			<a id="cancelBtn" href="<%= cancelURL %>"><span class="btn cancel-btn" style="width:auto;"><liferay-ui:message key="button-cancel" /></span></a>
		</div>
		 <div id="confirm">
	         <table id ="confirmBoxTable">
	         	<tr>
	         		 <div class="hostMessage">Host name.</div>
	         	</tr>
	         	<tr>
	         		<td>
	         		   <img id="confirmBoxImage" src = "/o/ubsecure-theme/images/common/warning.png" width="35" height="35"/>
	         		</td>
	         		<td>
	         		  <div class="message">This is a warning message.</div>
	         		</td>
	         	</tr>
	         	<tr>
	         		<td></td>
	         		<td>
	         			<div class="confirmButtons">
	         				<button type="submit">OK</button>
        	 				<button type='button' class="no" onclick="hideConfirmBox();"><liferay-ui:message key="button-cancel" /></button>
        	 				<%-- <a onclick="hideConfirmBox();"><span class="btn cancel-btn" ><liferay-ui:message key="button-cancel" /></span></a> --%>
        	 			</div>
	         		</td>
	         	</tr>
	         </table>
     	 </div>
	</form>
</div>
<%
	} else {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="message-no-access-rights" />
		</div>
		<%
	}
%>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/vex/register_scan_simple_setting.js"></script>
<script>
	define.amd = define._amd;
</script>
