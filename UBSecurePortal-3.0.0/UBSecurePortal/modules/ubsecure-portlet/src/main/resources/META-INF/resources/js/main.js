$('.dropdown-toggle').click(function () {
	var screenName = $("#screenName").val();
	
	if (screenName == "project_list") {
		$("tr td:nth-child(5)").each(function() {
			var endDate = this.innerHTML;
			
			if (isProjectEndDateExceeded(endDate) == true && isProjectCompleted(this.nextElementSibling.innerText) == false) {
				$(this).closest('tr').children('td').removeClass('end-project-background-hover');
				$(this).closest('tr').children('td').addClass('end-project-background');
			} else {
				$(this).closest('tr').children('td').removeClass('default-background-hover');
				$(this).closest('tr').children('td').addClass('default-background');
			}
		});
		
		var endDate = $(this).closest('tr').find('td:nth-child(5)').text();
		
		var prevBackgroundClass = 'default-background';
		var backgroundClass = 'default-background-hover';
		
		if (isProjectEndDateExceeded(endDate) == true && isProjectCompleted($(this).closest('tr').find('td:nth-child(6)').text()) == false) {
			prevBackgroundClass = 'end-project-background';
			backgroundClass = 'end-project-background-hover';
		}
		
		$(this).closest('tr').children('td').removeClass(prevBackgroundClass);
		$(this).closest('tr').children('td').addClass(backgroundClass);
	} else {
		$('.table').removeClass('table-hover');
		$('.table').find('td').css('background-color', '#FFF');
		$(this).closest('tr').children('td').css('background-color', '#EDF8FD');
	}
});

$(document).click(function(e) {
	var screenName = $("#screenName").val();
	
	if (screenName == "project_list") {
		var clickedElement = event.target.nodeName;
		var clickedElementProjectId = '';
		
		if (clickedElement == 'TD') {
			clickedElementProjectId = $(event.target).parent().find('td:nth-child(1)').text();
		} else if (clickedElement == 'SPAN') {
			if ($(event.target).text() == '�X�L�����ꗗ') {
				clickedElementProjectId = $(event.target).closest('tr').find('td:nth-child(1)').text();
			}
		} else {
			var classNames = $(event.target).closest('a').attr('class');
			
			if (classNames != null && classNames != '' && classNames != 'undefined') {
				classNames = classNames.split(' ');
				
				for (var index = 0; index < classNames.length; index++) {
					if (classNames[index] == 'dropdown-toggle') {
						clickedElementProjectId = $(event.target).closest('tr').find('td:nth-child(1)').text();
						break;
					}
				}
			}
		}
		
		$("tr td:nth-child(5)").each(function() {
			if ($(this).parent().find('td:nth-child(1)').text() != clickedElementProjectId) {
				var endDate = this.innerHTML;
				
				if (isProjectEndDateExceeded(endDate) == true && isProjectCompleted(this.nextElementSibling.innerText) == false) {
					$(this).parent().find('td').removeClass('end-project-background-hover');
					$(this).parent().find('td').addClass('end-project-background');
				} else {
					$(this).parent().find('td').removeClass('default-background-hover');
					$(this).parent().find('td').addClass('default-background');
				}
			}
		});

		var closestAnchor = $(event.target).closest('a');

		if (closestAnchor != null && closestAnchor != '' && closestAnchor != 'undefined') {
			var id = closestAnchor.attr('id');
			
			if (id != null && id != '' && id != 'undefined') {
				id = id.split('-');
				
				for (var index = 0; index < id.length; index++) {
					if (id[index] == 'delete') {
						$('.table td').unbind('mouseover');
						break;
					}
				}
			}
		}
	} else {
		$('.table').find('td').removeAttr('style');
		$('.table').addClass('table-hover');
	}
});

function isProjectEndDateExceeded (endDate) {
	var exceeded = false;
	
	if (endDate != null && endDate != '' && endDate != 'undefined') {
		var today = new Date();
		today = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
		
		var endYear = endDate.split("/")[0];
		var endMonth = endDate.split("/")[1] - 1;
		var endDay = endDate.split("/")[2];
		
		endDate = new Date(endYear, endMonth, endDay, 0, 0, 0);
		
		if (endDate < today) {
			exceeded = true;
		}
	}
	
	return exceeded;
}

function isProjectCompleted (status) {
	var complete = false;
	
	if (status != null && status != '' && status != 'undefined') {
		if (status == '����' || status == ' ���� ') {
			complete = true;
		}
	}
	
	return complete;
}