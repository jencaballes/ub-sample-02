<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.liferay.portal.kernel.model.User"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Validator"%>

<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="java.util.Date"%>

<%@ page import="javax.portlet.PortletSession"%>
<%@ page import="javax.portlet.PortletURL"%>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResponseModel"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil"%>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil"%>

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	if (row.getObject() instanceof ScanItem) {
		ScanItem scan = (ScanItem) row.getObject();	
		
		String strScanId 				= PortalConstants.STRING_EMPTY;
		String strModelId 				= PortalConstants.STRING_EMPTY;
		String strDataTarget 			= PortalConstants.STRING_EMPTY;
		String projectName 				= PortalConstants.STRING_EMPTY;
		String scanName					= PortalConstants.STRING_EMPTY;
		String strFile					= PortalConstants.STRING_EMPTY;
		String fileName					= PortalConstants.STRING_EMPTY;
		String strScanImplementation 	= PortalConstants.STRING_EMPTY;
		String strProtocol				= PortalConstants.STRING_EMPTY;
		String strHost					= PortalConstants.STRING_EMPTY;
		String strPort					= PortalConstants.STRING_EMPTY;
		String strLoginURL				= PortalConstants.STRING_EMPTY;
		String strStartTime				= PortalConstants.STRING_EMPTY;
		String strEndTime				= PortalConstants.STRING_EMPTY;
		String strStartURL				= PortalConstants.STRING_EMPTY;
		String strAuthorizedPartrolUrl	= PortalConstants.STRING_EMPTY; 
		String strEmailNotifFlag		= PortalConstants.STRING_EMPTY; 
		/* String strAdvancedModelId		= PortalConstants.STRING_EMPTY; 
		String strAdvancedDataTarget	= PortalConstants.STRING_EMPTY; 
		String strHttpVersion			= PortalConstants.STRING_EMPTY; 
		String strKeepAliveConnection	= PortalConstants.STRING_EMPTY; 
		String strResponseContentLength	= PortalConstants.STRING_EMPTY; 
		String strAcceptEncodingHeader	= PortalConstants.STRING_EMPTY; 
		String strUnzipResponse			= PortalConstants.STRING_EMPTY;
		String strHttpProtocol			= PortalConstants.STRING_EMPTY;
		String strExternalProxyHost		= PortalConstants.STRING_EMPTY;
		String strExternalProxyPort		= PortalConstants.STRING_EMPTY;
		String strExternalProxyAuthId	= PortalConstants.STRING_EMPTY;
		String strExternalProxyAuthPassword = PortalConstants.STRING_EMPTY;
		String strUseClientCertificate	= PortalConstants.STRING_EMPTY;
		String strCertificateFile		= PortalConstants.STRING_EMPTY;
		String strCertificateFilePassword = PortalConstants.STRING_EMPTY;
		String strNtlmAuthId			= PortalConstants.STRING_EMPTY;
		String strNtlmAuthPassword		= PortalConstants.STRING_EMPTY;
		String strNtlmAuthDomain		= PortalConstants.STRING_EMPTY;
		String strNtlmAuthHost			= PortalConstants.STRING_EMPTY;
		String strDigestAuthId			= PortalConstants.STRING_EMPTY;
		String strDigestAuthPassword	= PortalConstants.STRING_EMPTY;
		String strBasicAuthId			= PortalConstants.STRING_EMPTY;
		String strBasicAuthPassword		= PortalConstants.STRING_EMPTY;
		String strAccessExclusionPath	= PortalConstants.STRING_EMPTY; */
		List<VexLoginSettingItem> loginSetting = null;
		
		if( scan != null)
		{
			//ScanID
			strScanId = String.valueOf(scan.getScanId());
			strModelId = "ScanDetailsModal" + strScanId;
			strDataTarget = "#" + strModelId; 
			/* strAdvancedModelId = "AdvancedDetailsModal" + strScanId;
			strAdvancedDataTarget = "#" + strAdvancedModelId; */
			
			// Project Name
			projectName = String.valueOf(scan.getProjectName()); 
		
			// Scan Name
			scanName = String.valueOf(scan.getFileName()); 
			
			// File Name
			strFile = scan.getFilePath();
		
			strFile = strFile.replace("\\", "/");
			int index = strFile.lastIndexOf("/");
			fileName = strFile.substring(index + 1);
					
			// Scan Implementation 
			int iImplementationInfo = scan.getImplementationEnvironment();
			strScanImplementation = LanguageUtil.get(request, PortalConstants.KEY_SCAN_PRODUCTION_ENVIRONMENT);
			if (iImplementationInfo == 1) {
				strScanImplementation = LanguageUtil.get(request, PortalConstants.KEY_SCAN_VERIFICATION_ENVIRONMENT);
			}
			
			// Target Information
			strProtocol = String.valueOf(scan.getProtocol());
			switch(strProtocol){
				case "1" : strProtocol = "http://"; break;
				case "2" : strProtocol = "https://"; break;
			}
			strHost = String.valueOf(scan.getHost());
			strPort = String.valueOf(scan.getPort());
			
			// Login Infomormation
			strLoginURL = String.valueOf(scan.getLoginUrl());
			strLoginURL = HtmlUtil.escapeAttribute(strLoginURL.replaceAll(";"," ; "));
			strLoginURL = strLoginURL.replaceAll("&#x20;&#x3b;&#x20;","<br/>");
			
			//Login Parameters
			loginSetting = scan.getLoginSetting();
			
			// Time Register
			DateFormat formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
			formatter.setTimeZone(TimeZone.getTimeZone(PortalConstants.TIMEZONE_TOKYO));
			
			if(scan.getScanStartTime() != null ){
				strStartTime = formatter.format(scan.getScanStartTime());
			}
					
			if(scan.getScanEndTime() != null ){
				strEndTime = formatter.format(scan.getScanEndTime());
			}
			
			// Start Url
			strStartURL = String.valueOf(scan.getScanStartURL());
			strStartURL = HtmlUtil.escapeAttribute(strStartURL.replaceAll(";"," ; "));
			strStartURL = strStartURL.replaceAll("&#x20;&#x3b;&#x20;","<br/>");
			
			// Authorized Patrol Url
			strAuthorizedPartrolUrl = String.valueOf(scan.getScanAuthorizedPatrolURL());
			strAuthorizedPartrolUrl = HtmlUtil.escapeAttribute(strAuthorizedPartrolUrl.replaceAll(";"," ; "));
			strAuthorizedPartrolUrl = strAuthorizedPartrolUrl.replaceAll("&#x20;&#x3b;&#x20;","<br/>");
			
			// Email Notification
			int iEmailNotifFlag = scan.getSetEmailNotification();
			strEmailNotifFlag = (iEmailNotifFlag == 0)? "OFF" : "ON";
			
			// Scan Advanced Details
			
			/* if(scan.getHttpVersion() == 0) {
				strHttpVersion = "none";
			} else if(scan.getHttpVersion() == 1) {
				strHttpVersion = "1.0";
			} else if(scan.getHttpVersion() == 2) {
				strHttpVersion = "1.1";
			}
			
			if(scan.getSetKeepAliveConnection() == 0) {
				strKeepAliveConnection = "label-do-not-use";
			} else if(scan.getSetKeepAliveConnection() == 1) {
				strKeepAliveConnection = "label-use";
			}
			
			if(scan.getSetResponseContentLength() == 0) {
				strResponseContentLength = "label-do-not-ignore";
			} else if(scan.getSetResponseContentLength() == 1) {
				strResponseContentLength = "label-ignore";
			}
			
			if(scan.getUseAcceptEncodingHeader() == 0) {
				strAcceptEncodingHeader = "label-do-not-delete-encoding";
			} else if(scan.getUseAcceptEncodingHeader() == 1) {
				strAcceptEncodingHeader = "label-delete-encoding";
			}
			
			if(scan.getUnzipResponse() == 0) {
				strUnzipResponse = "label-do-not-do-response";
			} else if(scan.getUnzipResponse() == 1) {
				strUnzipResponse = "label-do-response";
			}
			
			strHttpProtocol = scan.getHttpProtocol();
			strExternalProxyHost = scan.getExternalProxyHost();
			strExternalProxyPort = scan.getExternalProxyPort();
			strExternalProxyAuthId = scan.getExternalProxyAuthId();
			strExternalProxyAuthPassword = scan.getExternalProxyAuthPassword();
			strUseClientCertificate	= scan.getUseClientCertificate();
			strCertificateFile = scan.getCertificateFile();
			strCertificateFilePassword = scan.getCertificateFilePassword();
			strNtlmAuthId = scan.getNtlmAuthId();
			strNtlmAuthPassword = scan.getNtlmAuthPassword();
			strNtlmAuthDomain = scan.getNtlmAuthDomain();
			strNtlmAuthHost = scan.getNtlmAuthHost();
			strDigestAuthId = scan.getDigestAuthId();
			strDigestAuthPassword = scan.getDigestAuthPassword();
			strBasicAuthId = scan.getBasicAuthId();
			strBasicAuthPassword = scan.getBasicAuthPassword();
			strAccessExclusionPath = scan.getAccessExclusionPath(); */
		}
		
		// Scan Information
		int ScanMaxNumDetectionLinks = scan.getMaximumDetectionLinks();
		int ScanInfoType = 0; 
		if(strProtocol.equals("null") && strHost.equals("null") && strPort.equals("0")){
			ScanInfoType = 2; //Scan Import
		}
		
		if(ScanMaxNumDetectionLinks > 0)
		{
			ScanInfoType = 1; //Scan Advance
		}
		
		session.setAttribute("ScanId", strDataTarget);
%>

<portlet:resourceURL var="scanDownloadReferenceURL">
	<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_DOWNLOAD_REFERENCE) %>" />
	<portlet:param name="projectId" value="<%= String.valueOf(scan.getProjectId()) %>" />
	<portlet:param name="scanId" value="<%= String.valueOf(scan.getScanId()) %>" />
	<portlet:param name="status" value="<%= String.valueOf(scan.getScanStatus()) %>" /> 
</portlet:resourceURL>

<liferay-ui:icon-menu message="button-action">

	<%if(ScanInfoType == 0) { %>
		<button type="button" data-toggle="modal" data-target="<%=strDataTarget%>"
			style="color: #404040; margin-right: 5px; width:100px;"
			class="btn">
			<liferay-ui:message key="label-details" />
		</button>
		
		<!-- Details Modal -->
		<div class="modal fade ScanDetailsModal" id="<%=strModelId%>" role="dialog"
			aria-hidden="true" data-backdrop="static" data-keyboard="false"
			style="width: auto; display: none; overflow: auto;">
			<div class="modal-dialog" style="width: 80%;height: auto; overflow: hidden;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<liferay-ui:message key="label-details" />
					</div>
					<div class="modal-body" style="overflow: auto;">
							<table id="mainTable" width="100%" style="position: relative; display: table;">
								<!-- Meta Information Section -->
								<tr height="30px">
									<td width="20%" height="30px"><liferay-ui:message key="scan-details-meta-info" /></td>
									<td width="30%" height="30px"><liferay-ui:message key="label-project-name" /></td>
									<td height="30px"><liferay-ui:message key="<%=projectName%>" /></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-scan-name" /></td>
									<td><liferay-ui:message key="<%=scanName%>" /></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-scan-information" /></td>
									
									<%if(ScanInfoType == 0 ) { %>
										<td><liferay-ui:message key="scan-information-simple" /></td>
									<% } else if (ScanInfoType ==1) { %>
										<td>
											<liferay-ui:message key="scan-information-advance" />
											<liferay-ui:icon message="scan-details-advance-link" url="#" onClick="javascript: hidecurrentModal()" />
											<%-- <a data-toggle="modal" href="<%=strAdvancedDataTarget%>">
												<liferay-ui:icon message="scan-details-advance-link" />
											</a> --%>
										</td>
									<%} %>
								</tr>
								
								<!-- Target Information Section -->
								<tr>
									<td><liferay-ui:message key="label-target-information" /></td>
									<td><liferay-ui:message key="label-implementation-environment" /></td>
									<td><liferay-ui:message key="<%=strScanImplementation%>" /></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="scan-details-implementation-url"  /></td>
									<%
									String strImplURL = strProtocol + strHost + ":" + strPort;
									%>
									<td><liferay-ui:message key="<%=strImplURL%>" /></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-start-url" /></td>
									<td><liferay-ui:message key="<%=strStartURL%>" /></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-authorized-patrol-url" /></td>
									<td><liferay-ui:message key="<%=strAuthorizedPartrolUrl%>" /></td>
								</tr>
								
								<!-- Login Information -->
								<tr>
									<td><liferay-ui:message key="label-login-information" /></td>
									<td><liferay-ui:message key="label-login-url" /></td>
									<td><liferay-ui:message key="<%=strLoginURL%>" /></td>
								</tr>
							
								<%
									if(loginSetting != null){
										int counter = 0;
										for (VexLoginSettingItem item : loginSetting){
											counter++;
											String strParamName = PortalConstants.STRING_EMPTY; 
											String strParamValue = PortalConstants.STRING_EMPTY;
											String StrInputVal = "入力値"+counter;
													
											if(item != null){
												strParamName = String.valueOf(item.getParamname());
												strParamValue = String.valueOf(item.getParamvalue());
											%>
												<tr class="notranslate">
													<td></td>
													<td><span><%=StrInputVal%></span></td>
													<td>
														<span class="notranslate"><%=HtmlUtil.render(strParamName)%> : <%=HtmlUtil.render(strParamValue)%></span>
													</td>
												</tr>
											<%}
										}
									}%>
									
									<!-- Timer Setting -->									
									<tr>
										<td><liferay-ui:message key="label-timer-setting" /></td>
										<td><liferay-ui:message key="label-start-time" /></td>
										<td><liferay-ui:message key="<%=strStartTime%>" /></td>
									</tr>
									<tr>
										<td></td>
										<td><liferay-ui:message key="label-end-time" /></td>
										<td><liferay-ui:message key="<%=strEndTime%>" /></td>
									</tr>
									
									<!-- Email Notification -->
									<tr>
										<td><liferay-ui:message key="label-email-notification" /></td>
										<td></td>
										<td><liferay-ui:message key="<%=strEmailNotifFlag%>" /></td>
									</tr>
							</table>
							
							<!-- Advance Table -->
							<table id="advanceInfoTable" width="100%" style="position: relative; display: none;">
								<!--Advance Target Information -->
								<tr height="30px">
									<td width="20%" height="30px"><liferay-ui:message key="scan-details-target-information" /></td>
									<td width="30%" height="30px"><liferay-ui:message key="label-http-version-colon" /></td>
									<td height="30px"></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-keep-alive-connection-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-response-content-length-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-accept-encoding-header-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-response-unzip-colon"  /></td>
									<td></td>
								</tr>
								<tr>
									<td><liferay-ui:message key="label-external-proxy-colon" /></td>
									<td><liferay-ui:message key="label-host-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-port-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-authentication-ID" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-authentication-password" /></td>
									<td></td>
								</tr>
								
								<!-- Client Certification -->
								<tr>
									<td><liferay-ui:message key="label-client-certificate-colon" /></td>
									<td><liferay-ui:message key="label-certificate-colon" /></td>
									<td></td>
								</tr>
								
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-passphrase-colon" /></td>
									<td></td>
								</tr>
								
								<!-- NTML Certification -->
								<tr>
									<td><liferay-ui:message key="label-ntlm-authentication-colon" /></td>
									<td><liferay-ui:message key="label-username-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-password-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-domain-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-hostname-colon" /></td>
									<td></td>
								</tr>
								
								<!-- Digest Authentication -->
								<tr>
									<td><liferay-ui:message key="label-digest-authentication-colon" /></td>
									<td><liferay-ui:message key="label-username-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-password-colon" /></td>
									<td></td>
								</tr>
								
								<!-- Basic Authentication -->
								<tr>
									<td><liferay-ui:message key="label-basic-authentication-colon" /></td>
									<td><liferay-ui:message key="label-username-colon" /></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-password-colon" /></td>
									<td></td>
								</tr>
								
								<!-- Access Execution Path -->
								<tr>
									<td><liferay-ui:message key="label-access-execution-path-colon" /></td>
									<td colspan="2"></td>
								</tr>
							</table>
						</div>
				</div>
			</div>
		</div>
		
		<!-- Advanced Details Modal -->
		<%-- <div class="modal fade ScanDetailsModal" id="<%=strAdvancedModelId%>" role="dialog"
			aria-hidden="true" data-backdrop="static" data-keyboard="false"
			style="width: auto; display: none; overflow: auto;">
			<div class="modal-dialog" style="width: 80%;height: auto; overflow: hidden;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
							<liferay-ui:message key="label-details" />
					</div>
					<div class="modal-body" style="overflow: auto;">
						<!-- Advance Table -->
						<table id="advanceInfoTable" width="100%" style="position: relative;">
							<!--Advance Target Information -->
							<tr height="30px">
								<td width="20%" height="30px"><liferay-ui:message key="scan-details-target-information" /></td>
								<td width="30%" height="30px"><liferay-ui:message key="label-http-version-colon" /></td>
								<td height="30px"><liferay-ui:message key="<%=strHttpVersion%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-keep-alive-connection-colon" /></td>
								<td><liferay-ui:message key="<%=strKeepAliveConnection%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-response-content-length-colon" /></td>
								<td><liferay-ui:message key="<%=strResponseContentLength%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-accept-encoding-header-colon" /></td>
								<td><liferay-ui:message key="<%=strAcceptEncodingHeader%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-response-unzip-colon"  /></td>
								<td><liferay-ui:message key="<%=strUnzipResponse%>" /></td>
							</tr>
							<tr>
								<td><liferay-ui:message key="label-external-proxy-colon" /></td>
								<td><liferay-ui:message key="label-host-colon" /></td>
								<td><liferay-ui:message key="<%=strExternalProxyHost%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-port-colon" /></td>
								<td><liferay-ui:message key="<%=strExternalProxyPort%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-authentication-ID" /></td>
								<td><liferay-ui:message key="<%=strExternalProxyAuthId%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-authentication-password" /></td>
								<td><liferay-ui:message key="<%=strExternalProxyAuthPassword%>" /></td>
							</tr>
							
							<!-- Client Certification -->
							<tr>
								<td><liferay-ui:message key="label-client-certificate-colon" /></td>
								<td><liferay-ui:message key="label-certificate-colon" /></td>
								<td><liferay-ui:message key="<%=strCertificateFile%>" /></td>
							</tr>
							
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-passphrase-colon" /></td>
								<td><liferay-ui:message key="<%=strCertificateFilePassword%>" /></td>
							</tr>
							
							<!-- NTML Certification -->
							<tr>
								<td><liferay-ui:message key="label-ntlm-authentication-colon" /></td>
								<td><liferay-ui:message key="label-username-colon" /></td>
								<td><liferay-ui:message key="<%=strNtlmAuthId%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-password-colon" /></td>
								<td><liferay-ui:message key="<%=strNtlmAuthPassword%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-domain-colon" /></td>
								<td><liferay-ui:message key="<%=strNtlmAuthDomain%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-hostname-colon" /></td>
								<td><liferay-ui:message key="<%=strNtlmAuthHost%>" /></td>
							</tr>
							
							<!-- Digest Authentication -->
							<tr>
								<td><liferay-ui:message key="label-digest-authentication-colon" /></td>
								<td><liferay-ui:message key="label-username-colon" /></td>
								<td><liferay-ui:message key="<%=strDigestAuthId%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-password-colon" /></td>
								<td><liferay-ui:message key="<%=strDigestAuthPassword%>" /></td>
							</tr>
								
							<!-- Basic Authentication -->
							<tr>
								<td><liferay-ui:message key="label-basic-authentication-colon" /></td>
								<td><liferay-ui:message key="label-username-colon" /></td>
								<td><liferay-ui:message key="<%=strBasicAuthId%>" /></td>
							</tr>
							<tr>
								<td></td>
								<td><liferay-ui:message key="label-password-colon" /></td>
								<td><liferay-ui:message key="<%=strBasicAuthPassword%>" /></td>
							</tr>
							
							<!-- Access Execution Path -->
							<tr>
								<td><liferay-ui:message key="label-access-execution-path-colon" /></td>
								<td colspan="2"><liferay-ui:message key="<%=strAccessExclusionPath%>" /></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>  --%>
	<% } else if(ScanInfoType == 2) { %>
		<button type="button" data-toggle="modal" data-target="<%=strDataTarget%>"
			style="color: #404040; margin-right: 5px; width:100px;"
			class="btn">
			<liferay-ui:message key="label-details" />
		</button>
		
		<!-- Details Modal -->
		<div class="modal fade ScanDetailsModal" id="<%=strModelId%>" role="dialog"
			aria-hidden="true" data-backdrop="static" data-keyboard="false"
			style="width: auto; display: none; overflow: auto;">
			<div class="modal-dialog" style="width: 80%;height: auto; overflow: hidden;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<liferay-ui:message key="label-details" />
					</div>
					<div class="modal-body" style="overflow: auto;">
					<table width="100%" style="position: relative;">
								<!-- Meta Information Section -->
								<tr height="30px">
									<td width="20%" height="30px"><liferay-ui:message key="scan-details-meta-info" /></td>
									<td width="30%" height="30px"><liferay-ui:message key="label-project-name" /></td>
									<td height="30px"><liferay-ui:message key="<%=projectName%>" /></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-scan-name" /></td>
									<td><liferay-ui:message key="<%=scanName%>" /></td>
								</tr>
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-scan-information" /></td>
									<td><liferay-ui:message key="scan-information-import" /></td>
								</tr>
								
								<tr>
									<td></td>
									<td><liferay-ui:message key="label-file-name" /></td>
									<td><liferay-ui:message key="<%=fileName%>" /></td>
								</tr>
							</table>
					</div>
				</div>
			</div>
		</div> 
	<%} else { %>
		<button data-toggle="modal" data-target="<%=strDataTarget%>"
			style="color: #404040; margin-right: 5px; width:100px;"
			class="btn" disabled="disabled">
			<liferay-ui:message key="label-details" />
		</button>
	<% } %>
</liferay-ui:icon-menu>

<%
	}
%>

<script type="text/javascript">	
	$(document).ready(function () {		
		$(".ScanDetailsModal").on('hide.bs.modal', function (e) {
			$('body').removeClass('modal-open');
			$('body').removeClass('modal-backdrop');
			$('.modal-backdrop').remove();
			
			$("div[class*=modal-backdrop]").removeClass('modal-backdrop');
		});
		
		$(".ScanDetailsModal").on('hidden.bs.modal', function (e) {
			$('body').removeClass('modal-open');
			$('body').removeClass('modal-backdrop');
			$('.modal-backdrop').remove();
			
			$("div[class*=modal-backdrop]").removeClass('modal-backdrop');
		});
		
		$(".ScanDetailsModal").on('hide', function (e) {
			$('body').removeClass('modal-open');
			$('body').removeClass('modal-backdrop');
			$('.modal-backdrop').remove();
			
			$("div[class*=modal-backdrop]").removeClass('modal-backdrop');
		});
		
		$(".ScanDetailsModal").on('hidden', function (e) {
			$('body').removeClass('modal-open');
			$('body').removeClass('modal-backdrop');
			$('.modal-backdrop').remove();
			
			$("div[class*=modal-backdrop]").removeClass('modal-backdrop');
		});
		
		$('body').removeClass('modal-open');
		$('body').removeClass('modal-backdrop');
		$('.modal-backdrop').remove();
	});
	
	function hidecurrentModal() {
		var lTable = document.getElementById("advanceInfoTable");
	    lTable.style.display = (lTable.style.display == "table") ? "none" : "table";
	    
	    var lTable2 = document.getElementById("mainTable");
	    lTable2.style.display = (lTable2.style.display == "table") ? "none" : "table";
	}
</script>