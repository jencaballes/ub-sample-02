<%@page import="java.text.ParseException"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ProjectItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResponseModel" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<portlet:defineObjects />

<!-- CxSuite -->

<%
	// Data from session
	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	httpSession.setAttribute("Current_screen", "CxSuite");
	Object downloadFrom = httpSession.getAttribute("download_from");
	String strDownloadFrom = PortalConstants.STRING_EMPTY;
	Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
	long lUserId = PortalConstants.LONG_ZERO;
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	int iUserRole = PortalConstants.INT_ZERO;
	Object oUseCxSuite = httpSession.getAttribute(PortalConstants.USE_CXSUITE);
	boolean bUseCxSuite = false;
	boolean bHasSearchInput = false;
	
	if (!CommonUtil.isObjectNull(oUserId)) {
		lUserId = Long.parseLong(oUserId.toString());
	}
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
		
		if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
			bUseCxSuite = true;
		} else if (!CommonUtil.isObjectNull(oUseCxSuite)) {
			bUseCxSuite = Boolean.parseBoolean(oUseCxSuite.toString());
		}
	}
	// End
	
	String portletNamespace = null;
	portletNamespace = renderResponse.getNamespace();
	
	PortletSession pSession = renderRequest.getPortletSession();
	
	// Data from renderRequest
	Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
	String strCxServerErrorMsg = null;
	List<Object> projectList = (List<Object>) pSession.getAttribute(PortalConstants.PARAM_PROJECT_LIST);
	Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
	boolean bIsFromSearch = false;
	Object oViewProjectList = renderRequest.getAttribute(PortalConstants.PARAM_VIEW_PROJECT_LIST);
	boolean bViewProjectList = false;
	Object oDoViewProjectList = renderRequest.getAttribute(PortalConstants.PARAM_DO_VIEW_PROJECT_LIST);
	boolean bDoViewProjectList = false;
	Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;
	Object oCxCancelScanFailed = pSession.getAttribute(PortalMessages.CX_CANCEL_SCAN_FAILED);
	Object oCxDeleteProjectFailed = pSession.getAttribute(PortalMessages.CX_DELETE_PROJECT_FAILED);
	String strDeleteFailed = PortalConstants.STRING_EMPTY;
	Object oHasError = pSession.getAttribute(PortalConstants.PARAM_HAS_ERROR);
	boolean bHasError = false;
	boolean bErrorFromCxSuite = false;
	
	if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
		strCxServerErrorMsg = oCxServerErrorMsg.toString();
	}
	
	if (!CommonUtil.isObjectNull(oIsFromSearch)) {
		bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
	}
	
	if (!CommonUtil.isObjectNull(oViewProjectList)) {
		bViewProjectList = Boolean.parseBoolean(oViewProjectList.toString());
	}
	
	if (!CommonUtil.isObjectNull(oDoViewProjectList)) {
		bDoViewProjectList = Boolean.parseBoolean(oDoViewProjectList.toString());
	}
	
	if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
		strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
	}
	
	if (!CommonUtil.isObjectNull(oHasError)) {
		bHasError = Boolean.parseBoolean(oHasError.toString());
	}
	
	if (!CommonUtil.isObjectNull(oCxCancelScanFailed)) {
		bErrorFromCxSuite = true;
	}
	
	if (!CommonUtil.isObjectNull(oCxDeleteProjectFailed)) {
		bErrorFromCxSuite = true;
	}
	
	pSession.removeAttribute(PortalConstants.PARAM_HAS_ERROR);
	pSession.removeAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	// End
	
	// Show error indicators
	boolean bShowDBConnError = false;
	boolean bShowCxSessionIdError = false;
	boolean bShowPaginationError = false;
	// End
	
	// Previous and current screen
	Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
	String strCurrScreen = PortalConstants.STRING_EMPTY;
	
	if (!CommonUtil.isObjectNull(oCurrScreen)) {
		strCurrScreen = oCurrScreen.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
			pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
		}
	}
	
	pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_PROJECT_LIST);
	// End
	
	// Search project attributes
	Map<String, Object> searchedProject = (Map<String, Object>) pSession.getAttribute(PortalConstants.PARAM_PROJECT);
	String strSearchedProjectId = PortalConstants.STRING_EMPTY;
	String strSearchedGroupName = PortalConstants.STRING_EMPTY;
	String strSearchedCaseNumber = PortalConstants.STRING_EMPTY;
	String strSearchedProjectName = PortalConstants.STRING_EMPTY;
	String strSearchedProjectEndDateLow = PortalConstants.STRING_EMPTY;
	String strSearchedProjectEndDateHigh = PortalConstants.STRING_EMPTY;
	String strSearchedStatus = PortalConstants.STRING_EMPTY;
	String strSearchedScanCount = PortalConstants.STRING_EMPTY;
	boolean bCompleteSelected = false;
	boolean bNotCompleteSelected = false;
	Calendar dteSearchedProjectEndDateLow = null;
	Calendar dteSearchedProjectEndDateHigh = null;
	int year = PortalConstants.INT_ZERO;
	int month = PortalConstants.INT_ZERO;
	int day = PortalConstants.INT_ZERO;
	String strSearchInfo = PortalConstants.STRING_EMPTY;
	String strSearchInfoConnector = PortalConstants.STRING_EMPTY;
	String strSearchInfoComma = LanguageUtil.get(request, PortalConstants.KEY_FILTER_LIST_COMMA);
	
	if (bIsFromSearch && !CommonUtil.isObjectNull(searchedProject)) {
		Object oSearchedProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
		Object oSearchedGroupName = searchedProject.get(PortalConstants.PARAM_OWNER_GROUP);
		Object oSearchedCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
		Object oSearchedProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
		Object oSearchedProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
		Object oSearchedProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
		Object oSearchedStatus = searchedProject.get(PortalConstants.PARAM_STATUS);
		Object oSearchedScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
		int iSearchedStatus = PortalConstants.INT_ZERO;
		DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
		
		if (!CommonUtil.isObjectNull(oSearchedProjectId)) {
			strSearchedProjectId = oSearchedProjectId.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedProjectId)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_PROJECT_ID, strSearchedProjectId, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedGroupName)) {
			strSearchedGroupName = oSearchedGroupName.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedGroupName)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_GROUP_NAME, strSearchedGroupName, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedCaseNumber)) {
			strSearchedCaseNumber = oSearchedCaseNumber.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedCaseNumber)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_CASE_NUMBER, strSearchedCaseNumber, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedProjectName)) {
			strSearchedProjectName = oSearchedProjectName.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedProjectName)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_PROJECT_NAME, strSearchedProjectName, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedProjectEndDateLow)) {
			strSearchedProjectEndDateLow = oSearchedProjectEndDateLow.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedProjectEndDateLow)
					&& !strSearchedProjectEndDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				dteSearchedProjectEndDateLow = Calendar.getInstance();
				try {
					dteSearchedProjectEndDateLow.setTime(format.parse(strSearchedProjectEndDateLow));
				} catch(ParseException pe) {
					//do nothing;
				}
				
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedProjectEndDateHigh)) {
			strSearchedProjectEndDateHigh = oSearchedProjectEndDateHigh.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedProjectEndDateHigh)
					&& !strSearchedProjectEndDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				dteSearchedProjectEndDateHigh = Calendar.getInstance();
				try {
					dteSearchedProjectEndDateHigh.setTime(format.parse(strSearchedProjectEndDateHigh));
				} catch (ParseException pe) {
					//do nothing
				}
				
			}
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strSearchedProjectEndDateLow)
				|| !CommonUtil.isStringNullOrEmpty(strSearchedProjectEndDateHigh)) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_PROJECT_END_DATE, strSearchedProjectEndDateLow + PortalConstants.STRING_TILDE + strSearchedProjectEndDateHigh, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
		
		if (!CommonUtil.isObjectNull(oSearchedStatus)) {
			strSearchedStatus = oSearchedStatus.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedStatus)) {
				iSearchedStatus = Integer.parseInt(strSearchedStatus);
				bHasSearchInput = true;
				
				if (iSearchedStatus == ProjectStatus.COMPLETE.getInteger()) {
					strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_STATUS, PortalConstants.KEY_PROJECT_STATUS_COMPLETE, true);
					bCompleteSelected = true;
				} else if (iSearchedStatus == ProjectStatus.NOT_YET_COMPLETE.getInteger()) {
					strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_STATUS, PortalConstants.KEY_PROJECT_STATUS_OPEN, true);
					bNotCompleteSelected = true;
				}
				
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
		
		if (!CommonUtil.isObjectNull(oSearchedScanCount)) {
			strSearchedScanCount = oSearchedScanCount.toString();
			
			if (!CommonUtil.isStringNullOrEmpty(strSearchedScanCount)) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_SCAN_COUNT, strSearchedScanCount, false);
				strSearchInfoConnector = strSearchInfoComma;
			}
		}
	}
	// End
	
	// Pagination variables
	Object curPageObj = null;
	int curPage = PortalConstants.INT_ONE;	
	int start = PortalConstants.INT_ZERO;
	int end = PortalConstants.PAGINATION_DELTA;
	// End
	
	// Sorting variables
	String orderByCol = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_COL);
	String orderByType = ParamUtil.getString(request, PortalConstants.PARAM_ORDER_BY_TYPE);
	
	if (orderByType == null || orderByType.isEmpty()) {
		Object oOrderByType = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
		
		if (oOrderByType != null) {
			orderByType = oOrderByType.toString();
			
			if (orderByType.isEmpty()) {
				orderByType = PortalConstants.SORT_ASCENDING;
			}
		} else {
			orderByType = PortalConstants.SORT_ASCENDING;
		}
		
		Object oOrderByCol = pSession.getAttribute(PortalConstants.PARAM_ORDER_BY_COL);
		
		if (oOrderByCol != null) {
			orderByCol = oOrderByCol.toString();
			
			if (!orderByCol.isEmpty()) {
				pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
			}
		}
	} else {
		pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_TYPE, orderByType);
		pSession.setAttribute(PortalConstants.PARAM_ORDER_BY_COL, orderByCol);
	}
	// End
		
	// Show project list
	if (bUseCxSuite) {
		curPageObj = request.getParameter("cur");
		
		if (Validator.isNotNull(curPageObj)) {
			curPage = Integer.valueOf(String.valueOf(curPageObj));
			start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
			end = curPage * PortalConstants.PAGINATION_DELTA;
		}
		
		int size = 0;
		
		if (!CommonUtil.isListNullOrEmpty(projectList)) {
			size = projectList.size();
		}
		
		if (size == 0 && !CommonUtil.isListNullOrEmpty(projectList)) {
			--curPage;
			start = (curPage - 1) * PortalConstants.PAGINATION_DELTA;
			end = curPage * PortalConstants.PAGINATION_DELTA;
			request.setAttribute("index-overlap", "");
			
			if (SessionErrors.isEmpty(renderRequest) && SessionMessages.isEmpty(renderRequest)) {
				bShowPaginationError = true;
			}
		}
		
		ResponseModel rModel = null;
		
		if (bIsFromSearch) {
			rModel = ControllerHelper.getProjects(lUserId, ProjectType.CX_SUITE.getInteger(), searchedProject, start);
		} else {
			rModel = ControllerHelper.getProjects(lUserId, ProjectType.CX_SUITE.getInteger(), start);
			
			Object oLoggedInCx = httpSession.getAttribute(PortalConstants.LOGGED_IN_CX);
			boolean bLoggedInCx = false;
			
			if (bDoViewProjectList) {
				if (oLoggedInCx != null) {
					bLoggedInCx = Boolean.parseBoolean(oLoggedInCx.toString());
					if (!bLoggedInCx) {
						Object oCxSessionId = httpSession.getAttribute(PortalConstants.CXSESSION_ID);
						
						if (oCxSessionId == null) {
							bShowCxSessionIdError = true;
						} else {
							String strCxSessionId = oCxSessionId.toString();
							
							if (CommonUtil.isStringNullOrEmpty(strCxSessionId)) {
								bShowCxSessionIdError = true;
							}
						}
					}
				}
			}
		}
		
		if (rModel != null) {
			if (rModel.getStatus() && rModel.getData() != null) {
				projectList = (List<Object>) rModel.getData();
			} else {
				if (SessionErrors.isEmpty(renderRequest)) {
					if (rModel.getMessage().equals(PortalConstants.ORM_EXCEPTION)) {
						if (bViewProjectList) {
							if (SessionMessages.isEmpty(renderRequest) && SessionErrors.isEmpty(renderRequest)) {
								bShowDBConnError = true;
							}
						}
					} else {
						SessionErrors.add(renderRequest, rModel.getMessage());
					}
				}
			}
		}
		
		List<ProjectItem> projectsList = new ArrayList<ProjectItem>();

		if (!CommonUtil.isStringNullOrEmpty(orderByCol)
				&&  !CommonUtil.isStringNullOrEmpty(orderByType)) {
			if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
				projectList = ControllerHelper.sortProjects(lUserId, iUserRole, searchedProject, orderByCol, orderByType, ProjectType.CX_SUITE.getInteger(), start, PortalConstants.PAGINATION_DELTA);
			} else {
				projectList = ControllerHelper.sortUserProjects(lUserId, searchedProject, orderByCol, orderByType, ProjectType.CX_SUITE.getInteger(), start, PortalConstants.PAGINATION_DELTA);
			}
		}
		
		 if (!CommonUtil.isListNullOrEmpty(projectList)) {
			for (Object item : projectList) {
				projectsList.add((ProjectItem) item);
			}
		}

		/* if (SessionErrors.contains(renderRequest, PortalMessages.CX_DELETE_PROJECT_FAILED)
				|| SessionErrors.contains(renderRequest, PortalMessages.CX_DELETE_SCAN_FAILED)) {
			bErrorFromCxSuite = true;
		}  */

		Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
		String strManualDownloadError = PortalConstants.STRING_EMPTY;
		
		if (!CommonUtil.isObjectNull(oManualDownloadError)) {
			strManualDownloadError = oManualDownloadError.toString();
		}
		
		if (!CommonUtil.isObjectNull(downloadFrom)){
			strDownloadFrom = downloadFrom.toString();
		}
%>

<div style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px;">
	<liferay-ui:message key="header-project-list" />
</div>

<div style="height: 14px;">
	<hr style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
</div>

<div>
<%
	if (bShowCxSessionIdError) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="error-cx-session-id-invalid" />
		</div>
		<%
	} else if (bShowDBConnError) {
		%>
			<div id="dbConnError" class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.ORM_EXCEPTION %>" />
			</div>
		<%
	} else if (bShowPaginationError) {
		%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.NEXT_PAGINATION_ERROR %>" />
			</div>
		<%
	} else if (bErrorFromCxSuite) {
		%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.CX_DELETE_PROJECT_FAILED %>" /><liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
			</div>
		<%
		pSession.removeAttribute(PortalMessages.CX_CANCEL_SCAN_FAILED);
		pSession.removeAttribute(PortalMessages.CX_DELETE_PROJECT_FAILED);
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("CxSuite")) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
		</div>
	<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("CxSuite")) {
		%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
			</div>
		<%
		httpSession.removeAttribute(PortalConstants.ERROR);
	}

	Set<String> keys = SessionMessages.keySet(renderRequest);
	Iterator<String> it = keys.iterator();
	boolean bHasSessionMessage = false;
	
	while (it.hasNext()) {
		String key = it.next();
		if (!key.contains(SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE)
				&& !key.contains(SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE)) {
			bHasSessionMessage = true;
			break;
		}
	}

	if (bHasSessionMessage) {
	%>
		<liferay-ui:success key="<%= PortalConstants.ADD_PROJECT_SUCCESSFUL %>" message="<%= PortalMessages.ADD_PROJECT_SUCCESSFUL %>" />
		<liferay-ui:success key="<%= PortalConstants.DELETE_PROJECT_SUCCESSFUL %>" message="<%= PortalMessages.DELETE_PROJECT_SUCCESSFUL %>" />
		<liferay-ui:success key="<%= PortalConstants.UPDATE_PROJECT_SUCCESSFUL %>" message="<%= PortalMessages.UPDATE_PROJECT_SUCCESSFUL %>" />
	<%
	} else {
	%>
		<liferay-ui:error key="<%= PortalMessages.USER_ID_INVALID %>" message="<%= PortalMessages.USER_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
		<liferay-ui:error key="<%= PortalMessages.USER_INVALID %>" message="<%= PortalMessages.USER_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.DELETE_PROJECT_FAILED %>" message="<%= PortalMessages.DELETE_PROJECT_FAILED %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PROJECT_ID_INVALID %>" message="<%= PortalMessages.PROJECT_ID_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.GROUP_NAME_TOO_LONG %>" message="<%= PortalMessages.GROUP_NAME_TOO_LONG %>" />
		
		<liferay-ui:error key="<%= PortalMessages.CASE_NUMBER_INVALID %>" message="<%= PortalMessages.CASE_NUMBER_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.CASE_NUMBER_TOO_LONG %>" message="<%= PortalMessages.CASE_NUMBER_TOO_LONG %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PROJECT_NAME_TOO_LONG %>" message="<%= PortalMessages.PROJECT_NAME_TOO_LONG %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_NAME_INVALID %>" message="<%= PortalMessages.PROJECT_NAME_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PROJECT_END_DATE_INVALID %>" message="<%= PortalMessages.PROJECT_END_DATE_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.SCAN_COUNT_INVALID %>" message="<%= PortalMessages.SCAN_COUNT_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PROJECT_TYPE_NOT_CX %>" message="<%= PortalMessages.PROJECT_TYPE_NOT_CX %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" message="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_INVALID %>" message="<%= PortalMessages.PROJECT_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
		
		<liferay-ui:error key="<%= PortalErrors.CX_SERVER_ERROR %>" message="<%= strCxServerErrorMsg %>" />
		<%-- <liferay-ui:error key="<%= PortalMessages.CX_DELETE_PROJECT_FAILED %>" message="<%= PortalMessages.CX_DELETE_PROJECT_FAILED %>" /> --%>
		<liferay-ui:error key="<%= PortalMessages.CX_SESSION_ID_INVALID %>" message="<%= PortalMessages.CX_SESSION_ID_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.USER_NO_RIGHTS_ACTION %>" message="<%= PortalMessages.USER_NO_RIGHTS_ACTION %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PERIOD_INVALID %>" message="<%= PortalMessages.PERIOD_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_SUMMARY %>" message="<%= PortalMessages.NO_SUMMARY %>" />
		<liferay-ui:error key="<%= PortalMessages.NO_PROJECT_LIST %>" message="<%= PortalMessages.NO_PROJECT_LIST %>" />
	<%
	}
	%>
</div>

<portlet:actionURL name="viewEditProject" var="viewCreateProjectURL">
	<portlet:param name="projectId" value="<%= String.valueOf(0) %>" />
	<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_VIEW_CREATE_PROJECT) %>" />
</portlet:actionURL>

<portlet:actionURL name="viewEntireScanList" var="viewEntireScanListURL">
	<portlet:param name="type" value="<%= String.valueOf(ProjectType.CX_SUITE.getInteger()) %>" />
	<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_VIEW_ENTIRE_SCAN_LIST) %>" />
</portlet:actionURL>

<portlet:actionURL name="searchProjects" var="getProjectsURL">
	<portlet:param name="start" value="<%= String.valueOf(start) %>" />
</portlet:actionURL>

<portlet:actionURL name="clearFilterProjects" var="clearFilterProjectsURL">
	<portlet:param name="start" value="<%= String.valueOf(start) %>" />
</portlet:actionURL>

<portlet:resourceURL var="downloadSummaryURL">
	<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_DOWNLOAD_SUMMARY) %>" />
</portlet:resourceURL>

<portlet:resourceURL var="downloadProjectListURL">
	<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_DOWNLOAD_PROJECT_LIST) %>" />
</portlet:resourceURL>

<%
	String downloadSummary = "downloadSummary('" + downloadSummaryURL.toString() + "'); return false;";
%>

<aui:button-row>
	<%
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
		%>		
		<aui:button name="projectRegistrationBtn" value="button-new-registration" href="<%= viewCreateProjectURL %>" style="width:150px; color:#404040;" />
		<aui:button name="entireScanListBtn" value="button-entire-scan-list" href="<%= viewEntireScanListURL %>" style="width:150px; color:#404040;" />
		<button data-toggle="modal" data-target="#downloadSummaryModal" style="width:150px; color:#404040; float:right; margin-right: 5px;" class="btn"><liferay-ui:message key="button-download-summary" /></button>
		
		<div style="height: 15px; padding-top: -10px;">
			<hr style="height: 1px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
		</div>
		<%
	} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
		%>		
		<aui:button name="projectRegistrationBtn" value="button-new-registration" href="<%= viewCreateProjectURL %>" style="width:150px; color:#404040;" />
		
		<div style="height: 15px; padding-top: -10px;">
			<hr style="height: 1px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
		</div>
		<%
	}
	%>
</aui:button-row>

<aui:button-row>
	<%
	if (bIsFromSearch && bHasSearchInput) {
		%>
		<aui:button style="width:150px; color: #404040; float:right;" value="button-clear-filter" href="<%= clearFilterProjectsURL %>" />
		<%
	} else {
		%>
		<aui:button style="width:150px; color: #404040; float:right;" value="button-clear-filter" disabled="true" />
		<%
	}
	%>
	<button data-toggle="modal" data-target="#projectFilterModal" style="width:150px; color:#404040; float:right; margin-right: 5px;" class="btn"><liferay-ui:message key="button-filter" /></button>
</aui:button-row>

<input type="hidden" id="orderByCol" value="<%= orderByCol %>" />
<input type="hidden" id="orderByType" value="<%= HtmlUtil.escape(orderByType) %>" />
<input type="hidden" id="screenName" value="project_list" />
<input type="hidden" id="clearFilterProjectsURL" value="<%= clearFilterProjectsURL %>" />
<input type="hidden" id="downloadProjectListURL" value="<%= downloadProjectListURL %>" />
<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
<input type="hidden" id="hasError" value="<%= bHasError %>" />

<!-- Modal -->
<div class="modal fade" id="projectFilterModal" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false" style="width: auto; display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<liferay-ui:message key="header-project-filter" />
			</div>
			<form action="<%= getProjectsURL.toString() %>" method="post" name="fm" id="filterForm" style="margin: 0;">
				<div class="modal-body">
					<table>
						<tr>
							<td><liferay-ui:message key="label-id" /></td>
							<td><aui:input type="text" name="<%= PortalConstants.PARAM_PROJECT_ID %>" label="" value="<%= strSearchedProjectId %>" /></td>
						</tr>
						
						<tr>
							<td><liferay-ui:message key="label-group-name" /></td>
							<td><aui:input type="text" name="<%= PortalConstants.PARAM_OWNER_GROUP %>" label="" value="<%= strSearchedGroupName %>" /></td>
						</tr>
						
						<tr>
							<td><liferay-ui:message key="label-case-number" /></td>
							<td><aui:input type="text" name="<%= PortalConstants.PARAM_CASE_NUMBER %>" label="" value="<%= strSearchedCaseNumber %>" /></td>
						</tr>
						
						<tr>
							<td><liferay-ui:message key="label-project-name" /></td>
							<td><aui:input type="text" name="<%= PortalConstants.PARAM_PROJECT_NAME %>" label="" value="<%= strSearchedProjectName %>" /></td>
						</tr>
						
						<tr>
							<td><liferay-ui:message key="label-project-end-date" /></td>
							<td>
								<div class="form-group date-input-text">
									<%
										year = 0;
										month = -1;
										day = 0;
										
										if (dteSearchedProjectEndDateLow != null) {
											year = dteSearchedProjectEndDateLow.get(Calendar.YEAR);
											month = dteSearchedProjectEndDateLow.get(Calendar.MONTH);
											day = dteSearchedProjectEndDateLow.get(Calendar.DAY_OF_MONTH);
										}
									%>
									<liferay-ui:input-date name="<%= PortalConstants.PARAM_PROJECT_END_DATE_LOW %>" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= day %>" />
								</div>
							</td>
							<td class="date-hyphen">~</td>
							<td>
								<div class="form-group date-input-text">
									<%
										year = 0;
										month = -1;
										day = 0;
										
										if (dteSearchedProjectEndDateHigh != null) {
											year = dteSearchedProjectEndDateHigh.get(Calendar.YEAR);
											month = dteSearchedProjectEndDateHigh.get(Calendar.MONTH);
											day = dteSearchedProjectEndDateHigh.get(Calendar.DAY_OF_MONTH);
										}
									%>
									<liferay-ui:input-date name="<%= PortalConstants.PARAM_PROJECT_END_DATE_HIGH %>" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= day %>" />
								</div> 
							</td>
						</tr>
						
						<tr>
							<td><liferay-ui:message key="label-status" /></td>
							<td>
								<aui:select name="<%= PortalConstants.PARAM_STATUS %>" label="">
									<aui:option></aui:option>
									<aui:option value="<%= ProjectStatus.COMPLETE.getInteger() %>" selected="<%= bCompleteSelected %>"><liferay-ui:message key="<%= PortalConstants.KEY_PROJECT_STATUS_COMPLETE %>" /></aui:option>
									<aui:option value="<%= ProjectStatus.NOT_YET_COMPLETE.getInteger() %>" selected="<%= bNotCompleteSelected %>"><liferay-ui:message key="<%= PortalConstants.KEY_PROJECT_STATUS_OPEN %>" /></aui:option>
								</aui:select>
							</td>
						</tr>
						
						<tr>
							<td><liferay-ui:message key="label-no-of-scans" /></td>
							<td><aui:input type="text" name="<%= PortalConstants.PARAM_NO_OF_SCANS %>" label="" value="<%= strSearchedScanCount %>" /></td>
						</tr>
					</table>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn" onclick="searchProjects();"><liferay-ui:message key="button-filter" /></button>
					<button type="button" class="btn" data-dismiss="modal"><liferay-ui:message key="button-cancel" /></button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="downloadSummaryModal" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false" style="width: auto; display: none;">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<liferay-ui:message key="header-download-summary" />
			</div>
			<form action="<%= downloadSummaryURL.toString() %>" method="post" name="downloadSummaryFM" style="margin: 0;">
				<div class="modal-body">
					<table>
						<tr>
							<td><liferay-ui:message key="label-period" /></td>
							<td>
								<div class="form-group date-input-text">
									<liferay-ui:input-date name="<%= PortalConstants.PARAM_PERIOD_START_DATE %>" />
								</div>
							</td>
							<td class="date-hyphen">~</td>
							<td>
								<div class="form-group date-input-text">
									<liferay-ui:input-date name="<%= PortalConstants.PARAM_PERIOD_END_DATE %>" />
								</div>
							</td>
							
						</tr>
					</table>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn" onclick="<%= downloadSummary %>"><liferay-ui:message key="button-download" /></button>
					<button type="button" class="btn" data-dismiss="modal"><liferay-ui:message key="button-cancel" /></button>
				</div>
			</form>
		</div>
	</div>
</div>

<%
	if (bIsFromSearch && !CommonUtil.isStringNullOrEmpty(strSearchInfo)) {
		%>
			<div class="alert alert-info">
				<liferay-ui:message arguments="<%= HtmlUtil.escape(strSearchInfo) %>" key="<%= PortalConstants.KEY_FILTER_LIST %>" localizeKey="<%= false %>" />
			</div>
		<%
	}

int totalSize = 0;

if (!CommonUtil.isListNullOrEmpty(projectsList)) {
	if (bIsFromSearch) {
		totalSize = ControllerHelper.getSearchedProjectsCount(ProjectType.CX_SUITE.getInteger(), searchedProject, lUserId);
	} else {
		totalSize = ControllerHelper.getProjectsCount(lUserId, ProjectType.CX_SUITE.getInteger());
	}
}
%>

<liferay-ui:search-container emptyResultsMessage="message-no-projects-to-display" orderByType="<%= HtmlUtil.escape(orderByType) %>" orderByCol="<%= orderByCol %>" total="<%= totalSize %>">
	<liferay-ui:search-container-results>
		<%
		int projectsCount = PortalConstants.INT_ZERO;
		
		if (!CommonUtil.isListNullOrEmpty(projectsList)) {
			projectsCount = projectsList.size();
			
			if (projectsCount > 0) {
				results = projectsList;
				
				pageContext.setAttribute("results", results);
			}
		}
	%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="jp.ubsecure.portal.jubjub.portlet.model.ProjectItem" keyProperty="projectId" modelVar="project" escapedModel="<%= true %>">
		<%
			long projectId = project.getProjectId();
			String strProjectId = String.format("%05d", projectId);
			
			String className = "default-background";
			Calendar now = Calendar.getInstance();
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			
			if (project.getProjectEndDate().before(now.getTime())
					&& project.getStatus() == ProjectStatus.NOT_YET_COMPLETE.getInteger()) {
				className = "end-project-background";
			}
		%>
		<liferay-ui:search-container-column-text
			name="label-id"
			orderable="<%= true %>"
			orderableProperty="projectId"
			cssClass="<%= className %>"
			value="<%= strProjectId %>" />
		
		<liferay-ui:search-container-column-text
			name="label-group-name"
			orderable="<%= true %>"
			orderableProperty="groupName"
			cssClass="<%= className %>"
			value="<%= HtmlUtil.escapeAttribute(project.getOwnerGroupName()) %>" />
		
		<liferay-ui:search-container-column-text
			name="label-case-number"
			orderable="<%= true %>"
			orderableProperty="caseNumber"
			cssClass="<%= className %>"
			value="<%= project.getCaseNumber() %>" />
		
		<liferay-ui:search-container-column-text
			name="label-project-name"
			orderable="<%= true %>"
			orderableProperty="projectName"
			cssClass="<%= className %>"
			value="<%= HtmlUtil.escapeAttribute(project.getProjectName()) %>" />
		
		<%
			DateFormat formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT);
		%>
		
		<liferay-ui:search-container-column-text
			name="label-project-end-date"
			orderable="<%= true %>"
			orderableProperty="projectEndDate"
			cssClass="<%= className %>"
			value="<%= formatter.format(project.getProjectEndDate()) %>" />
		
		<%
			String strStatus = PortalConstants.STRING_EMPTY;
		
			if (project.getStatus() == ProjectStatus.COMPLETE.getInteger()) {
				strStatus = LanguageUtil.get(request, PortalConstants.KEY_PROJECT_STATUS_COMPLETE);
			}
		%>
		<liferay-ui:search-container-column-text
			name="label-status"
			orderable="<%= true %>"
			orderableProperty="status"
			cssClass="<%= className %>"
			value="<%= strStatus %>" />
		
		<liferay-ui:search-container-column-text
			name="label-no-of-scans"
			orderable="<%= true %>"
			orderableProperty="scanCount"
			cssClass="<%= className %>"
			value="<%= String.valueOf(project.getScanCount()) %>" />
		
		<liferay-ui:search-container-column-jsp
			name="button-action"
			cssClass="<%= className %>"
			align="right"
			path="<%= PortalConstants.CX_PROJECT_ACTION_JSP %>" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator />
</liferay-ui:search-container>
<%
	} else {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="message-no-access-rights" />
		</div>
		<%
	}
%>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/cxsuite/project_list.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#<portlet:namespace />projectEndDateLow").click(function() {
			changeZIndex();
		});
	
		$("#<portlet:namespace />projectEndDateHigh").click(function() {
			changeZIndex();
		});
		
		$("#<portlet:namespace />startDate").click(function () {
			changeZIndex();
		});
	
		$("#<portlet:namespace />endDate").click(function () {
			changeZIndex();
		});
		
		function changeZIndex () {
			var datePicker = document.getElementsByClassName("datepicker-popover");
			   
			if (datePicker != null) {
				for (var index = 0; index < datePicker.length; index++) {
					$(datePicker[index]).css("z-index", 2000);
				}
			}
		}
	});
</script>
<script>
	define.amd = define._amd;
</script>