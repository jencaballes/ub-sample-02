/**
 * Vex
 */

var focus = false;
var prevElemDetectionResult = "";
var row = 0;

$(document).ready(function() {
	var screenName = $('#screenName').val();
	var screenNo = $("#screenNo").val();
	var browse = $("#browseBtn");
	browse.focus();
	
	$("#nonExistingScanError").addClass("hideError");
	
	if (screenName == "refresh_page") {
		var sortedColumn = $("#orderByCol").val();
		var sortType = $("#orderByType").val();
		var screenList = $("#screenList").val();
		
		showSortIndicator(sortedColumn, sortType, screenList);
		
		//showHeaderLink(screenNo);
		
		/* window.setInterval(function(){
			refreshScan();
		}, 10000);*/
	}
	
	$(function() {
		var focusedElement = null;
		$(document).on('focus', 'input', function() {
			if (focusedElement == $(this)) return;
			focusedElement = $(this);
			setTimeout(function() {
				focusedElement.select();
			}, 50);
		});
	});
	
});

$('.dropdown-toggle').click(function () {
	focus = true;
	row = $(this).closest('tr').index() + 1;
});

$(document).click(function(e) {
	if (focus) {
		focus = false;
	}
});

function showSortIndicator (sortedColumn, sortType, screenList) {
	var tableColumn = 0;
		switch (sortedColumn) {
			case "scanresultid":
				tableColumn = 1;
				break;
			case "detectionresultid":
				tableColumn = 2;
				break;
			case "risklevel":
				tableColumn = 3;
				break;
			case "category":
				tableColumn = 4;
				break;
			case "overview":
				tableColumn = 5;
				break;
			case "functionname":
				tableColumn = 6;
				break;
			case "url":
				tableColumn = 7;
				break;
			case "parametername":
				tableColumn = 8;
				break;
			case "detectionjudgment":
				tableColumn = 9;
				break;
			case "reviewcomment":
				tableColumn = 10;
				break;
		}
	
	
	$('.lfr-search-container ').find('thead').find('tr').each(function(index) {
		$(document).find('th:nth-child(' + tableColumn + ')').addClass('table-sorted');
		
		if (sortType == "desc") {
			$(document).find('th:nth-child(' + tableColumn + ')').addClass('table-sorted-desc');
		}
	});
}

function showHeaderLink (screenNo) {
	if (screenNo == 3) {
		$('.search-div').find('.lfr-search-container ').find('thead').find('tr').each(function(index) {
			var strHeader = "レポート <a href='#' style='font-weight: bold; font-size: 8px; text-decoration: underline;' onclick=\"downloadVulnerability('"+$('#downloadVuln').val()+"'); return false;\">(解説)</a>";
			$(document).find('th:nth-child(9)').html(strHeader);
		});
	} else if (screenNo == 4) {
		$('.search-div').find('.lfr-search-container ').find('thead').find('tr').each(function(index) {
			var strHeader = "レポート <a href='#' style='font-weight: bold; font-size: 8px; text-decoration: underline;' onclick=\"downloadVulnerability('"+$('#downloadVuln').val()+"'); return false;\">(解説)</a>";
			
			if ($(document).find('th:first-child').text().indexOf('ID') > -1) {
				$(this).closest('tr').find('th:nth-child(7)').html(strHeader);
			}
		});
	}
}

function enableReviewComment(elem, resultUrl){
	var portletNamespace = $("#portletNamespace").val();
	if(elem.value == "1"){
		document.getElementById("selectedDetectionJudgementRow").value=elem.id;
		enableReviewCommentModal(resultUrl);
	}
}


function enableReviewCommentModal(resultUrl){
	 document.getElementById("confirmationCommentForm").action=resultUrl;
	 document.getElementById("confirmationCommentBtn").click();
}

function resetOption(){
	var elem = document.getElementById("selectedDetectionJudgementRow").value;
	document.getElementById(elem).selectedIndex = 0;
}

function submitConfirmation(){
	var portletNamespace = $("#portletNamespace").val();
	var url = $("#confirmationCommentForm")[0].action;
	var detectionResultUrl =  $("#viewDetectionResultURL").val();
	var reviewCommentName =portletNamespace+"reviewcomment"; 
	var reviewCommentValue=$("#"+reviewCommentName).val();
	url = url +"&"+reviewCommentName+"="+reviewCommentValue;
	$.ajax({
		url: url,
		async:false,
		success: function (data) {
			if (data.redirect == false) {
				window.location.href = detectionResultUrl;
			} else {
				window.location.href = detectionResultUrl;
			}
		}
	});
}

function confirmResendResult (resultUrl, iIsResend) {
	var msg = "本当に脆弱性が検出されるか手動で確認を行う？";
	if(iIsResend==1){
		msg = "登録したサイトにリクエストが発生します。よろしいですか？";
	}
	var url = resultUrl;
	var scanListUrl=$("#viewScanListURL").val();
	var screenNo = $("#screenNo").val();
	var detectionResultUrl =  $("#viewDetectionResultURL").val();
	
	var portletNamespace = $("#portletNamespace").val();
	var number = resultUrl.split(portletNamespace+"number=")[1];
	var newNumber = number.split("&")[0];
	
	var reviewComment =  portletNamespace+"reviewcomment_"+newNumber;
	var reviewVal = $("#"+reviewComment).val();
	
	var newReviewComment=portletNamespace+"reviewcomment";
	if(reviewVal != null && reviewVal != ''){
		url=resultUrl+"&"+newReviewComment+"="+reviewVal;
	}
	//disableHover(scanId);
	
	var proceed = confirm(msg);
	if (proceed) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = detectionResultUrl;
			} else {
				window.location.href = detectionResultUrl;
			}
		} else {
			$.ajax({
				url: url,
				async:false,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = detectionResultUrl;
					} else {
						window.location.href = detectionResultUrl;
					}
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function viewDetectionResult (scanId) {
	var msg = "本当によろしいですか？";
	var url = $("#viewDetectionResultURL_" + scanId).val()
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl=$("#viewScanListURL").val();
	var screenNo = $("#screenNo").val();
	
//	if (screenNo == 4) {
//		scanListUrl = $("#viewScanListURL").val();
//	} else if (screenNo == 3) {
//		scanListUrl = $("#viewEntireScanListURL").val();
//	}
	disableHover(scanId);
	
	var proceed = confirm(msg);
	if (proceed) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = url;
			}
		} else {
			$.ajax({
				url: url,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = url;
					}
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function showOrHideResendResultTable(resendNumber){
	var elemId = "#resend_result_table_"+resendNumber;
	
	if(prevElemDetectionResult == ""){
		prevElemDetectionResult = elemId;
	} else if (prevElemDetectionResult != elemId){
		$(''+prevElemDetectionResult+'').hide();
		prevElemDetectionResult = elemId;
	}
	
	var isElemHidden = $(''+elemId+'').is(':hidden');
	if(isElemHidden == true){
		$(''+elemId+'').show();
	}else{
		$(''+elemId+'').hide();
	}
	
}

function disableHover(scanId) {
	var strScanId = "";
	
	if (scanId < 10) {
		strScanId = "0000" + scanId;
	} else if (scanId < 100) {
		strScanId = "000" + scanId;
	} else if (scanId < 1000) {
		strScanId = "00" + scanId;
	} else if (scanId < 10000) {
		strScanId = "0" + scanId;
	} else {
		strScanId = scanId + "";
	}
	
	$('.table').removeClass('table-hover');

	$("tr td:contains('"+ strScanId +"')").each(function(){
		$(this).closest('tr').children('td').css('background-color', '#EDF8FD');
	});
}

function isBrowserIE () {
	var browser = navigator.userAgent;
	
	return (browser.indexOf("MSIE", 0) > -1 || (browser.indexOf("Firefox", 0) == -1 && browser.indexOf("Chrome", 0) == -1));
}
