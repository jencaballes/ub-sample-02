/**
 * Vex
 */

$(document).ready(function() {
	var screenName = $("#screenName").val();
	
	if (screenName == "edit_project") {
		var portletNamespace = $("#portletNamespace").val();
		var fieldNumber = $("#fieldNumber").val();
		var userListSortOrder = $("#userListSortOrder").val();
		var availableUsersSortOrder = $("#availableUsersSortOrder").val();
		
		if (fieldNumber == 0 || fieldNumber == 1) {
			var caseNumber = $("#" + portletNamespace + "caseNumber");
			caseNumber.focus();
		} else if (fieldNumber == 2) {
			var projectName = $("#" + portletNamespace + "projectName");
			projectName.focus();
		} else if (fieldNumber == 3) {
			var ownerGroup = $("#ownerGroup");
			ownerGroup.focus();
		} else if (fieldNumber == 4) {
			var projectEndDate = $("#" + portletNamespace + "projectEndDate");
			projectEndDate.focus();
		} else if (fieldNumber == 10) {
			var projectUsers = $("#userList");
			projectUsers.focus();
		}
		
		var selectedUsers = $("#selectedUsers").val();
		
		if (selectedUsers != null && selectedUsers != "") {
			users = selectedUsers.split(",");
		}
		
		if (userListSortOrder == "asc") {
			document.getElementById("userListSortUp").style.color="#555";
			document.getElementById("userListSortDown").style.color="#AAA";
		} else if (userListSortOrder == "desc") {
			document.getElementById("userListSortUp").style.color="#AAA";
			document.getElementById("userListSortDown").style.color="#555";
		}
		
		if (availableUsersSortOrder == "asc") {
			document.getElementById("availableUsersSortUp").style.color="#555";
			document.getElementById("availableUsersSortDown").style.color="#AAA";
		} else if (availableUsersSortOrder == "desc") {
			document.getElementById("availableUsersSortUp").style.color="#AAA";
			document.getElementById("availableUsersSortDown").style.color="#555";
		}
	}
	
	$(".selectable").selectable();

	$("#ownerGroup").on("change", function() {
		var selectedGroup = $(this).val();
		var url = $("#url").val() + "?r=" + generateID();
		var availableUsersSortOrder = $("#availableUsersSortOrder").val().trim();
		
		$("#userList").empty();
		$("#selectedUsers").val("");
		
		$("#userIdError").addClass("hideError");
		$("#userNameError").addClass("hideError");
		$("#dbConnError").addClass("hideError");
		$("#userDoesNotExistError").addClass("hideError");
		$("#userDoesNotBelongToGroupError").addClass("hideError");
		
		$("#userId").val("");
		$("#userName").val("");
		
		$.ajax({
			url: url,
			data: {selectedGroup : selectedGroup,
				userAction : 22,
				availableUsersSortOrder : availableUsersSortOrder},
			dataType: "JSON",
			success: function(data) {
				var errorMsg = data[0].errorMsg;
				var availableUsers = data[1].orgUsers;
				var options = "";
				
				var length = availableUsers.length;
				
				for (var i = 0; i < length; i++) {
					options += "<li id='" + availableUsers[i].userId + "'>" + availableUsers[i].screenName + " : " + availableUsers[i].emailAddress + "</li>";
				}
				
				if (errorMsg != null) {
					if (errorMsg.length > 0 && errorMsg[0].DBConnError == true) {
						$("#dbConnError").removeClass("hideError");
					}
				}
				
				$("#availableUsers").html(options);
			}
		});
	});
});

function filterUsers () {
	var userId = $("#userId").val().trim();
	var userName = $("#userName").val().trim();
	var selectedUsers = $("#selectedUsers").val();
	var userListSortOrder = $("#userListSortOrder").val().trim();
	var availableUsersSortOrder = $("#availableUsersSortOrder").val().trim();
	
	$("#userId").val(userId);
	$("#userName").val(userName);
	
	var url = $("#url").val();
	var selectedGroup = $("#ownerGroup").val();
	
	$("#userIdError").addClass("hideError");
	$("#userNameError").addClass("hideError");
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	userName = encodeURI(userName);
	
	$.ajax({
		url: url,
		data: {selectedGroup: selectedGroup,
			userId : userId,
			userName : userName,
			userAction : 26,
			selectedUsers : selectedUsers,
			userListSortOrder : userListSortOrder,
			availableUsersSortOrder : availableUsersSortOrder},
		dataType: "JSON",
		success: function(data) {
			var errorMsg = data[0].errorMsg;
			var availableUsers = data[1].availableUsers;
	
			var availableOptions = "";
			var length = availableUsers.length;
			for (var i = 0; i < length; i++) {
				availableOptions += "<li id='" + availableUsers[i].userId + "'>" + availableUsers[i].screenName + " : " + availableUsers[i].emailAddress + "</li>";
			}
			
			length = errorMsg.length;
			if (length > 0 && errorMsg[0].userIdTooLong) {
				$(".alert").addClass("hideError");
				$("#userIdError").removeClass("hideError");
			} else if (length > 1 && errorMsg[1].userNameTooLong) {
				$(".alert").addClass("hideError");
				$("#userNameError").removeClass("hideError");
			} else if (length > 2 && errorMsg[2].DBConnError == true) {
				$(".alert").addClass("hideError");
				$("#dbConnError").removeClass("hideError");
			}
			
			$("#availableUsers").html(availableOptions);
		}
	});
}

function addUser () {
	var selectedUser = document.getElementsByClassName("ui-selected");
	var length = selectedUser.length;
	var userListOptions = "";
	var availableUsersOptions = "";
	var selectedUsers = $("#selectedUsers").val();
	var url = $("#url").val();
	var selectedGroup = $("#ownerGroup").val();
	var userId = $("#userId").val().trim();
	var userName = $("#userName").val().trim();
	var userListSortOrder = $("#userListSortOrder").val().trim();
	var availableUsersSortOrder = $("#availableUsersSortOrder").val().trim();
	
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	$("#userId").val(userId);
	$("#userName").val(userName);
	
	userName = encodeURI(userName);
	
	for (var i = 0; i < length; i++) {
		if (selectedUser[i].parentNode.id == "availableUsers") {
			selectedUsers += selectedUser[i].id + ",";
		}
	}
	
	$.ajax({
		url: url,
		data: {selectedUsers: selectedUsers,
			selectedGroup: selectedGroup,
			userAction: 27,
			userId : userId,
			userName : userName,
			userListSortOrder : userListSortOrder,
			availableUsersSortOrder : availableUsersSortOrder},
		dataType: "JSON",
		async: false,
		success: function(data) {
			var errorMsg = data[0].errorMsg;
			var userList = data[1].usersList;
			var availableUsers = data[2].availableUsers;
			
			selectedUsers = "";
			length = userList.length;
			for (var i = 0; i < length; i++) {
				userListOptions += "<li id='" + userList[i].userId + "'>" + userList[i].screenName + " : " + userList[i].emailAddress + "</li>";
				selectedUsers += userList[i].userId + ",";
			}
			
			length = availableUsers.length;
			for (var i = 0; i < length; i++) {
				availableUsersOptions += "<li id='" + availableUsers[i].userId + "'>" + availableUsers[i].screenName + " : " + availableUsers[i].emailAddress + "</li>";
			}
			
			if (errorMsg != null) {
				if (errorMsg.length > 0) {
					if (errorMsg[0].DBConnError == true) {
						$("#dbConnError").removeClass("hideError");
					} else if (errorMsg[0].userDoesNotExist == true) {
						$("#userDoesNotExistError").removeClass("hideError");
					} else if (errorMsg[0].userDoesNotBelongToGroup == true) {
						$("#userDoesNotBelongToGroupError").removeClass("hideError");
					}
				}
			}
			
			
		}
	});
	
	$("#userList").html(userListOptions);
	$("#availableUsers").html(availableUsersOptions);
	$("#selectedUsers").val(selectedUsers);
}

function removeUser () {
	var toRemoveUsers = document.getElementsByClassName("ui-selected");
	var length = toRemoveUsers.length;
	var usersToRemove = "";
	var userListOptions = "";
	var availableUsersOptions = "";
	var selectedUsers = $("#selectedUsers").val();
	var url = $("#url").val();
	var selectedGroup = $("#ownerGroup").val();
	var userId = $("#userId").val().trim();
	var userName = $("#userName").val().trim();
	var userListSortOrder = $("#userListSortOrder").val().trim();
	var availableUsersSortOrder = $("#availableUsersSortOrder").val().trim();
	
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	$("#userId").val(userId);
	$("#userName").val(userName);
	
	userName = encodeURI(userName);
	
	for (var i = 0; i < length; i++) {
		if (toRemoveUsers[i].parentNode.id == "userList") {
			usersToRemove += toRemoveUsers[i].id + ",";
		}
	}
	
	$.ajax({
		url: url,
		data: {selectedUsers: selectedUsers,
			usersToRemove: usersToRemove,
			selectedGroup: selectedGroup,
			userAction: 28,
			userId : userId,
			userName : userName,
			userListSortOrder : userListSortOrder,
			availableUsersSortOrder : availableUsersSortOrder},
		dataType: "JSON",
		async: false,
		success: function(data) {
			var errorMsg = data[0].errorMsg;
			var userList = data[1].usersList;
			var availableUsers = data[2].availableUsers;
			
			selectedUsers = "";
			length = userList.length;
			for (var i = 0; i < length; i++) {
				userListOptions += "<li id='" + userList[i].userId + "'>" + userList[i].screenName + " : " + userList[i].emailAddress + "</li>";
				selectedUsers += userList[i].userId + ",";
			}
			
			length = availableUsers.length;
			for (var i = 0; i < length; i++) {
				availableUsersOptions += "<li id='" + availableUsers[i].userId + "'>" + availableUsers[i].screenName + " : " + availableUsers[i].emailAddress + "</li>";
			}
			
			if (errorMsg != null) {
				if (errorMsg.length > 0) {
					if (errorMsg[0].DBConnError == true) {
						$("#dbConnError").removeClass("hideError");
					} else if (errorMsg[0].userDoesNotExist == true) {
						$("#userDoesNotExistError").removeClass("hideError");
					} else if (errorMsg[0].userDoesNotBelongToGroup == true) {
						$("#userDoesNotBelongToGroupError").removeClass("hideError");
					}
				}
			}
		}
	});

	$("#userList").html(userListOptions);
	$("#availableUsers").html(availableUsersOptions);
	$("#selectedUsers").val(selectedUsers);
}

function submitProjectForm () {
	var portletNamespace = $("#portletNamespace").val();
	$("#filterBtn").prop("disabled", true);
	$("#" + portletNamespace + "addBtn").prop("disabled", true);
	$("#" + portletNamespace + "removeBtn").prop("disabled", true);
	$("#saveBtn").prop("disabled", true);
	$("#cancelBtn").addClass("disabled");
	$("#cancelBtnHref").removeAttr("href");
	$("#filterBtn").removeAttr("onclick");
	$("#" + portletNamespace + "addBtn").removeAttr("onclick");
	$("#" + portletNamespace + "removeBtn").removeAttr("onclick");
	$("#cancelBtn").removeAttr("onclick");
	$("#saveBtn").removeAttr("onclick");
	
	document.getElementById("fm").submit();
}

function cancelAddUpdateProject () {
	var portletNamespace = $("#portletNamespace").val();
	$("#filterBtn").prop("disabled", true);
	$("#" + portletNamespace + "addBtn").prop("disabled", true);
	$("#" + portletNamespace + "removeBtn").prop("disabled", true);
	$("#saveBtn").prop("disabled", true);
	$("#cancelBtn").addClass("disabled");
	$("#saveBtn").removeAttr("onclick");
	$("#filterBtn").removeAttr("onclick");
	$("#" + portletNamespace + "addBtn").removeAttr("onclick");
	$("#" + portletNamespace + "removeBtn").removeAttr("onclick");
}

function byteCount (string) {
    return encodeURI(string).split(/%..|./).length - 1;
}

function sortUserList (order) {
	var my_options = $("#userList li");
	var my_new_options = checkSelectedUsers(my_options);
	
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	my_new_options = sort(my_new_options, order);
	
	var selectedUsers = "";
	var optionLength = my_new_options.length;
	for (var i = 0; i < optionLength; i++) {
		if ((my_new_options[i] != null && my_new_options[i] != "undefined")) {
			selectedUsers += my_new_options[i].id + ",";
		}
	}
	
	$("#userList").append(my_new_options);
	$("#selectedUsers").val(selectedUsers);
	$("#userListSortOrder").val(order);
	
	if (order == "asc") {
		document.getElementById("userListSortUp").style.color="#555";
		document.getElementById("userListSortDown").style.color="#AAA";
	} else if (order == "desc") {
		document.getElementById("userListSortUp").style.color="#AAA";
		document.getElementById("userListSortDown").style.color="#555";
	}
}

function sortAvailableUsers (order) {
	var my_options = $("#availableUsers li");
	var my_new_options = checkAvailableUsers(my_options);
	
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	my_new_options = sort(my_new_options, order);
	
	$("#availableUsers").append(my_new_options);
	$("#availableUsersSortOrder").val(order);
	
	if (order == "asc") {
		document.getElementById("availableUsersSortUp").style.color="#555";
		document.getElementById("availableUsersSortDown").style.color="#AAA";
	} else if (order == "desc") {
		document.getElementById("availableUsersSortUp").style.color="#AAA";
		document.getElementById("availableUsersSortDown").style.color="#555";
	}
}

function checkSelectedUsers (my_options) {
	var my_new_options = [];
	var selectedUsers = $("#selectedUsers").val();
	var userList = [];
	
	if (selectedUsers != null) {
		userList = selectedUsers.split(",");
	}
	
	var optionLength = my_options.length;
	var userListLength = userList.length;
	for (var i = 0; i < optionLength; i++) {
		for (var j = 0; j < userListLength; j++) {
			if ((my_options[i] != null && my_options[i] != "undefined") && my_options[i].id == userList[j]) {
				my_new_options[i] = my_options[i];
				break;
			}
		}
	}
	
	my_new_options = finalizeOptions(my_new_options);
	
	return my_new_options;
}

function checkAvailableUsers (my_options) {
	var my_new_options = [];
	var selectedUsers = $("#selectedUsers").val();
	var userList = [];
	
	if (selectedUsers != null) {
		userList = selectedUsers.split(",");
	}
	
	var optionLength = my_options.length;
	var userListLength = userList.length;
	for (var i = 0; i < optionLength; i++) {
		var isSelected = false;
		
		for (var j = 0; j < userListLength; j++) {
			if ((my_options[i] != null && my_options[i] != "undefined") && my_options[i].id == userList[j]) {
				isSelected = true;
				break;
			}
		}
		
		if (isSelected == false && (my_options[i] != null && my_options[i] != "undefined")) {
			my_new_options[my_new_options.length] = my_options[i];
		}
	}
	
	my_new_options = finalizeOptions(my_new_options);
	
	return my_new_options;
}

function finalizeOptions (my_options) {
	var my_new_options = [];
	
	var optionLength = my_options.length;
	for (var i = 0; i < optionLength; i++) {
		var isExisting = false;
		
		var myNewOptionsLength = my_new_options.length;
		for (var j = 0; j < myNewOptionsLength; j++) {
			if ((my_options[i] != null && my_options[i] != "undefined") && my_new_options[j].id == my_options[i].id) {
				isExisting = true;
				break;
			}
		}
		
		if (isExisting == false && (my_options[i] != null && my_options[i] != "undefined")) {
			my_new_options[myNewOptionsLength] = my_options[i];
		}
	}
	
	return my_new_options;
}

function sort (options, order) {
	if (order == "asc") {
		options.sort(function (a, b) {
			if ((a.textContent).toLowerCase() > (b.textContent).toLowerCase()) return 1;
			else if ((a.textContent).toLowerCase() < (b.textContent).toLowerCase()) return -1;
			else return 0;
		});
	} else if (order == "desc") {
		options.sort(function (a, b) {
			if ((a.textContent).toLowerCase() < (b.textContent).toLowerCase()) return 1;
			else if ((a.textContent).toLowerCase() > (b.textContent).toLowerCase()) return -1;
			else return 0;
		});
	}
	
	return options;
}

function addRow(tableID) {
	var portletNamespace = $("#portletNamespace").val();

	var table = document.getElementById(tableID);


	if(tableID === "targetURLTable"){
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount-1);
		var rowIndex = parseInt(document.getElementById(portletNamespace+"targetURLlistTableSize").value)+1;
	
		var cell1 = row.insertCell(0);
		var element1 = document.createElement("select");
		element1.name = portletNamespace+"targetURLprotocolGroup"+rowIndex;
		element1.setAttribute('style','width: auto !important');
		element1.setAttribute("onchange", "changePortNumbers(this,"+rowIndex+")");
		element1.setAttribute('class',"field form-control");
		element1.id = portletNamespace+"select"+rowIndex;		
			var option1 = document.createElement("option");
				option1.text = "http://";
				option1.value = 1;
		element1.add(option1);
			var option2 = document.createElement("option");
				option2.text = "https://";
				option2.value = 2;
		element1.add(option2);
		cell1.appendChild(element1);

		var cell2 = row.insertCell(1);
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = portletNamespace+"targetURLhost"+rowIndex;
		element2.id = portletNamespace+"targetURLhost"+rowIndex;
		element2.setAttribute('class',"field form-control");
		element2.setAttribute('placeholder', 'example.com');
		element2.setAttribute('style','width:400px;');
		cell2.appendChild(element2);

		var cell3 = row.insertCell(2);
		var element3 = document.createElement("input");
		element3.type = "text";
		element3.name = portletNamespace+"targetURLport"+rowIndex;
		element3.id = portletNamespace+"targetURLport"+rowIndex;
		element3.setAttribute('class',"field form-control");
		element3.setAttribute('placeholder', '80');
		element3.setAttribute('value', '80');
		element3.setAttribute('style','width:80px;');
		cell3.appendChild(element3);
	
		var cell4 = row.insertCell(3);
		var element4 = document.createElement("input");
		element4.type = "button";
		element4.name = portletNamespace+"targetURLbtnDelete"+rowIndex;
		element4.setAttribute('onclick',"deleteRow(this)");
		element4.setAttribute('class',"btn btn-default");
		element4.setAttribute('style','width:100%;margin-top:0px;');
	
		if ($('#labelProjectName').text() == "label-project-name") {
			element4.value = "button-delete";
		} else {
			element4.value = "削除";
		}
	
		element4.id = portletNamespace+"targetURLbtnDelete"+ rowIndex;
		cell4.appendChild(element4);

		//var currentSize = document.getElementById(portletNamespace+"currentListTableSize").value;
	
		document.getElementById(portletNamespace+"targetURLlistTableSize").value = rowIndex;
		//document.getElementById(portletNamespace+"currentListTableSize").value = ++currentSize;
	}else if(tableID === "productionEnvUrlTable"){
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount-1);
		var rowIndex = parseInt(document.getElementById(portletNamespace+"productionEnvUrllistTableSize").value)+1;
	
		var cell1 = row.insertCell(0);
		var element1 = document.createElement("select");
		element1.name = portletNamespace+"productionEnvUrlprotocolGroup"+rowIndex;
		element1.setAttribute("onchange", "changeProdPortNumbers(this,"+rowIndex+")");
		element1.setAttribute('style','width: auto !important');
		element1.setAttribute('class',"field form-control");
		element1.id = portletNamespace+"select"+rowIndex;		
			var option1 = document.createElement("option");
				option1.text = "http://";
				option1.value = 1;
		element1.add(option1);
			var option2 = document.createElement("option");
				option2.text = "https://";
				option2.value = 2;
		element1.add(option2);
		cell1.appendChild(element1);

		var cell2 = row.insertCell(1);
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = portletNamespace+"productionEnvUrlhost"+rowIndex;
		element2.id = portletNamespace+"productionEnvUrlhost"+rowIndex;
		element2.setAttribute('class',"field form-control");
		element2.setAttribute('placeholder', 'example.com');
		element2.setAttribute('style','width:400px;');
		cell2.appendChild(element2);

		var cell3 = row.insertCell(2);
		var element3 = document.createElement("input");
		element3.type = "text";
		element3.name = portletNamespace+"productionEnvUrlport"+rowIndex;
		element3.id = portletNamespace+"productionEnvUrlport"+rowIndex;
		element3.setAttribute('class',"field form-control");
		element3.setAttribute('placeholder', '80');
		element3.setAttribute('value', '80');
		element3.setAttribute('style','width:80px;');
		cell3.appendChild(element3);
	
		var cell4 = row.insertCell(3);
		var element4 = document.createElement("input");
		element4.type = "button";
		element4.name = portletNamespace+"productionEnvUrlbtnDelete"+rowIndex;
		element4.setAttribute('onclick',"deleteRow(this)");
		element4.setAttribute('class',"btn btn-default");
		element4.setAttribute('style','width:100%;margin-top:0px;');
	
		if ($('#labelProjectName').text() == "label-project-name") {
			element4.value = "button-delete";
		} else {
			element4.value = "削除";
		}
	
		element4.id = portletNamespace+"productionEnvUrlbtnDelete"+ rowIndex;
		cell4.appendChild(element4);

		//var currentSize = document.getElementById(portletNamespace+"currentListTableSize").value;
	
		document.getElementById(portletNamespace+"productionEnvUrllistTableSize").value = rowIndex;
		//document.getElementById(portletNamespace+"currentListTableSize").value = ++currentSize;
	}

}

function deleteRow(button) {
	var portletNamespace = $("#portletNamespace").val();
	var tableID = button.parentNode.parentNode.parentNode.parentNode.id;
    var tableRowCount = button.parentNode.parentNode.parentNode.parentNode.rows.length;

	if(tableID === "targetURLTable"){
		var rowCount = document.getElementById(portletNamespace+"targetURLlistTableSize").value;
	}else if(tableID === "productionEnvUrlTable"){
		var rowCount = document.getElementById(portletNamespace+"productionEnvUrllistTableSize").value;
	}

	var i = button.parentNode.parentNode.rowIndex;
	if(i != 1 || tableRowCount != 3){ //table header,one rowItem,add row
		if(tableID === "targetURLTable"){
			document.getElementById("targetURLTable").deleteRow(i);
		}else if(tableID === "productionEnvUrlTable"){
			document.getElementById("productionEnvUrlTable").deleteRow(i);
		}
	}
}

function changePortNumbers(select, index) {
	var portletNamespace = $("#portletNamespace").val();
	
	if(select.value == 1){
		document.getElementById(portletNamespace+"targetURLport"+index).value = "80"; 
		document.getElementById(portletNamespace+"targetURLport"+index).innerText = "80"; 
		
	}else if(select.value == 2 ){
		document.getElementById(portletNamespace+"targetURLport"+index).value = "443"; 
		document.getElementById(portletNamespace+"targetURLport"+index).innerText = "443"; 
	}
}

function changeProdPortNumbers(select, index) {
	var portletNamespace = $("#portletNamespace").val();
	
	if(select.value == 1){
		document.getElementById(portletNamespace+"productionEnvUrlport"+index).value = "80"; 
		document.getElementById(portletNamespace+"productionEnvUrlport"+index).innerText = "80"; 
		
	}else if(select.value == 2 ){
		document.getElementById(portletNamespace+"productionEnvUrlport"+index).value = "443"; 
		document.getElementById(portletNamespace+"productionEnvUrlport"+index).innerText = "443"; 
	}
}


function isBrowserIE () {
	var browser = navigator.userAgent;
	
	return (browser.indexOf("MSIE", 0) > -1 || (browser.indexOf("Firefox", 0) == -1 && browser.indexOf("Chrome", 0) == -1));
}

function generateID () {
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	var id = "";
	
	for (var count = 0; count < 15; count++) {
		id += possible.charAt(getRandomInt(1, 62));
	}
	
	return id;
}

function getRandomInt (min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * ((max - min) + 1)) + min;
}