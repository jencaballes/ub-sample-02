<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/html/portal/init.jsp" %>

<%@ page errorPage = "/html/portal/update_password.jsp" %>

<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Set" %>

<%
String currentURL = PortalUtil.getCurrentURL(request);

String referer = ParamUtil.getString(request, WebKeys.REFERER, currentURL);

Ticket ticket = (Ticket)request.getAttribute(WebKeys.TICKET);

String ticketKey = ParamUtil.getString(request, "ticketKey");

if (referer.startsWith(themeDisplay.getPathMain() + "/portal/update_password") && Validator.isNotNull(ticketKey)) {
	referer = themeDisplay.getPathMain();
}

PasswordPolicy passwordPolicy = user.getPasswordPolicy();

String strScreenName = HtmlUtil.escape(user.getFirstName());

boolean bShowDBConnError = false;

try {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	bShowDBConnError = true;
}
%>

<c:choose>
	<c:when test="<%= SessionErrors.contains(request, UserLockoutException.class.getName()) %>">
		<div id="dbConnError" class="alert alert-danger">
			<liferay-ui:message key="this-account-has-been-locked" />
		</div>
	</c:when>
	<c:otherwise>
		<aui:form action='<%= themeDisplay.getPathMain() + "/portal/update_password" %>' method="post" name="fm" id="fm">
			<aui:input name="dbConnError" type="hidden" value="<%= bShowDBConnError %>" />
			<aui:input name="p_l_id" type="hidden" value="<%= layout.getPlid() %>" />
			<aui:input name="p_auth" type="hidden" value="<%= AuthTokenUtil.getToken(request) %>" />
			<aui:input name="doAsUserId" type="hidden" value="<%= themeDisplay.getDoAsUserId() %>" />
			<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
			<aui:input name="<%= WebKeys.REFERER %>" type="hidden" value="<%= referer %>" />
			<aui:input name="ticketKey" type="hidden" value="<%= ticketKey %>" />

			<!-- Alert info is removed from here and is moved down after header New Password -->

			
			<!--aui:fieldset  orig liferay: newpassword -->				
				<div style="color: rgb(59, 137, 175); font-weight: bold; font-size:9px; height:4px;">
					<liferay-ui:message key="header-initial-password-change" ></liferay-ui:message>					
				</div>
				<div style="height:14px;">
					<hr style="height:2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
				</div>	 			
			    <%-- <div class="alert alert-info" style="font-weight:bold; background-color: #DDEFE3; border-color:#4BCD61; color: #67D47B;">
					<!-- liferay-ui:message key="please-set-a-new-password" /-->
					<liferay-ui:message key="message-operation-execution-result"/>
			    </div> --%>
			     <%--
			    <label class="aui-field-label" for="<portlet:namespace/>password1"><%=password %></label>     
				<aui:input inlineLabel="true"  autoFocus="<%= true %>" class="lfr-input-text-container" label="" name="password1" type="password" />
				<aui:input inlineLabel="true" class="lfr-input-text-container" label="enter-again" name="password2" type="password" />
				--%>
				<!-- <div id="err" class="alert alert-error">パスワードを正しく入力してください</div> -->
				
				<%
					if (bShowDBConnError) {
						%>
						<div class="alert alert-danger">
							<liferay-ui:message key="error-orm-exception" />
						</div>
						<%
					} else if (SessionErrors.contains(request, UserPasswordException.MustMatch.class)) {
						%>
						<div class="alert alert-danger">
							<liferay-ui:message key="error-password-did-not-match" />
						</div>
						<%
					} else if (SessionErrors.contains(request, UserPasswordException.MustNotBeNull.class)
							|| SessionErrors.contains(request, UserPasswordException.MustBeLonger.class)
							|| SessionErrors.contains(request, UserPasswordException.MustComplyWithRegex.class)) {
						%>
						<div class="alert alert-danger">
							<liferay-ui:message key="error-password-invalid" />
						</div>
						<%
					} else if (SessionErrors.contains(request, UserPasswordException.MustNotBeEqualToCurrent.class)) {
						%>
						<div class="alert alert-danger">
							<liferay-ui:message key="error-new-password-same-as-old-password" />
						</div>
						<%
					}
				%>
				</div>
				
				<div class="initial-password-change-form">					  	
				  <table>
				        <tr>
				            <td style="color:#606060;width:140px; padding: 10px 20px 10px 0px;" >
				            	<liferay-ui:message key="label-user-name" > &nbsp; &nbsp; </liferay-ui:message>
				            </td>
				            <td style="color:#606060; padding: 10px 0px 10px 0px;">
				            	<liferay-ui:message key="<%= strScreenName %>" > </liferay-ui:message> 
				            </td>
				        </tr>
				        <tr style="height:0px;"></tr>	
				        <tr>
				            <td style="color:#606060;width:0px;padding: 10px 20px 10px 0px;" >
				            	<liferay-ui:message key="label-new-password" > &nbsp; &nbsp; </liferay-ui:message>
				            </td>
				            <td style="padding: 10px 0px 10px 0px;">
				                <aui:input id="pass1" name="password1" type="password" label="" />				                
				            </td>
				        </tr>
				        <tr style="height:0px;"></tr>	
				        <tr>
				            <td style="color:#606060;width:0px;padding: 10px 20px 10px 0px;" >
				            	<liferay-ui:message key="label-new-password-retype"></liferay-ui:message>
				            </td>
				            <td style="padding: 10px 0px 10px 0px;">
				                <aui:input id="pass2" name="password2" type="password" label="" />				              
				            </td>
				        </tr>
				  </table>		
				</div>			
			<!-- /aui:fieldset-->

			<aui:button-row style="display:inline-block;">
			    <div>
					<aui:button value="button-password-change" type="button"  onClick="validate()" />
				</div>
			</aui:button-row>
		</aui:form>
	</c:otherwise>
</c:choose>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>

<script type="text/javascript" src="/o/jpubsecureportalportlet/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="/o/jpubsecureportalportlet/js/jquery.fileDownload.js"></script>
<script type="text/javascript">

	function validate() {
		var password = $("#<portlet:namespace />pass1").val();
		var password2 = $("#<portlet:namespace />pass2").val();
		var size = encodeURI(password).split(/%..|./).length - 1;
		
		if(size > 40)
			{
			if(password2 == password){
				 $("#<portlet:namespace />pass2").val(password2+"@");
			}	
				
			 $("#<portlet:namespace />pass1").val(password+"@");
			 document.getElementById("fm").submit();
		}
		else{
			document.getElementById("fm").submit();
		}
	}
</script>

<script>
	define.amd = define._amd;
</script>