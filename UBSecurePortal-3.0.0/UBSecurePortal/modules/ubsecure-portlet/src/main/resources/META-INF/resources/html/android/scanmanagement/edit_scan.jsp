<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.model.User" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<portlet:defineObjects />

<style>
	.hideInput{
		display:none;
	}
</style>

<%
HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
httpSession.setAttribute("Current_screen", "Android");
Object downloadFrom = httpSession.getAttribute("download_from");
String strDownloadFrom = PortalConstants.STRING_EMPTY;
Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
int iUserRole = PortalConstants.INT_ZERO;

//Previous and current screen
PortletSession pSession = renderRequest.getPortletSession();
Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
String strCurrScreen = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oCurrScreen)) {
	strCurrScreen = oCurrScreen.toString();
	
	if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
		pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
	}
}

pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_REGISTRATION);
// End

if (!CommonUtil.isObjectNull(oUserRole)) {
	iUserRole = Integer.parseInt(oUserRole.toString());
}

Object oUseAndroid = httpSession.getAttribute(PortalConstants.USE_ANDROID);
boolean bUseAndroid = false;

if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
	bUseAndroid = true;
} else {
	if (!CommonUtil.isObjectNull(oUseAndroid)) {
		bUseAndroid = Boolean.parseBoolean(oUseAndroid.toString());
	}
}

Object oFieldNumber = renderRequest.getAttribute(PortalConstants.PARAM_FIELD_NUMBER);
int iFieldNumber = PortalConstants.INT_ZERO;

if (!CommonUtil.isObjectNull(oFieldNumber)) {
	iFieldNumber = Integer.parseInt(oFieldNumber.toString());
}

Object oAndroidServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG);
String strAndroidServerErrorMsg = null;

if (!CommonUtil.isObjectNull(oAndroidServerErrorMsg)) {
	strAndroidServerErrorMsg = oAndroidServerErrorMsg.toString();
}

Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);

String strFunctionName = PortalConstants.STRING_EMPTY;
String strHeaderName = PortalConstants.STRING_EMPTY;
long lProjectId = PortalConstants.LONG_ZERO;
String strProjectName = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(project)) {
	lProjectId = project.getProjectId();
	strProjectName = project.getProjectName();
}

Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
long lUserId = PortalConstants.LONG_ZERO;

if (!CommonUtil.isObjectNull(oUserId)) {
	lUserId = Long.parseLong(oUserId.toString());
}

User user = null;

if (lUserId != PortalConstants.LONG_ZERO) {
	try {
		user = ControllerHelper.getUser(lUserId);
	} catch (UBSPortalException e) {
		if (e.getErrorCode().equalsIgnoreCase(PortalErrors.ORM_EXCEPTION)) {
			if (SessionErrors.isEmpty(request) && SessionMessages.isEmpty(request)) {
				SessionErrors.add(request, PortalMessages.ORM_EXCEPTION);
			}
		}
	}
}

String emailAddress = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(user)) {
	emailAddress = user.getEmailAddress();
}

boolean bCanAccess = false;
bCanAccess = ControllerHelper.canAccessScanList(emailAddress, lProjectId);

if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
	bCanAccess = true;
}

Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
String strManualDownloadError = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oManualDownloadError)) {
	strManualDownloadError = oManualDownloadError.toString();
}

if (!CommonUtil.isObjectNull(downloadFrom)){
	strDownloadFrom = downloadFrom.toString();
}

if (bUseAndroid && bCanAccess) {
	%>
	<div
		style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px;">
		<liferay-ui:message key="header-scan-registration" />
	</div>
	
	<div style="height: 14px;">
		<hr
			style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
	</div>
	
	<div>
		<%
			if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("Android")) {
				%>
				<div class="alert alert-danger">
					<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
				</div>
			<%
				httpSession.removeAttribute(PortalConstants.ERROR);
			} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("Android")) {
				%>
					<div class="alert alert-danger">
						<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
					</div>
				<%
				httpSession.removeAttribute(PortalConstants.ERROR);
			}
		
		%>
		<liferay-ui:error key="<%= PortalMessages.USER_ID_INVALID %>" message="<%= PortalMessages.USER_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
		<liferay-ui:error key="<%= PortalMessages.USER_INVALID %>" message="<%= PortalMessages.USER_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PROJECT_INVALID %>" message="<%= PortalMessages.PROJECT_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.APK_FILE_NAME_TOO_LONG %>" message="<%= PortalMessages.APK_FILE_NAME_TOO_LONG %>" />
		<liferay-ui:error key="<%= PortalMessages.FILE_INVALID %>" message="<%= PortalMessages.FILE_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.APK_FILE_SIZE_TOO_BIG %>" message="<%= PortalMessages.APK_FILE_SIZE_TOO_BIG %>" />
		
		<liferay-ui:error key="<%= PortalMessages.ADD_SCAN_FAILED %>" message="<%= PortalMessages.ADD_SCAN_FAILED %>" />
		
		<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
		
		<liferay-ui:error key="<%= PortalErrors.ANDROID_SERVER_ERROR %>" message="<%= strAndroidServerErrorMsg %>" />
		<liferay-ui:error key="<%= PortalMessages.ANDO_REGISTER_SCAN_FAILED %>" message="<%= PortalMessages.ANDO_REGISTER_SCAN_FAILED %>" />
		<liferay-ui:error key="<%= PortalMessages.APK_FILE_NAME_CONTAINS_MULTIBYTE_CHARACTERS %>" message="<%= PortalMessages.APK_FILE_NAME_CONTAINS_MULTIBYTE_CHARACTERS %>"/>
	</div>
	
	<portlet:actionURL name="addScan" var="addScanURL">
		<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	</portlet:actionURL>
	
	<portlet:actionURL name="viewScanList" var="viewScanListURL">
		<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	</portlet:actionURL>
	
	<form action="<%= addScanURL.toString() %>" method="post" id="addScanForm" enctype="multipart/form-data" class="edit-scan-form">
		<input type="hidden" id="fieldNumber" value="<%= iFieldNumber %>" />
		<table>
			<tr >
				<td style="padding: 20px 20px 10px 0px">
					<liferay-ui:message key="label-project-colon" />
				</td>
				<td style="padding: 20px 0px 10px 0px"><%= HtmlUtil.escapeAttribute(strProjectName) %></td>
			</tr>
			
			<tr>
				<td><liferay-ui:message key="label-inspection-file" /><span style="color: red;">*</span></td>
				
				<td>
					<button type="button" name="browseBtn" id="browseBtn" class="btn" value="button-browse" id="btn-file" onclick="clickFile()"
					style="margin-top: 0;">
						<liferay-ui:message key="button-browse" />
					</button>&nbsp;&nbsp;
					<span id="strFile" name="strFile" value="" >
						<liferay-ui:message key="message-file-not-selected" />
					</span>
				</td>
				
				<td>
					<div class="hideInput">
						<aui:input type="file" id="inspectionFile" name="inspectionFile" accept="<%=PortalConstants.CONTENT_TYPE_APK%>" onchange="sendValue(this)" />
					</div>
				</td>
				
			</tr>
		</table>
	
		<aui:button-row>
			<button type="button" name="saveBtn" id="saveBtn" class="btn" value="button-scan" onClick="confirmAddScan()" style="width:120px; color:#404040; "><liferay-ui:message key="button-scan" /></button>&nbsp;&nbsp;
			<a id="cancelBtnHref" href="<%= viewScanListURL.toString() %>" ><span id="cancelBtn" class="btn" onClick="cancelAddScan()" style="width:94px; color:#404040;"><liferay-ui:message key="button-cancel" /></span></a>
		</aui:button-row>
		
		<span style="color: red;"><liferay-ui:message key="message-reminder-300MB" /></span>
		<br />
		<span style="color: red;"><liferay-ui:message key="message-reminder-scan-reservation" /></span>
	</form>
	
	<script>
		define._amd = define.amd;
		define.amd = false;
	</script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/js/android/scan_list.js"></script>
	<script>
		define.amd = define._amd;
	</script>
	<script type="text/javascript">
		function sendValue(){
			var path = $("#<portlet:namespace />inspectionFile").val();
			var fileName = path.split("\\");
			var file = fileName[fileName.length - 1];
	
			if (file == null || file == "") {
				$("#strFile").html("<liferay-ui:message key='message-file-not-selected' />");
				document.getElementById("addScanForm").action = "${addScanURL}&hasFileUpload=" + false;
			} else {
				$("#strFile").text(file);
				document.getElementById("addScanForm").action = "${addScanURL}&hasFileUpload=" + true;
			}
		}
		
		function clickFile(){
			$("#<portlet:namespace />inspectionFile").trigger('click');
		}
	</script>
	<%
} else {
	%>
	<div class="alert alert-danger">
		<liferay-ui:message key="message-no-access-rights" />
	</div>
	<%
}
%>