<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<%@ page import="com.liferay.portal.kernel.model.Organization" %>
<%@ page import="com.liferay.portal.kernel.model.User" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>

<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.List" %>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ProjectUsersItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<portlet:defineObjects />

<!-- Android -->

<%
HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
httpSession.setAttribute("Current_screen", "Android");
Object downloadFrom = httpSession.getAttribute("download_from");
String strDownloadFrom = PortalConstants.STRING_EMPTY;
Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
long lUserId = PortalConstants.LONG_ZERO;

if (!CommonUtil.isObjectNull(oUserId)) {
	lUserId = Long.parseLong(oUserId.toString());
}

Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
int iUserRole = 0;
boolean bAdmin = false;

if (!CommonUtil.isObjectNull(oUserRole)) {
	iUserRole = Integer.parseInt(oUserRole.toString());
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
		bAdmin = true;
	}
}

boolean bUseAndroid = false;

if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
	bUseAndroid = true;
} else {
	bUseAndroid = Boolean.parseBoolean(httpSession.getAttribute(PortalConstants.USE_ANDROID).toString());
}

//Previous and current screen
PortletSession pSession = renderRequest.getPortletSession();
Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
String strCurrScreen = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oCurrScreen)) {
	strCurrScreen = oCurrScreen.toString();
	
	if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
		pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
	}
}

pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_PROJECT_REGISTRATION);
// End

Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);
List<Organization> organizationList = (List<Organization>) renderRequest.getAttribute(PortalConstants.PARAM_ORGANIZATION_LIST);
List<ProjectUsersItem> projectUsersList = (List<ProjectUsersItem>) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT_USERS_LIST);
List<User> orgUsersList = (List<User>) renderRequest.getAttribute(PortalConstants.PARAM_ORG_USERS_LIST);
List<String> packageNamesList = (List<String>) renderRequest.getAttribute(PortalConstants.PARAM_PACKAGE_NAMES_LIST);

Object oFieldNumber = renderRequest.getAttribute(PortalConstants.PARAM_FIELD_NUMBER);
int iFieldNumber = 0;

if (oFieldNumber != null) {
	iFieldNumber = Integer.parseInt(oFieldNumber.toString());
}

Object oAndroidServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG);
String strAndroidServerErrorMsg = null;

if (oAndroidServerErrorMsg != null) {
	strAndroidServerErrorMsg = oAndroidServerErrorMsg.toString();
}

Object oUserListSortOrder = renderRequest.getAttribute(PortalConstants.PARAM_USER_LIST_SORT_ORDER);
String strUserListSortOrder = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oUserListSortOrder)) {
	strUserListSortOrder = oUserListSortOrder.toString();
}

Object oAvailableUsersSortOrder = renderRequest.getAttribute(PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
String strAvailableUsersSortOrder = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oAvailableUsersSortOrder)) {
	strAvailableUsersSortOrder = oAvailableUsersSortOrder.toString();
}

String strCaseNumber = null;
String strProjectName = null;
long lOwnerGroup = PortalConstants.LONG_ZERO;
int iAttribute = PortalConstants.INT_ZERO;
String strChecklistFileName = null;
String strAddUpdateFunction = null;
long lProjectId = PortalConstants.LONG_ZERO;
String selectedUsers = PortalConstants.STRING_EMPTY;
String strAddUpdateButton = PortalConstants.STRING_EMPTY;
String strPackageNames = PortalConstants.STRING_EMPTY;
Calendar dteProjectEndDate = null;
String strHeader =  PortalConstants.STRING_EMPTY;


if (project != null) {
	strCaseNumber = project.getCaseNumber();
	strProjectName = project.getProjectName();
	lOwnerGroup = project.getOwnerGroup();
	iAttribute = project.getAttribute();
	
	if (project.getProjectEndDate() != null) {
		dteProjectEndDate = Calendar.getInstance();
		dteProjectEndDate.setTime(project.getProjectEndDate());
	}
	
	strChecklistFileName = project.getChecklistFileName();
	lProjectId = project.getProjectId();
	if (lProjectId != 0) {
		strAddUpdateFunction = "updateProject";
		strAddUpdateButton = "button-change";
		strHeader = "header-project-change";
	} else {
		strAddUpdateFunction = "addProject";
		strAddUpdateButton = "button-register";
		strHeader = "header-project-registration";
	}
	
} else {
	lProjectId = 0L;
	strAddUpdateFunction = "addProject";
	strAddUpdateButton = "button-register";
	strHeader = "header-project-registration";
}

if (packageNamesList != null) {
	for (int i = 0; i < packageNamesList.size(); i++) {
		if (i == packageNamesList.size() - 1) {
			strPackageNames += packageNamesList.get(i);
		} else {
			strPackageNames += packageNamesList.get(i) + "\n";
		}
	}
}

Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
String strManualDownloadError = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oManualDownloadError)) {
	strManualDownloadError = oManualDownloadError.toString();
}

if (!CommonUtil.isObjectNull(downloadFrom)){
	strDownloadFrom = downloadFrom.toString();
}

if (bUseAndroid && (bAdmin || iUserRole == UserRole.GEN_USER.getInteger())) {
%>
	<portlet:actionURL name="viewProjectList" var="cancelURL">
		<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
		<portlet:param name="type" value="<%=String.valueOf(ProjectType.ANDROID.getInteger())%>" />
	</portlet:actionURL>
	
	<portlet:actionURL name="<%= strAddUpdateFunction %>" var="updateProjectURL">
		<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
		<portlet:param name="type" value="<%=String.valueOf(ProjectType.ANDROID.getInteger())%>" />
	</portlet:actionURL>
	<%
		if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("Android")) {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
			</div>
		<%
			httpSession.removeAttribute(PortalConstants.ERROR);
		} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("Android")) {
			%>
				<div class="alert alert-danger">
					<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
				</div>
			<%
			httpSession.removeAttribute(PortalConstants.ERROR);
		}
	%>
	<div style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px;">
		<liferay-ui:message key="<%= strHeader %>" />
	</div>
	
	<div style="height: 14px;">
		<hr style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
	</div>

	<div id="userIdError" class="alert alert-danger hideError">
		<liferay-ui:message key="error-search-user-id-too-long" />
	</div>
	
	<div id="userNameError" class="alert alert-danger hideError">
		<liferay-ui:message key="error-search-username-too-long" />
	</div>
	
	<div id="userDoesNotExistError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	</div>
	
	<div id="userDoesNotBelongToGroupError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP %>" />
	</div>
	
	<div id="dbConnError" class="alert alert-danger hideError">
		<liferay-ui:message key="<%= PortalMessages.ORM_EXCEPTION %>" />
	</div>
	
	<portlet:resourceURL var="getUsersURL">
		<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_SELECT_GROUP) %>" />
	</portlet:resourceURL>
	
	<div>
		<liferay-ui:error key="<%= PortalMessages.USER_ID_INVALID %>" message="<%= PortalMessages.USER_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
		<liferay-ui:error key="<%= PortalMessages.USER_INVALID %>" message="<%= PortalMessages.USER_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.CASE_NUMBER_INVALID %>" message="<%= PortalMessages.CASE_NUMBER_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.CASE_NUMBER_ALREADY_EXIST %>" message="<%= PortalMessages.CASE_NUMBER_ALREADY_EXIST %>" />
		<liferay-ui:error key="<%= PortalMessages.CASE_NUMBER_TOO_LONG %>" message="<%= PortalMessages.CASE_NUMBER_TOO_LONG %>" />
		
		<liferay-ui:error key="<%= PortalMessages.NO_PROJECT_NAME %>" message="<%= PortalMessages.NO_PROJECT_NAME %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_NAME_TOO_LONG %>" message="<%= PortalMessages.PROJECT_NAME_TOO_LONG %>" />
		
		<liferay-ui:error key="<%= PortalMessages.NO_OWNER_GROUP %>" message="<%= PortalMessages.NO_OWNER_GROUP %>" />
		<liferay-ui:error key="<%= PortalMessages.OWNER_GROUP_DOES_NOT_EXIST %>" message="<%= PortalMessages.OWNER_GROUP_DOES_NOT_EXIST %>" />
		
		<liferay-ui:error key="<%= PortalMessages.NO_PROJECT_END_DATE %>" message="<%= PortalMessages.NO_PROJECT_END_DATE %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_END_DATE_INVALID %>" message="<%= PortalMessages.PROJECT_END_DATE_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.CHECKLIST_FILE_SIZE_TOO_BIG %>" message="<%= PortalMessages.CHECKLIST_FILE_SIZE_TOO_BIG %>" />
		<liferay-ui:error key="<%= PortalMessages.FILE_INVALID %>" message="<%= PortalMessages.FILE_INVALID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.NO_PROJECT_USERS %>" message="<%= PortalMessages.NO_PROJECT_USERS %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP %>" message="<%= PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.PROJECT_USER_DOES_NOT_EXIST %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PACKAGE_NAME_INVALID %>" message="<%= PortalMessages.PACKAGE_NAME_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PACKAGE_NAME_TOO_LONG %>" message="<%= PortalMessages.PACKAGE_NAME_TOO_LONG %>" />
		
		<liferay-ui:error key="<%= PortalMessages.PROJECT_ID_INVALID %>" message="<%= PortalMessages.PROJECT_ID_INVALID %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" message="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" />
		<liferay-ui:error key="<%= PortalMessages.PROJECT_TYPE_NOT_ANDROID %>" message="<%= PortalMessages.PROJECT_TYPE_NOT_ANDROID %>" />
		
		<liferay-ui:error key="<%= PortalMessages.ADD_PROJECT_FAILED %>" message="<%= PortalMessages.ADD_PROJECT_FAILED %>" />
		<liferay-ui:error key="<%= PortalMessages.UPDATE_PROJECT_FAILED %>" message="<%= PortalMessages.UPDATE_PROJECT_FAILED %>" />
		
		<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
		<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
		
		<liferay-ui:error key="<%= PortalErrors.ANDROID_SERVER_ERROR %>" message="<%= strAndroidServerErrorMsg %>" />
		<liferay-ui:error key="<%= PortalMessages.ANDO_CREATE_PROJECT_FAILED %>" message="<%= PortalMessages.ANDO_CREATE_PROJECT_FAILED %>" />
		<liferay-ui:error key="<%= PortalMessages.ANDO_UPDATE_PROJECT_FAILED %>" message="<%= PortalMessages.ANDO_UPDATE_PROJECT_FAILED %>" />
		<liferay-ui:error key="<%= PortalMessages.ANDO_GET_PACKAGE_NAMES_FAILED %>" message="<%= PortalMessages.ANDO_GET_PACKAGE_NAMES_FAILED %>" />
	</div>    
	
	<%
	String portletNamespace = renderResponse.getNamespace();
	%>
	<div style="width: 100%; position: relative;">
		<form action="<%=updateProjectURL.toString()%>" method="post" id="fm" enctype="multipart/form-data" class="edit-project-form">
			<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
			<input type="hidden" id="fieldNumber" value="<%= iFieldNumber %>" />
			<input type="hidden" id="projectId" name="projectId" value="<%= lProjectId %>" />
			<input type="hidden" id="loggedInUser" name="loggedInUser" value="<%= lUserId %>" />
			<input type="hidden" id="screenName" name="screenName" value="edit_project" />
			<input type="hidden" id="url" name="url" value="<%=getUsersURL.toString()%>" />
			<aui:input type="hidden" name="type" value="<%=ProjectType.ANDROID.getInteger()%>" />
	
			<table>
				<%
				if (bAdmin) {
				%>
				<tr class="input-row">
					<td><liferay-ui:message key="label-case-number" /></td>
					<td><aui:input type="text" name="caseNumber" id="caseNumber" label="" value="<%= strCaseNumber %>" /></td>
					<td width="100px"></td>
					<td></td>
				</tr>
	
				<tr class="input-row">
					<td><liferay-ui:message key="label-project-name" /><span style="color: red;">*</span></td>
					<td><aui:input type="text" name="projectName" id="projectName" label="" value="<%= strProjectName %>" /></td>
					<td></td>
					<td></td>
				</tr>
	
				<tr class="input-row">
					<td><liferay-ui:message key="label-belonging-group" /><span style="color: red;">*</span></td>
					<td><select id="ownerGroup" name="ownerGroup">
							<option value=""></option>
							<%
								String strSelected = PortalConstants.STRING_EMPTY;
							
								if (organizationList != null && !organizationList.isEmpty()) {
									for (Organization o : organizationList) {
										if (o.getOrganizationId() == lOwnerGroup) {
											strSelected = "selected";
										} else {
											strSelected = PortalConstants.STRING_EMPTY;
										}
										%>
											<option value="<%= o.getOrganizationId() %>" <%= strSelected %> style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"><%= HtmlUtil.escapeAttribute(o.getName()) %></option>
										<%
									}
								}
							%>
					</select></td>
					<td></td>
					<td></td>
				</tr>
				
				<tr class="input-row">
					<td width="180px"><liferay-ui:message key="label-project-end-date" /><span style="color: red;">*</span></td>
					<td>
						<%
						int year = 0;
						int month = -1;
						int date = 0;
						
						if (dteProjectEndDate != null) {
							year = dteProjectEndDate.get(Calendar.YEAR);
							month = dteProjectEndDate.get(Calendar.MONTH);
							date = dteProjectEndDate.get(Calendar.DAY_OF_MONTH);
						}
						%>
						<div class="form-group date-input-text">
							<liferay-ui:input-date name="projectEndDate" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= date %>" />
						</div>
					</td>
					<td></td>
					<td></td>
				</tr>
	
				<%
				}
				%>
				<tr class="input-row">
					<td width="180px"><liferay-ui:message key="label-exclusion-library-setting" /></td>
					<td><aui:input type="textarea" name="librarySetting" id="librarySetting" label="" value="<%= strPackageNames %>" /></td>
					<td></td>
					<td></td>
				</tr>
			
				<tr class="input-row">
					<td><liferay-ui:message key="label-exclusion-rule-file" /></td>
					<td>
						<aui:button type="button" name="browseBtn" id="browseBtn" value="button-browse" onclick="clickFile()"/>&nbsp;&nbsp;
						<%
							if (!CommonUtil.isStringNullOrEmpty(strChecklistFileName)) {
								%>
								<aui:input type="hidden" id="fileName" name="fileName" value="<%= strChecklistFileName %>" />
								<span id="strFile" ><%= strChecklistFileName %></span>
								<%
							} else {
								%>
								<span id="strFile" ><liferay-ui:message key="message-file-not-selected" /> </span>
								<%
							}
						%>
						
						
					
					</td>
					<td>
						<div class="hideInput">
							<aui:input type="file" id="exclusionRuleFile" name="exclusionRuleFile"
								 accept="<%=PortalConstants.CONTENT_TYPE_XLS%>" onchange="sendValue(this)" />
							<input type="hidden" id="newFile" name="newFile" value="<%= false %>" />
						 </div>
					</td>
					<td></td>
				</tr>
				
				<%
				if (bAdmin) {
				%>
				<tr class="input-row">
					<td></td>
					<td>
						<div style="float: left;">
							<liferay-ui:message key="label-user-list" />
						</div>
						
						<div style="float: right; margin-right: 4px; z-index: 1; padding-top: 5px;">
							<div>
								<a href="javascript:sortUserList('asc');" class="sort sort-up-link"></a>
								<i class="icon-sort-up sort-icon sort-up-icon" id="userListSortUp"></i>
							</div>
							<div style="margin-top: -1px;">
								<a href="javascript:sortUserList('desc');" class="sort sort-down-link"></a>
								<i class="icon-sort-down sort-icon sort-down-icon" id="userListSortDown"></i>
							</div>
						</div>
					</td>
					<td style="padding-left: 30px;"><liferay-ui:message key="label-user-id" /></td>
					<td><input type="text" id="userId" name="userId" /></td>
				</tr>
				
				<tr class="input-row">
					<td rowspan=5 width="180px"><liferay-ui:message key="label-used-users" /><span style="color: red;">*</span></td>
					<td rowspan=5 width="303px" valign="top">
						<div class="user-selection selected-users">
							<ol class="menuOptions selectable" id="userList" name="userList">
								<%
									int userListCount = 0;
								
									if (projectUsersList != null && !projectUsersList.isEmpty()) {
										for (ProjectUsersItem projectUsers : projectUsersList) {
											userListCount++;
											selectedUsers += projectUsers.getUserId() + ",";
											
											%>
											<li id="<%= projectUsers.getUserId() %>" value="<%= projectUsers.getUserId() %>"><%= projectUsers.getUserName() + " : " + projectUsers.getEmailAddress() %></li>
											<%
										}
									}
								%>
							</ol>
						</div>
					</td>
					<td style="padding-left: 30px;"><liferay-ui:message key="label-user-name" /></td>
					<td><input type="text" id="userName" name="userName" /></td>
				</tr>
				
				<tr>
					<td></td>
					<td align="right" valign="top" style="padding-bottom: 25px;">
						<button class="btn user-filter-btn" id="filterBtn" name="filterBtn" onclick="filterUsers()" type="button">
							<liferay-ui:message key="button-filter" />
						</button>
					</td>
				</tr>
	
				<tr>
					<td></td>
					<td>
						<div style="float: left;">
							<liferay-ui:message key="label-available-users" />
						</div>
						
						<div style="float: right; margin-right: 4px; z-index: 1; padding-top: 5px;">
							<div>
								<a href="javascript:sortAvailableUsers('asc');" class="sort sort-up-link"></a>
								<i class="icon-sort-up sort-icon sort-up-icon" id="availableUsersSortUp"></i>
							</div>
							<div style="margin-top: -1px;">
								<a href="javascript:sortAvailableUsers('desc');" class="sort sort-down-link"></a>
								<i class="icon-sort-down sort-icon sort-down-icon" id="availableUsersSortDown"></i>
							</div>
						</div>
					</td>
				</tr>
				
				<tr class="input-row">
					<td width="100px" align="left" valign="bottom" style="padding-bottom: 25px; padding: 0 30px;">
						<aui:button name="addBtn" value="button-add-arrow" onclick="addUser()" cssClass="user-btn"/></td>
					<td rowspan=2>
						<div class="user-selection available-users">
							<ol class="menuOptions selectable" id="availableUsers" name="availableUsers">
								<%
									int orgUsersCount = 0;
								
									if (orgUsersList != null && !orgUsersList.isEmpty()) {
										for (User user : orgUsersList) {
											orgUsersCount++;
											%>
											<li id="<%= user.getUserId() %>" value="<%= user.getUserId() %>"><%= user.getFirstName() + " : " + user.getEmailAddress() %></li>
											<%
										}
									}
								%>
							</ol>
						</div>
					</td>
				</tr>
	
				<tr class="input-row">
					<td align="left" valign="top" style="padding-left: 30px;"><aui:button name="removeBtn" value="button-remove-arrow" onclick="removeUser()" cssClass="user-btn" /></td>
				</tr>
				<%
				}
				%>
			</table>
	
			<input type="hidden" id="selectedUsers" name="selectedUsers" value="<%= selectedUsers %>" />
			<input type="hidden" id="<%= PortalConstants.PARAM_USER_LIST_SORT_ORDER %>" name="<%= PortalConstants.PARAM_USER_LIST_SORT_ORDER %>" value="<%= strUserListSortOrder %>" />
			<input type="hidden" id="<%= PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER %>" name="<%= PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER %>" value="<%= strAvailableUsersSortOrder %>" />
			<div style="padding-top: 10px;">
				<button type="button" name="saveBtn" id="saveBtn" class="btn" value="<%= strAddUpdateButton %>" onclick="submitProjectForm()" ><liferay-ui:message key="<%= strAddUpdateButton %>" /></button>&nbsp;&nbsp;
				<a id="cancelBtnHref" href="<%= cancelURL.toString() %>"><span id="cancelBtn" class="btn cancel-btn" onClick="cancelAddUpdateProject()"><liferay-ui:message key="button-cancel" /></span></a>
			</div>
		</form>
	</div>
	<%
} else {
	%>
	<div class="alert alert-danger">
		<liferay-ui:message key="message-no-access-rights" />
	</div>
	<%
}
%>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/android/edit_project.js"></script>
<script>
	define.amd = define._amd;
</script>
<script type="text/javascript">
	function sendValue(){
		var path = $("#<portlet:namespace />exclusionRuleFile").val();
		var fileName = path.split("\\");
		var file = fileName[fileName.length - 1];

		if (file == null || file == "") {
			$("#strFile").html("<liferay-ui:message key='message-file-not-selected' />");
		} else {
			$("#strFile").text(file);
		}
	}
	
	function clickFile() {
		$("#newFile").val("true");
		$("#<portlet:namespace />exclusionRuleFile").trigger('click');
	}
</script>