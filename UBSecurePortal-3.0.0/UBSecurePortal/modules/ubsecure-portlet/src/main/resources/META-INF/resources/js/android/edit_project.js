/**
 * Android
 */

$(document).ready(function() {
	var screenName = $("#screenName").val();
	
	if (screenName == "edit_project") {
		var portletNamespace = $("#portletNamespace").val();
		var fieldNumber = $("#fieldNumber").val();
		var userListSortOrder = $("#userListSortOrder").val();
		var availableUsersSortOrder = $("#availableUsersSortOrder").val();
		
		if (fieldNumber == 0 || fieldNumber == 1) {
			var caseNumber = $("#" + portletNamespace + "caseNumber");
			caseNumber.focus();
		} else if (fieldNumber == 2) {
			var projectName = $("#" + portletNamespace + "projectName");
			projectName.focus();
		} else if (fieldNumber == 3) {
			var ownerGroup = $("#ownerGroup").first();
			ownerGroup.focus();
		} else if (fieldNumber == 4) {
			var projectEndDate = $("#" + portletNamespace + "projectEndDate");
			projectEndDate.focus();
		} else if (fieldNumber == 6) {
			var packageName = $("#" + portletNamespace + "librarySetting");
			packageName.focus();
		} else if (fieldNumber == 7) {
			var checklist = $("#" + portletNamespace + "browseBtn");
			checklist.focus();
		} else if (fieldNumber == 10) {
			var projectUsers = $("#userList");
			projectUsers.focus();
		}

		var selectedUsers = $("#selectedUsers").val();
		
		if (selectedUsers != null && selectedUsers != "") {
			users = selectedUsers.split(",");
		}

		if (userListSortOrder == "asc") {
			document.getElementById("userListSortUp").style.color="#555";
			document.getElementById("userListSortDown").style.color="#AAA";
		} else if (userListSortOrder == "desc") {
			document.getElementById("userListSortUp").style.color="#AAA";
			document.getElementById("userListSortDown").style.color="#555";
		}
		
		if (availableUsersSortOrder == "asc") {
			document.getElementById("availableUsersSortUp").style.color="#555";
			document.getElementById("availableUsersSortDown").style.color="#AAA";
		} else if (availableUsersSortOrder == "desc") {
			document.getElementById("availableUsersSortUp").style.color="#AAA";
			document.getElementById("availableUsersSortDown").style.color="#555";
		}
		
		function getFile(){
			document.getElementById("exclusionRuleFile").click();
		}
	}
	
	$(".selectable").selectable();
	
	$("#ownerGroup").change(function() {
		var selectedGroup = $(this).val();
		var url = $("#url").val() + "?r=" + generateID();
		var availableUsersSortOrder = $("#availableUsersSortOrder").val().trim();
		
		$("#userList").empty();
		$("#selectedUsers").val("");
		
		$("#userIdError").addClass("hideError");
		$("#userNameError").addClass("hideError");
		$("#dbConnError").addClass("hideError");
		$("#userDoesNotExistError").addClass("hideError");
		$("#userDoesNotBelongToGroupError").addClass("hideError");
		
		$("#userId").val("");
		$("#userName").val("");
		
		$.ajax({
			url: url,
			data: {selectedGroup : selectedGroup,
				userAction : 22,
				availableUsersSortOrder : availableUsersSortOrder},
			dataType: "JSON",
			success: function(data) {
				var errorMsg = data[0].errorMsg;
				var availableUsers = data[1].orgUsers;
				var options = "";
				
				var length = availableUsers.length;
				for (var i = 0; i < length; i++) {
					options += "<li id='" + availableUsers[i].userId + "' value='" + availableUsers[i].userId +"'>" + availableUsers[i].screenName + " : " + availableUsers[i].emailAddress + "</li>";
				}

				if (errorMsg != null) {
					if (errorMsg.length > 0 && errorMsg[0].DBConnError == true) {
						$("#dbConnError").removeClass("hideError");
					}
				}
				
				$("#availableUsers").html(options);
			}
		});
	});
});

function filterUsers () {
	var userId = $("#userId").val().trim();
	var userName = $("#userName").val().trim();
	var selectedUsers = $("#selectedUsers").val();
	var userListSortOrder = $("#userListSortOrder").val().trim();
	var availableUsersSortOrder = $("#availableUsersSortOrder").val().trim();
	
	$("#userId").val(userId);
	$("#userName").val(userName);
	
	var url = $("#url").val();
	var selectedGroup = $("#ownerGroup").val();
	
	$("#userIdError").addClass("hideError");
	$("#userNameError").addClass("hideError");
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	userName = encodeURI(userName);
	
	$.ajax({
		url: url,
		data: {selectedGroup: selectedGroup,
			userId : userId,
			userName : userName,
			userAction : 26,
			selectedUsers : selectedUsers,
			userListSortOrder : userListSortOrder,
			availableUsersSortOrder : availableUsersSortOrder},
		dataType: "JSON",
		success: function(data) {
			var errorMsg = data[0].errorMsg;
			var availableUsers = data[1].availableUsers;
			
			var availableOptions = "";
			var length = availableUsers.length;
			for (var i = 0; i < length; i++) {
				availableOptions += "<li id='" + availableUsers[i].userId + "' value='" + availableUsers[i].userId +"'>" + availableUsers[i].screenName + " : " + availableUsers[i].emailAddress + "</li>";
			}
			
			length = errorMsg.length;
			if (length > 0 && errorMsg[0].userIdTooLong == true) {
				$(".alert").addClass("hideError");
				$("#userIdError").removeClass("hideError");
			} else if (length > 1 && errorMsg[1].userNameTooLong == true) {
				$(".alert").addClass("hideError");
				$("#userNameError").removeClass("hideError");
			} else if (length > 2 && errorMsg[2].DBConnError == true) {
				$(".alert").addClass("hideError");
				$("#dbConnError").removeClass("hideError");
			} 
			
			$("#availableUsers").html(availableOptions);
		}
	});
}

function addUser () {
	var selectedUser = document.getElementsByClassName("ui-selected");
	var length = selectedUser.length;
	var userListOptions = "";
	var availableUsersOptions = "";
	var selectedUsers = $("#selectedUsers").val();
	var url = $("#url").val();
	var selectedGroup = $("#ownerGroup").val();
	var userId = $("#userId").val().trim();
	var userName = $("#userName").val().trim();
	var userListSortOrder = $("#userListSortOrder").val().trim();
	var availableUsersSortOrder = $("#availableUsersSortOrder").val().trim();
	
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	$("#userId").val(userId);
	$("#userName").val(userName);
	
	userName = encodeURI(userName);
	
	for (var i = 0; i < length; i++) {
		if (selectedUser[i].parentNode.id == "availableUsers") {
			selectedUsers += selectedUser[i].id + ",";
		}
	}
	
	$.ajax({
		url: url,
		data: {selectedUsers: selectedUsers,
			selectedGroup: selectedGroup,
			userAction: 27,
			userId : userId,
			userName : userName,
			userListSortOrder : userListSortOrder,
			availableUsersSortOrder : availableUsersSortOrder},
		dataType: "JSON",
		async: false,
		success: function(data) {
			var errorMsg = data[0].errorMsg;
			var userList = data[1].usersList;
			var availableUsers = data[2].availableUsers;
			
			selectedUsers = "";
			length = userList.length;
			for (var i = 0; i < length; i++) {
				userListOptions += "<li id='" + userList[i].userId + "' value='" + userList[i].userId +"'>" + userList[i].screenName + " : " + userList[i].emailAddress + "</li>";
				selectedUsers += userList[i].userId + ",";
			}
			
			length = availableUsers.length;
			for (var i = 0; i < length; i++) {
				availableUsersOptions += "<li id='" + availableUsers[i].userId + "' value='" + availableUsers[i].userId +"'>" + availableUsers[i].screenName + " : " + availableUsers[i].emailAddress + "</li>";
			}
			
			if (errorMsg != null) {
				if (errorMsg.length > 0) {
					if (errorMsg[0].DBConnError == true) {
						$("#dbConnError").removeClass("hideError");
					} else if (errorMsg[0].userDoesNotExist == true) {
						$("#userDoesNotExistError").removeClass("hideError");
					} else if (errorMsg[0].userDoesNotBelongToGroup == true) {
						$("#userDoesNotBelongToGroupError").removeClass("hideError");
					}
				}
			}
		}
	});
	
	$("#userList").html(userListOptions);
	$("#availableUsers").html(availableUsersOptions);
	$("#selectedUsers").val(selectedUsers);
}

function removeUser () {
	var toRemoveUsers = document.getElementsByClassName("ui-selected");
	var length = toRemoveUsers.length;
	var usersToRemove = "";
	var userListOptions = "";
	var availableUsersOptions = "";
	var selectedUsers = $("#selectedUsers").val();
	var url = $("#url").val();
	var selectedGroup = $("#ownerGroup").val();
	var userId = $("#userId").val().trim();
	var userName = $("#userName").val().trim();
	var userListSortOrder = $("#userListSortOrder").val().trim();
	var availableUsersSortOrder = $("#availableUsersSortOrder").val().trim();
	
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	$("#userId").val(userId);
	$("#userName").val(userName);
	
	userName = encodeURI(userName);
	
	for (var i = 0; i < length; i++) {
		if (toRemoveUsers[i].parentNode.id == "userList") {
			usersToRemove += toRemoveUsers[i].id + ",";
		}
	}
	
	$.ajax({
		url: url,
		data: {selectedUsers: selectedUsers,
			usersToRemove: usersToRemove,
			selectedGroup: selectedGroup,
			userAction: 28,
			userId : userId,
			userName : userName,
			userListSortOrder : userListSortOrder,
			availableUsersSortOrder : availableUsersSortOrder},
		dataType: "JSON",
		async: false,
		success: function(data) {
			var errorMsg = data[0].errorMsg;
			var userList = data[1].usersList;
			var availableUsers = data[2].availableUsers;
			
			selectedUsers = "";
			length = userList.length;
			for (var i = 0; i < length; i++) {
				userListOptions += "<li id='" + userList[i].userId + "' value='" + userList[i].userId +"'>" + userList[i].screenName + " : " + userList[i].emailAddress + "</li>";
				selectedUsers += userList[i].userId + ",";
			}
			
			length = availableUsers.length;
			for (var i = 0; i < length; i++) {
				availableUsersOptions += "<li id='" + availableUsers[i].userId + "' value='" + availableUsers[i].userId +"'>" + availableUsers[i].screenName + " : " + availableUsers[i].emailAddress + "</li>";
			}
			
			if (errorMsg != null) {
				if (errorMsg.length > 0) {
					if (errorMsg[0].DBConnError == true) {
						$("#dbConnError").removeClass("hideError");
					} else if (errorMsg[0].userDoesNotExist == true) {
						$("#userDoesNotExistError").removeClass("hideError");
					} else if (errorMsg[0].userDoesNotBelongToGroup == true) {
						$("#userDoesNotBelongToGroupError").removeClass("hideError");
					}
				}
			}
		}
	});
	
	$("#userList").html(userListOptions);
	$("#availableUsers").html(availableUsersOptions);
	$("#selectedUsers").val(selectedUsers);
}

function submitProjectForm () {
	var portletNamespace = $("#portletNamespace").val();
	$("#" + portletNamespace + "browseBtn").prop("disabled", true);
	$("#filterBtn").prop("disabled", true);
	$("#" + portletNamespace + "addBtn").prop("disabled", true);
	$("#" + portletNamespace + "removeBtn").prop("disabled", true);
	$("#saveBtn").prop("disabled", true);
	$("#cancelBtn").addClass("disabled");
	$("#cancelBtnHref").removeAttr("href");
	$("#saveBtn").removeAttr("onclick");
	$("#filterBtn").removeAttr("onclick");
	$("#" + portletNamespace + "addBtn").removeAttr("onclick");
	$("#" + portletNamespace + "removeBtn").removeAttr("onclick");
	$("#cancelBtn").removeAttr("onclick");
	
	document.getElementById("fm").submit();
}

function cancelAddUpdateProject () {
	var portletNamespace = $("#portletNamespace").val();
	$("#" + portletNamespace + "browseBtn").prop("disabled", true);
	$("#filterBtn").prop("disabled", true);
	$("#" + portletNamespace + "addBtn").prop("disabled", true);
	$("#" + portletNamespace + "removeBtn").prop("disabled", true);
	$("#saveBtn").prop("disabled", true);
	$("#cancelBtn").addClass("disabled");
	$("#saveBtn").removeAttr("onclick");
	$("#filterBtn").removeAttr("onclick");
	$("#" + portletNamespace + "addBtn").removeAttr("onclick");
	$("#" + portletNamespace + "removeBtn").removeAttr("onclick");
}

function byteCount(string) {
    return encodeURI(string).split(/%..|./).length - 1;
}

function sortUserList (order) {
	var my_options = $("#userList li");
	var my_new_options = checkSelectedUsers(my_options);
	
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	my_new_options = sort(my_new_options, order);
	
	var selectedUsers = "";
	
	var myNewOptionsLength = my_new_options.length;
	for (var i = 0; i < myNewOptionsLength; i++) {
		if ((my_new_options[i] != null && my_new_options[i] != "undefined")) {
			selectedUsers += my_new_options[i].id + ",";
		}
	}
	
	$("#userList").append(my_new_options);
	$("#selectedUsers").val(selectedUsers);
	$("#userListSortOrder").val(order);
	
	if (order == "asc") {
		document.getElementById("userListSortUp").style.color="#555";
		document.getElementById("userListSortDown").style.color="#AAA";
	} else if (order == "desc") {
		document.getElementById("userListSortUp").style.color="#AAA";
		document.getElementById("userListSortDown").style.color="#555";
	}
}

function sortAvailableUsers (order) {
	var my_options = $("#availableUsers li");
	var my_new_options = checkAvailableUsers(my_options);
	
	$("#dbConnError").addClass("hideError");
	$("#userDoesNotExistError").addClass("hideError");
	$("#userDoesNotBelongToGroupError").addClass("hideError");
	
	my_new_options = sort(my_new_options, order);
	
	$("#availableUsers").append(my_new_options);
	$("#availableUsersSortOrder").val(order);
	
	if (order == "asc") {
		document.getElementById("availableUsersSortUp").style.color="#555";
		document.getElementById("availableUsersSortDown").style.color="#AAA";
	} else if (order == "desc") {
		document.getElementById("availableUsersSortUp").style.color="#AAA";
		document.getElementById("availableUsersSortDown").style.color="#555";
	}
}

function checkSelectedUsers (my_options) {
	var my_new_options = [];
	var selectedUsers = $("#selectedUsers").val();
	var userList = [];
	
	if (selectedUsers != null) {
		userList = selectedUsers.split(",");
	}
	
	var myOptionsLength = my_options.length;
	var userListLength = userList.length;
	for (var i = 0; i < myOptionsLength; i++) {
		for (var j = 0; j < userListLength; j++) {
			if ((my_options[i] != null && my_options[i] != "undefined") && my_options[i].id == userList[j]) {
				my_new_options[i] = my_options[i];
				break;
			}
		}
	}
	
	my_new_options = finalizeOptions(my_new_options);
	
	return my_new_options;
}

function checkAvailableUsers (my_options) {
	var my_new_options = [];
	var selectedUsers = $("#selectedUsers").val();
	var userList = [];
	
	if (selectedUsers != null) {
		userList = selectedUsers.split(",");
	}
	
	var myOptionsLength = my_options.length;
	var userListLength = userList.length;
	
	for (var i = 0; i < myOptionsLength; i++) {
		var isSelected = false;
		
		for (var j = 0; j < userListLength; j++) {
			if ((my_options[i] != null && my_options[i] != "undefined") && my_options[i].id == userList[j]) {
				isSelected = true;
				break;
			}
		}
		
		
		if (isSelected == false && (my_options[i] != null && my_options[i] != "undefined")) {
			my_new_options[my_new_options.length] = my_options[i];
		}
	}
	
	my_new_options = finalizeOptions(my_new_options);
	
	return my_new_options;
}

function finalizeOptions (my_options) {
	var my_new_options = [];
	
	var myOptionsLength = my_options.length;
	
	for (var i = 0; i < myOptionsLength; i++) {
		var isExisting = false;
		
		var myNewOptionsLength = my_new_options.length;
		for (var j = 0; j < myNewOptionsLength; j++) {
			if ((my_options[i] != null && my_options[i] != "undefined") && my_new_options[j].id == my_options[i].id) {
				isExisting = true;
				break;
			}
		}
		
		if (isExisting == false && (my_options[i] != null && my_options[i] != "undefined")) {
			my_new_options[myNewOptionsLength] = my_options[i];
		}
	}
	
	return my_new_options;
}

function sort (options, order) {
	if (order == "asc") {
		options.sort(function (a, b) {
			if ((a.textContent).toLowerCase() > (b.textContent).toLowerCase()) return 1;
			else if ((a.textContent).toLowerCase() < (b.textContent).toLowerCase()) return -1;
			else return 0;
		});
	} else if (order == "desc") {
		options.sort(function (a, b) {
			if ((a.textContent).toLowerCase() < (b.textContent).toLowerCase()) return 1;
			else if ((a.textContent).toLowerCase() > (b.textContent).toLowerCase()) return -1;
			else return 0;
		});
	}
	
	return options;
}

function generateID () {
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	var id = "";
	
	for (var count = 0; count < 15; count++) {
		id += possible.charAt(getRandomInt(1, 62));
	}
	
	return id;
}

function getRandomInt (min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * ((max - min) + 1)) + min;
}