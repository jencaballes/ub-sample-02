/**
 * Vex
 * 
 * Please note that this javascript file is used by 
 * register_scan_simple_setting.jsp 
 * and 
 * register_scan_advanced_setting.jsp.
 * 
 */

$(document).ready(function() {
	var portletNamespace = $("#portletNamespace").val();
	$("#" + portletNamespace + "startTime").css("margin-bottom","20px");
	$("#" + portletNamespace + "startTime").css("width","120px");
	$("#" + portletNamespace + "endTime").css("width","120px");
	
	if ($("#scanId").val() != "0") {
		if ($("#scanSettingStartTime").val().length == 0) {
			$("#" + portletNamespace + "startTime").val("");
		}
		
		if ($("#scanSettingEndTime").val().length == 0) {
			$("#" + portletNamespace + "endTime").val("");
		}
	} else {
		if ($("#paramStartTime").val().length == 0) {
			$("#" + portletNamespace + "startTime").val("");
		}
		
		if ($("#paramEndTime").val().length == 0) {
			$("#" + portletNamespace + "endTime").val("");
		}
	}
	
	$("#" + portletNamespace + "startTime").click(function() {
		$(".timepicker-popover").css("z-index","999999");
	});
	$("#" + portletNamespace + "endTime").click(function() {
		$(".timepicker-popover").css("z-index","999999");
	});
});

function submitForm() {
	var val = $("input[name='_jp_ubsecure_portal_jubjub_portlet_VexPortlet_implementationEnvironment']:checked").val();
	if(val == 1){
		if (confirm("スキャンを行ますか。")) {
			$("#saveBtn").prop("disabled", true);
			$("#cancelBtn").prop("disabled", true);
			$("#cancelBtn").removeAttr("onclick");
			$("#saveBtn").removeAttr("onclick");
			
			document.getElementById("fm").submit();
		} else {
		    
		}
	} else {
		if (confirm("本番環境にスキャンを行いますか。")) {
			$("#saveBtn").prop("disabled", true);
			$("#cancelBtn").prop("disabled", true);
			$("#cancelBtn").removeAttr("onclick");
			$("#saveBtn").removeAttr("onclick");
			
			document.getElementById("fm").submit();
		} else {
		    
		}
	}
	
	
}

function addRow(tableID) {
	var portletNamespace = $("#portletNamespace").val();

	var table = document.getElementById(tableID);

	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount-1);
	var rowIndex = document.getElementById(portletNamespace+"listTableSize").value;
	
	var cell1 = row.insertCell(0);
	var element1 = document.createElement("select");
	element1.name = portletNamespace+"protocolGroup"+rowIndex;
	element1.id = portletNamespace+"protocolGroup"+rowIndex;
	element1.setAttribute('style','width: auto !important');
	element1.setAttribute('class',"field form-control");
	element1.setAttribute("onchange", "changePortNumbers(this,"+rowIndex+")");	
		var option1 = document.createElement("option");
			option1.text = "http://";
			option1.value = 1;
		element1.add(option1);
		var option2 = document.createElement("option");
			option2.text = "https://";
		option2.value = 2;
		element1.add(option2);
	cell1.appendChild(element1);

	var cell2 = row.insertCell(1);
	var element2 = document.createElement("input");
	element2.type = "text";
	element2.name = portletNamespace+"host"+rowIndex;
	element2.id = portletNamespace+"host"+rowIndex;
	element2.setAttribute('class',"field form-control");
	element2.setAttribute('placeholder', 'example.com');
	element2.setAttribute('style','width:300px;');
	cell2.appendChild(element2);

	var cell3 = row.insertCell(2);
	var element3 = document.createElement("input");
	element3.type = "text";
	element3.name = portletNamespace+"port"+rowIndex;
	element3.id = portletNamespace+"port"+rowIndex;
	element3.setAttribute('class',"field form-control");
	element3.setAttribute('placeholder', '80');
	element3.setAttribute('value', '80');
	element3.setAttribute('style','width:80px;');
	cell3.appendChild(element3);
	
	var cell4 = row.insertCell(3);
	var element4 = document.createElement("input");
	element4.type = "button";
	element4.name = portletNamespace+"btnDelete"+rowIndex;
	element4.setAttribute('onclick',"deleteRow(this)");
	element4.setAttribute('class',"btn btn-default");
	element4.setAttribute('style','width:100%;margin-top:0px;');
	
	if ($('#labelProjectName').text() == "label-project-name") {
		element4.value = "button-delete";
	} else {
		element4.value = "削除";
	}
	
	element4.id = portletNamespace+"btnDelete"+ rowIndex;
	cell4.appendChild(element4);

	var currentSize = document.getElementById(portletNamespace+"currentListTableSize").value;
	
	document.getElementById(portletNamespace+"listTableSize").value = ++rowIndex;
	document.getElementById(portletNamespace+"currentListTableSize").value = ++currentSize;
}

function deleteRow(button) {
	var portletNamespace = $("#portletNamespace").val();
	var rowCount = document.getElementById(portletNamespace+"currentListTableSize").value;
	var i = button.parentNode.parentNode.rowIndex;
	if (rowCount > 1) {
		document.getElementById("targetInfoTable").deleteRow(i);
		document.getElementById(portletNamespace+"currentListTableSize").value = rowCount - 1;
		document.getElementById(portletNamespace+"listTableSize").value = rowCount - 1;
	}
}

function changePortNumbers(select, index) {
	var portletNamespace = $("#portletNamespace").val();
	
	if(select.value == 1){
		document.getElementById(portletNamespace+"port"+index).value = "80"; 
		document.getElementById(portletNamespace+"port"+index).innerText = "80"; 
		
	}else if(select.value == 2 ){
		document.getElementById(portletNamespace+"port"+index).value = "443"; 
		document.getElementById(portletNamespace+"port"+index).innerText = "443"; 
	}
}

function updateStartUrlTextArea(tableID, textAreaID) {
	var portletNamespace = $("#portletNamespace").val();

	var textArea = document.getElementById(textAreaID);
	
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	var textAreaValue = "";
	
	if (textArea.value != ""){
		if (confirm("URLが入力されていますが、ターゲット情報で上書きしますか？")) {
			confirmUpdateTextArea(tableID, textAreaID);
		}
	}else if(textArea.value == ""){
		confirmUpdateTextArea(tableID, textAreaID);
	}
}

function confirmUpdateTextArea(tableID, textAreaID) {
	var portletNamespace = $("#portletNamespace").val();

	var textArea = document.getElementById(textAreaID);
	
	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
	var textAreaValue = "";
	
	for (index = 0; index < rowCount; index++){
		var portalGroup = document.getElementById(portletNamespace+"protocolGroup"+index);
		var portalHost = document.getElementById(portletNamespace+"host"+index);
		var portalPort = document.getElementById(portletNamespace+"port"+index);
		var urlString = "";
		
		if(index == 0 && (portalHost.value=="" || portalPort.value == "" )){
			alert ("ターゲット情報を入力してください、");
		}else if(portalGroup!= null && portalGroup.value == 1){
			urlString = "http://" + portalHost.value + ":" + portalPort.value + "/";
		}else if(portalGroup!= null && portalGroup.value == 2 ){
			urlString = "https://" + portalHost.value + ":" + portalPort.value + "/";
		}
		
		textAreaValue = textAreaValue + urlString +'\r\n';
	}
	
	if (textAreaValue != "" ){
		textArea.value = textAreaValue;
		textArea.innterText = textAreaValue;
	}
}


function disableField(checkbox) {
	var portletNamespace = $("#portletNamespace").val();
	var numIndex = checkbox.getAttribute('id').replace( /^\D+/g, ''); 
	var inputNameFieldId = "#"+portletNamespace+"parameterName"+numIndex;
	var inputValueFieldId = "#"+portletNamespace+"parameterValue"+numIndex;

	if(checkbox.value == 0){
		checkbox.value = 1;
		$(inputNameFieldId).prop("readonly", "");
		$(inputValueFieldId).prop("readonly", "");
		$(checkbox).prop("checked", true);
	}
	else{
		checkbox.value = 0;
		$(inputNameFieldId).prop("readonly", "readonly");
		$(inputValueFieldId).prop("readonly", "readonly");
		$(checkbox).prop("checked", false);
	}
}

function submitFormGetLoginInfo() {
	var portletNamespace = $("#portletNamespace").val();
	var acquireLoginInformationURL = $("#acquireLoginInformationURL").val();
    
    document.getElementById("fm").action = acquireLoginInformationURL;
	document.getElementById("fm").submit();
}

function isBrowserIE () {
	var browser = navigator.userAgent;
	
	return (browser.indexOf("MSIE", 0) > -1 || (browser.indexOf("Firefox", 0) == -1 && browser.indexOf("Chrome", 0) == -1));
}

function submitConfirmFormAdvancedSetting(myYes,myNo) {
	var portletNamespace = $("#portletNamespace").val();
	var val = $("input[name='"+portletNamespace+"implementationEnvironment']:checked").val();
	
	var hostMsg = location.protocol+"//" +location.host+" says:";
	var msg="本番環境にスキャンを行いますか？";
	if(val == 1){
		msg = "スキャンを行ますか。";
		$("#confirmBoxImage").css("display","none");
	}else{
		$("#confirmBoxImage").css("display","");
	}
	
    var confirmBox = $("#confirm");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".hostMessage").text(hostMsg);
    confirmBox.find(".yes,.no").unbind().click(function() {
        confirmBox.hide();
     });
    confirmBox.find(".yes").click(myYes);
    confirmBox.find(".no").click(myNo);

    overlay(true);
    confirmBox.show();
    //document.getElementById("fm").submit();
 }

function submitConfirmForm(myYes,myNo) {
	var portletNamespace = $("#portletNamespace").val();
	var val = $("input[name='"+portletNamespace+"implementationEnvironment']:checked").val();
	
	var hostMsg = location.protocol+"//" +location.host+" says:";
	var msg="スキャンを行います。 本番環境にスキャンを行いますか？";
	if(val == 1){
		msg = "スキャンを行います。本当によろしいですか？";
		$("#confirmBoxImage").css("display","none");
	}else{
		$("#confirmBoxImage").css("display","");
	}
	
	$("#" + portletNamespace + "setCrawlingOnly").val(0);
    var confirmBox = $("#confirm");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".hostMessage").text(hostMsg);
    confirmBox.find(".yes,.no").unbind().click(function() {
        confirmBox.hide();
     });
    confirmBox.find(".yes").click(myYes);
    confirmBox.find(".no").click(myNo);

    overlay(true);
    confirmBox.show();
    //document.getElementById("fm").submit();
 }

function submitPatrolOnlyConfirmForm(myYes,myNo) {
	var portletNamespace = $("#portletNamespace").val();
	var val = $("input[name='"+portletNamespace+"implementationEnvironment']:checked").val();
	
	var hostMsg = location.protocol+"//" +location.host+" says:";
	var msg="自動巡回を行います。本番環境にスキャンを行いますか？";
	if(val == 1){
		msg = "自動巡回を行います。本当によろしいですか？";
		$("#confirmBoxImage").css("display","none");
	}else{
		$("#confirmBoxImage").css("display","");
	}
	
	$("#" + portletNamespace + "setCrawlingOnly").val(1);
	
    var confirmBox = $("#confirm");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".hostMessage").text(hostMsg);
    confirmBox.find(".yes,.no").unbind().click(function() {
        confirmBox.hide();
     });
    confirmBox.find(".yes").click(myYes);
    confirmBox.find(".no").click(myNo);

    overlay(true);
    confirmBox.show();
    //document.getElementById("fm").submit();
 }

function submitConfirmFormUrlUpdate (myYes,myNo) {
	var portletNamespace = $("#portletNamespace").val();
	var val = $("input[name='"+portletNamespace+"implementationEnvironment']:checked").val();
	
	var hostMsg = location.protocol+"//" +location.host+" says:";
	var msg="URLが入力されていますが、ターゲット情報で上書きしますか？";
	if(val == 1){
		msg = "URLが入力されていますが、ターゲット情報で上書きしますか？";
		$("#confirmBoxImageUpdate").css("display","none");
	}else{
		$("#confirmBoxImageUpdate").css("display","");
	}
	
    var confirmBox = $("#confirmUpdate");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".hostMessage").text(hostMsg);
//    confirmBox.find(".yes,.no").unbind().click(function() {
//        confirmBox.hide();
//     });
//    confirmBox.find(".yes").click(myYes);
//    confirmBox.find(".no").click(myNo);

    overlay(true);
    confirmBox.show();
    //document.getElementById("fm").submit();
 }

function hideConfirmBox(){
	var confirmBox = $("#confirm");
	confirmBox.hide();
	 overlay(false);
}

function overlay(showOverlay) {
	if(showOverlay){
		//Create overlay div after div wrapper
		$('#wrapper').after('<div id="overlay"></div>');
	}
	
	//Get overlay element and change its visibility.
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}