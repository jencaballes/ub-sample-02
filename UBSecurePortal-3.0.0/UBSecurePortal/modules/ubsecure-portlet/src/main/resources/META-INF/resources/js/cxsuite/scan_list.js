/**
 * CxSuite
 */

var focus = false;
var row = 0;

$(document).ready(function() {
	var screenName = $('#screenName').val();
	var screenNo = $("#screenNo").val();
	var browse = $("#browseBtn");
	browse.focus();
	
	$("#nonExistingScanError").addClass("hideError");
	
	if (screenName == "refresh_page") {
		var sortedColumn = $("#orderByCol").val();
		var sortType = $("#orderByType").val();
		var screenList = $("#screenList").val();
		
		showSortIndicator(sortedColumn, sortType, screenList);
		
		showHeaderLink(screenNo);
		
		window.setInterval(function(){
			refreshScan();
		}, 10000);
	}
	
	$(function() {
		var focusedElement = null;
		$(document).on('focus', 'input', function() {
			if (focusedElement == $(this)) return;
			focusedElement = $(this);
			setTimeout(function() {
				focusedElement.select();
			}, 50);
		});
	});
});

$('.dropdown-toggle').click(function () {
	focus = true;
	row = $(this).closest('tr').index() + 1;
});

$(document).click(function(e) {
	if (focus) {
		focus = false;
	}
});

function refreshScan() {
	var screenNo = $("#screenNo").val();
	var sortedColumn = $("#orderByCol").val();
	var sortType = $("#orderByType").val();
	var screenList = $("#screenList").val();
	var showCxError = $("#showCxError").val();
	
	$.ajax({
		url: $('#updateActionURL').val(),
		type: "POST",
		dataType: "html",
		async: false,
		success: function (data) {
			if (data != undefined && data != null && data != ''
				&& data.toString().indexOf('login') < 0 && data.toString().indexOf('screenNo') > -1) {
				$('div.search-div').empty();
				
				if (focus) {
					data = data.toString().replace("table-hover", "");
				}
				
				$('div.search-div').html(data);
				
				if (focus) {
					$('.table').find('tr:nth-child('+ row +')').children('td').css('background-color', '#EDF8FD');					
				}
				
				$("#dbConnError").hide();
				var error = $('.alert').text();
				
				if (error.indexOf("データベースにアクセス中、エラーが発生しました。 管理者に連絡してください。") > -1) {
					$('.alert').css('display', 'none');
				}
				
				$('.dropdown-toggle').click(function () {
					focus = true;
					row = $(this).closest('tr').index() + 1;
					$('.table').removeClass('table-hover');
					$('.table').find('td').css('background-color', '#FFF');
					$(this).closest('tr').children('td').css('background-color', '#EDF8FD');
				});

				$(document).click(function(e) {
					var focus = $('.taglib-icon.focus').text();
					if (focus == '') {
						$('.table').find('td').removeAttr('style');
						$('.table').addClass('table-hover');
					} else {
						$('.table').removeClass('table-hover');
					}
				});
			}
			showHeaderLink(screenNo);
		},
		error: function (data, error, status) {
		} 
	});
	
	showSortIndicator(sortedColumn, sortType, screenList);
	
	var error = $("#cxErrorMessage").val();
	
	if (error != null && error.length > 0 && error != "undefined") {
		$('.alert').addClass("hideError");
		$("#nonExistingScanError").removeClass("hideError");
		$("#nonExistingScanError").text($("#cxErrorMessage").val());
	}
	/*if (showCxError == "true") {
		
	}*/
}

function showSortIndicator (sortedColumn, sortType, screenList) {
	var tableColumn = 0;
	
	if (screenList == "entire_scan_list") {
		switch (sortedColumn) {
			case "scanId":
				tableColumn = 1;
				break;
			case "projectName":
				tableColumn = 2;
				break;
			case "groupName":
				tableColumn = 3;
				break;
			case "fileName":
				tableColumn = 4;
				break;
			case "hashValue":
				tableColumn = 5;
				break;
			case "scanManager":
				tableColumn = 6;
				break;
			case "scanRegistrationDate":
				tableColumn = 7;
				break;
			case "status":
				tableColumn = 8;
				break;
			case "process":
				tableColumn = 9;
				break;
			case "cxAndroidScanId":
				tableColumn = 12;
				break;
		}
	} else {
		switch (sortedColumn) {
			case "scanId":
				tableColumn = 1;
				break;
			case "fileName":
				tableColumn = 2;
				break;
			case "hashValue":
				tableColumn = 3;
				break;
			case "scanManager":
				tableColumn = 4;
				break;
			case "scanRegistrationDate":
				tableColumn = 5;
				break;
			case "scanStatus":
				tableColumn = 6;
				break;
			case "process":
				tableColumn = 7;
				break;
			case "cxAndroidScanId":
				tableColumn = 10;
				break;
		}
	}
	
	$('.lfr-search-container ').find('thead').find('tr').each(function(index) {
		$(document).find('th:nth-child(' + tableColumn + ')').addClass('table-sorted');
		
		if (sortType == "desc") {
			$(document).find('th:nth-child(' + tableColumn + ')').addClass('table-sorted-desc');
		}
	});
}

function showHeaderLink (screenNo) {
	if (screenNo == 3) {
		$('.search-div').find('.lfr-search-container ').find('thead').find('tr').each(function(index) {
			var strHeader = "レポート <a href='#' style='font-weight: bold; font-size: 8px; text-decoration: underline;' onclick=\"downloadVulnerability('"+$('#downloadVuln').val()+"'); return false;\">(解説)</a>";
			$(document).find('th:nth-child(10)').html(strHeader);
		});
	} else if (screenNo == 4) {
		$('.search-div').find('.lfr-search-container ').find('thead').find('tr').each(function(index) {
			var strHeader = "レポート <a href='#' style='font-weight: bold; font-size: 8px; text-decoration: underline;' onclick=\"downloadVulnerability('"+$('#downloadVuln').val()+"'); return false;\">(解説)</a>";
			$(document).find('th:nth-child(8)').html(strHeader);
		});
	}
}

function downloadVulnerability(url) {
	$.fileDownload(url, {
		successCallback: function (url) {
			
		},
		failCallback: function (html, url) {
			location.reload();
		}
	});
}

function confirmCompleteProject (projectId) {
	var msg = "選択したプロジェクトを完了しますか。";
	var url = $("#completeURL").val();
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl = $("#viewScanListURL").val();
	
	if (confirm(msg)) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = projectListUrl;
			}
		} else {
			$.ajax({
				url: url,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = projectListUrl;
					}
				},
				error: function(data, error, status) {
					
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function confirmOpenProject (projectId) {
	var msg = "選択したプロジェクトを公開しますか。";
	var url = $("#openURL").val();
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl = $("#viewScanListURL").val();
	
	if (confirm(msg)) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = projectListUrl;
			}
		} else {
			$.ajax({
				url: url,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = projectListUrl;
					}
				},
				error: function(data, error, status) {
					
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function confirmStopScan (scanId) {
	var msg = "選択したスキャンを中止しますか。";
	var url = $("#stopURL_" + scanId).val();
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl;
	var screenNo = $("#screenNo").val();
	
	if (screenNo == 4) {
		scanListUrl = $("#viewScanListURL").val();
	} else if (screenNo == 3) {
		scanListUrl = $("#viewEntireScanListURL").val();
	}
	disableHover(scanId);
	
	if (confirm(msg)) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = projectListUrl;
			}
		} else {
			$.ajax({
				url: url,
				async: false,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = projectListUrl;
					}
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function confirmReexecuteScan (scanId) {
	var msg = "選択したスキャンを再実施しますか。";
	var url = $("#reexecuteURL_" + scanId).val();
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl;
	var screenNo = $("#screenNo").val();
	
	if (screenNo == 4) {
		scanListUrl = $("#viewScanListURL").val();
	} else if (screenNo == 3) {
		scanListUrl = $("#viewEntireScanListURL").val();
	}
	disableHover(scanId);
	
	if (confirm(msg)) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = projectListUrl;
			}
		} else {
			$.ajax({
				url: url,
				dataType: "JSON",
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = projectListUrl;
					}
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function confirmRequestReview (scanId) {
	var msg = "対象のスキャンの精査依頼を送りますか。";
	var url = $("#requestURL_" + scanId).val();
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl;
	var screenNo = $("#screenNo").val();
	
	if (screenNo == 4) {
		scanListUrl = $("#viewScanListURL").val();
	} else if (screenNo == 3) {
		scanListUrl = $("#viewEntireScanListURL").val();
	}
	disableHover(scanId);
	
	if (confirm(msg)) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = projectListUrl;
			}
		} else {
			$.ajax({
				url: url,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = projectListUrl;
					}
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');

		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function confirmCancelReview (scanId) {
	var msg = "対象のスキャンの精査を取り消しますか。";
	var url = $("#cancelURL_" + scanId).val();
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl;
	var screenNo = $("#screenNo").val();
	
	if (screenNo == 4) {
		scanListUrl = $("#viewScanListURL").val();
	} else if (screenNo == 3) {
		scanListUrl = $("#viewEntireScanListURL").val();
	}
	disableHover(scanId);
	
	if (confirm(msg)) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = projectListUrl;
			}
		} else {
			$.ajax({
				url: url,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = projectListUrl;
					}
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function confirmRegenerateReport (scanId) {
	var msg = "対象のスキャンのレポートを再作成しますか。";
	var url = $("#regenerateURL_" + scanId).val();
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl;
	var screenNo = $("#screenNo").val();
	
	if (screenNo == 4) {
		scanListUrl = $("#viewScanListURL").val();
	} else if (screenNo == 3) {
		scanListUrl = $("#viewEntireScanListURL").val();
	}
	disableHover(scanId);
	
	if (confirm(msg)) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = projectListUrl;
			}
		} else {
			$.ajax({
				url: url,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = projectListUrl;
					}
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function confirmDeleteScan (scanId) {
	var msg = "選択したスキャンを削除しますか。";
	var url = $("#deleteURL_" + scanId).val();
	var projectListUrl = $("#viewProjectListURL").val();
	var scanListUrl;
	var screenNo = $("#screenNo").val();
	
	if (screenNo == 4) {
		scanListUrl = $("#viewScanListURL").val();
	} else if (screenNo == 3) {
		scanListUrl = $("#viewEntireScanListURL").val();
	}
	disableHover(scanId);
	
	if (confirm(msg)) {
		if (isBrowserIE()) {
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url, false);
			xhr.send();
			var JSON = $.parseJSON(xhr.responseText);
			
			if (JSON.redirect == false) {
				window.location.href = scanListUrl;
			} else {
				window.location.href = projectListUrl;
			}
		} else {
			$.ajax({
				url: url,
				success: function (data) {
					if (data.redirect == false) {
						window.location.href = scanListUrl;
					} else {
						window.location.href = projectListUrl;
					}
				}
			});
		}
	} else {
		$('.table').addClass('table-hover');
		
		$(".lfr-search-container").find('tbody').find('tr').each(function(index) {
			$(document).find('td').css('background-color', '#FFFFFF');
		});
	}
}

function confirmAddScan () {
	var msg = "スキャンを行いますか。";
	
	if (confirm(msg)) {
		$("#browseBtn").prop("disabled", true);
		$("#saveBtn").prop("disabled", true);
		$("#cancelBtn").addClass("disabled");
		$("#cancelBtnHref").removeAttr("href");
		$("#saveBtn").removeAttr("onclick");
		$("#cancelBtn").removeAttr("onclick");
		
		document.getElementById("fm").submit();
	}
}

function confirmUpdateScan () {
	var msg = "選択したスキャンを変更しますか。";
	
	if (confirm(msg)) {
		$("#browseBtn").prop("disabled", true);
		$("#saveBtn").prop("disabled", true);
		$("#cancelBtn").addClass("disabled");
		$("#saveBtn").removeAttr("onclick");
		
		document.getElementById("fm").submit();
	}
}

function cancelAddUpdateScan () {
	$("#browseBtn").prop("disabled", true);
	$("#saveBtn").prop("disabled", true);
	$("#cancelBtn").addClass("disabled");
	$("#saveBtn").removeAttr("onclick");
}

function downloadReport (scanId, projectId) {
	$.ajax({
		url: $("#downloadReportURL_" + scanId + "_" + projectId).val(),
		success: function (data) {

		}
	});
}

function downloadReference (scanId, projectId) {
	$.ajax({
		url: $("#downloadReferenceURL_" + scanId + "_" + projectId).val(),
		success: function (data) {
			if (data.redirect == false) {
				location.reload();
			} else if (data.redirect == true) {
				window.location.href = $("#viewProjectListURL").val();
			}
		}
	});
}

function downloadReport (url) {
	$.fileDownload(url, {
		successCallback: function (url) {
		},
		failCallback: function (html, url) {
			location.reload();
		}
	});
}

function disableHover(scanId) {
	var strScanId = "";
	
	if (scanId < 10) {
		strScanId = "0000" + scanId;
	} else if (scanId < 100) {
		strScanId = "000" + scanId;
	} else if (scanId < 1000) {
		strScanId = "00" + scanId;
	} else if (scanId < 10000) {
		strScanId = "0" + scanId;
	} else {
		strScanId = scanId + "";
	}
	
	$('.table').removeClass('table-hover');

	$("tr td:contains('"+ strScanId +"')").each(function(){
		$(this).closest('tr').children('td').css('background-color', '#EDF8FD');
	});
}

function isBrowserIE () {
	var browser = navigator.userAgent;
	
	return (browser.indexOf("MSIE", 0) > -1 || (browser.indexOf("Firefox", 0) == -1 && browser.indexOf("Chrome", 0) == -1));
}