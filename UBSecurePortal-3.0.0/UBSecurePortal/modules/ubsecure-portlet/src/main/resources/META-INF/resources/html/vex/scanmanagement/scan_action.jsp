<%@page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ScanItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<portlet:defineObjects />

<!-- Vex -->

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	ScanItem scan = (ScanItem) row.getObject();
	Project project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
	
	long scanId = scan.getScanId();
	String scanName = scan.getFileName();
	String name = ScanItem.class.getName();
	
	HttpSession httpSession = null;

	if (renderRequest != null) {
		httpSession = ControllerHelper.getHttpSession(renderRequest);
	} else if (resourceRequest != null) {
		httpSession = ControllerHelper.getHttpSession(resourceRequest);
	} else {
		httpSession = request.getSession();
	}
	
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	int iUserRole = PortalConstants.INT_ZERO;

	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
%>
<portlet:actionURL name="viewScanList" var="viewScanListURL">
	<portlet:param name="projectId" value="<%= String.valueOf(scan.getProjectId()) %>" />
</portlet:actionURL>

<input type="hidden" id="url" value="<%= viewScanListURL.toString() %>" />

<liferay-ui:icon-menu message="button-action" icon="/o/ubsecure-theme/images/common/tool.png" cssClass="portlet-action">
	<%
	
	if(scan.getProjectStatus() != ProjectStatus.COMPLETE.getInteger())
	{
		/* Recrawling*/
		if (scan.getScanStatus() == ScanStatus.CRAWLING_WAITING.getInteger()) {
			String confirmReCrawl = "javascript:confirmReCrawl(" + scanId + ")";
			%>
			<portlet:actionURL name="reCrawl" var="reCrawlURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL   >
		
			<% String strRecrawl = "reCrawlURL_" + scanId; %>
			<input type="hidden" name="reCrawlURL" id="<%= strRecrawl %>" value="<%= reCrawlURL.toString() %>" />
			<liferay-ui:icon image="reexecute" message="button-recrawling" url="<%= confirmReCrawl %>" cssClass="portlet-action-icon" />
			<%
		}
		
		/*Abort crawl*/
		if (scan.getScanStatus() == ScanStatus.CRAWLING_WAITING.getInteger()) {
			String confirmAbortCrawl = "javascript:confirmAbortCrawl(" + scanId + ")";
			%>
			<portlet:actionURL name="abortCrawl" var="abortCrawlURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>
		
			<% String strAbortCrawl = "abortCrawlURL_" + scanId; %>
			<input type="hidden" name="abortCrawlURL" id="<%= strAbortCrawl %>" value="<%= abortCrawlURL.toString() %>" />
			<liferay-ui:icon image="stop" message="button-stop" url="<%= confirmAbortCrawl %>" cssClass="portlet-action-icon" /> 
			<%
		}
		
		/* Interrupted Crawl*/
		if (scan.getScanStatus() == ScanStatus.CRAWLING.getInteger()) {
			String confirmInterruptedCrawl = "javascript:confirmInterruptedCrawl(" + scanId + ")";
			%>
			<portlet:actionURL name="interruptedCrawl" var="interruptedCrawlURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>
		
			<% String strInterruptedCrawl = "interruptedCrawlURL_" + scanId; %>
			<input type="hidden" name="interruptedCrawlURL" id="<%= strInterruptedCrawl %>" value="<%= interruptedCrawlURL.toString() %>" />
			<liferay-ui:icon image="stop" message="button-crawling-interrupted" url="<%= confirmInterruptedCrawl %>" cssClass="portlet-action-icon" /> 
			<%
			
		/* Cancel Crawl*/
			String confirmCancelCrawl = "javascript:confirmCancelCrawl(" + scanId + ")";
			%>
			<portlet:actionURL name="cancelCrawl" var="cancelCrawlURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>
		
			<% String strCancelCrawl = "cancelCrawlURL_" + scanId; %>
			<input type="hidden" name="cancelCrawlURL" id="<%= strCancelCrawl %>" value="<%= cancelCrawlURL.toString() %>" />
			<liferay-ui:icon image="stop" message="button-crawling-cancel" url="<%= confirmCancelCrawl %>" cssClass="portlet-action-icon" />
			<%
		}
		
		
		/* Restart Crawling */
		if (scan.getScanStatus() == ScanStatus.CRAWLING_INTERRUPTED.getInteger()) {
			String confirmRestartCrawl = "javascript:confirmRestartCrawl(" + scanId + ")";
			%>
			<portlet:actionURL name="restartCrawl" var="restartCrawlURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL   >
		
			<% String strRestartCrawl = "restartCrawlURL_" + scanId; %>
			<input type="hidden" name="restartCrawlURL" id="<%= strRestartCrawl %>" value="<%= restartCrawlURL.toString() %>" />
			<liferay-ui:icon image="reexecute" message="button-crawling-restart" url="<%= confirmRestartCrawl %>" cssClass="portlet-action-icon" />
			<%
		}
		
		/* Copy Crawling Setting*/
		if ((scan.getScanStatus() == ScanStatus.CRAWLING_INTERRUPTED.getInteger() ||  scan.getScanStatus() == ScanStatus.CRAWLING_FAILURE.getInteger() || 
		 	scan.getScanStatus() == ScanStatus.SCAN_WAITING.getInteger() || scan.getScanStatus() == ScanStatus.SCAN_INTERRUPTED.getInteger() || 
		    scan.getScanStatus() == ScanStatus.FAILURE.getInteger() || scan.getScanStatus() == ScanStatus.COMPLETE.getInteger()) && scan.getProjectStatus() != ProjectStatus.COMPLETE.getInteger()) {
			String confirmCopyCrawlingSetting = "javascript:confirmCopyCrawlingSetting(" + scanId +")";
			%>
				<portlet:actionURL name="copyCrawlingSetting" var="copyCrawlingSettingURL"> 
				<%-- <portlet:actionURL name="viewRegisterScanSimpleSetting" var="copyCrawlingSettingURL"> --%>
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
				<portlet:param name="userAction" value="<%=String.valueOf(PortalConstants.USER_EVENT_VIEW_CREATE_SCAN)%>" />
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>  
		
			<% String strCopyCrawlingSetting = "copyCrawlingSettingURL_" + scanId; %>                        
			<input type="hidden" name="copyCrawlingSettingURL" id="<%= strCopyCrawlingSetting %>" value="<%= copyCrawlingSettingURL.toString() %>" />
			<liferay-ui:icon image="copy" message="button-crawling-copy-setting" url="<%= copyCrawlingSettingURL %>" cssClass="portlet-action-icon" />
			 
			<%-- <liferay-ui:icon image="copy" message="button-crawling-copy-setting" url="<%= confirmCopyCrawlingSetting %>" cssClass="portlet-action-icon" />
			 --%>
			 <%
		}
		
		/*Rescan*/
		if (scan.getScanStatus() == ScanStatus.SCAN_WAITING.getInteger()) {
			String confirmReexecute = "javascript:confirmReexecuteScan(" + scanId + ")";
			%>
			<portlet:actionURL name="reexecuteScan" var="reexecuteScanURL">
		        <portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
		        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
		        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
				<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
		    </portlet:actionURL>
			<% String strReexecute = "reexecuteURL_" + scanId; %>
		    <input type="hidden" name="reexecuteURL" id="<%= strReexecute %>" value="<%= reexecuteScanURL.toString() %>" />
			<liferay-ui:icon image="reexecute" message="button-rescan" url="<%= confirmReexecute %>" cssClass="portlet-action-icon" />
			<%
			
		/*Stop scan*/	
			String confirmStopScan = "javascript:confirmStopScan(" + scanId + ")";
			%>
			<portlet:actionURL name="stopVexScan" var="stopScanURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>
		
			<% String strStopScan = "stopScanURL_" + scanId; %>
			<input type="hidden" name="stopScanURL" id="<%= strStopScan %>" value="<%= stopScanURL.toString() %>" />
			<liferay-ui:icon image="stop" message="button-stop" url="<%= confirmStopScan %>" cssClass="portlet-action-icon" />
			<%
		}
		
		/* Interrupted scan*/
		if (scan.getScanStatus() == ScanStatus.SCANNING.getInteger()) {
			String confirmInterruptedScan = "javascript:confirmInterruptedScan(" + scanId + ")";
			%>
			<portlet:actionURL name="interruptedScan" var="interruptedScanURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
				<portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
				<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>
		
			<% String strInterruptedScan = "interruptedScanURL_" + scanId; %>
			<input type="hidden" name="interruptedScanURL" id="<%= strInterruptedScan %>" value="<%= interruptedScanURL.toString() %>" />
			<liferay-ui:icon image="stop" message="button-scan-interrupted" url="<%= confirmInterruptedScan %>" cssClass="portlet-action-icon" /> 
			<%
			
		/* Cancel Scan*/
			String confirmCancelScan = "javascript:confirmCancelScan(" + scanId + ")";
			%>
			<portlet:actionURL name="cancelScan" var="cancelScanURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>
		
			<% String strCancelScan = "cancelScanURL_" + scanId; %>
			<input type="hidden" name="cancelScanURL" id="<%= strCancelScan %>" value="<%= cancelScanURL.toString() %>" />
			<liferay-ui:icon image="stop" message="button-scan-cancel" url="<%= confirmCancelScan %>" cssClass="portlet-action-icon" />
			<%
		}
		
		/* Restart Scan */
		if (scan.getScanStatus() == ScanStatus.SCAN_INTERRUPTED.getInteger()) {
			String confirmRestartScan = "javascript:confirmRestartScan(" + scanId + ")";
			%>
			<portlet:actionURL name="restartScan" var="restartScanURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL   >
		
			<% String strRestartScan = "restartScanURL_" + scanId; %>
			<input type="hidden" name="restartScanURL" id="<%= strRestartScan %>" value="<%= restartScanURL.toString() %>" />
			<liferay-ui:icon image="reexecute" message="button-scan-restart" url="<%= confirmRestartScan %>" cssClass="portlet-action-icon" />
			<%
		}
		
		/* Copy Crawling Result and scan*/
		if (scan.getScanStatus() == ScanStatus.SCAN_INTERRUPTED.getInteger() ||  scan.getScanStatus() == ScanStatus.FAILURE.getInteger() ||
			scan.getScanStatus() == ScanStatus.COMPLETE.getInteger()) {
			 String confirmCopyCrawlingAndScan = "javascript:confirmCopyCrawlingAndScan(" + scanId + ")"; 
			%>
			<portlet:actionURL name="copyCrawlingAndScan" var="copyCrawlingAndScanURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>
		
			<% String strCopyCrawlingAndScan = "copyCrawlingAndScanURL_" + scanId; %>
			<input type="hidden" name="copyCrawlingAndScanURL" id="<%= strCopyCrawlingAndScan %>" value="<%= copyCrawlingAndScanURL.toString() %>"/>
			<liferay-ui:icon image="regenerate" message="button-scan-copy-crawling" url="<%= confirmCopyCrawlingAndScan %>" cssClass="portlet-action-icon" />
			<%
		}
		
		/* Review Detection Result - Complete status */
		if (scan.getScanStatus() == ScanStatus.COMPLETE.getInteger()) {
			String viewDetectionResult = "javascript:viewDetectionResult(" + scanId + ")";
			
			%>
			<portlet:actionURL name="viewDetectionResult" var="viewDetectionResultURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
		        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
		        <portlet:param name="fileName" value="<%= String.valueOf(scanName) %>" />
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_DETECTION_RESULT_REVIEW) %>" />
		    </portlet:actionURL>
		<% 
		
		String strViewDetectionResult = "viewDetectionResultURL_" + scanId; %>
	    <input type="hidden" name="viewDetectionResultURL" id="<%= strViewDetectionResult %>" value="<%= viewDetectionResultURL.toString() %>" />
		<liferay-ui:icon image="regenerate" message="button-review-detection-result" url="<%= viewDetectionResult%>" cssClass="portlet-action-icon" />
		<%
		}
		
		/* Copy patrol(crawling) setting - CRAWLING COMPLETE status only*/
		if (scan.getScanStatus() == ScanStatus.CRAWLING_COMPLETED.getInteger()) {
			String copyPatrolSetting = "javascript:confirmCopyPatrolSetting(" + scanId + ")";
			
			%>
			<portlet:actionURL name="copyPatrolSetting" var="copyPatrolSettingURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
		        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
		        <portlet:param name="fileName" value="<%= String.valueOf(scanName) %>" />
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_DETECTION_RESULT_REVIEW) %>" />
		    </portlet:actionURL>
		<% 
		
		String strCopyPatrolSetting = "copyPatrolSettingURL_" + scanId; %>
	    <input type="hidden" name="viewDetectionResultURL" id="<%= strCopyPatrolSetting %>" value="<%= copyPatrolSettingURL.toString() %>" />
		<liferay-ui:icon image="copy" message="button-copy-crawling-setting" url="<%= copyPatrolSettingURL%>" cssClass="portlet-action-icon" />
		<%
		}
		
		/*  Scan action for Crawling Completed */
		if (scan.getScanStatus() == ScanStatus.CRAWLING_COMPLETED.getInteger()) {
			 String confirmUpdateScanPatrolSettings = "javascript:confirmUpdateScanPatrolSettings(" + scanId + ")"; 
			%>                                                        
			<portlet:actionURL name="updateScanPatrolSettings" var="updateScanPatrolSettingsURL">
				<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
				<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			
		        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
			</portlet:actionURL>
		
			<% String strUpdateScanPatrolSettings = "updateScanPatrolSettingsURL_" + scanId; %>
			<input type="hidden" name="updateScanPatrolSettings" id="<%= strUpdateScanPatrolSettings %>" value="<%= updateScanPatrolSettingsURL.toString() %>"/>
			<liferay-ui:icon image="regenerate" message="button-scan-using-patrol-results" url="<%= confirmUpdateScanPatrolSettings %>" cssClass="portlet-action-icon" />
			<%
		}
		
	}
	
	if(iUserRole == UserRole.OVERALL_ADMIN.getInteger()){
		String confirmDeleteScan = "javascript:confirmDeleteScan(" + scanId + ")";
		%>
		<portlet:actionURL name="deleteScan" var="deleteScanURL">
			<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	       <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_SCAN_LIST) %>" />
	    </portlet:actionURL>
		<% String strDelete = "deleteURL_" + scanId; %>
	    <input type="hidden" name="deleteURL" id="<%= strDelete %>" value="<%= deleteScanURL.toString() %>" />
		<liferay-ui:icon image="trash" message="button-delete" url="<%= confirmDeleteScan %>" cssClass="portlet-action-icon" />
		<%
	}
	%> 
</liferay-ui:icon-menu>

