/*** Vex */

$(document).ready(function() {
	if ($('#labelAutoCrawlingSetting').text() == "label-autocrawling-setting") {
		document.getElementById("form-textarea-not-send-password").innerHTML = "Specify whether to send password during crawling." +
				"\nIf checked, do not change password during crawling," + "\nSend with password empty." + "\nThe condition for determining whether the parameter is a password is" + 
				"\nin 'Parameter name as password', enter it." ;
	
		document.getElementById("form-textarea-auto-post-form").innerHTML = "Specify whether or not to automatically send the form found during crawling." +
		"\nIf checked, the form will be sent automatically." + "\nIf not checked, we will not submit the form.";
		
		document.getElementById("form-textarea-post-method").innerHTML = "Specify whether to execute the POST method during crawling." +
		"\nIf checked, execute the POST method." + "\nIf not checked, the POST method will not be executed." + 
		"\nWeb site change processing, completion processing, deletion processing will be done by crawling" +
		"\nIf you want to reduce it, please uncheck it.";
	}
});

function goToTargetInfo() {
	var goToTargetInfo = document.getElementById("goToTargetInfo").value.trim();
	
	document.getElementById("fm").action = goToTargetInfo;
	document.getElementById("fm").submit();
}

function submitFormGetParams() {
	var portletNamespace = $("#portletNamespace").val();
	var loginFormPath = document.getElementById(portletNamespace+"loginformpath").value.trim();
	if(loginFormPath == ""){
		alert("Please enter login form path.");
	}else{
		document.getElementById("fmGetLoginInfo").submit();
	}
}

function useLoginForm() {
	document.getElementById("displayloginForm").style = "display:block";
}

function submitFormModal() {
	$("#submitBtnModal").prop("disabled", true);
	$("#submitBtnModal").removeAttr("onclick");
	$("#cancelBtnModal").prop("disabled", true);
	$("#cancelBtnModal").removeAttr("onclick");
	
	document.getElementById("fModal").submit();
}

function deleteParentRow(button) {
	var portletNamespace = $("#portletNamespace").val();
	var table = document.getElementById("parentLoginSettingList");
	var i = button.parentNode.parentNode.parentNode.parentNode.rowIndex; // table: parentLoginSettingList rowIndex
	table.deleteRow(i);
}

function changeValue(checkbox) {
	var checkboxID = checkbox.getAttribute('id');
	if(checkbox.value == 0){
		$(checkboxID).prop("checked", true);
		checkbox.value = 1;
	}else{
		$(checkboxID).prop("checked", false);
		checkbox.value = 0;
	}
}

function cancelAddUpdateScan () {
	$("#saveHostList").prop("disabled", true);
	$("#cancelBtn").addClass("disabled");
	$("#saveHostList").removeAttr("onclick");
}

function editLoginSetting(button){
	var buttonID = button.getAttribute('id');
	var numIndex = buttonID.replace( /^\D+/g, ''); 
	var editLoginFormUrl = $("#editLoginFormUrl"+numIndex).val();

			Liferay.Util.openWindow({
					dialog: {
							destroyOnHide:true,
							height:600,
							width:600,
							resizable: false
						},
						id: 'open_get_login_form',
						uri: editLoginFormUrl,
						title: Liferay.Language.get('header-login-settings')
						});
}

function deleteLoginSetting(button) {
	var buttonID = button.getAttribute('id');
	var numIndex = buttonID.replace( /^\D+/g, ''); 
	var deleteLoginFormUrl = $("#deleteLoginFormUrl"+numIndex).val();

	var portletNamespace = $("#portletNamespace").val();
	var table = document.getElementById("parentLoginSettingList");

	var i = button.parentNode.parentNode.rowIndex; // table: parentLoginSettingList rowIndex

	table.deleteRow(i);

	$.ajax({
             url : deleteLoginFormUrl,
		     type: "POST",
             processData: false,
             contentType: false,
             success:function() 
              	{
              		$("#loginSettingContainer1").load(" #loginSettingContainer1 > *");
              		$("#loginSettingContainer2").load(" #loginSettingContainer2 > *:not(#button-holder-login)");
                }
	});
}

function isBrowserIE () {
	var browser = navigator.userAgent;
	
	return (browser.indexOf("MSIE", 0) > -1 || (browser.indexOf("Firefox", 0) == -1 && browser.indexOf("Chrome", 0) == -1));
}

function submitConfirmForm(myYes,myNo) {
	var portletNamespace = $("#portletNamespace").val();
	var val = $("input[name='"+portletNamespace+"implementationEnvironment']:checked").val();
	
	var hostMsg = location.protocol+"//" +location.host+" says:";
	var msg="スキャンを行います。 本番環境にスキャンを行いますか？";
	if(val == 1){
		msg = "スキャンを行います。本当によろしいですか？";
		$("#confirmBoxImage").css("display","none");
	}else{
		$("#confirmBoxImage").css("display","");
	}
	
	$("#" + portletNamespace + "setCrawlingOnly").val(0);
    var confirmBox = $("#confirm");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".hostMessage").text(hostMsg);
    confirmBox.find(".yes,.no").unbind().click(function() {
        confirmBox.hide();
     });
    confirmBox.find(".yes").click(myYes);
    confirmBox.find(".no").click(myNo);

    overlay(true);
    confirmBox.show();
    //document.getElementById("fm").submit();
 }

function submitPatrolOnlyConfirmForm(myYes,myNo) {
	var portletNamespace = $("#portletNamespace").val();
	var val = $("input[name='"+portletNamespace+"implementationEnvironment']:checked").val();
	
	var hostMsg = location.protocol+"//" +location.host+" says:";
	var msg="自動巡回を行います。本番環境にスキャンを行いますか？";
	if(val == 1){
		msg = "自動巡回を行います。本当によろしいですか？";
		$("#confirmBoxImage").css("display","none");
	}else{
		$("#confirmBoxImage").css("display","");
	}
	
	$("#" + portletNamespace + "setCrawlingOnly").val(1);
		
    var confirmBox = $("#confirm");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".hostMessage").text(hostMsg);
    confirmBox.find(".yes,.no").unbind().click(function() {
        confirmBox.hide();
     });
    confirmBox.find(".yes").click(myYes);
    confirmBox.find(".no").click(myNo);

    overlay(true);
    confirmBox.show();
    //document.getElementById("fm").submit();
 }

function hideConfirmBox(){
	var confirmBox = $("#confirm");
	confirmBox.hide();
	 overlay(false);
}

function overlay(showOverlay) {
	if(showOverlay){
		//Create overlay div after div wrapper
		$('#wrapper').after('<div id="overlay"></div>');
	}
	
	//Get overlay element and change its visibility.
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}

