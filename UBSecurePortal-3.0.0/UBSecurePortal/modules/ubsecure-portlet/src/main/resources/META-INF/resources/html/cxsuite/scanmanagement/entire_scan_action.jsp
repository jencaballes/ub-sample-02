<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@  taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %>
<%@ page import="com.liferay.portal.kernel.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResultItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser" %>

<!-- CxSuite -->

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	ResultItem scan = (ResultItem) row.getObject();
	Project project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
	
	long scanId = scan.getScanId();
	String name = ResultItem.class.getName();
	
	String redirect = PortalConstants.STRING_EMPTY;
	
	int defaultHigh = Integer.parseInt(ConfigurationFileParser.getConfig("cxsuite.closeexamination.threshold"));
%>
<portlet:actionURL name="viewEntireScanList" var="viewEntireScanListURL">
	
</portlet:actionURL>

<input type="hidden" id="url" value="<%= viewEntireScanListURL.toString() %>" />

<liferay-ui:icon-menu message="button-action" icon="/o/ubsecure-theme/images/common/tool.png" cssClass="portlet-action">
	<portlet:actionURL name="viewEditScan" var="viewEditScanURL">
		<portlet:param name="projectId"	value="<%= String.valueOf(project.getProjectId()) %>" />
		<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
		<portlet:param name="userAction" value="<%= String.valueOf(PortalConstants.USER_EVENT_VIEW_SCAN_CHANGE) %>" />
		<portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
	</portlet:actionURL>

	<liferay-ui:icon image="edit" message="button-scan-change" url="<%= viewEditScanURL.toString() %>" cssClass="portlet-action-icon" />
	
	<%
	if (scan.getStatus() == ScanStatus.SCAN_WAITING.getInteger() || scan.getStatus() == ScanStatus.SCANNING.getInteger()) {
		String confirmStop = "javascript:confirmStopScan(" + scanId + ")";
		%>
		<portlet:actionURL name="stopScan" var="stopScanURL">
			<portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
			<portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
			<portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
		</portlet:actionURL>
		
		<% String strStop = "stopURL_" + scanId; %>
		<input type="hidden" name="stopURL" id="<%= strStop %>" value="<%= stopScanURL.toString() %>" />
		<liferay-ui:icon image="stop" message="button-stop" url="<%= confirmStop %>" cssClass="portlet-action-icon" />
		<%
	}
	
	if (scan.getStatus() == ScanStatus.SCAN_WAITING.getInteger() || scan.getStatus() == ScanStatus.SCANNING.getInteger() || scan.getStatus() == ScanStatus.FAILURE.getInteger()) {
		String confirmReexecute = "javascript:confirmReexecuteScan(" + scanId + ")";
		%>
		<portlet:actionURL name="reexecuteScan" var="reexecuteScanURL">
			<portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
	    </portlet:actionURL>
	    <% String strReexecute = "reexecuteURL_" + scanId; %>
		<input type="hidden" name="reexecuteURL" id="<%= strReexecute %>" value="<%= reexecuteScanURL.toString() %>" />
	    <liferay-ui:icon image="reexecute" message="button-reexecute" url="<%= confirmReexecute %>" cssClass="portlet-action-icon" />
		<%
	}
	
	if (scan.getStatus() == ScanStatus.COMPLETE.getInteger() && scan.getHighCount() > defaultHigh) {
		String confirmRequest = "javascript:confirmRequestReview(" + scanId + ")";
		%>
		<portlet:actionURL name="requestReview" var="requestReviewURL">
			<portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
	    </portlet:actionURL>
	    <% String strRequest = "requestURL_" + scanId; %>
		<input type="hidden" name="requestURL" id="<%= strRequest %>" value="<%= requestReviewURL.toString() %>" />
	    <liferay-ui:icon image="view_tasks" message="button-request-review" url="<%= confirmRequest %>" cssClass="portlet-action-icon" />
		<%
	}
	
	if (scan.getStatus() == ScanStatus.UNDER_REVIEW.getInteger()) {
		String confirmCancel = "javascript:confirmCancelReview(" + scanId + ")";
		%>
		<portlet:actionURL name="cancelReview" var="cancelReviewURL">
			<portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
	    </portlet:actionURL>
	    <% String strCancel = "cancelURL_" + scanId; %>
		<input type="hidden" name="cancelURL" id="<%= strCancel %>" value="<%= cancelReviewURL.toString() %>" />
	    <liferay-ui:icon image="delete" message="button-cancel-review" url="<%= confirmCancel %>" cssClass="portlet-action-icon" />
		<%
	}
	
	if (scan.getStatus() == ScanStatus.COMPLETE.getInteger()
			|| scan.getStatus() == ScanStatus.UNDER_REVIEW.getInteger()
			|| (scan.getStatus() == ScanStatus.FAILURE.getInteger() && scan.getReportCount() > 0 && scan.getReferenceCount() > 0)) {
		String confirmRegenerate = "javascript:confirmRegenerateReport(" + scanId + ")";
		%>
		<portlet:actionURL name="regenerateReport" var="regenerateReportURL">
			<portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
	        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
	        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
			<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
	        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
	    </portlet:actionURL>
	    <% String strRegenerate = "regenerateURL_" + scanId; %>
		<input type="hidden" name="regenerateURL" id="<%= strRegenerate %>" value="<%= regenerateReportURL.toString() %>" />
	    <liferay-ui:icon image="regenerate" message="button-regenerate-report" url="<%= confirmRegenerate %>" cssClass="portlet-action-icon" />
		<%
	}
	
	String confirmDelete = "javascript:confirmDeleteScan(" + scanId + ")";
	%>
	<portlet:actionURL name="deleteScan" var="deleteScanURL">
        <portlet:param name="projectId" value="<%= String.valueOf(project.getProjectId()) %>" />
        <portlet:param name="scanId" value="<%= String.valueOf(scanId) %>" />
        <portlet:param name="cxAndroidProjectId" value="<%= project.getCxAndroidProjectId() %>" />
		<portlet:param name="cxAndroidScanId" value="<%= scan.getCxAndroidScanId() %>" />
        <portlet:param name="screenNo" value="<%= String.valueOf(PortalConstants.SCREEN_ENTIRE_SCAN_LIST) %>" />
    </portlet:actionURL>
    <% String strDelete = "deleteURL_" + scanId; %>
	<input type="hidden" name="deleteURL" id="<%= strDelete %>" value="<%= deleteScanURL.toString() %>" />
    <liferay-ui:icon image="trash" message="button-delete" url="<%= confirmDelete %>" cssClass="portlet-action-icon" />
</liferay-ui:icon-menu>