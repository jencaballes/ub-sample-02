<%@ page import="java.text.ParseException"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.net.URL"%>
<%@ page import="java.util.LinkedHashSet"%>
<%@ page import="java.util.Set"%>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ProjectItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.ResponseModel" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.Project" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformationItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem" %>

<meta http-equiv="Cache-Control" content="no-cache" >
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="Expires" content="-1" >

<portlet:defineObjects />
<!-- Vex -->

<%
	//Previous and current screen
	PortletSession pSession = renderRequest.getPortletSession();
	Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
	String strCurrScreen = PortalConstants.STRING_EMPTY;
	
	if (!CommonUtil.isObjectNull(oCurrScreen)) {
		strCurrScreen = oCurrScreen.toString();
		
		if (!CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
			pSession.setAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN, strCurrScreen);
		}
	}
	
	pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, PortalConstants.SCREEN_SCAN_AUTOCRAWLING_SETTING_REGISTRATION);
	// End

	List<VexTargetInformationItem> targetInformationList = (List<VexTargetInformationItem>) pSession.getAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
	List <ArrayList<VexLoginSettingItem>> loginSettingList = (ArrayList<ArrayList<VexLoginSettingItem>>) pSession.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
	     int totalNumOfParams = (int) pSession.getAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
	     int rowIndex = (int) renderRequest.getAttribute(PortalConstants.PARAM_LOGIN_INFO_INDEX);

	HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
	httpSession.setAttribute("Current_screen", "Vex");
	Object downloadFrom = httpSession.getAttribute("download_from");
	String strDownloadFrom = PortalConstants.STRING_EMPTY;
	int iUserRole = PortalConstants.INT_ZERO;
	long lUserId = PortalConstants.LONG_ZERO;
	boolean bUseVex = PortalConstants.FALSE;
	Object oUseVex = httpSession.getAttribute(PortalConstants.USE_VEX);
	Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
	Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
	int iUserAction = PortalConstants.INT_ZERO;
	
	String portletNamespace = null;
	String labelParameterName  = "parameterName";
	String labelParameterValue = "parameterValue";
	String checkItem = "checkItem";
	
	if(rowIndex < 0)
		iUserAction = PortalConstants.USER_EVENT_ADD_LOGIN_INFORMATION;
	else 
		iUserAction = PortalConstants.USER_EVENT_UPDATE_LOGIN_INFORMATION;

	Project project = (Project) renderRequest.getAttribute(PortalConstants.PARAM_PROJECT);
	long lProjectId = PortalConstants.LONG_ZERO;

	portletNamespace = renderResponse.getNamespace();

	if (!CommonUtil.isObjectNull(project)) {
		lProjectId = project.getProjectId();
	}
	
	if (!CommonUtil.isObjectNull(oUserRole)) {
		iUserRole = Integer.parseInt(oUserRole.toString());
	}
	
	if (!CommonUtil.isObjectNull(oUserId)) {
		lUserId = Long.parseLong(oUserId.toString());
	}
	
	if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
		bUseVex = true;
	} else {
		if (!CommonUtil.isObjectNull(oUseVex)) {
			bUseVex = Boolean.parseBoolean(oUseVex.toString());
		}
	}
	
	Object oCxServerErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG);
	String strCxServerErrorMsg = null;

	if (!CommonUtil.isObjectNull(oCxServerErrorMsg)) {
		strCxServerErrorMsg = oCxServerErrorMsg.toString();
	}
	
	Object oCxAPICallErrorMsg = pSession.getAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG);
	String strCxAPICallErrorMsg = PortalConstants.STRING_EMPTY;

	if (!CommonUtil.isObjectNull(oCxAPICallErrorMsg)) {
		strCxAPICallErrorMsg = oCxAPICallErrorMsg.toString();
	}
	
	if (bUseVex) {
		boolean bErrorFromVex = false;
		String strPortalMessage = PortalConstants.STRING_EMPTY;
		
		if (SessionErrors.contains(renderRequest, PortalMessages.VEX_REGISTER_SCAN_FAILED)) {
			strPortalMessage = PortalMessages.VEX_REGISTER_SCAN_FAILED;
			bErrorFromVex = true;
		}
%>
<div style="margin:10px">
	<portlet:actionURL name="viewRegisterScanAutoCrawlingSetting" var="viewRegisterScanAutoCrawlingSettingURL">
		<portlet:param name="projectId" value="<%=String.valueOf(lProjectId)%>" />
	</portlet:actionURL>
	
	<portlet:actionURL name="acquireLoginInformationDetailSetting" var="acquireLoginInformationURL">
		<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
		<portlet:param name="rowIndex" value="<%= String.valueOf(rowIndex) %>" />
		<portlet:param name="iUserAction" value="<%= String.valueOf(iUserAction) %>" />
	</portlet:actionURL>
	
	<portlet:actionURL name="addLoginInformation" var="addLoginInformationURL">
		<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
	</portlet:actionURL>
	
	<portlet:actionURL name="editLoginInformation" var="editLoginInformationURL">
		<portlet:param name="projectId" value="<%= String.valueOf(lProjectId) %>" />
		<portlet:param name="rowIndex" value="<%= String.valueOf(rowIndex) %>" />
</portlet:actionURL>
</div>

	<%
	if (bErrorFromVex) {
		%>
		<div class="alert alert-danger">
			<liferay-ui:message key="<%= strPortalMessage %>" /><liferay-ui:message key="<%= strCxAPICallErrorMsg %>" />
		</div>	
		<%
	} %>

<div class="modal fade in" id="login-form" style="width:100% !important">	
		<div style="overflow-y:auto;height:481px;">
			<div style="margin: 10px;">
			<liferay-ui:error key="<%= PortalMessages.PROJECT_ID_INVALID %>" message="<%= PortalMessages.PROJECT_ID_INVALID %>" />
			<liferay-ui:error key="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" message="<%= PortalMessages.PROJECT_DOES_NOT_EXIST %>" />
			<liferay-ui:success key="<%= PortalMessages.USER_DOES_NOT_EXIST %>" message="<%= PortalMessages.USER_DOES_NOT_EXIST %>" />
	
			<liferay-ui:error key="<%= PortalMessages.SYSTEM_EXCEPTION %>" message="<%= PortalMessages.SYSTEM_EXCEPTION %>" />
			<liferay-ui:error key="<%= PortalMessages.PORTAL_EXCEPTION %>" message="<%= PortalMessages.PORTAL_EXCEPTION %>" />
			<liferay-ui:error key="<%= PortalMessages.ORM_EXCEPTION %>" message="<%= PortalMessages.ORM_EXCEPTION %>" />
			<liferay-ui:error key="<%= PortalMessages.COMMON_EXCEPTION %>" message="<%= PortalMessages.COMMON_EXCEPTION %>" />
	
			<liferay-ui:error key="<%= PortalErrors.CX_SERVER_ERROR %>" message="<%= strCxServerErrorMsg %>" />
			<liferay-ui:error key="<%= PortalMessages.CX_SESSION_ID_INVALID %>" message="<%= PortalMessages.CX_SESSION_ID_INVALID %>" />	
			<liferay-ui:error key="<%= PortalMessages.PRESET_ID_INVALID %>" message="<%= PortalMessages.PRESET_ID_INVALID %>" />

			<liferay-ui:error key="<%= PortalMessages.SESSION_GET_TARGET_INFORMATION_LIST_FAILED %>" message="<%= PortalMessages.SESSION_GET_TARGET_INFORMATION_LIST_FAILED %>" />
			<liferay-ui:error key="<%= PortalMessages.MALFORMEDURL_EXCEPTION %>" message="<%= PortalMessages.MALFORMEDURL_EXCEPTION %>" />

			<liferay-ui:error key="<%= PortalMessages.GET_CRAWL_LOGIN_INFO_FAILED %>" message="<%= PortalMessages.GET_CRAWL_LOGIN_INFO_FAILED %>" />
			<liferay-ui:error key="<%= PortalMessages.LOGIN_URL_NOT_FOUND_IN_TARGET_INFORMATION_LIST %>" message="<%= PortalMessages.LOGIN_URL_NOT_FOUND_IN_TARGET_INFORMATION_LIST %>" />
				<div id="loginParamValueInvalidError" class="alert alert-danger" style="display:none">
					<liferay-ui:message key="<%= PortalMessages.LOGIN_PARAM_VALUE_INVALID %>" />
				</div>	
				<div id="unusableCharacterInputError" class="alert alert-danger" style="display:none">
					<liferay-ui:message key="<%= PortalMessages.UNUSABLE_CHARACTER_INPUT %>" />
				</div>
			</div>
			<div class="form-label"><liferay-ui:message key="header-select-login-URL" /></div>
				<form action="<%= acquireLoginInformationURL.toString() %>" method="post" id="fmGetLoginInfo" style="margin: 0;">
					<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
					<input type="hidden" id="projectId" name="projectId" value="<%= lProjectId %>" />
					<input type="hidden" id="userAction" name="userAction" value="<%= PortalConstants.USER_EVENT_GET_LOGIN_INFORMATION %>" />

					<div style="margin:10px">
						<table class="full-width with-border">
							<tbody>
							<tr class="input-row">
								<td class="form-label" style="padding-top: 15px;"><liferay-ui:message key="label-target-information" /></td>
									<td><select class="field form-control" style="width: auto !important" label="" name="<%= portletNamespace %>targetInformationGroup" >
											<% 
												if (!CommonUtil.isListNullOrEmpty(targetInformationList) && targetInformationList.size() > 0) {
													for (int k = 0; k < targetInformationList.size(); k++) {
														VexTargetInformationItem item = targetInformationList.get(k);
														String targetInformation = item.getProtocol() + item.getHost() + ":" + item.getPort();

														if(iUserAction == PortalConstants.USER_EVENT_UPDATE_LOGIN_INFORMATION && loginSettingList.size() != 0){
															URL loginURL = new URL(loginSettingList.get(rowIndex).get(k).getLoginurl());
															String updateTargetURL = loginURL.getProtocol() + "://" + loginURL.getAuthority();

															if(updateTargetURL.equals(targetInformation)){ %>
																<option value="<%= targetInformation %>" label="<%= targetInformation %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" selected="true"></option>
														<%	}else{ %>
																<option value="<%= targetInformation %>" label="<%= targetInformation %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"></option>
														<%	} %>
													 <%	}else{ %>
															<option value="<%= targetInformation %>" label="<%= targetInformation %>" style="white-space: nowrap; text-overflow: ellipsis; overflow: hidden;"></option>
													<%	} } } %>
										</select>
									</td>
							</tr>
							<tr class="input-row">
										<td class="form-label"><liferay-ui:message key="label-login-form-path" /></td>
										<% if(iUserAction == PortalConstants.USER_EVENT_UPDATE_LOGIN_INFORMATION && loginSettingList.size() != 0){
												URL loginURL = new URL(loginSettingList.get(rowIndex).get(0).getLoginurl());
												String updateLoginPath = loginURL.getPath();  %>
											<td><aui:input type="text" name="loginformpath" value="<%=updateLoginPath %>" label="" style="width:300px" cssClass="with-margin-top-10 with-margin-bottom-15"/>
										<% } else { %>
											<td><aui:input type="text" name="loginformpath" value="" label="" style="width:300px" cssClass="with-margin-top-10 with-margin-bottom-15"/></td>
											</td>
									<% } %>
							</tr>
							</tbody>
						</table>
						<button type="button" class="btn" onClick="submitFormGetParams()" style="width: 130px;float:right"><liferay-ui:message key="button-acquire-information" /></button>
					</div>
				</form>
				<!-- Visible when Acquire Information is executed -->
					   <% 
					   		String loginPath = (String) renderRequest.getAttribute(PortalConstants.PARAM_LOGIN_PATH_LABEL); 
					   		boolean isFormParamsVisible = (Boolean) renderRequest.getAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS); 
					   		List<VexLoginSettingItem> loginParams = (List<VexLoginSettingItem>) renderRequest.getAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS);
					   		int i = PortalConstants.INT_ZERO;
					   		int numOfEditForms = PortalConstants.INT_ZERO;
					   		int numOfUniqueLoginParams = PortalConstants.INT_ZERO;

					   		if(iUserAction == PortalConstants.USER_EVENT_UPDATE_LOGIN_INFORMATION && loginSettingList.size() != 0 
										&& CommonUtil.isStringNullOrEmpty(loginPath)){
										 loginPath = loginParams.get(i).getLoginurl();
										 List tempParams = new ArrayList<String>();
											
										// remove repeated param names
										Set<String> tempSet = new LinkedHashSet<>();
										for(VexLoginSettingItem item : loginSettingList.get(rowIndex)){
											tempParams.add(item.getParamname());
										}
											tempSet = new LinkedHashSet<>(tempParams);
											tempParams.clear();
											tempParams.addAll(tempSet);

										//get unique no. of params per form
										numOfEditForms = loginSettingList.get(rowIndex).size()/tempParams.size(); 
										numOfUniqueLoginParams = tempParams.size();
					   		}
					   	%>
				<form action="<%= ((iUserAction == PortalConstants.USER_EVENT_UPDATE_LOGIN_INFORMATION && loginSettingList.size() != 0) ? editLoginInformationURL.toString() : addLoginInformationURL.toString()) %>" method="post" name="<%= portletNamespace %>fmaddLoginForm" id="<%= portletNamespace %>fmaddLoginForm" style="margin: 0;">
									<input type="hidden" id="projectId" name="projectId" value="<%= lProjectId %>" />
									<input type="hidden" id="parentURL" name="parentURL" value="<%= viewRegisterScanAutoCrawlingSettingURL %>" />
									<input type="hidden" id="loginPath" name="<%= portletNamespace %>loginformpathlabel" value="<%= loginPath %>" />
									<input type="hidden" id="portletNamespace" value="<%= portletNamespace %>" />
									<input type="hidden" id="userAction" name="userAction" value="<%= PortalConstants.USER_EVENT_ADD_LOGIN_INFORMATION %>" />
									<input type="hidden" id="<%= portletNamespace %>currentListTableSize" name="<%= portletNamespace %>currentListTableSize" value="1" />

					<div id="displayloginForm" style="<%= ((isFormParamsVisible) ? "display:block;margin-top: 50px;" : "display:none") %>">
								<div class=full-width" style="padding:15px;">
									<table>
										<tr><td style="width:115px;"><liferay-ui:message key="header-parameter-setting" /></td>
											<td><button type="button" class="btn" onClick="addLoginForm()"><liferay-ui:message key="button-add" /></button></td>
										</tr>
									</table>
								</div>
					   	<% if(iUserAction == PortalConstants.USER_EVENT_UPDATE_LOGIN_INFORMATION && loginSettingList.size() != 0){ %>
					   		<!-- Layout when editing login information -->
							<div id="parentLoginForm">
							<table border=0 id="container" style="width: 100%;">
								<% i = totalNumOfParams; 
								   int ptrIndex = PortalConstants.INT_ZERO; // act as pointer on login params array %>
								<% for(int j = 0; j < numOfEditForms; j++){ %>
								<tr><td>
								<div class="with-border" style="margin:15px;" id="childForm<%=j%>">
									<p style="padding:10px 0px 0px 10px;"><liferay-ui:message key="label-form-action" /><%=loginPath%>)</p>
									<div class="" style="padding-left: 0px;padding-right: 0px;">
										<table class="full-width with-border" style="border-left:0px;border-right:0px;">
											<tr>
												<td class="full-width" id>
													<p style="font-size:small;margin-bottom:0px;padding-left: 15px" id="formNumber"></p>
												</td>
												<td>
													<button type="button" class="btn" onClick="deleteRow(this)" style="float:right;margin:10px;" ><liferay-ui:message key="button-delete" /></button>
												</td>
											</tr>
										</table>
										<div class="" style="margin:15px;">
											<table class="full-width with-border" border="1" id="inputFormFieldTable">
												<thead>
													<th style="padding-left:20px;"><liferay-ui:message key="label-parameter-name" /></th>
													<th style="padding-left:15px;"><liferay-ui:message key="label-this-is-an-account-information" /></th>
													<th style="padding-left:15px;"><liferay-ui:message key="label-parameter-value" /></th>
												</thead>
												<tbody>
												<% 
													if(!CommonUtil.isListNullOrEmpty(loginParams) && loginParams.size() > 0) {
														for(int ctr = 0; ctr < numOfUniqueLoginParams; ctr++){ 
																VexLoginSettingItem item = loginParams.get(ptrIndex);
																String parameterName = item.getParamname();
																String parameterValue = item.getParamvalue();
																 %>
															<tr>
																<td style="padding-left: 20px;width: 115px;"><%=parameterName%>
																	<input type="hidden" id="<%= portletNamespace+labelParameterName+i %>" name="<%= portletNamespace+labelParameterName+i %>" value="<%=parameterName%>" />
																</td>
																<td><center><input type="checkbox" id="<%=checkItem+i%>" name="<%=portletNamespace+checkItem+i%>" onclick="disableField(this)" checked value="1"></center></td>
																<td style="padding:5px;">
																<input type="text" name="<%= portletNamespace+labelParameterValue+i %>" id="<%= portletNamespace+labelParameterValue+i %>" value="<%= ((null == parameterValue ) ? ' ' : parameterValue) %>" class="field form-control"/></td>
															</tr>
														<% ++i;
														   ptrIndex++;  } } %>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								</td></tr>
								<% } %>
							</table>
							</div>			
					   	<% }else {%>
							<!-- Layout when adding login information -->
							<div id="parentLoginForm">
							<table border=0 id="container" style="width: 100%;">
								<tr><td>
								<div class="with-border" style="margin:15px;" id="childForm0">
									<% i = totalNumOfParams; %>
									<p style="padding:10px 0px 0px 10px;"><liferay-ui:message key="label-form-action" /><%=loginPath%>)</p>
									<div class="" style="padding-left: 0px;padding-right: 0px;">
										<table class="full-width with-border" style="border-left:0px;border-right:0px;">
											<tr>
												<td class="full-width" id>
													<p style="font-size:small;margin-bottom:0px;padding-left: 15px" id="formNumber"></p>
												</td>
												<td>
													<button type="button" class="btn" onClick="deleteRow(this)" style="float:right;margin:10px;" ><liferay-ui:message key="button-delete" /></button>
												</td>
											</tr>
										</table>
										<div class="" style="margin:15px;">
											<table class="full-width with-border" border="1" id="inputFormFieldTable">
												<thead>
													<th style="padding-left:20px;"><liferay-ui:message key="label-parameter-name" /></th>
													<th style="padding-left:15px;"><liferay-ui:message key="label-this-is-an-account-information" /></th>
													<th style="padding-left:15px;"><liferay-ui:message key="label-parameter-value" /></th>
												</thead>
												<tbody>
												<% 
													if(!CommonUtil.isListNullOrEmpty(loginParams) && loginParams.size() > 0) {
														for(VexLoginSettingItem item : loginParams){ 
																String parameterName = item.getParamname();
																String parameterValue = item.getParamvalue();
																 %>
															<tr>
																<td style="padding-left: 20px;width: 115px;"><%=parameterName%>
																	<input type="hidden" id="<%= portletNamespace+labelParameterName+i %>" name="<%= portletNamespace+labelParameterName+i %>" value="<%=parameterName%>" />
																</td>
																<td><center><input type="checkbox" id="<%=checkItem+i%>" name="<%=portletNamespace+checkItem+i%>" onclick="disableField(this)" checked value="1"></center></td>
																<td style="padding:5px;">
																<input type="text" name="<%= portletNamespace+labelParameterValue+i %>" id="<%= portletNamespace+labelParameterValue+i %>" value="<%= ((null == parameterValue ) ? ' ' : parameterValue) %>" class="field form-control"/></td>
															</tr>
														<% ++i; } } %>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								</td></tr>
							</table>
							</div>
						<%	} %>
							<input type="hidden" id="totalParams" name="<%= portletNamespace %>totalNumOfParams" value="<%=i%>" />
					</div>
					<div class="modal-footer">
						<button type="button" name="cancelBtnModal" id="cancelBtnModal" data-dismiss="modal" class="btn" style="float:right;margin-left:10px" value="close"><liferay-ui:message key="button-cancel" /></button>
						<button name="submitBtnModal" id="submitBtnModal" type="button" class="btn" style="float:right;" <%= ((isFormParamsVisible) ? "enabled" : "disabled") %>><liferay-ui:message key="button-execute" /></button>
					</div>
				</form>	
				<script>
					$("#submitBtnModal").on("click",function() {
						var isAllValidInputs = true;
						var portlet_Namespace = $('#portletNamespace').val();
						var numOfParams = $('#totalParams').val();
						for(var i = 0; i < numOfParams; i++) {
							var paramname = $('#' + portlet_Namespace + 'parameterName' + i).val();
							var paramvalue = $('#' + portlet_Namespace + 'parameterValue' + i).val();
							var checked = $('#checkItem' +i).val();
							if((paramname == "" || paramname == null) && (paramvalue == "" || paramvalue == null)) {
								continue;
							} else if(checked && paramvalue != "") {
								if (paramvalue.indexOf("\"") >= 0 || paramvalue.indexOf("\\") >= 0) {
									$("#loginParamValueInvalidError").hide();
									$("#unusableCharacterInputError").show();
									isAllValidInputs = false;
									break;
								}
								if (paramvalue.indexOf("&") >= 0
									|| paramvalue.indexOf("<") >= 0
									|| paramvalue.indexOf(">") >= 0
									|| paramvalue.indexOf("\'") >= 0) {
									$("#loginParamValueInvalidError").show();
									$("#unusableCharacterInputError").hide();
									isAllValidInputs = false;
									break;
								}
							}
						}
						
						if (isAllValidInputs){
							$("#loginParamValueInvalidError").hide();
							$("#unusableCharacterInputError").hide();
							var formURL = $("#_jp_ubsecure_portal_jubjub_portlet_VexPortlet_fmaddLoginForm").attr("action");
				       		var parentURL = $("#parentURL").attr("value").toString();
				         	$.ajax({
				            	url : formURL,
						        type: "POST",
						        data: new FormData($("#_jp_ubsecure_portal_jubjub_portlet_VexPortlet_fmaddLoginForm")[0]),
				               	processData: false,
				                contentType: false,
				                success:function(result) 
				              	{
				                	console.log('my message' + result);
				              		parent.$("#loginSettingContainer1").load(" #loginSettingContainer1 > *");
				              		parent.$("#loginSettingContainer2").load(" #loginSettingContainer2 > *:not(#button-holder-login)");
				                  	Liferay.Util.getOpener().close_get_login_form('open_get_login_form');
				              	},
				           		error: function() 
				              	{
				                	alert("Failed to execute in add/update log in information");      
				              	}
				          	});
						}
					});
				</script>
		</div>
		<aui:script use="liferay-util-window">
			A.one('#cancelBtnModal').on('click', function(event) {
				Liferay.Util.getOpener().close_get_login_form('open_get_login_form');
			});
		</aui:script>				
</div>
<%
	} else {
	 %>
		<div class="alert alert-danger">
			<liferay-ui:message key="message-no-access-rights" />
		</div>
		<%
	}
%>
<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/vex/register_scan_login_form.js"></script>
<script>
	define.amd = define._amd;
</script>
