/**
 * Vex
 */
var fromFilter = false;

$(document).ready(function() {
	var screenName = $("#screenName").val();
	
	if (screenName == "project_list") {
		var sortedColumn = $("#orderByCol").val();
		var sortType = $("#orderByType").val();
		
		$('.lfr-search-container ').find('thead').find('tr').each(function(index) {
			var tableColumn = 0;
			
			switch (sortedColumn) {
				case "projectId":
					tableColumn = 1;
					break;
				case "groupName":
					tableColumn = 2;
					break;
				case "projectName":
					tableColumn = 3;
					break;
				case "caseName":
					tableColumn = 4;
					break;
				case "projectEndDate":
					tableColumn = 5;
					break;
				case "targetUrl":
					tableColumn = 6;
					break;
				case "productionEnvironmentUrl":
					tableColumn = 7;
					break;
				case "status":
					tableColumn = 8;
					break;
				case "scanCount":
					tableColumn = 9;
					break;
			}
			
			$(document).find('th:nth-child(' + tableColumn + ')').addClass('table-sorted');
			
			if (sortType == "desc") {
				$(document).find('th:nth-child(' + tableColumn + ')').addClass('table-sorted-desc');
			}
		});
		
		$(function() {
			var focusedElement = null;
			$(document).on('focus', 'input', function() {
				if (focusedElement == $(this)) return;
				focusedElement = $(this);
				setTimeout(function() {
					focusedElement.select();
				}, 50);
			});
		});
		
		var jsFromSearch = sessionStorage.getItem("jsFromSearch");
		if (jsFromSearch != null && jsFromSearch == 'true') {
			sessionStorage.removeItem("jsFromSearch");
			if ($("#hasError").val() == 'false') {
				//downloadProjectList();
			}
		}
		
		// change background color of projects that exceed end date
		$('.table').removeClass('table-striped table-hover');
		
		$('.table td').mouseover(function () {
			var endDate = $(this).closest('tr').find('td:nth-child(5)').text();
			var prevBackgroundClass = 'default-background';
			var backgroundClass = 'default-background-hover';
			
			if (isProjectEndDateExceeded(endDate) == true && isProjectCompleted($(this).closest('tr').find('td:nth-child(6)').text()) == false) {
				prevBackgroundClass = 'end-project-background';
				backgroundClass = 'end-project-background-hover';
			}
			
			$(this).closest('tr').children('td').removeClass(prevBackgroundClass);
			$(this).closest('tr').children('td').addClass(backgroundClass);
		}).mouseout(function () {
			var endDate = $(this).closest('tr').find('td:nth-child(5)').text();
			var classNames = $(this).closest('tr').find('td:nth-child(8)').find('div').attr('class');
			var isOpen = false;
			
			if (classNames != null && classNames != '' && classNames != 'undefined') {
				classNames = classNames.split(' ');
				for (var index = 0; index < classNames.length; index++) {
					if (classNames[index] == 'open') {
						isOpen = true;
						break;
					}
				}
			}
			
			var prevBackgroundClass = 'default-background-hover';
			var backgroundClass = 'default-background';
			
			if (isProjectEndDateExceeded(endDate) == true && isProjectCompleted($(this).closest('tr').find('td:nth-child(6)').text()) == false) {
				if (isOpen == true) {
					prevBackgroundClass = 'end-project-background';
					backgroundClass = 'end-project-background-hover';
				} else {
					prevBackgroundClass = 'end-project-background-hover';
					backgroundClass = 'end-project-background';
				}
			} else {
				if (isOpen == true) {
					prevBackgroundClass = 'default-background';
					backgroundClass = 'default-background-hover';
				} else {
					prevBackgroundClass = 'default-background-hover';
					backgroundClass = 'default-background';
				}
			}
			
			$(this).closest('tr').children('td').removeClass(prevBackgroundClass);
			$(this).closest('tr').children('td').addClass(backgroundClass);
		});
	}
});

function confirmDeleteProject (projectId) {
	var msg = "選択したプロジェクトを削除しますか。";
	var strProjectId = "";
	
	if (projectId < 10) {
		strProjectId = "0000" + projectId;
	} else if (projectId < 100) {
		strProjectId = "000" + projectId;
	} else if (projectId < 1000) {
		strProjectId = "00" + projectId;
	} else if (projectId < 10000) {
		strProjectId = "0" + projectId;
	} else {
		strProjectId = "" + projectId;
	}
	
	$("tr td:contains('"+ strProjectId +"')").each(function(){
		var endDate = $(this).closest('tr').find("td:nth-child(5)").text();
		
		if (isProjectEndDateExceeded(endDate) == true && isProjectCompleted($(this).closest('tr').find('td:nth-child(6)').text()) == false) {
			$(this).closest('tr').children('td').removeClass('end-project-background');
			$(this).closest('tr').children('td').addClass('end-project-background-hover');
		} else {
			$(this).closest('tr').children('td').removeClass('default-background');
			$(this).closest('tr').children('td').addClass('default-background-hover');
		}
	});
	
	if (confirm(msg)) {
		$.ajax({
			url: $("#deleteURL_" + projectId).val(),
			async: false,
			success: function (data) {
				location.reload();
			}
		});
	} else {
		$("tr td:contains('"+ strProjectId +"')").each(function(){
			var endDate = $(this).closest('tr').find("td:nth-child(5)").text();
			
			if (isProjectEndDateExceeded(endDate) == true && isProjectCompleted($(this).closest('tr').find('td:nth-child(6)').text()) == false) {
				$(this).closest('tr').children('td').addClass('end-project-background');
				$(this).closest('tr').children('td').removeClass('end-project-background-hover');
			} else {
				$(this).closest('tr').children('td').addClass('default-background');
				$(this).closest('tr').children('td').removeClass('default-background-hover');
			}
		});
	}
	
	$('.table td').mouseover(function () {
		var endDate = $(this).closest('tr').find('td:nth-child(5)').text();
		var backgroundClass = 'default-background-hover';
		var prevBackgroundClass = 'default-background';
		
		if (isProjectEndDateExceeded(endDate) == true && isProjectCompleted($(this).closest('tr').find('td:nth-child(6)').text()) == false) {
			prevBackgroundClass = 'end-project-background';
			backgroundClass = 'end-project-background-hover';
		}
		
		$(this).closest('tr').children('td').removeClass(prevBackgroundClass);
		$(this).closest('tr').children('td').addClass(backgroundClass);
	});
}

function downloadSummary (url) {
	var namespace = $("#portletNamespace").val();
	var startDate = $("#" + namespace + "startDate").val();
	var endDate = $("#" + namespace + "endDate").val();
	
	$("#downloadSummaryModal").modal("hide");
	
	$.fileDownload(url, {
		data: {startDate : startDate,
			endDate : endDate},
		successCallback: function (url) {
			$("button[data-dismiss=modal].btn")[1].click();
		},
		failCallback: function (html, url) {
			location.reload();
		}
	});
}

function searchProjects() {
	sessionStorage.setItem("jsFromSearch", true);
	document.getElementById("filterForm").submit();
}

function downloadProjectList () {
	var download = $("#downloadProjectListURL").val();
	var namespace = $("#portletNamespace").val();
	var projectId = $("#" + namespace + "projectId").val();
	var ownerGroup = $("#" + namespace + "ownerGroup").val();
	var caseNumber = $("#" + namespace + "caseNumber").val();
	var projectName = $("#" + namespace + "projectName").val();
	var projectEndDateLow = $("#" + namespace + "projectEndDateLow").val();
	var projectEndDateHigh = $("#" + namespace + "projectEndDateHigh").val();
	var status = $("#" + namespace + "status").val();
	var scanCount = $("#" + namespace + "scanCount").val();
	
	if (download != null) {
		$.fileDownload(download, {
			data: {projectId : projectId,
				ownerGroup : ownerGroup,
				caseNumber : caseNumber,
				projectName : projectName,
				projectEndDateLow : projectEndDateLow,
				projectEndDateHigh : projectEndDateHigh,
				status : status,
				scanCount : scanCount}})
		.done(function() {
		})
		.fail(function() {
			location.reload();
		});
	}
}

function isProjectEndDateExceeded (endDate) {
	var exceeded = false;
	
	if (endDate != null && endDate != '' && endDate != 'undefined') {
		var today = new Date();
		today = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
		
		var endYear = endDate.split("/")[0];
		var endMonth = endDate.split("/")[1] - 1;
		var endDay = endDate.split("/")[2];
		
		endDate = new Date(endYear, endMonth, endDay, 0, 0, 0);
		
		if (endDate < today) {
			exceeded = true;
		}
	}
	
	return exceeded;
}

function isProjectCompleted (status) {
	var complete = false;
	
	if (status != null && status != '' && status != 'undefined') {
		if (status == '完了' || status == ' 完了 ') {
			complete = true;
		}
	}
	
	return complete;
}