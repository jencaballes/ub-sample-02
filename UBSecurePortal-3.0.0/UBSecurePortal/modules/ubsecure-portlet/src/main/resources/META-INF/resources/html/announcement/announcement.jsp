<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.PortalUtil" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<portlet:defineObjects />

<%
	String strAnnouncement = (String) renderRequest.getAttribute(PortalConstants.PARAM_ANNOUNCEMENT);
	Boolean bIsMaximized = (Boolean) renderRequest.getAttribute(PortalConstants.PARAM_IS_MAXIMIZED);
	String maximizedUrl = (String) renderRequest.getAttribute(PortalConstants.PARAM_MAXIMIZED_URL);
	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(renderRequest);
	HttpServletRequest origRequest = PortalUtil.getOriginalServletRequest(request);
	HttpSession httpSession = origRequest.getSession(false);
	httpSession.setAttribute("Current_screen", "announcement");
	Object downloadFrom = httpSession.getAttribute("download_from");
	Object oDownloadManualError = httpSession.getAttribute(PortalConstants.ERROR);
	String strError = PortalConstants.STRING_EMPTY;
	String strDownloadFrom = PortalConstants.STRING_EMPTY;
	Object oLoggedIn = httpSession.getAttribute(PortalConstants.LOGGED_IN);
	boolean bLoggedIn = false;
	
	if (!CommonUtil.isObjectNull(oLoggedIn)) {
		bLoggedIn = Boolean.parseBoolean(oLoggedIn.toString());
	}
	
	if (!CommonUtil.isObjectNull(oDownloadManualError)) {
		strError = oDownloadManualError.toString();
	}
	
	if (!CommonUtil.isObjectNull(downloadFrom)){
		strDownloadFrom = downloadFrom.toString();
	}
%>
<div> 
	<%
		if (!CommonUtil.isStringNullOrEmpty(strError) &&!bIsMaximized && bLoggedIn && strDownloadFrom.equals("announcement")) {
			if (strError.equalsIgnoreCase(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL)) {
				%>
				<div class="alert alert-danger">
					<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
				</div>
				<%
			} else {
				%>
				<div class="alert alert-danger">
					<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
				</div>
				<%
			}
			httpSession.removeAttribute(PortalConstants.ERROR);
		}
	%>
	<%= strAnnouncement %>
</div>

<script type="text/javascript">
	var maximizedFlag = <%= bIsMaximized %>;
	
	if (maximizedFlag) {
		location.href = '<%= maximizedUrl %>';
	}
</script>
