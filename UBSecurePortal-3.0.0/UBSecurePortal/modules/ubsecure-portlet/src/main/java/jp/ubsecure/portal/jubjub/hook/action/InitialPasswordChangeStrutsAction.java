package jp.ubsecure.portal.jubjub.hook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.struts.StrutsAction;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_PATH + PortalConstants.PATH_UPDATE_PASSWORD,
	},
	service = StrutsAction.class
)
public class InitialPasswordChangeStrutsAction extends BaseStrutsAction {
	
	private static long userId;
	
	public String execute (StrutsAction originalStrutsAction, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String strReturn = PortalConstants.STRING_EMPTY;
		HttpSession httpSession = null;
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			strReturn = originalStrutsAction.execute(request, response);
			
			httpSession = request.getSession(false);
			oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());

				oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				if (CommonUtil.isStringNullOrEmpty(strReturn) && userId != lUserId && (iUserRole == UserRole.GEN_USER.getInteger()|| iUserRole == UserRole.GROUP_ADMIN.getInteger()) ) {
					MailUtil.sendEmail(PortalConstants.EMAIL_EVENT_PASSWORD_CHANGE, lUserId, null);
					userId = lUserId;
				}
			}
		} catch (Exception e) {
			throw e;
		}
		
		return strReturn;
	}
}
