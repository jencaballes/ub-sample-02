package jp.ubsecure.portal.jubjub.portlet.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.security.auth.EmailAddressValidator;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
		immediate = true,
		property = {
			PortalConstants.KEY_CONTEXT_ID + "CustomEmailAddressValidator",
			PortalConstants.KEY_CONTEXT_NAME + "UBSecure Custom Email Address Validator",
			PortalConstants.KEY_SERVICE_RANKING + PortalConstants.SERVICE_RANKING_100
		}
	)
public class CustomEmailAddressValidator implements EmailAddressValidator {

	private static final Pattern _emailAddressPattern = Pattern.compile("[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?");
	
	@Override
	public boolean validate(long companyId, String emailAddress) {
		if ((!isEmailAddress(emailAddress)) || (emailAddress.startsWith("postmaster@"))
				|| (emailAddress.startsWith("root@"))) {
			return false;
		}
		return true;
	}
	
	public boolean isEmailAddress(String emailAddress) {
		if (Validator.isNull(emailAddress)) {
			return false;
		}

		Matcher matcher = _emailAddressPattern.matcher(emailAddress);

		return matcher.matches();
	}

}
