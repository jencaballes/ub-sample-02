/**
 * ProjectSettings.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class ProjectSettings  implements java.io.Serializable {
    private long projectID;

    private java.lang.String projectName;

    private long presetID;

    private long taskId;

    private java.lang.String associatedGroupID;

    private long scanConfigurationID;

    private java.lang.String description;

    private java.lang.String owner;

    private boolean isPublic;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSharedLocation openSourceSettings;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectOrigin openSourceAnalysisOrigin;

    public ProjectSettings() {
    }

    public ProjectSettings(
           long projectID,
           java.lang.String projectName,
           long presetID,
           long taskId,
           java.lang.String associatedGroupID,
           long scanConfigurationID,
           java.lang.String description,
           java.lang.String owner,
           boolean isPublic,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSharedLocation openSourceSettings,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectOrigin openSourceAnalysisOrigin) {
           this.projectID = projectID;
           this.projectName = projectName;
           this.presetID = presetID;
           this.taskId = taskId;
           this.associatedGroupID = associatedGroupID;
           this.scanConfigurationID = scanConfigurationID;
           this.description = description;
           this.owner = owner;
           this.isPublic = isPublic;
           this.openSourceSettings = openSourceSettings;
           this.openSourceAnalysisOrigin = openSourceAnalysisOrigin;
    }


    /**
     * Gets the projectID value for this ProjectSettings.
     * 
     * @return projectID
     */
    public long getProjectID() {
        return projectID;
    }


    /**
     * Sets the projectID value for this ProjectSettings.
     * 
     * @param projectID
     */
    public void setProjectID(long projectID) {
        this.projectID = projectID;
    }


    /**
     * Gets the projectName value for this ProjectSettings.
     * 
     * @return projectName
     */
    public java.lang.String getProjectName() {
        return projectName;
    }


    /**
     * Sets the projectName value for this ProjectSettings.
     * 
     * @param projectName
     */
    public void setProjectName(java.lang.String projectName) {
        this.projectName = projectName;
    }


    /**
     * Gets the presetID value for this ProjectSettings.
     * 
     * @return presetID
     */
    public long getPresetID() {
        return presetID;
    }


    /**
     * Sets the presetID value for this ProjectSettings.
     * 
     * @param presetID
     */
    public void setPresetID(long presetID) {
        this.presetID = presetID;
    }


    /**
     * Gets the taskId value for this ProjectSettings.
     * 
     * @return taskId
     */
    public long getTaskId() {
        return taskId;
    }


    /**
     * Sets the taskId value for this ProjectSettings.
     * 
     * @param taskId
     */
    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }


    /**
     * Gets the associatedGroupID value for this ProjectSettings.
     * 
     * @return associatedGroupID
     */
    public java.lang.String getAssociatedGroupID() {
        return associatedGroupID;
    }


    /**
     * Sets the associatedGroupID value for this ProjectSettings.
     * 
     * @param associatedGroupID
     */
    public void setAssociatedGroupID(java.lang.String associatedGroupID) {
        this.associatedGroupID = associatedGroupID;
    }


    /**
     * Gets the scanConfigurationID value for this ProjectSettings.
     * 
     * @return scanConfigurationID
     */
    public long getScanConfigurationID() {
        return scanConfigurationID;
    }


    /**
     * Sets the scanConfigurationID value for this ProjectSettings.
     * 
     * @param scanConfigurationID
     */
    public void setScanConfigurationID(long scanConfigurationID) {
        this.scanConfigurationID = scanConfigurationID;
    }


    /**
     * Gets the description value for this ProjectSettings.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this ProjectSettings.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the owner value for this ProjectSettings.
     * 
     * @return owner
     */
    public java.lang.String getOwner() {
        return owner;
    }


    /**
     * Sets the owner value for this ProjectSettings.
     * 
     * @param owner
     */
    public void setOwner(java.lang.String owner) {
        this.owner = owner;
    }


    /**
     * Gets the isPublic value for this ProjectSettings.
     * 
     * @return isPublic
     */
    public boolean isIsPublic() {
        return isPublic;
    }


    /**
     * Sets the isPublic value for this ProjectSettings.
     * 
     * @param isPublic
     */
    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }


    /**
     * Gets the openSourceSettings value for this ProjectSettings.
     * 
     * @return openSourceSettings
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSharedLocation getOpenSourceSettings() {
        return openSourceSettings;
    }


    /**
     * Sets the openSourceSettings value for this ProjectSettings.
     * 
     * @param openSourceSettings
     */
    public void setOpenSourceSettings(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSharedLocation openSourceSettings) {
        this.openSourceSettings = openSourceSettings;
    }


    /**
     * Gets the openSourceAnalysisOrigin value for this ProjectSettings.
     * 
     * @return openSourceAnalysisOrigin
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectOrigin getOpenSourceAnalysisOrigin() {
        return openSourceAnalysisOrigin;
    }


    /**
     * Sets the openSourceAnalysisOrigin value for this ProjectSettings.
     * 
     * @param openSourceAnalysisOrigin
     */
    public void setOpenSourceAnalysisOrigin(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectOrigin openSourceAnalysisOrigin) {
        this.openSourceAnalysisOrigin = openSourceAnalysisOrigin;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProjectSettings)) return false;
        ProjectSettings other = (ProjectSettings) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.projectID == other.getProjectID() &&
            ((this.projectName==null && other.getProjectName()==null) || 
             (this.projectName!=null &&
              this.projectName.equals(other.getProjectName()))) &&
            this.presetID == other.getPresetID() &&
            this.taskId == other.getTaskId() &&
            ((this.associatedGroupID==null && other.getAssociatedGroupID()==null) || 
             (this.associatedGroupID!=null &&
              this.associatedGroupID.equals(other.getAssociatedGroupID()))) &&
            this.scanConfigurationID == other.getScanConfigurationID() &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.owner==null && other.getOwner()==null) || 
             (this.owner!=null &&
              this.owner.equals(other.getOwner()))) &&
            this.isPublic == other.isIsPublic() &&
            ((this.openSourceSettings==null && other.getOpenSourceSettings()==null) || 
             (this.openSourceSettings!=null &&
              this.openSourceSettings.equals(other.getOpenSourceSettings()))) &&
            ((this.openSourceAnalysisOrigin==null && other.getOpenSourceAnalysisOrigin()==null) || 
             (this.openSourceAnalysisOrigin!=null &&
              this.openSourceAnalysisOrigin.equals(other.getOpenSourceAnalysisOrigin())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getProjectID()).hashCode();
        if (getProjectName() != null) {
            _hashCode += getProjectName().hashCode();
        }
        _hashCode += new Long(getPresetID()).hashCode();
        _hashCode += new Long(getTaskId()).hashCode();
        if (getAssociatedGroupID() != null) {
            _hashCode += getAssociatedGroupID().hashCode();
        }
        _hashCode += new Long(getScanConfigurationID()).hashCode();
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getOwner() != null) {
            _hashCode += getOwner().hashCode();
        }
        _hashCode += (isIsPublic() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getOpenSourceSettings() != null) {
            _hashCode += getOpenSourceSettings().hashCode();
        }
        if (getOpenSourceAnalysisOrigin() != null) {
            _hashCode += getOpenSourceAnalysisOrigin().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProjectSettings.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ProjectSettings"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("projectID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "projectID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("projectName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ProjectName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("presetID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "PresetID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "TaskId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("associatedGroupID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "AssociatedGroupID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scanConfigurationID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScanConfigurationID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("owner");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "Owner"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPublic");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "IsPublic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openSourceSettings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "OpenSourceSettings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ProjectSharedLocation"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("openSourceAnalysisOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "OpenSourceAnalysisOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ProjectOrigin"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
