package jp.ubsecure.portal.jubjub.portlet.service.android;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.util.FileUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.snark.ProjectManager;
import jp.ubsecure.portal.snark.ScanConfig;
import jp.ubsecure.portal.snark.SnarkException;

/**
 * @since  12/9/14
 */
public class AndroidProjectManager {
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(AndroidProjectManager.class);
	
	public static long createAndroidProject(String strProjectName, File checklistFile, List<String> lstExcludedPackageNameList) throws UBSPortalException {
		byte[] tChecklist = null;
		Map<String, String> packageNames = null;
		Collection<String> values = null;
		Map<String, Object> params = new HashMap<String, Object>();
		long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
		
		try {
			
			if (CommonUtil.isStringNullOrEmpty(strProjectName)) {
				params.put("strProjectName", strProjectName);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
				throw new UBSPortalException(PortalErrors.API_INVALID_PROJECT_NAME);
			}				
			
			if (checklistFile != null && !CommonUtil.isValidFile(checklistFile)) {
				params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);
			}			
			
			lCxAndroidProjectId = ProjectManager.createProject(strProjectName);			
			if (lCxAndroidProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID);
			}	
			
			if (CommonUtil.isListNullOrEmpty(lstExcludedPackageNameList)) {
				packageNames = ConfigurationFileParser.getConfig(ConfigurationFileParser.DEFAULT_EXCLUDED_PACKAGE_NAMES);
				if (CommonUtil.isMapNullOrEmpty(packageNames)) {
					params.put("packageNames", packageNames.values());
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
					throw new UBSPortalException(PortalErrors.CONFIG_VALUE_ERROR);
				}
				
				lstExcludedPackageNameList = new ArrayList<String>();
				values = packageNames.values();				
				for (String strValue : values) {
					lstExcludedPackageNameList.add(strValue);
				}
			}			
			if (checklistFile != null) {
				tChecklist = FileProcessUtil.parseAndroidChecklist(checklistFile);			
				if (!CommonUtil.isValidBytes(tChecklist)) {
					params.put("tChecklist", CommonUtil.isValidBytes(tChecklist));
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CHECKLIST));
					throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR);
				}	
			}
			
			ProjectManager.setScanConfig(lCxAndroidProjectId, tChecklist, lstExcludedPackageNameList);
			
		} catch (UBSPortalException e) {
			e.setErrorMessage(PortalMessages.ANDO_CREATE_PROJECT_FAILED);
			params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_ANDROID_PROJECT, params, e);
			throw e;
		} catch (SnarkException e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("strProjectName", strProjectName);
			params.put("tChecklist", CommonUtil.isValidBytes(tChecklist));
			params.put("lstExcludedPackageNameList", lstExcludedPackageNameList);
			
			log.debug(PortalErrors.ANDROID_SERVER_ERROR, PortalConstants.METHOD_CREATE_ANDROID_PROJECT, params, e);
			throw new UBSPortalException(PortalErrors.ANDROID_SERVER_ERROR, PortalMessages.ANDO_CREATE_PROJECT_FAILED, e);	
		} catch (Exception e) {
			params.put("strProjectName", strProjectName);
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
			params.put("lstExcludedPackageNameList", lstExcludedPackageNameList);
			params.put("packageNames", packageNames);
			params.put("tChecklist valid", CommonUtil.isValidBytes(tChecklist));
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_CREATE_ANDROID_PROJECT, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.ANDO_CREATE_PROJECT_FAILED, e);
		} finally {
			tChecklist = null;	
			packageNames = null;
			values = null;
			lstExcludedPackageNameList = null;
			params.clear();
			params = null;
		}
		
		return lCxAndroidProjectId;
	}
	
	public static void updateAndroidProjectName(String strProjectName, File checklistFile, List<String> lstExcludedPackageNameList, long lCxAndroidProjectId) throws UBSPortalException {
		byte[] tChecklist = null;
		Map<String, String> packageNames = null;
		Collection<String> values = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (checklistFile != null && !CommonUtil.isValidFile(checklistFile)) {
				params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CHECKLIST));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);
			}
			
			if (CommonUtil.isStringNullOrEmpty(strProjectName)) {
				params.put("strProjectName", strProjectName);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_NAME);
			}
			
			if (lCxAndroidProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID);
			}
				
			ProjectManager.editProject(lCxAndroidProjectId, strProjectName);
			
			if(CommonUtil.isListNullOrEmpty(lstExcludedPackageNameList)) {
				packageNames = ConfigurationFileParser.getConfig(ConfigurationFileParser.DEFAULT_EXCLUDED_PACKAGE_NAMES);
				if (CommonUtil.isMapNullOrEmpty(packageNames)) {
					params.put("packageNames", packageNames.values());
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
					throw new UBSPortalException(PortalErrors.CONFIG_VALUE_ERROR);
				}
				
				lstExcludedPackageNameList = new ArrayList<String>();
				values = packageNames.values();
				for (String strValue : values) {
					lstExcludedPackageNameList.add(strValue);
				}
			}
			if (checklistFile != null) {
				tChecklist = FileProcessUtil.parseAndroidChecklist(checklistFile);			
				if (!CommonUtil.isValidBytes(tChecklist)) {
					params.put("tChecklist valid", CommonUtil.isValidBytes(tChecklist));
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CHECKLIST));
					throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR);
				}		
			}

			ProjectManager.setScanConfig(lCxAndroidProjectId, tChecklist, lstExcludedPackageNameList);
			
		} catch (UBSPortalException e) {
			e.setErrorMessage(PortalMessages.ANDO_UPDATE_PROJECT_FAILED);
			params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_UPDATE_ANDROID_PROJECT_NAME, params, e);
			throw e;
		} catch (SnarkException e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("strProjectName", strProjectName);
			params.put("tChecklist valid", CommonUtil.isValidBytes(tChecklist));
			params.put("lstExcludedPackageNameList", lstExcludedPackageNameList);
			
			log.debug(PortalErrors.ANDROID_SERVER_ERROR, PortalConstants.METHOD_UPDATE_ANDROID_PROJECT_NAME, params, e);
			throw new UBSPortalException(PortalErrors.ANDROID_SERVER_ERROR, PortalMessages.ANDO_UPDATE_PROJECT_FAILED, e);	
		} catch (Exception e) {
			params.put("strProjectName", strProjectName);
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
			params.put("lstExcludedPackageNameList", lstExcludedPackageNameList);
			params.put("packageNames", packageNames);
			params.put("tChecklist valid", CommonUtil.isValidBytes(tChecklist));
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_UPDATE_ANDROID_PROJECT_NAME, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.ANDO_UPDATE_PROJECT_FAILED, e);
		} finally {
			tChecklist = null;	
			packageNames = null;
			values = null;
			lstExcludedPackageNameList = null;
			params.clear();
			params = null;
		}
	}
	
	public static void deleteAndroidProject(long lCxAndroidProjectId, long lProjectId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lCxAndroidProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID);
			}
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lProjectId", lProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			
			try {
				ProjectManager.deleteProject(lCxAndroidProjectId);
			} catch (SnarkException e) {
				if (e.getErrorCode().equalsIgnoreCase(PortalErrors.ANDROID_DELETE_NON_EXISTING_PROJECT)) {
					params.put("lCxAndroidProjectId", lCxAndroidProjectId);	
					log.debug(PortalErrors.ANDROID_DELETE_NON_EXISTING_PROJECT, PortalConstants.METHOD_DELETE_ANDROID_PROJECT, params, e);
				} else {
					throw e;
				}
			}
			
			File projectDirectory = new File(FileProcessUtil.locateProjectDirectory(lProjectId));
			if (projectDirectory.exists()) {
				FileUtil.deltree(projectDirectory);
			}

			FileProcessUtil.deleteReportFilesByProjectId(lProjectId);
		} catch (SnarkException e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			
			log.debug(PortalErrors.ANDROID_SERVER_ERROR, PortalConstants.METHOD_DELETE_ANDROID_PROJECT, params, e);
			throw new UBSPortalException(PortalErrors.ANDROID_SERVER_ERROR, PortalMessages.ANDO_DELETE_PROJECT_FAILED, e);		
		} catch (UBSPortalException e) {
			e.setErrorMessage(PortalMessages.ANDO_DELETE_PROJECT_FAILED);
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_DELETE_ANDROID_PROJECT, params, e);
			throw e;		
		} finally {
			params.clear();
			params = null;
		}
	}
	
	public static List<String> getExcludedPackageNames(long lCxAndroidProjectId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<String> lstPackages = new ArrayList<String>();
		ScanConfig scanConfig = null;
		
		try {
			
			if (lCxAndroidProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID);
			}			
			
			scanConfig = ProjectManager.getScanConfig(lCxAndroidProjectId);
			
			if (CommonUtil.isObjectNull(scanConfig)) {
				params.put("scanConfig", scanConfig);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_CONFIG));
				throw new UBSPortalException(PortalErrors.API_INVALID_SCAN_CONFIG);
			}
			
			lstPackages = scanConfig.getExcludePackageNameList();
			
		} catch (UBSPortalException e) {
			e.setErrorMessage(PortalMessages.ANDO_GET_PACKAGE_NAMES_FAILED);
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_EXCLUDED_PACKAGE_NAMES, params, e);
			throw e;	
		} catch (SnarkException e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			
			log.debug(PortalErrors.ANDROID_SERVER_ERROR, PortalConstants.METHOD_GET_EXCLUDED_PACKAGE_NAMES, params, e);
			throw new UBSPortalException(PortalErrors.ANDROID_SERVER_ERROR, PortalMessages.ANDO_GET_PACKAGE_NAMES_FAILED, e);		
		} catch (Exception e) {
			params.put("ConfigurationFileParser.DEFAULT_EXCLUDED_PACKAGE_NAMES", ConfigurationFileParser.DEFAULT_EXCLUDED_PACKAGE_NAMES.toArray());
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("scanConfig", scanConfig);
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_EXCLUDED_PACKAGE_NAMES, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.ANDO_GET_PACKAGE_NAMES_FAILED, e);
		} finally {			
			scanConfig = null;				
			params.clear();
			params = null;
		}
		
		return lstPackages;
	}
}