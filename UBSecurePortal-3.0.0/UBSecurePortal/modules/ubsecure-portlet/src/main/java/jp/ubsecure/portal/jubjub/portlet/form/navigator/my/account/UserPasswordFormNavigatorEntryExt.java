package jp.ubsecure.portal.jubjub.portlet.form.navigator.my.account;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.users.admin.web.servlet.taglib.ui.UserPasswordFormNavigatorEntry;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_FORM_NAVIGATOR_ENTRY_ORDER + PortalConstants.ENTRY_ORDER_USER_PASSWORD
	},
	service = FormNavigatorEntry.class
)
public class UserPasswordFormNavigatorEntryExt extends UserPasswordFormNavigatorEntry {
	@Override
	public String getLabel(Locale locale) {
		return LanguageUtil.get(locale, PortalConstants.KEY_LABEL_PASSWORD);
	}
	
	@Override
	public String getFormNavigatorId() {
		return PortalConstants.MY_ACCOUNT_PREFIX + super.getFormNavigatorId();
	}

	@Override
	public boolean isVisible(User user, User selUser) {
		boolean visible = GetterUtil.getBoolean(PropsUtil.get(PortalConstants.MY_ACCOUNT_PASSWORD_VISIBLE), true);
		return visible && super.isVisible(user, selUser);
	}
}