package jp.ubsecure.portal.jubjub.hook.action;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.DuplicateOrganizationException;
import com.liferay.portal.kernel.exception.NoSuchOrganizationException;
import com.liferay.portal.kernel.exception.OrganizationNameException;
import com.liferay.portal.kernel.exception.RequiredOrganizationException;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.Encrypter;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_JAVAX_PORTLET_NAME + PortalConstants.PORTLET_NAME_USERS_ADMIN,
		PortalConstants.KEY_MVC_COMMAND_NAME + PortalConstants.COMMAND_NAME_EDIT_ORGANIZATION,
		PortalConstants.KEY_SERVICE_RANKING + PortalConstants.SERVICE_RANKING_100
	},
	service = MVCActionCommand.class
)
public class GroupMgmtStrutsAction extends BaseMVCActionCommand {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(GroupMgmtStrutsAction.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String cmd = ParamUtil.getString(actionRequest, Constants.CMD);
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		HttpSession session = request.getSession(false);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		String redirect = null;
		Long lUserId = PortalConstants.LONG_ZERO;
		PortletSession portletSession = actionRequest.getPortletSession(false);
		SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
		
		try {
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.valueOf(oUserId.toString());
			}
			
			if (cmd.equals(Constants.ADD) || cmd.equals(Constants.UPDATE)) {
				StrutsActionManager.addUpdateGroup(actionRequest);
				
				if (cmd.equals(Constants.ADD)) {
					log.info(PortalMessages.GROUP_EVENT_ADD_GROUP, lUserId);
					SessionMessages.add(actionRequest, PortalConstants.SUCCESS_ADD);
				} else {
					log.info(PortalMessages.GROUP_EVENT_EDIT_GROUP, lUserId);
					SessionMessages.add(actionRequest, PortalConstants.SUCCESS_EDIT);
				}
				
				portletSession.removeAttribute(PortalConstants.NAME, PortletSession.PORTLET_SCOPE);
				portletSession.removeAttribute(PortalConstants.CXSUITE, PortletSession.PORTLET_SCOPE);
				portletSession.removeAttribute(PortalConstants.ANDROID, PortletSession.PORTLET_SCOPE);
				portletSession.removeAttribute(PortalConstants.IOS, PortletSession.PORTLET_SCOPE);
				portletSession.removeAttribute(PortalConstants.VEX, PortletSession.PORTLET_SCOPE);
			} else if (cmd.equals(Constants.DELETE)) {
				log.info(PortalMessages.GROUP_EVENT_DELETE_GROUP, lUserId);
				
				StrutsActionManager.deleteGroup(actionRequest);
				SessionMessages.add(actionRequest, PortalConstants.SUCCESS_DELETE);
			} else if (cmd.equals(Constants.SEARCH)) {
				log.info(PortalMessages.GROUP_EVENT_SEARCH_GROUP, lUserId);
				
				List<Organization> orgLists = StrutsActionManager.searchGroup(actionRequest);
				portletSession.setAttribute(PortalConstants.ORG_LISTS, orgLists, PortletSession.PORTLET_SCOPE);
				
			} else if (cmd.equals(PortalConstants.CLEAR_SEARCH)) {
				log.info(PortalMessages.GROUP_EVENT_CLEAR_SEARCH_GROUP, lUserId);
				List<Organization> orgList = StrutsActionManager.clearSearchGroup(actionRequest);
				
				portletSession.setAttribute(PortalConstants.ORG_LISTS, orgList, PortletSession.PORTLET_SCOPE);
				
				portletSession.removeAttribute("searchedGroup", PortletSession.PORTLET_SCOPE);
				session.removeAttribute("searchedGroup");
			}
			
			if (!cmd.equals(Constants.SEARCH)) { 
				redirect = ParamUtil.getString(actionRequest, PortalConstants.BACK_URL);
				
				HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession(false);
				Key key = (Key) httpSession.getAttribute("key");
				boolean bIsEncrypted = false;
				
				try {
					bIsEncrypted = Encrypter.isEncrypted(redirect, key);
				} catch (Exception e) {
					if (e instanceof IllegalBlockSizeException ||
							e instanceof BadPaddingException) {
						bIsEncrypted = false;
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
					httpSession.removeAttribute("key");
					
					redirect = Encrypter.decrypt(redirect, key);
				}
				
				String [] redirectContent = redirect.split(PortalConstants.DELIMITER_125);
				
				for (int i = 0; i < redirectContent.length; i++) {
					if (!redirectContent[i].contains(PortalConstants.USERS_SEARCH_CONTAINER_PK) &&
							!redirectContent[i].contains("groupName")) {
						if (i == 0) {
							redirect = redirectContent[i];
						} else {
							redirect += PortalConstants.DELIMITER_125 + redirectContent[i];
						}
					}
				}
				
				sendRedirect(actionRequest, actionResponse, redirect);
			}
		} catch (Exception e) {	
			session.removeAttribute("searchedGroup");
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			
			if (e instanceof DuplicateOrganizationException
					|| e instanceof OrganizationNameException
					|| e instanceof RequiredOrganizationException) {

				if (e instanceof RequiredOrganizationException) {
					SessionErrors.add(actionRequest, e.getClass());
				} else if (e instanceof OrganizationNameException){
					SessionErrors.add(actionRequest, PortalErrors.ADD_GROUP_EXCEEDS_BYTE_SIZE);
				} else if (e instanceof DuplicateOrganizationException) {
					SessionErrors.add(actionRequest, PortalErrors.ADD_GROUP_ALREADY_EXISTS);
				}
				
				
				if (e instanceof OrganizationNameException && cmd.equals(Constants.SEARCH)) {
					List<Organization> orgLists = new ArrayList<Organization>();
					portletSession.setAttribute(PortalConstants.ORG_LISTS, orgLists, PortletSession.PORTLET_SCOPE);
				}

				redirect = PortalUtil.escapeRedirect(ParamUtil
						.getString(actionRequest, PortalConstants.REDIRECT));
				
				HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession(false);
				Key key = (Key) httpSession.getAttribute("key");
				boolean bIsEncrypted = false;
				
				try {
					bIsEncrypted = Encrypter.isEncrypted(redirect, key);
				} catch (Exception ex) {
					if (ex instanceof IllegalBlockSizeException ||
							ex instanceof BadPaddingException) {
						bIsEncrypted = false;
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
					httpSession.removeAttribute("key");
					
					redirect = Encrypter.decrypt(redirect, key);
				}

				long organizationId = ParamUtil.getLong(actionRequest,
						PortalConstants.ORGANIZATION_ID);

				if (organizationId > 0) {
					redirect = HttpUtil.setParameter(redirect,
							actionResponse.getNamespace()
									+ PortalConstants.ORGANIZATION_ID, organizationId);
				}

				if (Validator.isNotNull(redirect)) {
					actionResponse.sendRedirect(redirect);
				}
			} else if (e instanceof UBSPortalException) {
				if (((UBSPortalException) e).getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					SessionErrors.add(actionRequest, PortalConstants.ORM_EXCEPTION);
					
					if (cmd.equals(Constants.SEARCH) || cmd.equals(Constants.DELETE)) {
						List<Organization> orgLists = new ArrayList<Organization>();
						portletSession.setAttribute(PortalConstants.ORG_LISTS, orgLists, PortletSession.PORTLET_SCOPE);
					
						redirect = PortalUtil.escapeRedirect(ParamUtil
								.getString(actionRequest, PortalConstants.REDIRECT));
						
						HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession(false);
						Key key = (Key) httpSession.getAttribute("key");
						boolean bIsEncrypted = false;
						
						try {
							bIsEncrypted = Encrypter.isEncrypted(redirect, key);
						} catch (Exception ex) {
							if (ex instanceof IllegalBlockSizeException ||
									ex instanceof BadPaddingException) {
								bIsEncrypted = false;
							}
						}
						
						if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
							httpSession.removeAttribute("key");
							
							redirect = Encrypter.decrypt(redirect, key);
						}

						long organizationId = ParamUtil.getLong(actionRequest,
								PortalConstants.ORGANIZATION_ID);

						if (organizationId > 0) {
							redirect = HttpUtil.setParameter(redirect,
									actionResponse.getNamespace()
											+ PortalConstants.ORGANIZATION_ID, organizationId);
						}

						if (Validator.isNotNull(redirect)) {
							actionResponse.sendRedirect(redirect);
						}
					}
				} else if (((UBSPortalException) e).getErrorCode().equals(PortalErrors.SEARCH_GROUP_INVALID_NO_OF_USERS)) {
					SessionErrors.add(actionRequest, PortalMessages.NO_OF_USERS_INVALID);
					
					if (cmd.equals(Constants.SEARCH) || cmd.equals(Constants.DELETE)) {
						List<Organization> orgLists = new ArrayList<Organization>();
						portletSession.setAttribute(PortalConstants.ORG_LISTS, orgLists, PortletSession.PORTLET_SCOPE);
					
						redirect = PortalUtil.escapeRedirect(ParamUtil
								.getString(actionRequest, PortalConstants.REDIRECT));
						
						HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession(false);
						Key key = (Key) httpSession.getAttribute("key");
						boolean bIsEncrypted = false;
						
						try {
							bIsEncrypted = Encrypter.isEncrypted(redirect, key);
						} catch (Exception ex) {
							if (ex instanceof IllegalBlockSizeException ||
									ex instanceof BadPaddingException) {
								bIsEncrypted = false;
							}
						}
						
						if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
							httpSession.removeAttribute("key");
							
							redirect = Encrypter.decrypt(redirect, key);
						}

						long organizationId = ParamUtil.getLong(actionRequest,
								PortalConstants.ORGANIZATION_ID);

						if (organizationId > 0) {
							redirect = HttpUtil.setParameter(redirect,
									actionResponse.getNamespace()
											+ PortalConstants.ORGANIZATION_ID, organizationId);
						}

						if (Validator.isNotNull(redirect)) {
							actionResponse.sendRedirect(redirect);
						}
					}
				}
			} else if (e instanceof NoSuchOrganizationException) {
				SessionErrors.add(actionRequest, NoSuchOrganizationException.class);
				redirect = ParamUtil.getString(actionRequest, PortalConstants.BACK_URL);
				
				HttpSession httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession(false);
				Key key = (Key) httpSession.getAttribute("key");
				boolean bIsEncrypted = false;
				
				try {
					bIsEncrypted = Encrypter.isEncrypted(redirect, key);
				} catch (Exception ex) {
					if (ex instanceof IllegalBlockSizeException ||
							ex instanceof BadPaddingException) {
						bIsEncrypted = false;
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
					httpSession.removeAttribute("key");
					
					redirect = Encrypter.decrypt(redirect, key);
				}
				
				sendRedirect(actionRequest, actionResponse, redirect);
			} else {
				throw e;
			}
		}
	}
}