package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.CsvVulnSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class CsvVulnSummaryEntryParser implements CSVEntryConverter<CsvVulnSummaryEntry> {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(CsvVulnSummaryEntryParser.class);
	
	@Override
	public String[] convertEntry(CsvVulnSummaryEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (entry == null) {
			params.put("entry", entry);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException(PortalErrors.FILE_PROCESS_ERROR);					
		}
		String requestReview = entry.getRequestReview();
		String[] columns = null;
		
		if (CommonUtil.isStringNullOrEmpty(requestReview)) {
			columns = new String[14];
		} else {
			columns = new String[15];
			columns[14] = requestReview;
		}
		
		columns[0] = entry.getNumber();
	    columns[1] = entry.getGroupName();
	    columns[2] = entry.getProjectName();
	    columns[3] = entry.getCaseNumber();
	    columns[4] = entry.getAttribute();
	    columns[5] = entry.getScanId();
	    columns[6] = entry.getProcess();
	    columns[7] = entry.getPreset();
	    columns[8] = entry.getExecutionDate();
	    columns[9] = entry.getSeverity();
	    columns[10] = entry.getVulnerabilityName();
	    columns[11] = entry.getQueryName();
	    columns[12] = entry.getInputName();
	    columns[13] = entry.getReportId();
	    
	    for (int index = 0; index < columns.length; index++) {
	    	if (columns[index] == null) {
	    		columns[index] = PortalConstants.STRING_EMPTY;
	    	}
	    }
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}
}
