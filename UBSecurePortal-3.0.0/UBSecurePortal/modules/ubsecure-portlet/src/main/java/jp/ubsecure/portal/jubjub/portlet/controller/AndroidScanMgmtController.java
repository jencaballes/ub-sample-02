package jp.ubsecure.portal.jubjub.portlet.controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectException;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.model.ResultItem;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.ScanItem;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.android.AndroidScanManager;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

public class AndroidScanMgmtController {
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(AndroidScanMgmtController.class);
	
	public static boolean addScan (UploadPortletRequest uploadRequest, long lUserId) throws UBSPortalException {
		long lProjectId = PortalConstants.LONG_ZERO;
		File file = null;
		String strContentType = null;
		Scan scan = null;
		Project project = null;
		boolean bSuccess = false;
		User user = null;
		String strFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		long lScanId = PortalConstants.LONG_ZERO;
		boolean bHasFile = false;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				lProjectId = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_PROJECT_ID);
				bHasFile = ParamUtil.getBoolean(uploadRequest, PortalConstants.PARAM_HAS_FILE_UPLOAD);
				
				file = uploadRequest.getFile(PortalConstants.PARAM_INSPECTION_FILE);
				
				if (file == null && bHasFile) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_APK));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG, PortalMessages.APK_FILE_SIZE_TOO_BIG);
				}
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.ANDROID.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
						}
					}
				}
				
				if (file != null) {
					strContentType = uploadRequest.getContentType(PortalConstants.PARAM_INSPECTION_FILE);
					strFileName = uploadRequest.getFileName(PortalConstants.PARAM_INSPECTION_FILE);
					
					if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
						if (CommonUtil.getStringBytes(strFileName) > 100) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_FILENAME));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_NAME_TOO_LONG, PortalMessages.APK_FILE_NAME_TOO_LONG);
						}
						
						if (!isAPKFilenameValid(strFileName)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_CONSISTS_MULTIBYTE_CHARACTERS, PortalConstants.ERROR_LOG_PARAM_FILENAME));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_NAME_INVALID, PortalMessages.APK_FILE_NAME_CONTAINS_MULTIBYTE_CHARACTERS);
						}
					}
				}
				
				if (file == null) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_FILE.toLowerCase()));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
				}
				
				if (CommonUtil.isStringNullOrEmpty(strContentType)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_FILE.toLowerCase()));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
				}
				
				isAPKValid(strContentType, file);
				
				if (CommonUtil.isStringNullOrEmpty(strFileName)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_FILE.toLowerCase()));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
				}
				
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}
				
				user = UserLocalServiceUtil.getUser(lUserId);
				
				if (CommonUtil.isObjectNull(user)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
				}
				
				scan = ScanLocalServiceUtil.createScanObj();
				lScanId = ScanLocalServiceUtil.createScanId();
				scan.setScanId(lScanId);
				
				scan.setProjectId(lProjectId);
				scan.setScanManager(user.getEmailAddress());
				scan.setFileName(strFileName);
				scan.setStatus(ScanStatus.SCAN_WAITING.getInteger());
				scan.setRegistrationDate(new Date());
				scan.setModifiedDate(new Date());
				
				long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
				
				if (!CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId())) {
					lCxAndroidProjectId = Long.parseLong(project.getCxAndroidProjectId());
				}
				
				Scan newScan = AndroidScanManager.registerAndroidScan(lCxAndroidProjectId, scan, file);
				
				Scan updatedScan = ScanLocalServiceUtil.updateScan(newScan);
				
				if (CommonUtil.isObjectNull(updatedScan)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
				} else {
					bSuccess = true;
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_ADD_SCAN, params, nspe);
			throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_ADD_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("scan", scan);
			params.put("file", file);
			params.put("project", project);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_ADD_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("file", file);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lUserId", lUserId);
				params.put("project", project);
				params.put("file", file);
				params.put("strFileName", strFileName);
				params.put("strContentType", strContentType);
				params.put("user", user);
				params.put("scan", scan);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_SCAN, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_SCAN, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean stopScan (ActionRequest actionRequest) throws UBSPortalException {
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		Project project = null;
		Scan scan = null;
		boolean bGetProject = true;
		long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
		long lCxAndroidScanId = PortalConstants.LONG_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					ProjectLocalServiceUtil.clearCache();
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						long[] userOrgIdList = user.getOrganizationIds();
						for (long userOrgId : userOrgIdList) {
							if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
								bIsProjectUser = true;
								break;
							}
						}
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.STOP_SCAN_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.ANDROID.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
						}
						
						if (!CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId())) {
							lCxAndroidProjectId = Long.parseLong(project.getCxAndroidProjectId());
						}
					}
					
					if (lScanId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.STOP_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.getScan(lScanId);
							
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							if (scan.getScanId() == PortalConstants.LONG_ZERO) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.STOP_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
								
								if (CommonUtil.isObjectNull(project)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.STOP_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.ANDROID.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.STOP_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
									}
								}
								
								if (!CommonUtil.isStringNullOrEmpty(scan.getCxAndroidScanId())) {
									lCxAndroidScanId = Long.parseLong(scan.getCxAndroidScanId());
								}
							}
						}
					}
				}
				
				AndroidScanManager.stopAndroidScan(lCxAndroidProjectId, lCxAndroidScanId, project.getProjectId(), scan.getScanId());
				
				scan.setStatus(ScanStatus.FAILURE.getInteger());
				scan.setModifiedDate(new Date());
				
				bSuccess = ScanLocalServiceUtil.updateScan(PortalConstants.USER_EVENT_STOP_SCAN, scan);
				
				if (bSuccess) {
					MailUtil.sendEmail(PortalConstants.EMAIL_EVENT_SCAN_CANCELLED, lScanId, null);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_STOP_SCAN, params, nsse);
			throw new UBSPortalException(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_STOP_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.STOP_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_STOP_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.STOP_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_STOP_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_STOP_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_STOP_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_STOP_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
				params.put("lScanId", lScanId);
				params.put("scan", scan);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_STOP_SCAN, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	private static void isAPKValid (String strContentType, File file) throws UBSPortalException {
		double dFileSize = PortalConstants.DOUBLE_ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		String strExtension = PortalConstants.STRING_EMPTY;
		String strUserAgent = PortalConstants.STRING_EMPTY;
		boolean bIsValidFile = false;
		
		try {
			if (!CommonUtil.isStringNullOrEmpty(strContentType)) {
				strContentType = Normalizer.normalize(strContentType, Normalizer.Form.NFKD);
				
				bIsValidFile = CommonUtil.isValidFile(file);
				
				if (bIsValidFile) {
					strExtension = FileUtil.getExtension(file.getAbsolutePath());
					
					if (strExtension.equalsIgnoreCase(PortalConstants.APK_FILE_EXTENSION)) {
						if (strUserAgent.toUpperCase().contains(PortalConstants.USER_AGENT_MSIE) ||
								(!strUserAgent.toUpperCase().contains(PortalConstants.USER_AGENT_FIREFOX) &&
										!strUserAgent.toUpperCase().contains(PortalConstants.USER_AGENT_CHROME))) {
							if (strContentType.equalsIgnoreCase(PortalConstants.CONTENT_TYPE_ZIP)) {
								strContentType = PortalConstants.CONTENT_TYPE_APK;
							}
						}
					}
				}
			}
			
			if (!bIsValidFile ||
					!strContentType.equalsIgnoreCase(PortalConstants.CONTENT_TYPE_APK) ||
					!strExtension.equalsIgnoreCase(PortalConstants.APK_FILE_EXTENSION)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_TYPE_INVALID, PortalConstants.ERROR_LOG_PARAM_APK));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_NOT_APK, PortalMessages.FILE_INVALID);
			}
			
			dFileSize = FileProcessUtil.getFileSize(file);
			
			if (dFileSize > PortalConstants.MAX_SIZE_APK) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_APK));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG_50, PortalMessages.APK_FILE_SIZE_TOO_BIG);
			}
			
			if (dFileSize == PortalConstants.DOUBLE_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_EMPTY, PortalMessages.FILE_INVALID);
			}
		} catch (UBSPortalException e) {
			String strErrorCode = e.getErrorCode();
			
			params.put("strContentType", strContentType);
			params.put("file", file);
			params.put("dFileSize", dFileSize);
			
			if (strErrorCode.equals(PortalErrors.INVALID_FILE)
					&& CommonUtil.isStringNullOrEmpty(e.getErrorMessage())) {
				e.setErrorMessage(PortalMessages.FILE_INVALID);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_IS_APK_VALID, params, e);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_IS_APK_VALID, params, e);
			}
			throw e;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
	}
	
	private static boolean isAPKFilenameValid (String filename) {
		for (int index = 0; index < filename.length(); index++) {
			String character = filename.charAt(index) + PortalConstants.STRING_EMPTY;
			
			if (character.getBytes().length > 1) {
				return false;
			}
		}
		
		return true;
	}
	
	public static void getScansForRefresh (ResourceRequest resourceRequest, ResourceResponse resourceResponse, PortletConfig portletConfig) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<ScanItem> scanList = null;
		Project project = null;
		String strScanId = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String strHashValue = PortalConstants.STRING_EMPTY;
		String strScanManager = PortalConstants.STRING_EMPTY;
		String strRegDateLow = PortalConstants.STRING_EMPTY;
		String strRegDateHigh = PortalConstants.STRING_EMPTY;
		String strStatus = PortalConstants.STRING_EMPTY;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isObjectNull(oLoggedIn) && Boolean.parseBoolean(oLoggedIn.toString())) {
					project = AndroidProjectMgmtController.getProject(resourceRequest);
					scanList = AndroidScanMgmtController.getScans(resourceRequest);
					
					strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID);
					strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME);
					strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE);
					strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER);
					strRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW);
					strRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH);
					strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
					
					Map<String, Object> searchedScan = new HashMap<String, Object>();
					searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
					searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strRegDateLow);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strRegDateHigh);
					searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
					
					resourceRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
					resourceRequest.setAttribute(PortalConstants.PARAM_SCAN_LIST, scanList);
					resourceRequest.setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
					resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_TEXT_HTML);
				
					portletConfig.getPortletContext().getRequestDispatcher(PortalConstants.ANDROID_SCAN_LIST_REFRESH_JSP).include(resourceRequest, resourceResponse);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortletException e) {
			params.put("portletConfig", portletConfig);
			log.debug(PortalErrors.PORTLET_EXCEPTION, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.PORTLET_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (IOException e) {
			params.put("resourceRequest", resourceRequest);
			params.put("resourceResponse", resourceResponse);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("portletConfig", portletConfig);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
	}
	
	public static void getEntireScansForRefresh (ResourceRequest resourceRequest, ResourceResponse resourceResponse, PortletConfig portletConfig) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<ResultItem> entireScanList = null;
		String strScanId = PortalConstants.STRING_EMPTY;
		String strProjectName = PortalConstants.STRING_EMPTY;
		String strGroupName = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String strHashValue = PortalConstants.STRING_EMPTY;
		String strScanManager = PortalConstants.STRING_EMPTY;
		String strRegDateLow = PortalConstants.STRING_EMPTY;
		String strRegDateHigh = PortalConstants.STRING_EMPTY;
		String strStatus = PortalConstants.STRING_EMPTY;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);

		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isObjectNull(oLoggedIn) && Boolean.parseBoolean(oLoggedIn.toString())) {
					entireScanList = AndroidScanMgmtController.getEntireScans(resourceRequest);
					
					strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID);
					strProjectName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROJECT_NAME);
					strGroupName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_OWNER_GROUP);
					strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME);
					strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE);
					strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER);
					strRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW);
					strRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH);
					strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
					
					Map<String, Object> searchedScan = new HashMap<String, Object>();
					searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
					searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strRegDateLow);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strRegDateHigh);
					searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
					
					resourceRequest.setAttribute(PortalConstants.PARAM_SCAN_LIST, entireScanList);
					resourceRequest.setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
					resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_TEXT_HTML);
					
					portletConfig.getPortletContext().getRequestDispatcher(PortalConstants.ANDROID_ENTIRE_SCAN_LIST_REFRESH_JSP).include(resourceRequest, resourceResponse);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortletException e) {
			params.put("portletConfig", portletConfig);
			log.debug(PortalErrors.PORTLET_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.PORTLET_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (IOException e) {
			params.put("resourceRequest", resourceRequest);
			params.put("resourceResponse", resourceResponse);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("resourceRequest", resourceRequest);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(entireScanList)) {
				entireScanList = null;
			}
		}
	}
	
	private static List<ScanItem> getScans (ResourceRequest resourceRequest) throws UBSPortalException {
		long lProjectId = 0;
		String strScanId = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String strStatus = null;
		String strOrderByCol = null;
		String strOrderByType = null;
		DateFormat formatter = null;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		List<Object> scanList = null;
		List<ScanItem> returnList = null;
		int start = PortalConstants.INT_ZERO;
		HttpSession session = null;
		long lUserId = 0L;
		Map<String, Object> params = new HashMap<String, Object>();
		User user = null;
		Object oUserId = null;
		Map<String, Object> searchedScan = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				start = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_START);
				lProjectId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_PROJECT_ID);
				strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID).trim();
				strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME).trim();
				strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE).trim();
				strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
				strScanRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
				strScanRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
				strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
				strOrderByCol = ParamUtil.getString(resourceRequest, "orderByCol").trim();
				strOrderByType = ParamUtil.getString(resourceRequest, "orderByType").trim();
				
				if (lProjectId == 0) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
					try {
						long lScanId = Long.parseLong(strScanId);
						
						if (lScanId < PortalConstants.LONG_ZERO) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
					if (!Validator.isAlphanumericName(strHashValue)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
					
					if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
				}
				
				try {
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
						if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateLow = formatter.parse(strScanRegDateLow);
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
						if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateHigh = Calendar.getInstance();
						dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
						dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteRegDateHigh.set(Calendar.MINUTE, 59);
						dteRegDateHigh.set(Calendar.SECOND, 59);
					}
					
					if (!CommonUtil.isObjectNull(dteRegDateLow)
							&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
						if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE.toLowerCase()));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
					}
				} catch (ParseException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
				}
				
				session = ControllerHelper.getHttpSession(resourceRequest);
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId != 0) {
						user = UserLocalServiceUtil.getUser(lUserId);
					} else {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					}
				}
				
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
				searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
				
				scanList = ScanLocalServiceUtil.sortScans(lProjectId, searchedScan, strOrderByCol, strOrderByType, start);
				
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
					returnList = new ArrayList<ScanItem>();
					long lCompanyId = user.getCompanyId();
					
					for (Object item : scanList) {
						ScanItem scan = (ScanItem) item;
						
						try {
							user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
							
							if (CommonUtil.isObjectNull(user)) {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							} else {
								scan.setScanManager(user.getFirstName());
							}
						} catch (NoSuchUserException e) {
							scan.setScanManager(PortalConstants.STRING_EMPTY);
						}
						
						returnList.add(scan);
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (SystemException se) {
			se.printStackTrace();
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			pe.printStackTrace();
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("user", user);
			params.put("searchedScan", searchedScan);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCANS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
			params.put("oUserId", oUserId);
			log.debug(PortalErrors.NUMBER_FORMAT_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, nfe);
			throw new UBSPortalException(PortalErrors.NUMBER_FORMAT_EXCEPTION, PortalMessages.COMMON_EXCEPTION, nfe);
		} catch (UBSPortalException ubspe) {
			ubspe.printStackTrace();
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lUserId", lUserId);
				params.put("searchedScan", searchedScan);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_SCANS, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_SCANS, params, ubspe);
			}
			throw ubspe;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
		
		return returnList;
	}
	
	public static List<ResultItem> getEntireScans (ResourceRequest resourceRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = null;
		List<ResultItem> returnList = null;
		String strScanId = null;
		String strProjectName = null;
		String strGroupName = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String strStatus = null;
		String strOrderByCol = null;
		String strOrderByType = null;
		DateFormat formatter = null;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		int iStart = PortalConstants.INT_ZERO;
		Map<String, Object> searchedScan = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				iStart = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_START);
				strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID).trim();
				strProjectName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
				strGroupName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_OWNER_GROUP).trim();
				strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME).trim();
				strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE).trim();
				strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
				strScanRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
				strScanRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
				strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
				strOrderByCol = ParamUtil.getString(resourceRequest, "orderByCol").trim();
				strOrderByType = ParamUtil.getString(resourceRequest, "orderByType").trim();
				
				if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
					try {
						long lScanId = Long.parseLong(strScanId);
						
						if (lScanId < PortalConstants.LONG_ZERO) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
					if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
					if (CommonUtil.getStringBytes(strGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_GROUP_NAME_TOO_LONG, PortalMessages.GROUP_NAME_TOO_LONG);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
					if (!Validator.isAlphanumericName(strHashValue)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
					
					if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
				}
				
				try {
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
						if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateLow = formatter.parse(strScanRegDateLow);
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
						if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
	
						dteRegDateHigh = Calendar.getInstance();
						dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
						dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteRegDateHigh.set(Calendar.MINUTE, 59);
						dteRegDateHigh.set(Calendar.SECOND, 59);
					}
					
					if (!CommonUtil.isObjectNull(dteRegDateLow)
							&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
						if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE.toLowerCase()));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
					}
				} catch (ParseException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
				}
				
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
				searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
				searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
				searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
				
				scanList = ScanLocalServiceUtil.sortEntireScans(ProjectType.ANDROID.getInteger(), searchedScan, strOrderByCol, strOrderByType, iStart);
				
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
					returnList = new ArrayList<ResultItem>();
					List<User> userList = UserLocalServiceUtil.getUsers(PortalConstants.INT_ZERO, PortalConstants.INT_ONE);
					long lCompanyId = PortalConstants.LONG_ZERO;
					
					if (!CommonUtil.isListNullOrEmpty(userList)) {
						for (User user : userList) {
							lCompanyId = user.getCompanyId();
							break;
						}
					}
					
					for (Object item : scanList) {
						ResultItem scan = (ResultItem) item;
						User user = null;
						
						try {
							user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
							
							if (CommonUtil.isObjectNull(user)) {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							} else {
								scan.setScanManager(user.getFirstName());
							}
						} catch (NoSuchUserException e) {
							scan.setScanManager(PortalConstants.STRING_EMPTY);
						}
						
						returnList.add(scan);
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException pe) {
			params.put("searchedScan", searchedScan);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("dteRegDateLow", dteRegDateLow);
				params.put("dteRegDateHigh", dteRegDateHigh);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, ubspe);
			}
			throw ubspe;
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, e);
		} finally {
			if (!CommonUtil.isObjectNull(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
		
		return returnList;
	}
	
	public static boolean reexecuteScan (ActionRequest actionRequest) throws UBSPortalException {
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		Project project = null;
		Scan scan = null;
		boolean bGetProject = true;
		long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
		long lCxAndroidScanId = PortalConstants.LONG_ZERO;
		long lNewCxAndroidScanId = PortalConstants.LONG_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (oUserId != null) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == 0L) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (user == null) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (oUserRole != null) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == 0) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					ProjectLocalServiceUtil.clearCache();
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						long[] userOrgIdList = user.getOrganizationIds();
						for (long userOrgId : userOrgIdList) {
							if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
								bIsProjectUser = true;
								break;
							}
						}
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
					
					if (project == null) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.ANDROID.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
						}
						
						if (!CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId())) {
							lCxAndroidProjectId = Long.parseLong(project.getCxAndroidProjectId());
						}
					}
					
					if (lScanId == 0L) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.getScan(lScanId);
							
						if (scan == null) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							if (scan.getProjectId() == 0L) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
								
								if (project == null) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.ANDROID.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
									}
								}
								
								if (!CommonUtil.isStringNullOrEmpty(scan.getCxAndroidScanId())) {
									lCxAndroidScanId = Long.parseLong(scan.getCxAndroidScanId());
								}
							}
						}
					}
				}
				
				lNewCxAndroidScanId = AndroidScanManager.reRunAndroidScan(lCxAndroidProjectId, lCxAndroidScanId, project.getProjectId(), lScanId, scan.getStatus());
				
				if (lNewCxAndroidScanId != 0L) {
					scan.setRegistrationDate(new Date());
					scan.setCxAndroidScanId(String.valueOf(lNewCxAndroidScanId));
					scan.setStatus(ScanStatus.SCAN_WAITING.getInteger());
					scan.setModifiedDate(new Date());
					
					bSuccess = ScanLocalServiceUtil.updateScan(PortalConstants.USER_EVENT_REEXECUTE_SCAN, scan);
				} else {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
					throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID, PortalMessages.CX_ANDROID_SCAN_ID_INVALID);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nsse);
			throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.REEXECUTE_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_REEXECUTE_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_REEXECUTE_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_REEXECUTE_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
				params.put("lScanId", lScanId);
				params.put("scan", scan);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_REEXECUTE_SCAN, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean deleteScan (ActionRequest actionRequest) throws UBSPortalException {
		boolean bSuccess = false;
		long lScanId = 0L;
		long lProjectId = 0L;
		Map<String, Object> params = new HashMap<String, Object>();
		Project project = null;
		Scan scan = null;
		boolean bGetProject = true;
		long lCxAndroidProjectId = 0L;
		long lCxAndroidScanId = 0L;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = 0L;
		User user = null;
		Object oUserRole = null;
		int iUserRole = 0;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (oUserId != null) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == 0L) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (user == null) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (oUserRole != null) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == 0) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
					
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					
					if (project == null) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.ANDROID.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
						}
						
						if (!CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId())) {
							lCxAndroidProjectId = Long.parseLong(project.getCxAndroidProjectId());
						}
					}
					
					if (lScanId == 0) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.DELETE_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.getScan(lScanId);
							
						if (scan == null) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							if (scan.getScanId() == 0L) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.DELETE_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(scan.getProjectId());
								
								if (project == null) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.ANDROID.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.DELETE_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
									}
									
									if (!CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId())) {
										lCxAndroidScanId = Long.parseLong(project.getCxAndroidProjectId());
									}
								}
								
								if (!CommonUtil.isStringNullOrEmpty(scan.getCxAndroidScanId())) {
									lCxAndroidScanId = Long.parseLong(scan.getCxAndroidScanId());
								}
							}
						}
					}
				}
				
				AndroidScanManager.deleteAndroidScan(lCxAndroidProjectId, lCxAndroidScanId, project.getProjectId(), scan.getScanId(), true);
				
				bSuccess = ScanLocalServiceUtil.deleteScan(ProjectType.ANDROID.getInteger(), lScanId);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params, nsse);
			throw new UBSPortalException(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));

			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_DELETE_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_DELETE_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_DELETE_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lScanId", lScanId);
				params.put("scan", scan);
				params.put("lProjectId", lProjectId);
				params.put("project", project);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_DELETE_SCAN, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
}
