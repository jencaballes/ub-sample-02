package jp.ubsecure.portal.jubjub.portlet.util;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

public class ValidateResult {
	public boolean valid_flg;
	public String message;

	public ValidateResult() {
		super();
		this.valid_flg = PortalConstants.TRUE;
		this.message = PortalConstants.STRING_EMPTY;
	}
}
