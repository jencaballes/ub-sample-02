package jp.ubsecure.portal.jubjub.portlet.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.jabberwock.cli.bridge.jubjub.ApiInitInfo;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.ApiProjectCopyInfo;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.ApiResendResultData;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.ApiScanInfo;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.ApiScanType;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceAutoCrawlGetLoginParameters;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceAutoCrawlSetting;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceEditReportDefinition;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceGetStatus;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfacePause;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceProjectAdd;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceProjectDelete;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceProjectImport;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceResend;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceRestart;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceScan;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.InterfaceStopAutoCrawl;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.VexInstanceFactory;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.ApiAuditType;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.ApiSignatureSetInfo;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.GetSignatureSetList;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.ProjectCopy;
import jp.ubsecure.portal.jubjub.portlet.VexPortlet;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectException;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.model.ResultItem;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.ScanItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;
import jp.ubsecure.portal.jubjub.portlet.model.VexEnvRemarkItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexLoginSetting;
import jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexProjectAddSettingItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexReportInfoSettingItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexReportOptionsItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting;
import jp.ubsecure.portal.jubjub.portlet.model.VexScanSettingItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetFuncItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetHostItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInfoSettingItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformation;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformationItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexUser;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.VexDetectionResultLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.VexLoginSettingLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.VexScanSettingLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.VexTargetInformationLocalServiceUtil;
//import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.CxScanManager;
//import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.CxScanReport;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ValidateResult;
import jp.ubsecure.portal.jubjub.portlet.util.ValidationUtil;

public class VexScanMgmtController {
private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexScanMgmtController.class);

	public static Scan uploadScan (ActionRequest actionRequest, UploadPortletRequest uploadRequest, long lUserId, String prevUploadFilePath) throws UBSPortalException {
		long lProjectId = PortalConstants.LONG_ZERO;
		File sourceFile = null;
		String strContentType = null;
		Scan scan = null;
		Project project = null;
		boolean bSuccess = false;
		User user = null;
		int iStatus = 0;
		String strFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		long lScanId = PortalConstants.LONG_ZERO;
		boolean bHasFile = false;
		String strScanId = null;
		boolean hasInspectionLog = true;
		File scanDirectory = null;
		String fileName  = PortalConstants.STRING_EMPTY;
		String target = PortalConstants.STRING_EMPTY;
		File prevUploadFile = null;
		try {	
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			String scanName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_NAME);
			
			if (CommonUtil.isStringNullOrEmpty(scanName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_SCAN_NAME);
			}
			
			if(!ControllerHelper.isVexInputValid(scanName)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.SCAN_NAME_INVALID);
			}
			
			lProjectId = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}
			
			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			sourceFile = uploadRequest.getFile(PortalConstants.PARAM_INSPECTION_FILE);
			fileName = uploadRequest.getFileName(PortalConstants.PARAM_INSPECTION_FILE);
			bHasFile = ParamUtil.getBoolean(uploadRequest, PortalConstants.PARAM_HAS_FILE_UPLOAD);
						
			if(!CommonUtil.isStringNullOrEmpty(prevUploadFilePath)){
				target = prevUploadFilePath.replaceFirst("[\\\\/]$", "").replaceAll("\"", "");
				prevUploadFile = new File(target);
				
				if(sourceFile!=null && prevUploadFile != null && prevUploadFile.isFile()){
					if(fileName.equals(prevUploadFile.getName().toString()) || CommonUtil.isStringNullOrEmpty(fileName)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_ALREADY_UPLOADED, PortalConstants.ERROR_LOG_PARAM_FILE));
						throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_FILE_ALREADY_UPLOADED, PortalMessages.UPLOAD_FILE_ALREADY_UPLOADED);
					}
				}
			}
			
			if (CommonUtil.isObjectNull(sourceFile) && bHasFile) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_EXP));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_FILE_SIZE_TOO_BIG, PortalMessages.EXP_FILE_SIZE_TOO_BIG_VEX);
			}
						
			if (CommonUtil.isObjectNull(sourceFile)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
			}
			
			if (FileProcessUtil.getFileSize(sourceFile) == PortalConstants.DOUBLE_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_FILE_EMPTY, PortalMessages.FILE_INVALID);
			}
			
			if (FileProcessUtil.getFileSize(sourceFile) > PortalConstants.MAX_SIZE_EXP) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_EXP));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_FILE_SIZE_TOO_BIG, PortalMessages.EXP_FILE_SIZE_TOO_BIG_VEX);
			}
			
			strContentType = FilenameUtils.getExtension(sourceFile.getAbsolutePath());
			
			if (CommonUtil.isStringNullOrEmpty(strContentType)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
			}
			
			if (!strContentType.equals("exp")) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_TYPE_INVALID, PortalConstants.ERROR_LOG_PARAM_EXP));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_FILE_NOT_EXP, PortalMessages.FILE_INVALID);
			}
			
			if (lUserId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
				throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
			}
			
			user = UserLocalServiceUtil.getUser(lUserId);
			
			if (CommonUtil.isObjectNull(user)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
			}
			
//			if (CommonUtil.isStringNullOrEmpty(strSessionId)) {
//				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
//				throw new UBSPortalException(PortalErrors.INVALID_CX_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
//			}
			
			strScanId = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_SCAN_ID);
			
			if (!CommonUtil.isStringNullOrEmpty(strScanId) && Validator.isDigit(strScanId)) {
				lScanId = Long.parseLong(strScanId);
				scan = ScanLocalServiceUtil.createScan(lScanId);
			}else{
				lScanId = ScanLocalServiceUtil.createScanId();
				scan = ScanLocalServiceUtil.createScanObj();
			}
										
			try {
				String strTargetDirectory = FileProcessUtil.createUploadDirectory(lProjectId, lScanId);
				scanDirectory = new File(strTargetDirectory);
				File[] files = scanDirectory.listFiles();
				if(null != files && files.length > 0){
					if(FileProcessUtil.hasSpecificFileInFolder(scanDirectory, fileName)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_ALREADY_EXISTS, PortalConstants.ERROR_LOG_PARAM_FILE));
						throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_FILE_ALREADY_UPLOADED, PortalMessages.UPLOAD_FILE_ALREADY_UPLOADED);
					} else {
						FileUtils.deleteDirectory(scanDirectory);
						strTargetDirectory = FileProcessUtil.createUploadDirectory(lProjectId, lScanId);
					}
				}

				//TODO Check filename for japanese characters
				
				File destinationFile = new File(strTargetDirectory, fileName);
				destinationFile.getParentFile().mkdirs();
				destinationFile.createNewFile();
				
				Path sourceDirectory = Paths.get(sourceFile.getAbsolutePath());
		        Path destinationDirectory = Paths.get(destinationFile.getAbsolutePath());
				Files.copy(sourceDirectory, destinationDirectory, StandardCopyOption.REPLACE_EXISTING);
				String targetFolder = null;
				String zipFilePath = FileProcessUtil.getNewFileWithNewExtension(destinationDirectory.toString(), ".zip");
				if(FileProcessUtil.copyFile(destinationDirectory.toString(), zipFilePath)){
					targetFolder = FileProcessUtil.getFileNameOnly(destinationDirectory.toString());
					iStatus = getImportScanStatusFromInspectionLog(zipFilePath, targetFolder); 
				}
				if(iStatus == 0){
					hasInspectionLog = false;
					params.put("targetFolder",targetFolder);
					params.put("zipFilePath",zipFilePath);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_IMPORT_SCAN_NO_INSPECTION_LOG);
					throw new UBSPortalException(PortalErrors.IMPORT_SCAN_NO_INSPECTION_LOG, PortalMessages.IMPORT_SCAN_NO_INSPECTION_LOG);
				}
				
				scan.setScanId(lScanId);
				scan.setProjectId(lProjectId);
				scan.setFileName(scanName);
				scan.setFilePath(destinationDirectory.toString());
				scan.setStatus(iStatus);
				bSuccess = true;
			} catch (IOException e) {
				bSuccess = false;
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_DELETE_SCAN));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
			}
		    
			//Delete temporary source file after uploading it to liferay upload folder.
			if(bSuccess){
				sourceFile.delete();
			}else{
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_FAILED, PortalMessages.UPLOAD_SCAN_FAILED);
			}
			
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.UPLOAD_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_UPLOAD_SCAN, params, nspe);
			throw new UBSPortalException(PortalErrors.UPLOAD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_UPLOAD_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("scan", scan);
			params.put("file", sourceFile);
			params.put("project", project);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_UPLOAD_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}
	
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_UPLOAD_SCAN, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("file", sourceFile);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPLOAD_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else if (strErrorCode.equals(PortalErrors.INVALID_FILE)) {
				ubspe.setErrorMessage(PortalMessages.FILE_INVALID);
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lUserId", lUserId);
				params.put("project", project);
				params.put("file", sourceFile);
				params.put("strFileName", strFileName);
				params.put("strContentType", strContentType);
				params.put("user", user);
				params.put("scan", scan);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_UPLOAD_SCAN, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_UPLOAD_SCAN, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if(!hasInspectionLog && scanDirectory.exists() ){
				FileProcessUtil.deleteDirectory(scanDirectory);
			}
		}
		return scan;
	}
	
	public static String uploadCertificateFile (long lProjectId, long lUserID, UploadPortletRequest uploadRequest, String certificateName) throws UBSPortalException {
		File sourceFile = null;
		String strContentType = null;
		boolean bSuccess = false;
		//User user = null;
		String strFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		//File strTargetDirectory = null;
	    String certFilePath = PortalConstants.STRING_EMPTY;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.PROJECT_ID_INVALID);
			}
			
			if (lUserID == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.USER_ID_INVALID);
			}
			
			strContentType = FilenameUtils.getExtension(uploadRequest.getFile(certificateName).getAbsolutePath());
			
			if (CommonUtil.isStringNullOrEmpty(strContentType)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE, PortalMessages.FILE_INVALID);
			}
			
			if (!strContentType.equals("p12")) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_TYPE_INVALID, PortalConstants.ERROR_LOG_PARAM_EXP));
				throw new UBSPortalException(PortalErrors.INVALID_FILE, PortalMessages.FILE_INVALID);
			}
			
				try {
					
					strFileName = uploadRequest.getFile(certificateName).getName();
					sourceFile = uploadRequest.getFile(certificateName);
					
					File uploadedCertitficateSourceFile = FileProcessUtil.uploadCertificateFile(lProjectId, sourceFile, strFileName);
					
					if (!CommonUtil.isValidAndExistingFile(uploadedCertitficateSourceFile)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.UPLOAD_CERTIFICATE_FAILED);
					}

					certFilePath = uploadedCertitficateSourceFile.getAbsolutePath();
					
					if (CommonUtil.isStringNullOrEmpty(certFilePath)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.UPLOAD_CERTIFICATE_FAILED);
					}

					bSuccess = true;
				} catch (UBSPortalException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.UPLOAD_CERTIFICATE_FAILED);
				}
		    
			if(bSuccess){
				return certFilePath;
			}
			
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserID);
			params.put("file", sourceFile);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPLOAD_CERTIFICATE_FILE, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else if (strErrorCode.equals(PortalErrors.INVALID_FILE)) {
				ubspe.setErrorMessage(PortalMessages.FILE_INVALID);
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lUserId", lUserID);
				params.put("file", sourceFile);
				params.put("strFileName", strFileName);
				params.put("strContentType", strContentType);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_UPLOAD_CERTIFICATE_FILE, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_UPLOAD_CERTIFICATE_FILE, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		return certFilePath;
	}
	
	public static boolean importScan (UploadPortletRequest uploadRequest, long lUserId, String filePath, ApiInitInfo info) throws UBSPortalException {
		long lProjectId = PortalConstants.LONG_ZERO;
		File file = null;
		String strContentType = null;
		Project project = null;
		boolean bSuccess = false;
		User user = null;
		String strFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		Scan scan = null;
		String errorMsg="";
		String strScanId = null;
		Long lScanId = PortalConstants.LONG_ZERO;
		int iStatus = 0;
		String strStatus = null;
		String scanName = null;
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			scanName = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_SCAN_NAME);
			if (CommonUtil.isStringNullOrEmpty(scanName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_SCAN_NAME);
			}
			
			if(!ControllerHelper.isVexInputValid(scanName)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.SCAN_NAME_INVALID);
			}
			
			lProjectId = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}
			
			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			file = new File(filePath);
						
			if (CommonUtil.isObjectNull(file) || !file.exists()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
			}
						
			strContentType = FilenameUtils.getExtension(file.getAbsolutePath());
						
			if (CommonUtil.isStringNullOrEmpty(strContentType)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
			}
			
			if (!strContentType.equals("exp")) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_TYPE_INVALID, PortalConstants.ERROR_LOG_PARAM_EXP));
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FILE_NOT_EXP, PortalMessages.FILE_INVALID);
			}
			
			if (FileProcessUtil.getFileSize(file) == PortalConstants.DOUBLE_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FILE_EMPTY, PortalMessages.FILE_INVALID);
			}
			
			if (FileProcessUtil.getFileSize(file) > PortalConstants.MAX_SIZE_EXP) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_ZIP));
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FILE_SIZE_TOO_BIG, PortalMessages.EXP_FILE_SIZE_TOO_BIG_VEX);
			}
			
			if (lUserId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
				throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
			}
			
			user = UserLocalServiceUtil.getUser(lUserId);
			
			if (CommonUtil.isObjectNull(user)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
			}
			
			strStatus = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_STATUS);
			strScanId = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_SCAN_ID);
			
			if(!CommonUtil.isStringNullOrEmpty(strStatus) && Validator.isDigit(strStatus)){
				iStatus = Integer.parseInt(strStatus);
			}else{
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_IMPORT_SCAN_NO_INSPECTION_LOG);
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_NO_INSPECTION_LOG, PortalMessages.IMPORT_SCAN_NO_INSPECTION_LOG);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanId) && Validator.isDigit(strScanId)) {
				lScanId = Long.parseLong(strScanId);
			}else{
				lScanId = ScanLocalServiceUtil.createScanId();
			}
			
			String vexScanName = lProjectId + PortalConstants.STR_UNDERSCORE + lScanId + PortalConstants.STR_UNDERSCORE+ scanName;		
			InterfaceProjectImport scanImport = importScan(info, vexScanName, filePath);
			
			if(scanImport == null || !scanImport.getResult()){
				if(scanImport!=null){
					errorMsg = scanImport.getErrorMessage();
					params.put("Import Result: ", scanImport.getResult());
					params.put("Import Status: ", scanImport.GetStatus());
				}
				
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FAILED, PortalMessages.IMPORT_SCAN_FAILED);
			}
			String vexScanId = PortalConstants.STRING_EMPTY;
			
			try{
				vexScanId = scanImport.GetNewProjectID();
			}catch(Exception e){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, e.getMessage());
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FAILED, PortalMessages.IMPORT_SCAN_FAILED);
			} catch (Error e){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, e.getMessage());
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FAILED, PortalMessages.IMPORT_SCAN_FAILED);
			}
			
			if(!vexScanId.isEmpty()){
				scan = ScanLocalServiceUtil.createScanObj();
				scan.setScanId(lScanId);
				scan.setStatus(iStatus);
				scan.setFileName(scanName);
				scan.setFilePath(filePath);
				scan.setCxAndroidScanId(vexScanId);
				scan.setProjectId(lProjectId);
				scan.setScanManager(user.getEmailAddress());
				scan.setRegistrationDate(new Date());
				scan.setModifiedDate(new Date());
				
				Scan updatedScan = ScanLocalServiceUtil.updateScan(scan);
				
				if (CommonUtil.isObjectNull(updatedScan)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
					throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FAILED, PortalMessages.IMPORT_SCAN_FAILED);
				}else{
					//Set report output setting
					boolean infoFile = false;
					String report_information_path = null;

					try {
						File fileDirectory = new File(file.getParentFile().toString());
						report_information_path = createReportInformationFileDetailSetting(lScanId, lProjectId, fileDirectory, vexScanName, null, 0);
						InterfaceEditReportDefinition reportDefinition = VexInstanceFactory.getEditReportDefinition(info);
						infoFile = reportDefinition.execute(vexScanId, report_information_path);
					} catch(Exception e){
						params.put("Vex Scan Id", vexScanId);
						report_information_path = report_information_path.replace("//", "/");
						params.put("report definition filepath", report_information_path);
						params.put("error", e.getCause());
						throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FAILED, PortalMessages.IMPORT_SCAN_FAILED);
					}catch (Throwable t){
						params.put("Vex Scan Id", vexScanId);
						report_information_path = report_information_path.replace("//", "/");
						params.put("report definition filepath", report_information_path);
						params.put("error", t.getCause());
						throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FAILED, PortalMessages.IMPORT_SCAN_FAILED);
					} finally {
						if (infoFile) {
							deleteYmlFile(report_information_path);
						}
					}
					
					bSuccess = setImportDefaultVexScanSetting(lScanId, lProjectId);
				}
			}else{
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
				throw new UBSPortalException(PortalErrors.IMPORT_SCAN_FAILED, PortalMessages.IMPORT_SCAN_FAILED);
			}
			
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_IMPORT_SCAN, params, nspe);
			throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_IMPORT_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("scan", scan);
			params.put("file", file);
			params.put("project", project);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			params.put("vexError", errorMsg);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_IMPORT_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_IMPORT_SCAN, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("file", file);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			params.put("vexError", errorMsg);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_IMPORT_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else if (strErrorCode.equals(PortalErrors.INVALID_FILE)) {
				ubspe.setErrorMessage(PortalMessages.FILE_INVALID);
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lUserId", lUserId);
				params.put("project", project);
				params.put("file", file);
				params.put("strFileName", strFileName);
				params.put("strContentType", strContentType);
				params.put("user", user);
				params.put("scan", scan);
				params.put("vexError", errorMsg);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_IMPORT_SCAN, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_IMPORT_SCAN, params, ubspe);
			}
			throw ubspe;
		} finally {
			if(file.exists()){
				file.delete();
			}
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
				return bSuccess;
			}
		}
		
		return bSuccess;
	}

	public static boolean addScan (UploadPortletRequest uploadRequest, long lUserId) throws UBSPortalException {
		long lProjectId = PortalConstants.LONG_ZERO;
		File file = null;
		String strContentType = null;
		Scan scan = null;
		Project project = null;
		boolean bSuccess = false;
		User user = null;
		String strFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		long lScanId = PortalConstants.LONG_ZERO;
		boolean bHasFile = false;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			lProjectId = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}
			
			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			file = uploadRequest.getFile(PortalConstants.PARAM_INSPECTION_FILE);
			bHasFile = ParamUtil.getBoolean(uploadRequest, PortalConstants.PARAM_HAS_FILE_UPLOAD);
			
			if (CommonUtil.isObjectNull(file) && bHasFile) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_ZIP));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG, PortalMessages.ZIP_FILE_SIZE_TOO_BIG);
			}
			
			if (CommonUtil.isObjectNull(file)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
			}
			
			strContentType = uploadRequest.getContentType(PortalConstants.PARAM_INSPECTION_FILE);
			strFileName = uploadRequest.getFileName(PortalConstants.PARAM_INSPECTION_FILE);
			
			if (CommonUtil.isStringNullOrEmpty(strContentType)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
			}
			
			if (!ControllerHelper.isZIPValid(strContentType, file)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_TYPE_INVALID, PortalConstants.ERROR_LOG_PARAM_ZIP));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_NOT_ZIP, PortalMessages.FILE_INVALID);
			}
			
			if (FileProcessUtil.getFileSize(file) == PortalConstants.DOUBLE_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_EMPTY, PortalMessages.FILE_INVALID);
			}
			
			if (FileProcessUtil.getFileSize(file) > PortalConstants.MAX_SIZE_VEX) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_ZIP));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG, PortalMessages.ZIP_FILE_SIZE_TOO_BIG_VEX);
			}
			
			if (lUserId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
				throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
			}
			
			user = UserLocalServiceUtil.getUser(lUserId);
			
			if (CommonUtil.isObjectNull(user)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
			}
			
			scan = ScanLocalServiceUtil.createScanObj();
			lScanId = ScanLocalServiceUtil.createScanId();
			scan.setScanId(lScanId);
			scan.setProjectId(lProjectId);
			scan.setScanManager(user.getEmailAddress());
			scan.setFileName(strFileName);
			scan.setStatus(ScanStatus.SCAN_WAITING.getInteger());
			scan.setRegistrationDate(new Date());
			scan.setModifiedDate(new Date());

			Scan updatedScan = ScanLocalServiceUtil.updateScan(scan);
			if (CommonUtil.isObjectNull(updatedScan)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
			}
			
			bSuccess = true;
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_ADD_SCAN, params, nspe);
			throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_ADD_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("scan", scan);
			params.put("file", file);
			params.put("project", project);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_ADD_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_SCAN, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lUserId", lUserId);
			params.put("file", file);
			params.put("strFileName", strFileName);
			params.put("strContentType", strContentType);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else if (strErrorCode.equals(PortalErrors.INVALID_FILE)) {
				ubspe.setErrorMessage(PortalMessages.FILE_INVALID);
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lUserId", lUserId);
				params.put("project", project);
				params.put("file", file);
				params.put("strFileName", strFileName);
				params.put("strContentType", strContentType);
				params.put("user", user);
				params.put("scan", scan);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_SCAN, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_SCAN, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean regenerateReport (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		String strCxSessionId = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Scan scan = null;
		boolean bGetProject = true;
		long lCxAndroidScanId = PortalConstants.LONG_ZERO;
		long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession(false);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			oUserId = session.getAttribute(PortalConstants.USER_ID);
			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}

				user = UserLocalServiceUtil.getUser(lUserId);
				
				if (CommonUtil.isObjectNull(user)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
				}
			}
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}

			List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
			ProjectLocalServiceUtil.clearCache();
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			boolean bIsProjectUser = false;
			
			if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
				bIsProjectUser = true;
			} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
				long[] userOrgIdList = user.getOrganizationIds();
				for (long userOrgId : userOrgIdList) {
					if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
						bIsProjectUser = true;
						break;
					}
				}
			} else {
				if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
					for (ProjectUsers pu : projectUsersList) {
						if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
							bIsProjectUser = true;
							break;
						}
					}
				}
			}
			
			if (!bIsProjectUser) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}

			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			if (lScanId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
			}

			scan = ScanLocalServiceUtil.retrieveScan(lScanId);
			
			@SuppressWarnings("unchecked")
			List<Long> scansRegeneratedList = (List<Long>) pSession.getAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT);
			
			if (CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
				scansRegeneratedList = new ArrayList<Long>();
			}
			
			scansRegeneratedList.add(lScanId);
			
			pSession.setAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT, scansRegeneratedList);
			
			if (CommonUtil.isObjectNull(scan)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
			}

			lProjectId = scan.getProjectId();
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}

			project = null;
			bGetProject = false;
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}

			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId())) {
				lCxAndroidProjectId = Long.parseLong(project.getCxAndroidProjectId());
			}
			
			int iStatus = ScanLocalServiceUtil.getScanStatus(lScanId);
			
			if (iStatus != ScanStatus.COMPLETE.getInteger() &&
					iStatus != ScanStatus.FAILURE.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_CANNOT_BE_ACTION, PortalConstants.ERROR_LOG_PARAM_REVIEW, PortalConstants.ERROR_LOG_ACTION_REGENERATE));
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_SCAN_NOT_IN_REVIEW_COMPLETE_STATE, PortalMessages.REPORT_CANNOT_BE_REGENERATED);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(scan.getCxAndroidScanId())) {
				lCxAndroidScanId = Long.parseLong(scan.getCxAndroidScanId());
			}
			
			ResultItem item = new ResultItem();
			
			item.setScanId(lScanId);
			item.setCxAndroidProjectId(String.valueOf(lCxAndroidProjectId));
			item.setCxAndroidScanId(String.valueOf(lCxAndroidScanId));
			item.setProjectId(lProjectId);
			item.setType(ProjectType.VEX.getInteger());
			
			bSuccess = true;
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_REGENERATE_REPORT, params, nsse);
			throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REGENERATE_REPORT, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}

			log.debug(PortalErrors.REGENERATE_REPORT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REGENERATE_REPORT, params, nspe);
			throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_REGENERATE_REPORT, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_REGENERATE_REPORT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_REGENERATE_REPORT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_REGENERATE_REPORT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lScanId", lScanId);
				params.put("strCxSessionId", strCxSessionId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_REGENERATE_REPORT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	private static List<ScanItem> getScans(ResourceRequest resourceRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		long lProjectId = PortalConstants.LONG_ZERO;
		String strScanId = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String strStatus = null;
		String strCxScanId = null;
		String strOrderByCol = null;
		String strOrderByType = null;
		DateFormat formatter = null;
		DateFormat datetimeFormatter = null;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		List<Object> scanList = null;
		List<ScanItem> returnList = null;
		int start = PortalConstants.INT_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Map<String, Object> searchedScan = new HashMap<String, Object>();
		String strStartUrl = PortalConstants.STRING_EMPTY;
		String strScanStartTime= PortalConstants.STRING_EMPTY;
		String strScanEndTime = PortalConstants.STRING_EMPTY;
		String strImplementationEnvironment = PortalConstants.STRING_EMPTY;
		Date dteStartTime = null;
		Calendar dteEndTime = null;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException (PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
			datetimeFormatter= new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
			start = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_START);
			lProjectId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_PROJECT_ID);
			strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID).trim();
			strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME).trim();
			strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE).trim();
			strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
			strScanRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
			strScanRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
			strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
			strCxScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_CX_SCAN_ID).trim();
			strOrderByCol = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_ORDER_BY_COL).trim();
			strOrderByType = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_ORDER_BY_TYPE).trim();
			strStartUrl = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_START_URL).trim();
			strScanStartTime= ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_START_TIME).trim();
			strScanEndTime = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_END_TIME).trim();
			strImplementationEnvironment = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT).trim();
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
				try {
					long lScanId = Long.parseLong(strScanId);
					
					if (lScanId < PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
				} catch (NumberFormatException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
				if (!Validator.isAlphanumericName(strHashValue)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
				}
				
				if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
				if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
				}
				
				dteRegDateLow = formatter.parse(strScanRegDateLow);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
				if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
				}
				
				dteRegDateHigh = Calendar.getInstance();
				dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
				dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
				dteRegDateHigh.set(Calendar.MINUTE, 59);
				dteRegDateHigh.set(Calendar.SECOND, 59);
			}
			
			if (dteRegDateLow != null && dteRegDateHigh != null) {
				if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE.toLowerCase()));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanStartTime) && !strScanStartTime.trim().equals("00:00")) {
				if (strScanStartTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_TIME));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
				}
				
				try{
					dteStartTime= datetimeFormatter.parse(strScanStartTime);
				}catch (ParseException pe) {
					params.put("dteStartTime", strScanStartTime);
					log.debug(PortalErrors.INVALID_SCAN_START_TIME, PortalConstants.METHOD_GET_SCANS, params, pe);
					throw new UBSPortalException(PortalErrors.INVALID_SCAN_START_TIME, PortalMessages.START_TIME_INVALID, pe);
				} 
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanEndTime) && !strScanEndTime.trim().equals("00:00")) {
				if (strScanEndTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_END_TIME));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
				}
				try{
					dteEndTime = Calendar.getInstance();
					dteEndTime.setTime(datetimeFormatter.parse(strScanEndTime));
				}catch (ParseException pe) {
					params.put("dteEndTime", strScanEndTime);
					log.debug(PortalErrors.INVALID_SCAN_END_TIME, PortalConstants.METHOD_GET_SCANS, params, pe);
					throw new UBSPortalException(PortalErrors.INVALID_SCAN_END_TIME, PortalMessages.END_TIME_INVALID, pe);
				} 
			}
			
			if (!CommonUtil.isObjectNull(dteStartTime) && !CommonUtil.isObjectNull(dteEndTime)) {
				if (dteStartTime.after(dteEndTime.getTime())) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_START_TIME, PortalConstants.ERROR_LOG_PARAM_START_TIME.toLowerCase()));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_AND_END_TIME_INVALID, PortalMessages.START_TIME_AND_END_TIME_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
				try {
					long lCxScanId = Long.parseLong(strCxScanId);
					
					if (lCxScanId < PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
					}
				} catch (NumberFormatException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
				}
			}
			
			Object oUserId = session.getAttribute(PortalConstants.USER_ID);
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
			}
			
			if (lUserId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
				throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
			}
			user = UserLocalServiceUtil.getUser(lUserId);
			
			searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
			searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
			searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
			searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
			searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
			searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
			searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
			searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
			searchedScan.put(PortalConstants.PARAM_SCAN_START_URL, strStartUrl);
			searchedScan.put(PortalConstants.PARAM_SCAN_START_TIME, strScanStartTime);
			searchedScan.put(PortalConstants.PARAM_SCAN_END_TIME, strScanEndTime);
			searchedScan.put(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT, strImplementationEnvironment);
			
			scanList = ScanLocalServiceUtil.sortScans(lProjectId, searchedScan, strOrderByCol, strOrderByType, start);
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				returnList = new ArrayList<ScanItem>();
				long lCompanyId = user.getCompanyId();
				
				for (Object item : scanList) {
					long lScanId 									= PortalConstants.LONG_ZERO;
					int scanProjectType								= PortalConstants.INT_ZERO;
					ScanItem scan 									= (ScanItem) item;
					List<VexLoginSettingItem> loginSettingList 		= null;
					VexScanSetting scanSetting 						= null;	
											
					try {
						user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
						
						if (!CommonUtil.isObjectNull(user)) {
							scan.setScanManager(user.getFirstName());
						} else {
							scan.setScanManager(PortalConstants.STRING_EMPTY);
						}
					} catch (NoSuchUserException e) {
						scan.setScanManager(PortalConstants.STRING_EMPTY);
					}
					
					try {
						//Get Scan ID
						lScanId = scan.getScanId();
						scanProjectType = scan.getProjectType();
						
						if(scanProjectType == PortalConstants.PROJECT_TYPE_VEX){
							if(lScanId > PortalConstants.LONG_ZERO)
							{
								//Get Login Information Setting
								loginSettingList = VexScanMgmtController.getVexLoginSettingList(lScanId);
								
								//Get Vex Scan Settings
								scanSetting = VexScanMgmtController.getVexScanSetting(lScanId);
							}
							
							scan.setLoginSetting(loginSettingList);
														
							if(scanSetting != null)
							{
								scan.setMaximumDetectionLinks(scanSetting.getMaxnumdetectionlink());
							}
							
						} else {
							scan.setMaximumDetectionLinks(0);
							scan.setLoginSetting(loginSettingList);
						}
						
					} catch (Exception e) {
						scan.setMaximumDetectionLinks(0);
						scan.setLoginSetting(loginSettingList);
					} 
					
					returnList.add(scan);
				}
			}
			
			
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put("user", user);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_GET_SCANS, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
		} catch (ParseException pe) {
			params.put("strRegDateLow", strScanRegDateLow);
			params.put("strRegDateHigh", strScanRegDateHigh);
			log.debug(PortalErrors.INVALID_SCAN_REGISTRATION_DATE, PortalConstants.METHOD_GET_SCANS, params, pe);
			throw new UBSPortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE, PortalMessages.SCAN_REGISTRATION_DATE_INVALID, pe);
		} catch (SystemException se) {
			params.put("searchedScan", searchedScan);
			params.put("lUserId", lUserId);
			params.put("user", user);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("searchedScan", searchedScan);
			params.put("lUserId", lUserId);
			params.put("user", user);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCANS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (NumberFormatException nfe) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.GET_USER_USER_ID_INVALID, PortalConstants.METHOD_GET_SCANS, params, nfe);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.COMMON_EXCEPTION, nfe);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("searchedScan", searchedScan);
				params.put("lUserId", lUserId);
				params.put("user", user);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_SCANS, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_SCANS, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
		
		return returnList;
	}
	
	private static List<ResultItem> getEntireScans (ResourceRequest resourceRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = null;
		List<ResultItem> returnList = null;
		String strScanId = null;
		String strProjectName = null;
		String strGroupName = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String strStatus = null;
		String strCxScanId = null;
		String strOrderByCol = null;
		String strOrderByType = null;
		DateFormat formatter = null;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		int iStart = PortalConstants.INT_ZERO;
		Map<String, Object> searchedScan = new HashMap<String, Object>();
		String strStartUrl = PortalConstants.STRING_EMPTY;
		String strScanStartTime= PortalConstants.STRING_EMPTY;
		String strScanEndTime = PortalConstants.STRING_EMPTY;
		String strImplementationEnvironment = PortalConstants.STRING_EMPTY;
		Date dteStartTime = null;
		Calendar dteEndTime = null;
		DateFormat datetimeFormatter = null;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
			iStart = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_START);
			strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID).trim();
			strProjectName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
			strGroupName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_OWNER_GROUP).trim();
			strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME).trim();
			strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE).trim();
			strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
			strScanRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
			strScanRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
			strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
			strCxScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_CX_SCAN_ID).trim();
			strOrderByCol = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_ORDER_BY_COL).trim();
			strOrderByType = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_ORDER_BY_TYPE).trim();
			strStartUrl = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_START_URL).trim();
			strScanStartTime= ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_START_TIME).trim();
			strScanEndTime = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_END_TIME).trim();
			strImplementationEnvironment = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT).trim();
			
			if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
				try {
					long lScanId = Long.parseLong(strScanId);
					
					if (lScanId < PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
				} catch (NumberFormatException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
				if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
				}
				
				if (!ControllerHelper.isCxProjectNameValid(strProjectName)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_INVALID, PortalMessages.PROJECT_NAME_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
				if (CommonUtil.getStringBytes(strGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_GROUP_NAME_TOO_LONG, PortalMessages.GROUP_NAME_TOO_LONG);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
				if (!Validator.isAlphanumericName(strHashValue)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
				}
				
				if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
				}
			}
			
			try {
				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
					}
					
					dteRegDateLow = formatter.parse(strScanRegDateLow);
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
					}
					
					dteRegDateHigh = Calendar.getInstance();
					dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
					dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
					dteRegDateHigh.set(Calendar.MINUTE, 59);
					dteRegDateHigh.set(Calendar.SECOND, 59);
				}
				
				if (!CommonUtil.isObjectNull(dteRegDateLow)
						&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
					if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE.toLowerCase()));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
					}
				}
				
			} catch (ParseException e) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanStartTime) && !strScanStartTime.trim().equals("00:00")) {
				if (strScanStartTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_TIME));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
				}
				
				try{
					dteStartTime= datetimeFormatter.parse(strScanStartTime);
				}catch (ParseException pe) {
					params.put("dteStartTime", strScanStartTime);
					log.debug(PortalErrors.INVALID_SCAN_START_TIME, PortalConstants.METHOD_GET_SCANS, params, pe);
					throw new UBSPortalException(PortalErrors.INVALID_SCAN_START_TIME, PortalMessages.START_TIME_INVALID, pe);
				} 
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanEndTime) && !strScanEndTime.trim().equals("00:00")) {
				if (strScanEndTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_END_TIME));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
				}
				try{
					dteEndTime = Calendar.getInstance();
					dteEndTime.setTime(datetimeFormatter.parse(strScanEndTime));
				}catch (ParseException pe) {
					params.put("dteEndTime", strScanEndTime);
					log.debug(PortalErrors.INVALID_SCAN_END_TIME, PortalConstants.METHOD_GET_SCANS, params, pe);
					throw new UBSPortalException(PortalErrors.INVALID_SCAN_END_TIME, PortalMessages.END_TIME_INVALID, pe);
				} 
			}
			
			if (!CommonUtil.isObjectNull(dteStartTime) && !CommonUtil.isObjectNull(dteEndTime)) {
				if (dteStartTime.after(dteEndTime.getTime())) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_START_TIME, PortalConstants.ERROR_LOG_PARAM_START_TIME.toLowerCase()));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_AND_END_TIME_INVALID, PortalMessages.START_TIME_AND_END_TIME_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
				try {
					long lCxScanId = Long.parseLong(strCxScanId);
					
					if (lCxScanId < PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
					}
				} catch (NumberFormatException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
				}
			}
			
			searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
			searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
			searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
			searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
			searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
			searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
			searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
			searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
			searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
			searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
			searchedScan.put(PortalConstants.PARAM_SCAN_START_URL, strStartUrl);
			searchedScan.put(PortalConstants.PARAM_SCAN_START_TIME, strScanStartTime);
			searchedScan.put(PortalConstants.PARAM_SCAN_END_TIME, strScanEndTime);
			searchedScan.put(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT, strImplementationEnvironment);
			
			scanList = ScanLocalServiceUtil.sortEntireScans(ProjectType.VEX.getInteger(), searchedScan, strOrderByCol, strOrderByType, iStart);
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				returnList = new ArrayList<ResultItem>();
				List<User> userList = UserLocalServiceUtil.getUsers(PortalConstants.INT_ZERO, PortalConstants.INT_ONE);
				long lCompanyId = PortalConstants.LONG_ZERO;
				
				if (!CommonUtil.isListNullOrEmpty(userList)) {
					for (User user : userList) {
						lCompanyId = user.getCompanyId();
						break;
					}
					
					for (Object item : scanList) {
						ResultItem scan = (ResultItem) item;
						User user = null;
						
						try {
							user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
							
							if (CommonUtil.isObjectNull(user)) {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							} else {
								scan.setScanManager(user.getFirstName());
							}
						} catch (NoSuchUserException e) {
							scan.setScanManager(PortalConstants.STRING_EMPTY);
						}
						
						returnList.add(scan);
					}
				}
			}
		} catch (PortalException pe) {
			params.put("searchedScan", searchedScan);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("searchedScan", searchedScan);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, ubspe);
			}
			
			throw ubspe;
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
		
		return returnList;
	}
	
	public static void getScansForRefresh(ResourceRequest resourceRequest, ResourceResponse resourceResponse, PortletConfig portletConfig) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<ScanItem> scanList = null;
		Project project = null;
		String strScanId = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String strHashValue = PortalConstants.STRING_EMPTY;
		String strScanManager = PortalConstants.STRING_EMPTY;
		String strRegDateLow = PortalConstants.STRING_EMPTY;
		String strRegDateHigh = PortalConstants.STRING_EMPTY;
		String strStatus = PortalConstants.STRING_EMPTY;
		String strCxScanId = PortalConstants.STRING_EMPTY;
		String strStartUrl = PortalConstants.STRING_EMPTY;
		String strScanStartTime= PortalConstants.STRING_EMPTY;
		String strScanEndTime = PortalConstants.STRING_EMPTY;
		String strImplementationEnvironment = PortalConstants.STRING_EMPTY;
		PortletSession pSession = resourceRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		boolean bHasError = false;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			if (!CommonUtil.isObjectNull(oLoggedIn) && Boolean.parseBoolean(oLoggedIn.toString())) {
				project = VexProjectMgmtController.getProject(resourceRequest);
				scanList = getScans(resourceRequest);
			
				strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID);
				strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME);
				strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE);
				strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER);
				strRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW);
				strRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH);
				strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
				strCxScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_CX_SCAN_ID);
				strStartUrl = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_START_URL);
				strScanStartTime= ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_START_TIME);
				strScanEndTime = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_END_TIME);
				strImplementationEnvironment = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
				
				if(!CommonUtil.isStringNullOrEmpty(strScanStartTime)){
					String strStartHour = ParamUtil.getString(resourceRequest, "startHourTime").trim();
					strScanStartTime+=" "+strStartHour;
				}
				
				if(!CommonUtil.isStringNullOrEmpty(strScanEndTime)){
					String strEndHour = ParamUtil.getString(resourceRequest, "endHourTime").trim();
					strScanEndTime +=" "+strEndHour;
				}
				
				Map<String, Object> searchedScan = new HashMap<String, Object>();
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strRegDateLow);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strRegDateHigh);
				searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
				searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
				searchedScan.put(PortalConstants.PARAM_SCAN_START_URL, strStartUrl);
				searchedScan.put(PortalConstants.PARAM_SCAN_START_TIME, strScanStartTime);
				searchedScan.put(PortalConstants.PARAM_SCAN_END_TIME, strScanEndTime);
				searchedScan.put(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT, strImplementationEnvironment);
				
				@SuppressWarnings("unchecked")
				List<Long> scansRegeneratedList = (List<Long>) pSession.getAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT);
				
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
					for (ScanItem scan : scanList) {
						String errorMessage = scan.getFailedScanCause();
						long lScanId = scan.getScanId();
						int iStatus = scan.getScanStatus();
						
						if (!CommonUtil.isStringNullOrEmpty(errorMessage)
								&& iStatus == ScanStatus.FAILURE.getInteger()) {
							
							if (!CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
								Iterator<Long> it = scansRegeneratedList.iterator();
								
								while (it.hasNext()) {
									if (it.next().longValue() == lScanId) {
										it.remove();
										if (!bHasError) {
											resourceRequest.setAttribute(PortalConstants.PARAM_CX_DOES_NOT_EXIST_ERROR_MSG, errorMessage);
											bHasError = true;
										}
										break;
									}
								}
							}
						} else if (iStatus == ScanStatus.COMPLETE.getInteger()) {
							if (!CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
								Iterator<Long> it = scansRegeneratedList.iterator();
								
								while (it.hasNext()) {
									if (it.next().longValue() == lScanId) {
										it.remove();
										break;
									}
								}
							}
						}
					}
					
					pSession.setAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT, scansRegeneratedList);
				}
				
				resourceRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				resourceRequest.setAttribute(PortalConstants.PARAM_SCAN_LIST, scanList);
				resourceRequest.setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
				resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_TEXT_HTML);

				portletConfig.getPortletContext().getRequestDispatcher(PortalConstants.VEX_SCAN_LIST_REFRESH_JSP).include(resourceRequest, resourceResponse);
			}
		}  catch (PortletException e) {
			params.put("portletConfig", portletConfig);
			log.debug(PortalErrors.PORTLET_EXCEPTION, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.PORTLET_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (IOException e) {
			params.put("portletConfig", portletConfig);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (UBSPortalException e) {
			String strErrorCode = e.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("project", project);
				params.put("scanList", scanList);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			}
			throw e;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
	}
	
	public static void getEntireScansForRefresh(ResourceRequest resourceRequest, ResourceResponse resourceResponse, PortletConfig portletConfig) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<ResultItem> entireScanList = null;
		String strScanId = PortalConstants.STRING_EMPTY;
		String strProjectName = PortalConstants.STRING_EMPTY;
		String strGroupName = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String strHashValue = PortalConstants.STRING_EMPTY;
		String strScanManager = PortalConstants.STRING_EMPTY;
		String strRegDateLow = PortalConstants.STRING_EMPTY;
		String strRegDateHigh = PortalConstants.STRING_EMPTY;
		String strStatus = PortalConstants.STRING_EMPTY;
		String strCxScanId = PortalConstants.STRING_EMPTY;
		String strStartUrl = PortalConstants.STRING_EMPTY;
		String strScanStartTime= PortalConstants.STRING_EMPTY;
		String strScanEndTime = PortalConstants.STRING_EMPTY;
		String strImplementationEnvironment = PortalConstants.STRING_EMPTY;
		
		PortletSession pSession = resourceRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		boolean bHasError = false;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			if (!CommonUtil.isObjectNull(oLoggedIn) && Boolean.parseBoolean(oLoggedIn.toString())) {
				entireScanList = getEntireScans(resourceRequest);
				
				strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID);
				strProjectName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROJECT_NAME);
				strGroupName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_OWNER_GROUP);
				strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME);
				strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE);
				strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER);
				strRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW);
				strRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH);
				strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
				strCxScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_CX_SCAN_ID);
				strStartUrl = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_START_URL);
				strScanStartTime= ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_START_TIME);
				strScanEndTime = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_END_TIME);
				strImplementationEnvironment = ParamUtil.getString(resourceRequest,PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
				
				Map<String, Object> searchedScan = new HashMap<String, Object>();
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
				searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
				searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strRegDateLow);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strRegDateHigh);
				searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
				searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
				searchedScan.put(PortalConstants.PARAM_SCAN_START_URL, strStartUrl);
				searchedScan.put(PortalConstants.PARAM_SCAN_START_TIME, strScanStartTime);
				searchedScan.put(PortalConstants.PARAM_SCAN_END_TIME, strScanEndTime);
				searchedScan.put(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT, strImplementationEnvironment);
				
				@SuppressWarnings("unchecked")
				List<Long> scansRegeneratedList = (List<Long>) pSession.getAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT);
				
				if (!CommonUtil.isListNullOrEmpty(entireScanList)) {
					for (ResultItem scan : entireScanList) {
						String errorMessage = scan.getFailedScanCause();
						long lScanId = scan.getScanId();
						int iStatus = scan.getStatus();
						
						if (!CommonUtil.isStringNullOrEmpty(errorMessage)
								&& iStatus == ScanStatus.FAILURE.getInteger()) {
							
							 if (!CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
								 Iterator<Long> it = scansRegeneratedList.iterator();
								 
								 while (it.hasNext()) {
									 if (it.next().longValue() == lScanId) {
										 it.remove();
										 if (!bHasError) {
											 resourceRequest.setAttribute(PortalConstants.PARAM_CX_DOES_NOT_EXIST_ERROR_MSG, errorMessage);
											 bHasError = true;
										 }
										 break;
									 }
								 }
							 }
						} else if (iStatus == ScanStatus.COMPLETE.getInteger()) {
							if (!CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
								Iterator<Long> it = scansRegeneratedList.iterator();
								 
								 while (it.hasNext()) {
									 if (it.next().longValue() == lScanId) {
										 it.remove();
										 break;
									 }
								 }
							}
						}
					}
					
					pSession.setAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT, scansRegeneratedList);
				}
				
				resourceRequest.setAttribute(PortalConstants.PARAM_SCAN_LIST, entireScanList);
				resourceRequest.setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
				resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_TEXT_HTML);
				
				portletConfig.getPortletContext().getRequestDispatcher(PortalConstants.VEX_ENTIRE_SCAN_LIST_REFRESH_JSP).include(resourceRequest, resourceResponse);
			}
		} catch (PortletException e) {
			params.put("resourceRequest", resourceRequest);
			params.put("resourceResponse", resourceResponse);
			log.debug(PortalErrors.PORTLET_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.PORTLET_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (IOException e) {
			params.put("resourceRequest", resourceRequest);
			params.put("resourceResponse", resourceResponse);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("resourceRequest", resourceRequest);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(entireScanList)) {
				entireScanList = null;
			}
		}
	}
		
	private static void reCrawl(ApiInitInfo info, String scanId) {
		new Thread(() -> threadRecrawl(info, scanId)).start();
					
	}
	
	private static String abortCrawl(ApiInitInfo info, String scanId) {
		Map<String, Object> params = null;
		String result = "";
		int status = 0;
		try{
			InterfaceStopAutoCrawl stop = VexInstanceFactory.getStopAutoCrawl(info);
			for(int x = 1; x < 5; x++){
				if(stop.execute(scanId))
					result = "" ;
				else
					result = stop.getErrorMessage();
				Thread.sleep(1000);
				status = getScanStatus(info, scanId);
				if(status == ScanStatus.CRAWLING_FAILURE.getInteger()){
					break;
				}
			}
			
		}catch(Exception e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.ABORT_CRAWL_FAILED, PortalConstants.METHOD_ABORT_CRAWL, params, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.ABORT_CRAWL_FAILED, PortalConstants.METHOD_ABORT_CRAWL, params, e.getCause());
		}
		return result;
	}
	
	private static Map<String, Object> resendDetectionResult(ApiInitInfo info, String scanId, String signatureNo) {
		Map<String, Object> params = new HashMap<String, Object>();
		ApiResendResultData resendResult = null;
		Map<String, Object> resultObj = new HashMap<String, Object>();
		InterfaceResend resend = null;
		try{
			resend = VexInstanceFactory.getResend(info);
			resend.execute(scanId, signatureNo);
			if(resend.getResult()){
				resendResult = resend.GetExecuteResult();
			}else{
				throw new UBSPortalException(PortalErrors.RESEND_FAILED, resend.getErrorMessage(), null);
			}
			
		}catch(Exception e){
			params.put("Vex Scan Id", scanId);
			params.put("Signature Number", signatureNo);
			log.debug(PortalErrors.RESEND_FAILED, PortalConstants.METHOD_RESEND_DETECTION_RESULT, params, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			params.put("Signature Number", signatureNo);
			log.debug(PortalErrors.RESEND_FAILED, PortalConstants.METHOD_RESEND_DETECTION_RESULT, params, e.getCause());
		}finally{
			resultObj.put(PortalConstants.STR_DETECTION_RESEND_RESULT, resendResult);
			resultObj.put(PortalConstants.STR_DETECTION_RESEND, resend);
		}
		return resultObj;
	}
	
	public static String cancelCrawl(ApiInitInfo info, String scanId) {
		Map<String, Object> params = new HashMap<String, Object>();
		String result = "";
		int status = 0;
		try{
			InterfaceStopAutoCrawl cancel = VexInstanceFactory.getStopAutoCrawl(info);
			for(int x = 1; x < 5; x++){
				if(cancel.execute(scanId))
					result = "";
				else
					result = cancel.getErrorMessage();
				Thread.sleep(1000);
				status = getScanStatus(info, scanId);
				if(status == ScanStatus.CRAWLING_FAILURE.getInteger()){
					break;
				}
			}
		}catch(Exception e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.CANCEL_CRAWL_FAILED, PortalConstants.METHOD_CANCEL_CRAWL, params, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.CANCEL_CRAWL_FAILED, PortalConstants.METHOD_CANCEL_CRAWL, params, e.getCause());
		}
		return result;
	}
	
	private static String interruptedCrawl(ApiInitInfo info, String scanId) {
		Map<String, Object> params = null;
		String result = "";
		int status = 0;
		try{
			InterfacePause interrupted = VexInstanceFactory.getPause(info);
			for(int x = 1; x < 5; x++){
				if(interrupted.execute(scanId, ApiScanType.TYPE_CRAWL))
					result = "";
				else
					result = interrupted.getErrorMessage();
				Thread.sleep(1000);
				status = getScanStatus(info, scanId);
				if(status == ScanStatus.CRAWLING_INTERRUPTED.getInteger()){
					break;
				}
			}
		}catch(Exception e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.INTERRUPTED_CRAWL_FAILED, PortalConstants.METHOD_INTERRUPTED_CRAWL, params, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.INTERRUPTED_CRAWL_FAILED, PortalConstants.METHOD_INTERRUPTED_CRAWL, params, e.getCause());
		}
		return result;
	}

	private static void restartCrawl(ApiInitInfo info, String scanId) {
		new Thread(() -> threadRestartCrawl(info, scanId)).start();
	}

	private static String interruptedScan(ApiInitInfo info, String scanId) {
;		Map<String, Object> params = null;
		String result = "";
		int status = 0;
		try{
			for(int x = 1; x < 5; x++){
				InterfacePause interrupted = VexInstanceFactory.getPause(info);
				interrupted.execute(scanId, ApiScanType.TYPE_AUDIT);
				if(interrupted.getResult())
					result = "";
				else
					result = interrupted.getErrorMessage();
				Thread.sleep(2000);
				status = getFailScanStatus(info, scanId);
				if(status == ScanStatus.FAILURE.getInteger()){
					break;
				}
			}
		}catch(Exception e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.INTERRUPTED_SCAN_FAILED, PortalConstants.METHOD_INTERRUPTED_SCAN, params, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.INTERRUPTED_SCAN_FAILED, PortalConstants.METHOD_INTERRUPTED_SCAN, params, e.getCause());
		}
		return result;
	}
 
	public static String cancelScan(ApiInitInfo info, String scanId) {
		Map<String, Object> params = null;
		String result = "";
		int status = 0;
		try{
			
			for(int x = 1; x < 5; x++){
				InterfacePause cancel = VexInstanceFactory.getPause(info);
				cancel.execute(scanId, ApiScanType.TYPE_AUDIT);
				if(cancel.getResult())
					result = "";
				else
					result = cancel.getErrorMessage();
				Thread.sleep(1000);
				status = getFailScanStatus(info, scanId);
				if(status == ScanStatus.FAILURE.getInteger()){
					break;
				}
			}
		}catch(Exception e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.CANCEL_SCAN_FAILED, PortalConstants.METHOD_CANCEL_SCAN, params, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.CANCEL_SCAN_FAILED, PortalConstants.METHOD_CANCEL_SCAN, params, e.getCause());
		}
		return result;
	}
	
	private static String stopScan(ApiInitInfo info, String scanId) {
		Map<String, Object> params = null;
		String result = "";
		int status = 0;
		try{
			for(int x = 1; x < 5; x++){
				InterfacePause stop = VexInstanceFactory.getPause(info);
				stop.execute(scanId, ApiScanType.TYPE_AUDIT);
				if(stop.getResult())
					result = "";
				else
					result = stop.getErrorMessage();
				Thread.sleep(1000);
				status = getScanStatus(info, scanId);
				if(status == ScanStatus.FAILURE.getInteger()){
					break;
				}
			}
		}catch(Exception e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.STOP_SCAN_FAILED, PortalConstants.METHOD_STOP_SCAN, params, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.STOP_SCAN_FAILED, PortalConstants.METHOD_STOP_SCAN, params, e.getCause());
		}
		return result;
	}
	
	private static InterfaceProjectImport importScan(ApiInitInfo info, String scanName, String filePath) {
		Map<String, Object> params = new HashMap<String, Object>();
		InterfaceProjectImport importProject = null;
		String importStatus = PortalConstants.STRING_EMPTY;
		try{
			importProject =  VexInstanceFactory.getProjectImport(info);
			importProject.execute(scanName, filePath);
			do{
				Thread.sleep(5000L);
				importStatus = importProject.GetStatus();
			}while(importStatus.equals("インポ�?�ト中で�?..."));

		}catch(Exception e){
			params.put("error", importProject.getErrorMessage());
			log.debug(PortalErrors.IMPORT_SCAN_FAILED, PortalConstants.METHOD_IMPORT_SCAN, params, e.getCause());
		}catch (Throwable e){
			params.put("error", importProject.getErrorMessage());
			log.debug(PortalErrors.IMPORT_SCAN_FAILED, PortalConstants.METHOD_IMPORT_SCAN, params, e.getCause());
		}
				
		return importProject;
	}

	private static void restartScan(ApiInitInfo info, String scanId) {
		new Thread(() -> threadRestartScan(info, scanId)).start();
	}
	
	public static String deleteScan(ApiInitInfo info, String scanId, int status) {
		Map<String, Object> params = null;
		String result = "";
		String stopMessage = "";
		boolean isDeleted = false;
		try{
			InterfaceProjectDelete delete =  VexInstanceFactory.getProjectDelete(info);
			for(int x = 0 ; x < 5 ; x++){
				if(status == ScanStatus.CRAWLING.getInteger() || status == ScanStatus.CRAWLING_INTERRUPTED.getInteger() ){
					stopMessage = abortCrawl(info, scanId);
				}
				if(status == ScanStatus.SCANNING.getInteger()){
					stopMessage = stopScan(info, scanId);
				}
				
				if(CommonUtil.isStringNullOrEmpty(stopMessage)){
					do{
						Thread.sleep(5000);
						delete.execute(scanId);
						result = "";
						log.info("InterfaceProjectDelete", "Vex Scan Id: "+scanId +" delete in progress.");
						if(getScanStatus(info, scanId) == 0){
							isDeleted = true;
						}else{
							isDeleted = delete.getResult();
						}
					}while(!isDeleted);
				}
				else{
					result = delete.getErrorMessage() + " " + stopMessage;
				}
				
				if((CommonUtil.isStringNullOrEmpty(result) && delete.getResult()) || isDeleted){
					log.info("InterfaceProjectDelete", "Vex Scan Id: "+scanId +" was successfully deleted.");
					break;
				}else{
					status = getScanStatus(info, scanId);
				}
			}
		}catch(Exception e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.DELETE_SCAN_FAILED, PortalConstants.METHOD_DELETE_SCAN, params, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.DELETE_SCAN_FAILED, PortalConstants.METHOD_DELETE_SCAN, params, e.getCause());
		}
		return result;
	}
		
	@SuppressWarnings("unchecked")
	private static int getImportScanStatusFromInspectionLog(String zipFile, String unzipFolder){
		int iStatus = 0;
		Map<String,Object> params = new HashMap<String, Object>();
    	params.put("ZipFile", zipFile);
    	params.put("UnzipFolder", unzipFolder);
		try{
			if(FileProcessUtil.unzipVexFile(zipFile)){
				File crawlFolder = new File(unzipFolder + File.separator + PortalConstants.CRAWLER_LOG_FOLDER);
				File proxyFolder = new File(unzipFolder + File.separator + PortalConstants.PROXY_LOG_FOLDER);
				File backendFolder = new File(unzipFolder + File.separator + PortalConstants.BACKEND_RESULT_FOLDER);
				File auditFolder = new File(unzipFolder + File.separator + PortalConstants.AUDIT_RESULT_FOLDER);
				File slithyFolder = new File(unzipFolder + File.separator + PortalConstants.SLITHY_RESULT_FOLDER);
				
				boolean hasCrawlFolder = FileProcessUtil.hasFilesInFolder(crawlFolder);
				boolean hasProxyFolder = FileProcessUtil.hasFilesInFolder(proxyFolder);
				boolean hasAuditFolder = FileProcessUtil.hasFilesInFolder(auditFolder);
				boolean hasBackendFolder = FileProcessUtil.hasFilesInFolder(backendFolder);
				boolean hasSlithyFolder = FileProcessUtil.hasFilesInFolder(slithyFolder);
								
				if (hasCrawlFolder || hasProxyFolder){
					if(hasAuditFolder && hasBackendFolder && hasSlithyFolder){
						iStatus = PortalConstants.REPORT_MAKING;
					}else{
						iStatus = PortalConstants.SCAN_WAITING;
					}
				}else{
					iStatus = PortalConstants.CRAWLING_WAITING;
				}
			}
		}catch (Exception e){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
		} finally{
			params= null;
			File fileZip = new File(zipFile);
			File fileTargetFolder = new File(unzipFolder);
			if(fileZip.exists()){
				fileZip.delete();
			}
			if(fileTargetFolder.exists()){
				try {
					FileUtils.deleteDirectory(fileTargetFolder);
				} catch (IOException e) {
					params = new HashMap<String, Object>();
					params.put("UnzipFolder", unzipFolder);
					log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
				}
			}
		}
		
		return iStatus;
	}
				
	private static int getScanStatus(ApiInitInfo info, String scanId) {
		InterfaceGetStatus status =  VexInstanceFactory.getGetStatus(info);
		Map<String, Object> mapJSON = null;
		int iResponse = 0;
		
		try{
			mapJSON = (Map<String, Object>) status.execute(scanId);
		}catch(Throwable e){
			//do nothing  
		}
		
		if(!CommonUtil.isObjectNull(mapJSON)){
			Map<String, Object> mapStatus = (Map<String, Object>) mapJSON.get("d");
			String crawler = mapStatus.get("crawler").toString();
			String scan = mapStatus.get("audit").toString();
			String optimizer = mapStatus.get("optimizer").toString();
			String slithy = mapStatus.get("slithy").toString();
			String backend = mapStatus.get("backend").toString();
			
			if(!crawler.equals("complete")){
				iResponse = getCrawlerStatus(crawler);
			}else if (!optimizer.equals("complete")){
				iResponse = getOptimizerStatus(optimizer);
			}else{
				iResponse = getScanStatus(scan);
				
				if(scan.equals("complete")){
					if(!backend.equals("complete")){
						iResponse = getBackEndStatus(backend);
					}else if(!slithy.equals("complete")){
						iResponse = getSlithyStatus(slithy);
					}
				}
			}
		}
		return iResponse;
	}
	
	/**
	 * Get scan status only. Crawl not included
	 * @param info
	 * @param scanId
	 * @return
	 */
	private static int getFailScanStatus(ApiInitInfo info, String scanId) {
		InterfaceGetStatus status =  VexInstanceFactory.getGetStatus(info);
		Map<String, Object> mapJSON = null;
		
		int iResponse = 0;
		try{
			mapJSON = (Map<String, Object>) status.execute(scanId);
		}catch(Throwable e){
			//do nothing
		}
		
		if(!CommonUtil.isObjectNull(mapJSON)){
			Map<String, Object> mapStatus = (Map<String, Object>) mapJSON.get("d");
			String crawler = mapStatus.get("crawler").toString();
			String scan = mapStatus.get("audit").toString();
			String optimizer = mapStatus.get("optimizer").toString();
			int ibackEnd = 0;
			int iSlithy = 0;
			
			iResponse = getScanStatus(scan);
				
			if(scan.equals("complete")){
				String slithy = mapStatus.get("slithy").toString();
				String backend = mapStatus.get("backend").toString();
				
				if(!backend.equals("complete")){
					ibackEnd = getBackEndStatus(backend);
				}
				
				if(!slithy.equals("complete")){
					iSlithy = getSlithyStatus(slithy);
				}
				
				if(iSlithy == ScanStatus.FAILURE.getInteger() || ibackEnd == ScanStatus.FAILURE.getInteger()){
					iResponse =  ScanStatus.FAILURE.getInteger();
				}
			}
		}
		return iResponse;
	}
	
	private static int getCrawlerStatus(String strResponse) {
		int iStatus = 0;
		switch(strResponse){
			case "before_start":
			case "waiting":
				iStatus = ScanStatus.CRAWLING_WAITING.getInteger();
				break;
			case "start":
			case "resume":
				iStatus = ScanStatus.CRAWLING.getInteger();
				break;
			case "pause":
				iStatus = ScanStatus.CRAWLING_INTERRUPTED.getInteger();
				break;
			case "stopped":
			case "failure":
				iStatus = ScanStatus.CRAWLING_FAILURE.getInteger();
				break;
		}
		
		return iStatus;
	}
	
	private static int getOptimizerStatus(String strResponse) {
		int iStatus = 0;
		switch(strResponse){
			case "before_start":
			case "waiting":
			case "start":
			case "resume":
				iStatus = ScanStatus.CRAWLING.getInteger();
				break;
			case "pause":
				iStatus = ScanStatus.CRAWLING_INTERRUPTED.getInteger();
				break;
			case "stopped":
			case "failure":
				iStatus = ScanStatus.CRAWLING_FAILURE.getInteger();
				break;
		}
		return iStatus ;
	}
	
	private static int getScanStatus(String strResponse) {
		int iStatus = 0;
		switch(strResponse){
			case "waiting":
				iStatus = ScanStatus.SCAN_WAITING.getInteger();
				break;
			case "start":
				iStatus = ScanStatus.SCANNING.getInteger();
				break;
			case "pause":
				iStatus = ScanStatus.SCAN_INTERRUPTED.getInteger();
				break;
			case "stopped":
				iStatus = ScanStatus.FAILURE.getInteger();
				break;
			case "complete":
				iStatus = ScanStatus.REPORT_MAKING.getInteger();
				break;
		}
		
		return iStatus;
	}
	
	public static List<VexLoginSettingItem> getCrawlLoginInfo(Long lProjectId, String targetInfo, String loginPath) throws UBSPortalException {
		List<VexLoginSettingItem> list = new ArrayList<VexLoginSettingItem>();
		Map<String, Object> params = new HashMap<String, Object>();
		String tempprojectadd_filepath = null;
		String newID = null;
		ApiInitInfo oInitInfo = null;

		try {
			oInitInfo = VexScanMgmtController.setLoginCredentials();
			File fileDirectory = FileProcessUtil.createTempProjectYAMLPath(lProjectId);
			
			if (fileDirectory.isDirectory()) {
				tempprojectadd_filepath = createProjectAddTempFile(fileDirectory, targetInfo);
			}
			
			oInitInfo = VexScanMgmtController.setLoginCredentials();

			try {
				InterfaceProjectAdd projectAdd = VexInstanceFactory.getProjectAdd(oInitInfo);
				newID = projectAdd.executeEX(tempprojectadd_filepath);
			} catch (Throwable e) {
				params.put("Temp Project YML File: ", tempprojectadd_filepath);
				params.put(PortalConstants.ERROR_LOG_MESSAGE + ": ", e.getCause());
				throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.GET_CRAWL_LOGIN_INFO_FAILED, e);
			}
			
			deleteYmlFile(tempprojectadd_filepath);	
			
			String loginInfoParams = null;
			
				InterfaceAutoCrawlGetLoginParameters getLoginInfo = VexInstanceFactory.getAutoCrawlGetLoginParameters(oInitInfo);
				loginInfoParams = getLoginInfo.execute(newID, targetInfo, loginPath);
				
				if(!CommonUtil.isStringNullOrEmpty(getLoginInfo.getErrorMessage())){
					try {
						InterfaceProjectDelete deleteTempProject = VexInstanceFactory.getProjectDelete(oInitInfo);
						deleteTempProject.execute(newID);
					} catch (Throwable e) {
						params.put("Delete Temporary Project error:", e.getCause());
						throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.GET_CRAWL_LOGIN_INFO_FAILED, e);
					}
					params.put(PortalConstants.ERROR_LOG_MESSAGE + ": ", getLoginInfo.getErrorMessage());
					throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.GET_CRAWL_LOGIN_INFO_FAILED, null);
				}
		
			
			if(!CommonUtil.isStringNullOrEmpty(loginInfoParams)){
				String[] loginInfoParamsArr = loginInfoParams.split(",");
				for(String loginParam : loginInfoParamsArr){
					VexLoginSettingItem item = new VexLoginSettingItem();
					item.setParamname(loginParam);
					list.add(item);
				}
			}

			try {
				InterfaceProjectDelete deleteTempProject = VexInstanceFactory.getProjectDelete(oInitInfo);
				deleteTempProject.execute(newID);
			} catch (Throwable e) {
				params.put("Delete Temporary Project error:", e.getCause());
				throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.GET_CRAWL_LOGIN_INFO_FAILED, e);
			}
			
		} catch (FileNotFoundException fnfe) {
			log.debug(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, fnfe);
			throw new UBSPortalException(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalMessages.FILE_NOT_FOUND_EXCEPTION, fnfe);
		} catch (IOException ie) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, ie);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.IO_EXCEPTION, ie);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, ubspe);
			}
			throw ubspe;
		}
		
		return list;
	}
	
	private static int getBackEndStatus(String strResponse){
		int iStatus = 0;
		switch(strResponse){
			case "waiting":
			case "start":
			case "resume":
				iStatus = ScanStatus.SCANNING.getInteger();
				break;
			case "pause":
				iStatus = ScanStatus.SCAN_INTERRUPTED.getInteger();
				break;
			case "stopped":
			case "failure":
				iStatus = ScanStatus.FAILURE.getInteger();
				break;
			case "complete":
				iStatus = ScanStatus.REPORT_MAKING.getInteger();
				break;
		}
		return iStatus ;
	}
	
	private static int getSlithyStatus(String strResponse){
		int iStatus = 0;
		switch(strResponse){
			case "waiting":
			case "start":
			case "resume":
				iStatus = ScanStatus.SCANNING.getInteger();
				break;
			case "pause":
				iStatus = ScanStatus.SCAN_INTERRUPTED.getInteger();
				break;
			case "stopped":
			case "failure":
				iStatus = ScanStatus.FAILURE.getInteger();
				break;
			case "complete":
				iStatus = ScanStatus.REPORT_MAKING.getInteger();
				break;
		}
		return iStatus ;
	}
	
	//Check if scan is completed and can be changed to Report Making.
	private static boolean isVexScanCompleted(String backend, String slithy, Long portalScanId) {
		boolean response = false;
		try {
			VexScanSetting vexScan = VexScanMgmtController.getVexScanSetting(portalScanId);
			boolean isServerFile = vexScan.getGetserverfiles() == 1 ? true : false;
			boolean isServerSetting= vexScan.getGetserversettings() == 1 ? true : false;
			
			if(!isServerFile && !isServerSetting){
				if(backend.equals("waiting") && slithy.equals("waiting")){
					response = true;
				}
			}
			
			if(isServerFile && !isServerSetting){
				if(backend.equals("complete") && slithy.equals("waiting")){
					response = true;
				}
			}
			
			if(!isServerFile && isServerSetting){
				if(backend.equals("waiting") && slithy.equals("complete")){
					response = true;
				}
			}
			
			if(isServerFile && isServerSetting){
				if(backend.equals("complete") && slithy.equals("complete")){
					response = true;
				}
			}
			
		} catch(Exception e){
			//do nothing
		} catch(Throwable e){
			//do nothing
		}
	    
		return response;
	}
		
	private static void reexecuteScan(ApiInitInfo info, String portalScanId, Long vexScanId) {
		new Thread(() -> threadReexecuteScan(info, portalScanId, vexScanId)).start();
	}

	/**
	 * Rescan method in separate thread
	 * 
	 * @param info
	 * @param scanId
	 * @return
	 */
	private static void threadReexecuteScan(ApiInitInfo info, String portalScanId, Long vexScanId) {
		
		InterfaceScan scan = VexInstanceFactory.getScan(info);
		 ApiScanInfo scanInfo = null;
		 Map<String, Object> params = null;
		
		try {
			VexScanSetting scanSetting = VexScanMgmtController.getVexScanSetting(Long.valueOf(portalScanId));
			
			if(scanSetting!=null){
				 String webScanSignatureSetID = scanSetting.getSelectedwebsignature();
				 String webScanThreadNo = String.valueOf(scanSetting.getNumofthread());
				 
				 Boolean serverFilesScanFlag = false;
				 if (scanSetting.getGetserverfiles() == 1){
					 serverFilesScanFlag = true;
				 }
						 
				 Boolean serverSettingsScanFlag = false;
				 if (scanSetting.getGetserversettings() == 1){
					 serverFilesScanFlag = true;
				 }
				 
				 String serverFilesSignatureSetID = scanSetting.getSelectedserverfilessignature();
				 String serverSettingsSignatureSetID = scanSetting.getSelectedserversettingssignature();
				 
				 boolean stopByScenarioError = false;
				 boolean stopByAbnormalTerminated = false;
				
				 scanInfo = new ApiScanInfo(String.valueOf(vexScanId), webScanSignatureSetID, webScanThreadNo, serverFilesScanFlag, serverSettingsScanFlag, 
						 serverFilesSignatureSetID, serverSettingsSignatureSetID, stopByScenarioError, stopByAbnormalTerminated);
				 
				 boolean response = scan.execute(scanInfo);
				 if (!response) {
						throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_FAILED, scan.getErrorMessage(), null);
				}
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("Portal Scan Id", portalScanId);
				params.put("Vex Scan Id", vexScanId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_REEXECUTE_SCAN, params, ubspe);
		} catch (Exception e) {
			log.debug(PortalErrors.REEXECUTE_SCAN_FAILED, PortalConstants.METHOD_REEXECUTE_SCAN, null, e.getCause());;
		}catch (Throwable e){
			params.put("Portal Scan Id", portalScanId);
			params.put("Vex Scan Id", vexScanId);
			log.debug(PortalErrors.REEXECUTE_SCAN_FAILED, PortalConstants.METHOD_REEXECUTE_SCAN, params, e.getCause());
		}
	}
	
	private static void threadRestartScan(ApiInitInfo info, String scanId) {
		Map<String, Object> params = null;
		try{
			InterfaceRestart restart = VexInstanceFactory.getRestart(info);
//			boolean response = restart.execute(scanId, ApiScanType.TYPE_AUDIT, false);
			boolean response = false;
			if (!response) {
					throw new UBSPortalException(PortalErrors.RESTART_SCAN_FAILED, restart.getErrorMessage(), null);
			}
				
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("Vex Scan Id", scanId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_RESTART_SCAN, params, ubspe);
		} catch (Exception e) {
			log.debug(PortalErrors.RESTART_SCAN_FAILED, PortalConstants.METHOD_RESTART_SCAN, null, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.RESTART_SCAN_FAILED, PortalConstants.METHOD_RESTART_SCAN, params, e.getCause());
		}
	}
	
	private static void threadRestartCrawl(ApiInitInfo info, String scanId) {
		Map<String, Object> params = null;
		try{
			InterfaceRestart restart = VexInstanceFactory.getRestart(info);
//			boolean response = restart.execute(scanId, ApiScanType.TYPE_CRAWL, false);
			boolean response = false;
			if (!response) {
				throw new UBSPortalException(PortalErrors.RESTART_CRAWL_FAILED, restart.getErrorMessage(), null);
			}
		}
		catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("Vex Scan Id: ", scanId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_RESTART_CRAWL, params, ubspe);
		} catch (Exception e) {
			log.debug(PortalErrors.RESTART_CRAWL_FAILED, PortalConstants.METHOD_RESTART_CRAWL, null, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.RESTART_CRAWL_FAILED, PortalConstants.METHOD_RESTART_CRAWL, params, e.getCause());
		}
	}
	
	private static void threadRecrawl(ApiInitInfo info, String scanId) {
		Map<String, Object> params = null;
		try{
			ApiScanInfo scanInfo = new ApiScanInfo(scanId);
			InterfaceScan crawl = VexInstanceFactory.getScan(info);
			boolean response = crawl.execute(scanInfo);
		 	if (!response) {
				throw new UBSPortalException(PortalErrors.RECRAWL_FAILED, crawl.getErrorMessage(), null);
			}
		}
		catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("Vex Scan Id: ", scanId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_RECRAWL, params, ubspe);
		} catch (Exception e) {
			log.debug(PortalErrors.RECRAWL_FAILED, PortalConstants.METHOD_RECRAWL, null, e.getCause());
		}catch (Throwable e){
			params.put("Vex Scan Id", scanId);
			log.debug(PortalErrors.RECRAWL_FAILED, PortalConstants.METHOD_RECRAWL, params, e.getCause());
		}
				
	}
	
	private static ProjectCopy copyCrawlAndScan(ApiInitInfo info, String scanId, String projectName) {
		ProjectCopy scanCopy = new ProjectCopy(info);
		ApiProjectCopyInfo copyInfo = new ApiProjectCopyInfo();
		
		//Set scan id using the existing id before copying it.
		copyInfo.setSourceProjectID(scanId);
		copyInfo.setProjectName(projectName);
		
		// Copy the existing scan and return the new scan id.
		scanCopy.execute(copyInfo);
		return scanCopy;
	}
	
	public static VexScanSettingItem addVexScanAdvancedSetting (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		VexScanSettingItem advancedScanSettingitem = null;
		String[] arrStartTime = null;
		String[] arrEndTime = null;
		Calendar nowStart = null;
		Calendar nowEnd = null;
		try {	
			long lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			Project project = ProjectLocalServiceUtil.getProject(lProjectId);
			if(CommonUtil.isObjectNull(project)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}
			String webInspectionSignatureSetGroup = project.getSelectedWebSignature();
			int serverFiles = project.getGetServerFiles();
			int serverSettings = project.getGetServerSettings();
			String serverFilesSignatureSetGroup = project.getSelectedServerFilesSignature();
			String serverSettingsSignatureSetGroup = project.getSelectedServerSettingsSignature();
			Object oScanName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_NAME).trim();
			int implementationEnvironment = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_IMPLEMENTATION_ENVIRONMENT);
			int emailNotification = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_EMAIL_NOTIFICATION);
			String oStartTime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_START_TIME).trim();
			String oEndTime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_END_TIME).trim();
			
			String scanName = oScanName.toString();
			String startTime = oStartTime.toString();
			String endTime = oEndTime.toString();
			
			if (CommonUtil.isStringNullOrEmpty(scanName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_SCAN_NAME);
			}else{
				if(!ControllerHelper.isVexInputValid(scanName)){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.SCAN_NAME_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(startTime) && !CommonUtil.isStringNullOrEmpty(endTime)) {
				arrStartTime = startTime.split(":");
				arrEndTime = endTime.split(":");
				
				nowStart = Calendar.getInstance();
				nowStart.set(Calendar.HOUR, Integer.parseInt(arrStartTime[0]));
				nowStart.set(Calendar.MINUTE, Integer.parseInt(arrStartTime[1]));
				nowStart.set(Calendar.SECOND, 0);
				
		        nowEnd = Calendar.getInstance();
		        nowEnd.set(Calendar.HOUR, Integer.parseInt(arrEndTime[0]));
		        nowEnd.set(Calendar.MINUTE, Integer.parseInt(arrEndTime[1]));
		        nowEnd.set(Calendar.SECOND, 0);
				
				if (nowStart.after(nowEnd)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_START_TIME_AND_END_TIME_INVALID);
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_TIME_AND_END_TIME_INVALID);
				}
				else if(PortletCommonUtil.getTimeDifference(nowStart, nowEnd, ChronoUnit.MINUTES) < 30){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_START_TIME_END_TIME_DIFFERENCE);
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_TIME_END_TIME_DIFFERENCE);
				}
				
			} else if (!CommonUtil.isStringNullOrEmpty(startTime) || !CommonUtil.isStringNullOrEmpty(endTime)) {
				
				if (CommonUtil.isStringNullOrEmpty(startTime)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_START_TIME));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_TIME_INVALID);
				}
				
				if (CommonUtil.isStringNullOrEmpty(endTime)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_END_TIME));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.END_TIME_INVALID);
				}
				
			}
			
			advancedScanSettingitem = new VexScanSettingItem();
			advancedScanSettingitem.setImplementationenvironment(implementationEnvironment);
			advancedScanSettingitem.setSetemailnotification(emailNotification);
			advancedScanSettingitem.setGetserverfiles(serverFiles);
			advancedScanSettingitem.setGetserversettings(serverSettings);
			advancedScanSettingitem.setSelectedwebsignature(webInspectionSignatureSetGroup);
			advancedScanSettingitem.setSelectedserverfilessignature(serverFilesSignatureSetGroup);
			advancedScanSettingitem.setSelectedserversettingssignature(serverSettingsSignatureSetGroup);
			
			if (null != nowStart && null != nowEnd) {
				advancedScanSettingitem.setStarttime(nowStart.getTime());
				advancedScanSettingitem.setEndtime(nowEnd.getTime());
			}
			
		} catch (PortalException pe) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, ubspe);
			}
			throw ubspe;
		}  finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		return advancedScanSettingitem;
	}
	
	public static List<VexTargetInformationItem> addVexScanTargetInformation(ActionRequest actionRequest, long lUserId, long lProjectId) throws UBSPortalException{
		Map<String, Object> params = new HashMap<String, Object>();
		List<VexTargetInformationItem> targetInformationList = null;
		
		try {
		
		String rowIndexes = ParamUtil.getString(actionRequest, "rowIndexes");
		String[] parts = rowIndexes.split(",");
		int[] indexes = new int[parts.length];
		for (int i = 0; i < parts.length; i++) {
			indexes[i] = Integer.parseInt(parts[i]);
		}
		
		int targetInfoListSize = parts.length;
		targetInformationList = new ArrayList<>();
		
		for (int x = 0; x < targetInfoListSize; x++) {
			int i = indexes[x];
			String protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROTOCOL_GROUP + i);
			String host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HOST + i).trim();
			String port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PORT + i).trim();
			
			Object httpversion = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_HTTP_VERSION + i);
			Object setkeepaliveconnection =  ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_SET_KEEP_ALIVE_CONNECTION + i);
			Object setresponsecontentlength =  ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_SET_RESPONSE_CONTENT_LENGTH + i);
			Object useacceptencodingheader = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_USE_ACCEPTED_ENCODING_HEADER + i);
			Object unzipresponse = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_UNZIP_RESPONSE + i);
			
			String httpprotocol =  ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_HTTP_PROTOCOL + i).trim();
			
			String externalproxyhost =  ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_EXTERNAL_PROXY_HOST + i).trim();
			String externalproxyport = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_EXTERNAL_PROXY_PORT + i);
			String externalproxyauthid =  ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_EXTERNAL_PROXY_AUTH_ID + i).trim();
			String externalproxyauthpassword =  ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_EXTERNAL_PROXY_AUTH_PASSWORD + i).trim();
			
			Object useclientcertificate = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_USE_CLIENT_CERTIFICATE + i);
			String certificatefilepassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_CERTIFICATE_FILE_PASSWORD + i).trim();
			
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			String fileName=uploadRequest.getFileName(PortalConstants.PARAM_TARGET_INFO_CERTIFICATE_FILE + i);
						
			String ntlmauthid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_NTLM_AUTH_ID + i).trim();
			String ntlmauthpassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_NTLM_AUTH_PASSWORD + i).trim();
			String ntlmauthdomain = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_NTLM_AUTH_DOMAIN + i).trim();
			String ntlmauthhost = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_NTLM_AUTH_HOST + i).trim();
			
			String digestauthid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_DIGEST_AUTH_ID + i).trim();
			String digestauthpassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_DIGEST_AUTH_PASSWORD + i).trim();
			
			String basicauthid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_BASIC_AUTH_ID + i).trim();
			String basicauthpassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_BASIC_AUTH_PASSWORD + i).trim();
			
			String accessexclusionpath = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_ACCESS_EXCLUSION_PATH + i).trim();
			
			if (CommonUtil.isStringNullOrEmpty(protocol) || CommonUtil.isStringNullOrEmpty(host) && CommonUtil.isStringNullOrEmpty(port)) {
				
				if (CommonUtil.isStringNullOrEmpty(protocol)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROTOCOL));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PROTOCOL_INVALID);
				}
				
				if (CommonUtil.isStringNullOrEmpty(host)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_HOST));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_HOST);
				} else {
					if (!Validator.isHostName(host)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_INVALID);
					}
					
					if(host.equals("localhost") || host.equals("127.0.0.1")){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_LOCALHOST_INVALID);
					}
				}
				
				if (CommonUtil.isStringNullOrEmpty(port)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PORT));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_PORT);
				} else {				
					if (!Validator.isNumber(port)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PORT));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PORT_INVALID);
					}
				}
				
			} else if (!CommonUtil.isStringNullOrEmpty(protocol) && !CommonUtil.isStringNullOrEmpty(host) && !CommonUtil.isStringNullOrEmpty(port)) {
				
				if (!Validator.isHostName(host)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_INVALID);
				}

				if (!Validator.isNumber(port)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PORT));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PORT_INVALID);
				}
				
				if(port.equals("0")){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PORT));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PORT_INVALID);
				}
				
				if(host.equals("localhost") || host.equals("127.0.0.1")){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_LOCALHOST_INVALID);
				}
				
				VexTargetInformationItem item = new VexTargetInformationItem();
				item.setScanid(i);
				if(protocol.equals("1")){
					item.setProtocol("http://");
				}else{
					item.setProtocol("https://");
				}
				item.setHost(host);
				item.setPort(port);
				
				if(!CommonUtil.isObjectNull(httpversion)){
					item.setHttpversion((int) httpversion);
				}
				
				if(!CommonUtil.isObjectNull(setkeepaliveconnection)){
					item.setSetkeepaliveconnection((int)setkeepaliveconnection);
				}
				
				if(!CommonUtil.isObjectNull(setresponsecontentlength)){
					item.setSetresponsecontentlength((int)setresponsecontentlength);
				}
				
				if(!CommonUtil.isObjectNull(useacceptencodingheader)){
					item.setUseacceptencodingheader((int)useacceptencodingheader);
				}
				
				if(!CommonUtil.isObjectNull(unzipresponse)){
					item.setUnzipresponse((int)unzipresponse);
				}
				
				if(!CommonUtil.isStringNullOrEmpty(httpprotocol)){
					item.setHttpprotocol(httpprotocol);
				}
				
				if(!CommonUtil.isStringNullOrEmpty(externalproxyport) || !CommonUtil.isStringNullOrEmpty(externalproxyhost) || !CommonUtil.isStringNullOrEmpty(externalproxyauthid) || !CommonUtil.isStringNullOrEmpty(externalproxyauthpassword)){
									
					if(CommonUtil.isStringNullOrEmpty(externalproxyhost)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_EXTERNAL_PROXY_HOST);
					}
					
					if(CommonUtil.isStringNullOrEmpty(externalproxyport)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_PORT));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_EXTERNAL_PROXY_PORT);
					}
					
					if(CommonUtil.isStringNullOrEmpty(externalproxyauthid)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_AUTH_ID));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_EXTERNAL_PROXY_AUTH_ID);
					}
					
					if(CommonUtil.isStringNullOrEmpty(externalproxyauthpassword)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_AUTH_PASSWORD));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_EXTERNAL_PROXY_AUTH_PASSWORD);
					}
					
					if(!ControllerHelper.isVexInputValid(externalproxyauthid)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_AUTH_ID));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.EXTERNAL_PROXY_AUTH_ID_INVALID);
					}
					
					if(!ControllerHelper.isVexInputValid(externalproxyauthpassword)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_AUTH_PASSWORD));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.EXTERNAL_PROXY_AUTH_PASSWORD_INVALID);
					}
					
					if (!Validator.isHostName(externalproxyhost)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.EXTERNAL_PROXY_HOST_INVALID);
					}
					
					if(externalproxyhost.equals("localhost") || externalproxyhost.equals("127.0.0.1")){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.EXTERNAL_PROXY_HOST_LOCALHOST_INVALID);
					}
					
					if (!Validator.isNumber(String.valueOf(externalproxyport))) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_PORT));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.EXTERNAL_PROXY_PORT_INVALID);
					}
					
					if (String.valueOf(externalproxyport).equals("0")) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXTERNAL_PROXY_PORT));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.EXTERNAL_PROXY_PORT_INVALID);
					}
					
					item.setExternalproxyhost(externalproxyhost);
					item.setExternalproxyport(Integer.parseInt(externalproxyport));
					item.setExternalproxyauthid(externalproxyauthid);
					item.setExternalproxyauthpassword(externalproxyauthpassword);
				}
				
				if(!CommonUtil.isStringNullOrEmpty(fileName) || !CommonUtil.isStringNullOrEmpty(certificatefilepassword)){
					
					if((int)useclientcertificate == 1){
						if(fileName == null){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_CERTIFICATE_FILE));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_CERTIFICATE_FILE);
						}
						
						if(certificatefilepassword == null){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_CERTIFICATE_PASSWORD));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_CERTIFICATE_PASSWORD);
						}else{
							if(!ControllerHelper.isVexInputValid(certificatefilepassword)){
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CERTIFICATE_PASSWORD));
								throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.CERTIFICATE_PASSWORD_INVALID);
							}
						}
						
						if (!Validator.isFileName(String.valueOf(fileName))) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CERTIFICATE_FILE));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.CERTIFICATE_FILE_INVALID);
						}
						
						File fileCertificate = uploadRequest.getFile(PortalConstants.PARAM_TARGET_INFO_CERTIFICATE_FILE + i);
						if(fileCertificate == null){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CERTIFICATE_FILE));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.CERTIFICATE_FILE_INVALID);
						}
						
						String certFilePathinDataUpload = uploadCertificateFile(lProjectId, lUserId, uploadRequest, PortalConstants.PARAM_TARGET_INFO_CERTIFICATE_FILE + i);
						
						item.setUseclientcertificate((int)useclientcertificate);
						item.setCertificatefile(certFilePathinDataUpload);
						item.setCertificatefilepassword(certificatefilepassword);
						
					}else{
						if(fileName != null || certificatefilepassword !=null){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USE_CLIENT_CERTIFICATE));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_USE_CLIENT_CERTIFICATE);
						}
						
					}
					
				}
								
				if(!CommonUtil.isStringNullOrEmpty(ntlmauthid) || !CommonUtil.isStringNullOrEmpty(ntlmauthpassword) || !CommonUtil.isStringNullOrEmpty(ntlmauthdomain) || !CommonUtil.isStringNullOrEmpty(ntlmauthhost)){
					if(CommonUtil.isStringNullOrEmpty(ntlmauthid)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_ID));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_NTLM_AUTH_ID);
					}else{
						if(!ControllerHelper.isVexInputValid(ntlmauthid)){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_ID));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NTLM_AUTH_ID_INVALID);
						}
					}
					
					if(CommonUtil.isStringNullOrEmpty(ntlmauthpassword)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_PASSWORD));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_NTLM_AUTH_PASSWORD);
					}else{
						if(!ControllerHelper.isVexInputValid(ntlmauthpassword)){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_PASSWORD));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NTLM_AUTH_PASSWORD_INVALID);
						}
					}
					
					if(CommonUtil.isStringNullOrEmpty(ntlmauthdomain)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_DOMAIN));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_NTLM_AUTH_DOMAIN);
					}
					
					if(CommonUtil.isStringNullOrEmpty(ntlmauthhost)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_NTLM_AUTH_HOST);
					}
					
					if (!Validator.isDomain(String.valueOf(ntlmauthdomain))) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_DOMAIN));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NTLM_AUTH_DOMAIN_INVALID);
					}
					
					if(ntlmauthdomain.equals("localhost") || ntlmauthdomain.equals("127.0.0.1")){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_DOMAIN));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NTLM_AUTH_DOMAIN_LOCALHOST_INVALID);
					}
					
					if (!Validator.isHostName(String.valueOf(ntlmauthhost))) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NTLM_AUTH_HOST_INVALID);
					}
					
					if(ntlmauthhost.equals("localhost") || ntlmauthhost.equals("127.0.0.1")){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NTLM_AUTH_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NTLM_AUTH_HOST_LOCALHOST_INVALID);
					}
					
					item.setNtlmauthid(ntlmauthid);
					item.setNtlmauthpassword(ntlmauthpassword);
					item.setNtlmauthdomain(ntlmauthdomain);
					item.setNtlmauthhost(ntlmauthhost);
				}
								
				if(!CommonUtil.isStringNullOrEmpty(digestauthid) || !CommonUtil.isStringNullOrEmpty(digestauthpassword)){
					if(CommonUtil.isStringNullOrEmpty(digestauthid)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_DIGEST_AUTH_ID));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_DIGEST_AUTH_ID);
					}else{
						if(!ControllerHelper.isVexInputValid(digestauthid)){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_DIGEST_AUTH_ID));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.DIGEST_AUTH_ID_INVALID);
						}
					}
					
					if(CommonUtil.isStringNullOrEmpty(digestauthpassword)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_DIGEST_AUTH_PASSWORD));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_DIGEST_AUTH_PASSWORD);
					}else{
						if(!ControllerHelper.isVexInputValid(digestauthpassword)){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_DIGEST_AUTH_PASSWORD));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.DIGEST_AUTH_PASSWORD_INVALID);
						}
					}
					
					item.setDigestauthid(digestauthid);
					item.setDigestauthpassword(digestauthpassword);
				}
				
				if(!CommonUtil.isStringNullOrEmpty(basicauthid) || !CommonUtil.isStringNullOrEmpty(basicauthpassword)){
					if(CommonUtil.isStringNullOrEmpty(basicauthid)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_BASIC_AUTH_ID));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_BASIC_AUTH_ID);
					}else{
						if(!ControllerHelper.isVexInputValid(basicauthid)){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_BASIC_AUTH_ID));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.BASIC_AUTH_ID_INVALID);
						}
					}
					
					if(CommonUtil.isStringNullOrEmpty(basicauthpassword)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_BASIC_AUTH_PASSWORD));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_BASIC_AUTH_PASSWORD);
					}else{
						if(!ControllerHelper.isVexInputValid(basicauthpassword)){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_BASIC_AUTH_PASSWORD));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.BASIC_AUTH_PASSWORD_INVALID);
						}
					}
					
					item.setBasicauthid(basicauthid);
					item.setBasicauthpassword(basicauthpassword);
				}
				
				if(!CommonUtil.isStringNullOrEmpty(accessexclusionpath)){
					if(!ControllerHelper.isVexInputValid(accessexclusionpath)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_ACCESS_EXCLUSION_PATH));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ACCESS_EXCLUSION_PATH_INVALID);
					}
					item.setAccessexclusionpath(accessexclusionpath);
				}else{
					item.setAccessexclusionpath(PortalConstants.STRING_EMPTY);
				}
				
				targetInformationList.add(item);
				
			}else if (!CommonUtil.isStringNullOrEmpty(host) || !CommonUtil.isStringNullOrEmpty(port)) {
				
					if (CommonUtil.isStringNullOrEmpty(protocol)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROTOCOL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PROTOCOL_INVALID);
					}
					
					if (CommonUtil.isStringNullOrEmpty(host)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_HOST);
					} else {
						if (!Validator.isHostName(host)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_INVALID);
						}
						
						if(host.equals("localhost") || host.equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_LOCALHOST_INVALID);
						}
					}
					
					if (CommonUtil.isStringNullOrEmpty(port)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PORT));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_PORT);
					} else {				
						if (!Validator.isNumber(port)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PORT));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PORT_INVALID);
						}
					}
				 }
				}
			} catch (UBSPortalException ubspe) {
				String strErrorCode = ubspe.getErrorCode();
				if (ControllerHelper.isUserInputError(strErrorCode)) {
					log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_TARGET_INFORMATION, params, ubspe);
				} else {
					log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_TARGET_INFORMATION, params, ubspe);
				}
				throw ubspe;
			} finally {
				if (!CommonUtil.isMapNullOrEmpty(params)) {
					params.clear();
					params = null;
				}
			}
		
			return targetInformationList;
	}
	
	public static List<VexLoginSettingItem> getLoginInfoSetting (ActionRequest actionRequest, long lProjectId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<VexTargetInformationItem> targetInformationList = null;
		String targetInfo = null;
		String loginPath = null;
		List<VexLoginSettingItem> loginParams = new ArrayList<>();
		boolean isInTargetInfo = PortalConstants.FALSE;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			String loginURL = ParamUtil.getString(actionRequest, PortalConstants.PARAM_LOGIN_URL).trim();
			int numOfParams = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
			int targetInfoListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFORMATION_LIST_SIZE);
			targetInformationList = new ArrayList<>();
			
			if (CommonUtil.isStringNullOrEmpty(loginURL) && numOfParams != PortalConstants.INT_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_LOGIN_URL));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_LOGIN_URL);
			} else {
				if (!CommonUtil.isStringNullOrEmpty(loginURL) && !Validator.isUrl(loginURL.trim())) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE,
							String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
									PortalConstants.ERROR_LOG_PARAM_LOGIN_URL));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.LOGIN_URL_INVALID);
				}
			}

			for (int i = 0; i <= targetInfoListSize; i++) {
				String protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROTOCOL_GROUP + i);
				String host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HOST + i).trim();
				String port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PORT + i).trim();
				
				if (CommonUtil.isStringNullOrEmpty(protocol) || CommonUtil.isStringNullOrEmpty(host) && CommonUtil.isStringNullOrEmpty(port)) {
					
					continue;
					
				} else if (!CommonUtil.isStringNullOrEmpty(protocol) && !CommonUtil.isStringNullOrEmpty(host) && !CommonUtil.isStringNullOrEmpty(port)) {
					
					if (!Validator.isHostName(host)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.HOST_INVALID);
					}

					if (!Validator.isNumber(port)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PORT));
						throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.PORT_INVALID);
					}
					
					if(host.equals("localhost") || host.equals("127.0.0.1")){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_LOCALHOST_INVALID);
					}
					
					VexTargetInformationItem item = new VexTargetInformationItem();
					item.setScanid(i);
					item.setProtocol(protocol);
					item.setHost(host);
					item.setPort(port);
					targetInformationList.add(item);
					
					//check if target info in login url is in target information table
					URL targetUrl = new URL(loginURL);
					
					if (protocol.equals("1")) {
						protocol = "http";
					} else {
						protocol = "https";
					}
					
					if(targetUrl.getProtocol().equals(protocol) && targetUrl.getHost().equals(host) && targetUrl.getPort() == Integer.parseInt(port)){
						targetInfo = targetUrl.getProtocol() + "://" + targetUrl.getAuthority();
						loginPath = targetUrl.getPath();
						isInTargetInfo = PortalConstants.TRUE;
						break;
					}
					
				}else if (!CommonUtil.isStringNullOrEmpty(host) || !CommonUtil.isStringNullOrEmpty(port)) {
					
					if (CommonUtil.isStringNullOrEmpty(protocol)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY,
										PortalConstants.ERROR_LOG_PARAM_PROTOCOL));
						throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.PROTOCOL_INVALID);
					}
					
					if (CommonUtil.isStringNullOrEmpty(host)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.NO_HOST);
					} else {
						if (!Validator.isHostName(host)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_HOST));
							throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.HOST_INVALID);
						}
						
						if(host.equals("localhost") || host.equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
									PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_LOCALHOST_INVALID);
						}
					}
					
					if (CommonUtil.isStringNullOrEmpty(port)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PORT));
						throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.NO_PORT);
					} else {				
						if (!Validator.isNumber(port)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_PORT));
							throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.PORT_INVALID);
						}
					}
					
				}
			}
			
			if (targetInformationList.size() == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY,
								PortalConstants.ERROR_LOG_PARAM_TARGET_INFORMATION_LIST));
				throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.NO_TARGET_INFORMATION_LIST);
			}
			
			if(isInTargetInfo)
				loginParams = VexScanMgmtController.getCrawlLoginInfo(lProjectId, targetInfo, loginPath); 
			else{
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY,
								PortalConstants.ERROR_LOG_PARAM_LOGIN_URL));
				throw new UBSPortalException(PortalErrors.GET_CRAWL_LOGIN_INFO_FAILED, PortalMessages.LOGIN_URL_NOT_FOUND_IN_TARGET_INFORMATION_LIST);
			}
				
		} catch (MalformedURLException e) {
			log.debug(PortalErrors.MALFORMEDURL_EXCEPTION, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, e);
			throw new UBSPortalException(PortalErrors.MALFORMEDURL_EXCEPTION, PortalMessages.MALFORMEDURL_EXCEPTION, e);
		}catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return loginParams;
	}

	public static boolean addVexScanSetting (ActionRequest actionRequest, int userAction, long lUserId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		ArrayList<VexLoginSettingItem> loginSetting = null;
		ArrayList<VexLoginSetting> loginSettingforDB = null;
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		String[] arrStartTime = null;
		String[] arrEndTime = null;
		Calendar nowStart = null;
		Calendar nowEnd = null;
		List<VexTargetInformation> targetInformationList = null;
		BufferedReader reader = null;
		String temp = null;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			Project project = ProjectLocalServiceUtil.getProject(lProjectId);
			if(CommonUtil.isObjectNull(project)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}
			
			String webInspectionSignatureSetGroup = project.getSelectedWebSignature();
			int serverFiles = project.getGetServerFiles();
			int serverSettings = project.getGetServerSettings();
			String serverFilesSignatureSetGroup = project.getSelectedServerFilesSignature();
			String serverSettingsSignatureSetGroup = project.getSelectedServerSettingsSignature();
			
			String scanName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_NAME).trim();
			int implementationEnvironment = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_IMPLEMENTATION_ENVIRONMENT);
			String startTime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_START_TIME).trim();
			String endTime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_END_TIME).trim();
			int emailNotification = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_EMAIL_NOTIFICATION);
			String startURL = ParamUtil.getString(originalRequest, PortalConstants.PARAM_START_URL);
			String authorizedPatrolURL = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AUTHORIZED_PATROL_URL);
			String loginURL = ParamUtil.getString(actionRequest, PortalConstants.PARAM_LOGIN_URL).trim();
			int targetInfoListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFORMATION_LIST_SIZE);
			int numOfParams = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
			int setCrawlingOnly = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_CRAWLING_ONLY);

			StringBuilder tempStartURL = null;
			StringBuilder tempAuthorizedPatrolURL = null;
			
			if (CommonUtil.isStringNullOrEmpty(scanName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_SCAN_NAME);
			}else{
				if(!ControllerHelper.isVexInputValid(scanName)){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.SCAN_NAME_INVALID);
				}
			}
			
			if(CommonUtil.isStringNullOrEmpty(webInspectionSignatureSetGroup)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_WEB_INSPECTION_SIGNATURE_SET_GROUP);
			}
			
			if(CommonUtil.isStringNullOrEmpty(serverFilesSignatureSetGroup)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_SERVER_FILE_SIGNATURE_SET_GROUP);
			}
			
			if(CommonUtil.isStringNullOrEmpty(serverSettingsSignatureSetGroup)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_SERVER_SETTING_SIGNATURE_SET_GROUP);
			}
			
			targetInformationList = new ArrayList<>();
			for (int i = 0; i < targetInfoListSize; i++) {
				String protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROTOCOL_GROUP + i);
				String host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HOST + i).trim();
				String port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PORT + i).trim();
				
				if (CommonUtil.isStringNullOrEmpty(protocol) || CommonUtil.isStringNullOrEmpty(host) && CommonUtil.isStringNullOrEmpty(port)) {
					
					continue;
					
				} else if (!CommonUtil.isStringNullOrEmpty(protocol) && !CommonUtil.isStringNullOrEmpty(host) && !CommonUtil.isStringNullOrEmpty(port)) {
					
					if (!Validator.isHostName(host)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_INVALID);
					}

					if (!Validator.isNumber(port)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PORT));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PORT_INVALID);
					}
					
					if(host.equals("localhost") || host.equals("127.0.0.1")){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_LOCALHOST_INVALID);
					}
					
					VexTargetInformation item = VexTargetInformationLocalServiceUtil.createVexTargetInformationObj();
					item.setScanid(i);
					item.setProtocol(protocol);
					item.setHost(host);
					item.setPort(Integer.parseInt(port));
					targetInformationList.add(item);
					
				}else if (!CommonUtil.isStringNullOrEmpty(host) || !CommonUtil.isStringNullOrEmpty(port)) {
					
					if (CommonUtil.isStringNullOrEmpty(protocol)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY,
										PortalConstants.ERROR_LOG_PARAM_PROTOCOL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PROTOCOL_INVALID);
					}
					
					if (CommonUtil.isStringNullOrEmpty(host)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_HOST));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_HOST);
					} else {
						if (!Validator.isHostName(host)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_HOST));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_INVALID);
						}
						
						if(host.equals("localhost") || host.equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
									PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HOST));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.HOST_LOCALHOST_INVALID);
						}
					}
					
					if (CommonUtil.isStringNullOrEmpty(port)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
								PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PORT));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_PORT);
					} else {				
						if (!Validator.isNumber(port)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_PORT));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.PORT_INVALID);
						}
					}
					
				}
			}
			
			if (targetInformationList.size() == 0) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY,
								PortalConstants.ERROR_LOG_PARAM_TARGET_INFORMATION_LIST));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_TARGET_INFORMATION_LIST);
			}
			
			if (CommonUtil.isStringNullOrEmpty(startURL)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_START_URL));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_START_URL);
			} else {
				tempStartURL = new StringBuilder();
				reader = new BufferedReader(new StringReader(startURL));
				while ((temp = reader.readLine()) != null) {
					if (!Validator.isUrl(temp.trim())) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_URL_INVALID);
					}
					
					if (temp.trim().contains(PortalConstants.STRING_DOUBLE_QUOTE) || temp.trim().contains(PortalConstants.STRING_BACK_SLASH)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.UNUSABLE_CHARACTER_INPUT);
					}
					
					if(!ControllerHelper.isVexInputValid(temp.trim())){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.INVALID_CHARACTER_INPUT);
					}
					
					try {
						URL strstartURL = new URL(temp);
						if(strstartURL.getHost().equals("localhost") || strstartURL.getHost().equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_START_URL));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_URL_LOCALHOST_INVALID);
						}
					} catch (MalformedURLException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_URL_LOCALHOST_INVALID);
					} 
					
					boolean isValid = false;
					for(VexTargetInformation targetInfo : targetInformationList){
						String protocol = null;
						if (targetInfo.getProtocol().equals("1")) {
							protocol = "http://";
						} else {
							protocol = "https://";
						}
						String ti = protocol + targetInfo.getHost() + ":" + targetInfo.getPort()+"/";
						
						if (temp.trim().startsWith(ti)){
							isValid = true;
						}
					}
					
					if(!isValid) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_URL_INVALID);
					} else {
						tempStartURL.append(temp.trim() + ";");
					}
		        }
				
			}
			
			if (CommonUtil.isStringNullOrEmpty(authorizedPatrolURL)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE,
						String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY,
								PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_AUTHORIZED_PATROL_URL);
			} else {
				tempAuthorizedPatrolURL = new StringBuilder();
				reader = new BufferedReader(new StringReader(authorizedPatrolURL));
				while ((temp = reader.readLine()) != null) {
					if (!Validator.isUrl(temp.trim())) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.AUTHORIZED_PATROL_URL_INVALID);
					}
					
					if (temp.trim().contains(PortalConstants.STRING_DOUBLE_QUOTE) || temp.trim().contains(PortalConstants.STRING_BACK_SLASH)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.UNUSABLE_CHARACTER_INPUT);
					}
					
					if(!ControllerHelper.isVexInputValid(temp.trim())){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.INVALID_CHARACTER_INPUT);
					}
					
					try {
						URL strAuthorizedPatrolURL = new URL(temp);
						if(strAuthorizedPatrolURL.getHost().equals("localhost") || strAuthorizedPatrolURL.getHost().equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.AUTHORIZED_PATROL_URL_LOCALHOST_INVALID);
						}
					} catch (MalformedURLException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.AUTHORIZED_PATROL_URL_LOCALHOST_INVALID);
					} 
					
					boolean isValid = false;
					for(VexTargetInformation targetInfo : targetInformationList){
						String protocol = null;
						if (targetInfo.getProtocol().equals("1")) {
							protocol = "http://";
						} else {
							protocol = "https://";
						}
						String ti = protocol + targetInfo.getHost() + ":" + targetInfo.getPort();
						
						if (temp.trim().startsWith(ti)){
							isValid = true;
						}
					}
					
					if(!isValid) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.AUTHORIZED_PATROL_URL_INVALID);
					} else {
						tempAuthorizedPatrolURL.append(temp.trim() + ";");
					}
		        }
			}
			
			
			
			if (!CommonUtil.isStringNullOrEmpty(startTime) && !CommonUtil.isStringNullOrEmpty(endTime)) {
				arrStartTime = startTime.split(":");
				arrEndTime = endTime.split(":");
				nowStart = Calendar.getInstance();
				nowStart.set(Calendar.HOUR, Integer.parseInt(arrStartTime[0]));
				nowStart.set(Calendar.MINUTE, Integer.parseInt(arrStartTime[1]));
				nowStart.set(Calendar.SECOND, 0);
				
		        nowEnd = Calendar.getInstance();
		        nowEnd.set(Calendar.HOUR, Integer.parseInt(arrEndTime[0]));
		        nowEnd.set(Calendar.MINUTE, Integer.parseInt(arrEndTime[1]));
		        nowEnd.set(Calendar.SECOND, 0);
				
				if (nowStart.after(nowEnd)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_START_TIME_AND_END_TIME_INVALID);
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_TIME_AND_END_TIME_INVALID);
				}
				else if(PortletCommonUtil.getTimeDifference(nowStart, nowEnd, ChronoUnit.MINUTES) < 30){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_START_TIME_END_TIME_DIFFERENCE);
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_TIME_END_TIME_DIFFERENCE);
				}
				
			} else if (!CommonUtil.isStringNullOrEmpty(startTime) || !CommonUtil.isStringNullOrEmpty(endTime)) {
				
				if (CommonUtil.isStringNullOrEmpty(startTime)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
							PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_START_TIME));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_TIME_INVALID);
				}
				
				if (CommonUtil.isStringNullOrEmpty(endTime)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
							PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_END_TIME));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.END_TIME_INVALID);
				}
				
			}
			
			
			loginSetting = new ArrayList<>();
			loginSettingforDB = new ArrayList<>();
			
			if (CommonUtil.isStringNullOrEmpty(loginURL) && numOfParams != PortalConstants.INT_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(
						PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_LOGIN_URL));
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_LOGIN_URL);
			} else { 
				if ((!CommonUtil.isStringNullOrEmpty(loginURL) && Validator.isUrl(loginURL.trim()))){
					try {
						URL strloginURL = new URL(loginURL);
						if(strloginURL.getHost().equals("localhost") || strloginURL.getHost().equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_LOGIN_URL));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.LOGIN_URL_LOCALHOST_INVALID);
						}
					} catch (MalformedURLException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_LOGIN_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.LOGIN_URL_INVALID);
					} 
				}
				
				if ((!CommonUtil.isStringNullOrEmpty(loginURL) && !Validator.isUrl(loginURL.trim())) 
						|| (!CommonUtil.isStringNullOrEmpty(loginURL) && numOfParams == PortalConstants.INT_ZERO)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE,
							String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
									PortalConstants.ERROR_LOG_PARAM_LOGIN_URL));
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.LOGIN_URL_INVALID);
				}
				
				for (int i = 0; i < numOfParams; i++) {
					String paramname = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PARAMETER_NAME + i).trim();
					String paramvalue = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PARAMETER_VALUE + i).trim();
					int checked = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_CHECK_PARAMETER + i);
					
					if(CommonUtil.isStringNullOrEmpty(paramname) && CommonUtil.isStringNullOrEmpty(paramvalue)){
						continue;
					}else if(!CommonUtil.isStringNullOrEmpty(paramname) || !CommonUtil.isStringNullOrEmpty(paramvalue)){
						if (checked == PortalConstants.INT_ONE && CommonUtil.isStringNullOrEmpty(paramvalue)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_LOGIN_PARAMETER_VALUE));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.NO_LOGIN_PARAMETER_VALUE);
						}
						
						if (paramvalue.contains(PortalConstants.STRING_DOUBLE_QUOTE) || paramvalue.contains(PortalConstants.STRING_BACK_SLASH)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_LOGIN_PARAMETER_VALUE));
							throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.UNUSABLE_CHARACTER_INPUT);
						}
						
						if (!ControllerHelper.isVexInputValid(paramvalue)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_LOGIN_PARAMETER_VALUE));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.LOGIN_PARAM_VALUE_INVALID);
						}
						
						VexLoginSetting itemforDB = VexLoginSettingLocalServiceUtil.createVexLoginSettingObj();
						itemforDB.setScanid(i);
						itemforDB.setLoginurl(loginURL);
						itemforDB.setParamname(paramname);
						if(checked != PortalConstants.INT_ZERO){
							itemforDB.setParamvalue(paramvalue);
						}else{
							itemforDB.setParamvalue("");
						}
						loginSettingforDB.add(itemforDB);
						
						
						VexLoginSettingItem item = new VexLoginSettingItem();
						item.setScanid(i);
						item.setLoginurl(loginURL);
						item.setParamname(paramname);
						if(checked != PortalConstants.INT_ZERO){
							item.setParamvalue(paramvalue);
						}else{
							item.setParamvalue("");
						}
						item.setChecked(checked);
						loginSetting.add(item);
					}
				}
			}
			
			lScanId = ScanLocalServiceUtil.createScanId();
			File fileDirectory = FileProcessUtil.createCrawlSettingYAMLPath(lProjectId, lScanId);
			
			if (fileDirectory.isDirectory()) {
				String vexScanName = lProjectId + PortalConstants.STR_UNDERSCORE + lScanId+ PortalConstants.STR_UNDERSCORE +scanName;
				reader = new BufferedReader(new StringReader(startURL));
				StringBuilder base_start_url = new StringBuilder();
				StringBuilder base_crawl_allow_url_wildcard = new StringBuilder();
				temp = null;
				while ((temp = reader.readLine()) != null) {
					base_start_url.append(PortalConstants.STR_SPACE + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(authorizedPatrolURL));
				while ((temp = reader.readLine()) != null) {
					base_crawl_allow_url_wildcard.append(PortalConstants.STR_SPACE + temp.trim() + System.lineSeparator());
		        }
				String crawling_filepath = createCrawlingSettingFile(lScanId, lProjectId,
						loginSetting, loginURL, fileDirectory, base_start_url, base_crawl_allow_url_wildcard);
				
				String project_add_filepath = createProjectAddSettingFile(lScanId, lProjectId, loginURL,
						fileDirectory, vexScanName, targetInformationList);
				
				String report_information_path = createReportInformationFile(lScanId, lProjectId, loginURL,
						fileDirectory, vexScanName, targetInformationList, implementationEnvironment);
				
				ApiInitInfo oInitInfo = setLoginCredentials();
				
				InterfaceProjectAdd projectAdd = VexInstanceFactory.getProjectAdd(oInitInfo);
				String newID = null;
				
				try {
					newID = projectAdd.executeEX(project_add_filepath);
				} catch (Throwable e) {
					params.put("YML File:", project_add_filepath);
					params.put("error", e.getCause());
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED, e);
				}
				
				bSuccess = saveVexProjectToDatabase(lUserId, params, loginSettingforDB, bSuccess,
						lScanId, lProjectId, nowStart, nowEnd, targetInformationList, scanName,
						implementationEnvironment, webInspectionSignatureSetGroup, serverFiles, serverSettings,
						serverFilesSignatureSetGroup, serverSettingsSignatureSetGroup, emailNotification, tempStartURL,
						tempAuthorizedPatrolURL, newID, setCrawlingOnly);
				
				if (bSuccess) {
					deleteYmlFile(project_add_filepath);
					
					InterfaceAutoCrawlSetting test = VexInstanceFactory.getAutoCrawlSetting(oInitInfo);
					boolean res = false;
					try{
						res = test.execute(newID,crawling_filepath);
						if(CommonUtil.isObjectNull(test)){
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_AUTO_CRAWL));
							throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
						}
						if(!res || !CommonUtil.isStringNullOrEmpty(test.getErrorMessage())){
							params.put("Vex Error Message", test.getErrorMessage());
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_AUTO_CRAWL));
							throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
						}
					}catch(Exception e){
						params.put("Vex Scan Id", newID);
						crawling_filepath = crawling_filepath.replace("//", "/");
						params.put("crawling filepath", crawling_filepath);
						params.put("error", e.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					}catch (Throwable t){
						params.put("Vex Scan Id", newID);
						crawling_filepath = crawling_filepath.replace("//", "/");
						params.put("crawling filepath", crawling_filepath);
						params.put("error", t.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					} finally {
						if (res) {
							deleteYmlFile(crawling_filepath);
						}
					}
					
					boolean infoFile = false;
					InterfaceEditReportDefinition reportDefinition = VexInstanceFactory.getEditReportDefinition(oInitInfo);
					
					try {
						infoFile = reportDefinition.execute(newID, report_information_path);
					} catch(Exception e){
						params.put("Vex Scan Id", newID);
						report_information_path = report_information_path.replace("//", "/");
						params.put("report definition filepath", report_information_path);
						params.put("error", e.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					}catch (Throwable t){
						params.put("Vex Scan Id", newID);
						report_information_path = report_information_path.replace("//", "/");
						params.put("report definition filepath", report_information_path);
						params.put("error", t.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					} finally {
						if (infoFile) {
							deleteYmlFile(report_information_path);
						}
					}
					
				}else{
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
				}
			}
		} catch (FileNotFoundException fnfe) {
			log.debug(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, fnfe);
			throw new UBSPortalException(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalMessages.FILE_NOT_FOUND_EXCEPTION, fnfe);
		} catch (IOException ie) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ie);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.IO_EXCEPTION, ie);
		} catch (PortalException pe) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static VexScanSettingItem addUpdateAutoCrawlingSetting (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		VexScanSettingItem scanSetting = new VexScanSettingItem();
		BufferedReader reader = null;
		String temp = null;
		
		try {
			String startURL = ParamUtil.getString(actionRequest, PortalConstants.PARAM_START_URL);
			String authorizedPatrolURL = ParamUtil.getString(actionRequest, PortalConstants.PARAM_AUTHORIZED_PATROL_URL);
			String unauthorizedpatrolURL = ParamUtil.getString(actionRequest, PortalConstants.PARAM_UNAUTHORIZED_PATROL_URL);
			String loginstatusdetection = ParamUtil.getString(actionRequest, PortalConstants.PARAM_LOGIN_STATUS_DETECTION).trim();
			int setnotsendpassword = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SET_NOT_SEND_PASSWORD);
			String parameternameaspassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PARAMETER_NAME_AS_PASSWORD).trim();
			String maxnumdetectionlink = ParamUtil.getString(actionRequest, PortalConstants.PARAM_MAX_NUM_DETECTION_LINK).trim();
			String maxnumdetectionlinkperpage = ParamUtil.getString(actionRequest, PortalConstants.PARAM_MAX_NUM_DETECTION_LINK_PER_PAGE).trim();
			String detectionlinkdepthlimit = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_LINK_DEPTH_LIMIT).trim();
			String waittime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_WAIT_TIME).trim();
			String numofthread = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NUM_OF_THREAD).trim();
			String timeouttime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TIME_OUT_TIME).trim();
			int setautopostform = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SET_AUTO_POST_FORM);
			int setpostmethod = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SET_POST_METHOD);
			String requestheader = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REQUEST_HEADER).trim();
			String numofretryaterror = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NUM_OF_RETRY_AT_ERROR).trim();
			String sessionerrordetection = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SESSION_ERROR_DETECTION).trim();
			String screentransitionerrordetection  = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCREEN_TRANSITION_ERROR_DETECTION).trim();
			String excludepathinforegex = ParamUtil.getString(actionRequest, PortalConstants.PARAM_EXCLUDE_PATH_INFO_REGEX).trim();
			String excludeparameterdeleteparamregex = ParamUtil.getString(actionRequest, PortalConstants.PARAM_EXCLUDE_PARAM_REGEX).trim();
			String excludeparameterdeletenameregex  = ParamUtil.getString(actionRequest, PortalConstants.PARAM_EXCLUDE_NAME_REGEX).trim();
			String extracturlregex = ParamUtil.getString(actionRequest, PortalConstants.PARAM_EXTRACT_URL_REGEX).trim();
			String notparseextension = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NOT_PARSE_EXTENTION).trim();
			String notparsefilenameregex  = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NOT_PARSE_FILENAME_REGEX).trim();
			
			StringBuilder tempStartURL = null;
			StringBuilder tempAuthorizedPatrolURL = null;
			StringBuilder tempUnauthorizedPatrolURL = null;
			StringBuilder tempLoginStatusDetection = null;
			StringBuilder tempParameterNameAsPassword = null;
			StringBuilder tempRequestHeader = null;
			StringBuilder tempSessionErrorDetection = null;
			StringBuilder tempScreenTransitionErrorDetection = null;
			StringBuilder tempExcludePathInfoRegex = null;
			StringBuilder tempExcludeParameterDeleteParamRegex = null;
			StringBuilder tempExcludeParameterDeleteNameRegex = null;
			StringBuilder tempExtractUrlRegex = null;
			StringBuilder tempNotParseExtension = null;
			StringBuilder tempNotParseFileNameRegex = null;
			
			if (!CommonUtil.isStringNullOrEmpty(startURL)) {
				tempStartURL = new StringBuilder();
				reader = new BufferedReader(new StringReader(startURL));
				while ((temp = reader.readLine()) != null) {
					tempStartURL.append(temp.trim() + "\n");
				}
				scanSetting.setStarturl(tempStartURL.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(authorizedPatrolURL)) {
				tempAuthorizedPatrolURL = new StringBuilder();
				reader = new BufferedReader(new StringReader(authorizedPatrolURL));
				while ((temp = reader.readLine()) != null) {
					tempAuthorizedPatrolURL.append(temp.trim() + "\n");
		        }
				scanSetting.setAuthorizedpatrolurl(tempAuthorizedPatrolURL.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(unauthorizedpatrolURL)) {
				tempUnauthorizedPatrolURL = new StringBuilder();
				reader = new BufferedReader(new StringReader(unauthorizedpatrolURL));
				while ((temp = reader.readLine()) != null) {
					tempUnauthorizedPatrolURL.append(temp.trim() + "\n");
		        }
				scanSetting.setUnauthorizedpatrolurl(tempUnauthorizedPatrolURL.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(loginstatusdetection)) {
				tempLoginStatusDetection = new StringBuilder();
				reader = new BufferedReader(new StringReader(loginstatusdetection));
				while ((temp = reader.readLine()) != null) {
					tempLoginStatusDetection.append(temp.trim() + "\n");
		        }
				scanSetting.setLoginstatusdetection(tempLoginStatusDetection.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(parameternameaspassword)) {
				tempParameterNameAsPassword = new StringBuilder();
				reader = new BufferedReader(new StringReader(parameternameaspassword));
				while ((temp = reader.readLine()) != null) {
					tempParameterNameAsPassword.append(temp.trim() + "\n");
		        }
				scanSetting.setParamnamesaspassword(tempParameterNameAsPassword.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(requestheader)) {
				tempRequestHeader = new StringBuilder();
				reader = new BufferedReader(new StringReader(requestheader));
				while ((temp = reader.readLine()) != null) {
					tempRequestHeader.append(temp.trim() + "\n");
		        }
				scanSetting.setRequestheader(tempRequestHeader.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(sessionerrordetection)) {
				tempSessionErrorDetection = new StringBuilder();
				reader = new BufferedReader(new StringReader(sessionerrordetection));
				while ((temp = reader.readLine()) != null) {
					tempSessionErrorDetection.append(temp.trim() + "\n");
		        }
				scanSetting.setSessionerrordetection(tempSessionErrorDetection.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(screentransitionerrordetection)) {
				tempScreenTransitionErrorDetection = new StringBuilder();
				reader = new BufferedReader(new StringReader(screentransitionerrordetection));
				while ((temp = reader.readLine()) != null) {
					tempScreenTransitionErrorDetection.append(temp.trim() + "\n");
		        }
				scanSetting.setScreentransitionerrordetection(tempScreenTransitionErrorDetection.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(excludepathinforegex)) {
				tempExcludePathInfoRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(excludepathinforegex));
				while ((temp = reader.readLine()) != null) {
					tempExcludePathInfoRegex.append(temp.trim() + "\n");
		        }
				scanSetting.setExcludepathinforegex(tempExcludePathInfoRegex.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(excludeparameterdeleteparamregex)) {
				tempExcludeParameterDeleteParamRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(excludeparameterdeleteparamregex));
				while ((temp = reader.readLine()) != null) {
					tempExcludeParameterDeleteParamRegex.append(temp.trim() + "\n");
		        }
				scanSetting.setExcludeparameterdeleteparamregex(tempExcludeParameterDeleteParamRegex.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(excludeparameterdeletenameregex)) {
				tempExcludeParameterDeleteNameRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(excludeparameterdeletenameregex));
				while ((temp = reader.readLine()) != null) {
					tempExcludeParameterDeleteNameRegex.append(temp.trim() + "\n");
		        }
				scanSetting.setExcludeparameterdeletenameregex(tempExcludeParameterDeleteNameRegex.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(extracturlregex)) {
				tempExtractUrlRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(extracturlregex));
				while ((temp = reader.readLine()) != null) {
					tempExtractUrlRegex.append(temp.trim() + "\n");
		        }
				scanSetting.setExtracturlregex(tempExtractUrlRegex.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(notparseextension)) {
				tempNotParseExtension = new StringBuilder();
				reader = new BufferedReader(new StringReader(notparseextension));
				while ((temp = reader.readLine()) != null) {
					tempNotParseExtension.append(temp.trim() + "\n");
		        }
				scanSetting.setNotparseextension(tempNotParseExtension.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(notparsefilenameregex)) {
				tempNotParseFileNameRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(notparsefilenameregex));
				while ((temp = reader.readLine()) != null) {
					tempNotParseFileNameRegex.append(temp.trim() + "\n");
		        }
				scanSetting.setNotparsefilenameregex(tempNotParseFileNameRegex.toString());
			}
			
		    scanSetting.setSetnotsendpassword(setnotsendpassword);
		    if (Validator.isNumber(maxnumdetectionlink)) {
		    	 scanSetting.setMaxnumdetectionlink(Integer.parseInt(maxnumdetectionlink));
			}else{
				scanSetting.setMaxnumdetectionlink(500);
			}
		    if (Validator.isNumber(maxnumdetectionlinkperpage)) {
		    	 scanSetting.setMaxnumdetectionlinkperpage(Integer.parseInt(maxnumdetectionlinkperpage));
			}else{
				scanSetting.setMaxnumdetectionlinkperpage(100);
			}
		    if(Validator.isNumber(detectionlinkdepthlimit)) {
		    	 scanSetting.setDetectionlinkdepthlimit(Integer.parseInt(detectionlinkdepthlimit));
			}else{
				 scanSetting.setDetectionlinkdepthlimit(20);
			}
		    if (Validator.isNumber(waittime)) {
		    	 scanSetting.setWaittime(Integer.parseInt(waittime));
			}else{
				 scanSetting.setWaittime(0);
			}
		    if (Validator.isNumber(numofthread)) {
		    	 scanSetting.setNumofthread(Integer.parseInt(numofthread));
			}else{
				 scanSetting.setNumofthread(1);
			}
		    if (Validator.isNumber(timeouttime)) {
		    	 scanSetting.setTimeouttime(Integer.parseInt(timeouttime));
			}else{
				 scanSetting.setTimeouttime(60000);
			}
		    scanSetting.setSetautopostform(setautopostform);
		    scanSetting.setSetpostmethod(setpostmethod);
		    scanSetting.setNumofretryaterror(numofretryaterror);

		} catch (IOException ie) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, ie);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.IO_EXCEPTION, ie);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return scanSetting;
	}
	
	public static boolean addVexScanDetailSetting (ActionRequest actionRequest, int userAction, long lUserId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		PortletSession pSession = actionRequest.getPortletSession();
		VexScanSettingItem scanSettingfromSession = null;
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		List<VexTargetInformation> targetInformationList = null;
		ArrayList<ArrayList<VexLoginSetting>> loginSettingList = null;
		ArrayList<VexLoginSetting> loginSettingListItem = null;
		List<VexTargetInformationItem> targetInformationListfromSession = null;
		ArrayList <ArrayList<VexLoginSettingItem>> loginSettingListfromSession = null;
		BufferedReader reader = null;
		String temp = null;
		String scanName = PortalConstants.STRING_EMPTY;
		ValidateResult result = null;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			try{
				scanName = pSession.getAttribute(PortalConstants.PARAM_SCAN_NAME).toString();
				scanSettingfromSession = (VexScanSettingItem) pSession.getAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
				targetInformationListfromSession = (List<VexTargetInformationItem>) pSession.getAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
			}catch(NullPointerException e){
				if (CommonUtil.isStringNullOrEmpty(scanName)){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_SESSION_GET_DATA_FAILED, PortalConstants.ERROR_LOG_PARAM_SCAN_NAME));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.SESSION_GET_SCAN_NAME_FAILED);
				}else if(CommonUtil.isObjectNull(scanSettingfromSession)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_SESSION_GET_DATA_FAILED, PortalConstants.ERROR_LOG_PARAM_SCAN_SETTING));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.SESSION_GET_SCAN_SETTING_FAILED);
				}else if(CommonUtil.isListNullOrEmpty(targetInformationListfromSession)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_SESSION_GET_DATA_FAILED, PortalConstants.ERROR_LOG_PARAM_TARGET_INFORMATION_LIST));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.SESSION_GET_TARGET_INFORMATION_LIST_FAILED);
				}
			}
			
			loginSettingListfromSession = (ArrayList <ArrayList<VexLoginSettingItem>>) pSession.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
			
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			String startURL = ParamUtil.getString(actionRequest, PortalConstants.PARAM_START_URL).trim();
			String authorizedPatrolURL = ParamUtil.getString(actionRequest, PortalConstants.PARAM_AUTHORIZED_PATROL_URL).trim();
			String unauthorizedpatrolURL = ParamUtil.getString(actionRequest, PortalConstants.PARAM_UNAUTHORIZED_PATROL_URL).trim();
			String loginstatusdetection = ParamUtil.getString(actionRequest, PortalConstants.PARAM_LOGIN_STATUS_DETECTION).trim();
			int setnotsendpassword = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SET_NOT_SEND_PASSWORD);
			String parameternameaspassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PARAMETER_NAME_AS_PASSWORD).trim();
			String maxnumdetectionlink = ParamUtil.getString(actionRequest, PortalConstants.PARAM_MAX_NUM_DETECTION_LINK).trim();
			String maxnumdetectionlinkperpage = ParamUtil.getString(actionRequest, PortalConstants.PARAM_MAX_NUM_DETECTION_LINK_PER_PAGE).trim();
			String detectionlinkdepthlimit = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_LINK_DEPTH_LIMIT).trim();
			String waittime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_WAIT_TIME).trim();
			String numofthread = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NUM_OF_THREAD).trim();
			String timeouttime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TIME_OUT_TIME).trim();
			int setautopostform = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SET_AUTO_POST_FORM);
			int setpostmethod = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SET_POST_METHOD);
			String requestheader = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REQUEST_HEADER).trim();
			String numofretryaterror = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NUM_OF_RETRY_AT_ERROR).trim();
			String sessionerrordetection = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SESSION_ERROR_DETECTION).trim();
			String screentransitionerrordetection  = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCREEN_TRANSITION_ERROR_DETECTION).trim();
			String excludepathinforegex = ParamUtil.getString(actionRequest, PortalConstants.PARAM_EXCLUDE_PATH_INFO_REGEX).trim();
			String excludeparameterdeletenameregex  = ParamUtil.getString(actionRequest, PortalConstants.PARAM_EXCLUDE_NAME_REGEX).trim();
			String excludeparameterdeleteparamregex = ParamUtil.getString(actionRequest, PortalConstants.PARAM_EXCLUDE_PARAM_REGEX).trim();
			String extracturlregex = ParamUtil.getString(actionRequest, PortalConstants.PARAM_EXTRACT_URL_REGEX).trim();
			String notparseextension = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NOT_PARSE_EXTENTION).trim();
			String notparsefilenameregex  = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NOT_PARSE_FILENAME_REGEX).trim();

			StringBuilder tempStartURL = null;
			StringBuilder tempAuthorizedPatrolURL = null;
			StringBuilder tempUnauthorizedPatrolURL = null;
			StringBuilder tempLoginStatusDetection = null;
			StringBuilder tempParameterNameAsPassword = null;
			StringBuilder tempRequestHeader = null;
			StringBuilder tempSessionErrorDetection = null;
			StringBuilder tempScreenTransitionErrorDetection = null;
			StringBuilder tempExcludePathInfoRegex = null;
			StringBuilder tempExcludeParameterDeleteNameRegex = null;
			StringBuilder tempExcludeParameterDeleteParamRegex = null;
			StringBuilder tempExtractUrlRegex = null;
			StringBuilder tempNotParseExtension = null;
			StringBuilder tempNotParseFileNameRegex = null;
			
			if (CommonUtil.isStringNullOrEmpty(startURL)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_START_URL));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_START_URL);
			} else {
				tempStartURL = new StringBuilder();
				reader = new BufferedReader(new StringReader(startURL));
				while ((temp = reader.readLine()) != null) {
					if (!Validator.isUrl(temp.trim())) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.START_URL_INVALID);
					}

					if (temp.trim().contains(PortalConstants.STRING_DOUBLE_QUOTE) || temp.trim().contains(PortalConstants.STRING_BACK_SLASH)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.UNUSABLE_CHARACTER_INPUT);
					}
					
					if(!ControllerHelper.isVexInputValid(temp.trim())){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.INVALID_CHARACTER_INPUT);
					}
					
					try {
						URL strstartURL = new URL(temp);
						if(strstartURL.getHost().equals("localhost") || strstartURL.getHost().equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_START_URL));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_URL_LOCALHOST_INVALID);
						}
					} catch (MalformedURLException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.START_URL_LOCALHOST_INVALID);
					} 
					
					boolean isStartURLInTargetInfo = PortalConstants.FALSE;
					
					for (VexTargetInformationItem item : targetInformationListfromSession) {
						//check if target info in start url is in target information table
						URL startUrl = new URL(temp.trim());
						
						if(startUrl.getProtocol().equals(item.getProtocol().replaceAll("[^a-zA-Z0-9]", "")) && startUrl.getHost().equals(item.getHost()) && startUrl.getPort() == Integer.parseInt(item.getPort())){
							isStartURLInTargetInfo = PortalConstants.TRUE;
							break;
						}
					}
					
					if (!isStartURLInTargetInfo) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.START_URL_INVALID);
					}
					
					tempStartURL.append(temp.trim() + ";");
		        }
			}
			
			if (CommonUtil.isStringNullOrEmpty(authorizedPatrolURL)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_AUTHORIZED_PATROL_URL);
			} else {
				tempAuthorizedPatrolURL = new StringBuilder();
				reader = new BufferedReader(new StringReader(authorizedPatrolURL));
				while ((temp = reader.readLine()) != null) {
					if (!Validator.isUrl(temp.trim())) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.AUTHORIZED_PATROL_URL_INVALID);
					}

					if (temp.trim().contains(PortalConstants.STRING_DOUBLE_QUOTE) || temp.trim().contains(PortalConstants.STRING_BACK_SLASH)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.UNUSABLE_CHARACTER_INPUT);
					}
					
					if(!ControllerHelper.isVexInputValid(temp.trim())){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.INVALID_CHARACTER_INPUT);
					}
					
					try {
						URL strAuthorizedPatrolURL = new URL(temp);
						if(strAuthorizedPatrolURL.getHost().equals("localhost") || strAuthorizedPatrolURL.getHost().equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
							throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.AUTHORIZED_PATROL_URL_LOCALHOST_INVALID);
						}
					} catch (MalformedURLException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
										PortalConstants.ERROR_LOG_PARAM_AUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.AUTHORIZED_PATROL_URL_LOCALHOST_INVALID);
					} 
					
					tempAuthorizedPatrolURL.append(temp.trim() + ";");
		        }
			}
			
			if (!CommonUtil.isStringNullOrEmpty(unauthorizedpatrolURL)) {
				tempUnauthorizedPatrolURL = new StringBuilder();
				reader = new BufferedReader(new StringReader(unauthorizedpatrolURL));
				while ((temp = reader.readLine()) != null) {
					if (temp.trim().contains(PortalConstants.STRING_DOUBLE_QUOTE) || temp.trim().contains(PortalConstants.STRING_BACK_SLASH)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_UNAUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.UNUSABLE_CHARACTER_INPUT);
					}
					
					if(!ControllerHelper.isVexInputValid(temp.trim())){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_UNAUTHORIZED_PATROL_URL));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.INVALID_CHARACTER_INPUT);
					}
					tempUnauthorizedPatrolURL.append(temp.trim() + ";");
		        }
			}
			
			if (!CommonUtil.isStringNullOrEmpty(loginstatusdetection)) {
				tempLoginStatusDetection = new StringBuilder();
				reader = new BufferedReader(new StringReader(loginstatusdetection));
				while ((temp = reader.readLine()) != null) {
					result = ValidationUtil.validateRegex(temp, PortalConstants.FALSE);
					if(!result.valid_flg){
				    	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
				    }
					tempLoginStatusDetection.append(temp.trim() + ";");
		        }
			}
			
			if (!CommonUtil.isStringNullOrEmpty(parameternameaspassword)) {
				tempParameterNameAsPassword = new StringBuilder();
				reader = new BufferedReader(new StringReader(parameternameaspassword));
				while ((temp = reader.readLine()) != null) {
					String param = temp.trim().replaceAll("\\W", "");
					if (!Validator.isVariableName(param.trim())) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PARAMETER_NAME_AS_PASSWORD));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.PARAMETER_NAME_AS_PASSWORD_INVALID);
					}
					result = ValidationUtil.validateRegex(temp, PortalConstants.FALSE);
					if(!result.valid_flg){
				    	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
				    }
					tempParameterNameAsPassword.append(temp.trim() + ";");
		        }
			}
			
			if (CommonUtil.isStringNullOrEmpty(maxnumdetectionlink)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_MAX_NUM_DETECTION_LINK));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_MAX_NUM_DETECTION_LINK);
			} else {
				if (!Validator.isNumber(maxnumdetectionlink)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_MAX_NUM_DETECTION_LINK));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.MAX_NUM_DETECTION_LINK_INVALID);
				}
			}
			
			if (CommonUtil.isStringNullOrEmpty(maxnumdetectionlinkperpage)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_MAX_NUM_DETECTION_LINK_PER_PAGE));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_MAX_NUM_DETECTION_LINK_PER_PAGE);
			} else {
				if (!Validator.isNumber(maxnumdetectionlinkperpage)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_MAX_NUM_DETECTION_LINK_PER_PAGE));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.MAX_NUM_DETECTION_LINK_PER_PAGE_INVALID);
				}
			}
			
			if (CommonUtil.isStringNullOrEmpty(detectionlinkdepthlimit)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_DETECTION_LINK_DEPTH_LIMIT));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_DETECTION_LINK_DEPTH_LIMIT);
			} else {
				if (!Validator.isNumber(detectionlinkdepthlimit)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_DETECTION_LINK_DEPTH_LIMIT));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.DETECTION_LINK_DEPTH_LIMIT_INVALID);
				}
			}
			
			if (CommonUtil.isStringNullOrEmpty(waittime)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_WAIT_TIME));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_WAIT_TIME);
			} else {
				if (!Validator.isNumber(waittime)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_WAIT_TIME));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.WAIT_TIME_INVALID);
				}
			}
			
			if (CommonUtil.isStringNullOrEmpty(numofthread)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_NUM_OF_THREAD));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_NUM_OF_THREAD);
			} else {
				if (!Validator.isNumber(numofthread)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NUM_OF_THREAD));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NUM_OF_THREAD_INVALID);
				}
			}
			
			if (CommonUtil.isStringNullOrEmpty(timeouttime)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_TIME_OUT_TIME));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_TIME_OUT_TIME);
			} else {
				if (!Validator.isNumber(timeouttime)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_TIME_OUT_TIME));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.TIME_OUT_TIME_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(requestheader)) {
				tempRequestHeader = new StringBuilder();
				
				reader = new BufferedReader(new StringReader(requestheader));
				
				while ((temp = reader.readLine()) != null) {
					tempRequestHeader.append(temp.trim() + "|");
		        }
			}
			
			if (CommonUtil.isStringNullOrEmpty(numofretryaterror)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_NUM_OF_RETRY_AT_ERROR));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_NUM_OF_RETRY_AT_ERROR);
			} else {
				if (!Validator.isNumber(numofretryaterror)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NUM_OF_RETRY_AT_ERROR));
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NUM_OF_RETRY_AT_ERROR_INVALID);
				}
			}
			
			
			if (!CommonUtil.isStringNullOrEmpty(sessionerrordetection)) {
				tempSessionErrorDetection = new StringBuilder();
				reader = new BufferedReader(new StringReader(sessionerrordetection));
				while ((temp = reader.readLine()) != null) {
					result = ValidationUtil.validateRegex(temp, PortalConstants.FALSE);
					if(!result.valid_flg){
				    	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
				    }
					tempSessionErrorDetection.append(temp.trim() + ";");
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(screentransitionerrordetection)) {
				tempScreenTransitionErrorDetection = new StringBuilder();
				reader = new BufferedReader(new StringReader(screentransitionerrordetection));
				while ((temp = reader.readLine()) != null) {
					result = ValidationUtil.validateRegex(temp, PortalConstants.FALSE);
					if(!result.valid_flg){
				    	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
				    }
					tempScreenTransitionErrorDetection.append(temp.trim() + ";");
		        }
			}
			
			if (!CommonUtil.isStringNullOrEmpty(excludepathinforegex)) {
				tempExcludePathInfoRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(excludepathinforegex));
				while ((temp = reader.readLine()) != null) {
					result = ValidationUtil.validateRegex(temp, PortalConstants.FALSE);
					if(!result.valid_flg){
				    	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
				    }
					tempExcludePathInfoRegex.append(temp.trim() + ";");
		        }
			}
			
			if (!CommonUtil.isStringNullOrEmpty(excludeparameterdeletenameregex)) {
				tempExcludeParameterDeleteNameRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(excludeparameterdeletenameregex));
				while ((temp = reader.readLine()) != null) {
					result = ValidationUtil.validateRegex(temp, PortalConstants.FALSE);
					if(!result.valid_flg){
				    	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
				    }
					tempExcludeParameterDeleteNameRegex.append(temp.trim() + ";");
		        }
			}
			
			if (!CommonUtil.isStringNullOrEmpty(excludeparameterdeleteparamregex)) {
				tempExcludeParameterDeleteParamRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(excludeparameterdeleteparamregex));
				while ((temp = reader.readLine()) != null) {
					result = ValidationUtil.validateRegex(temp, PortalConstants.FALSE);
					if(!result.valid_flg){
				    	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
				    }
					tempExcludeParameterDeleteParamRegex.append(temp.trim() + ";");
		        }
			}

			if (!CommonUtil.isStringNullOrEmpty(extracturlregex)) {
				tempExtractUrlRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(extracturlregex));
				while ((temp = reader.readLine()) != null) {
					result = ValidationUtil.validateRegex(temp, PortalConstants.TRUE);
					if(!result.valid_flg){
						if(result.message.equals(PortalMessages.REGULAR_EXPRESSION_INVALID)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
							throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
						} else {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_GROUPING_INVALID));
							throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_GROUPING_INVALID);
						}
				    }
					tempExtractUrlRegex.append(temp.trim() + ";");
		        }
			}
			
			if (!CommonUtil.isStringNullOrEmpty(notparseextension)) {
				tempNotParseExtension = new StringBuilder();
				reader = new BufferedReader(new StringReader(notparseextension));
				while ((temp = reader.readLine()) != null) {
					tempNotParseExtension.append(temp.trim() + ";");
		        }
			}
			
			if (!CommonUtil.isStringNullOrEmpty(notparsefilenameregex)) {
				tempNotParseFileNameRegex = new StringBuilder();
				reader = new BufferedReader(new StringReader(notparsefilenameregex));
				while ((temp = reader.readLine()) != null) {
					result = ValidationUtil.validateRegex(temp, PortalConstants.FALSE);
					if(!result.valid_flg){
				    	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REGULAR_EXPRESSION_INVALID));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.REGULAR_EXPRESSION_INVALID);
				    }
					tempNotParseFileNameRegex.append(temp.trim() + ";");
		        }
			}
			
			int implementationEnvironment = scanSettingfromSession.getImplementationenvironment();
			String webInspectionSignatureSetGroup = scanSettingfromSession.getSelectedwebsignature();
			int serverFiles = scanSettingfromSession.getGetserverfiles();
			int serverSettings = scanSettingfromSession.getGetserversettings();
			String serverFilesSignatureSetGroup = scanSettingfromSession.getSelectedserverfilessignature();
			String serverSettingsSignatureSetGroup = scanSettingfromSession.getSelectedserversettingssignature();
			Date startTime = scanSettingfromSession.getStarttime();
			Date endTime = scanSettingfromSession.getEndtime();
			int emailNotification = scanSettingfromSession.getSetemailnotification();
			int setCrawlingOnly = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_CRAWLING_ONLY);
			
			if(CommonUtil.isStringNullOrEmpty(webInspectionSignatureSetGroup)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_WEB_INSPECTION_SIGNATURE_SET_GROUP);
			}
			
			if(CommonUtil.isStringNullOrEmpty(serverFilesSignatureSetGroup)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_SERVER_FILE_SIGNATURE_SET_GROUP);
			}
			
			if(CommonUtil.isStringNullOrEmpty(serverSettingsSignatureSetGroup)){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP));
				throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.NO_SERVER_SETTING_SIGNATURE_SET_GROUP);
			}

			targetInformationList = new ArrayList<>();
			
			for (int i = 0; i < targetInformationListfromSession.size(); i++) {
					VexTargetInformation item = VexTargetInformationLocalServiceUtil.createVexTargetInformationObj();
					item.setScanid(i);
					item.setProtocol(targetInformationListfromSession.get(i).getProtocol());
					item.setHost(targetInformationListfromSession.get(i).getHost());
					item.setPort(Integer.parseInt(targetInformationListfromSession.get(i).getPort()));
					item.setHttpversion(targetInformationListfromSession.get(i).getHttpversion());
					item.setSetkeepaliveconnection(targetInformationListfromSession.get(i).getSetkeepaliveconnection());
					item.setSetresponsecontentlength(targetInformationListfromSession.get(i).getSetresponsecontentlength());
					item.setUseacceptencodingheader(targetInformationListfromSession.get(i).getUseacceptencodingheader());
					item.setUnzipresponse(targetInformationListfromSession.get(i).getUnzipresponse());
					item.setHttpprotocol(targetInformationListfromSession.get(i).getHttpprotocol());
					item.setExternalproxyhost(targetInformationListfromSession.get(i).getExternalproxyhost());
					item.setExternalproxyport(targetInformationListfromSession.get(i).getExternalproxyport());
					item.setExternalproxyauthid(targetInformationListfromSession.get(i).getExternalproxyauthid());
					item.setExternalproxyauthpassword(targetInformationListfromSession.get(i).getExternalproxyauthpassword());
					item.setUseclientcertificate(targetInformationListfromSession.get(i).getUseclientcertificate());
					item.setCertificatefile(targetInformationListfromSession.get(i).getCertificatefile());
					item.setCertificatefilepassword(targetInformationListfromSession.get(i).getCertificatefilepassword());
					item.setNtlmauthid(targetInformationListfromSession.get(i).getNtlmauthid());
					item.setNtlmauthpassword(targetInformationListfromSession.get(i).getNtlmauthpassword());
					item.setNtlmauthdomain(targetInformationListfromSession.get(i).getNtlmauthdomain());
					item.setNtlmauthhost(targetInformationListfromSession.get(i).getNtlmauthhost());
					item.setDigestauthid(targetInformationListfromSession.get(i).getDigestauthid());
					item.setDigestauthpassword(targetInformationListfromSession.get(i).getDigestauthpassword());
					item.setBasicauthid(targetInformationListfromSession.get(i).getBasicauthid());
					item.setBasicauthpassword(targetInformationListfromSession.get(i).getBasicauthpassword());
					item.setAccessexclusionpath(targetInformationListfromSession.get(i).getAccessexclusionpath());
					targetInformationList.add(item);
			}
			
			loginSettingList = new ArrayList <ArrayList<VexLoginSetting>>(loginSettingListfromSession.size());
			
			int i = 0;
			for (ArrayList<VexLoginSettingItem> sessionItem : loginSettingListfromSession) {
				loginSettingListItem = new ArrayList<>();
				for(VexLoginSettingItem sessionSubItem : sessionItem){
					if(sessionSubItem.getChecked() != PortalConstants.INT_ZERO){
						VexLoginSetting item = VexLoginSettingLocalServiceUtil.createVexLoginSettingObj();
						item.setScanid(i);
						item.setLoginurl(sessionSubItem.getLoginurl());
						item.setParamname(sessionSubItem.getParamname());
						item.setParamvalue(sessionSubItem.getParamvalue().replaceAll(PortalConstants.STRING_DOUBLE_QUOTE, PortalConstants.STRING_EMPTY));
						loginSettingListItem.add(item);
						i++;
					}
				}
				loginSettingList.add(loginSettingListfromSession.indexOf(sessionItem),loginSettingListItem);
			}

			// create scan id
			lScanId = ScanLocalServiceUtil.createScanId();
			File fileDirectory = FileProcessUtil.createCrawlSettingYAMLPath(lProjectId, lScanId);
			
			
			if (fileDirectory.isDirectory()) {
				String vexScanName = lProjectId + PortalConstants.STR_UNDERSCORE + lScanId + PortalConstants.STR_UNDERSCORE + scanName;
				//create _PROJECT_ADD.yml file
				String projectadd_filepath = createProjectAddDetailSettingFile(lScanId, lProjectId, lUserId,
						vexScanName, fileDirectory, targetInformationList);
				
				StringBuilder base_start_url = new StringBuilder();
				StringBuilder base_crawl_allow_url_wildcard = new StringBuilder();
				StringBuilder base_crawl_exclude_url_wildcard = new StringBuilder();
				StringBuilder base_crawl_session_success_body_regex = new StringBuilder();
				StringBuilder base_password_parameter_name_regex = new StringBuilder();
				StringBuilder base_request_header = new StringBuilder();
				StringBuilder base_crawl_session_error_body_regex  = new StringBuilder();
				StringBuilder base_crawl_transition_error_body_regex  = new StringBuilder();
				StringBuilder base_dup_exclude_pathinfo_regex  = new StringBuilder();
				StringBuilder base_dup_exclude_parameter_delete_param_regex  = new StringBuilder();
				StringBuilder base_dup_exclude_parameter_delete_name_regex  = new StringBuilder();
				StringBuilder base_extract_url_regex  = new StringBuilder();
				StringBuilder base_crawl_not_parse_extension  = new StringBuilder();
				StringBuilder base_crawl_not_parse_filename_regex  = new StringBuilder();
				
				temp = null;
				
				reader = new BufferedReader(new StringReader(startURL));
				while ((temp = reader.readLine()) != null) {
					base_start_url.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(authorizedPatrolURL));
				while ((temp = reader.readLine()) != null) {
					base_crawl_allow_url_wildcard.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(unauthorizedpatrolURL));
				while ((temp = reader.readLine()) != null) {
					base_crawl_exclude_url_wildcard.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(loginstatusdetection));
				while ((temp = reader.readLine()) != null) {
					base_crawl_session_success_body_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(parameternameaspassword));
				while ((temp = reader.readLine()) != null) {
					base_password_parameter_name_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(requestheader));
				while ((temp = reader.readLine()) != null) {
					base_request_header.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(sessionerrordetection));
				while ((temp = reader.readLine()) != null) {
					base_crawl_session_error_body_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(screentransitionerrordetection));
				while ((temp = reader.readLine()) != null) {
					base_crawl_transition_error_body_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(excludepathinforegex));
				while ((temp = reader.readLine()) != null) {
					base_dup_exclude_pathinfo_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(excludeparameterdeletenameregex));
				while ((temp = reader.readLine()) != null) {
					base_dup_exclude_parameter_delete_name_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(excludeparameterdeleteparamregex));
				while ((temp = reader.readLine()) != null) {
					base_dup_exclude_parameter_delete_param_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(extracturlregex));
				while ((temp = reader.readLine()) != null) {
					base_extract_url_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(notparseextension));
				while ((temp = reader.readLine()) != null) {
					base_crawl_not_parse_extension.append(" " + temp.trim() + System.lineSeparator());
		        }
				reader = new BufferedReader(new StringReader(notparsefilenameregex));
				while ((temp = reader.readLine()) != null) {
					base_crawl_not_parse_filename_regex.append(" " + temp.trim() + System.lineSeparator());
		        }
				
				//create _CRAWLER_SETTING.yml file
				String crawlingsetting_filepath = createCrawlingDetailSettingFile(lScanId, lProjectId, fileDirectory,
						base_start_url, base_crawl_allow_url_wildcard, base_crawl_exclude_url_wildcard, base_crawl_session_success_body_regex,
						base_password_parameter_name_regex, base_request_header, base_crawl_session_error_body_regex, base_crawl_transition_error_body_regex,
						base_dup_exclude_pathinfo_regex, base_dup_exclude_parameter_delete_param_regex, base_dup_exclude_parameter_delete_name_regex,
						base_extract_url_regex, base_crawl_not_parse_extension, base_crawl_not_parse_filename_regex,
						setautopostform, setpostmethod, maxnumdetectionlinkperpage, maxnumdetectionlink, setnotsendpassword, numofthread, 
						timeouttime, detectionlinkdepthlimit, waittime, numofretryaterror, loginSettingListfromSession, loginSettingList.size());
			
				//create _REPORT_INFORMATION_SETTING.yml file
				String report_information_path = createReportInformationFileDetailSetting(lScanId, lProjectId,
						fileDirectory, vexScanName, targetInformationList, implementationEnvironment);
				
				ApiInitInfo oInitInfo = VexScanMgmtController.setLoginCredentials();
				
				InterfaceProjectAdd projectAdd = VexInstanceFactory.getProjectAdd(oInitInfo);
				String newID = null;
				
				try {
					newID = projectAdd.executeEX(projectadd_filepath);
					if(CommonUtil.isObjectNull(projectAdd)){
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, "Vex" +
										PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.ADD_VEX_SCAN_FAILED);
					}
					if(!CommonUtil.isStringNullOrEmpty(projectAdd.getErrorMessage())){
						params.put("Vex Error Message", projectAdd.getErrorMessage());
						params.put(PortalConstants.ERROR_LOG_MESSAGE,
								String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, "Vex" +
										PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.ADD_VEX_SCAN_FAILED);
					}
				} catch (Throwable e) {
					projectadd_filepath = projectadd_filepath.replace("//", "/");
					params.put("projectadd filepath", projectadd_filepath);
					params.put("error", e.getCause());
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.ADD_VEX_SCAN_FAILED, e);		
				}

				bSuccess = saveVexProjectToDatabaseDetailSetting(lUserId, params,lScanId, lProjectId, scanName, bSuccess,
						targetInformationList, tempStartURL, tempAuthorizedPatrolURL, tempUnauthorizedPatrolURL, tempLoginStatusDetection,
						tempParameterNameAsPassword, tempRequestHeader, tempSessionErrorDetection, tempScreenTransitionErrorDetection, 
						tempExcludePathInfoRegex, tempExcludeParameterDeleteNameRegex, tempExcludeParameterDeleteParamRegex, 
						tempExtractUrlRegex, tempNotParseExtension, tempNotParseFileNameRegex, implementationEnvironment,
						setautopostform, setpostmethod, maxnumdetectionlinkperpage, maxnumdetectionlink, setnotsendpassword, numofthread, 
						timeouttime, detectionlinkdepthlimit, waittime, numofretryaterror, loginSettingList, newID,
						startTime, endTime, emailNotification, webInspectionSignatureSetGroup, serverFiles, serverSettings, 
						serverFilesSignatureSetGroup, serverSettingsSignatureSetGroup, setCrawlingOnly);
				
				
				if (bSuccess) {
					deleteYmlFile(projectadd_filepath);
					
					InterfaceAutoCrawlSetting test = VexInstanceFactory.getAutoCrawlSetting(oInitInfo);
					boolean res = false;
					try{
						res = test.execute(newID,crawlingsetting_filepath);
						if(CommonUtil.isObjectNull(test)){
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_AUTO_CRAWL));
							throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
						}
						if(!res || !CommonUtil.isStringNullOrEmpty(test.getErrorMessage())){
							params.put("Vex Error Message", test.getErrorMessage());
							params.put(PortalConstants.ERROR_LOG_MESSAGE,
									String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
											PortalConstants.ERROR_LOG_AUTO_CRAWL));
							throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
						}
					}catch(Exception e){
						params.put("Vex Scan Id", newID);
						crawlingsetting_filepath = crawlingsetting_filepath.replace("//", "/");
						params.put("crawling filepath", crawlingsetting_filepath);
						params.put("error", e.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					}catch (Throwable t){
						params.put("Vex Scan Id", newID);
						crawlingsetting_filepath = crawlingsetting_filepath.replace("//", "/");
						params.put("crawling filepath", crawlingsetting_filepath);
						params.put("error", t.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					} finally {
						if (res) {
							deleteYmlFile(crawlingsetting_filepath);
						}
					}
					
					boolean infoFile = false;
					InterfaceEditReportDefinition reportDefinition = VexInstanceFactory.getEditReportDefinition(oInitInfo);
					
					try {
						infoFile = reportDefinition.execute(newID, report_information_path);
					} catch(Exception e){
						params.put("Vex Scan Id", newID);
						report_information_path = report_information_path.replace("//", "/");
						params.put("report definition filepath", report_information_path);
						params.put("error", e.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					}catch (Throwable t){
						params.put("Vex Scan Id", newID);
						report_information_path = report_information_path.replace("//", "/");
						params.put("report definition filepath", report_information_path);
						params.put("error", t.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					} finally {
						if (infoFile) {
							deleteYmlFile(report_information_path);
						}
					}
					
				}else{
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
					throw new UBSPortalException(PortalErrors.ADD_VEX_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
				}
			}
		} catch (FileNotFoundException fnfe) {
			log.debug(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, fnfe);
			throw new UBSPortalException(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalMessages.FILE_NOT_FOUND_EXCEPTION, fnfe);
		} catch (IOException ie) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, ie);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.IO_EXCEPTION, ie);
		} catch (PortalException pe) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_ADVANCED_SETTING, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static ArrayList<VexLoginSettingItem> addUpdateLoginSettingList(ActionRequest actionRequest) throws UBSPortalException {
 		Map<String, Object> params = new HashMap<String, Object>();
		PortletSession pSession = actionRequest.getPortletSession();
		ArrayList <VexLoginSettingItem> loginInfoList = new ArrayList<>();

		try {	
			int numOfParams = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
			
			loginInfoList = new ArrayList<VexLoginSettingItem>();
			
			for (int i = 0; i < numOfParams; i++) {
				String loginurl = ParamUtil.getString(actionRequest, PortalConstants.PARAM_LOGIN_PATH_LABEL).trim();
				String paramname = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PARAMETER_NAME + i).trim();
				String paramvalue = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PARAMETER_VALUE + i).trim();
				int checked = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_CHECK_PARAMETER + i);
				
				if(!CommonUtil.isStringNullOrEmpty(loginurl) && CommonUtil.isStringNullOrEmpty(paramname) && CommonUtil.isStringNullOrEmpty(paramvalue)){
					continue;
				}else if(!CommonUtil.isStringNullOrEmpty(loginurl) && !CommonUtil.isStringNullOrEmpty(paramname) || !CommonUtil.isStringNullOrEmpty(paramvalue)){
					if (paramvalue.contains(PortalConstants.STRING_DOUBLE_QUOTE) || paramvalue.contains(PortalConstants.STRING_BACK_SLASH)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_LOGIN_PARAMETER_VALUE));
						throw new UBSPortalException(PortalErrors.UPDATE_LOGIN_SETTING_FAILED, PortalMessages.UNUSABLE_CHARACTER_INPUT);
					}
					
					if (!ControllerHelper.isVexInputValid(paramvalue)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_LOGIN_PARAMETER_VALUE));
						throw new UBSPortalException(PortalErrors.UPDATE_LOGIN_SETTING_FAILED, PortalMessages.LOGIN_PARAM_VALUE_INVALID);
					}
					
					VexLoginSettingItem item = new VexLoginSettingItem();
					item.setLoginurl(loginurl);
					item.setParamname(paramname);
					item.setParamvalue(paramvalue);
					item.setChecked(checked);
					loginInfoList.add(item);
				}
			}
			pSession.setAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS, numOfParams);	
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_CRAWL_LOGIN_INFO, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return loginInfoList;
	}
	
	private static String createReportInformationFile(long lScanId, long lProjectId, String loginURL,
			File fileDirectory, String scanName, List<VexTargetInformation> targetInformationList, int implementationEnvironment) throws IOException {
		StringWriter writer = new StringWriter();
		DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.PLAIN);
        options.setExplicitStart(false);
		Yaml reportInfoSettingYaml = new Yaml(options);
		
		VexReportInfoSettingItem reportInfoSetting = new VexReportInfoSettingItem();
		setReportInformationYamlContents(scanName, targetInformationList, reportInfoSetting, implementationEnvironment);
		reportInfoSettingYaml.dump(reportInfoSetting, writer);		
		
		SimpleDateFormat formatter = new SimpleDateFormat(PortalConstants.REPORT_DATE_FORMAT);
		String filename = formatter.format(new Date()) + "_" + lProjectId + "_" + lScanId + "_REPORT_INFORMATION_SETTING.yml";
		String filepath = fileDirectory.getAbsolutePath() + "//" + filename;
		FileOutputStream fos = new FileOutputStream(filepath);
		fos.write(writer.toString().getBytes());
		fos.close();
		return filepath;
	}
	
	private static String createReportInformationFileDetailSetting(long lScanId, long lProjectId,
			File fileDirectory, String scanName, List<VexTargetInformation> targetInformationList, int implementationEnvironment) throws IOException {
		StringWriter writer = new StringWriter();
		DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.DOUBLE_QUOTED);
        options.setExplicitStart(false);
		Yaml reportInfoSettingYaml = new Yaml(options);
		
		VexReportInfoSettingItem reportInfoSetting = new VexReportInfoSettingItem();
		setReportInformationYamlContents(scanName, targetInformationList, reportInfoSetting, implementationEnvironment);
		reportInfoSettingYaml.dump(reportInfoSetting, writer);
		
		SimpleDateFormat formatter = new SimpleDateFormat(PortalConstants.REPORT_DATE_FORMAT);
		String filename = formatter.format(new Date()) + "_" + lProjectId + "_" + lScanId + "_REPORT_INFORMATION_SETTING.yml";
		String filepath = fileDirectory.getAbsolutePath() + "//" + filename;
		FileOutputStream fos = new FileOutputStream(filepath);
		fos.write(writer.toString().getBytes());
		fos.close();
		return filepath;
	}
	
	private static void setReportInformationYamlContents(String scanName, List<VexTargetInformation> targetInformationList, VexReportInfoSettingItem reportInfoSetting, int implEnv) {
		List<VexTargetHostItem> targetHostList = new ArrayList<VexTargetHostItem>();
		List<VexTargetFuncItem> targetFunctionList = new ArrayList<VexTargetFuncItem>();
		List<VexEnvRemarkItem> envRemarkList = new ArrayList<VexEnvRemarkItem>();
		VexTargetHostItem targetHost;
		VexTargetFuncItem targetFunction;
		VexEnvRemarkItem envRemark;
		Calendar calendar = Calendar.getInstance();
		String audit_month = calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1); 
		String audit_period = calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DATE);
		
		VexReportOptionsItem reportOptions = new VexReportOptionsItem();
		reportOptions.setUseCapture(PortalConstants.TRUE);
		reportOptions.setReCapture(PortalConstants.FALSE);
		reportOptions.setOutput_by_signature(PortalConstants.TRUE);
		reportOptions.setOutput_by_request(PortalConstants.TRUE);
		reportOptions.setOutput_vuln_request_parameters(PortalConstants.TRUE);
		reportOptions.setOutput_host(PortalConstants.TRUE);
		reportOptions.setOutputRequestDetail(PortalConstants.FALSE);
		reportOptions.setOutputVulnDetail(PortalConstants.TRUE);
		reportOptions.setOutput_appendix(PortalConstants.TRUE);
		reportOptions.setXls_report_split_flg(PortalConstants.TRUE);
		
		if( null != targetInformationList){
			for (VexTargetInformation info : targetInformationList) {
				targetHost = new VexTargetHostItem();
				targetHost.setHost_info(info.getProtocol().equals(PortalConstants.INT_ONE) ? "http://" + info.getHost() + ":" + info.getPort()
				: "https://" + info.getHost() + ":" + info.getPort());
				targetHostList.add(targetHost);
			}
		} else {
			targetHost = new VexTargetHostItem();
			targetHostList.add(targetHost);
		}
		
		targetFunction = new VexTargetFuncItem();
		targetFunctionList.add(targetFunction);
		
		envRemark = new VexEnvRemarkItem();
		envRemarkList.add(envRemark);
		
		reportInfoSetting.setCustomer_name("TestCustomerName");
		reportInfoSetting.setTarget_system(scanName);
		reportInfoSetting.setAudit_month(audit_month);
		reportInfoSetting.setAudit_period(audit_period);
		reportInfoSetting.setOwn_company_name("TestOwnCompanyName");
		reportInfoSetting.setAudit_location("TestAuditLocation");
		reportInfoSetting.setAudit_place("TestAuditPlace");
		reportInfoSetting.setFrom_ip("TestFromIp");
		reportInfoSetting.setTarget_env(implEnv == PortalConstants.INT_ZERO ? PortalConstants.VEX_TARGET_ENVIRONEMENT_PRODUCTION : PortalConstants.VEX_TARGET_ENVIRONEMENT_VERIFICATION);
		reportInfoSetting.setSummary("TestSummary");
		reportInfoSetting.setReportOptions(reportOptions);
		reportInfoSetting.setTarget_host_list(targetHostList);
		reportInfoSetting.setTarget_function_list(targetFunctionList);
		reportInfoSetting.setEnv_remark_list(envRemarkList);
	}

	private static void deleteYmlFile(String crawling_filepath) {
		File autoCrawlingYamlFile = new File(crawling_filepath);
		autoCrawlingYamlFile.delete();
	}

	private static boolean saveVexProjectToDatabase(long lUserId, Map<String, Object> params,
			ArrayList<VexLoginSetting> loginSettingforDB, boolean bSuccess, long lScanId,
			long lProjectId, Calendar nowStart, Calendar nowEnd, List<VexTargetInformation> targetInformationList,
			String scanName, int implementationEnvironment, String webInspectionSignatureSetGroup, int serverFiles,
			int serverSettings, String serverFilesSignatureSetGroup, String serverSettingsSignatureSetGroup,
			int emailNotification, StringBuilder tempStartURL, StringBuilder tempAuthorizedPatrolURL, String newID, int setCrawlingOnly)
			throws PortalException, UBSPortalException {
		VexScanSetting scanSetting;
		Scan scan = null;
		User user;
		if (!newID.equals("-1")) {
			user = UserLocalServiceUtil.getUser(lUserId);
			scan = ScanLocalServiceUtil.createScanObj();
			scan.setCxAndroidScanId(newID);
			scan.setScanId(lScanId);
			scan.setProjectId(lProjectId);
			scan.setScanManager(user.getEmailAddress());
			scan.setFileName(scanName);
			scan.setStatus(ScanStatus.CRAWLING_WAITING.getInteger());
			scan.setRegistrationDate(new Date());
			scan.setModifiedDate(new Date());
			Scan addScan = ScanLocalServiceUtil.addScan(scan);
			
			if (CommonUtil.isObjectNull(addScan)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
			}
			
			scanSetting = VexScanSettingLocalServiceUtil.createVexScanSettingObj();
			scanSetting.setScanid(addScan.getScanId());
			scanSetting.setImplementationenvironment(implementationEnvironment);
			scanSetting.setSetemailnotification(emailNotification);
			scanSetting.setGetserverfiles(serverFiles);
			scanSetting.setGetserversettings(serverSettings);
			scanSetting.setSelectedwebsignature(webInspectionSignatureSetGroup);
			scanSetting.setSelectedserverfilessignature(serverFilesSignatureSetGroup);
			scanSetting.setSelectedserversettingssignature(serverSettingsSignatureSetGroup);
			scanSetting.setStarturl(tempStartURL.toString());
			scanSetting.setAuthorizedpatrolurl(tempAuthorizedPatrolURL.toString());
			scanSetting.setSetcrawlingonly(setCrawlingOnly);
			
			if (null != nowStart && null != nowEnd) {
				scanSetting.setStarttime(nowStart.getTime());
				scanSetting.setEndtime(nowEnd.getTime());
			}
			
			VexScanSettingLocalServiceUtil.addVexScanSetting(scanSetting);
			
			for (VexTargetInformation item : targetInformationList) {
				item.setScanid(addScan.getScanId());
				VexTargetInformationLocalServiceUtil.addVexTargetInformation(item);
			}
			
			if(!CommonUtil.isListNullOrEmpty(loginSettingforDB)){
				for (VexLoginSetting item : loginSettingforDB) {
					item.setScanid(addScan.getScanId());
					VexLoginSettingLocalServiceUtil.addVexLoginSetting(item);
				}
			}

			bSuccess = true;
		}
		return bSuccess;
	}
	
	private static boolean saveVexProjectToDatabaseDetailSetting(long lUserId, Map<String, Object> params, long lScanId, long lProjectId, String scanName, boolean bSuccess,
			List<VexTargetInformation> targetInformationList, StringBuilder tempStartURL, StringBuilder tempAuthorizedPatrolURL, StringBuilder tempUnauthorizedPatrolURL, StringBuilder tempLoginStatusDetection,
			StringBuilder tempParameterNameAsPassword, StringBuilder tempRequestHeader, StringBuilder tempSessionErrorDetection, StringBuilder tempScreenTransitionErrorDetection,
			StringBuilder tempExcludePathInfoRegex, StringBuilder tempExcludeParameterDeleteNameRegex, StringBuilder tempExcludeParameterDeleteParamRegex, StringBuilder tempExtractUrlRegex,
			StringBuilder tempNotParseExtension, StringBuilder tempNotParseFileNameRegex, int implementationEnvironment,
			int setautopostform, int setpostmethod, String maxnumdetectionlinkperpage, String maxnumdetectionlink, int setnotsendpassword, String numofthread, 
			String timeouttime, String detectionlinkdepthlimit, String waittime, String numofretryaterror, ArrayList<ArrayList<VexLoginSetting>> loginSettingList, String newID,
			Date startTime, Date endTime, int emailNotification, String webInspectionSignatureSetGroup, 
			int serverFiles, int serverSettings, String serverFilesSignatureSetGroup, String serverSettingsSignatureSetGroup, int setCrawlingOnly)
			throws PortalException, UBSPortalException {
		
		VexScanSetting scanSetting = null;
		Scan scan = null;
		User user = null;
		if (!newID.equals("-1")) {
			user = UserLocalServiceUtil.getUser(lUserId);
			scan = ScanLocalServiceUtil.createScanObj();
			scan.setCxAndroidScanId(newID);
			scan.setScanId(lScanId);
			scan.setProjectId(lProjectId);
			scan.setScanManager(user.getEmailAddress());
			scan.setFileName(scanName);
			scan.setStatus(ScanStatus.CRAWLING_WAITING.getInteger());
			scan.setRegistrationDate(new Date());
			scan.setModifiedDate(new Date());
			Scan addScan = ScanLocalServiceUtil.addScan(scan);
		
			if (CommonUtil.isObjectNull(addScan)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
				throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
			}
			
			scanSetting = VexScanSettingLocalServiceUtil.createVexScanSettingObj();
			scanSetting.setScanid(addScan.getScanId());
			scanSetting.setStarturl(tempStartURL.toString());
			scanSetting.setAuthorizedpatrolurl(tempAuthorizedPatrolURL.toString());
		
			if(tempUnauthorizedPatrolURL != null)
				scanSetting.setUnauthorizedpatrolurl(tempUnauthorizedPatrolURL.toString());
			else
				scanSetting.setUnauthorizedpatrolurl("");
		
			scanSetting.setStarttime(startTime);
			scanSetting.setEndtime(endTime);
			scanSetting.setSetemailnotification(emailNotification);
			scanSetting.setSelectedwebsignature(webInspectionSignatureSetGroup);
			scanSetting.setGetserverfiles(serverFiles);
			scanSetting.setGetserversettings(serverSettings);
			scanSetting.setSelectedserverfilessignature(serverFilesSignatureSetGroup);
			scanSetting.setSelectedserversettingssignature(serverSettingsSignatureSetGroup);
			scanSetting.setMaxnumdetectionlink(Integer.parseInt(maxnumdetectionlink));
			scanSetting.setMaxnumdetectionlinkperpage(Integer.parseInt(maxnumdetectionlinkperpage));
			scanSetting.setDetectionlinkdepthlimit(Integer.parseInt(detectionlinkdepthlimit));
			scanSetting.setWaittime(Integer.parseInt(waittime));
			scanSetting.setNumofthread(Integer.parseInt(numofthread));
			scanSetting.setTimeouttime(Integer.parseInt(timeouttime));
			scanSetting.setSetautopostform(setautopostform);
			scanSetting.setSetpostmethod(setpostmethod);
			scanSetting.setSetcrawlingonly(setCrawlingOnly);
			
			if(tempRequestHeader != null)
				scanSetting.setRequestheader(tempRequestHeader.toString());
			else
				scanSetting.setRequestheader("");
		
			if(tempLoginStatusDetection != null)
				scanSetting.setLoginstatusdetection(tempLoginStatusDetection.toString());
			else
				scanSetting.setLoginstatusdetection("");
		
			scanSetting.setSetnotsendpassword(setnotsendpassword);
		
			if(tempParameterNameAsPassword != null)
				scanSetting.setParamnamesaspassword(tempParameterNameAsPassword.toString());
			else
				scanSetting.setParamnamesaspassword("");
		
			scanSetting.setNumofretryaterror(Integer.parseInt(numofretryaterror));
		
			if(tempSessionErrorDetection != null)
				scanSetting.setSessionerrordetection(tempSessionErrorDetection.toString());
			else
				scanSetting.setSessionerrordetection("");
		
			if(tempScreenTransitionErrorDetection != null)
				scanSetting.setScreentransitionerrordetection(tempScreenTransitionErrorDetection.toString());
			else
				scanSetting.setScreentransitionerrordetection("");
			
			if(tempExcludePathInfoRegex != null)
				scanSetting.setExcludepathinforegex(tempExcludePathInfoRegex.toString());
			else
				scanSetting.setExcludepathinforegex("");
			
			if(tempExcludeParameterDeleteNameRegex != null)
				scanSetting.setExcludeparameterdeletenameregex(tempExcludeParameterDeleteNameRegex.toString());
			else
				scanSetting.setExcludeparameterdeletenameregex("");
			
			if(tempExcludeParameterDeleteParamRegex != null)
				scanSetting.setExcludeparameterdeleteparamregex(tempExcludeParameterDeleteParamRegex.toString());
			else
				scanSetting.setExcludeparameterdeleteparamregex("");
			
			if(tempExtractUrlRegex != null)
				scanSetting.setExtracturlregex(tempExtractUrlRegex.toString());
			else
				scanSetting.setExtracturlregex("");
			
			if(tempNotParseExtension != null)
				scanSetting.setNotparseextension(tempNotParseExtension.toString());
			else
				scanSetting.setNotparseextension("");
			
			if(tempNotParseFileNameRegex != null)
				scanSetting.setNotparsefilenameregex(tempNotParseFileNameRegex.toString());
			else
				scanSetting.setNotparsefilenameregex("");
			
		
			scanSetting.setImplementationenvironment(implementationEnvironment);
		
			VexScanSettingLocalServiceUtil.addVexScanSetting(scanSetting);
		
			for (VexTargetInformation item : targetInformationList) {
				item.setScanid(addScan.getScanId());
				VexTargetInformationLocalServiceUtil.addVexTargetInformation(item);
			}

			for (ArrayList<VexLoginSetting> item : loginSettingList) {
				for(VexLoginSetting subItem : item){
					subItem.setScanid(addScan.getScanId());
					VexLoginSettingLocalServiceUtil.addVexLoginSetting(subItem);
				}
			
			}
		
			bSuccess = true;
		}
		
		return bSuccess;
	}

	private static String createProjectAddSettingFile(long lScanId, long lProjectId, String loginURL,
			File fileDirectory, String scanName, List<VexTargetInformation> targetInformationList) throws IOException {
		StringWriter writer = new StringWriter();
		DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.DOUBLE_QUOTED);
        options.setExplicitStart(false);
		Yaml projectAddSettingYaml = new Yaml(options);
		
		VexProjectAddSettingItem projectAddSetting = new VexProjectAddSettingItem();
		setProjectAddYamlContents(scanName, targetInformationList, projectAddSetting);
		projectAddSettingYaml.dump(projectAddSetting, writer);
		
		SimpleDateFormat formatter = new SimpleDateFormat(PortalConstants.REPORT_DATE_FORMAT);
		String filename = formatter.format(new Date()) + "_" + lProjectId + "_" + lScanId + "_PROJECT_ADD_SETTING.yml";
		String filepath = fileDirectory.getAbsolutePath() + "//" + filename;
		FileOutputStream fos = new FileOutputStream(filepath);
		fos.write(writer.toString().getBytes());
		fos.close();
		return filepath;
	}
	
	private static void setProjectAddYamlContents(String scanName, List<VexTargetInformation> targetInformationList,  VexProjectAddSettingItem projectAddSetting) {
		List<VexTargetInfoSettingItem> targetInfoSettingList = new ArrayList<VexTargetInfoSettingItem>();
		VexTargetInfoSettingItem targetInfoSetting;
		
		for (VexTargetInformation targetInfo : targetInformationList) {
			targetInfoSetting = new VexTargetInfoSettingItem();
			targetInfoSetting.setAudit_flag(PortalConstants.TRUE);
			targetInfoSetting.setDelete_accept_encoding_header(PortalConstants.TRUE);
			targetInfoSetting.setHttp_version("1.0");
			targetInfoSetting.setHttps_enabled_protocol("Auto");
			targetInfoSetting.setIgnore_content_length(PortalConstants.TRUE);
			targetInfoSetting.setProtocol(targetInfo.getProtocol().equals("1")? "http": "https");
			targetInfoSetting.setTarget_host(targetInfo.getHost());
			targetInfoSetting.setTarget_port(String.valueOf(targetInfo.getPort()));
			targetInfoSetting.setUnzip_response(PortalConstants.TRUE);
			targetInfoSetting.setUse_keep_alive(PortalConstants.FALSE);
			targetInfoSettingList.add(targetInfoSetting);
		}
		
		projectAddSetting.setExtensionFilterRule("jpg,jpeg,gif,png,js,css,swf,ico,bmp");
		projectAddSetting.setFilenameFilterRule("doNotDisplayFileName");
		projectAddSetting.setOwner("-2");
		projectAddSetting.setProject_name(scanName);
		projectAddSetting.setUnLoggingHandler(PortalConstants.TRUE);
		projectAddSetting.setTargetInfoList(targetInfoSettingList);
	}
	
	private static String createProjectAddDetailSettingFile(long lScanId, long lProjectId, long lUserId, String scanName, File fileDirectory,
			List<VexTargetInformation> targetInformationList) throws IOException {
		StringWriter writer = new StringWriter();
		DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.DOUBLE_QUOTED);
		Yaml projectAddSettingYaml = new Yaml(options);
		
		List<VexTargetInfoSettingItem> targetInfoSettingList = new ArrayList<VexTargetInfoSettingItem>();
		VexTargetInfoSettingItem targetInfoSetting;
		for (VexTargetInformation targetInfo : targetInformationList) {
			targetInfoSetting = new VexTargetInfoSettingItem();
			targetInfoSetting.setBasic_auth_id(CommonUtil.isStringNullOrEmpty(targetInfo.getBasicauthid()) ? PortalConstants.STRING_EMPTY : targetInfo.getBasicauthid());
			targetInfoSetting.setAudit_flag(PortalConstants.TRUE);
			targetInfoSetting.setBasic_auth_pass(CommonUtil.isStringNullOrEmpty(targetInfo.getBasicauthpassword()) ? PortalConstants.STRING_EMPTY : targetInfo.getBasicauthpassword());
			targetInfoSetting.setClient_auth_file(CommonUtil.isStringNullOrEmpty(targetInfo.getCertificatefile()) ? PortalConstants.STRING_EMPTY : targetInfo.getCertificatefile());
			targetInfoSetting.setClient_auth_passphrase(CommonUtil.isStringNullOrEmpty(targetInfo.getCertificatefilepassword()) ? PortalConstants.STRING_EMPTY : targetInfo.getCertificatefilepassword());
			targetInfoSetting.setClient_auth_use(targetInfo.getUseclientcertificate() == PortalConstants.INT_ZERO ? PortalConstants.FALSE : PortalConstants.TRUE);
			targetInfoSetting.setDelete_accept_encoding_header(targetInfo.getUseacceptencodingheader() == PortalConstants.INT_ZERO ? PortalConstants.FALSE : PortalConstants.TRUE);
			targetInfoSetting.setDigest_password(CommonUtil.isStringNullOrEmpty(targetInfo.getDigestauthpassword()) ? PortalConstants.STRING_EMPTY : targetInfo.getDigestauthpassword());
			targetInfoSetting.setDigest_user_id(CommonUtil.isStringNullOrEmpty(targetInfo.getDigestauthid()) ? PortalConstants.STRING_EMPTY : targetInfo.getDigestauthid());
			targetInfoSetting.setExternal_proxy_host(CommonUtil.isStringNullOrEmpty(targetInfo.getExternalproxyhost()) ? PortalConstants.STRING_EMPTY : targetInfo.getExternalproxyhost());
			targetInfoSetting.setExternal_proxy_port(targetInfo.getExternalproxyport() == PortalConstants.INT_ZERO ? PortalConstants.STRING_EMPTY : String.valueOf(targetInfo.getExternalproxyport()));
			if(targetInfo.getHttpversion() == PortalConstants.INT_ONE){
				targetInfoSetting.setHttp_version("1.0");
			} else if (targetInfo.getHttpversion() == 2){
				targetInfoSetting.setHttp_version("1.1");
			}
			targetInfoSetting.setHttps_enabled_protocol(targetInfo.getHttpprotocol());
			targetInfoSetting.setIgnore_content_length(targetInfo.getSetresponsecontentlength() == PortalConstants.INT_ZERO ? PortalConstants.FALSE : PortalConstants.TRUE);
			targetInfoSetting.setNtlm_domain(CommonUtil.isStringNullOrEmpty(targetInfo.getNtlmauthdomain()) ? PortalConstants.STRING_EMPTY : targetInfo.getNtlmauthdomain());
			targetInfoSetting.setNtlm_hostname(CommonUtil.isStringNullOrEmpty(targetInfo.getNtlmauthhost()) ? PortalConstants.STRING_EMPTY : targetInfo.getNtlmauthhost());
			targetInfoSetting.setNtlm_password(CommonUtil.isStringNullOrEmpty(targetInfo.getNtlmauthpassword()) ? PortalConstants.STRING_EMPTY : targetInfo.getNtlmauthpassword());
			targetInfoSetting.setNtlm_user_id(CommonUtil.isStringNullOrEmpty(targetInfo.getNtlmauthid()) ? PortalConstants.STRING_EMPTY : targetInfo.getNtlmauthid());
			URL tempURL = new URL(targetInfo.getProtocol());
			targetInfoSetting.setProtocol(tempURL.getProtocol());
			targetInfoSetting.setProxy_auth_id(CommonUtil.isStringNullOrEmpty(targetInfo.getExternalproxyauthid()) ? PortalConstants.STRING_EMPTY : targetInfo.getExternalproxyauthid());
			targetInfoSetting.setProxy_auth_pass(CommonUtil.isStringNullOrEmpty(targetInfo.getExternalproxyauthpassword()) ? PortalConstants.STRING_EMPTY : targetInfo.getExternalproxyauthpassword());
			targetInfoSetting.setRejectRule(CommonUtil.isStringNullOrEmpty(targetInfo.getAccessexclusionpath()) ? PortalConstants.STRING_EMPTY : targetInfo.getAccessexclusionpath());
			targetInfoSetting.setTarget_host(targetInfo.getHost());
			targetInfoSetting.setTarget_port(String.valueOf(targetInfo.getPort()));
			targetInfoSetting.setUnzip_response(targetInfo.getUnzipresponse() == PortalConstants.INT_ZERO ? PortalConstants.FALSE : PortalConstants.TRUE);
			targetInfoSetting.setUse_keep_alive(targetInfo.getSetkeepaliveconnection() == PortalConstants.INT_ZERO ? PortalConstants.FALSE : PortalConstants.TRUE);
			targetInfoSettingList.add(targetInfoSetting);
		}
		
		VexProjectAddSettingItem projectAddSetting = new VexProjectAddSettingItem();
		projectAddSetting.setExtensionFilterRule("jpg,jpeg,gif,png,js,css,swf,ico,bmp");
		projectAddSetting.setFilenameFilterRule("doNotDisplayFileName");
		projectAddSetting.setOwner("-2");
		projectAddSetting.setProject_name(scanName);
		projectAddSetting.setUnLoggingHandler(PortalConstants.TRUE);
		projectAddSetting.setTargetInfoList(targetInfoSettingList);
		
		projectAddSettingYaml.dump(projectAddSetting, writer);
		
		SimpleDateFormat formatter = new SimpleDateFormat(PortalConstants.REPORT_DATE_FORMAT);
		String filename = formatter.format(new Date()) + "_" + lProjectId + "_" + lScanId + "_PROJECT_ADD_SETTING.yml";
		String filepath = fileDirectory.getAbsolutePath() + "//" + filename;
		FileOutputStream fos = new FileOutputStream(filepath);
		fos.write(writer.toString().getBytes());
		fos.close();
		return filepath;
	}
	
	private static String createProjectAddTempFile(File fileDirectory, String target_url) throws IOException {
		URL target_info = new URL(target_url);
		StringWriter writer = new StringWriter();
		DumperOptions options = new DumperOptions();
        options.setIndent(2);
        options.setPrettyFlow(true);
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setDefaultScalarStyle(DumperOptions.ScalarStyle.DOUBLE_QUOTED);
        options.setExplicitStart(false);
		Yaml projectAddSettingYaml = new Yaml(options);
		
		List<VexTargetInfoSettingItem> targetInfoSettingList = new ArrayList<VexTargetInfoSettingItem>();
		VexTargetInfoSettingItem targetInfoSetting;
		targetInfoSetting = new VexTargetInfoSettingItem();
		targetInfoSetting.setAudit_flag(PortalConstants.TRUE);
		targetInfoSetting.setDelete_accept_encoding_header(PortalConstants.TRUE);
		targetInfoSetting.setHttp_version("1.0");
		targetInfoSetting.setHttps_enabled_protocol("Auto");
		targetInfoSetting.setIgnore_content_length(PortalConstants.TRUE);
		targetInfoSetting.setProtocol(target_info.getProtocol());
		targetInfoSetting.setTarget_host(target_info.getHost());
		targetInfoSetting.setTarget_port(String.valueOf(target_info.getPort()));
		targetInfoSetting.setUnzip_response(PortalConstants.TRUE);
		targetInfoSetting.setUse_keep_alive(PortalConstants.FALSE);
		targetInfoSettingList.add(targetInfoSetting);

		VexProjectAddSettingItem projectAddSetting = new VexProjectAddSettingItem();
		projectAddSetting.setExtensionFilterRule("jpg,jpeg,gif,png,js,css,swf,ico,bmp");
		projectAddSetting.setFilenameFilterRule("doNotDisplayFileName");
		projectAddSetting.setOwner("-2");
		projectAddSetting.setProject_name("tempProjectForGetAutoCrawlLoginInfo");
		projectAddSetting.setUnLoggingHandler(PortalConstants.TRUE);
		projectAddSetting.setTargetInfoList(targetInfoSettingList);
		
		projectAddSettingYaml.dump(projectAddSetting, writer);
		
		SimpleDateFormat formatter = new SimpleDateFormat(PortalConstants.REPORT_DATE_FORMAT);
		String filename = formatter.format(new Date()) + "_TEMP_PROJECT_ADD_SETTING.yml";
		String filepath = fileDirectory.getAbsolutePath() + "\\" + filename;
		
		FileOutputStream fos = new FileOutputStream(filepath);
		fos.write(writer.toString().getBytes());
		fos.close(); 
		
		return filepath;
	}

	private static String createCrawlingSettingFile(long lScanId, long lProjectId, ArrayList<VexLoginSettingItem> loginSetting, String loginURL,
			File fileDirectory, StringBuilder base_start_url, StringBuilder base_crawl_allow_url_wildcard)
			throws MalformedURLException, FileNotFoundException, IOException {
		String path = "";
		String targetUrl = "";
		if (!CommonUtil.isStringNullOrEmpty(loginURL)) {
			URL url = new URL(loginURL);
			if (!CommonUtil.isStringNullOrEmpty(url.getProtocol())) {
				targetUrl += url.getProtocol() + "://";
			}
			path = url.getPath();
			targetUrl += url.getHost() + ":" + url.getPort();
		}
		
		StringBuilder sb = new StringBuilder();
		setAutoCrawlYamlContents(loginSetting, base_start_url, base_crawl_allow_url_wildcard, path, targetUrl, sb, loginURL);
		SimpleDateFormat formatter = new SimpleDateFormat(PortalConstants.REPORT_DATE_FORMAT);
		String filename = formatter.format(new Date()) + "_" + lProjectId + "_" + lScanId + "_CRAWLER_SETTING.yml";
		String filepath = fileDirectory.getAbsolutePath() + "//" + filename;
		FileOutputStream fos = new FileOutputStream(filepath);
		fos.write(sb.toString().getBytes());
		fos.close();
		return filepath;
	}
	
	private static String createCrawlingDetailSettingFile(long lScanId, long lProjectId, File fileDirectory, 
			StringBuilder base_start_url, StringBuilder base_crawl_allow_url_wildcard, StringBuilder base_crawl_exclude_url_wildcard, StringBuilder base_crawl_session_success_body_regex,
			StringBuilder base_password_parameter_name_regex, StringBuilder base_request_header, StringBuilder base_crawl_session_error_body_regex, StringBuilder base_crawl_transition_error_body_regex,
			StringBuilder base_dup_exclude_pathinfo_regex, StringBuilder base_dup_exclude_parameter_delete_param_regex, StringBuilder base_dup_exclude_parameter_delete_name_regex,
			StringBuilder base_extract_url_regex, StringBuilder base_crawl_not_parse_extension, StringBuilder base_crawl_not_parse_filename_regex,
			int setautopostform, int setpostmethod, String maxnumdetectionlinkperpage, String maxnumdetectionlink, int setnotsendpassword, String numofthread, 
			String timeouttime, String detectionlinkdepthlimit, String waittime, String numofretryaterror, ArrayList<ArrayList<VexLoginSettingItem>> loginSettingListfromSession, int loginSettingListSize) throws 
			MalformedURLException, FileNotFoundException, IOException {
		
		StringBuilder sb = new StringBuilder();
		
		if(setautopostform == 0)
			sb.append("base_allow_form_submit_flg: false");
		else
			sb.append("base_allow_form_submit_flg: true");
		sb.append(System.lineSeparator());
		if(setpostmethod == 0)
			sb.append("base_allow_post_flg: false");
		else
			sb.append("base_allow_post_flg: true");
		sb.append(System.lineSeparator());
		sb.append("base_crawl_allow_url_wildcard: |" + System.lineSeparator());
		sb.append(base_crawl_allow_url_wildcard);
		sb.append("base_crawl_error_retry: " + numofretryaterror);
		sb.append(System.lineSeparator());
		sb.append("base_crawl_exclude_url_wildcard: |" + System.lineSeparator());
		sb.append(base_crawl_exclude_url_wildcard);
		sb.append("base_crawl_not_parse_extension: |" + System.lineSeparator());
		sb.append(base_crawl_not_parse_extension);
		sb.append("base_crawl_not_parse_filename_regex: |" + System.lineSeparator());
		sb.append(base_crawl_not_parse_filename_regex);
		sb.append("base_crawl_session_error_body_regex: |" + System.lineSeparator());
		sb.append(base_crawl_session_error_body_regex);
		sb.append("base_crawl_session_success_body_regex: |" + System.lineSeparator());
		sb.append(base_crawl_session_success_body_regex);			
		sb.append("base_crawl_transition_error_body_regex: |" + System.lineSeparator());
		sb.append(base_crawl_transition_error_body_regex);
		sb.append("base_dup_exclude_parameter_delete_name_regex: |" + System.lineSeparator());
		sb.append(base_dup_exclude_parameter_delete_name_regex);
		sb.append("base_dup_exclude_parameter_delete_param_regex: |" + System.lineSeparator());
		sb.append(base_dup_exclude_parameter_delete_param_regex);
		sb.append("base_dup_exclude_pathinfo_regex: |" + System.lineSeparator());
		sb.append(base_dup_exclude_pathinfo_regex);
		sb.append("base_extract_url_regex: |" + System.lineSeparator());
		sb.append(base_extract_url_regex);
		sb.append("base_max_children: " + maxnumdetectionlinkperpage);
		sb.append(System.lineSeparator());
		sb.append("base_max_node: " + maxnumdetectionlink);
		sb.append(System.lineSeparator());	
		sb.append("base_password_parameter_name_regex: |" + System.lineSeparator());
		sb.append(base_password_parameter_name_regex);
		if(setnotsendpassword == 0)
			sb.append("base_password_send_block_flg: false");
		else
			sb.append("base_password_send_block_flg: true");
		sb.append(System.lineSeparator());
		sb.append("base_start_url: |" + System.lineSeparator());
		sb.append(base_start_url);
		sb.append("base_request_header: |" + System.lineSeparator());
		sb.append(base_request_header);
		setAutoCrawlYamlContentsNotInScreen(sb,PortalConstants.FALSE,PortalConstants.TRUE);
		sb.append("base_thread_num: " + numofthread);
		sb.append(System.lineSeparator());
		sb.append("base_timeout: " + timeouttime);
		sb.append(System.lineSeparator());
		sb.append("base_transition_max: " + detectionlinkdepthlimit);
		sb.append(System.lineSeparator());
		sb.append("base_wait: " + waittime);
		sb.append(System.lineSeparator());
		
		int i = 0;

		StringBuilder loginInfo = new StringBuilder();
		
		
		for (ArrayList<VexLoginSettingItem> item : loginSettingListfromSession) {	
			URL loginURL = new URL(item.get(i).getLoginurl());
			
			loginInfo.append(" path: '" + loginURL.getPath() + "'" + System.lineSeparator());
			loginInfo.append(" target_url: '" + loginURL.getProtocol() + "://" + loginURL.getAuthority() + "'" + System.lineSeparator());
			loginInfo.append(" crawlSettingLoginParamInfo:" + System.lineSeparator());
			loginInfo.append(" -" + System.lineSeparator());
			
			int j = 0;
			
			for(VexLoginSettingItem subItem : item){
				loginInfo.append("  name: " + PortalConstants.STRING_DOUBLE_QUOTE + subItem.getParamname() + PortalConstants.STRING_DOUBLE_QUOTE);
				loginInfo.append(System.lineSeparator());
				if(subItem.getChecked() == PortalConstants.INT_ONE){
					loginInfo.append("  value: " + PortalConstants.STRING_DOUBLE_QUOTE + subItem.getParamvalue() + PortalConstants.STRING_DOUBLE_QUOTE );
					loginInfo.append(System.lineSeparator());
					loginInfo.append("  checked: true" + System.lineSeparator());
				}else{
					loginInfo.append("  value: ''");
					loginInfo.append(System.lineSeparator());
					loginInfo.append("  checked: false" + System.lineSeparator());
				}
					
				if(++j != item.size())
					loginInfo.append(" -" + System.lineSeparator());
			}
			i++;
			
			if(i != loginSettingListSize)
				loginInfo.append("-" + System.lineSeparator());
			
		}
		
		if(loginSettingListSize != 0){
			sb.append("login_cnt: " + i + System.lineSeparator());
			sb.append("loginInfo: " + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(loginInfo);
		}else{
			sb.append("login_cnt: 0" + System.lineSeparator());
		}
		
		setAutoCrawlYamlContentsNotInScreen(sb,PortalConstants.TRUE,PortalConstants.FALSE);
	
		SimpleDateFormat formatter = new SimpleDateFormat(PortalConstants.REPORT_DATE_FORMAT);
		String filename = formatter.format(new Date()) + "_" + lProjectId + "_" + lScanId + "_CRAWLER_SETTING.yml";
		String filepath = fileDirectory.getAbsolutePath() + "//" + filename;
		
		FileOutputStream fos = new FileOutputStream(filepath);
		fos.write(sb.toString().getBytes());
		fos.close(); 
		
		return filepath;
	}

	private static void setAutoCrawlYamlContents(ArrayList<VexLoginSettingItem> loginSetting, StringBuilder base_start_url,
			StringBuilder base_crawl_allow_url_wildcard, String path, String targetUrl, StringBuilder sb, String loginURL) {
		sb.append("base_allow_form_submit_flg: true" + System.lineSeparator());
		sb.append("base_allow_post_flg: true" + System.lineSeparator());
		sb.append("base_crawl_allow_url_wildcard: |" + System.lineSeparator());
		sb.append(base_crawl_allow_url_wildcard);
		sb.append("base_crawl_error_retry: 1" + System.lineSeparator());
		sb.append("base_crawl_exclude_url_wildcard: |" + System.lineSeparator());
		sb.append(" *delete*" + System.lineSeparator());
		sb.append(" *exit*" + System.lineSeparator());
		sb.append(" *logoff*" + System.lineSeparator());
		sb.append(" *logout*" + System.lineSeparator());
		sb.append(" *remove*" + System.lineSeparator());
		sb.append(" *reset*" + System.lineSeparator());
		sb.append(" *signoff*" + System.lineSeparator());
		sb.append(" *signout*" + System.lineSeparator());
		setAutoCrawlYamlContentsNotInScreen(sb,PortalConstants.FALSE,PortalConstants.FALSE);
		sb.append("base_crawl_session_error_body_regex: |" + System.lineSeparator());
		sb.append(" ログアウ�?[^\\r\\n]{0,10}ました" + System.lineSeparator());
		sb.append(" タイ�?アウ�?[^\\r\\n]{0,10}ました" + System.lineSeparator());
		sb.append(" (S|s)ession[^\\r\\n]{0,10}(T|t)imeout" + System.lineSeparator());
		sb.append(" (S|s)ession[^\\r\\n]{0,10}ended" + System.lineSeparator());
		sb.append(" (L|l)ogged[^\\r\\n]{0,10}out" + System.lineSeparator());
		sb.append(" (L|l)ogged[^\\r\\n]{0,10}off" + System.lineSeparator());
		sb.append(" Lost[^\\r\\n]{0,10}Password" + System.lineSeparator());
		sb.append(" Forgot[^\\r\\n]{0,10}Password" + System.lineSeparator());
		sb.append("base_crawl_transition_error_body_regex: |" + System.lineSeparator());
		sb.append(" 遷移[^\\r\\n]{0,10}不正" + System.lineSeparator());
		sb.append(" 遷移[^\\r\\n]{0,10}エラー" + System.lineSeparator());
		sb.append(" 不正[^\\r\\n]{0,10}遷移" + System.lineSeparator());
		sb.append(" エラー[^\\r\\n]{0,10}遷移" + System.lineSeparator());
		sb.append(" (T|t)ransition[^\\r\\n]{0,10}(E|e)rror" + System.lineSeparator());
		sb.append(" (I|i)nvalid[^\\r\\n]{0,10}(T|t)ransition" + System.lineSeparator());
		sb.append(" (I|i)llegal[^\\r\\n]{0,10}(T|t)ransition" + System.lineSeparator());
		sb.append("base_max_children: 100" + System.lineSeparator());
		sb.append("base_max_node: 500" + System.lineSeparator());
		sb.append("base_password_parameter_name_regex: |" + System.lineSeparator());
		sb.append(" .*pass.*" + System.lineSeparator());
		sb.append(" .*pwd.*" + System.lineSeparator());
		sb.append("base_password_send_block_flg: true" + System.lineSeparator());
		sb.append("base_start_url: |" + System.lineSeparator());
		sb.append(base_start_url);
		
		sb.append("base_request_header: |" + System.lineSeparator());
		sb.append(" Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/vnd.ms-excel, application/msword, application/vnd.ms-powerpoint, */*" + System.lineSeparator());
		sb.append(" Accept-Language: ja" + System.lineSeparator());
		sb.append(" Accept-Encoding: gzip, deflate" + System.lineSeparator());
		sb.append(" User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)" + System.lineSeparator());
		sb.append(" Host: $host" + System.lineSeparator());
		sb.append(" Proxy-Connection: Keep-Alive" + System.lineSeparator());
		
		setAutoCrawlYamlContentsNotInScreen(sb,PortalConstants.FALSE,PortalConstants.TRUE);
		
		sb.append("base_thread_num: 1" + System.lineSeparator());
		sb.append("base_timeout: 60000" + System.lineSeparator());
		sb.append("base_transition_max: 20" + System.lineSeparator());
		sb.append("base_wait: 0" + System.lineSeparator());
				
		if(!CommonUtil.isListNullOrEmpty(loginSetting)){
			sb.append("login_cnt: 1" + System.lineSeparator());
			sb.append("loginInfo: " + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" path: '" + path + "'" + System.lineSeparator());
			sb.append(" target_url: '" + targetUrl + "'" + System.lineSeparator());
			sb.append(" crawlSettingLoginParamInfo:" + System.lineSeparator());
			sb.append(" -" + System.lineSeparator());
			
			int j=0;
			for(VexLoginSettingItem item: loginSetting){
				sb.append("  name: " + PortalConstants.STRING_DOUBLE_QUOTE + item.getParamname() + PortalConstants.STRING_DOUBLE_QUOTE + System.lineSeparator());
				if(CommonUtil.isStringNullOrEmpty(item.getParamvalue()))
					sb.append("  value: ''" + System.lineSeparator());
				else
					sb.append("  value: " + PortalConstants.STRING_DOUBLE_QUOTE + item.getParamvalue() + PortalConstants.STRING_DOUBLE_QUOTE + System.lineSeparator());
				
				sb.append("  checked: " + ((item.getChecked() == 1) ? true : false) + System.lineSeparator());
				if(++j != loginSetting.size())
					sb.append(" -" + System.lineSeparator());
				}
			sb.append(System.lineSeparator());
		}else{
			sb.append("login_cnt: 0" + System.lineSeparator());
		}	
		
		setAutoCrawlYamlContentsNotInScreen(sb,PortalConstants.TRUE,PortalConstants.FALSE);
	}
	
	private static void setAutoCrawlYamlContentsNotInScreen(StringBuilder sb, boolean isAtEndofLoginSetting, boolean nextToRequestHeader) {
		if(!isAtEndofLoginSetting){
			if(!nextToRequestHeader){
				sb.append("base_crawl_not_parse_extension: |" + System.lineSeparator());
				sb.append(" jpg" + System.lineSeparator());
				sb.append(" jpeg" + System.lineSeparator());
				sb.append(" gif" + System.lineSeparator());
				sb.append(" png" + System.lineSeparator());
				sb.append(" ico" + System.lineSeparator());
				sb.append(" bmp" + System.lineSeparator());
				sb.append(" zip" + System.lineSeparator());
				sb.append(" pdf" + System.lineSeparator());
				sb.append(" doc" + System.lineSeparator());
				sb.append(" xls" + System.lineSeparator());
				sb.append(" csv" + System.lineSeparator());
				sb.append(" ppt" + System.lineSeparator());
				sb.append("base_crawl_not_parse_filename_regex: |" + System.lineSeparator());
				sb.append(" jquery\\.min\\.js" + System.lineSeparator());
				sb.append(" jquery\\.js" + System.lineSeparator());
				sb.append(" jquery\\-[\\d.]+.min\\.js" + System.lineSeparator());
				sb.append(" jquery\\-[\\d.]+\\.js" + System.lineSeparator());
				sb.append(" dojo\\.js" + System.lineSeparator());
				sb.append(" dojo\\.xd\\.js" + System.lineSeparator());
				sb.append(" prototype\\.js" + System.lineSeparator());
				sb.append(" Async\\.js" + System.lineSeparator());
				sb.append(" MochiKit\\.js" + System.lineSeparator());
				sb.append(" mootools\\.js" + System.lineSeparator());
				sb.append(" mootools\\.core\\.js" + System.lineSeparator());
				sb.append(" mootools\\-[\\d.]+\\-core\\-yc\\.js" + System.lineSeparator());
				sb.append(" mootools\\-[\\d.]+\\-core\\-jm\\.js" + System.lineSeparator());
				sb.append(" mootools\\-[\\d.]+\\-core\\-nc\\.js" + System.lineSeparator());
				sb.append(" extjs\\.js" + System.lineSeparator());
				sb.append(" ext\\-all\\.js" + System.lineSeparator());
				sb.append(" ext\\-all\\-debug\\.js" + System.lineSeparator());
				sb.append(" ext\\-base\\.js" + System.lineSeparator());
				sb.append(" ext\\-core\\.js" + System.lineSeparator());
				sb.append(" ext\\-core\\-debug\\.js" + System.lineSeparator());
				sb.append(" scriptaculous\\.js" + System.lineSeparator());
				sb.append(" connection_core\\-min\\.js" + System.lineSeparator());
				sb.append(" yuiloader\\-dom\\-event\\.js" + System.lineSeparator());
				sb.append(" simpleajax\\.js" + System.lineSeparator());
			}
			if(nextToRequestHeader){
				sb.append("base_session_parameter: |" + System.lineSeparator());
				sb.append(" org.apache.struts.taglib.html.TOKEN" + System.lineSeparator());
				sb.append(" __VIEWSTATE" + System.lineSeparator());
				sb.append(" __EVENTVALIDATION" + System.lineSeparator());
				sb.append(" authenticity_token" + System.lineSeparator());
				sb.append(" CFTOKEN" + System.lineSeparator());
				sb.append(" CFID" + System.lineSeparator());
				sb.append(" BV_SessionID" + System.lineSeparator());
				sb.append(" BV_EngineID" + System.lineSeparator());
				sb.append(" PHPSESSID" + System.lineSeparator());
			}
		}else{
			sb.append("parameter_default_filename: test.txt" + System.lineSeparator());
			sb.append("parameter_default_filevalue: |" + System.lineSeparator());
			sb.append(" vex_test" + System.lineSeparator());
			sb.append("parameter_default_value: vex_test" + System.lineSeparator());
			sb.append("parameterReplaceInfo: " + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 1" + System.lineSeparator());
			sb.append(" parameter_url: \"*\""+ System.lineSeparator());
			sb.append(" parameter_name: \".*mail.*\"" + System.lineSeparator());
			sb.append(" parameter_value: vex_test" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 2" + System.lineSeparator());
			sb.append(" parameter_url: \"*\"" + System.lineSeparator());
			sb.append(" parameter_name: \".*year.*\"" + System.lineSeparator());
			sb.append(" parameter_value: 2000" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 3" + System.lineSeparator());
			sb.append(" parameter_url: \"*\"" + System.lineSeparator());
			sb.append(" parameter_name: \".*month.*\"" + System.lineSeparator());
			sb.append(" parameter_value: 1" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 4" + System.lineSeparator());
			sb.append(" parameter_url: \"*\"" + System.lineSeparator());
			sb.append(" parameter_name: \".*mail.*\"" + System.lineSeparator());
			sb.append(" parameter_value: 1" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 5" + System.lineSeparator());
			sb.append(" parameter_url: \"*\"" + System.lineSeparator());
			sb.append(" parameter_name: \".*(tel|fax).*\"" + System.lineSeparator());
			sb.append(" parameter_value: 0000000000" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 6" + System.lineSeparator());
			sb.append(" parameter_url: \"*\"" + System.lineSeparator());
			sb.append(" parameter_name: \".*zip.*\"" + System.lineSeparator());
			sb.append(" parameter_value: 1000001" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 7" + System.lineSeparator());
			sb.append(" parameter_url: \"*\"" + System.lineSeparator());
			sb.append(" parameter_name: \".*age.*\"" + System.lineSeparator());
			sb.append(" parameter_value: 30" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 8" + System.lineSeparator());
			sb.append(" parameter_url: \"*\"" + System.lineSeparator());
			sb.append(" parameter_name: \".*credit.*\"" + System.lineSeparator());
			sb.append(" parameter_value: 0000000000000000" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
			sb.append("-" + System.lineSeparator());
			sb.append(" parameter_order_no: 9" + System.lineSeparator());
			sb.append(" parameter_url: \"*\"" + System.lineSeparator());
			sb.append(" parameter_name: \".*date.*\"" + System.lineSeparator());
			sb.append(" parameter_value: 2000/1/1" + System.lineSeparator());
			sb.append(" parameter_cookie_flg: false" + System.lineSeparator());
			sb.append(" parameter_blank_only_flg: true" + System.lineSeparator());
		}
	}
	
	public static VexScanSettingItem setAutoCrawlDefaultValuesForDetailSetting(VexScanSettingItem setting, Project project) throws UBSPortalException {
		StringBuilder tempStartUrl = new StringBuilder();
		StringBuilder tempAuthorizedpatrolurl = new StringBuilder();
		String[] array = null;
		
		if (!CommonUtil.isStringNullOrEmpty(project.getTargetUrl())) {
			array = project.getTargetUrl().split(";");
			for (String url : array) {
				tempStartUrl.append(url + System.lineSeparator());
				tempAuthorizedpatrolurl.append(url + System.lineSeparator());
			}
		}
		
		setting.setStarturl(tempStartUrl.toString());
		setting.setAuthorizedpatrolurl(tempAuthorizedpatrolurl.toString());
		
		StringBuilder temp =  new StringBuilder();
		
		temp.append("*delete*" + System.lineSeparator());
		temp.append("*exit*" + System.lineSeparator());
		temp.append("*logoff*" + System.lineSeparator());
		temp.append("*logout*" + System.lineSeparator());
		temp.append("*remove*" + System.lineSeparator());
		temp.append("*reset*" + System.lineSeparator());
		temp.append("*signoff*" + System.lineSeparator());
		temp.append("*signout*" + System.lineSeparator());
		setting.setUnauthorizedpatrolurl(temp.toString());
		setting.setSetautopostform(PortalConstants.INT_ONE);
		setting.setSetpostmethod(PortalConstants.INT_ONE);
		setting.setSetnotsendpassword(PortalConstants.INT_ONE);
		
		temp =  new StringBuilder();
		temp.append("Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-shockwave-flash, application/vnd.ms-excel, application/msword, application/vnd.ms-powerpoint, */*" + System.lineSeparator());
		temp.append("Accept-Language: ja" + System.lineSeparator());
		temp.append("Accept-Encoding: gzip, deflate" + System.lineSeparator());
		temp.append("User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)" + System.lineSeparator());
		temp.append("Host: $host" + System.lineSeparator());
		temp.append("Proxy-Connection: Keep-Alive" + System.lineSeparator());
		setting.setRequestheader(temp.toString());
		
		temp =  new StringBuilder();
		temp.append(".*pass.*" + System.lineSeparator());
		temp.append(".*pwd.*" + System.lineSeparator());
		setting.setParamnamesaspassword(temp.toString());
		
		temp =  new StringBuilder();

		temp.append("ログアウ�?[^\\r\\n]{0,10}ました" + System.lineSeparator());
		temp.append("タイ�?アウ�?[^\\r\\n]{0,10}ました" + System.lineSeparator());
		temp.append("(S|s)ession[^\\r\\n]{0,10}(T|t)imeout" + System.lineSeparator());
		temp.append("(S|s)ession[^\\r\\n]{0,10}ended" + System.lineSeparator());
		temp.append("(L|l)ogged[^\\r\\n]{0,10}out" + System.lineSeparator());
		temp.append("(L|l)ogged[^\\r\\n]{0,10}off" + System.lineSeparator());
		temp.append("Lost[^\\r\\n]{0,10}Password" + System.lineSeparator());
		temp.append("Forgot[^\\r\\n]{0,10}Password" + System.lineSeparator());
		setting.setSessionerrordetection(temp.toString());
		
		temp =  new StringBuilder();
		temp.append("遷移[^\\r\\n]{0,10}不正" + System.lineSeparator());
		temp.append("遷移[^\\r\\n]{0,10}エラー" + System.lineSeparator());
		temp.append("不正[^\\r\\n]{0,10}遷移" + System.lineSeparator());
		temp.append("エラー[^\\r\\n]{0,10}遷移" + System.lineSeparator());
		temp.append("(T|t)ransition[^\\r\\n]{0,10}(E|e)rror" + System.lineSeparator());
		temp.append("(I|i)nvalid[^\\r\\n]{0,10}(T|t)ransition" + System.lineSeparator());
		temp.append("(I|i)llegal[^\\r\\n]{0,10}(T|t)ransition" + System.lineSeparator());
		setting.setScreentransitionerrordetection(temp.toString());
		
		temp =  new StringBuilder();
		temp.append("jpg" + System.lineSeparator());
		temp.append("jpeg" + System.lineSeparator());
		temp.append("gif" + System.lineSeparator());
		temp.append("png" + System.lineSeparator());
		temp.append("ico" + System.lineSeparator());
		temp.append("bmp" + System.lineSeparator());
		temp.append("zip" + System.lineSeparator());
		temp.append("pdf" + System.lineSeparator());
		temp.append("doc" + System.lineSeparator());
		temp.append("xls" + System.lineSeparator());
		temp.append("csv" + System.lineSeparator());
		temp.append("ppt" + System.lineSeparator());
		setting.setNotparseextension(temp.toString());
		
		temp =  new StringBuilder();
		temp.append("jquery\\.min\\.js" + System.lineSeparator());
		temp.append("jquery\\.js" + System.lineSeparator());
		temp.append("jquery\\-[\\d.]+.min\\.js" + System.lineSeparator());
		temp.append("jquery\\-[\\d.]+\\.js" + System.lineSeparator());
		temp.append("dojo\\.js" + System.lineSeparator());
		temp.append("dojo\\.xd\\.js" + System.lineSeparator());
		temp.append("prototype\\.js" + System.lineSeparator());
		temp.append("Async\\.js" + System.lineSeparator());
		temp.append("MochiKit\\.js" + System.lineSeparator());
		temp.append("mootools\\.js" + System.lineSeparator());
		temp.append("mootools\\.core\\.js" + System.lineSeparator());
		temp.append("mootools\\-[\\d.]+\\-core\\-yc\\.js" + System.lineSeparator());
		temp.append("mootools\\-[\\d.]+\\-core\\-jm\\.js" + System.lineSeparator());
		temp.append("mootools\\-[\\d.]+\\-core\\-nc\\.js" + System.lineSeparator());
		temp.append("extjs\\.js" + System.lineSeparator());
		temp.append("ext\\-all\\.js" + System.lineSeparator());
		temp.append("ext\\-all\\-debug\\.js" + System.lineSeparator());
		temp.append("ext\\-base\\.js" + System.lineSeparator());
		temp.append("ext\\-core\\.js" + System.lineSeparator());
		temp.append("ext\\-core\\-debug\\.js" + System.lineSeparator());
		temp.append("scriptaculous\\.js" + System.lineSeparator());
		temp.append("connection_core\\-min\\.js" + System.lineSeparator());
		temp.append("yuiloader\\-dom\\-event\\.js" + System.lineSeparator());
		temp.append("simpleajax\\.js" + System.lineSeparator());
		setting.setNotparsefilenameregex(temp.toString());
		
		return setting;
	}
	
	public static VexScanSettingItem copyAutoCrawlValuesForDetailSetting(VexScanSettingItem newScanSetting, VexScanSetting ScanSetting) throws UBSPortalException {
		StringBuilder temp = new StringBuilder();
		String[] array = null;
		newScanSetting = new VexScanSettingItem();
		
		if(ScanSetting != null){
			int maxnumdetectionlink = ScanSetting.getMaxnumdetectionlink();
			int maxnumdetectionlinkperpage = ScanSetting.getMaxnumdetectionlinkperpage();
			int detectionlinkdepthlimit = ScanSetting.getDetectionlinkdepthlimit();
			int waittime = ScanSetting.getWaittime();
			int numofthread = ScanSetting.getNumofthread();
			int timeouttime = ScanSetting.getTimeouttime();
			int setautopostform = ScanSetting.getSetautopostform();
			int setpostmethod = ScanSetting.getSetpostmethod();
			int setnotsendpassword = ScanSetting.getSetnotsendpassword();
			String numofretryaterror = String.valueOf(ScanSetting.getNumofretryaterror());
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getStarturl())) {
				temp = new StringBuilder();
				array = ScanSetting.getStarturl().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setStarturl(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getAuthorizedpatrolurl())) {
				temp = new StringBuilder();
				array = ScanSetting.getAuthorizedpatrolurl().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setAuthorizedpatrolurl(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getUnauthorizedpatrolurl())) {
				temp = new StringBuilder();
				array = ScanSetting.getUnauthorizedpatrolurl().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setUnauthorizedpatrolurl(temp.toString());
			}
			
			newScanSetting.setMaxnumdetectionlink(maxnumdetectionlink);
			newScanSetting.setMaxnumdetectionlinkperpage(maxnumdetectionlinkperpage);
			newScanSetting.setDetectionlinkdepthlimit(detectionlinkdepthlimit);
			newScanSetting.setWaittime(waittime);
			newScanSetting.setNumofthread(numofthread);
			newScanSetting.setTimeouttime(timeouttime);
			newScanSetting.setSetautopostform(setautopostform);
			newScanSetting.setSetpostmethod(setpostmethod);
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getRequestheader())) {
				temp = new StringBuilder();
				array = ScanSetting.getRequestheader().split(Pattern.quote("|"));
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setRequestheader(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getLoginstatusdetection())) {
				temp = new StringBuilder();
				array = ScanSetting.getLoginstatusdetection().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setLoginstatusdetection(temp.toString());
			}
			
			newScanSetting.setSetnotsendpassword(setnotsendpassword);
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getParamnamesaspassword())) {
				temp = new StringBuilder();
				array = ScanSetting.getParamnamesaspassword().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setParamnamesaspassword(temp.toString());
			}
			
			newScanSetting.setNumofretryaterror(numofretryaterror);
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getSessionerrordetection())) {
				temp = new StringBuilder();
				array = ScanSetting.getSessionerrordetection().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setSessionerrordetection(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getScreentransitionerrordetection())) {
				temp = new StringBuilder();
				array = ScanSetting.getScreentransitionerrordetection().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setScreentransitionerrordetection(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getExcludepathinforegex())) {
				temp = new StringBuilder();
				array = ScanSetting.getExcludepathinforegex().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setExcludepathinforegex(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getExcludeparameterdeleteparamregex())) {
				temp = new StringBuilder();
				array = ScanSetting.getExcludeparameterdeleteparamregex().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setExcludeparameterdeleteparamregex(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getExcludeparameterdeletenameregex())) {
				temp = new StringBuilder();
				array = ScanSetting.getExcludeparameterdeletenameregex().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setExcludeparameterdeletenameregex(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getExtracturlregex())) {
				temp = new StringBuilder();
				array = ScanSetting.getExtracturlregex().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setExtracturlregex(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getNotparseextension())) {
				temp = new StringBuilder();
				array = ScanSetting.getNotparseextension().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setNotparseextension(temp.toString());
			}
			
			if (!CommonUtil.isStringNullOrEmpty(ScanSetting.getNotparsefilenameregex())) {
				temp = new StringBuilder();
				array = ScanSetting.getNotparsefilenameregex().split(";");
				for (String line : array) {
					temp.append(line.trim() + System.lineSeparator());
				}
				newScanSetting.setNotparsefilenameregex(temp.toString());
			}
		}
		
		return newScanSetting;
	}
	
	
	public static ArrayList<ArrayList<VexLoginSettingItem>> copyLoginSettingsForDetailSetting(List<VexLoginSettingItem> loginSettingItems) throws UBSPortalException {
		ArrayList<ArrayList<VexLoginSettingItem>> loginSettingList = new ArrayList<ArrayList<VexLoginSettingItem>>();
		
		if(!CommonUtil.isListNullOrEmpty(loginSettingItems)) {
			ArrayList<VexLoginSettingItem> loginSettingListArray = new ArrayList<VexLoginSettingItem>(loginSettingItems);
			Map<String, ArrayList<VexLoginSettingItem>> data = new HashMap<String, ArrayList<VexLoginSettingItem>>();
			
			for (VexLoginSettingItem loginItem: loginSettingListArray) {
				ArrayList<VexLoginSettingItem> list = data.get(loginItem.getLoginurl());
			    if (list == null) {
			        list = new ArrayList<VexLoginSettingItem>();
			        data.put(loginItem.getLoginurl(), list);
			    }
			    list.add(loginItem);
			}

			for (Map.Entry<String,ArrayList<VexLoginSettingItem>> entry : data.entrySet()){
				ArrayList<VexLoginSettingItem> loginItemList = new ArrayList<VexLoginSettingItem>();
				for(VexLoginSettingItem loginItem: entry.getValue()){
					VexLoginSettingItem item = new VexLoginSettingItem();
					item.setLoginurl(loginItem.getLoginurl());
					item.setChecked(CommonUtil.isStringNullOrEmpty(loginItem.getParamvalue()) ? PortalConstants.INT_ZERO : PortalConstants.INT_ONE);
					item.setParamname(loginItem.getParamname());
					item.setParamvalue(loginItem.getParamvalue() == null ? PortalConstants.STRING_EMPTY : loginItem.getParamvalue());
					
					loginItemList.add(item);
				}
				loginSettingList.add(loginItemList);
			}
		}
		
		return loginSettingList;
	}
	
	public static boolean manageVexCrawlScan(ActionRequest actionRequest, int userAction, ApiInitInfo info) throws UBSPortalException, InterruptedException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		String strCxSessionId = null;
		Scan scan = null;
		Project project = null;
		boolean bGetProject = true;
		String strNewRunId = null;
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		String strScanIdInvalid = PortalConstants.STRING_EMPTY;
		String strProjectIdInvalid =PortalConstants.STRING_EMPTY;
		String strProjectDoesNotExist = PortalConstants.STRING_EMPTY;
		String strScanDoesNotExist = PortalConstants.STRING_EMPTY;
		String strIncorrectState =PortalConstants.STRING_EMPTY;
		String strWebSserviceFailed =PortalConstants.STRING_EMPTY;
		String strProjectTypeInvalid = PortalConstants.STRING_EMPTY;
		String strNoAccessRights =PortalConstants.STRING_EMPTY;
		String strPresetIdInvalid = PortalConstants.STRING_EMPTY;
		String strMethodName= PortalConstants.STRING_EMPTY;
		String strCannotExecuteScan= PortalConstants.STRING_EMPTY;
		String strFailMessage = PortalConstants.STRING_EMPTY;
		String strLogAction = PortalConstants.STRING_EMPTY;
		
		try {
			oUserId = session.getAttribute(PortalConstants.USER_ID);
			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}

				user = UserLocalServiceUtil.getUser(lUserId);
				
				if (CommonUtil.isObjectNull(user)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
				}
			}
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
		
			lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
			
			boolean bIsProjectUser = false;
			
			if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
				bIsProjectUser = true;
			} else {
				if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
					for (ProjectUsers pu : projectUsersList) {
						if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
							bIsProjectUser = true;
							break;
						}
					}
				}
			}
			
			String errorMsg[] = getScanMessages(userAction);

			strScanIdInvalid = errorMsg[0];
			strProjectIdInvalid = errorMsg[1];
			strProjectDoesNotExist = errorMsg[2];
			strScanDoesNotExist = errorMsg[3];
			strIncorrectState = errorMsg[4];
			strWebSserviceFailed = errorMsg[5];
			strProjectTypeInvalid = errorMsg[6];
			strNoAccessRights = errorMsg[7];
			strPresetIdInvalid = errorMsg[8];
			strMethodName = errorMsg[9];
			strCannotExecuteScan= errorMsg[10];
			strFailMessage = errorMsg[11];
			strLogAction =  errorMsg[12];
			
			if (!bIsProjectUser) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
				throw new UBSPortalException(strNoAccessRights, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}

			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			if (lScanId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(strScanIdInvalid, PortalMessages.SCAN_ID_INVALID);
			}

			scan = ScanLocalServiceUtil.retrieveScan(lScanId);
			
			if (CommonUtil.isObjectNull(scan)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new UBSPortalException(strScanDoesNotExist, PortalMessages.SCAN_DOES_NOT_EXIST);
			}
			
			if(CommonUtil.isStringNullOrEmpty(scan.getCxAndroidScanId())){
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID, PortalMessages.CX_ANDROID_SCAN_ID_INVALID);
			}

			scan.setRegistrationDate(new Date());
			lProjectId = scan.getProjectId();
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(strProjectIdInvalid, PortalMessages.PROJECT_ID_INVALID);
			}
			
			project = null;
			bGetProject = false;
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(strProjectDoesNotExist, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}

			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(strProjectTypeInvalid, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			boolean bActionValid = isValidScanAction(scan.getStatus(), userAction);
			if (!bActionValid) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_CANNOT_BE_ACTION, PortalConstants.ERROR_LOG_PARAM_SCAN, strLogAction));
				throw new UBSPortalException(strIncorrectState, strCannotExecuteScan);
			}
						
			String newScanName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_FILE_NAME).trim();
			List<String> scanResult = executeVexCrawlScan(info, scan, userAction, newScanName, actionRequest, user, lProjectId); 
			String strErrorMsg = scanResult.get(0).toString().trim();
			
			if (!CommonUtil.isObjectNull(scanResult) && CommonUtil.isStringNullOrEmpty(strErrorMsg)) {
				scan.setStatus(Integer.parseInt(scanResult.get(1)));
				scan.setModifiedDate(new Date());
				
				if (userAction == PortalConstants.USER_EVENT_DELETE_SCAN){
					long lCxAndroidScanId = Long.parseLong(scan.getCxAndroidScanId());
					if (lCxAndroidScanId > PortalConstants.LONG_ZERO) {
						ScanLocalServiceUtil.deleteScan(lScanId);
						bSuccess = true;
					}else{
						bSuccess = false;
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
						throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID, PortalMessages.CX_ANDROID_SCAN_ID_INVALID);
					}
				}else if(userAction == PortalConstants.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN){
					bSuccess = true;
				}else{
					bSuccess = ScanLocalServiceUtil.updateScan(userAction, scan);
					
					if(bSuccess && userAction == PortalConstants.USER_EVENT_PERFORM_SCAN)
					{
						// SWP-315 : update vex scan setting information for crawling only
						VexScanSetting scanSetting =  VexScanSettingLocalServiceUtil.getVexScanSetting(scan.getScanId());
						if(scanSetting != null)
						{
							scanSetting.setSetcrawlingonly(0);
							VexScanSettingLocalServiceUtil.updateVexScanSetting(scanSetting);
						}
					}
				}
			} 
			//Refer to SWP-94 where it state to force delete a scan in portal DB. In case, the user deleted the scan using Vex GUI.
			else if (userAction == PortalConstants.USER_EVENT_DELETE_SCAN){
				long lCxAndroidScanId = Long.parseLong(scan.getCxAndroidScanId());
				if (lCxAndroidScanId > PortalConstants.LONG_ZERO) {
					ScanLocalServiceUtil.deleteScan(lScanId);
					bSuccess = true;
				}else{
					bSuccess = false;
					params.put("vexError", strErrorMsg);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
					throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID, PortalMessages.CX_ANDROID_SCAN_ID_INVALID);
				} 
			}else{
				if(!CommonUtil.isStringNullOrEmpty(strErrorMsg) || strErrorMsg.length() >=0){
					scan.setStatus(Integer.parseInt(scanResult.get(1)));
					scan.setModifiedDate(new Date());
					ScanLocalServiceUtil.updateScan(userAction, scan);
				}
				params.put("vexError", strErrorMsg);
				throw new UBSPortalException(strFailMessage, strCannotExecuteScan);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(strScanDoesNotExist, strMethodName, params, nsse);
			throw new UBSPortalException(strScanDoesNotExist, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, strMethodName, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
			log.debug(strProjectDoesNotExist, strMethodName, params, nspe);
			throw new UBSPortalException(strProjectDoesNotExist, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, strMethodName, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, strMethodName, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), strMethodName, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, strMethodName, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lScanId", lScanId);
				params.put("strCxSessionId", strCxSessionId);
			}
			log.debug(ubspe.getErrorCode(), strMethodName, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
				return bSuccess;
			} 
		}
		
		return bSuccess;
	}
	private static String[] getScanMessages(int userAction) {
		String[] errorMsg = new String[13];
		
		switch (userAction) {	
		case PortalConstants.USER_EVENT_RECRAWL:
				errorMsg[0] = PortalErrors.RECRAWL_ID_INVALID;
				errorMsg[1] = PortalErrors.RECRAWL_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.RECRAWL_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.RECRAWL_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.RECRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.RECRAWL_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.RECRAWL_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.RECRAWL_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.RECRAWL_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_RECRAWL;
				errorMsg[10] = PortalMessages.CRAWL_CANNOT_BE_RECRAWLED;
				errorMsg[11] = PortalErrors.RECRAWL_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_RECRAWL;
			break;
			
		case PortalConstants.USER_EVENT_ABORT_CRAWL:
				errorMsg[0] = PortalErrors.ABORT_CRAWL_ID_INVALID;
				errorMsg[1] = PortalErrors.ABORT_CRAWL_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.ABORT_CRAWL_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.ABORT_CRAWL_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.ABORT_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.ABORT_CRAWL_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.ABORT_CRAWL_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.ABORT_CRAWL_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.ABORT_CRAWL_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_ABORT_CRAWL;
				errorMsg[10] = PortalMessages.CRAWL_CANNOT_BE_ABORTED;
				errorMsg[11]=PortalErrors.ABORT_CRAWL_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_ABORT_CRAWL;
			break;
			
		case PortalConstants.USER_EVENT_INTERRUPTED_CRAWL:
				errorMsg[0] = PortalErrors.INTERRUPTED_CRAWL_ID_INVALID;
				errorMsg[1] = PortalErrors.INTERRUPTED_CRAWL_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.INTERRUPTED_CRAWL_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.INTERRUPTED_CRAWL_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.INTERRUPTED_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.INTERRUPTED_CRAWL_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.INTERRUPTED_CRAWL_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.INTERRUPTED_CRAWL_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.INTERRUPTED_CRAWL_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_INTERRUPTED_CRAWL;
				errorMsg[10] = PortalMessages.CRAWL_CANNOT_BE_INTERRUPTEDED;
				errorMsg[11]=PortalErrors.INTERRUPTED_CRAWL_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_INTERRUPTED_CRAWL;
			break;
			
		case PortalConstants.USER_EVENT_CANCEL_CRAWL:
				errorMsg[0] = PortalErrors.CANCEL_CRAWL_ID_INVALID;
				errorMsg[1] = PortalErrors.CANCEL_CRAWL_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.CANCEL_CRAWL_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.CANCEL_CRAWL_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.CANCEL_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.CANCEL_CRAWL_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.CANCEL_CRAWL_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.CANCEL_CRAWL_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.CANCEL_CRAWL_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_CANCEL_CRAWL;
				errorMsg[10] = PortalMessages.CRAWL_CANNOT_BE_CANCELED;
				errorMsg[11]=PortalErrors.CANCEL_CRAWL_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_CANCEL_CRAWL;
			break;
			
		case PortalConstants.USER_EVENT_RESTART_CRAWL:
				errorMsg[0] = PortalErrors.RESTART_CRAWL_ID_INVALID;
				errorMsg[1] = PortalErrors.RESTART_CRAWL_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.RESTART_CRAWL_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.RESTART_CRAWL_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.RESTART_CRAWL_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.RESTART_CRAWL_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.RESTART_CRAWL_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.RESTART_CRAWL_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.RESTART_CRAWL_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_RESTART_CRAWL;
				errorMsg[10] = PortalMessages.CRAWL_CANNOT_BE_RESTARTED;
				errorMsg[11]=PortalErrors.RESTART_CRAWL_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_RESTART_CRAWL;
			break;
			
		case PortalConstants.USER_EVENT_INTERRUPTED_SCAN:
				errorMsg[0] = PortalErrors.INTERRUPTED_SCAN_ID_INVALID;
				errorMsg[1] = PortalErrors.INTERRUPTED_SCAN_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.INTERRUPTED_SCAN_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.INTERRUPTED_SCAN_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.INTERRUPTED_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.INTERRUPTED_SCAN_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.INTERRUPTED_SCAN_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.INTERRUPTED_SCAN_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.INTERRUPTED_SCAN_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_INTERRUPTED_SCAN;
				errorMsg[10] = PortalMessages.SCAN_CANNOT_BE_INTERRUPTED;
				errorMsg[11]=PortalErrors.INTERRUPTED_SCAN_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_INTERRUPTED_SCAN;
			break;
			
		case PortalConstants.USER_EVENT_CANCEL_SCAN:
				errorMsg[0] = PortalErrors.CANCEL_SCAN_ID_INVALID;
				errorMsg[1] = PortalErrors.CANCEL_SCAN_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.CANCEL_SCAN_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.CANCEL_SCAN_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.CANCEL_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.CANCEL_SCAN_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.CANCEL_SCAN_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.CANCEL_SCAN_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.CANCEL_SCAN_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_CANCEL_SCAN;
				errorMsg[10] = PortalMessages.SCAN_CANNOT_BE_CANCELED;
				errorMsg[11]=PortalErrors.CANCEL_SCAN_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_CANCEL_SCAN;
			break;
			
		case PortalConstants.USER_EVENT_STOP_SCAN:
				errorMsg[0] = PortalErrors.STOP_SCAN_SCAN_ID_INVALID;
				errorMsg[1] = PortalErrors.STOP_SCAN_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.STOP_SCAN_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.STOP_SCAN_SCAN_NOT_IN_WAITING_SCANNING_STATE;
				errorMsg[5] = PortalErrors.STOP_SCAN_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.STOP_SCAN_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.STOP_SCAN_USER_NO_RIGHTS;
				errorMsg[8] = "";
				errorMsg[9] = PortalConstants.METHOD_STOP_SCAN;
				errorMsg[10] = PortalMessages.SCAN_CANNOT_BE_STOPPED;
				errorMsg[11]=PortalErrors.STOP_SCAN_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_ACTION_STOP;
			break;
			
		case PortalConstants.USER_EVENT_RESTART_SCAN:
				errorMsg[0] = PortalErrors.RESTART_SCAN_ID_INVALID;
				errorMsg[1] = PortalErrors.RESTART_SCAN_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.RESTART_SCAN_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.RESTART_SCAN_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.RESTART_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.RESTART_SCAN_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.RESTART_SCAN_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.RESTART_SCAN_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.RESTART_SCAN_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_RESTART_SCAN;
				errorMsg[10] = PortalMessages.SCAN_CANNOT_BE_RESTARTED;
				errorMsg[11]=PortalErrors.RESTART_SCAN_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_RESTART_SCAN;
			break;
						
		case PortalConstants.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN:
				errorMsg[0] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_ID_INVALID;
				errorMsg[1] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_WEB_SERVICE_API_FAILED;
				errorMsg[6] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_COPY_CRAWL_RESULT_AND_SCAN;
				errorMsg[10] = PortalMessages.SCAN_CANNOT_BE_COPIED_AND_SCANNED;
				errorMsg[11]= PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_COPY_CRAWL_RESULT_AND_SCAN;
			break;
			
		case PortalConstants.USER_EVENT_REEXECUTE_SCAN:
				errorMsg[0] = PortalErrors.REEXECUTE_SCAN_SCAN_ID_INVALID;
				errorMsg[1] = PortalErrors.RESTART_SCAN_PROJECT_ID_INVALID;
				errorMsg[2] = PortalErrors.RESTART_SCAN_PROJECT_DOES_NOT_EXIST;
				errorMsg[3] = PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST;
				errorMsg[4] = PortalErrors.REEXECUTE_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
				errorMsg[5] = "";
				errorMsg[6] = PortalErrors.REEXECUTE_SCAN_PROJECT_TYPE_INVALID;
				errorMsg[7] = PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS;
				errorMsg[8] = PortalErrors.REEXECUTE_SCAN_PRESET_ID_INVALID;
				errorMsg[9] = PortalConstants.METHOD_REEXECUTE_SCAN;
				errorMsg[10] = PortalMessages.SCAN_CANNOT_BE_REEXECUTED;
				errorMsg[11] = PortalErrors.REEXECUTE_SCAN_FAILED;
				errorMsg[12] = PortalConstants.ERROR_LOG_ACTION_REEXECUTE;
			break;
			

		case PortalConstants.USER_EVENT_DELETE_SCAN:
			errorMsg[0] = PortalErrors.DELETE_SCAN_SCAN_ID_INVALID;
			errorMsg[1] = PortalErrors.DELETE_SCAN_PROJECT_ID_INVALID;
			errorMsg[2] = PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST;
			errorMsg[3] = PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST;
			errorMsg[4] = PortalErrors.DELETE_SCAN_FAILED;
			errorMsg[5] = PortalErrors.DELETE_SCAN_WEB_SERVICE_API_FAILED;
			errorMsg[6] = PortalErrors.DELETE_SCAN_PROJECT_TYPE_INVALID;
			errorMsg[7] = PortalErrors.DELETE_SCAN_USER_NO_RIGHTS;
			errorMsg[8] = "";
			errorMsg[9] = PortalConstants.METHOD_DELETE_SCAN;
			errorMsg[10] = PortalMessages.DELETE_SCAN_FAILED;
			errorMsg[11] = PortalErrors.DELETE_SCAN_FAILED;
			errorMsg[12] = PortalConstants.ERROR_LOG_DELETE_SCAN;
		break;
		
		case PortalConstants.USER_EVENT_PERFORM_SCAN:
			errorMsg[0] = PortalErrors.UPDATE_SCAN_SETTING_ID_INVALID;
			errorMsg[1] = PortalErrors.UPDATE_SCAN_SETTING_PROJECT_ID_INVALID;
			errorMsg[2] = PortalErrors.UPDATE_SCAN_SETTING_PROJECT_DOES_NOT_EXIST;
			errorMsg[3] = PortalErrors.UPDATE_SCAN_SETTING_SCAN_DOES_NOT_EXIST;
			errorMsg[4] = PortalErrors.UPDATE_SCAN_SETTING_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE;
			errorMsg[5] = PortalErrors.UPDATE_SCAN_SETTING_WEB_SERVICE_API_FAILED;
			errorMsg[6] = PortalErrors.UPDATE_SCAN_SETTING_PROJECT_TYPE_INVALID;
			errorMsg[7] = PortalErrors.UPDATE_SCAN_SETTING_PRESET_ID_INVALID;
			errorMsg[8] = PortalErrors.UPDATE_SCAN_SETTING_USER_NO_RIGHTS;
			errorMsg[9] = PortalConstants.METHOD_UPDATE_SCAN_PATROL_SETTINGS;
			errorMsg[10] = PortalMessages.SCAN_CANNOT_BE_UPDATED;
			errorMsg[11]= PortalErrors.UPDATE_SCAN_FAILED;
			errorMsg[12] = PortalConstants.ERROR_LOG_UPDATE_SCAN_SETTINGS;
		break;
			
			
		default:
			break;
	}
		return errorMsg;
	}

	private static boolean isValidScanAction(int status, int userAction) {
		boolean isValid =false;
		
		switch (userAction) {	
		case PortalConstants.USER_EVENT_RECRAWL:
			if(status == ScanStatus.CRAWLING_WAITING.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_ABORT_CRAWL:
			if(status == ScanStatus.CRAWLING_WAITING.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_INTERRUPTED_CRAWL:
			if(status == ScanStatus.CRAWLING.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_CANCEL_CRAWL:
			if(status == ScanStatus.CRAWLING.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_RESTART_CRAWL:
			if(status == ScanStatus.CRAWLING_INTERRUPTED.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_INTERRUPTED_SCAN:
			if(status ==  ScanStatus.SCANNING.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_CANCEL_SCAN:
			if(status ==  ScanStatus.SCANNING.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_STOP_SCAN:
			if(status == ScanStatus.SCAN_WAITING.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_RESTART_SCAN:
			if(status == ScanStatus.SCAN_INTERRUPTED.getInteger()){
				isValid = true;
			}
			break;
						
		case PortalConstants.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN:
			if(status == ScanStatus.SCAN_INTERRUPTED.getInteger() ||  status == ScanStatus.FAILURE.getInteger() ||
					status== ScanStatus.COMPLETE.getInteger() || status== ScanStatus.CRAWLING_COMPLETED.getInteger() ){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_REEXECUTE_SCAN:
			if(status == ScanStatus.SCAN_WAITING.getInteger()){
				isValid = true;
			}
			break;
			
		case PortalConstants.USER_EVENT_DELETE_SCAN:{
				isValid = true;
		}
		
		case PortalConstants.USER_EVENT_PERFORM_SCAN:{
			isValid = true;
		}
			break;
	}
		return isValid;
	}

	/**
	 * Set error message for vex scan and crawl actions.
	 * 
	 * @param userAction
	 * @param lUserId 
	 * @param actionRequest 
	 * @return
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("unused")
	private static List<String> executeVexCrawlScan(ApiInitInfo info, Scan scan, int userAction, String newScanName, ActionRequest actionRequest, User user, Long lProjectId) throws InterruptedException, UBSPortalException {
		
		List<String> objResult = new ArrayList<String>();
		String strMsg = "";
		int iNewScanStatus = 0;
		String strNewScanId="";
		
		String strRunId = scan.getCxAndroidScanId();
		Long lRunId = Long.valueOf(strRunId);
		
		Long lScanId = scan.getScanId();
		String strScanId = String.valueOf(lScanId);
		
		switch (userAction) {	
			case PortalConstants.USER_EVENT_RECRAWL:
				iNewScanStatus = ScanStatus.CRAWLING_WAITING.getInteger();
				break;
				
			case PortalConstants.USER_EVENT_ABORT_CRAWL:
				iNewScanStatus = ScanStatus.CRAWLING_FAILURE.getInteger();
				break;
				
			case PortalConstants.USER_EVENT_INTERRUPTED_CRAWL:
				strMsg = interruptedCrawl(info, strRunId);
				if(strMsg.isEmpty()){
					iNewScanStatus = ScanStatus.CRAWLING_INTERRUPTED.getInteger();
				}else{
					iNewScanStatus = getScanStatus(info, strRunId);
				}
				break;
				
			case PortalConstants.USER_EVENT_CANCEL_CRAWL:
				strMsg = cancelCrawl(info, strRunId);
				
				if(strMsg.isEmpty()){
					iNewScanStatus = ScanStatus.CRAWLING_FAILURE.getInteger();
				}else{
					iNewScanStatus = getScanStatus(info, strRunId);
				}
				
				break;
				
			case PortalConstants.USER_EVENT_RESTART_CRAWL:
				iNewScanStatus = ScanStatus.CRAWLING_WAITING.getInteger();
				break;
				
			case PortalConstants.USER_EVENT_INTERRUPTED_SCAN:
				strMsg = interruptedScan(info, strRunId);
				if(strMsg.isEmpty()){
					iNewScanStatus = ScanStatus.SCAN_INTERRUPTED.getInteger();
				}else{
					iNewScanStatus = getScanStatus(info, strRunId);
				}
				break;
				
			case PortalConstants.USER_EVENT_CANCEL_SCAN:
				strMsg = cancelScan(info, strRunId);
				
				if(strMsg.isEmpty()){
					MailUtil.sendEmail(PortalConstants.EMAIL_EVENT_SCAN_CANCELLED, lScanId, null);
					iNewScanStatus = ScanStatus.FAILURE.getInteger();
				}else{
					iNewScanStatus = getScanStatus(info, strRunId);
				}
			
				break;
				
			case PortalConstants.USER_EVENT_STOP_SCAN:
				iNewScanStatus = ScanStatus.FAILURE.getInteger();
				strMsg="";
				break;
				
			case PortalConstants.USER_EVENT_RESTART_SCAN:
				//restartScan(info, strRunId);
				iNewScanStatus =  ScanStatus.SCAN_WAITING.getInteger();
				break;
				
			case PortalConstants.USER_EVENT_PERFORM_SCAN:
				iNewScanStatus =  ScanStatus.SCAN_WAITING.getInteger();
				break;
							
			case PortalConstants.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN:
			try{
				Scan newScan = ScanLocalServiceUtil.createScanObj();
				long newPortalScanId = ScanLocalServiceUtil.createScanId();
				
				String uniqueScanName = lProjectId+ PortalConstants.STR_UNDERSCORE +newPortalScanId+ PortalConstants.STR_UNDERSCORE +newScanName;
								
				//create yml files 
				strNewScanId = copyScanCreateYMLfiles(newPortalScanId, lProjectId, lScanId, uniqueScanName);
				
				if(CommonUtil.isStringNullOrEmpty(strNewScanId)== false && Integer.parseInt(strNewScanId) > 0){
					try {
						Scan copiedscan = VexScanMgmtController.getScan(lScanId);
						VexScanSetting scanSetting = VexScanMgmtController.getVexScanSetting(lScanId);
						List<VexTargetInformationItem> targetInformationList = VexScanMgmtController.getVexTargetInformationList(lScanId);
						List<VexLoginSettingItem> loginSettingList = VexScanMgmtController.getVexLoginSettingList(lScanId);
												
						try{
							newScan = copiedscan;
							newScan.setScanId(newPortalScanId);
							newScan.setFileName(newScanName);
							newScan.setCxAndroidScanId(strNewScanId);
							newScan.setScanManager(user.getEmailAddress());
							newScan.setStatus(ScanStatus.SCAN_WAITING.getInteger());
							newScan.setRegistrationDate(new Date());
							newScan.setModifiedDate(new Date());
							Scan addScan = ScanLocalServiceUtil.addScan(newScan);
							
							if (CommonUtil.isObjectNull(addScan)) {
								deleteScan(info, strNewScanId, ScanStatus.SCAN_WAITING.getInteger());
								throw new UBSPortalException(PortalErrors.COPY_CRAWL_RESULT_AND_SCAN_FAILED, PortalMessages.COPY_CRAWL_RESULT_AND_SCAN_FAILED);
							}
							
							if (!CommonUtil.isObjectNull(scanSetting)){
								scanSetting.setScanid(newPortalScanId);
								scanSetting.setSetcrawlingonly(0);
								VexScanSettingLocalServiceUtil.addVexScanSetting(scanSetting);
							}
							
							if (!CommonUtil.isObjectNull(targetInformationList) && targetInformationList.size() > 0) {

								for (VexTargetInformationItem vexTargetInformation: targetInformationList){
									VexTargetInformation item = VexTargetInformationLocalServiceUtil.createVexTargetInformationObj();
									item.setScanid(newPortalScanId);
									item.setProtocol(vexTargetInformation.getProtocol());
									item.setHost(vexTargetInformation.getHost());
									item.setPort(Integer.parseInt(vexTargetInformation.getPort()));
									VexTargetInformationLocalServiceUtil.addVexTargetInformation(item);
								}
							}
							
							if(!CommonUtil.isObjectNull(loginSettingList) && loginSettingList.size() > 0){
								for (VexLoginSettingItem vexLoginSetting: loginSettingList){									
									VexLoginSetting loginSetting = VexLoginSettingLocalServiceUtil.createVexLoginSettingObj();
									loginSetting.setScanid(newPortalScanId);
									loginSetting.setLoginurl(vexLoginSetting.getLoginurl());
									loginSetting.setParamname(vexLoginSetting.getParamname());
									loginSetting.setParamvalue(vexLoginSetting.getParamvalue().replaceAll(PortalConstants.STRING_DOUBLE_QUOTE, PortalConstants.STRING_EMPTY));
									VexLoginSettingLocalServiceUtil.addVexLoginSetting(loginSetting);
								}
							}
						}catch (Exception e){
							e.printStackTrace();
						}
						
						strRunId = strNewScanId;
					} catch (UBSPortalException e) {
						if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
									e.getErrorMessage());

							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, e.getErrorCode());
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, e.getErrorMessage());
							}
						}
					}
				}else{
					deleteScan(info, strNewScanId, ScanStatus.SCAN_WAITING.getInteger());
					strMsg = PortalMessages.CRAWL_CANNOT_BE_COPIED;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
				break;
				
			case PortalConstants.USER_EVENT_REEXECUTE_SCAN:
				//reexecuteScan(info, strScanId, lRunId);
				iNewScanStatus = ScanStatus.SCAN_WAITING.getInteger();
				break;
				
			case PortalConstants.USER_EVENT_DELETE_SCAN:
				strMsg = deleteScan(info, strRunId, scan.getStatus());
				iNewScanStatus = scan.getStatus();
				break;
		}
		
		objResult.add(0, strMsg);
		objResult.add(1, iNewScanStatus+"");
		objResult.add(2, strNewScanId);
		
		return objResult;
	}
	
	public static String copyScanCreateYMLfiles(long newPortalScanId, long lProjectId, long lScanId , String uniqueScanName) throws UBSPortalException
	{
		Map<String, Object> params 								= new HashMap<String, Object>();
		String newID											= PortalConstants.STRING_EMPTY;
		
		try {
			int scanType											= 0;
			int implementationEnvironment							= 0;
			String loginURL 										= PortalConstants.STRING_EMPTY; 
			String strRunId											= PortalConstants.STRING_EMPTY;
			StringBuilder base_start_url							= new StringBuilder();
			StringBuilder base_crawl_allow_url_wildcard 			= new StringBuilder();
			Scan copiedscan 										= VexScanMgmtController.getScan(lScanId);
			VexScanSetting scanSetting 								= VexScanMgmtController.getVexScanSetting(lScanId);
			List<VexTargetInformationItem> targetInformationList 	= VexScanMgmtController.getVexTargetInformationList(lScanId);
			List<VexLoginSettingItem> loginSettingList 				= VexScanMgmtController.getVexLoginSettingList(lScanId);
			ArrayList<VexLoginSettingItem> arrLoginSettingList 	 	= new ArrayList<>();
			List<VexTargetInformation> arrTargetInfoList 			= new ArrayList<>();
		
			ArrayList <ArrayList<VexLoginSettingItem>> loginSettingListfromSession = null;
			
			//identify scan type 
			if(scanSetting != null)
			{
				scanType = (scanSetting.getRequestheader().equals("") == false) ?  1 : 0; // if 1 : advance
			}
			
			if(loginSettingList != null)
			{
				arrLoginSettingList 		= new ArrayList<>(loginSettingList);
				loginSettingListfromSession = new ArrayList <ArrayList<VexLoginSettingItem>>();
				
				loginSettingListfromSession.add(arrLoginSettingList);
			}
			
			//Create a file directory
			File fileDirectory = FileProcessUtil.createCrawlSettingYAMLPath(lProjectId, newPortalScanId);
			
			if (fileDirectory.isDirectory()) {
				
				// get Login URL
				if(loginSettingList!= null && loginSettingList.size() > 0)
				{
					VexLoginSettingItem loginSettingItem = loginSettingList.get(0);
					loginURL = loginSettingItem.getLoginurl();
				}
				
				// get Target Information 
				String strStartUrl = scanSetting.getStarturl().replace(";", "");
				String strAuthUrl = scanSetting.getAuthorizedpatrolurl().replace(";", "");
				base_start_url.append(PortalConstants.STR_SPACE + strStartUrl.trim() + System.lineSeparator());
				base_crawl_allow_url_wildcard.append(PortalConstants.STR_SPACE + strAuthUrl.trim() + System.lineSeparator());
				
				if(targetInformationList != null)
				{
					targetInformationList.forEach(item -> {    
						VexTargetInformation vexTargetInfo = VexTargetInformationLocalServiceUtil.createVexTargetInformationObj();
						vexTargetInfo.setScanid(newPortalScanId);
						vexTargetInfo.setProtocol(item.getProtocol());
						vexTargetInfo.setHost(item.getHost());
						vexTargetInfo.setPort(Integer.parseInt(item.getPort()));
						
						arrTargetInfoList.add(vexTargetInfo);
					});
				}
				else
				{
					scanType = 1; // if 1 : import
				}
				
				if(arrLoginSettingList != null)
				{
					arrLoginSettingList.forEach(item -> {  
							if(item.getParamvalue().equals("") == false){
								item.setChecked(1);
							}
						});
				}
				
				implementationEnvironment = scanSetting.getImplementationenvironment();
				
				// Set Login Credentials
				ApiInitInfo oInitInfo = setLoginCredentials();
				InterfaceProjectAdd projectAdd = VexInstanceFactory.getProjectAdd(oInitInfo);
				
				String crawling_filepath = null;
				String project_add_filepath = null;
				if(scanType == 1)
				{
					strRunId = copiedscan.getCxAndroidScanId();
					
					ProjectCopy copy = copyCrawlAndScan(oInitInfo, strRunId, uniqueScanName);
					newID = copy.getNewProjectID();
					String strMsg = copy.getErrorMessage();
					
				} else {
					//create crawling_yml for simple setting
					crawling_filepath = createCrawlingSettingFile(newPortalScanId, lProjectId, arrLoginSettingList, loginURL, fileDirectory, base_start_url, base_crawl_allow_url_wildcard);
				
					//create project_add_yml
					project_add_filepath = createProjectAddSettingFile(newPortalScanId, lProjectId, loginURL, fileDirectory, uniqueScanName, arrTargetInfoList);
					
					try {
						newID = projectAdd.executeEX(project_add_filepath);
					} catch (Throwable e) {
						params.put("YML File:", project_add_filepath);
						params.put("error", e.getCause());
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED, e);
					}
				}
								
				//create report_information_yml
				String report_information_path = createReportInformationFile(newPortalScanId, lProjectId, loginURL, fileDirectory, uniqueScanName, arrTargetInfoList, implementationEnvironment);
				
				if (Integer.parseInt(newID) > 0) {
					if(scanType == 0)
					{
						deleteYmlFile(project_add_filepath);					
						InterfaceAutoCrawlSetting test = VexInstanceFactory.getAutoCrawlSetting(oInitInfo);
						boolean res = false;
						
						try{
	 						res = test.execute(newID,crawling_filepath);
							if(CommonUtil.isObjectNull(test)){
								params.put(PortalConstants.ERROR_LOG_MESSAGE,
										String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
												PortalConstants.ERROR_LOG_AUTO_CRAWL));
								throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
							}
							if(!res || !CommonUtil.isStringNullOrEmpty(test.getErrorMessage())){
								params.put("Vex Error Message", test.getErrorMessage());
								params.put(PortalConstants.ERROR_LOG_MESSAGE,
										String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID,
												PortalConstants.ERROR_LOG_AUTO_CRAWL));
								throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
							}
						}catch(Exception e){
							params.put("Vex Scan Id", newID);
							crawling_filepath = crawling_filepath.replace("//", "/");
							params.put("crawling filepath", crawling_filepath);
							params.put("error", e.getCause());
							throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
						}catch (Throwable t){
							params.put("Vex Scan Id", newID);
							crawling_filepath = crawling_filepath.replace("//", "/");
							params.put("crawling filepath", crawling_filepath);
							params.put("error", t.getCause());
							throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
						} finally {
							if (res) {
								deleteYmlFile(crawling_filepath);
							}
						}
					}
					
					boolean infoFile = false;
					InterfaceEditReportDefinition reportDefinition = VexInstanceFactory.getEditReportDefinition(oInitInfo);
					
					try {
						infoFile = reportDefinition.execute(newID, report_information_path);
					} catch(Exception e){
						params.put("Vex Scan Id", newID);
						report_information_path = report_information_path.replace("//", "/");
						params.put("report definition filepath", report_information_path);
						params.put("error", e.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					}catch (Throwable t){
						params.put("Vex Scan Id", newID);
						report_information_path = report_information_path.replace("//", "/");
						params.put("report definition filepath", report_information_path);
						params.put("error", t.getCause());
						throw new UBSPortalException(PortalErrors.AUTO_CRAWL_FAILED, PortalMessages.AUTO_CRAWL_FAILED);
					} finally {
						if (infoFile) {
							deleteYmlFile(report_information_path);
						}
					}
					
				}else{
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
					throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
				}
			}
		} catch (FileNotFoundException fnfe) {
			log.debug(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, fnfe);
			throw new UBSPortalException(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalMessages.FILE_NOT_FOUND_EXCEPTION, fnfe);
		} catch (IOException ie) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ie);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.IO_EXCEPTION, ie);
		} catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return newID;
	}

	public static Scan getScan (long lScanId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Scan scan = null;
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			scan = ScanLocalServiceUtil.getScan(lScanId);
		} catch (PortalException pe) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return scan;
	}

	public static VexScanSetting getVexScanSetting (long lScanId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		VexScanSetting scanSetting = null;
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			scanSetting = VexScanSettingLocalServiceUtil.getVexScanSetting(lScanId);
		} catch (PortalException pe) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return scanSetting;
	}
	
	public static List<VexTargetInformationItem> getVexTargetInformationList (long lScanId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<VexTargetInformationItem> targetInformationList = null;
		List<Object> resultList = null;
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			resultList = VexTargetInformationLocalServiceUtil.getTargetInformations(lScanId);
			if (!CommonUtil.isListNullOrEmpty(resultList)) {
				targetInformationList = new ArrayList<VexTargetInformationItem>();
				for (Object item : resultList) {
					VexTargetInformationItem targetInformation = (VexTargetInformationItem) item;
					targetInformationList.add(targetInformation);
				}
			}
		} catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return targetInformationList;
	}
	
	public static List<VexLoginSettingItem> getVexLoginSettingList (long lScanId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<VexLoginSettingItem> loginSettingList = null;
		List<Object> resultList = null;
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			resultList = VexLoginSettingLocalServiceUtil.getLoginSettings(lScanId);
			if (!CommonUtil.isListNullOrEmpty(resultList)) {
				loginSettingList = new ArrayList<VexLoginSettingItem>();
				for (Object item : resultList) {
					VexLoginSettingItem loginInformation = (VexLoginSettingItem) item;
					loginSettingList.add(loginInformation);
				}
			}
		} catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return loginSettingList;
	}
	
	public static List<VexTargetInformationItem> addVexScanTargetInformationForValidation(ActionRequest actionRequest) {
		List<VexTargetInformationItem> targetInformationList = null;
		
		try {
			int targetInfoListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFORMATION_LIST_SIZE);
			
			targetInformationList = new ArrayList<>();
			
			for (int i = 0; i < targetInfoListSize; i++) {
					String protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROTOCOL_GROUP + i);
					String host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HOST + i).trim();
					String port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PORT + i).trim();
					
					Object httpversion = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_HTTP_VERSION + i);
					Object setkeepaliveconnection =  ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_SET_KEEP_ALIVE_CONNECTION + i);
					Object setresponsecontentlength =  ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_SET_RESPONSE_CONTENT_LENGTH + i);
					Object useacceptencodingheader = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_USE_ACCEPTED_ENCODING_HEADER + i);
					Object unzipresponse = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_UNZIP_RESPONSE + i);
					
					String httpprotocol =  ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_HTTP_PROTOCOL + i).trim();
					
					String externalproxyhost =  ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_EXTERNAL_PROXY_HOST + i).trim();
					String externalproxyport = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_EXTERNAL_PROXY_PORT + i);
					String externalproxyauthid =  ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_EXTERNAL_PROXY_AUTH_ID + i).trim();
					String externalproxyauthpassword =  ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_EXTERNAL_PROXY_AUTH_PASSWORD + i).trim();
					
					Object useclientcertificate = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFO_USE_CLIENT_CERTIFICATE + i);
					String certificatefilepassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_CERTIFICATE_FILE_PASSWORD + i).trim();
					
					UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
					String fileName=uploadRequest.getFileName(PortalConstants.PARAM_TARGET_INFO_CERTIFICATE_FILE + i);
								
					String ntlmauthid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_NTLM_AUTH_ID + i).trim();
					String ntlmauthpassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_NTLM_AUTH_PASSWORD + i).trim();
					String ntlmauthdomain = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_NTLM_AUTH_DOMAIN + i).trim();
					String ntlmauthhost = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_NTLM_AUTH_HOST + i).trim();
					
					String digestauthid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_DIGEST_AUTH_ID + i).trim();
					String digestauthpassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_DIGEST_AUTH_PASSWORD + i).trim();
					
					String basicauthid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_BASIC_AUTH_ID + i).trim();
					String basicauthpassword = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_BASIC_AUTH_PASSWORD + i).trim();
					
					String accessexclusionpath = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFO_ACCESS_EXCLUSION_PATH + i).trim();
					
					VexTargetInformationItem item = new VexTargetInformationItem();
					item.setScanid(i);
					item.setProtocol(protocol);
					item.setHost(host);
					
					if(!CommonUtil.isObjectNull(protocol)){
						item.setProtocol(protocol);
					}
					
					if(!CommonUtil.isObjectNull(host)){
						item.setHost(host);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(port)){
						item.setPort(port);
					}
					
					if(!CommonUtil.isObjectNull(httpversion)){
						item.setHttpversion((int) httpversion);
					}
					
					if(!CommonUtil.isObjectNull(setkeepaliveconnection)){
						item.setSetkeepaliveconnection((int)setkeepaliveconnection);
					}
					
					if(!CommonUtil.isObjectNull(setresponsecontentlength)){
						item.setSetresponsecontentlength((int)setresponsecontentlength);
					}
					
					if(!CommonUtil.isObjectNull(useacceptencodingheader)){
						item.setUseacceptencodingheader((int)useacceptencodingheader);
					}
					
					if(!CommonUtil.isObjectNull(unzipresponse)){
						item.setUnzipresponse((int)unzipresponse);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(httpprotocol)){
						item.setHttpprotocol(httpprotocol);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(externalproxyhost)){
						item.setExternalproxyhost(externalproxyhost);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(externalproxyport)){
						item.setExternalproxyport(Integer.parseInt(externalproxyport));
					}
					
					if(!CommonUtil.isStringNullOrEmpty(externalproxyauthid)){
						item.setExternalproxyauthid(externalproxyauthid);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(externalproxyauthpassword)){
						item.setExternalproxyauthpassword(externalproxyauthpassword);
					}
					
					File fileCertificate = uploadRequest.getFile(PortalConstants.PARAM_TARGET_INFO_CERTIFICATE_FILE + i);
					

					if(!CommonUtil.isObjectNull(fileCertificate)){
						item.setCertificatefile(fileCertificate.getAbsolutePath().toString());
					}
								
					if(!CommonUtil.isObjectNull(useclientcertificate)){
						item.setUseclientcertificate((int)useclientcertificate);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(certificatefilepassword)){
						item.setCertificatefilepassword(certificatefilepassword);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(ntlmauthid)){
						item.setNtlmauthid(ntlmauthid);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(ntlmauthpassword)){
						item.setNtlmauthpassword(ntlmauthpassword);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(ntlmauthdomain)){
						item.setNtlmauthdomain(ntlmauthdomain);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(ntlmauthhost)){
						item.setNtlmauthhost(ntlmauthhost);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(digestauthid)){
						item.setDigestauthid(digestauthid);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(digestauthpassword)){
						item.setDigestauthpassword(digestauthpassword);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(basicauthid)){
						item.setBasicauthid(basicauthid);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(basicauthpassword)){
						item.setBasicauthpassword(basicauthpassword);
					}
					
					if(!CommonUtil.isStringNullOrEmpty(accessexclusionpath)){
						item.setAccessexclusionpath(accessexclusionpath);
					}else{
						item.setAccessexclusionpath(PortalConstants.STRING_EMPTY);
					}
					
					targetInformationList.add(item);
				}
			}catch(Exception e){
			
			}
		
		return targetInformationList;
		}
	
	@SuppressWarnings("finally")
	public static List<ApiSignatureSetInfo> getSignatureList(ApiAuditType type, ApiInitInfo info) {
		Map<String, Object> params = new HashMap<String, Object>();
		GetSignatureSetList getSignatureSetList = new GetSignatureSetList(info);
		List<ApiSignatureSetInfo> signatureSetInfo = new ArrayList<ApiSignatureSetInfo>();
		signatureSetInfo = null;
		
		try {
			signatureSetInfo = getSignatureSetList.execute(type);
		} catch (Throwable t) {
			params.put("ApiAuditType", type.name());
			params.put("ApiInitInfo", info.getUserName());
			t.printStackTrace();
			log.debug(null, "getSignatureList", params, t);
		} finally {
			return signatureSetInfo;
		}
	}
	
	public static List<VexUser> getVexUsers() throws IOException, java.io.IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<VexUser> vexUser = new ArrayList<VexUser>();
		Yaml yaml = new Yaml();
		
		String resourceBaseDirectory = FileProcessUtil.getResourceBasePath();
		if(CommonUtil.isStringNullOrEmpty(resourceBaseDirectory)){
			resourceBaseDirectory = FileProcessUtil.getDefaultResourceBasePath();
		}
		
		String file_path = resourceBaseDirectory
				+ File.separator + "vex_users.yml";
		try {
			InputStream ios = new FileInputStream(new File(file_path));
			@SuppressWarnings("unchecked")
			Map<String, VexUser> result = (Map<String, VexUser>) yaml.load(ios);
			
			@SuppressWarnings("unchecked")
			List<VexUser> vexUserFromYml = (List<VexUser>) result.get("VexUser");
			
			for(int x=0; x < vexUserFromYml.size() ; x++){
        		@SuppressWarnings("unchecked")
				Map<String, Object> mapJSON = (Map<String, Object>) vexUserFromYml.get(x);
        		VexUser vu = new VexUser();
        		vu.setUserName(mapJSON.get("userName").toString());
        		vu.setPassword(mapJSON.get("password").toString());
        		vu.setUserType(mapJSON.get("userType").toString());
        		vexUser.add(vu);
        	}
		} catch (Exception e) {
			log.debug(PortalErrors.VEX_GET_VEX_USERS, PortalConstants.METHOD_VEX_GET_VEX_USERS, params, e);
		} catch (Error e) {
			log.debug(PortalErrors.VEX_GET_VEX_USERS, PortalConstants.METHOD_VEX_GET_VEX_USERS, params, e);
		}
		return vexUser;
    }
	
	public static ApiInitInfo setLoginCredentials(){
		Map<String, Object> params = new HashMap<String, Object>();
		String localhost = null;
		List<VexUser> vexUser = null;
		try {
			vexUser = getVexUsers();
		} catch (IOException e1) {
			log.debug(PortalErrors.SET_LOGIN_CREDENTIALS, PortalConstants.METHOD_SET_LOGIN_CREDENTIALS, params, e1);
		}
		
		ApiInitInfo initInfo = null;
		try {
			 localhost = ConfigurationFileParser.getConfig(PortalConstants.VEX_HOST).toString().trim();
			 initInfo = new ApiInitInfo(localhost, vexUser.get(0).getUserName(), vexUser.get(0).getPassword());
		} catch (Exception e) {
			log.debug(PortalErrors.SET_LOGIN_CREDENTIALS, PortalConstants.METHOD_SET_LOGIN_CREDENTIALS, params, e);
		}
		return initInfo;
	}
	
	public static boolean setImportDefaultVexScanSetting(long lscanId, long lProjectId){
		boolean response = false;
		VexScanSetting scanSetting = null;
		Map<String, Object> params = new HashMap<String, Object>();
		Properties importFile = null;
		InputStream input = null;
		try {
			importFile = new Properties();	
			String resourceBaseDirectory = FileProcessUtil.getResourceBasePath();
			if(CommonUtil.isStringNullOrEmpty(resourceBaseDirectory)){
				resourceBaseDirectory = FileProcessUtil.getDefaultResourceBasePath();
			}
			
			String file_path = resourceBaseDirectory
					+ File.separator + "import_scan_info.properties";
			
			input = new FileInputStream(new File(file_path));
			importFile.load(input);
			
			Project project 				= ProjectLocalServiceUtil.getProject(lProjectId);
			int serverFiles 				= project.getGetServerFiles();
			int serverSettings 				= project.getGetServerSettings();
//			String webSignatureSetGroup 	= importFile.getProperty("webScanSignatureSetID");
//			String numofthread 				= importFile.getProperty("webScanThreadNo");
//			String serverFilesSignatureSetGroup 	= importFile.getProperty("serverFilesSignatureSetID");
//			String serverSettingsSignatureSetGroup 	= importFile.getProperty("serverSettingsSignatureSetID");
			String webSignatureSetGroup 	= project.getSelectedWebSignature();
			String numofthread 				= importFile.getProperty("webScanThreadNo");
			String serverFilesSignatureSetGroup 	= project.getSelectedServerFilesSignature();
			String serverSettingsSignatureSetGroup 	= project.getSelectedServerSettingsSignature();
			
			scanSetting = VexScanSettingLocalServiceUtil.createVexScanSettingObj();
			scanSetting.setScanid(lscanId);
			scanSetting.setImplementationenvironment(1);
			scanSetting.setSetemailnotification(1);
			scanSetting.setGetserverfiles(serverFiles);
			scanSetting.setGetserversettings(serverSettings);
			scanSetting.setSelectedwebsignature(webSignatureSetGroup);
			scanSetting.setSelectedserverfilessignature(serverFilesSignatureSetGroup);
			scanSetting.setSelectedserversettingssignature(serverSettingsSignatureSetGroup);
			scanSetting.setNumofthread(Integer.parseInt(numofthread));
			scanSetting.setStarturl(" ");
			scanSetting.setAuthorizedpatrolurl(" ");
			
			VexScanSettingLocalServiceUtil.addVexScanSetting(scanSetting);
			response = true;
		} catch (Exception e) {
			params.put("Portal Scan ID: ", lscanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, e);
		} finally{
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					params.put("Portal Scan ID: ", lscanId);
					log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_ADD_VEX_SCAN_SETTING, params, e);
				}
			}
		}
		return response;
	}
	
	public static List<Object> getVexDetectionResultReview (ActionRequest actionRequest, int iUserAction, ActionResponse actionResponse) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> detectionResultList = new ArrayList<Object>();
		Long lUserId = 0L;
		User user = null;
		Object oUserId = null;
		String strDetectionResultId = PortalConstants.STRING_EMPTY;
		String strScanid = null;
		String strRiskLevel;
		String strCategory = PortalConstants.STRING_EMPTY;
		String strOverview = PortalConstants.STRING_EMPTY;
		String strFunctionName = PortalConstants.STRING_EMPTY;
		String strUrl = PortalConstants.STRING_EMPTY;
		String strParameterName = PortalConstants.STRING_EMPTY;
		String strDetectionJudgment = PortalConstants.STRING_EMPTY;
		String strReviewComment = PortalConstants.STRING_EMPTY;
		String strResendInVex = PortalConstants.STRING_EMPTY;
		Map<String, Object> searchedDetectionResult = new HashMap<String, Object>();
		int istart = 0;
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}

			strScanid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID).trim();
			istart = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_START);
			
			HttpSession session = ControllerHelper.getHttpSession(actionRequest);
			oUserId = session.getAttribute(PortalConstants.USER_ID);
		
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId != PortalConstants.LONG_ZERO) {
					user = UserLocalServiceUtil.getUser(lUserId);
					
					if (CommonUtil.isObjectNull(user)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
						throw new UBSPortalException(PortalErrors.INVALID_USER, PortalMessages.USER_INVALID);
					}
				}
			}
			
			if (iUserAction == PortalConstants.USER_EVENT_SEARCH_DETECTION_REVIEW) {
				strDetectionResultId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_RESULT_ID).trim();
				strRiskLevel = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_RISK_LEVEL);
				strCategory = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_CATEGORY).trim();
				strOverview = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_OVERVIEW).trim();
				strFunctionName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_FUNCTION_NAME).trim();
				strUrl = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_URL).trim();
				strParameterName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_PARAMETER_NAME).trim();
				strDetectionJudgment = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_JUDGEMENT).trim();
				strReviewComment = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_REVIEW_COMMENT).trim();
				strResendInVex = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX).trim();
	
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_RESULT_ID, strDetectionResultId);
				searchedDetectionResult.put(PortalConstants.PARAM_SCAN_ID, strScanid);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_RISK_LEVEL, strRiskLevel);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_CATEGORY, strCategory);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_OVERVIEW, strOverview);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_FUNCTION_NAME, strFunctionName);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_URL, strUrl);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_PARAMETER_NAME, strParameterName);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_JUDGEMENT, strDetectionJudgment);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_REVIEW_COMMENT, strReviewComment);
				searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX, strResendInVex);
			} else {
				if(!CommonUtil.isObjectNull(strScanid)){
					long lScanId = Long.parseLong(strScanid.toString());
					detectionResultList = VexDetectionResultLocalServiceUtil.getVexDetectionResultReviews(lScanId, user, searchedDetectionResult, istart, null, null);
				}
				
				if(detectionResultList == null || detectionResultList.isEmpty()){
					VexPortlet.getDetectionResultsFromXml(actionRequest, actionResponse);
				}
			}
			
			if(!CommonUtil.isObjectNull(strScanid) && (detectionResultList == null || detectionResultList.isEmpty())){
				long lScanId = Long.parseLong(strScanid.toString());
				detectionResultList = VexDetectionResultLocalServiceUtil.getVexDetectionResultReviews(lScanId, user, searchedDetectionResult, istart, null, null);
			}
		} catch (PortalException pe) {
			params.put("lUserId", lUserId);
			params.put("user", user);
			params.put("searchedProject", searchedDetectionResult);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}
			
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("user", user);
				params.put("searchedDetectionResult", searchedDetectionResult);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isMapNullOrEmpty(searchedDetectionResult)) {
				searchedDetectionResult.clear();
				searchedDetectionResult = null;
			}
		}
		
		return detectionResultList;
	}
	
	public static boolean confirmDetectionResult(ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Long lUserId = 0L;
		User user = null;
		Object oUserId = null;
		String strScanid = null;
		String strResultNumber = null;
		boolean bIsSuccess = false;
		try{
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			strScanid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID).trim();
			HttpSession session = ControllerHelper.getHttpSession(actionRequest);
			oUserId = session.getAttribute(PortalConstants.USER_ID);
		
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId != PortalConstants.LONG_ZERO) {
					user = UserLocalServiceUtil.getUser(lUserId);
					
					if (CommonUtil.isObjectNull(user)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
						throw new UBSPortalException(PortalErrors.INVALID_USER, PortalMessages.USER_INVALID);
					}
				}
			}

			long number = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_DETECTION_CONFIRM_NUMBER);
			String strReviewcomment = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_REVIEW_COMMENT).trim();
			String detectionResultId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_CONFIRM_RESULT_ID).trim();
			
			//TODO work for SWP-382
			//int iRiskType = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_DETECTION_RISK_LEVEL);
			//String strDetectionReview = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_JUDGEMENT).trim();
			
			if (number <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_DETECTION_NUMBER));
				throw new UBSPortalException(PortalErrors.RESEND_FAILED, PortalMessages.DETECTION_NUMBER_INVALID);
			}
			
			if (CommonUtil.isStringNullOrEmpty(detectionResultId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_DETECTION_RESULT_ID));
				throw new UBSPortalException(PortalErrors.RESEND_FAILED, PortalMessages.DETECTION_RESULT_ID_EMPTY);
			}
			if (CommonUtil.isStringNullOrEmpty(strReviewcomment)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_DETECTION_REVIEW_COMMENT));
				throw new UBSPortalException(PortalErrors.RESEND_FAILED, PortalMessages.REVIEW_COMMENT_EMPTY);
			}else{
				if(!ControllerHelper.isVexInputValid(strReviewcomment)){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_DETECTION_REVIEW_COMMENT));
					throw new UBSPortalException(PortalErrors.RESEND_FAILED, PortalMessages.REVIEW_COMMENT_INVALID);
				} else if (CommonUtil.getStringBytes(strReviewcomment) > PortalConstants.STRING_BYTE_MAX_200){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_DETECTION_REVIEW_COMMENT));
					throw new UBSPortalException(PortalErrors.RESEND_FAILED, PortalMessages.REVIEW_COMMENT_TOO_LONG);
				}
			}
			
			VexDetectionResultPK vexDetectionResultPK = new VexDetectionResultPK();
			vexDetectionResultPK.setScanresultid(number);
			vexDetectionResultPK.setScanid(Long.parseLong(strScanid));
									
			VexDetectionResult vexDetectionResult = VexDetectionResultLocalServiceUtil.getVexDetectionResult(vexDetectionResultPK);
			if(!CommonUtil.isStringNullOrEmpty(strReviewcomment)){
				vexDetectionResult.setDetectionjudgment(PortalConstants.STR_DETECTION_JUDGEMENT_OVER_DETECTION);
				vexDetectionResult.setReviewcomment(strReviewcomment);
				
				//TODO work for SWP-382
				//vexDetectionResult.setDetectionjudgment(strDetectionReview);
				//vexDetectionResult.setRisklevel(iRiskType);
				
				//Save review comment to DB.
				VexDetectionResultLocalServiceUtil.updateVexDetectionResult(vexDetectionResult);
				bIsSuccess = true;
			}
						
			if(!bIsSuccess){
				throw new UBSPortalException(PortalErrors.RESEND_FAILED, PortalMessages.RESEND_FAILED);
			}
		}catch (PortalException pe) {
			params.put("lUserId", lUserId);
			params.put("user", user);
			params.put("scanId", strScanid);
			params.put("resultNumber",strResultNumber);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_CONFIRM_DETECTION_RESULT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}
			
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_CONFIRM_DETECTION_RESULT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_CONFIRM_DETECTION_RESULT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("user", user);
				params.put("scanId", strScanid);
				params.put("resultNumber",strResultNumber);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_CONFIRM_DETECTION_RESULT, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_CONFIRM_DETECTION_RESULT, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bIsSuccess;
	}

	public static boolean resendDetectionResult(ActionRequest actionRequest, ApiInitInfo info) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Long lUserId = 0L;
		User user = null;
		Object oUserId = null;
		String strScanid = null;
		String strResultNumber = null;
		boolean isSuccess = false;
		try{
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			strScanid = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID).trim();
			strResultNumber = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_NUMBER).trim();
			
			HttpSession session = ControllerHelper.getHttpSession(actionRequest);
			oUserId = session.getAttribute(PortalConstants.USER_ID);
		
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId != PortalConstants.LONG_ZERO) {
					user = UserLocalServiceUtil.getUser(lUserId);
					
					if (CommonUtil.isObjectNull(user)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
						throw new UBSPortalException(PortalErrors.INVALID_USER, PortalMessages.USER_INVALID);
					}
				}
			}

			long number = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_DETECTION_NUMBER);
			VexDetectionResultPK vexDetectionResultPK = new VexDetectionResultPK();
			vexDetectionResultPK.setScanresultid(number);
			vexDetectionResultPK.setScanid(Long.parseLong(strScanid));
			
			ApiResendResultData resultData = null;
			InterfaceResend resend = null;
			Scan scan = ScanLocalServiceUtil.getScan(Long.parseLong(strScanid));
			Map<String,Object> resultObj = resendDetectionResult(info, scan.getCxAndroidScanId().toString(), strResultNumber);
			
			if(null != resultObj){
				resultData = (ApiResendResultData) resultObj.get(PortalConstants.STR_DETECTION_RESEND_RESULT);
				resend = (InterfaceResend) resultObj.get(PortalConstants.STR_DETECTION_RESEND);
				
				if(null != resultData && resend.getResult()){
					VexDetectionResult vexDetectionResult = VexDetectionResultLocalServiceUtil.getVexDetectionResult(vexDetectionResultPK);
					boolean bDetectResult = false;
					String strNumber  = PortalConstants.STRING_EMPTY;
					String strDetectResult = PortalConstants.STRING_EMPTY;
					
					if(null != vexDetectionResult){
						List<Map<String,Object>> mapData  = resultData.rawdata;
						strNumber = mapData.get(0).get(PortalConstants.VEX_TAG_ATTRIB_AUDIT_ID).toString();
						strDetectResult = mapData.get(0).get(PortalConstants.DETECT_RESULT).toString().trim();
						
						if(Boolean.parseBoolean(strDetectResult)){
							bDetectResult = Boolean.parseBoolean(strDetectResult);
						}
						
						if(Validator.isNumber(strNumber)){
							
							LocalDateTime now = LocalDateTime.now();
							now.atZone(ZoneId.systemDefault());
						    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PortalConstants.DATE_FORMAT_M_D_Y_HH_MM);
							if(bDetectResult){
								vexDetectionResult.setListofdetectedresult(appendSemiColonValues(vexDetectionResult.getListofdetectedresult(), "0"));
								vexDetectionResult.setDetectionjudgment(PortalConstants.STR_DETECTION_JUDGEMENT);
							}else{
								vexDetectionResult.setListofdetectedresult(appendSemiColonValues(vexDetectionResult.getListofdetectedresult(), "2"));
								vexDetectionResult.setDetectionjudgment(PortalConstants.STR_DETECTION_JUDGEMENT_FIXED);
							}
							//Save the new updates of vulnerability
							vexDetectionResult.setListofinspectiondatetime(appendSemiColonValues(vexDetectionResult.getListofinspectiondatetime(),now.format(formatter)));
							vexDetectionResult.setIsresendinvex(1);
							VexDetectionResultLocalServiceUtil.updateVexDetectionResult(vexDetectionResult);
							isSuccess = true;
						}
					} 
				}
			}
			if(!isSuccess){
				if(null !=resend){
					params.put("Error ", resend.getErrorMessage());
				}
				throw new UBSPortalException(PortalErrors.RESEND_FAILED, PortalMessages.RESEND_FAILED);
			}
			
		}catch (PortalException pe) {
			params.put("lUserId", lUserId);
			params.put("user", user);
			params.put("scanId", strScanid);
			params.put("resultNumber",strResultNumber);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_RESEND_DETECTION_RESULT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}
			
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_RESEND_DETECTION_RESULT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_RESEND_DETECTION_RESULT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("user", user);
				params.put("scanId", strScanid);
				params.put("resultNumber",strResultNumber);
			}
			log.debug(strErrorCode, PortalConstants.METHOD_RESEND_DETECTION_RESULT, params, ubspe);
			throw ubspe;
		}  finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
				return isSuccess;
			}
		}
		
		return isSuccess;
	}
	
	private static String appendSemiColonValues(String strValue, String strNewValue){
		if(!CommonUtil.isStringNullOrEmpty(strValue)){
			if(strValue.endsWith(";")){
				strValue += strNewValue + ";";
			}else{
				strValue += ";" + strNewValue + ";";
			}
		}else{
			strValue = strNewValue + ";";
		}
		
		return strValue;
	}
}
