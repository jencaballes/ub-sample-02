package jp.ubsecure.portal.jubjub.portlet.service.mail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeUtility;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.Recipient;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;

import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;

public class MailSender implements Runnable {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(MailSender.class);
	
	private int emailEvent;
	private long entityId;
	private Object emailData;
	
	public MailSender (int emailEvent, long entityId, Object emailData) {
		this.emailEvent = emailEvent;
		this.entityId = entityId;
		this.emailData = emailData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void run () {
		Map<String, Object> params = new HashMap<String, Object>();
		String strSenderEmailAddress = PortalConstants.STRING_EMPTY;
		Map<String, Object> email = null;
		long lCompanyId = ControllerHelper.getCompanyId();
		String strReturnPath = PortalConstants.STRING_EMPTY;
		List<Recipient> iaRecipientsList = null;
		String strSubject = PortalConstants.STRING_EMPTY;
		String strContent = PortalConstants.STRING_EMPTY;
		
		try {
			try {
				strSenderEmailAddress = ConfigurationFileParser.getConfig(PortalConstants.CONFIG_KEY_EMAIL_SENDER_EMAIL_ADDRESS);
			} catch (UBSPortalException e) {
				params.put("strSenderEmailAddress", strSenderEmailAddress);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
				log.debug(e.getErrorCode(), PortalConstants.METHOD_RUN, params, e);
			}
			
			email = MailUtil.generateEmail(emailEvent, entityId, lCompanyId, this.emailData);
			
			if (!CommonUtil.isStringNullOrEmpty(strSenderEmailAddress) &&
					!CommonUtil.isMapNullOrEmpty(email)) {
				try {
					strReturnPath = ConfigurationFileParser.getConfig(PortalConstants.CONFIG_KEY_EMAIL_RETURN_PATH);
				} catch (UBSPortalException e) {
					params.put("strReturnPath", strReturnPath);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
					log.debug(e.getErrorCode(), PortalConstants.METHOD_RUN, params, e);
					
					strReturnPath = strSenderEmailAddress;
				}
				
				iaRecipientsList = (List<Recipient>) email.get(PortalConstants.EMAIL_RECIPIENTS);
				try {
					strSubject = MimeUtility.encodeText((String) email.get(PortalConstants.EMAIL_SUBJECT));
				} catch (NullPointerException e) {
					strSubject = null;
					params.put("strSubject", strSubject);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
					log.debug(PortalErrors.NULL_POINTER_EXCEPTION, PortalConstants.METHOD_RUN, params, e);
				}
				strContent = (String) email.get(PortalConstants.EMAIL_CONTENT);
				
				if (!CommonUtil.isObjectNull(strSubject) ||
						!CommonUtil.isObjectNull(strContent)) {
					if (CommonUtil.isListNullOrEmpty(iaRecipientsList)) {
						params.put("iaRecipientsList", iaRecipientsList);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_RECIPIENT));
						throw new UBSPortalException(PortalErrors.MAIL_SENDER_RUN_RECIPIENTS_INVALID, PortalMessages.RECIPIENTS_INVALID);
					} else {
						if (CommonUtil.isStringNullOrEmpty(strSubject)) {
							strSubject = PortalConstants.STRING_EMPTY;
						}
						
						if (CommonUtil.isStringNullOrEmpty(strContent)) {
							strContent = PortalConstants.STRING_EMPTY;
						}
						
						send(iaRecipientsList, strReturnPath, strSubject, strContent, strSenderEmailAddress);
					}
				}
			}
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_RUN, params, e);
		} catch (IOException e) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_RUN, params, e);
		} finally {
			params.clear();
			params = null;
		}
	}
	
	private static void send (List<Recipient> iaRecipientsList, String strReturnPath, String strSubject, String strContent, String strSenderEmailAddress) {
		Map<String, Object> params = new HashMap<String, Object>();
		String strTempMailFile = PortalConstants.TEMP_MAIL_FILE + Thread.currentThread().getId() + PortalConstants.TEXT_FILE_EXTENSION;
		String strCommand = PortalConstants.COMMAND_SENDMAIL_T_I_F + " %s < ";
		
		String resourceBaseDirectory = FileProcessUtil.getResourceBasePath();
		if(CommonUtil.isStringNullOrEmpty(resourceBaseDirectory)){
			resourceBaseDirectory = FileProcessUtil.getDefaultResourceBasePath();
		}
		String strFileLocation = resourceBaseDirectory
				+ File.separator
				+ strTempMailFile;
		
		try {
			strCommand = String.format(strCommand + strFileLocation, strReturnPath);
			
			for (Recipient recipient : iaRecipientsList) {
				if (!CommonUtil.isObjectNull(recipient) &&
						createTempMailFile(recipient, strSubject, strContent, strSenderEmailAddress, strFileLocation)) {
					ProcessBuilder processBuilder = new ProcessBuilder(PortalConstants.COMMAND_BIN_BASH, PortalConstants.COMMAND_MINUS_C, strCommand);
					Process process = null;
					
					for (int iSendAttempt = 0; iSendAttempt < 5; iSendAttempt++) {
						try {
							process = processBuilder.start();
							process.waitFor();
							
							break;
						} catch (IOException ioe) {
							params.put("strSenderEmailAddress", strSenderEmailAddress);
							params.put("iaRecipientsArr", iaRecipientsList);
							params.put("strSubject", strSubject);
							params.put("strContent", strContent);
							log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_RUN, params, ioe);
						}
						Thread.sleep(1000);
					}
					
					processBuilder = new ProcessBuilder(PortalConstants.COMMAND_BIN_BASH, PortalConstants.COMMAND_MINUS_C, PortalConstants.COMMAND_REMOVE + strFileLocation);
					process = processBuilder.start();
					process.waitFor();
					
					Thread.sleep(100);
				}
			}
		} catch (InterruptedException e) {
			log.debug(PortalErrors.INTERRUPTED_EXCEPTION, PortalConstants.METHOD_RUN, params, e);
		} catch (IOException e) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_RUN, params, e);
		} finally {
			if (CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
	}
	
	private static boolean createTempMailFile (Recipient recipient, String strSubject, String strContent, String strSenderEmailAddress, String strFileLocation) {
		Map<String, Object> params = new HashMap<String, Object>();
		PrintWriter pWriter = null;
		boolean bResult = true;
		
		try {
			pWriter = new PrintWriter(strFileLocation, PortalConstants.UTF8);
		
			String strTempMailContent = PortalConstants.MAIL_HEADER_FROM
					+ strSenderEmailAddress
					+ PortalConstants.STRING_CARRIAGE_RETURN
					+ PortalConstants.MAIL_HEADER_TO
					+ PortalConstants.FORMAT_SPECIFIER_STRING
					+ PortalConstants.STRING_CARRIAGE_RETURN
					+ PortalConstants.MAIL_HEADER_SUBJECT
					+ strSubject
					+ PortalConstants.STRING_CARRIAGE_RETURN
					+ PortalConstants.MAIL_HEADER_CONTENT_TYPE
					+ PortalConstants.STRING_CARRIAGE_RETURN
					+ PortalConstants.MAIL_HEADER_CONTENT_TRANSFER_ENCODING
					+ PortalConstants.STRING_CARRIAGE_RETURN
					+ strContent;
			
			String strText = String.format(strTempMailContent, recipient.getEmailAddress());
			
			if (!CommonUtil.isStringNullOrEmpty(strContent) && strContent.contains(PortalConstants.MAIL_CONTENT_USERNAME_PLACEHOLDER)) {
				strText = strText.replace(PortalConstants.MAIL_CONTENT_USERNAME_PLACEHOLDER, recipient.getUsername());
			}
	
			pWriter.write(strText);
			pWriter.flush();
			pWriter.close();
		} catch (FileNotFoundException e) {
			params.put("strFileLocation", strFileLocation);
			log.debug(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalConstants.METHOD_CREATE_TEMP_MAIL_FILE, params, e);
			bResult = false;
		} catch (UnsupportedEncodingException e) {
			params.put("pWriter", pWriter);
			log.debug(PortalErrors.UNSUPPORTED_ENCODING_EXCEPTION, PortalConstants.METHOD_CREATE_TEMP_MAIL_FILE, params, e);
			bResult = false;
		}
		
		return bResult;
	}
}
