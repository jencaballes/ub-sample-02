package jp.ubsecure.portal.jubjub.portlet.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.exception.NoSuchOrganizationException;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PwdGenerator;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsersItem;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.android.AndroidProjectManager;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

public class AndroidProjectMgmtController {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(AndroidProjectMgmtController.class);

	public static List<Object> getProjects (ActionRequest actionRequest, int iUserAction) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		String strProjectId = null;
		String strGroupName = null;
		String strCaseNumber = null;
		String strProjectName = null;
		String strProjectEndDateLow = null;
		String strProjectEndDateHigh = null;
		String strStatus = null;
		String strScanCount = null;
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		int iStart = PortalConstants.INT_ZERO;
		Map<String, Object> searchedProject = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				iStart = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_START);
				
				if (iUserAction == PortalConstants.USER_EVENT_SEARCH_PROJECT) {
					strProjectId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_ID).trim();
					strGroupName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_OWNER_GROUP).trim();
					strCaseNumber = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NUMBER).trim();
					strProjectName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
					strProjectEndDateLow = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE_LOW).trim();
					strProjectEndDateHigh = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE_HIGH).trim();
					strStatus = ParamUtil.getString(actionRequest, PortalConstants.PARAM_STATUS).trim();
					strScanCount = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NO_OF_SCANS).trim();
				}
				
				oUserId = session.getAttribute(PortalConstants.USER_ID);

				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId > PortalConstants.LONG_ZERO) {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					} else {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					}
					
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
					try {
						long lProjectId = Long.parseLong(strProjectId);
						
						if (lProjectId < PortalConstants.LONG_ZERO) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
					if (CommonUtil.getStringBytes(strGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_GROUP_NAME_TOO_LONG, PortalMessages.GROUP_NAME_TOO_LONG);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
					if (!Validator.isAlphanumericName(strCaseNumber)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
					}
					
					if (strCaseNumber.contains(PortalConstants.STRING_BLANK_SPACE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
					}
					
					if (CommonUtil.getStringBytes(strCaseNumber) > PortalConstants.STRING_BYTE_MAX_40) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_TOO_LONG, PortalMessages.CASE_NUMBER_TOO_LONG);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
					if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
					}
				}
				
				try {
					DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
					Date dteProjectEndDateLow = null;
					Date dteProjectEndDateHigh = null;
					
					if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
						if (strProjectEndDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
						}
						
						dteProjectEndDateLow = format.parse(strProjectEndDateLow);
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
						if (strProjectEndDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
						}
						
						dteProjectEndDateHigh = format.parse(strProjectEndDateHigh);
					}
					
					if (!CommonUtil.isObjectNull(dteProjectEndDateLow)
							&& !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
						if (dteProjectEndDateHigh.before(dteProjectEndDateLow)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE.toLowerCase()));
							throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
						}
					}
				} catch (ParseException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
					try {
						int iScanCount = Integer.parseInt(strScanCount);
						
						if (iScanCount < PortalConstants.INT_ZERO) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NO_OF_SCANS));
							throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_SCAN_COUNT_INVALID, PortalMessages.SCAN_COUNT_INVALID);
						}
					} catch (NumberFormatException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NO_OF_SCANS));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_SCAN_COUNT_INVALID, PortalMessages.SCAN_COUNT_INVALID);
					}
				}
				
				searchedProject.put(PortalConstants.PARAM_PROJECT_ID, strProjectId);
				searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
				searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, strCaseNumber);
				searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
				searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_LOW, strProjectEndDateLow);
				searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_HIGH, strProjectEndDateHigh);
				searchedProject.put(PortalConstants.PARAM_STATUS, strStatus);
				searchedProject.put(PortalConstants.PARAM_NO_OF_SCANS, strScanCount);
				
				projectList = ProjectLocalServiceUtil.getProjects(searchedProject, user, ProjectType.ANDROID.getInteger(), iStart);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put("user", user);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_GET_PROJECTS, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (PortalException pe) {
			params.put("searchedProject", searchedProject);
			params.put("user", user);
			params.put("lUserId", lUserId);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lUserId", lUserId);
			params.put("user", user);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalErrors.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("searchedProject", searchedProject);
				params.put("lUserId", lUserId);
				params.put("user", user);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_PROJECTS, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_PROJECTS, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return projectList;
	}
	
	public static Project getProject (ActionRequest actionRequest) throws UBSPortalException {
		Project project = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				
				if (CommonUtil.isObjectNull(project)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
				} else {
					if (project.getType() != ProjectType.ANDROID.getInteger()) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_GET_PROJECT, params, nspe);
			throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		}  catch (PortalException pe) {
			params.put("lProjectId", pe);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", se);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", ubspe);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_GET_PROJECT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return project;
	}
	
	public static boolean updateProject (UploadPortletRequest uploadRequest, HttpSession session, int userAction) throws UBSPortalException {
		long lProjectId = PortalConstants.LONG_ZERO;
		String strCaseNumber = null;
		String strProjectName = null;
		long lOwnerGroup = PortalConstants.LONG_ZERO;
		Date dteProjectEndDate = null;
		DateFormat formatter = null;
		long [] lUsersArr = null;
		String [] strUsersArr = null;
		String strContentType = null;
		String strPackageNames = null;
		String [] strPackageNamesArr = null;
		List<String> packageNamesList = new ArrayList<String>();
		File file = null;
		String strFileName = null;
		Project project = null;
		boolean bSuccess = false;
		Organization organization = null;
		boolean bIsCaseNumberValid = false;
		String strSelectedUsers = null;
		String strEndDate = null;
		boolean bNewFile = false;
		Map<String, Object> params = new HashMap<String, Object>();
		User user = null;
		List<Organization> userOrganizationList = null;
		long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
		Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
		boolean bAdmin = false;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isObjectNull(oUserRole)) {
					String strUserRole = oUserRole.toString();
					
					if (!CommonUtil.isStringNullOrEmpty(strUserRole) &&
							(Integer.parseInt(strUserRole) == UserRole.OVERALL_ADMIN.getInteger()
							|| Integer.parseInt(strUserRole) == UserRole.GROUP_ADMIN.getInteger())) {
						bAdmin = true;
					}
				}
				
				formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				
				lProjectId = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_PROJECT_ID);
				strCaseNumber = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_CASE_NUMBER).trim();
				strProjectName = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
				lOwnerGroup = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_OWNER_GROUP);
				strEndDate = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_PROJECT_END_DATE);
				strSelectedUsers = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_SELECTED_USERS);
				bNewFile = ParamUtil.getBoolean(uploadRequest, PortalConstants.PARAM_NEW_FILE);
				
				if (!CommonUtil.isStringNullOrEmpty(strSelectedUsers)) {
					strUsersArr = strSelectedUsers.split(PortalConstants.DELIMITER_COMMA);
				}
				
				strPackageNames = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_LIBRARY_SETTING).trim();
				
				if (bNewFile) {
					strFileName = uploadRequest.getFileName(PortalConstants.PARAM_EXCLUSION_RULE_FILE);
					
					if (CommonUtil.isStringNullOrEmpty(strFileName)) {
						bNewFile = false;
					} else {
						strContentType = uploadRequest.getContentType(PortalConstants.PARAM_EXCLUSION_RULE_FILE);
						file = uploadRequest.getFile(PortalConstants.PARAM_EXCLUSION_RULE_FILE);
					}
				}
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						project = ProjectLocalServiceUtil.createProjectObj();
						lProjectId = ProjectLocalServiceUtil.createProjectId();
						project.setProjectId(lProjectId);
						project.setStatus(ProjectStatus.NOT_YET_COMPLETE.getInteger());
						project.setProjectCreateDate(new Date());
					} else {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
					}
				} else {
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.ANDROID.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
						}
					}
				}
				
				if (bAdmin) {
					if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
						if (!Validator.isAlphanumericName(strCaseNumber)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
							} else {
								throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
							}
						}
						
						if (strCaseNumber.contains(PortalConstants.STRING_BLANK_SPACE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
							} else {
								throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
							}
						}
						
						if (CommonUtil.getStringBytes(strCaseNumber) > PortalConstants.STRING_BYTE_MAX_40) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_CASE_NUMBER_TOO_LONG, PortalMessages.CASE_NUMBER_TOO_LONG);
							} else {
								throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_TOO_LONG, PortalMessages.CASE_NUMBER_TOO_LONG);
							}
						}
	
						bIsCaseNumberValid = ProjectLocalServiceUtil.isCaseNumberValid(strCaseNumber, lProjectId);
						
						if (!bIsCaseNumberValid) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_ALREADY_EXISTS, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_CASE_NUMBER_ALREADY_EXISTS, PortalMessages.CASE_NUMBER_ALREADY_EXIST);
							} else {
								throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_ALREADY_EXISTS, PortalMessages.CASE_NUMBER_ALREADY_EXIST);
							}
						}
					} else {
						while (!bIsCaseNumberValid) {
							strCaseNumber = PwdGenerator.getPassword();
							
							bIsCaseNumberValid = ProjectLocalServiceUtil.isCaseNumberValid(strCaseNumber, lProjectId);
						}
					}
					
					if (CommonUtil.isStringNullOrEmpty(strProjectName)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_PROJECT_NAME, PortalMessages.NO_PROJECT_NAME);
						} else {
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_PROJECT_NAME, PortalMessages.NO_PROJECT_NAME);
						}
					} else if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
						} else {
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
						}
					}
					
					if (lOwnerGroup == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_GROUP.toLowerCase()));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_OWNER_GROUP, PortalMessages.NO_OWNER_GROUP);
						} else {
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_OWNER_GROUP, PortalMessages.NO_OWNER_GROUP);
						}
					} else {
						try { 
							organization = OrganizationLocalServiceUtil.getOrganization(lOwnerGroup);
						
							if (CommonUtil.isObjectNull(organization)) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_GROUP));
								
								if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
									throw new UBSPortalException(PortalErrors.ADD_PROJECT_OWNER_GROUP_DOES_NOT_EXIST, PortalMessages.OWNER_GROUP_DOES_NOT_EXIST);
								} else {
									throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_OWNER_GROUP_DOES_NOT_EXIST, PortalMessages.OWNER_GROUP_DOES_NOT_EXIST);
								}
							}
						} catch (NoSuchOrganizationException e) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_GROUP));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_OWNER_GROUP_DOES_NOT_EXIST, PortalMessages.OWNER_GROUP_DOES_NOT_EXIST);
							} else {
								throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_OWNER_GROUP_DOES_NOT_EXIST, PortalMessages.OWNER_GROUP_DOES_NOT_EXIST);
							}
						}
					}
					
					if (CommonUtil.isStringNullOrEmpty(strEndDate)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_PROJECT_END_DATE, PortalMessages.NO_PROJECT_END_DATE);
						} else {
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_PROJECT_END_DATE, PortalMessages.NO_PROJECT_END_DATE);
						}
					} else {
						if (strEndDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
							} else {
								throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
							}
						}
						dteProjectEndDate = formatter.parse(ParamUtil.getString(uploadRequest, PortalConstants.PARAM_PROJECT_END_DATE));
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strPackageNames)) {
					if (CommonUtil.getStringBytes(strPackageNames) > PortalConstants.STRING_BYTE_MAX_200) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_EXCLUSION_PACKAGE_NAME));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_PACKAGE_NAME_TOO_LONG, PortalMessages.PACKAGE_NAME_TOO_LONG);
						} else {
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PACKAGE_NAME_TOO_LONG, PortalMessages.PACKAGE_NAME_TOO_LONG);
						}
					}
					
					strPackageNamesArr = strPackageNames.split(PortalConstants.DELIMITER_NEXT_LINE);
					int packageNamesArrLength = strPackageNamesArr.length;
					
					String [] tempPackageNamesArr = new String[packageNamesArrLength];

					int j = PortalConstants.INT_ZERO;
					for (int i = 0; i < packageNamesArrLength; i++) {
						String packageName = strPackageNamesArr[i].trim();
						if (!CommonUtil.isStringNullOrEmpty(packageName)) {
							if (!packageName.equals(PortalConstants.DELIMITER_NEXT_LINE)) {
								tempPackageNamesArr[j] = packageName;
								j++;
							}
						}
					}
					
					if (j < packageNamesArrLength) {
						strPackageNamesArr = new String[j];
						
						for (int i = 0; i < j; i++) {
							strPackageNamesArr[i] = tempPackageNamesArr[i];
						}
					}
					
					packageNamesArrLength = strPackageNamesArr.length;
					
					if (strPackageNamesArr != null) {
						for (int i = 0; i < packageNamesArrLength; i++) {
							if (strPackageNamesArr[i].startsWith(PortalConstants.DELIMITER_DOT) ||
									strPackageNamesArr[i].endsWith(PortalConstants.DELIMITER_DOT)) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXCLUSION_PACKAGE_NAME));
								
								if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
									throw new UBSPortalException(PortalErrors.ADD_PROJECT_PACKAGE_NAME_INVALID, PortalMessages.PACKAGE_NAME_INVALID);
								} else {
									throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PACKAGE_NAME_INVALID, PortalMessages.PACKAGE_NAME_INVALID);
								}
							} else {
								String tempPackageName = strPackageNamesArr[i].toString();
								String [] packageName = tempPackageName.split(PortalConstants.DELIMITER_ESCAPED_DOT);
								int packageNameLength = packageName.length;
								
								for (int k = 0; k < packageNameLength; k++) {
									if (!Validator.isAlphanumericName(packageName[k])) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXCLUSION_PACKAGE_NAME));
										
										if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
											throw new UBSPortalException(PortalErrors.ADD_PROJECT_PACKAGE_NAME_INVALID, PortalMessages.PACKAGE_NAME_INVALID);
										} else {
											throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PACKAGE_NAME_INVALID, PortalMessages.PACKAGE_NAME_INVALID);
										}
									}
								}
							}
						}
					}
				}
				
				if (bNewFile) {
					if (!isChecklistValid(strContentType, file)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_TYPE_INVALID, PortalConstants.ERROR_LOG_PARAM_CHECKLIST));
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_FILE_TYPE_INVALID, PortalMessages.FILE_INVALID);
					}
					
					if (FileProcessUtil.getFileSize(file) > PortalConstants.MAX_SIZE_CHECKLIST) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_CHECKLIST));
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_FILE_SIZE_TOO_BIG_20, PortalMessages.CHECKLIST_FILE_SIZE_TOO_BIG);
					}
					
					if (FileProcessUtil.getFileSize(file) == PortalConstants.DOUBLE_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_CHECKLIST));
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_FILE_EMPTY, PortalMessages.FILE_INVALID);
					}
				}
				
				if (bAdmin) {
					if (strUsersArr != null) {
						int usersArrLength = strUsersArr.length;
						
						if (usersArrLength > 0) {
							lUsersArr = new long[usersArrLength];
							
							for (int i = 0; i < usersArrLength; i++) {
								lUsersArr[i] 			= Long.parseLong(strUsersArr[i]);
								List<Role> userRole 	= null;
								
								try {
									user = UserLocalServiceUtil.getUser(lUsersArr[i]);
									
									if (user == null) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
										
										if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
											throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.PROJECT_USER_DOES_NOT_EXIST);
										} else {
											throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.PROJECT_USER_DOES_NOT_EXIST);
										}
									}
									
									// Get user Role
									userRole = user.getRoles();
									
								} catch (NoSuchUserException e) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
									
									if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
										throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.PROJECT_USER_DOES_NOT_EXIST);
									} else {
										throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.PROJECT_USER_DOES_NOT_EXIST);
									}
								}
								
								userOrganizationList = OrganizationLocalServiceUtil.getUserOrganizations(lUsersArr[i]);
								
								if (CommonUtil.isListNullOrEmpty(userOrganizationList)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_DOES_NOT_BELONG_TO_GROUP);
									
									if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
										throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
									} else {
										throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
									}
								} else {
									// flag if organization is found 
									boolean orgIsFound = false; 
									
									for(Role r : userRole)
									{
										if(r.getName().equals(PortalConstants.GROUP_ADMINISTRATOR)){
											// find the organization 
											for(Organization org : userOrganizationList){
												if(org.getOrganizationId() == lOwnerGroup){
													orgIsFound = true; 
													break;
												}											
											}
										}else{
											orgIsFound = (userOrganizationList.get(0).getOrganizationId() == lOwnerGroup)? true : false;
										}
									}
									
									if (false == orgIsFound) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_DOES_NOT_BELONG_TO_GROUP);
										
										if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
											throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
										} else {
											throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
										}
									}
								}
							}
						} else {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_PROJECT_USER.toLowerCase()));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_PROJECT_USERS, PortalMessages.NO_PROJECT_USERS);
							} else {
								throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_PROJECT_USERS, PortalMessages.NO_PROJECT_USERS);
							}
						}
					} else {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_PROJECT_USER.toLowerCase()));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_PROJECT_USERS, PortalMessages.NO_PROJECT_USERS);
						} else {
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_PROJECT_USERS, PortalMessages.NO_PROJECT_USERS);
						}
					}
				}
				
				if (strPackageNamesArr != null) {
					int packageNamesArrLength = strPackageNamesArr.length;
					for (int i = 0; i < packageNamesArrLength; i++) {
						packageNamesList.add(strPackageNamesArr[i]);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strFileName)) {
					project.setChecklistFileName(strFileName);
				}
				
				if (bAdmin) {
					project.setCaseNumber(strCaseNumber);
					project.setProjectName(strProjectName);
					project.setOwnerGroup(lOwnerGroup);
					project.setProjectEndDate(dteProjectEndDate);
					project.setType(ProjectType.ANDROID.getInteger());
				} else {
					strProjectName = project.getProjectName();
				}
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					lCxAndroidProjectId = AndroidProjectManager.createAndroidProject(strProjectName, file, packageNamesList);
					
					if (lCxAndroidProjectId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_PROJECT_REGISTRATION_FAILED);
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_FAILED, PortalMessages.ADD_PROJECT_FAILED);
					} else {
						project.setCxAndroidProjectId(String.valueOf(lCxAndroidProjectId));
					}
				} else {
					AndroidProjectManager.updateAndroidProjectName(strProjectName, file, packageNamesList, Long.parseLong(project.getCxAndroidProjectId()));
				}
				
				bSuccess = ProjectLocalServiceUtil.updateProject(null, project, packageNamesList, file, lUsersArr, bAdmin);
				
				if (bSuccess) {
					Object oUserId = session.getAttribute(PortalConstants.USER_ID);
					long lUserId = PortalConstants.LONG_ZERO;
					int emailEvent = PortalConstants.INT_ZERO;
					
					if (!CommonUtil.isObjectNull(oUserId)) {
						lUserId = Long.parseLong(oUserId.toString());
					}
					
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						emailEvent = PortalConstants.EMAIL_EVENT_ADD_PROJECT;
					} else {
						emailEvent = PortalConstants.EMAIL_EVENT_UPDATE_PROJECT;
					}
					
					MailUtil.sendEmail(emailEvent, lUserId, lProjectId);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_PROJECT, params, nspe);
			throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		}
		catch (ParseException pe) {
			params.put("strEndDate", strEndDate);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
			
			if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
				log.debug(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID, PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID, pe);
			} else {
				log.debug(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID, PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID, pe);
			}
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			params.put("strCaseNumber", strCaseNumber);
			params.put("lOwnerGroup", lOwnerGroup);
			params.put("organization", organization);
			params.put("lUsersArr", lUsersArr);
			params.put("user", user);
			params.put("file", file);
			params.put("packageNamesList", packageNamesList);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lUsersArr", lUsersArr);
			params.put("user", user);
			params.put("userOrganizationList", userOrganizationList);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPDATE_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
				params.put("strCaseNumber", strCaseNumber);
				params.put("bIsCaseNumberValid", bIsCaseNumberValid);
				params.put("strProjectName", strProjectName);
				params.put("lOwnerGroup", lOwnerGroup);
				params.put("organization", organization);
				params.put("strEndDate", strEndDate);
				params.put("file", file);
				params.put("strContentType", strContentType);
				params.put("user", user);
				params.put("lUsersArr", lUsersArr);
				params.put("userOrganizationList", userOrganizationList);
				params.put("strFileName", strFileName);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_UPDATE_PROJECT, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_UPDATE_PROJECT, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(userOrganizationList)) {
				userOrganizationList = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(packageNamesList)) {
				packageNamesList = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean deleteProject (ActionRequest actionRequest) throws UBSPortalException {
		boolean bSuccess = false;
		long lProjectId = PortalConstants.LONG_ZERO;
		String strCxAndroidProjectId = null;
		long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.DELETE_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
				ProjectLocalServiceUtil.clearCache();
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				boolean bIsProjectUser = false;
				
				if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
					bIsProjectUser = true;
				} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
					long[] userOrgIdList = user.getOrganizationIds();
					for (long userOrgId : userOrgIdList) {
						if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
							bIsProjectUser = true;
							break;
						}
					}
				} else {
					if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
						for (ProjectUsers pu : projectUsersList) {
							if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
								bIsProjectUser = true;
								break;
							}
						}
					}
				}
				
				if (!bIsProjectUser) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
					throw new UBSPortalException(PortalErrors.DELETE_PROJECT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
				}
				
				if (CommonUtil.isObjectNull(project)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
					throw new UBSPortalException(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
				} else {
					if (project.getType() != ProjectType.ANDROID.getInteger()) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
						throw new UBSPortalException(PortalErrors.DELETE_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
					}
				}
				
				strCxAndroidProjectId = project.getCxAndroidProjectId();
				
				if (!CommonUtil.isStringNullOrEmpty(strCxAndroidProjectId)) {
					lCxAndroidProjectId = Long.parseLong(strCxAndroidProjectId);
				}
				
				AndroidProjectManager.deleteAndroidProject(lCxAndroidProjectId, lProjectId);
				bSuccess = ProjectLocalServiceUtil.deleteRelatedToProject(lProjectId, PortalConstants.LONG_ZERO, ProjectType.ANDROID.getInteger(), null);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_PROJECT, params, nspe);
			throw new UBSPortalException(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_DELETE_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_DELETE_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_DELETE_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_DELETE_PROJECT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean completeProject (ActionRequest actionRequest) throws UBSPortalException {
		boolean bSuccess = false;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		User user = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				
				if (CommonUtil.isObjectNull(project)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
					throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
				}
				
				boolean bIsProjectUser = false;
				
				if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
					bIsProjectUser = true;
				}
				
				List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					
				if (CommonUtil.isListNullOrEmpty(projectUsersList) && !bIsProjectUser) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
					throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
				} else {
					
					if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						long[] userOrgIdList = user.getOrganizationIds();
						for (long userOrgId : userOrgIdList) {
							if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
								bIsProjectUser = true;
								break;
							}
						}
					} else {
						for (ProjectUsers pu : projectUsersList) {
							if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
								bIsProjectUser = true;
								break;
							}
						}
					}
						
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
				}
					
				if (project.getType() != ProjectType.ANDROID.getInteger()) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
					throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
				}
				
				if (isProjectComplete(lProjectId)) {
					bSuccess = ProjectLocalServiceUtil.completeProject(lProjectId);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchProjectException e) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_COMPLETE_PROJECT, params, e);
			throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, e);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			params.put("bSuccess", bSuccess);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_COMPLETE_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_COMPLETE_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_COMPLETE_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_COMPLETE_PROJECT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean openProject (ActionRequest actionRequest) throws UBSPortalException {
		boolean bSuccess = false;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Map<String, Object> params = new HashMap<String, Object>();
		Object oUserId = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		User user = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.OPEN_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				
				if (CommonUtil.isObjectNull(project)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
					throw new UBSPortalException(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
				}
				
				if (iUserRole != UserRole.OVERALL_ADMIN.getInteger() && iUserRole != UserRole.GROUP_ADMIN.getInteger()) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
					throw new UBSPortalException(PortalErrors.OPEN_PROJECT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
				}
				
				if (project.getType() != ProjectType.ANDROID.getInteger()) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
					throw new UBSPortalException(PortalErrors.OPEN_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_ANDROID);
				}
				
				if (project.getStatus() == ProjectStatus.COMPLETE.getInteger()) {
					bSuccess = ProjectLocalServiceUtil.openProject(project);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			if (bSuccess) {
				MailUtil.sendEmail(PortalConstants.USER_EVENT_OPEN_PROJECT, lUserId, lProjectId);
			}else{
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_OPEN_PROJECT_FAILED);
				log.debug(PortalErrors.OPEN_PROJECT_FAILED, PortalConstants.METHOD_OPEN_PROJECT, params, new UBSPortalException(PortalErrors.OPEN_PROJECT_FAILED, PortalMessages.OPEN_PROJECT_FAILED));	
			}
		} catch (NoSuchProjectException e) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_OPEN_PROJECT, params, e);
			throw new UBSPortalException(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, e);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			params.put("bSuccess", bSuccess);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_OPEN_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_OPEN_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_OPEN_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_OPEN_PROJECT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static Project getProject (ResourceRequest resourceRequest) throws UBSPortalException {
		Project project = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				lProjectId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				project = ProjectLocalServiceUtil.getProject(lProjectId);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_GET_PROJECT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return project;
	}
	
	public static List<ProjectUsersItem> getProjectUsers (ActionRequest actionRequest) throws UBSPortalException {
		long lProjectId = PortalConstants.LONG_ZERO;
		List<ProjectUsersItem> resultList = new ArrayList<ProjectUsersItem>();
		List<User> userList = null;
		List<ProjectUsers> projectUsersList = null;
		ProjectUsersItem item = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
				
				if (!projectUsersList.isEmpty()) {
					userList = UserLocalServiceUtil.getUsers(0, UserLocalServiceUtil.getUsersCount());
					
					for (ProjectUsers projectUsers : projectUsersList) {
						for (User user : userList) {
							if (projectUsers.getUserId().equals(user.getEmailAddress())) {
								item = new ProjectUsersItem();
								
								item.setUserId(user.getUserId());
								item.setEmailAddress(user.getEmailAddress());
								item.setUserName(user.getFirstName());
								resultList.add(item);
							}
						}
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);		

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECT_USERS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT_USERS, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECT_USERS, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_GET_PROJECT_USERS, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(userList)) {
				userList = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
				projectUsersList = null;
			}
		}
		
		return resultList;
	}
	
	private static boolean isProjectComplete (long lProjectId) throws UBSPortalException {
		List<Scan> scanList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bFlag = false;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				scanList = ScanLocalServiceUtil.getScansByProjectId(lProjectId);
				
				if (scanList.size() == 0) {
					return true;
				}
				
				for (Scan scan : scanList) {
					if (scan.getStatus() == ScanStatus.SCAN_WAITING.getInteger() ||
						scan.getStatus() == ScanStatus.SCANNING.getInteger() ||
						scan.getStatus() == ScanStatus.FAILURE.getInteger() ||
						scan.getStatus() == ScanStatus.REPORT_MAKING.getInteger()) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_COMPLETE_PROJECT_FAILED);
						log.debug(PortalErrors.COMPLETE_PROJECT_PROJECT_CANNOT_BE_COMPLETED, PortalConstants.METHOD_IS_PROJECT_COMPLETE, params, new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_CANNOT_BE_COMPLETED, PortalMessages.COMPLETE_PROJECT_FAILED));
						return false;
					} else {
						bFlag = true;
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("scanList", scanList);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getAndroidErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_IS_PROJECT_COMPLETE, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_IS_PROJECT_COMPLETE, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_IS_PROJECT_COMPLETE, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bFlag;
	}
	
	private static boolean isChecklistValid (String strContentType, File file) throws UBSPortalException {
		boolean bIsValid = false;
		
		if (!CommonUtil.isStringNullOrEmpty(strContentType)) {
			strContentType = Normalizer.normalize(strContentType, Normalizer.Form.NFKD);
			
			if (strContentType.equalsIgnoreCase(PortalConstants.CONTENT_TYPE_XLS)) {
				if (CommonUtil.isValidFile(file)) {
					
					if (FileUtil.getExtension(file.getAbsolutePath()).equalsIgnoreCase(PortalConstants.EXCEL_FILE_EXTENSION)) {
						bIsValid = true;
					}
				}
			}
		}
		
		return bIsValid;
	}
}
