package jp.ubsecure.portal.jubjub.portlet.controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectException;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchScanException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.model.ResultItem;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.ScanItem;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.CxScanManager;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.CxScanReport;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

public class CxScanMgmtController {
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(CxScanMgmtController.class);
	
	public static Scan getScan (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		long lScanId = PortalConstants.LONG_ZERO;
		Scan scan = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				
				if (lScanId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
					throw new UBSPortalException(PortalErrors.GET_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
				} else {
					scan = ScanLocalServiceUtil.getScan(lScanId);
					
					if (!CommonUtil.isObjectNull(scan)) {
						long lProjectId = scan.getProjectId();
						
						Project project = ProjectLocalServiceUtil.getProject(lProjectId);
						
						if (project != null) {
							if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
								throw new UBSPortalException(PortalErrors.GET_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
							}
						}
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.GET_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_GET_SCAN, params, nsse);
			throw new UBSPortalException(PortalErrors.GET_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", scan.getProjectId());
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_GET_SCAN, params, nspe);
			throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (SystemException se) {
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lScanId", lScanId);
				params.put("scan", scan);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_GET_SCAN, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return scan;
	}
	
	public static boolean updateScan (UploadPortletRequest uploadRequest, String strCxSessionId, User user, int userAction) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		File file = null;
		int iProcess = PortalConstants.INT_ZERO;
		String strContentType = null;
		Scan scan = null;
		Project project = null;
		boolean bSuccess = false;
		String strFileName = null;
		/*String strRunId = null;*/
		long lPresetId = PortalConstants.LONG_ZERO;
		boolean bHasFile = false;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				lProjectId = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_PROJECT_ID);
				lScanId = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_SCAN_ID);
				iProcess = ParamUtil.getInteger(uploadRequest, PortalConstants.PARAM_PROCESS);
				bHasFile = ParamUtil.getBoolean(uploadRequest, PortalConstants.PARAM_HAS_FILE_UPLOAD);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					
					if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
						throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
					} else {
						throw new UBSPortalException(PortalErrors.UPDATE_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
					}
				} else {
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
							throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
						} else {
							throw new UBSPortalException(PortalErrors.UPDATE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
						}
					} else {
						if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
								throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
							} else {
								throw new UBSPortalException(PortalErrors.UPDATE_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
							}
						}
					}
				}
				
				if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
					file = uploadRequest.getFile(PortalConstants.PARAM_INSPECTION_FILE);
					
					if (file == null && bHasFile) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_ZIP));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG, PortalMessages.ZIP_FILE_SIZE_TOO_BIG);
					}
					
					if (file != null) {
						strContentType = uploadRequest.getContentType(PortalConstants.PARAM_INSPECTION_FILE);
						strFileName = uploadRequest.getFileName(PortalConstants.PARAM_INSPECTION_FILE);
					}
					
					if (CommonUtil.isStringNullOrEmpty(strContentType)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
					}
					
					if (!ControllerHelper.isZIPValid(strContentType, file)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_TYPE_INVALID, PortalConstants.ERROR_LOG_PARAM_ZIP));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_NOT_ZIP, PortalMessages.FILE_INVALID);
					}
					
					if (FileProcessUtil.getFileSize(file) == PortalConstants.DOUBLE_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_FILE));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_EMPTY, PortalMessages.FILE_INVALID);
					}
					
					if (FileProcessUtil.getFileSize(file) > PortalConstants.MAX_SIZE_ZIP) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_FILE_SIZE_TOO_BIG, PortalConstants.ERROR_LOG_PARAM_ZIP));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG, PortalMessages.ZIP_FILE_SIZE_TOO_BIG);
					}
				}
				
				if (iProcess == PortalConstants.INT_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_PROCESS.toLowerCase()));
					
					if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
						throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_PROCESS, PortalMessages.NO_PROCESS);
					} else {
						throw new UBSPortalException(PortalErrors.UPDATE_SCAN_NO_PROCESS, PortalMessages.NO_PROCESS);
					}
				}
				
				if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
					scan = ScanLocalServiceUtil.createScanObj();
					lScanId = ScanLocalServiceUtil.createScanId();
					scan.setScanId(lScanId);
					scan.setScanManager(user.getEmailAddress());
					scan.setProjectId(lProjectId);
					
					if (file == null) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_FILE.toLowerCase()));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_NO_FILE, PortalMessages.FILE_INVALID);
					}
					
					scan.setFileName(strFileName);
					
					if (lProjectId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
						throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
					} else {
						project = null;
						project = ProjectLocalServiceUtil.getProject(lProjectId);
						
						if (CommonUtil.isObjectNull(project)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
							throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
						} else {
							if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
								throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
							}
						}
					}
					
					lPresetId = project.getPresetId();
				} else {
					if (lScanId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.UPDATE_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.getScan(lScanId);
						
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.UPDATE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							lProjectId = scan.getProjectId();
							
							if (lProjectId == PortalConstants.LONG_ZERO) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.UPDATE_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								project = ProjectLocalServiceUtil.getProject(lProjectId);
								
								if (CommonUtil.isObjectNull(project)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.UPDATE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.UPDATE_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
									}
								}
							}
							
							lPresetId = project.getPresetId();
						}
					}
				}
				
				scan.setProcess(iProcess);
				
				if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
					if (CommonUtil.isStringNullOrEmpty(strCxSessionId)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
						throw new UBSPortalException(PortalErrors.INVALID_CX_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
					}
					
					Scan newScan = CxScanManager.registerCxScan(scan, file, strCxSessionId, project.getProjectName(), false, lPresetId, ProjectType.CX_SUITE.getInteger(), project.getProjectId());
					
					if (!CommonUtil.isObjectNull(newScan)) {
						newScan.setScanId(lScanId);
						newScan.setProjectId(scan.getProjectId());
						newScan.setRegistrationDate(new Date());
						newScan.setProcess(scan.getProcess());
						newScan.setScanManager(scan.getScanManager());
						newScan.setModifiedDate(new Date());
						
						scan = ScanLocalServiceUtil.updateScan(newScan);
						
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
							throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
						} else {
							bSuccess = true;
						}
					} else {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_REGISTRATION_FAILED);
						throw new UBSPortalException(PortalErrors.ADD_SCAN_FAILED, PortalMessages.ADD_SCAN_FAILED);
					}
				} else {
					scan = ScanLocalServiceUtil.updateScan(scan);
					
					if (CommonUtil.isObjectNull(scan)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_CHANGE_FAILED);
						throw new UBSPortalException(PortalErrors.UPDATE_SCAN_FAILED, PortalMessages.UPDATE_SCAN_FAILED);
					} else {
						bSuccess = true;
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
				log.debug(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.UPDATE_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.UPDATE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			
			log.debug(PortalErrors.UPDATE_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_SCAN, params, nsse);
			throw new UBSPortalException(PortalErrors.UPDATE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("iProcess", iProcess);
			params.put("strContentType", strContentType);
			params.put("file", file);
			params.put("strFileName", strFileName);
			params.put("strCxSessionId", strCxSessionId);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_UPDATE_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_UPDATE_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			params.put("lScanId", lScanId);
			params.put("iProcess", iProcess);
			params.put("strContentType", strContentType);
			params.put("file", file);
			params.put("strFileName", strFileName);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPDATE_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else if (strErrorCode.equals(PortalErrors.INVALID_FILE)) {
				ubspe.setErrorMessage(PortalMessages.FILE_INVALID);
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
				params.put("lScanId", lScanId);
				params.put("iProcess", iProcess);
				params.put("strContentType", strContentType);
				params.put("file", file);
				params.put("strFileName", strFileName);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_UPDATE_SCAN, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_UPDATE_SCAN, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean stopScan (ActionRequest actionRequest) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		String strCxSessionId = null;
		Project project = null;
		Scan scan = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		boolean bGetProject = true;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			oUserId = session.getAttribute(PortalConstants.USER_ID);
			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
			}
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			if (PortletCommonUtil.isDBConnected()) {
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				} else {
					user = UserLocalServiceUtil.getUser(lUserId);
					
					if (CommonUtil.isObjectNull(user)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
					}
				}
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					ProjectLocalServiceUtil.clearCache();
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						long[] userOrgIdList = user.getOrganizationIds();
						for (long userOrgId : userOrgIdList) {
							if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
								bIsProjectUser = true;
								break;
							}
						}
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.STOP_SCAN_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
						}
					}
					
					if (lScanId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.STOP_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.retrieveScan(lScanId);
						
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							lProjectId = scan.getProjectId();
							if (lProjectId == PortalConstants.LONG_ZERO) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.STOP_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(lProjectId);
								
								if (CommonUtil.isObjectNull(project)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
									throw new UBSPortalException(PortalErrors.STOP_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.STOP_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
									}
								}
							}
							
							if (scan.getStatus() != ScanStatus.SCAN_WAITING.getInteger() &&
									scan.getStatus() != ScanStatus.SCANNING.getInteger()) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_CANNOT_BE_ACTION, PortalConstants.ERROR_LOG_PARAM_SCAN, PortalConstants.ERROR_LOG_ACTION_STOP));
								throw new UBSPortalException(PortalErrors.STOP_SCAN_SCAN_NOT_IN_WAITING_SCANNING_STATE, PortalMessages.SCAN_CANNOT_BE_STOPPED);
							}
						}
					}
				}
				
				if (session.getAttribute(PortalConstants.CXSESSION_ID) == null) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
					throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
				} else {
					strCxSessionId = session.getAttribute(PortalConstants.CXSESSION_ID).toString();
					
					if (CommonUtil.isStringNullOrEmpty(strCxSessionId)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
						throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
					}
				}
				
				String strCxRunID 				= scan.getCxRunId();
				String strCxAndroidScanID 		= scan.getCxAndroidScanId();
				Long lCxRunID 					= Long.parseLong(strCxRunID);
				Long lCxAndroidScanId 			= Long.parseLong(strCxAndroidScanID);
				
				CxScanManager.cancelCxScan(strCxSessionId, strCxRunID , ProjectType.CX_SUITE.getInteger());
				
				scan.setStatus(ScanStatus.FAILURE.getInteger());
				scan.setFailedScanCause(PortalConstants.STOPPED_SCAN_MESSAGE);
				scan.setModifiedDate(new Date());
					
				bSuccess = ScanLocalServiceUtil.updateScan(PortalConstants.USER_EVENT_STOP_SCAN, scan);
				
				if (bSuccess) {
					Long lCxAndroidProjectID = Long.parseLong(scan.getCxAndroidProjectId());
					
					//delete the scan in CxSAST
					if(PortalConstants.LONG_ZERO < lCxAndroidScanId){
						CxScanManager.deleteCxScan(strCxSessionId, lCxAndroidScanId, lProjectId, lScanId, ProjectType.CX_SUITE.getInteger());
					}else{
						CxScanManager.deleteCxScan(strCxSessionId, lCxRunID, lProjectId, lScanId, ProjectType.CX_SUITE.getInteger());
					}
					//delete the project in CxSAST
					CxScanManager.deleteCxProject(strCxSessionId, lCxAndroidProjectID, lProjectId, ProjectType.CX_SUITE.getInteger());
					
					MailUtil.sendEmail(PortalConstants.EMAIL_EVENT_SCAN_CANCELLED, lScanId, null);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.STOP_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.STOP_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_STOP_SCAN, params, nsse);
			throw new UBSPortalException(PortalErrors.STOP_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_STOP_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_STOP_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_STOP_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_STOP_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lScanId", lScanId);
				params.put("strCxSessionId", strCxSessionId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_STOP_SCAN, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean reexecuteScan (ActionRequest actionRequest) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		String strCxSessionId = null;
		Scan scan = null;
		Project project = null;
		boolean bGetProject = true;
		String strNewRunId = null;
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		long lPresetId = PortalConstants.LONG_ZERO;
		
		try {
			oUserId = session.getAttribute(PortalConstants.USER_ID);
			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			
			if (PortletCommonUtil.isDBConnected()) {
				if (oUserId != null) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
			
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					ProjectLocalServiceUtil.clearCache();
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						long[] userOrgIdList = user.getOrganizationIds();
						for (long userOrgId : userOrgIdList) {
							if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
								bIsProjectUser = true;
								break;
							}
						}
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
						}
					}
					
					if (lScanId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.retrieveScan(lScanId);
						
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							scan.setRegistrationDate(new Date());
							lProjectId = scan.getProjectId();
							
							if (lProjectId == PortalConstants.LONG_ZERO) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(lProjectId);
								
								if (CommonUtil.isObjectNull(project)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
									}
									lPresetId = project.getPresetId();
								}
							}
							
							if (scan.getStatus() != ScanStatus.SCAN_WAITING.getInteger() &&
									scan.getStatus() != ScanStatus.SCANNING.getInteger() &&
									scan.getStatus() != ScanStatus.FAILURE.getInteger()) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_CANNOT_BE_ACTION, PortalConstants.ERROR_LOG_PARAM_SCAN, PortalConstants.ERROR_LOG_ACTION_REEXECUTE));
								throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_SCAN_NOT_IN_WAITING_SCANNING_FAILURE_STATE, PortalMessages.SCAN_CANNOT_BE_REEXECUTED);
							}
						}
					}
				}

				if (session.getAttribute(PortalConstants.CXSESSION_ID) == null) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
					throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
				} else {
					strCxSessionId = session.getAttribute(PortalConstants.CXSESSION_ID).toString();
					
					if (CommonUtil.isStringNullOrEmpty(strCxSessionId)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
						throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
					}
				}
				strNewRunId = CxScanManager.reRunCxScan(scan, scan.getFilePath(), strCxSessionId, project.getProjectName(), lPresetId, ProjectType.CX_SUITE.getInteger(), project.getProjectId());
				
				if (!CommonUtil.isStringNullOrEmpty(strNewRunId)) {
					scan.setCxRunId(strNewRunId);
					scan.setStatus(ScanStatus.SCAN_WAITING.getInteger());
					scan.setModifiedDate(new Date());
					
					bSuccess = ScanLocalServiceUtil.updateScan(PortalConstants.USER_EVENT_REEXECUTE_SCAN, scan);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nsse);
			throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.REEXECUTE_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.REEXECUTE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_REEXECUTE_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_REEXECUTE_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_REEXECUTE_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_REEXECUTE_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lScanId", lScanId);
				params.put("strCxSessionId", strCxSessionId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_REEXECUTE_SCAN, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean requestReview (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		Scan scan = null;
		Project project = null;
		boolean bGetProject = true;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_NO_RIGHTS_ACTION);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					ProjectLocalServiceUtil.clearCache();
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						long[] userOrgIdList = user.getOrganizationIds();
						for (long userOrgId : userOrgIdList) {
							if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
								bIsProjectUser = true;
								break;
							}
						}
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS);
					}
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
						}
					}
					
					if (lScanId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.retrieveScan(lScanId);
						
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							lProjectId = scan.getProjectId();
							
							if (lProjectId == PortalConstants.LONG_ZERO) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(lProjectId);
								
								if (CommonUtil.isObjectNull(project)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
									}
								}
							}
							
							if (scan.getStatus() != ScanStatus.COMPLETE.getInteger()) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_CANNOT_BE_ACTION, PortalConstants.ERROR_LOG_PARAM_REVIEW, PortalConstants.ERROR_LOG_ACTION_REQUEST));
								throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_SCAN_NOT_IN_COMPLETE_STATE, PortalMessages.REVIEW_CANNOT_BE_REQUESTED);
							}
						}
					}
				}
				
				bSuccess = ScanLocalServiceUtil.requestReview(lScanId);
				
				if (bSuccess) {
					MailUtil.sendEmail(PortalConstants.EMAIL_EVENT_REQUEST_REVIEW, lScanId, null);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_REQUEST_REVIEW, params, nsse);
			throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REQUEST_REVIEW, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.REQUEST_REVIEW_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REQUEST_REVIEW, params, nspe);
				throw new UBSPortalException(PortalErrors.REQUEST_REVIEW_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_REQUEST_REVIEW, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_REQUEST_REVIEW, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_REQUEST_REVIEW, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_REQUEST_REVIEW, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lScanId", lScanId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_REQUEST_REVIEW, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean cancelReview (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Scan scan = null;
		boolean bGetProject = true;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					ProjectLocalServiceUtil.clearCache();
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						long[] userOrgIdList = user.getOrganizationIds();
						for (long userOrgId : userOrgIdList) {
							if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
								bIsProjectUser = true;
								break;
							}
						}
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
						}
					}
					
					if (lScanId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.retrieveScan(lScanId);
						
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							lProjectId = scan.getProjectId();
							
							if (lProjectId == PortalConstants.LONG_ZERO) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
								throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(lProjectId);
								
								if (CommonUtil.isObjectNull(project)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
									}
								}
							}
							
							if (scan.getStatus() != ScanStatus.UNDER_REVIEW.getInteger()) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_CANNOT_BE_ACTION, PortalConstants.ERROR_LOG_PARAM_REVIEW, PortalConstants.ERROR_LOG_ACTION_CANCEL));
								throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_SCAN_NOT_IN_REVIEW_STATE, PortalMessages.REVIEW_CANNOT_BE_CANCELLED);
							}
						}
					}
				}
				
				bSuccess = ScanLocalServiceUtil.cancelReview(lScanId);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_CANCEL_REVIEW, params, nsse);
			throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_CANCEL_REVIEW, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.CANCEL_REVIEW_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_CANCEL_REVIEW, params, nspe);
				throw new UBSPortalException(PortalErrors.CANCEL_REVIEW_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_CANCEL_REVIEW, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_CANCEL_REVIEW, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_CANCEL_REVIEW, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_CANCEL_REVIEW, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lScanId", lScanId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_CANCEL_REVIEW, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean regenerateReport (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		String strCxSessionId = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Scan scan = null;
		boolean bGetProject = true;
		long lCxAndroidScanId = PortalConstants.LONG_ZERO;
		long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession(false);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					ProjectLocalServiceUtil.clearCache();
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						long[] userOrgIdList = user.getOrganizationIds();
						for (long userOrgId : userOrgIdList) {
							if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
								bIsProjectUser = true;
								break;
							}
						}
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
						}
					}
					
					if (lScanId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.retrieveScan(lScanId);
						
						@SuppressWarnings("unchecked")
						List<Long> scansRegeneratedList = (List<Long>) pSession.getAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT);
						
						if (CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
							scansRegeneratedList = new ArrayList<Long>();
						}
						
						scansRegeneratedList.add(lScanId);
						
						pSession.setAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT, scansRegeneratedList);
						
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							lProjectId = scan.getProjectId();
							
							if (lProjectId == PortalConstants.LONG_ZERO) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(lProjectId);
								
								if (CommonUtil.isObjectNull(project)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
									}
									
									if (!CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId())) {
										lCxAndroidProjectId = Long.parseLong(project.getCxAndroidProjectId());
									}
								}
							}
							
							int iStatus = ScanLocalServiceUtil.getScanStatus(lScanId);
							
							if (iStatus != ScanStatus.UNDER_REVIEW.getInteger() &&
									iStatus != ScanStatus.COMPLETE.getInteger() &&
									iStatus != ScanStatus.FAILURE.getInteger()) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_CANNOT_BE_ACTION, PortalConstants.ERROR_LOG_PARAM_REPORT, PortalConstants.ERROR_LOG_ACTION_REGENERATE));
								throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_SCAN_NOT_IN_REVIEW_COMPLETE_STATE, PortalMessages.REPORT_CANNOT_BE_REGENERATED);
							}
							
							if (!CommonUtil.isStringNullOrEmpty(scan.getCxAndroidScanId())) {
								lCxAndroidScanId = Long.parseLong(scan.getCxAndroidScanId());
							}
						
						}
					}
				}
				
				if (session.getAttribute(PortalConstants.CXSESSION_ID) == null) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
					throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
				} else {
					strCxSessionId = session.getAttribute(PortalConstants.CXSESSION_ID).toString();
				
					if (CommonUtil.isStringNullOrEmpty(strCxSessionId)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
						throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
					}
				}
				
				ResultItem item = new ResultItem();
				
				item.setScanId(lScanId);
				item.setCxAndroidProjectId(String.valueOf(lCxAndroidProjectId));
				item.setCxAndroidScanId(String.valueOf(lCxAndroidScanId));
				item.setProjectId(lProjectId);
				item.setType(ProjectType.CX_SUITE.getInteger());
				
				CxScanReport.reGenerateCxReport(item, strCxSessionId, ProjectType.CX_SUITE.getInteger());
				
				bSuccess = true;
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_REGENERATE_REPORT, params, nsse);
			throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REGENERATE_REPORT, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.REGENERATE_REPORT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_REGENERATE_REPORT, params, nspe);
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_REGENERATE_REPORT, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_REGENERATE_REPORT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCxSessionId", strCxSessionId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_REGENERATE_REPORT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_REGENERATE_REPORT, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("lScanId", lScanId);
				params.put("strCxSessionId", strCxSessionId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_REGENERATE_REPORT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean deleteScan (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lScanId = PortalConstants.LONG_ZERO;
		String strCxSessionId = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Scan scan = null;
		boolean bGetProject = true;
		long lCxAndroidScanId = PortalConstants.LONG_ZERO;
		String strRunId = null;
		int scanStatus = PortalConstants.INT_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		boolean isDeletedInCx = false;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				oUserId = session.getAttribute(PortalConstants.USER_ID);
				oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					
					if (lUserId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
						throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
					} else {
						user = UserLocalServiceUtil.getUser(lUserId);
						
						if (CommonUtil.isObjectNull(user)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
							throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
						}
					}
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				lScanId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID);
				lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				} else {
					List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
					
					boolean bIsProjectUser = false;
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						bIsProjectUser = true;
					} else {
						if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
							for (ProjectUsers pu : projectUsersList) {
								if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
									bIsProjectUser = true;
									break;
								}
							}
						}
					}
					
					if (!bIsProjectUser) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
						throw new UBSPortalException(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
					}
					
					project = ProjectLocalServiceUtil.getProject(lProjectId);
					
					if (CommonUtil.isObjectNull(project)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
						throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
					} else {
						if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
							throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
						}
					}
					
					if (lScanId == PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.DELETE_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					} else {
						scan = ScanLocalServiceUtil.retrieveScan(lScanId);
						
						if (CommonUtil.isObjectNull(scan)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
							throw new UBSPortalException(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
						} else {
							lProjectId = scan.getProjectId();
							
							if (lProjectId == PortalConstants.LONG_ZERO) {
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.DELETE_SCAN_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							} else {
								project = null;
								bGetProject = false;
								project = ProjectLocalServiceUtil.getProject(lProjectId);
								
								if (CommonUtil.isObjectNull(project)) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
									throw new UBSPortalException(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
								} else {
									if (project.getType() != ProjectType.CX_SUITE.getInteger()) {
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
										throw new UBSPortalException(PortalErrors.DELETE_SCAN_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_CX);
									}
								}
							}
							
							
						}
					}
				}
				
				if (session.getAttribute(PortalConstants.CXSESSION_ID) == null) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
					throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
				} else {
					Object oSessionId = session.getAttribute(PortalConstants.CXSESSION_ID);
					
					if (!CommonUtil.isObjectNull(oSessionId)) {
						strCxSessionId = oSessionId.toString();
					}
					
					if (CommonUtil.isStringNullOrEmpty(strCxSessionId)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
						throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID, PortalMessages.CX_SESSION_ID_INVALID);
					}
									
					scanStatus = ScanLocalServiceUtil.getScanStatus(lScanId);
					
					if (scanStatus == ScanStatus.SCAN_WAITING.getInteger()
							|| scanStatus == ScanStatus.SCANNING.getInteger()
							) {
						strRunId = scan.getCxRunId();
						CxScanManager.cancelCxScan(strCxSessionId, strRunId, ProjectType.CX_SUITE.getInteger());
						bSuccess = true;
					} else if (scanStatus == ScanStatus.FAILURE.getInteger()) {
						//nothing to cancel
						bSuccess = true;
					} else {
						if (!CommonUtil.isStringNullOrEmpty(scan.getCxAndroidScanId())) {
							lCxAndroidScanId = Long.parseLong(scan.getCxAndroidScanId());
							if (lCxAndroidScanId > PortalConstants.LONG_ZERO) {
								isDeletedInCx = ScanLocalServiceUtil.isDeletedScanInCxServer(lScanId);
								if (!isDeletedInCx) {
									scan.setIsDeletedInCx(PortalConstants.DELETED_FLAG);
									ScanLocalServiceUtil.updateScan(scan);
								}
							}
						}
						bSuccess = true;
					}
				}
				
				if (!bSuccess) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_DELETION_FAILED);
					throw new UBSPortalException(PortalErrors.DELETE_SCAN_FAILED, PortalMessages.DELETE_SCAN_FAILED);
				} else {
					bSuccess = ScanLocalServiceUtil.deleteScan(ProjectType.CX_SUITE.getInteger(), lScanId);
					Long lCxAndroidProjectId = Long.parseLong(scan.getCxAndroidProjectId());
					Long lCxRunID = Long.parseLong(scan.getCxRunId());
					
					//delete the scan in CxSAST
					if(PortalConstants.LONG_ZERO < lCxAndroidScanId){
						CxScanManager.deleteCxScan(strCxSessionId, lCxAndroidScanId, lProjectId, lScanId, ProjectType.CX_SUITE.getInteger());
					}else{
						CxScanManager.deleteCxScan(strCxSessionId, lCxRunID, lProjectId, lScanId, ProjectType.CX_SUITE.getInteger());
					}
					
					//delete the project in CxSAST
					CxScanManager.deleteCxProject(strCxSessionId, lCxAndroidProjectId, lProjectId, ProjectType.CX_SUITE.getInteger());
					
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchScanException nsse) {
			params.put("lScanId", lScanId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
			log.debug(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params, nsse);
			throw new UBSPortalException(PortalErrors.DELETE_SCAN_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST, nsse);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			
			if (bGetProject) {
				log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			} else {
				log.debug(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_SCAN, params, nspe);
				throw new UBSPortalException(PortalErrors.DELETE_SCAN_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_STOP_SCAN, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
		} catch (SystemException se) {
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			params.put("strCxSessionId", strCxSessionId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_DELETE_SCAN, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			params.put("strCxSessionId", strCxSessionId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_DELETE_SCAN, params, pe);
				throw new UBSPortalException(pe.getMessage(), message);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_DELETE_SCAN, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lScanId", lScanId);
				params.put("lProjectId", lProjectId);
				params.put("strCxSessionId", strCxSessionId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_DELETE_SCAN, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
	
		return bSuccess;
	}
	
//	public static JSONArray checkCxScanStatus(String strScanIds, String strScanDates) throws UBSPortalException {
//		Map<String, Object> params = new HashMap<String, Object>();
//		String[] scanIdArray = null;
//		String[] scanDateArray = null;
//		int iScanStatus = PortalConstants.INT_ZERO;
//		Date scanRegistrationDate = null;
//		int iScanWaiting = PortalConstants.INT_ZERO;
//		JSONArray statusJson = null;
//		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
//		
//		try {
//			if (PortletCommonUtil.isDBConnected()) {
//				if (strScanIds.length() > 0) {
//					scanIdArray = strScanIds.split(PortalConstants.STR_SPACE);
//					scanDateArray = strScanDates.split(PortalConstants.STR_SPACE);
//					
//					if (scanIdArray.length == scanDateArray.length) {
//						for (int i = 0; i < scanIdArray.length; i++) {
//							iScanStatus = ScanLocalServiceUtil.getScanStatus(Long.valueOf(scanIdArray[i]));
//							statusJson = JSONFactoryUtil.createJSONArray();
//							
//							if (CommonUtil.isOutOfRange(iScanStatus, 1, PortalConstants.SCAN_STATUS_LIMIT)) {
//								params.put("iScanStatus", iScanStatus);
//								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_STATUS));
//								throw new UBSPortalException(PortalErrors.INVALID_SCAN_STATUS, PortalMessages.SCAN_STATUS_INVALID);
//							}
//							if (iScanStatus == ScanStatus.SCAN_WAITING.getInteger()) {
//								scanRegistrationDate = new Date();
//								scanRegistrationDate.setTime(Long.valueOf(scanDateArray[i]));
//								iScanWaiting = ScanLocalServiceUtil.countScanWaiting(scanRegistrationDate, ProjectType.CX_SUITE.getInteger());
//							}
//							
//							statusJson.put(iScanStatus);
//							statusJson.put(iScanWaiting);
//							jsonArray.put(statusJson);
//						}
//					}
//				}
//			} else {
//				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
//			}
//		} catch (UBSPortalException e) {
//			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
//				params = null;
//			} else {
//				params.put("strScanIdArray", scanIdArray);
//			}
//			log.debug(e.getErrorCode(), PortalConstants.METHOD_CHECK_CX_SCAN_STATUS, params, e);
//			throw e;
//		} catch (IllegalArgumentException e) {
//			params.put("strScanIdArray", scanIdArray);
//			log.debug(PortalErrors.INVALID_SCAN_ID, PortalConstants.METHOD_CHECK_CX_SCAN_STATUS, params, e);
//			throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID, PortalMessages.SCAN_ID_INVALID, e);
//		} catch (Exception e) {
//			params.put("strScanIdArray", scanIdArray);
//			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_CHECK_CX_SCAN_STATUS, params, e);
//			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
//		} finally {
//			scanIdArray = null;
//			scanDateArray = null;
//			scanRegistrationDate = null;
//			statusJson = null;
//			if (!CommonUtil.isMapNullOrEmpty(params)) {
//				params.clear();
//				params = null;
//			}
//		}
//		
//		return jsonArray;
//	}

	@SuppressWarnings({ "deprecation" })
	private static List<ScanItem> getScans(ResourceRequest resourceRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		long lProjectId = PortalConstants.LONG_ZERO;
		String strScanId = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String strStatus = null;
		String strProcess = null;
		String strCxScanId = null;
		String strOrderByCol = null;
		String strOrderByType = null;
		DateFormat formatter = null;
		Date dteRegDateLow = null;
		Date dteRegDateHigh = null;
		List<Object> scanList = null;
		List<ScanItem> returnList = null;
		int start = PortalConstants.INT_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Map<String, Object> searchedScan = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				start = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_START);
				lProjectId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_PROJECT_ID);
				strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID).trim();
				strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME).trim();
				strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE).trim();
				strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
				strScanRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
				strScanRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
				strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
				strProcess = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROCESS);
				strCxScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_CX_SCAN_ID).trim();
				strOrderByCol = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_ORDER_BY_COL).trim();
				strOrderByType = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_ORDER_BY_TYPE).trim();
				
				if (lProjectId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
					try {
						long lScanId = Long.parseLong(strScanId);
						
						if (lScanId < PortalConstants.LONG_ZERO) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
					if (!Validator.isAlphanumericName(strHashValue)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
					
					if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
					if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
					}
					
					dteRegDateLow = formatter.parse(strScanRegDateLow);
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
					if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
					}
					
					dteRegDateHigh = formatter.parse(strScanRegDateHigh);
					dteRegDateHigh.setHours(23);
					dteRegDateHigh.setMinutes(59);
					dteRegDateHigh.setSeconds(59);
				}
				
				if (dteRegDateLow != null && dteRegDateHigh != null) {
					if (dteRegDateLow.after(dteRegDateHigh)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE.toLowerCase()));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
					try {
						long lCxScanId = Long.parseLong(strCxScanId);
						
						if (lCxScanId < PortalConstants.LONG_ZERO) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
					}
				}
				
				Object oUserId = session.getAttribute(PortalConstants.USER_ID);
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
				}
				
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}
				user = UserLocalServiceUtil.getUser(lUserId);
				
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
				searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
				searchedScan.put(PortalConstants.PARAM_PROCESS, strProcess);
				searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
				
				scanList = ScanLocalServiceUtil.sortScans(lProjectId, searchedScan, strOrderByCol, strOrderByType, start);
				
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
					returnList = new ArrayList<ScanItem>();
					long lCompanyId = user.getCompanyId();
					
					for (Object item : scanList) {
						ScanItem scan = (ScanItem) item;
						
						try {
							user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
							
							if (CommonUtil.isObjectNull(user)) {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							} else {
								scan.setScanManager(user.getFirstName());
							}
						} catch (NoSuchUserException e) {
							scan.setScanManager(PortalConstants.STRING_EMPTY);
						}
						
						returnList.add(scan);
					}
				}
			} else {
				throw new UBSPortalException (PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchUserException nsue) {
			params.put("lUserId", lUserId);
			params.put("user", user);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_GET_SCANS, params, nsue);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
		} catch (ParseException pe) {
			params.put("strRegDateLow", strScanRegDateLow);
			params.put("strRegDateHigh", strScanRegDateHigh);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
			log.debug(PortalErrors.INVALID_SCAN_REGISTRATION_DATE, PortalConstants.METHOD_GET_SCANS, params, pe);
			throw new UBSPortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE, PortalMessages.SCAN_REGISTRATION_DATE_INVALID, pe);
		} catch (SystemException se) {
			params.put("searchedScan", searchedScan);
			params.put("lUserId", lUserId);
			params.put("user", user);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (PortalException pe) {
			params.put("searchedScan", searchedScan);
			params.put("lUserId", lUserId);
			params.put("user", user);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_SCANS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (NumberFormatException nfe) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.GET_USER_USER_ID_INVALID, PortalConstants.METHOD_GET_SCANS, params, nfe);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.COMMON_EXCEPTION, nfe);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("searchedScan", searchedScan);
				params.put("lUserId", lUserId);
				params.put("user", user);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_SCANS, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_SCANS, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
		
		return returnList;
	}
	
	public static List<ResultItem> getEntireScans (ResourceRequest resourceRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = null;
		List<ResultItem> returnList = null;
		String strScanId = null;
		String strProjectName = null;
		String strGroupName = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String strStatus = null;
		String strProcess = null;
		String strCxScanId = null;
		String strOrderByCol = null;
		String strOrderByType = null;
		DateFormat formatter = null;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		int iStart = PortalConstants.INT_ZERO;
		Map<String, Object> searchedScan = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				iStart = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_START);
				strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID).trim();
				strProjectName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
				strGroupName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_OWNER_GROUP).trim();
				strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME).trim();
				strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE).trim();
				strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
				strScanRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
				strScanRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
				strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
				strProcess = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROCESS);
				strCxScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_CX_SCAN_ID).trim();
				strOrderByCol = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_ORDER_BY_COL).trim();
				strOrderByType = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_ORDER_BY_TYPE).trim();
				
				if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
					try {
						long lScanId = Long.parseLong(strScanId);
						
						if (lScanId < PortalConstants.LONG_ZERO) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
					if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
					}
					
					if (!ControllerHelper.isCxProjectNameValid(strProjectName)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_INVALID, PortalMessages.PROJECT_NAME_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
					if (CommonUtil.getStringBytes(strGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_GROUP_NAME_TOO_LONG, PortalMessages.GROUP_NAME_TOO_LONG);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
					if (!Validator.isAlphanumericName(strHashValue)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
					
					if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
				}
				
				try {
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
						if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateLow = formatter.parse(strScanRegDateLow);
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
						if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateHigh = Calendar.getInstance();
						dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
						dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteRegDateHigh.set(Calendar.MINUTE, 59);
						dteRegDateHigh.set(Calendar.SECOND, 59);
					}
					
					if (!CommonUtil.isObjectNull(dteRegDateLow)
							&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
						if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE.toLowerCase()));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
					}
				} catch (ParseException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
					try {
						long lCxScanId = Long.parseLong(strCxScanId);
						
						if (lCxScanId < PortalConstants.LONG_ZERO) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
					}
				}
				
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
				searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
				searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
				searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
				searchedScan.put(PortalConstants.PARAM_PROCESS, strProcess);
				searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
				
				scanList = ScanLocalServiceUtil.sortEntireScans(ProjectType.CX_SUITE.getInteger(), searchedScan, strOrderByCol, strOrderByType, iStart);
				
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
					returnList = new ArrayList<ResultItem>();
					List<User> userList = UserLocalServiceUtil.getUsers(PortalConstants.INT_ZERO, PortalConstants.INT_ONE);
					long lCompanyId = PortalConstants.LONG_ZERO;
					
					if (!CommonUtil.isListNullOrEmpty(userList)) {
						for (User user : userList) {
							lCompanyId = user.getCompanyId();
							break;
						}
						
						for (Object item : scanList) {
							ResultItem scan = (ResultItem) item;
							User user = null;
							
							try {
								user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
								
								if (CommonUtil.isObjectNull(user)) {
									scan.setScanManager(PortalConstants.STRING_EMPTY);
								} else {
									scan.setScanManager(user.getFirstName());
								}
							} catch (NoSuchUserException e) {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							}
							
							returnList.add(scan);
						}
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException pe) {
			params.put("searchedScan", searchedScan);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("searchedScan", searchedScan);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, ubspe);
			}
			
			throw ubspe;
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
		
		return returnList;
	}
	
	public static void getScansForRefresh(ResourceRequest resourceRequest, ResourceResponse resourceResponse, PortletConfig portletConfig) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<ScanItem> scanList = null;
		Project project = null;
		String strScanId = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String strHashValue = PortalConstants.STRING_EMPTY;
		String strScanManager = PortalConstants.STRING_EMPTY;
		String strRegDateLow = PortalConstants.STRING_EMPTY;
		String strRegDateHigh = PortalConstants.STRING_EMPTY;
		String strStatus = PortalConstants.STRING_EMPTY;
		String strProcess = PortalConstants.STRING_EMPTY;
		String strCxScanId = PortalConstants.STRING_EMPTY;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		PortletSession pSession = resourceRequest.getPortletSession(false);
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		boolean bHasError = false;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isObjectNull(oLoggedIn) && Boolean.parseBoolean(oLoggedIn.toString())) {
					project = CxProjectMgmtController.getProject(resourceRequest);
					scanList = CxScanMgmtController.getScans(resourceRequest);
				
					strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID);
					strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME);
					strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE);
					strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER);
					strRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW);
					strRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH);
					strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
					strProcess = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROCESS);
					strCxScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_CX_SCAN_ID);
					
					Map<String, Object> searchedScan = new HashMap<String, Object>();
					searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
					searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strRegDateLow);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strRegDateHigh);
					searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
					searchedScan.put(PortalConstants.PARAM_PROCESS, strProcess);
					searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
					
					@SuppressWarnings("unchecked")
					List<Long> scansRegeneratedList = (List<Long>) pSession.getAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT);
					
					if (!CommonUtil.isListNullOrEmpty(scanList)) {
						for (ScanItem scan : scanList) {
							String errorMessage = scan.getFailedScanCause();
							long lScanId = scan.getScanId();
							int iStatus = scan.getScanStatus();
							
							if (!CommonUtil.isStringNullOrEmpty(errorMessage)
									&& iStatus == ScanStatus.FAILURE.getInteger()) {
								
								if (!CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
									Iterator<Long> it = scansRegeneratedList.iterator();
									
									while (it.hasNext()) {
										if (it.next().longValue() == lScanId) {
											it.remove();
											if (!bHasError) {
												resourceRequest.setAttribute(PortalConstants.PARAM_CX_DOES_NOT_EXIST_ERROR_MSG, errorMessage);
												bHasError = true;
											}
											break;
										}
									}
								}
							} else if (iStatus == ScanStatus.COMPLETE.getInteger()) {
								if (!CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
									Iterator<Long> it = scansRegeneratedList.iterator();
									
									while (it.hasNext()) {
										if (it.next().longValue() == lScanId) {
											it.remove();
											break;
										}
									}
								}
							}
						}
						
						pSession.setAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT, scansRegeneratedList);
					}
					
					resourceRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
					resourceRequest.setAttribute(PortalConstants.PARAM_SCAN_LIST, scanList);
					resourceRequest.setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
					resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_TEXT_HTML);
	
					portletConfig.getPortletContext().getRequestDispatcher(PortalConstants.CX_SCAN_LIST_REFRESH_JSP).include(resourceRequest, resourceResponse);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortletException e) {
			params.put("portletConfig", portletConfig);
			log.debug(PortalErrors.PORTLET_EXCEPTION, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.PORTLET_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (IOException e) {
			params.put("portletConfig", portletConfig);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (UBSPortalException e) {
			String strErrorCode = e.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("project", project);
				params.put("scanList", scanList);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_SCANS_FOR_REFRESH, params, e);
			}
			throw e;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
	}
	
	public static void getEntireScansForRefresh(ResourceRequest resourceRequest, ResourceResponse resourceResponse, PortletConfig portletConfig) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<ResultItem> entireScanList = null;
		String strScanId = PortalConstants.STRING_EMPTY;
		String strProjectName = PortalConstants.STRING_EMPTY;
		String strGroupName = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String strHashValue = PortalConstants.STRING_EMPTY;
		String strScanManager = PortalConstants.STRING_EMPTY;
		String strRegDateLow = PortalConstants.STRING_EMPTY;
		String strRegDateHigh = PortalConstants.STRING_EMPTY;
		String strStatus = PortalConstants.STRING_EMPTY;
		String strProcess = PortalConstants.STRING_EMPTY;
		String strCxScanId = PortalConstants.STRING_EMPTY;
		PortletSession pSession = resourceRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		boolean bHasError = false;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isObjectNull(oLoggedIn) && Boolean.parseBoolean(oLoggedIn.toString())) {
					entireScanList = CxScanMgmtController.getEntireScans(resourceRequest);
					
					strScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_ID);
					strProjectName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROJECT_NAME);
					strGroupName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_OWNER_GROUP);
					strFileName = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_FILE_NAME);
					strHashValue = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_HASH_VALUE);
					strScanManager = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_SCAN_MANAGER);
					strRegDateLow = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_LOW);
					strRegDateHigh = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_REG_DATE_HIGH);
					strStatus = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_STATUS);
					strProcess = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_PROCESS);
					strCxScanId = ParamUtil.getString(resourceRequest, PortalConstants.PARAM_CX_SCAN_ID);
					
					Map<String, Object> searchedScan = new HashMap<String, Object>();
					searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
					searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strRegDateLow);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strRegDateHigh);
					searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
					searchedScan.put(PortalConstants.PARAM_PROCESS, strProcess);
					searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
					
					@SuppressWarnings("unchecked")
					List<Long> scansRegeneratedList = (List<Long>) pSession.getAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT);
					
					if (!CommonUtil.isListNullOrEmpty(entireScanList)) {
						for (ResultItem scan : entireScanList) {
							String errorMessage = scan.getFailedScanCause();
							long lScanId = scan.getScanId();
							int iStatus = scan.getStatus();
							
							if (!CommonUtil.isStringNullOrEmpty(errorMessage)
									&& iStatus == ScanStatus.FAILURE.getInteger()) {
								
								 if (!CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
									 Iterator<Long> it = scansRegeneratedList.iterator();
									 
									 while (it.hasNext()) {
										 if (it.next().longValue() == lScanId) {
											 it.remove();
											 if (!bHasError) {
												 resourceRequest.setAttribute(PortalConstants.PARAM_CX_DOES_NOT_EXIST_ERROR_MSG, errorMessage);
												 bHasError = true;
											 }
											 break;
										 }
									 }
								 }
							} else if (iStatus == ScanStatus.COMPLETE.getInteger()) {
								if (!CommonUtil.isListNullOrEmpty(scansRegeneratedList)) {
									Iterator<Long> it = scansRegeneratedList.iterator();
									 
									 while (it.hasNext()) {
										 if (it.next().longValue() == lScanId) {
											 it.remove();
											 break;
										 }
									 }
								}
							}
						}
						
						pSession.setAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT, scansRegeneratedList);
					}
					
					resourceRequest.setAttribute(PortalConstants.PARAM_SCAN_LIST, entireScanList);
					resourceRequest.setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
					resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_TEXT_HTML);
					
					portletConfig.getPortletContext().getRequestDispatcher(PortalConstants.CX_ENTIRE_SCAN_LIST_REFRESH_JSP).include(resourceRequest, resourceResponse);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortletException e) {
			params.put("resourceRequest", resourceRequest);
			params.put("resourceResponse", resourceResponse);
			log.debug(PortalErrors.PORTLET_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.PORTLET_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (IOException e) {
			params.put("resourceRequest", resourceRequest);
			params.put("resourceResponse", resourceResponse);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("resourceRequest", resourceRequest);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS_FOR_REFRESH, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(entireScanList)) {
				entireScanList = null;
			}
		}
	}
}