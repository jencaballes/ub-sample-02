package jp.ubsecure.portal.jubjub.portlet.service.cxsuite;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ReportType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoapProxy;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSCreateReportResponse;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSReportRequest;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSReportType;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;

public class CxScanReportUpdater{
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(CxScanReportUpdater.class);
	
	private String m_strSessionId;
	private long m_lScanId;
	private long m_lCxScanId; 
	private long m_lProjectId;
	private File pdfReportFile;
	private Report m_pdfReport;
	
	public void  init(String pstrSessionId, long plScanId, long plCxScanId, long lProjectId, boolean bRegenerate) {
		this.m_strSessionId = pstrSessionId;
		this.m_lScanId = plScanId;
		this.m_lCxScanId = plCxScanId;
		this.m_lProjectId = lProjectId;
	}	
	
	public void generatePDF () throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		CxWSReportRequest reportRequest = null;
		String strRegistrationDate = null;
		Date dteRegistrationDate = null;
		
		try {
			dteRegistrationDate = ScanLocalServiceUtil.getScanRegistrationDate(this.m_lScanId);
			if (CommonUtil.isObjectNull(dteRegistrationDate)) {
				params.put("scanRegistrationDate", dteRegistrationDate);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
			}
			strRegistrationDate = DateUtil.getDate(dteRegistrationDate, PortalConstants.REPORT_DATE_FORMAT, Locale.JAPAN);
			//add report
			this.m_pdfReport = ReportLocalServiceUtil.addReport(this.m_lScanId, ReportType.PDF_REPORT.getInteger());
						
			if (this.m_pdfReport != null) {
				reportRequest = new CxWSReportRequest();
				reportRequest.setScanID(this.m_lCxScanId);
				reportRequest.setType(CxWSReportType.PDF);
				
				if (CommonUtil.isStringNullOrEmpty(this.m_strSessionId)) {
					params.put("sessionId", this.m_strSessionId);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
					throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID);
				}
				
				if (CommonUtil.isStringNullOrEmpty(strRegistrationDate)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
					throw new UBSPortalException(PortalErrors.INVALID_REGISTRATION_DATE);
				}
				
				String strReportDirectory = FileProcessUtil.createUploadDirectory(this.m_lProjectId, this.m_lScanId);
				if (CommonUtil.isStringNullOrEmpty(strReportDirectory)) {
					params.put("strReportDirectory", strReportDirectory);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
					throw new UBSPortalException(PortalErrors.INVALID_FILE_PATH);
				}
				if (!CommonUtil.canWrite(strReportDirectory)) {
					params.put("strReportDirectory", strReportDirectory);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
					ScanLocalServiceUtil.updateAndroidScanStatus(this.m_lScanId, PortalConstants.FAIL);
					throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
				}
		
				CxSDKWebServiceSoapProxy cxSDKWebServiceSoapProxy = CxScanManager.getSoapProxy();
				
				log.info(PortalConstants.API_FUNCTION_CREATE_SCAN_REPORT, PortalConstants.BEFORE + PortalConstants.API_CALL_REPORT_GENERATION);
				CxWSCreateReportResponse cxWSCreateReportResponse = cxSDKWebServiceSoapProxy.createScanReport(this.m_strSessionId, reportRequest);
				log.info(PortalConstants.API_FUNCTION_CREATE_SCAN_REPORT, PortalConstants.AFTER + PortalConstants.API_CALL_REPORT_GENERATION);
				
				if (CommonUtil.isObjectNull(cxWSCreateReportResponse)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
					throw new UBSPortalException(PortalErrors.CX_SCAN_REPORT_ERROR);
				}
				
				if (!cxWSCreateReportResponse.isIsSuccesfull()) {
					String errorMessage = cxWSCreateReportResponse.getErrorMessage();
					
					if (CommonUtil.isStringNullOrEmpty(errorMessage)) {
						errorMessage = PortalErrors.CX_PROCESSING_EXCEPTION;
					}
					
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_WEB_SERVICE_REGENERATE_REPORT_FAILED);
					throw new UBSPortalException(PortalErrors.CX_SCAN_REPORT_ERROR, errorMessage);
				}
				
				Report report = ReportLocalServiceUtil.getReport(this.m_pdfReport.getReportId());
				report.setCxReportId(cxWSCreateReportResponse.getID());
				ReportLocalServiceUtil.updateReport(report);
				
				if (CommonUtil.isValidAndExistingFile(pdfReportFile)) {
					this.m_pdfReport.setReportName(PortalConstants.CX_FOLDER + strRegistrationDate + File.separator + pdfReportFile.getName());
				}
			}
		} catch (PortalException e) {
			params.put("strRegistrationDate", strRegistrationDate);
			log.debug(PortalErrors.CX_SCAN_REPORT_ERROR, PortalConstants.METHOD_GENERATE_PDF, params, e);
			throw new UBSPortalException(PortalErrors.CX_SCAN_REPORT_ERROR, PortalErrors.CX_PROCESSING_EXCEPTION);
		} catch (UBSPortalException e) {
			String strErrorMessage = e.getErrorMessage();
			
			if (strErrorMessage.equals(PortalErrors.CX_PROCESSING_EXCEPTION)) {
				strErrorMessage = PortalConstants.CX_REGENERATE_REPORT_FAILED;
			}
			
			updateScanFailedStatus(strErrorMessage);
			
			params.put("dteRegistrationDate", dteRegistrationDate);
			params.put("strRegistrationDate", strRegistrationDate);
			params.put("reportRequest", reportRequest);
			log.debug(PortalErrors.CX_SCAN_REPORT_ERROR, PortalConstants.METHOD_GENERATE_PDF, params, e);
			throw new UBSPortalException(PortalErrors.CX_SCAN_REPORT_ERROR, strErrorMessage);	 
		} catch (Exception e) {
			params.put("lScanId", this.m_lScanId);
			params.put("strRegistrationDate", strRegistrationDate);
			params.put("dteRegistrationDate", dteRegistrationDate);
			params.put("reportRequest", reportRequest);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_PDF, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION);
		} finally {
			params.clear();
			params = null;
			strRegistrationDate = null;
			this.m_pdfReport = null;
			reportRequest = null;
			dteRegistrationDate = null;
		}
	}
	
	private void updateScanFailedStatus (String errorMessage) {
		Scan scan = null;
		try {
			scan = ScanLocalServiceUtil.getScan(this.m_lScanId);
			scan.setFailedScanCause(errorMessage);
			if (errorMessage.toLowerCase().contains(PortalConstants.CX_CANNOT_RETRIEVE_SCAN_DETAILS_ERROR_MSG_EN.toLowerCase())
					|| errorMessage.contains(PortalConstants.CX_CANNOT_RETRIEVE_SCAN_DETAILS_ERROR_MSG_JP)) {
				scan.setIsDeletedInCx(PortalConstants.DELETED_FLAG);
			}
			
			scan.setStatus(ScanStatus.FAILURE.getInteger());
			scan.setModifiedDate(new Date());
			ScanLocalServiceUtil.updateScan(scan);
		} catch (PortalException pe) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, "generate", null, pe);
		} catch (SystemException se) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, "generate", null, se);
		}
	}
}
