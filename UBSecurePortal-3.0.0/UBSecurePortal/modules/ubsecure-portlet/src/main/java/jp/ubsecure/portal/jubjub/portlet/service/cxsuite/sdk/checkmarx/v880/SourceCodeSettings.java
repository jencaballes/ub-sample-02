/**
 * SourceCodeSettings.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class SourceCodeSettings  implements java.io.Serializable {
    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceLocationType sourceOrigin;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials userCredentials;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScanPath[] pathList;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceControlSettings sourceControlSetting;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.LocalCodeContainer packagedCode;

    private java.lang.String sourcePullingAction;

    private long sourceControlCommandId;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceFilterPatterns sourceFilterLists;

    public SourceCodeSettings() {
    }

    public SourceCodeSettings(
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceLocationType sourceOrigin,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials userCredentials,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScanPath[] pathList,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceControlSettings sourceControlSetting,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.LocalCodeContainer packagedCode,
           java.lang.String sourcePullingAction,
           long sourceControlCommandId,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceFilterPatterns sourceFilterLists) {
           this.sourceOrigin = sourceOrigin;
           this.userCredentials = userCredentials;
           this.pathList = pathList;
           this.sourceControlSetting = sourceControlSetting;
           this.packagedCode = packagedCode;
           this.sourcePullingAction = sourcePullingAction;
           this.sourceControlCommandId = sourceControlCommandId;
           this.sourceFilterLists = sourceFilterLists;
    }


    /**
     * Gets the sourceOrigin value for this SourceCodeSettings.
     * 
     * @return sourceOrigin
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceLocationType getSourceOrigin() {
        return sourceOrigin;
    }


    /**
     * Sets the sourceOrigin value for this SourceCodeSettings.
     * 
     * @param sourceOrigin
     */
    public void setSourceOrigin(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceLocationType sourceOrigin) {
        this.sourceOrigin = sourceOrigin;
    }


    /**
     * Gets the userCredentials value for this SourceCodeSettings.
     * 
     * @return userCredentials
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials getUserCredentials() {
        return userCredentials;
    }


    /**
     * Sets the userCredentials value for this SourceCodeSettings.
     * 
     * @param userCredentials
     */
    public void setUserCredentials(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials userCredentials) {
        this.userCredentials = userCredentials;
    }


    /**
     * Gets the pathList value for this SourceCodeSettings.
     * 
     * @return pathList
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScanPath[] getPathList() {
        return pathList;
    }


    /**
     * Sets the pathList value for this SourceCodeSettings.
     * 
     * @param pathList
     */
    public void setPathList(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScanPath[] pathList) {
        this.pathList = pathList;
    }


    /**
     * Gets the sourceControlSetting value for this SourceCodeSettings.
     * 
     * @return sourceControlSetting
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceControlSettings getSourceControlSetting() {
        return sourceControlSetting;
    }


    /**
     * Sets the sourceControlSetting value for this SourceCodeSettings.
     * 
     * @param sourceControlSetting
     */
    public void setSourceControlSetting(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceControlSettings sourceControlSetting) {
        this.sourceControlSetting = sourceControlSetting;
    }


    /**
     * Gets the packagedCode value for this SourceCodeSettings.
     * 
     * @return packagedCode
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.LocalCodeContainer getPackagedCode() {
        return packagedCode;
    }


    /**
     * Sets the packagedCode value for this SourceCodeSettings.
     * 
     * @param packagedCode
     */
    public void setPackagedCode(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.LocalCodeContainer packagedCode) {
        this.packagedCode = packagedCode;
    }


    /**
     * Gets the sourcePullingAction value for this SourceCodeSettings.
     * 
     * @return sourcePullingAction
     */
    public java.lang.String getSourcePullingAction() {
        return sourcePullingAction;
    }


    /**
     * Sets the sourcePullingAction value for this SourceCodeSettings.
     * 
     * @param sourcePullingAction
     */
    public void setSourcePullingAction(java.lang.String sourcePullingAction) {
        this.sourcePullingAction = sourcePullingAction;
    }


    /**
     * Gets the sourceControlCommandId value for this SourceCodeSettings.
     * 
     * @return sourceControlCommandId
     */
    public long getSourceControlCommandId() {
        return sourceControlCommandId;
    }


    /**
     * Sets the sourceControlCommandId value for this SourceCodeSettings.
     * 
     * @param sourceControlCommandId
     */
    public void setSourceControlCommandId(long sourceControlCommandId) {
        this.sourceControlCommandId = sourceControlCommandId;
    }


    /**
     * Gets the sourceFilterLists value for this SourceCodeSettings.
     * 
     * @return sourceFilterLists
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceFilterPatterns getSourceFilterLists() {
        return sourceFilterLists;
    }


    /**
     * Sets the sourceFilterLists value for this SourceCodeSettings.
     * 
     * @param sourceFilterLists
     */
    public void setSourceFilterLists(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceFilterPatterns sourceFilterLists) {
        this.sourceFilterLists = sourceFilterLists;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SourceCodeSettings)) return false;
        SourceCodeSettings other = (SourceCodeSettings) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sourceOrigin==null && other.getSourceOrigin()==null) || 
             (this.sourceOrigin!=null &&
              this.sourceOrigin.equals(other.getSourceOrigin()))) &&
            ((this.userCredentials==null && other.getUserCredentials()==null) || 
             (this.userCredentials!=null &&
              this.userCredentials.equals(other.getUserCredentials()))) &&
            ((this.pathList==null && other.getPathList()==null) || 
             (this.pathList!=null &&
              java.util.Arrays.equals(this.pathList, other.getPathList()))) &&
            ((this.sourceControlSetting==null && other.getSourceControlSetting()==null) || 
             (this.sourceControlSetting!=null &&
              this.sourceControlSetting.equals(other.getSourceControlSetting()))) &&
            ((this.packagedCode==null && other.getPackagedCode()==null) || 
             (this.packagedCode!=null &&
              this.packagedCode.equals(other.getPackagedCode()))) &&
            ((this.sourcePullingAction==null && other.getSourcePullingAction()==null) || 
             (this.sourcePullingAction!=null &&
              this.sourcePullingAction.equals(other.getSourcePullingAction()))) &&
            this.sourceControlCommandId == other.getSourceControlCommandId() &&
            ((this.sourceFilterLists==null && other.getSourceFilterLists()==null) || 
             (this.sourceFilterLists!=null &&
              this.sourceFilterLists.equals(other.getSourceFilterLists())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSourceOrigin() != null) {
            _hashCode += getSourceOrigin().hashCode();
        }
        if (getUserCredentials() != null) {
            _hashCode += getUserCredentials().hashCode();
        }
        if (getPathList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPathList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPathList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSourceControlSetting() != null) {
            _hashCode += getSourceControlSetting().hashCode();
        }
        if (getPackagedCode() != null) {
            _hashCode += getPackagedCode().hashCode();
        }
        if (getSourcePullingAction() != null) {
            _hashCode += getSourcePullingAction().hashCode();
        }
        _hashCode += new Long(getSourceControlCommandId()).hashCode();
        if (getSourceFilterLists() != null) {
            _hashCode += getSourceFilterLists().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SourceCodeSettings.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceCodeSettings"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceLocationType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCredentials");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "UserCredentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "Credentials"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pathList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "PathList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScanPath"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScanPath"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceControlSetting");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceControlSetting"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceControlSettings"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packagedCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "PackagedCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "LocalCodeContainer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcePullingAction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourcePullingAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceControlCommandId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceControlCommandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceFilterLists");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceFilterLists"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceFilterPatterns"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
