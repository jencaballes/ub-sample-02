package jp.ubsecure.portal.jubjub.portlet.form.navigator.organization;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorCategory;
import com.liferay.users.admin.web.servlet.taglib.ui.OrganizationIdentificationFormNavigatorCategory;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property = {
		PortalConstants.KEY_FORM_NAVIGATOR_CATEGORY_ORDER + PortalConstants.CATEGORY_ORDER_ORGANIZATION_IDENTIFICATION
	},
	service = FormNavigatorCategory.class
)
public class OrganizationIdentificationFormNavigatorCategoryExt extends OrganizationIdentificationFormNavigatorCategory {
	@Override
	public String getFormNavigatorId() {
		return PortalConstants.ORGANIZATION_PREFIX + super.getFormNavigatorId();
	}
}
