package jp.ubsecure.portal.jubjub.hook.action;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.WindowState;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.PasswordPolicyLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.theme.ThemeDisplay;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_JAVAX_PORTLET_NAME + PortalConstants.PORTLET_NAME_LOGIN,
		PortalConstants.KEY_MVC_COMMAND_NAME + PortalConstants.COMMAND_NAME_LOGIN,
		PortalConstants.KEY_SERVICE_RANKING + PortalConstants.SERVICE_RANKING_100
	},
	service = MVCActionCommand.class
)
public class LoginStrutsAction extends BaseMVCActionCommand {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(LoginStrutsAction.class);
	
	@Reference(target = "(&(" + PortalConstants.KEY_MVC_COMMAND_NAME + PortalConstants.COMMAND_NAME_LOGIN + ")(" + PortalConstants.KEY_JAVAX_PORTLET_NAME + PortalConstants.PORTLET_NAME_LOGIN + ")"
			+ "(" + PortalConstants.KEY_COMPONENT_NAME + PortalConstants.COMPONENT_NAME_LOGIN + "))")
	protected MVCActionCommand mvcActionCommand;
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		HttpSession httpSession = null;
		User user = null;
		String status = "SUCCESS";
		
		SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
		SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		try {
			PortletCommonUtil.isDBConnected();
			
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			String strLogin = ParamUtil.getString(actionRequest, PortalConstants.LOGIN);
			String strUbsp = ParamUtil.getString(actionRequest, PortalConstants.PASSWORD);
			
			boolean bExpired = StrutsActionManager.userLogin(actionRequest, strLogin, strUbsp);
			
			if (!bExpired) {
				actionRequest.setAttribute(PortalConstants.LOGIN, strLogin);
				actionRequest.setAttribute(PortalConstants.PASSWORD, strUbsp);

				httpSession = ControllerHelper.getHttpSession(actionRequest);
				Enumeration<String> names = httpSession.getAttributeNames();
				Map<String, Object> oldAttr = new HashMap<String, Object>();

				while (names.hasMoreElements()) {
					String name = names.nextElement();
					oldAttr.put(name, httpSession.getAttribute(name));
				}

				if (!CommonUtil.isObjectNull(httpSession)) {
					httpSession = PortalUtil.getHttpServletRequest(actionRequest).getSession(true);
				}

				for (String name : oldAttr.keySet()) {
					httpSession.setAttribute(name, oldAttr.get(name));
				}

				StrutsActionManager.origLogin(mvcActionCommand, actionRequest, actionResponse, strLogin, strUbsp,
						themeDisplay.getCompanyId());
				
				if (!strLogin.equals(PortalConstants.STRING_EMPTY) && !strUbsp.equals(PortalConstants.STRING_EMPTY)) {

					user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay.getCompanyId(), strLogin);

					if (user != null) {
						if (!user.getLockout()) {
							Long lCompanyId = themeDisplay.getCompanyId();
							int iAuth = UserLocalServiceUtil.authenticateByEmailAddress(lCompanyId, strLogin, strUbsp,
									null, actionRequest.getParameterMap(), null);
							user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay.getCompanyId(), strLogin);

							if (iAuth != PortalConstants.INT_ONE) {

								int iFailedLoginAttempts = user.getFailedLoginAttempts();
								int iMaxFailure = PasswordPolicyLocalServiceUtil.getDefaultPasswordPolicy(lCompanyId)
										.getMaxFailure();

								user.setFailedLoginAttempts(iFailedLoginAttempts - 1);

								if (iFailedLoginAttempts == iMaxFailure) {
									user.setLockout(false);
								}

								user = UserLocalServiceUtil.updateUser(user);

								status = "FAILED";

							} 

						} else {
							status = "FAILED";
						}

					} else {
						status = "FAILED";
					}
				} else {
					status = "FAILED";
				}

			} else {
				status = "FAILED";
			}
		} catch (Exception e) {
			status = "FAILED";
			if (e instanceof UBSPortalException) {
				if (((UBSPortalException) e).getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					SessionErrors.add(actionRequest, PortalConstants.ORM_EXCEPTION);
					actionResponse.setWindowState(WindowState.MAXIMIZED);
				} else {
					SessionErrors.add(actionRequest, PortalConstants.EXPIRED_USER);
					actionResponse.setWindowState(WindowState.MAXIMIZED);
				}
			} else {
				actionResponse.setWindowState(WindowState.MAXIMIZED);
				throw e;
			}
		} finally {
			if (user != null) {
				log.info(PortalMessages.USER_LOGIN, user.getUserId(), status);
			} else {
				log.info(PortalMessages.USER_LOGIN, PortalConstants.LONG_ZERO, status);
			}			
		}
	}
}