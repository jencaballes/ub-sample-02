
/**
 * CxSDKWebServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7;

    /**
     *  CxSDKWebServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CxSDKWebServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CxSDKWebServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CxSDKWebServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getPresetList method
            * override this method for handling normal response from getPresetList operation
            */
           public void receiveResultgetPresetList(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.GetPresetListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPresetList operation
           */
            public void receiveErrorgetPresetList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for login method
            * override this method for handling normal response from login operation
            */
           public void receiveResultlogin(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.LoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from login operation
           */
            public void receiveErrorlogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateProjectIncrementalConfiguration method
            * override this method for handling normal response from updateProjectIncrementalConfiguration operation
            */
           public void receiveResultupdateProjectIncrementalConfiguration(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.UpdateProjectIncrementalConfigurationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateProjectIncrementalConfiguration operation
           */
            public void receiveErrorupdateProjectIncrementalConfiguration(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectsDisplayData method
            * override this method for handling normal response from getProjectsDisplayData operation
            */
           public void receiveResultgetProjectsDisplayData(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.GetProjectsDisplayDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectsDisplayData operation
           */
            public void receiveErrorgetProjectsDisplayData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectConfiguration method
            * override this method for handling normal response from getProjectConfiguration operation
            */
           public void receiveResultgetProjectConfiguration(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.GetProjectConfigurationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectConfiguration operation
           */
            public void receiveErrorgetProjectConfiguration(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssociatedGroupsList method
            * override this method for handling normal response from getAssociatedGroupsList operation
            */
           public void receiveResultgetAssociatedGroupsList(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.GetAssociatedGroupsListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssociatedGroupsList operation
           */
            public void receiveErrorgetAssociatedGroupsList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getStatusOfSingleScan method
            * override this method for handling normal response from getStatusOfSingleScan operation
            */
           public void receiveResultgetStatusOfSingleScan(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.GetStatusOfSingleScanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getStatusOfSingleScan operation
           */
            public void receiveErrorgetStatusOfSingleScan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getScanReportStatus method
            * override this method for handling normal response from getScanReportStatus operation
            */
           public void receiveResultgetScanReportStatus(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.GetScanReportStatusResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getScanReportStatus operation
           */
            public void receiveErrorgetScanReportStatus(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteProjects method
            * override this method for handling normal response from deleteProjects operation
            */
           public void receiveResultdeleteProjects(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.DeleteProjectsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteProjects operation
           */
            public void receiveErrordeleteProjects(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getScanReport method
            * override this method for handling normal response from getScanReport operation
            */
           public void receiveResultgetScanReport(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.GetScanReportResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getScanReport operation
           */
            public void receiveErrorgetScanReport(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getConfigurationSetList method
            * override this method for handling normal response from getConfigurationSetList operation
            */
           public void receiveResultgetConfigurationSetList(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.GetConfigurationSetListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getConfigurationSetList operation
           */
            public void receiveErrorgetConfigurationSetList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateScanComment method
            * override this method for handling normal response from updateScanComment operation
            */
           public void receiveResultupdateScanComment(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.UpdateScanCommentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateScanComment operation
           */
            public void receiveErrorupdateScanComment(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for scan method
            * override this method for handling normal response from scan operation
            */
           public void receiveResultscan(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.ScanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from scan operation
           */
            public void receiveErrorscan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createScanReport method
            * override this method for handling normal response from createScanReport operation
            */
           public void receiveResultcreateScanReport(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.CreateScanReportResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createScanReport operation
           */
            public void receiveErrorcreateScanReport(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cancelScan method
            * override this method for handling normal response from cancelScan operation
            */
           public void receiveResultcancelScan(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.CancelScanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cancelScan operation
           */
            public void receiveErrorcancelScan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteScans method
            * override this method for handling normal response from deleteScans operation
            */
           public void receiveResultdeleteScans(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.DeleteScansResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteScans operation
           */
            public void receiveErrordeleteScans(java.lang.Exception e) {
            }
                


    }
    