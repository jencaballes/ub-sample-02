package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectListEntry;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class ProjectListEntryParser implements CSVEntryConverter<ProjectListEntry> {
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ProjectListEntryParser.class);
	
	@Override
	public String[] convertEntry(ProjectListEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (entry == null) {
			params.put("entry", entry);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException(PortalErrors.FILE_PROCESS_ERROR);					
		}
		
		String[] columns = new String[7];
	    columns[0] = entry.getProjectId();
	    columns[1] = entry.getGroupName();
	    columns[2] = entry.getCaseNumber();
	    columns[3] = entry.getProjectName();
	    columns[4] = entry.getProjectEndDate();
	    columns[5] = entry.getStatus();
	    columns[6] = entry.getScanCount();
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}

}
