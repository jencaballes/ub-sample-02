package jp.ubsecure.portal.jubjub.portlet.util;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;

public final class ValidationUtil {

	public static ValidateResult validateRegex(String reqexString, Boolean needGroupingFlg) {
		ValidateResult result = new ValidateResult();
		
		if (reqexString == null) {
			result.valid_flg = false;
			result.message = PortalMessages.REGULAR_EXPRESSION_INVALID;
			return result;
		} else if (reqexString.split("[\r\n]+").length == 0) {
			if (needGroupingFlg) {
				result.valid_flg = false;
				result.message = PortalMessages.REGULAR_EXPRESSION_GROUPING_INVALID;
				return result;
			}
		} else {
			for (String regex : reqexString.split("[\r\n]+")) {
				if (isValidRegex(regex, false)) {
					if (needGroupingFlg) {
						if (!isValidRegex(regex, true)) {
							result.valid_flg = false;
							result.message = PortalMessages.REGULAR_EXPRESSION_GROUPING_INVALID;
							return result;
						}
					}
				} else {
					result.valid_flg = false;
					result.message = PortalMessages.REGULAR_EXPRESSION_INVALID;
					return result;
				}
			}
		}
		return result;
	}

	private static boolean isValidRegex(String regex, boolean needGrouping) {
		String expressionTest = "";
		try {
			expressionTest.matches(regex);
			if (needGrouping && !regex.matches(".*[(].+[)].*")) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
