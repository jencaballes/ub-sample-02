package jp.ubsecure.portal.jubjub.portlet.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.portlet.PortalContext;

import com.liferay.expando.kernel.service.ExpandoValueLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Portal;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.Recipient;
import jp.ubsecure.portal.jubjub.portlet.model.ResponseModel;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.mail.MailSender;


public class MailUtil {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(MailUtil.class);

	public static void sendEmail(int emailEvent, long entityId, Object emailData) throws UBSPortalException {
		Thread mailSender = new Thread(new MailSender(emailEvent, entityId, emailData));
		mailSender.start();
	}

	public static Map<String, Object> generateEmail(int emailEvent, long entityId, long companyId, Object emailData) throws UBSPortalException {
		Map<String, Object> params 					= new HashMap<String, Object>();
		Map<String, Object> email	 				= null;
		Map<String, String> configKeys 				= null;
		User user 									= null;
		Scan scan 									= null;
		Project project 							= null;
		String strProjectId 						= PortalConstants.STRING_EMPTY;
		String strUsername 							= PortalConstants.STRING_EMPTY;
		long emailDataId							= PortalConstants.LONG_ZERO;
		boolean bAddScanManager	 					= false;
		List<Recipient> iaRecipientsList 			= null;
		List<Recipient> groupAdminAdressList 		= null;
		
		try {
			
			if(emailEvent == PortalConstants.USER_EVENT_OPEN_PROJECT){
				String strEmailData = emailData.toString();
				
				if(!CommonUtil.isStringNullOrEmpty(strEmailData)){
					emailDataId = Long.parseLong(strEmailData);
				}
			}else{
				emailDataId = entityId;
			}
			
			if (validateRecipients(emailEvent, emailDataId, companyId)) {
				// Set recipients
				if (isEmailForOneRecipient(emailEvent)) {
					user = ControllerHelper.getUser(entityId);
					
					if (!CommonUtil.isObjectNull(user)) {
						iaRecipientsList = addRecipient(user);
					}
					
					if (!CommonUtil.isObjectNull(emailData)) {
						if (emailEvent == PortalConstants.EMAIL_EVENT_ADD_ACCOUNT ||
								emailEvent == PortalConstants.EMAIL_EVENT_UPDATE_ACCOUNT) {
							strUsername = emailData.toString();
						} else if (emailEvent == PortalConstants.EMAIL_EVENT_ADD_PROJECT ||
								emailEvent == PortalConstants.EMAIL_EVENT_UPDATE_PROJECT ||
								emailEvent == PortalConstants.USER_EVENT_OPEN_PROJECT) {
							strProjectId = emailData.toString();
						}
					}
				} else {
					
					if(emailEvent == PortalConstants.USER_EVENT_OPEN_PROJECT){
						strProjectId = emailData.toString();
					}else{
						scan = ControllerHelper.getScan(entityId);
					}
					
					if (!CommonUtil.isObjectNull(scan)) {
						strProjectId = String.valueOf(scan.getProjectId());
						
						if(!CommonUtil.isStringNullOrEmpty(strProjectId)){
							Long lProjectId = Long.parseLong(strProjectId);
							project = ProjectLocalServiceUtil.getProject(lProjectId);
						}
						
					}
					
					iaRecipientsList = getRecipientsByRole(UserRole.OVERALL_ADMIN.getInteger(), PortalConstants.STRING_EMPTY);
					
					// include group administrators of the project
					groupAdminAdressList = getRecipientsByRole(UserRole.GROUP_ADMIN.getInteger(), strProjectId);
					
					if(!CommonUtil.isObjectNull(groupAdminAdressList)){
						 iaRecipientsList.addAll(groupAdminAdressList);
					}
					
					// include scan manager/creator					
					if (!CommonUtil.isObjectNull(scan)) {
						user = ControllerHelper.getUserByEmailAddress(companyId, scan.getScanManager());
						
						if (!CommonUtil.isObjectNull(user)) {
							List<Role> roleList = user.getRoles();
							
							if (!CommonUtil.isListNullOrEmpty(roleList)) {
								for (Role role : roleList) {
									if (role.getName().equals(PortalConstants.GENERAL_USER)) {
										bAddScanManager = true;
										break;
									}
								}
							}
						}
					}
					
					if (bAddScanManager) {
						List<Recipient> scanManagerAddressList = addRecipient(user);
						
						if (!CommonUtil.isListNullOrEmpty(scanManagerAddressList)) {
							iaRecipientsList.addAll(scanManagerAddressList);
						}
					}
				}
				
				configKeys = getConfigKeys(emailEvent);
				
				if (!CommonUtil.isMapNullOrEmpty(configKeys)) {
					email = getConfigValues(configKeys, strProjectId, entityId, strUsername, emailEvent, companyId);
				}
	
				email.put(PortalConstants.EMAIL_RECIPIENTS, iaRecipientsList);
			}
		} catch (UBSPortalException e) {
			params.put("user", user);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_EMAIL, params, e);
			throw e;
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} catch (PortalException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isMapNullOrEmpty(configKeys)) {
				configKeys.clear();
				configKeys = null;
			}
		}

		return email;
	}
	
	private static Map<String, Object> getConfigValues(Map<String, String> configKeys, String strProjectId, long entityId, String strUsername, int emailEvent, long companyId) {
		Map<String, Object> params = new HashMap<String, Object>();
		String strSubjectConfigKey = configKeys.get(PortalConstants.EMAIL_SUBJECT);
		String strContentConfigKey = configKeys.get(PortalConstants.EMAIL_CONTENT);
		String strSubject = PortalConstants.STRING_EMPTY;
		String strContent = PortalConstants.STRING_EMPTY;
		Map<String, Object> configValues = new HashMap<String, Object>();
		
		try {
			strSubject = ConfigurationFileParser.getConfig(strSubjectConfigKey);
		} catch (UBSPortalException e) {
			params.put("strSubject", strSubject);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
			log.debug(PortalErrors.GENERATE_EMAIL_SUBJECT_INVALID, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
			
			if (e.getErrorCode().equals(PortalErrors.CONFIG_VALUE_EMPTY_ERROR)) {
				strSubject = PortalConstants.STRING_EMPTY;
			} else if (e.getErrorCode().equals(PortalErrors.CONFIG_VALUE_NULL_ERROR)) {
				strSubject = null;
			}
		}
		
		try {
			strContent = ConfigurationFileParser.getConfig(strContentConfigKey);
			
			if (!CommonUtil.isStringNullOrEmpty(strContent)) {
				if (emailEvent == PortalConstants.EMAIL_EVENT_PASSWORD_CHANGE
						|| emailEvent == PortalConstants.EMAIL_EVENT_ADD_ACCOUNT
						|| emailEvent == PortalConstants.EMAIL_EVENT_UPDATE_ACCOUNT) {
					if (strContent.contains(PortalConstants.MAIL_CONTENT_ACCOUNT_NAME_PLACEHOLDER)) {
						strContent = strContent.replace(PortalConstants.MAIL_CONTENT_ACCOUNT_NAME_PLACEHOLDER, strUsername);
					}
				} else if (emailEvent == PortalConstants.EMAIL_EVENT_ADD_PROJECT
						|| emailEvent == PortalConstants.EMAIL_EVENT_UPDATE_PROJECT
						|| emailEvent == PortalConstants.USER_EVENT_OPEN_PROJECT) {
					if (strContent.contains(PortalConstants.MAIL_CONTENT_PROJECT_ID_PLACEHOLDER)) {
						strContent = strContent.replace(PortalConstants.MAIL_CONTENT_PROJECT_ID_PLACEHOLDER, strProjectId);
					}
				} else if (emailEvent == PortalConstants.EMAIL_EVENT_SCAN_DONE
						|| emailEvent == PortalConstants.EMAIL_EVENT_SCAN_FAILED
						|| emailEvent == PortalConstants.EMAIL_EVENT_SCAN_CANCELLED
						|| emailEvent == PortalConstants.EMAIL_EVENT_REQUEST_REVIEW
						|| emailEvent == PortalConstants.EMAIL_EVENT_REVIEW_DONE) {
					if (strContent.contains(PortalConstants.MAIL_CONTENT_PROJECT_ID_PLACEHOLDER)) {
						strContent = strContent.replace(PortalConstants.MAIL_CONTENT_PROJECT_ID_PLACEHOLDER, strProjectId);
					}
					
					if (strContent.contains(PortalConstants.MAIL_CONTENT_SCAN_ID_PLACEHOLDER)) {
						strContent = strContent.replace(PortalConstants.MAIL_CONTENT_SCAN_ID_PLACEHOLDER, String.valueOf(entityId));
					}
					
					if (emailEvent == PortalConstants.EMAIL_EVENT_SCAN_FAILED
							|| emailEvent == PortalConstants.EMAIL_EVENT_SCAN_CANCELLED) {
						String strGroupName = PortalConstants.STRING_EMPTY;
						String strProjectName = PortalConstants.STRING_EMPTY;
						Organization org = null;
						Project project = null;
						
						try {
							long lProjectId = Long.parseLong(strProjectId);
							
							project = ProjectLocalServiceUtil.getProject(lProjectId);
							
							if (!CommonUtil.isObjectNull(project)) {
								strProjectName = project.getProjectName();
								
								org = OrganizationLocalServiceUtil.getOrganization(project.getOwnerGroup());
								
								if (!CommonUtil.isObjectNull(org)) {
									strGroupName = org.getName();
								}
							}
							
							
						} catch (NumberFormatException e) {
							params.put("strProjectId", strProjectId);
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
							log.debug(PortalErrors.NUMBER_FORMAT_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
						} catch (PortalException e) {
							params.put("organization", org);
							params.put("project", project);
							log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
						} catch (SystemException e) {
							params.put("organization", org);
							params.put("project", project);
							log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
						}
						
						if (strContent.contains(PortalConstants.MAIL_CONTENT_GROUP_NAME_PLACEHOLDER)) {
							strContent = strContent.replace(PortalConstants.MAIL_CONTENT_GROUP_NAME_PLACEHOLDER, strGroupName);
						}
						
						if (strContent.contains(PortalConstants.MAIL_CONTENT_PROJECT_NAME_PLACEHOLDER)) {
							strContent = strContent.replace(PortalConstants.MAIL_CONTENT_PROJECT_NAME_PLACEHOLDER, strProjectName);
						}
					}
					
					if (emailEvent == PortalConstants.EMAIL_EVENT_SCAN_FAILED) {
						try {
							Scan scan = ScanLocalServiceUtil.getScan(entityId);
							User user = null;
							
							if (strContent.contains(PortalConstants.MAIL_CONTENT_ERROR_MESSAGE_PLACEHOLDER)) {
								if (!CommonUtil.isObjectNull(scan)) {
									strContent = strContent.replace(PortalConstants.MAIL_CONTENT_ERROR_MESSAGE_PLACEHOLDER, scan.getFailedScanCause());
								}
							}
						
							if (strContent.contains(PortalConstants.MAIL_CONTENT_SCAN_EXECUTOR_NAME_PLACEHOLDER)) {
								if (!CommonUtil.isObjectNull(scan)) {
									
									try {
										user = UserLocalServiceUtil.getUserByEmailAddress(companyId, scan.getScanManager());
										
										if (!CommonUtil.isObjectNull(user)) {
											strContent = strContent.replace(PortalConstants.MAIL_CONTENT_SCAN_EXECUTOR_NAME_PLACEHOLDER, user.getFirstName());
										}
									} catch (PortalException e) {
										params.put("scan", scan);
										params.put("user", user);
										log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
									} catch (SystemException e) {
										params.put("scan", scan);
										params.put("user", user);
										log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
									}
								}
							}
						} catch (PortalException e) {
							params.put("scanId", entityId);
							log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
						} catch (SystemException e) {
							params.put("scanId", entityId);
							log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
						}
					}
				}
			}
		} catch (UBSPortalException e) {
			params.put("strContent", strContent);
			log.debug(PortalErrors.GENERATE_EMAIL_CONTENT_INVALID, PortalConstants.METHOD_GENERATE_EMAIL, params, e);
			
			if (e.getErrorCode().equals(PortalErrors.CONFIG_VALUE_EMPTY_ERROR)) {
				strContent = PortalConstants.STRING_EMPTY;
			} else if (e.getErrorCode().equals(PortalErrors.CONFIG_VALUE_NULL_ERROR)) {
				strContent = null;
			}
		} finally {
			params.clear();
			params = null;
		}
		
		configValues.put(PortalConstants.EMAIL_SUBJECT, strSubject);
		configValues.put(PortalConstants.EMAIL_CONTENT, strContent);
		
		return configValues;
	}

	private static List<Recipient> getRecipientsByRole (int iRole , String strProjectId) throws UBSPortalException {
		String userRole 						= PortalConstants.STRING_EMPTY; 
		long lProjectId							= PortalConstants.LONG_ZERO;
		long lProjectOwnergroup					= PortalConstants.LONG_ZERO;
		long[] organizationList 				= null;
		boolean organizationFound				= false;
		Project project							= null;
		List<User> userList 					= null;
		List<Recipient> iaRecipientsList 		= null;
		Map<String, Object> params 				= new HashMap<String, Object>();
		
		try {
			if (CommonUtil.isOutOfRange(iRole, UserRole.SYSTEM_ADMIN.getInteger(), UserRole.ADMINISTRATOR.getInteger())) {
				params.put("iRole", iRole);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new UBSPortalException(PortalErrors.GET_RECIPIENTS_BY_ROLE_ROLE_INVALID, PortalMessages.ROLE_INVALID);
			}
			
			if (UserRole.OVERALL_ADMIN.getInteger() == iRole ){
				userRole = PortalConstants.OVERALL_ADMINISTRATOR; 
			}else if (UserRole.GROUP_ADMIN.getInteger() == iRole){
				userRole = PortalConstants.GROUP_ADMINISTRATOR; 
			}
			
			userList = UserLocalServiceUtil.getUsers(PortalConstants.INT_ZERO, UserLocalServiceUtil.getUsersCount());
			
			if (!CommonUtil.isListNullOrEmpty(userList)) {
				for (User user : userList) {
					List<Role> roleList = user.getRoles();
					
					if (!CommonUtil.isListNullOrEmpty(roleList)) {
						for (Role role : roleList) {
							if (role.getName().equalsIgnoreCase(userRole)) {
								if (CommonUtil.isListNullOrEmpty(iaRecipientsList)) {
									iaRecipientsList = new ArrayList<Recipient>();
								}
								
								// get group administrators for the organization
								if(role.getName().equalsIgnoreCase(PortalConstants.GROUP_ADMINISTRATOR) 
												&& !strProjectId.equals(PortalConstants.STRING_EMPTY)){
									lProjectId	= Long.parseLong(strProjectId);
									project 	= ProjectLocalServiceUtil.getProject(lProjectId);
									
									if(!CommonUtil.isObjectNull(project)){
										lProjectOwnergroup 	= project.getOwnerGroup();
										organizationList	= user.getOrganizationIds();
										
										for( long orgId : organizationList){
											if (String.valueOf(lProjectOwnergroup).equals(String.valueOf(orgId))) {
												organizationFound = true;
												break;
											}
										}
										
									}
									
									// if group admin is not part of the organization/group continue the outside loop
									if(false == organizationFound){
										continue; 
									}
								}
							
								Recipient recipient = new Recipient();
								recipient.setEmailAddress(new InternetAddress(user.getEmailAddress()));
								recipient.setUsername(user.getFirstName());
								iaRecipientsList.add(recipient);
								
								String strMailAddress = GetterUtil.getString(ExpandoValueLocalServiceUtil.getData(user.getCompanyId(), User.class.getName(), PortalConstants.CUSTOM_FIELDS, PortalConstants.EMAIL_ADDRESS, user.getUserId())); 
								
								if (!CommonUtil.isStringNullOrEmpty(strMailAddress)) {
									recipient = new Recipient();
									recipient.setEmailAddress(new InternetAddress(strMailAddress));
									recipient.setUsername(user.getFirstName());
									iaRecipientsList.add(recipient);
								}
							}
						}
					}
				}
			}
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_RECIPIENTS_BY_ROLE, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_RECIPIENTS_BY_ROLE, params, e);
			throw e;
		} catch (AddressException e) {
			log.debug(PortalErrors.ADDRESS_EXCEPTION, PortalConstants.METHOD_GET_RECIPIENTS_BY_ROLE, params, e);
			throw new UBSPortalException(PortalErrors.ADDRESS_EXCEPTION, PortalMessages.ADDRESS_EXCEPTION, e);
		} catch (PortalException e) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_RECIPIENTS_BY_ROLE, params, e);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(userList)) {
				userList = null;
			}
		}
		
		return iaRecipientsList;
	}
	
	private static List<Recipient> addRecipient (User user) {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Recipient> iaRecipientsList = null;
		String strMailAddress = PortalConstants.STRING_EMPTY;
		String strUserId = PortalConstants.STRING_EMPTY;
		String strFirstName = PortalConstants.STRING_EMPTY;
		
		try {
			strMailAddress = GetterUtil.getString(ExpandoValueLocalServiceUtil.getData(user.getCompanyId(), User.class.getName(), PortalConstants.CUSTOM_FIELDS, PortalConstants.EMAIL_ADDRESS, user.getUserId()));
			strUserId = user.getEmailAddress();
			strFirstName = user.getFirstName();
			
			iaRecipientsList = new ArrayList<Recipient>();
			
			Recipient recipient = new Recipient();
		
			if (!CommonUtil.isStringNullOrEmpty(strUserId)) {
				recipient.setEmailAddress(new InternetAddress(strUserId));
				recipient.setUsername(strFirstName);
				iaRecipientsList.add(recipient);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strMailAddress)) {
				recipient = new Recipient();
				recipient.setEmailAddress(new InternetAddress(strMailAddress));
				recipient.setUsername(strFirstName);
				iaRecipientsList.add(recipient);
			}
		} catch (AddressException e) {
			params.put("strUserId", strUserId);
			params.put("strMailAddress", strMailAddress);
			log.debug(PortalErrors.ADDRESS_EXCEPTION, PortalConstants.METHOD_ADD_RECIPIENT, params, e);
		} catch (PortalException e) {
			params.put("lCompanyId", user.getCompanyId());
			params.put("lUserId", user.getUserId());
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_RECIPIENT, params, e);
		} catch (SystemException e) {
			params.put("lCompanyId", user.getCompanyId());
			params.put("lUserId", user.getUserId());
			params.put("strUserId", strUserId);
			params.put("strFirstName", strFirstName);
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_ADD_RECIPIENT, params, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return iaRecipientsList;
	}
	
	private static Map<String, String> getConfigKeys (int emailEvent) {
		Map<String, String> configKeys = new HashMap<String, String>();
		String strSubjectConfigKey = PortalConstants.STRING_EMPTY;
		String strContentConfigKey = PortalConstants.STRING_EMPTY;
		
		switch (emailEvent) {
			case PortalConstants.EMAIL_EVENT_PASSWORD_CHANGE:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_PASSWORD_CHANGE_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_PASSWORD_CHANGE_CONTENT;
				break;
			
			case PortalConstants.EMAIL_EVENT_ADD_PROJECT:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_PROJECT_CREATE_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_PROJECT_CREATE_CONTENT;
				break;
				
			case PortalConstants.EMAIL_EVENT_UPDATE_PROJECT:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_PROJECT_CHANGE_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_PROJECT_CHANGE_CONTENT;
				break;
				
			case PortalConstants.USER_EVENT_OPEN_PROJECT:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_PROJECT_CHANGE_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_PROJECT_CHANGE_CONTENT;
				break;
				
			case PortalConstants.EMAIL_EVENT_ADD_ACCOUNT:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_ACCOUNT_CREATE_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_ACCOUNT_CREATE_CONTENT;
				break;
				
			case PortalConstants.EMAIL_EVENT_UPDATE_ACCOUNT:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_ACCOUNT_CHANGE_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_ACCOUNT_CHANGE_CONTENT;
				break;
				
			case PortalConstants.EMAIL_EVENT_REQUEST_REVIEW:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_REQUEST_REVIEW_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_REQUEST_REVIEW_CONTENT;
				break;
				
			case PortalConstants.EMAIL_EVENT_REVIEW_DONE:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_REVIEW_DONE_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_REVIEW_DONE_CONTENT;
				break;
				
			case PortalConstants.EMAIL_EVENT_SCAN_DONE:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_SCAN_DONE_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_SCAN_DONE_CONTENT;
				break;
				
			case PortalConstants.EMAIL_EVENT_SCAN_FAILED:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_SCAN_FAILED_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_SCAN_FAILED_CONTENT;
				break;
				
			case PortalConstants.EMAIL_EVENT_SCAN_CANCELLED:
				strSubjectConfigKey = PortalConstants.CONFIG_KEY_EMAIL_SCAN_CANCELLED_SUBJECT;
				strContentConfigKey = PortalConstants.CONFIG_KEY_EMAIL_SCAN_CANCELLED_CONTENT;
				break;
				
			default:
				configKeys = null;
				break;
		}
		
		configKeys.put(PortalConstants.EMAIL_SUBJECT, strSubjectConfigKey);
		configKeys.put(PortalConstants.EMAIL_CONTENT, strContentConfigKey);
		
		return configKeys;
	}
	
	private static boolean validateRecipients (int emailEvent, long entityId, long companyId) {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bIsValid = false;
		User user = null;
		Scan scan = null;
		Project project = null;
		
		try {
			if (CommonUtil.isOutOfRange(emailEvent, PortalConstants.EMAIL_EVENT_PASSWORD_CHANGE, PortalConstants.USER_EVENT_OPEN_PROJECT)) {
				params.put("emailEvent", emailEvent);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EMAIL_EVENT));
				throw new UBSPortalException(PortalErrors.GENERATE_EMAIL_EMAIL_EVENT_INVALID, PortalMessages.EMAIL_EVENT_INVALID);
			}
	
			if (entityId <= PortalConstants.LONG_ZERO) {
				params.put("entityId", entityId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_ENTITY_ID));
				throw new UBSPortalException(PortalErrors.GENERATE_EMAIL_ENTITY_ID_INVALID, PortalMessages.ENTITY_ID_INVALID);
			}
			
			if (companyId <= PortalConstants.LONG_ZERO) {
				params.put("companyId", companyId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_COMPANY_ID));
				throw new UBSPortalException(PortalErrors.GENERATE_EMAIL_COMPANY_ID_INVALID, PortalMessages.COMPANY_ID_INVALID);
			}
			
			// Check recipients
			if (isEmailForOneRecipient(emailEvent)) {
				user = ControllerHelper.getUser(entityId);
	
				if (CommonUtil.isObjectNull(user)) {
					params.put("user", user);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
					throw new UBSPortalException(PortalErrors.GENERATE_EMAIL_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
				}
			} else {
				
				if(emailEvent ==  PortalConstants.USER_EVENT_OPEN_PROJECT)
				{
					project = ProjectLocalServiceUtil.getProject(entityId);
					
					if (CommonUtil.isObjectNull(project)) {
						throw new UBSPortalException(PortalErrors.GENERATE_EMAIL_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
					}
				}else{
					scan = ControllerHelper.getScan(entityId);
					
					if (CommonUtil.isObjectNull(scan)) {
						params.put("scan", scan);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_SCAN));
						throw new UBSPortalException(PortalErrors.GENERATE_EMAIL_SCAN_DOES_NOT_EXIST, PortalMessages.SCAN_DOES_NOT_EXIST);
					}
				}
				
			}
			
			bIsValid = true;
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_VALIDATE_RECIPIENTS, params, e);
		} catch (PortalException ex) {
			log.debug(ex.getMessage(), PortalConstants.METHOD_VALIDATE_RECIPIENTS, params, ex);
		} finally {
			params.clear();
			params = null;
		}
		
		return bIsValid;
	}
	
	private static boolean isEmailForOneRecipient (int emailEvent) {
		return (emailEvent == PortalConstants.EMAIL_EVENT_PASSWORD_CHANGE ||
				emailEvent == PortalConstants.EMAIL_EVENT_ADD_PROJECT ||
				emailEvent == PortalConstants.EMAIL_EVENT_UPDATE_PROJECT ||
				emailEvent == PortalConstants.EMAIL_EVENT_ADD_ACCOUNT ||
				emailEvent == PortalConstants.EMAIL_EVENT_UPDATE_ACCOUNT);
	}
}
