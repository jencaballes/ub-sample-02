package jp.ubsecure.portal.jubjub.portlet.service.vex;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.CsvMetaSummaryData;
import jp.ubsecure.portal.jubjub.portlet.model.VexMetaSummaryData;
import jp.ubsecure.portal.jubjub.portlet.report.VulnerabilityMasterParser;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

public class MetaSummaryHandler extends DefaultHandler {
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(MetaSummaryHandler.class);
	
	private String projectName; 	// target_system
	private String companyName; 	// own_company_name
	private String implEnvironment; // target_env
	
	private String proposalNum;
	private String diagnosisStartDate;
	private String diagnosisEndDate;
	private String highRiskCount;
	private String mediumRiskCount;
	private String lowRiskCount;
	private String remarksRiskCount;
	private String infoRiskCount;
	private String scanExecutionTime;
	private String webSignatureName;
	private String serverSettingsSignatureName;
	private String serverFilesSignatureName;
	

	private boolean bProjectName;
	private boolean bCompanyName;
	private boolean bImplEnvironment;
	private boolean bRiskLevel;
	private boolean bVulnName;
	private boolean bSignatureID;
	private boolean bConditionStatus;
	private boolean bDetectionParameters;
	private boolean bOriginalValue;
	private boolean bChangeValue;
	private boolean bDetectionTrigger;
	private boolean bVulnerability;
	private boolean bTarget;
	
	private long highCount;
	private long mediumCount;
	private long lowCount;
	private long infoCount;
	private long remarksCount;
	private String severity;
	private String inputName;
	VexMetaSummaryData csvMetaSummaryData;
	
	public MetaSummaryHandler () {
		this.highCount 				= PortalConstants.LONG_ZERO;
		this.mediumCount 			= PortalConstants.LONG_ZERO;
		this.lowCount 				= PortalConstants.LONG_ZERO;
		this.infoCount 				= PortalConstants.LONG_ZERO;
		this.remarksCount 				= PortalConstants.LONG_ZERO;
		this.csvMetaSummaryData 	= new VexMetaSummaryData();
	}
	
	@Override
	public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("target_system")) {
			bProjectName = true;
		} else if (qName.equalsIgnoreCase("own_company_name")) {
			bCompanyName = true;
		} else if (qName.equalsIgnoreCase("target_env")) {
			bImplEnvironment = true;
		} else if (qName.equalsIgnoreCase("risk_level")) {
			bRiskLevel = true;
		}  
	}
	
	@Override
	public void characters (char ch [], int start, int length) throws SAXException {
		if (bProjectName) {
			inputName = new String (ch, start, length).trim();
			csvMetaSummaryData.setProjectName(inputName);
			bProjectName = false;
		}else if (bCompanyName){
			inputName = new String (ch, start, length).trim();
			csvMetaSummaryData.setCompanyName(inputName);
			bCompanyName = false;
		}else if(bImplEnvironment){
			inputName = new String (ch, start, length).trim();
			csvMetaSummaryData.setImplEnvironment(inputName);
			bImplEnvironment = false;
		}else if(bRiskLevel){
			String riskLevel = new String (ch, start, length).trim();
			
			if (!CommonUtil.isStringNullOrEmpty(riskLevel)) {
				if (PortalConstants.SEVERITY_HIGH_EN.equalsIgnoreCase(riskLevel) 
						|| PortalConstants.SEVERITY_HIGH_JP.equalsIgnoreCase(riskLevel)) { 
					highCount++;
				} else if (PortalConstants.SEVERITY_MID_EN.equalsIgnoreCase(riskLevel)
						|| PortalConstants.SEVERITY_MID_JP.equalsIgnoreCase(riskLevel)) {
					mediumCount++;
				} else if (PortalConstants.SEVERITY_LOW_EN.equalsIgnoreCase(riskLevel)
						|| PortalConstants.SEVERITY_LOW_JP.equalsIgnoreCase(riskLevel)) {
					lowCount++;
				} else if (PortalConstants.SEVERITY_INFO_EN.equalsIgnoreCase(riskLevel)
						|| PortalConstants.SEVERITY_INFO_JP.equalsIgnoreCase(riskLevel)) {
					infoCount++;
				} else if (PortalConstants.SEVERITY_REMARKS_EN.equalsIgnoreCase(riskLevel)
						|| PortalConstants.SEVERITY_REMARKS_JP.equalsIgnoreCase(riskLevel)) {
					remarksCount++; 
				}
			}
		}
		
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (bRiskLevel) {
			csvMetaSummaryData.setHighRiskCount(String.valueOf(highCount));
			csvMetaSummaryData.setMediumRiskCount(String.valueOf(mediumCount));
			csvMetaSummaryData.setLowRiskCount(String.valueOf(lowCount));
			csvMetaSummaryData.setInfoRiskCount(String.valueOf(infoCount));
			csvMetaSummaryData.setRemarksRiskCount(String.valueOf(remarksCount));
		}
	}
}
