/**
 * CxSDKWebServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810;

public interface CxSDKWebServiceSoap extends java.rmi.Remote {
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseRunID scan(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CliScanArgs args) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseRunID scanWithOriginName(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CliScanArgs args, java.lang.String origName) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse cancelScan(java.lang.String sessionID, java.lang.String runId) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse deleteProjects(java.lang.String sessionID, long[] projectIDs) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse deleteScans(java.lang.String sessionID, long[] scanIDs) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseRunID scanWithScheduling(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CliScanArgs args, java.lang.String schedulingData) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseRunID scanWithSchedulingWithCron(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CliScanArgs args, java.lang.String cronString, long utcEpochStartTime, long utcEpochEndTime) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse updateScanComment(java.lang.String sessionID, long scanID, java.lang.String comment) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponsePresetList getPresetList(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseConfigSetList getConfigurationSetList(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseProjectsDisplayData getProjectsDisplayData(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseProjectScannedDisplayData getProjectScannedDisplayData(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseProjectConfig getProjectConfiguration(java.lang.String sessionID, long projectID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse updateProjectIncrementalConfiguration(java.lang.String sessionID, long projectID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ProjectConfiguration projectConfiguration) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseLoginData login(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.Credentials applicationCredentials, int lcid) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse logout(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseGroupList getAssociatedGroupsList(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseScanStatus getStatusOfSingleScan(java.lang.String sessionID, java.lang.String runId) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseScansDisplayData getScansDisplayDataForAllProjects(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseScanSummary getScanSummary(java.lang.String sessionID, long scanID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse updateProjectConfiguration(java.lang.String sessionID, long projectID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ProjectConfiguration projectConfiguration) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse deleteUser(java.lang.String sessionID, int userID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseUserData getAllUsers(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSCreateReportResponse createScanReport(java.lang.String sessionID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSReportRequest reportRequest) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSReportStatusResponse getScanReportStatus(java.lang.String sessionID, long reportID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseScanResults getScanReport(java.lang.String sessionID, long reportID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse executeDataRetention(java.lang.String sessionID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDataRetentionConfiguration dataRetentionConfiguration) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse stopDataRetention(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSResponseTeamLdapGroupMappingData getTeamLdapGroupsMapping(java.lang.String sessionId, java.lang.String teamId) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse setTeamLdapGroupsMapping(java.lang.String sessionId, java.lang.String teamId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSLdapGroupMapping[] ldapGroups) throws java.rmi.RemoteException;
}
