package jp.ubsecure.portal.jubjub.portlet.form.navigator.my.account;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.users.admin.web.servlet.taglib.ui.UserUserGroupsFormNavigatorEntry;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property = {
		PortalConstants.KEY_FORM_NAVIGATOR_ENTRY_ORDER + PortalConstants.ENTRY_ORDER_USER_USER_GROUPS
	},
	service = FormNavigatorEntry.class
)
public class UserUserGroupsFormNavigatorEntryExt extends UserUserGroupsFormNavigatorEntry {
	private boolean visible = GetterUtil.getBoolean(PropsUtil.get(PortalConstants.MY_ACCOUNT_USER_GROUPS_VISIBLE), true);

	@Override
	public String getFormNavigatorId() {
		return PortalConstants.MY_ACCOUNT_PREFIX + super.getFormNavigatorId();
	}

	@Override
	public boolean isVisible(User user, User selUser) {
		return visible && super.isVisible(user, selUser);
	}
}