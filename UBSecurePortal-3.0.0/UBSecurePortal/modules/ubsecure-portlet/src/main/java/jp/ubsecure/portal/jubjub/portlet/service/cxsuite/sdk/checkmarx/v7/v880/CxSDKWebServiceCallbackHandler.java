
/**
 * CxSDKWebServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880;

    /**
     *  CxSDKWebServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CxSDKWebServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CxSDKWebServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CxSDKWebServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for stopDataRetention method
            * override this method for handling normal response from stopDataRetention operation
            */
           public void receiveResultstopDataRetention(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.StopDataRetentionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from stopDataRetention operation
           */
            public void receiveErrorstopDataRetention(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTeamLdapGroupsMapping method
            * override this method for handling normal response from getTeamLdapGroupsMapping operation
            */
           public void receiveResultgetTeamLdapGroupsMapping(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetTeamLdapGroupsMappingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTeamLdapGroupsMapping operation
           */
            public void receiveErrorgetTeamLdapGroupsMapping(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for branchProjectById method
            * override this method for handling normal response from branchProjectById operation
            */
           public void receiveResultbranchProjectById(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.BranchProjectByIdResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from branchProjectById operation
           */
            public void receiveErrorbranchProjectById(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteUser method
            * override this method for handling normal response from deleteUser operation
            */
           public void receiveResultdeleteUser(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.DeleteUserResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteUser operation
           */
            public void receiveErrordeleteUser(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateProjectIncrementalConfiguration method
            * override this method for handling normal response from updateProjectIncrementalConfiguration operation
            */
           public void receiveResultupdateProjectIncrementalConfiguration(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.UpdateProjectIncrementalConfigurationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateProjectIncrementalConfiguration operation
           */
            public void receiveErrorupdateProjectIncrementalConfiguration(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectsDisplayData method
            * override this method for handling normal response from getProjectsDisplayData operation
            */
           public void receiveResultgetProjectsDisplayData(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetProjectsDisplayDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectsDisplayData operation
           */
            public void receiveErrorgetProjectsDisplayData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteProjects method
            * override this method for handling normal response from deleteProjects operation
            */
           public void receiveResultdeleteProjects(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.DeleteProjectsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteProjects operation
           */
            public void receiveErrordeleteProjects(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getScanReportStatus method
            * override this method for handling normal response from getScanReportStatus operation
            */
           public void receiveResultgetScanReportStatus(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetScanReportStatusResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getScanReportStatus operation
           */
            public void receiveErrorgetScanReportStatus(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateProjectConfiguration method
            * override this method for handling normal response from updateProjectConfiguration operation
            */
           public void receiveResultupdateProjectConfiguration(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.UpdateProjectConfigurationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateProjectConfiguration operation
           */
            public void receiveErrorupdateProjectConfiguration(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ssoLogin method
            * override this method for handling normal response from ssoLogin operation
            */
           public void receiveResultssoLogin(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.SsoLoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ssoLogin operation
           */
            public void receiveErrorssoLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for scanWithScheduling method
            * override this method for handling normal response from scanWithScheduling operation
            */
           public void receiveResultscanWithScheduling(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.ScanWithSchedulingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from scanWithScheduling operation
           */
            public void receiveErrorscanWithScheduling(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAllUsers method
            * override this method for handling normal response from getAllUsers operation
            */
           public void receiveResultgetAllUsers(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetAllUsersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAllUsers operation
           */
            public void receiveErrorgetAllUsers(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for logout method
            * override this method for handling normal response from logout operation
            */
           public void receiveResultlogout(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.LogoutResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from logout operation
           */
            public void receiveErrorlogout(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for cancelScan method
            * override this method for handling normal response from cancelScan operation
            */
           public void receiveResultcancelScan(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.CancelScanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from cancelScan operation
           */
            public void receiveErrorcancelScan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getScansDisplayDataForAllProjects method
            * override this method for handling normal response from getScansDisplayDataForAllProjects operation
            */
           public void receiveResultgetScansDisplayDataForAllProjects(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetScansDisplayDataForAllProjectsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getScansDisplayDataForAllProjects operation
           */
            public void receiveErrorgetScansDisplayDataForAllProjects(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteScans method
            * override this method for handling normal response from deleteScans operation
            */
           public void receiveResultdeleteScans(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.DeleteScansResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteScans operation
           */
            public void receiveErrordeleteScans(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPresetList method
            * override this method for handling normal response from getPresetList operation
            */
           public void receiveResultgetPresetList(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetPresetListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPresetList operation
           */
            public void receiveErrorgetPresetList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for login method
            * override this method for handling normal response from login operation
            */
           public void receiveResultlogin(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.LoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from login operation
           */
            public void receiveErrorlogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for setTeamLdapGroupsMapping method
            * override this method for handling normal response from setTeamLdapGroupsMapping operation
            */
           public void receiveResultsetTeamLdapGroupsMapping(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.SetTeamLdapGroupsMappingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from setTeamLdapGroupsMapping operation
           */
            public void receiveErrorsetTeamLdapGroupsMapping(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for executeDataRetention method
            * override this method for handling normal response from executeDataRetention operation
            */
           public void receiveResultexecuteDataRetention(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.ExecuteDataRetentionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from executeDataRetention operation
           */
            public void receiveErrorexecuteDataRetention(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectScannedDisplayData method
            * override this method for handling normal response from getProjectScannedDisplayData operation
            */
           public void receiveResultgetProjectScannedDisplayData(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetProjectScannedDisplayDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectScannedDisplayData operation
           */
            public void receiveErrorgetProjectScannedDisplayData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectConfiguration method
            * override this method for handling normal response from getProjectConfiguration operation
            */
           public void receiveResultgetProjectConfiguration(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetProjectConfigurationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectConfiguration operation
           */
            public void receiveErrorgetProjectConfiguration(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssociatedGroupsList method
            * override this method for handling normal response from getAssociatedGroupsList operation
            */
           public void receiveResultgetAssociatedGroupsList(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetAssociatedGroupsListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssociatedGroupsList operation
           */
            public void receiveErrorgetAssociatedGroupsList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getStatusOfSingleScan method
            * override this method for handling normal response from getStatusOfSingleScan operation
            */
           public void receiveResultgetStatusOfSingleScan(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetStatusOfSingleScanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getStatusOfSingleScan operation
           */
            public void receiveErrorgetStatusOfSingleScan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getScanReport method
            * override this method for handling normal response from getScanReport operation
            */
           public void receiveResultgetScanReport(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetScanReportResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getScanReport operation
           */
            public void receiveErrorgetScanReport(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getConfigurationSetList method
            * override this method for handling normal response from getConfigurationSetList operation
            */
           public void receiveResultgetConfigurationSetList(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetConfigurationSetListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getConfigurationSetList operation
           */
            public void receiveErrorgetConfigurationSetList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateScanComment method
            * override this method for handling normal response from updateScanComment operation
            */
           public void receiveResultupdateScanComment(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.UpdateScanCommentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateScanComment operation
           */
            public void receiveErrorupdateScanComment(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getScanSummary method
            * override this method for handling normal response from getScanSummary operation
            */
           public void receiveResultgetScanSummary(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.GetScanSummaryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getScanSummary operation
           */
            public void receiveErrorgetScanSummary(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for scanWithOriginName method
            * override this method for handling normal response from scanWithOriginName operation
            */
           public void receiveResultscanWithOriginName(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.ScanWithOriginNameResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from scanWithOriginName operation
           */
            public void receiveErrorscanWithOriginName(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for scan method
            * override this method for handling normal response from scan operation
            */
           public void receiveResultscan(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.ScanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from scan operation
           */
            public void receiveErrorscan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for scanWithSchedulingWithCron method
            * override this method for handling normal response from scanWithSchedulingWithCron operation
            */
           public void receiveResultscanWithSchedulingWithCron(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.ScanWithSchedulingWithCronResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from scanWithSchedulingWithCron operation
           */
            public void receiveErrorscanWithSchedulingWithCron(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createScanReport method
            * override this method for handling normal response from createScanReport operation
            */
           public void receiveResultcreateScanReport(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.CreateScanReportResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createScanReport operation
           */
            public void receiveErrorcreateScanReport(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for isValidProjectName method
            * override this method for handling normal response from isValidProjectName operation
            */
           public void receiveResultisValidProjectName(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.IsValidProjectNameResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from isValidProjectName operation
           */
            public void receiveErrorisValidProjectName(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for loginWithToken method
            * override this method for handling normal response from loginWithToken operation
            */
           public void receiveResultloginWithToken(
                    jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.v880.CxSDKWebServiceStub.LoginWithTokenResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from loginWithToken operation
           */
            public void receiveErrorloginWithToken(java.lang.Exception e) {
            }
                


    }
    