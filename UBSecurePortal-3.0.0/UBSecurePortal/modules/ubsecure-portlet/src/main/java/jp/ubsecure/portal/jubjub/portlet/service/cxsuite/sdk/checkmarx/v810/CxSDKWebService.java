/**
 * CxSDKWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810;

public interface CxSDKWebService extends javax.xml.rpc.Service {
    public java.lang.String getCxSDKWebServiceSoapAddress();

    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxSDKWebServiceSoap getCxSDKWebServiceSoap() throws javax.xml.rpc.ServiceException;

    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxSDKWebServiceSoap getCxSDKWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
