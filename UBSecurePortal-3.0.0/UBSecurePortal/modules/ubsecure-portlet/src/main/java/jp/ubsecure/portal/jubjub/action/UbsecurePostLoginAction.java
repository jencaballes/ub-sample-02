package jp.ubsecure.portal.jubjub.action;

import java.text.Normalizer;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.expando.kernel.service.ExpandoValueLocalServiceUtil;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_KEY + PortalConstants.KEY_POST_LOGIN_EVENT
	},
	service = LifecycleAction.class
)
public class UbsecurePostLoginAction implements LifecycleAction {
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(UbsecurePostLoginAction.class);
	
	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {
		HttpServletRequest httpreq = lifecycleEvent.getRequest();
		String strUserId = String.valueOf(PortalUtil.getUserId(httpreq));
		String strSessionId = PortalConstants.STRING_EMPTY;
		
		if (!CommonUtil.isStringNullOrEmpty(strUserId)) {
			HttpSession session = null;
			strSessionId = httpreq.getRequestedSessionId();
			
			session = httpreq.getSession(false);
			
			session.setAttribute(PortalConstants.USER_ID, strUserId);
			session.setAttribute(PortalConstants.LOGGED_IN, true);
			session.setAttribute(PortalConstants.SESSION_ID, strSessionId);
			session.setAttribute(PortalConstants.CXNAME, PortalConstants.CX_NAME);
			session.setAttribute(PortalConstants.ANDROIDNAME, PortalConstants.ANDROID_NAME);
			session.setAttribute(PortalConstants.IOSNAME, PortalConstants.IOS_NAME);
			session.setAttribute(PortalConstants.VEXNAME, PortalConstants.VEX_NAME);
			
			try {
				User user = PortalUtil.getUser(httpreq);

				if (!CommonUtil.isObjectNull(user)) {
					List<Role> roles = user.getRoles();
					
					if (!CommonUtil.isListNullOrEmpty(roles)) {
						int iUserRole = PortalConstants.INT_ZERO;
						
						for (Role role : roles) {
							if (role.getName().equals(PortalConstants.SYSTEM_ADMINISTRATOR)) {
								iUserRole = UserRole.SYSTEM_ADMIN.getInteger();
								break;
							} else if (role.getName().equals(PortalConstants.OVERALL_ADMINISTRATOR)) {
								iUserRole = UserRole.OVERALL_ADMIN.getInteger();	
								break;
							} else if (role.getName().equals(PortalConstants.GROUP_ADMINISTRATOR)) {
								iUserRole = UserRole.GROUP_ADMIN.getInteger();
								break;
							} else if (role.getName().equals(PortalConstants.GENERAL_USER)) {
								iUserRole = UserRole.GEN_USER.getInteger();
								break;
							} else if (role.getName().equals(PortalConstants.ADMINISTRATOR)) {
								iUserRole = UserRole.ADMINISTRATOR.getInteger();
							}
						}
						
						session.setAttribute(PortalConstants.USER_ROLE, iUserRole);
						session.setAttribute(PortalConstants.LOGGED_IN_CX, false);
						
						if (iUserRole == UserRole.SYSTEM_ADMIN.getInteger() || iUserRole == UserRole.ADMINISTRATOR.getInteger()) {
							session.setAttribute(PortalConstants.USE_ANDROID, false);
							session.setAttribute(PortalConstants.USE_CXSUITE, false);
							session.setAttribute(PortalConstants.USE_IOS, false);
							session.setAttribute(PortalConstants.USE_VEX, false);
						} else if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
							session.setAttribute(PortalConstants.USE_ANDROID, true);
							session.setAttribute(PortalConstants.USE_CXSUITE, true);
							session.setAttribute(PortalConstants.USE_IOS, true);
							session.setAttribute(PortalConstants.USE_VEX, true);
						} else {
							session.setAttribute(PortalConstants.USE_ANDROID, false);
							session.setAttribute(PortalConstants.USE_CXSUITE, false);
							session.setAttribute(PortalConstants.USE_IOS, false);
							session.setAttribute(PortalConstants.USE_VEX, false);
							
							List<Organization> lOrgs = OrganizationLocalServiceUtil.getUserOrganizations(user.getUserId());		
							
							if (lOrgs != null && !lOrgs.isEmpty()) {
								for (Organization organization : lOrgs){
									if (!CommonUtil.isObjectNull(organization)) {
										if (GetterUtil.getBoolean(ExpandoValueLocalServiceUtil.getData(user.getCompanyId(), 
												Organization.class.getName(), PortalConstants.CUSTOM_FIELDS, PortalConstants.ANDROIDSERVICE_USE_AUTH, organization.getOrganizationId()))) {

											session.setAttribute(PortalConstants.USE_ANDROID, true);
										}
										
										if (GetterUtil.getBoolean(ExpandoValueLocalServiceUtil.getData(user.getCompanyId(), 
												Organization.class.getName(), PortalConstants.CUSTOM_FIELDS, PortalConstants.CXSERVICE_USE_AUTH, organization.getOrganizationId()))) {

											session.setAttribute(PortalConstants.USE_CXSUITE, true);
										}
										
										if (GetterUtil.getBoolean(ExpandoValueLocalServiceUtil.getData(user.getCompanyId(), 
												Organization.class.getName(), PortalConstants.CUSTOM_FIELDS, PortalConstants.IOSSERVICE_USE_AUTH, organization.getOrganizationId()))) {

											session.setAttribute(PortalConstants.USE_IOS, true); 
										}
										
										if (GetterUtil.getBoolean(ExpandoValueLocalServiceUtil.getData(user.getCompanyId(), 
												Organization.class.getName(), PortalConstants.CUSTOM_FIELDS, PortalConstants.VEXSERVICE_USE_AUTH, organization.getOrganizationId()))) {

											session.setAttribute(PortalConstants.USE_VEX, true);
											 
										}
									}
								}
							}
						}
					}
				}
			} catch (PortalException e) {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.USER_LOGIN, null, e);
			}
			Object oLastPath = session.getAttribute("LAST_PATH");
			
			if (oLastPath != null) {
				String strLastPath = Normalizer.normalize(oLastPath.toString(), Normalizer.Form.NFKD);
				if (strLastPath.equals("/") || strLastPath.equals("/guest/welcome")) {
					LastPath path = new LastPath(StringPool.BLANK, "");
					session.setAttribute(WebKeys.LAST_PATH, path);
				} else {
					session.setAttribute(WebKeys.LAST_PATH, oLastPath);
				}
			} else {
				LastPath path = new LastPath(StringPool.BLANK, "");
				session.setAttribute(WebKeys.LAST_PATH, path);
			}
		}
	}
}

