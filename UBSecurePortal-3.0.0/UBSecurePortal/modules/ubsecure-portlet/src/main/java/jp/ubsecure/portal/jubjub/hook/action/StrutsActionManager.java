package jp.ubsecure.portal.jubjub.hook.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.servlet.http.HttpSession;

import com.liferay.expando.kernel.service.ExpandoValueLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.DuplicateOrganizationException;
import com.liferay.portal.kernel.exception.EmailAddressException;
import com.liferay.portal.kernel.exception.NoSuchOrganizationException;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.OrganizationNameException;
import com.liferay.portal.kernel.exception.PasswordExpiredException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.RequiredOrganizationException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.exception.UserEmailAddressException;
import com.liferay.portal.kernel.exception.UserLockoutException;
import com.liferay.portal.kernel.exception.UserPasswordException;
import com.liferay.portal.kernel.exception.UserScreenNameException;
import com.liferay.portal.kernel.model.Address;
import com.liferay.portal.kernel.model.EmailAddress;
import com.liferay.portal.kernel.model.OrgLabor;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.OrganizationConstants;
import com.liferay.portal.kernel.model.Phone;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroupRole;
import com.liferay.portal.kernel.model.Website;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.service.OrganizationServiceUtil;
import com.liferay.portal.kernel.service.PasswordPolicyLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.service.UserServiceUtil;
import com.liferay.portal.kernel.service.persistence.OrganizationUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PwdGenerator;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.util.comparator.UserEmailAddressComparator;
import com.liferay.users.admin.kernel.util.UsersAdminUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalUserEmailAddressException;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalUserPasswordException;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalUserScreenNameException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

public class StrutsActionManager {
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(StrutsActionManager.class);
	
	protected static boolean userLogin(ActionRequest actionRequest, String strLogin, String strUbsp) 
			throws Exception {
		boolean bExpired = false;
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (!strLogin.equals(PortalConstants.STRING_EMPTY) && !strUbsp.equals(PortalConstants.STRING_EMPTY)) {
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
					.getAttribute(WebKeys.THEME_DISPLAY);
			int iAuth = PortalConstants.INT_ZERO;
			Long lCompanyId = themeDisplay.getCompanyId();
			DateFormat formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT);
			
			// Create default date: January 1, 1970
			final Date dteDefaultDate = new Date(0);
			Date dteExpiredDate = null;
			Date dteCurrentDate = new Date();
			
			Calendar now = Calendar.getInstance();
			now.set(Calendar.HOUR, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MILLISECOND, 0);
				
			User user = null;

			try {
				PortletCommonUtil.isDBConnected();
				
				if(CommonUtil.getStringBytes(strUbsp) > PortalConstants.STRING_BYTE_MAX_100){
					params.put("USER ID", user.getUserId());
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PASSWORD));
					log.warn(PortalErrors.LOGIN_INVALID_PASSWORD, "userLogin", params, (new UBSPortalUserPasswordException.MustNotExceedMaximumBytes(null)));
					throw new UBSPortalUserPasswordException.MustNotExceedMaximumBytes(null);
				}
				
				
				user = UserLocalServiceUtil.getUserByEmailAddress(
						lCompanyId, strLogin);
				
				if (user != null) {
					dteExpiredDate = GetterUtil.getDate(
							ExpandoValueLocalServiceUtil.getData(
									lCompanyId,
									User.class.getName(), PortalConstants.CUSTOM_FIELDS,
									PortalConstants.ACC_EXPIRATION_DATE, user.getUserId()), formatter);
	
					iAuth = UserLocalServiceUtil.authenticateByEmailAddress(lCompanyId, 
							strLogin, strUbsp, null, actionRequest.getParameterMap(), null);
					
					if (iAuth == PortalConstants.INT_ONE) {	// if successful authentication
						if (dteExpiredDate.before(now.getTime()) && !dteExpiredDate.equals(dteDefaultDate)) {
							bExpired = PortalConstants.TRUE;
							params.put("USER ID", user.getUserId());
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_PASSED, PortalConstants.ERROR_LOG_PARAM_EXPIRATION_DATE));
							throw new UBSPortalException(PortalConstants.EXPIRED_USER);
						}
					} else {
						user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, strLogin);
						int iFailedLoginAttempts = user.getFailedLoginAttempts();
						int iMaxFailure = PasswordPolicyLocalServiceUtil.getDefaultPasswordPolicy(lCompanyId).getMaxFailure();
						
						user.setFailedLoginAttempts(iFailedLoginAttempts - 1);
						
						if (iFailedLoginAttempts == iMaxFailure) {
							user.setLockout(false);
						}
						
						user = UserLocalServiceUtil.updateUser(user);
						
						params.put("USER ID", user.getUserId());
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PASSWORD));
						log.warn(PortalErrors.LOGIN_ID_PASSWORD_FAILED, PortalConstants.USER_LOGIN, params, new NoSuchUserException(PortalErrors.LOGIN_ID_PASSWORD_FAILED));
					}
				}
			} catch (UserPasswordException e) {
				log.warn(PortalErrors.LOGIN_INVALID_PASSWORD, PortalConstants.USER_LOGIN, params, e);
			} catch (NoSuchUserException e) {
				if (user != null) {
					params.put("USER ID", user.getUserId());
				}
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
				log.warn(PortalErrors.LOGIN_ID_PASSWORD_FAILED, PortalConstants.USER_LOGIN, params, e);
				throw e;
			} catch (UserLockoutException e) {
				if (user != null) {
					params.put("USER ID", user.getUserId());
				}
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_ACCOUNT_LOCK);
				log.warn(PortalErrors.LOGIN_ACCOUNT_LOCKED, PortalConstants.USER_LOGIN, params, e);
			} catch (SystemException e) {
				params.put(PortalConstants.FUNCTION_NAME, PortalConstants.GET_USER_BY_EMAIL_ADDRESS);
				params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
				params.put(PortalConstants.STR_LOGIN, strLogin);
				
				params.put(PortalConstants.FUNCTION_NAME, PortalConstants.GET_DATA);
				params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
				params.put(PortalConstants.CLASS_NAME, PortalConstants.EXPANDO_DIR);
				params.put(PortalConstants.TABLE_NAME, PortalConstants.CUSTOM_FIELDS);
				params.put(PortalConstants.COLUMN_NAME, PortalConstants.ACC_EXPIRATION_DATE);
				if (user != null) {
					params.put("USER ID", user.getUserId());
					params.put(PortalConstants.USER_ID, user.getUserId());
				} else {
					params.put(PortalConstants.USER, null);
				}
				
				params.put(PortalConstants.FUNCTION_NAME, PortalConstants.AUTH_BY_EMAIL);
				params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
				params.put(PortalConstants.STR_LOGIN, strLogin);
				params.put(PortalConstants.HEADER_MAP, null);
				params.put(PortalConstants.PARAMETER_MAP, actionRequest.getParameterMap());
				params.put(PortalConstants.RESULTS_MAP, null);
				log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.USER_LOGIN, params, e);	
			} catch (PortalException e) {
				params.put(PortalConstants.FUNCTION_NAME, PortalConstants.GET_USER_BY_EMAIL_ADDRESS);
				params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
				params.put(PortalConstants.STR_LOGIN, strLogin);
				
				params.put(PortalConstants.FUNCTION_NAME, "getData");
				params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
				params.put(PortalConstants.CLASS_NAME, PortalConstants.EXPANDO_DIR);
				params.put(PortalConstants.TABLE_NAME, PortalConstants.CUSTOM_FIELDS);
				params.put(PortalConstants.COLUMN_NAME, PortalConstants.ACC_EXPIRATION_DATE);
				if (user != null) {
					params.put("USER ID", user.getUserId());
					params.put(PortalConstants.USER_ID, user.getUserId());
				} else {
					params.put(PortalConstants.USER, null);
				}
				
				params.put(PortalConstants.FUNCTION_NAME, PortalConstants.AUTH_BY_EMAIL);
				params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
				params.put(PortalConstants.STR_LOGIN, strLogin);
				params.put(PortalConstants.HEADER_MAP, null);
				params.put(PortalConstants.PARAMETER_MAP, actionRequest.getParameterMap());
				params.put(PortalConstants.RESULTS_MAP, null);
				log.debug(PortalErrors.PORTAL_EXCEPTION,  PortalConstants.USER_LOGIN, params, e);	
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.USER_LOGIN, params, e);
				} else {
					if (user != null) {
						params.put("USER ID", user.getUserId());
					}
					params.put(PortalConstants.INT_AUTH, iAuth);
					params.put(PortalConstants.DATE_EXPIRED_DATE, dteExpiredDate);
					params.put(PortalConstants.DATE_CURRENT_DATE, dteCurrentDate);
					log.debug(PortalErrors.LOGIN_ACCOUNT_EXPIRED, PortalConstants.USER_LOGIN, params, e);
				}
				throw e;
			} finally {
				params.clear();
				params = null;
			}
		}

		return bExpired;
	}
	
	protected static void origLogin(MVCActionCommand mvcActionCommand, ActionRequest actionRequest, ActionResponse actionResponse, String login, String password, long lCompanyId) 
					throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			mvcActionCommand.processAction(actionRequest, actionResponse);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof NoSuchUserException) {
				params.put(PortalConstants.ACTION_REQUEST, actionRequest);
				params.put(PortalConstants.ACTION_RESPONSE, actionResponse);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
				log.warn(PortalErrors.LOGIN_ID_PASSWORD_FAILED, PortalConstants.ORIG_LOGIN, params, e);
			} else if (e instanceof UserEmailAddressException) {
				params.put(PortalConstants.ACTION_REQUEST, actionRequest);
				params.put(PortalConstants.ACTION_RESPONSE, actionResponse);
				log.warn(PortalErrors.LOGIN_INVALID_USER_ID,  PortalConstants.ORIG_LOGIN, params, e);
			} else if (e instanceof UserPasswordException) {
				params.put(PortalConstants.ACTION_REQUEST, actionRequest);
				params.put(PortalConstants.ACTION_RESPONSE, actionResponse);
				log.warn(PortalErrors.LOGIN_INVALID_PASSWORD,  PortalConstants.ORIG_LOGIN, params, e);
			} else if (e instanceof UserLockoutException) {
				params.put(PortalConstants.ACTION_REQUEST, actionRequest);
				params.put(PortalConstants.ACTION_RESPONSE, actionResponse);
				log.warn(PortalErrors.LOGIN_ACCOUNT_LOCKED, PortalConstants.ORIG_LOGIN, params, e);
			} else {
				log.warn(PortalErrors.LOGIN_ID_PASSWORD_FAILED,  PortalConstants.ORIG_LOGIN, params, e);
			}
		} finally {
			params.clear();
			params = null;
		}
	}
	
	protected static void changeP(MVCActionCommand mvcActionCommand, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		String strOldUbsp = ParamUtil.getString(actionRequest, PortalConstants.STRPNAME);
		String strNewUbsp = ParamUtil.getString(actionRequest, PortalConstants.STRPNAME1);
		String strNewUbsp_confirm = ParamUtil.getString(actionRequest, PortalConstants.STRPNAME2);
		try {
			PortletCommonUtil.isDBConnected();
			
			HttpSession session = ControllerHelper.getHttpSession(actionRequest);
			Object oUserId = session.getAttribute(PortalConstants.USER_ID);
			long lUserId = PortalConstants.LONG_ZERO;
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
			}
			//Checking
			if(CommonUtil.isStringNullOrEmpty(strOldUbsp)){//if password is empty
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PASSWORD));
				throw new UserPasswordException.MustNotBeNull(lUserId);
			} else if (CommonUtil.getStringBytes(strNewUbsp) > 40) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PASSWORD));
				throw new PasswordExpiredException();
			}else {
				UserServiceUtil.updatePassword(lUserId, strNewUbsp, strNewUbsp_confirm, false);
			}
		} catch (Exception e) {
			if (e instanceof PasswordExpiredException) {
				log.warn(PortalErrors.CHANGE_PASSWORD_TOO_LONG, PortalConstants.CHANGE_P, params, e);
			} else if (e instanceof UserPasswordException.MustNotBeNull) {
				log.warn(PortalErrors.CHANGE_PASSWORD_EMPTY, PortalConstants.CHANGE_P, params, e);
			} else if (e instanceof UBSPortalException) {
				if (((UBSPortalException) e).getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.CHANGE_P, params, e);
				}
			}
			
			throw e;
		} finally {
			params.clear();
			params = null;
		}
	}
	
	/*--- USER MANAGEMENT ---*/
	
	protected static void addEditUser(ActionRequest actionRequest, String strCmd) throws Exception {
		actionRequest.setAttribute(PortalConstants.IS_SEARCHED, PortalConstants.FALSE);
		// Preset values
		final boolean bAutoPassword = ParamUtil.getBoolean(actionRequest, PortalConstants.AUTO_PASSWORD);
		final boolean bAutoScreenName = ParamUtil.getBoolean(actionRequest, PortalConstants.AUTO_SCREENNAME);
		final boolean bMale = ParamUtil.getBoolean(actionRequest, PortalConstants.MALE, PortalConstants.TRUE);
		final boolean bSendEmail = PortalConstants.TRUE;
		
		final long lfacebookId = PortalConstants.LONG_ZERO;
		final long[] lGroupIds = getLongArray(actionRequest, PortalConstants.GROUP_SEARCH_CONTAINTER_PK);
		final long[] lOrganizationIds = getLongArray(actionRequest, PortalConstants.GROUP_ID);
		final long[] lUserGroupIds = getLongArray(actionRequest, PortalConstants.USER_GROUP_CONTAINER_PK);
		final int iPrefixId = ParamUtil.getInteger(actionRequest, PortalConstants.PREFIX_ID);
		final int iSuffixId = ParamUtil.getInteger(actionRequest, PortalConstants.SUFFIX_ID);
		
		final String strReminderQueryQuestion = ParamUtil.getString(actionRequest, PortalConstants.REMINDER_QUERY_QUESTION);
		final String strReminderQueryAnswer = ParamUtil.getString(actionRequest, PortalConstants.REMINDER_QUERY_ANSWER);
		final String strOpenId = ParamUtil.getString(actionRequest, PortalConstants.OPEN_ID);
		final String strLanguageId = ParamUtil.getString(actionRequest, PortalConstants.STR_LANGUAGE_ID);
		final String strTimeZoneId = ParamUtil.getString(actionRequest, PortalConstants.TIMEZONE_ID);
		final String strGreeting = ParamUtil.getString(actionRequest, PortalConstants.GREETING);
		final String strComments = ParamUtil.getString(actionRequest, PortalConstants.COMMENTS);
		final String strTextSn = ParamUtil.getString(actionRequest, PortalConstants.SMS_SN);
		final String strAimSn = ParamUtil.getString(actionRequest, PortalConstants.AIM_SN);
		final String strFacebookSn = ParamUtil.getString(actionRequest, PortalConstants.FACEBOOK_SN);
		final String strIcqSn = ParamUtil.getString(actionRequest, PortalConstants.ICQ_SN);
		final String strJabberSn = ParamUtil.getString(actionRequest, PortalConstants.JABBER_SN);
		final String strMsnSn = ParamUtil.getString(actionRequest, PortalConstants.MSN_SN);
		final String strJobTitle = ParamUtil.getString(actionRequest, PortalConstants.JOB_TITLE);
		final List<UserGroupRole> userGroupRolesList = UsersAdminUtil.getUserGroupRoles(actionRequest);
		
		Map<String, Object> params = new HashMap<String, Object>();
		User user = null;
		ServiceContext serviceContext = null;
		Calendar now = Calendar.getInstance();
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		String strPassword1 = null;
		String strPassword2 = null;
		String strEmailAddress = null;
		String strFirstName = null;
		String strUserOrganization = null;
		String strUserRole = null;
		String strMail = null;
		String[] arrUserOrganization = null;
		
		long lCompanyId = PortalConstants.LONG_ZERO;
		long[] lRoleIds = null;
		int iBirthdayMonth = now.get(Calendar.MONTH);
		int iBirthdayDay = now.get(Calendar.DAY_OF_MONTH);
		int iBirthdayYear = now.get(Calendar.YEAR);
		
		boolean isAdd = false;
		boolean hasOrganization = PortalConstants.FALSE;
		boolean bAccountLock = false;
		
		Date dteExpirationDate = null;
		DateFormat format = null;
		
		Organization organization = null;		
		Role role = null;
		
		lCompanyId = themeDisplay.getCompanyId();
		
		Map<String, Object> userDetails = new HashMap<String, Object>();
		PortletSession pSession = actionRequest.getPortletSession(false);
		
		try{
			PortletCommonUtil.isDBConnected();
			
			user = PortalUtil.getSelectedUser(actionRequest);
			isAdd = (user == null);
			
			strEmailAddress = ParamUtil.getString(actionRequest, PortalConstants.EMAIL_ADDRESS);
			strFirstName = ParamUtil.getString(actionRequest, PortalConstants.FIRST_NAME);
			strMail = ParamUtil.getString(actionRequest, PortalConstants.MAIL_ADDRESS);
			strPassword1 = actionRequest.getParameter(PortalConstants.PASSWORD); // if add
			if (!isAdd) {
				strPassword1 = actionRequest.getParameter(PortalConstants.PASSWORD_2); // if edit
			} 
			if(ParamUtil.getString(actionRequest, PortalConstants.GROUP_NAME) == null || ParamUtil.getString(actionRequest, PortalConstants.GROUP_NAME) == PortalConstants.STRING_EMPTY){
				strUserOrganization = PortalConstants.STRING_EMPTY;
				hasOrganization = PortalConstants.FALSE;
			}else{
				arrUserOrganization = ParamUtil.getParameterValues(actionRequest, PortalConstants.GROUP_NAME);
				hasOrganization = PortalConstants.TRUE;
			}
			String strExpirationDate = ParamUtil.getString(actionRequest, PortalConstants.EXPIRATION_DATE);
			bAccountLock = ParamUtil.getBoolean(actionRequest, PortalConstants.ACCOUNT_LOCK);
			
			userDetails.put(PortalConstants.EMAIL_ADDRESS, strEmailAddress);
			userDetails.put(PortalConstants.FIRST_NAME, strFirstName);
			userDetails.put(PortalConstants.MAIL_ADDRESS, strMail);
			userDetails.put(PortalConstants.PASSWORD, strPassword1);
			userDetails.put(PortalConstants.GROUP_NAME, arrUserOrganization);
			userDetails.put(PortalConstants.EXPIRATION_DATE, strExpirationDate);
			userDetails.put(PortalConstants.ACCOUNT_LOCK, bAccountLock);
			
			if(isAdd && strCmd.equals(Constants.UPDATE)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
				throw new NoSuchUserException();
			}
		
			serviceContext = ServiceContextFactory.getInstance(
				User.class.getName(), actionRequest);
			
			//User ID	
			//Checking
			if (CommonUtil.isStringNullOrEmpty(strEmailAddress)){//if ID is empty
				params.put(PortalConstants.STR_EMAIL, strEmailAddress);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_USER_ID));
				log.warn(PortalErrors.ADD_USER_EMPTY_USER_ID, PortalConstants.ADD_EDIT_USER, params, (new EmailAddressException(PortalErrors.ADD_USER_EMPTY_USER_ID)));
				
				pSession.setAttribute(PortalConstants.ERROR, PortalConstants.USER_ID);
				throw new EmailAddressException(PortalErrors.ADD_USER_EMPTY_USER_ID);
			} else if (CommonUtil.getStringBytes(strEmailAddress) > PortalConstants.STRING_BYTE_MAX_75){ // if ID is too long
				params.put(PortalConstants.COND1, CommonUtil.getStringBytes(strEmailAddress));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_USER_ID));
				log.warn(PortalErrors.ADD_USER_TOO_LONG_USER_ID, PortalConstants.ADD_EDIT_USER, params, (new EmailAddressException(PortalErrors.ADD_USER_TOO_LONG_USER_ID)));
				
				pSession.setAttribute(PortalConstants.ERROR, PortalConstants.USER_ID);
				throw new EmailAddressException(PortalErrors.ADD_USER_TOO_LONG_USER_ID);
			}
		
			// Username	
			// FirstName is set to ScreenName
			//Checking
			if(CommonUtil.isStringNullOrEmpty(strFirstName)){ // if username is empty
				params.put(PortalConstants.COND2, strFirstName);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_USERNAME));
				String strErrorCode = PortalConstants.STRING_EMPTY;
				
				if(isAdd){
					strErrorCode = PortalErrors.ADD_USER_EMPTY_USERNAME;
				}else{
					strErrorCode = PortalErrors.EDIT_USER_EMPTY_USERNAME;
				}
				
				log.warn(strErrorCode, PortalConstants.ADD_EDIT_USER, params, (new UserScreenNameException.MustNotBeNull()));
				pSession.setAttribute(PortalConstants.ERROR, PortalConstants.FIRST_NAME);
				throw new UserScreenNameException.MustNotBeNull();
			} else if (CommonUtil.getStringBytes(strFirstName) > PortalConstants.STRING_BYTE_MAX_40) { // if username is too long
				params.put(PortalConstants.COND3, CommonUtil.getStringBytes(strFirstName));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_USERNAME));
				String strErrorCode = PortalConstants.STRING_EMPTY;
				
				if(isAdd){
					strErrorCode = PortalErrors.ADD_USER_TOO_LONG_USERNAME;
				}else{
					strErrorCode = PortalErrors.EDIT_USER_TOO_LONG_USERNAME;
				}
				
				log.warn(strErrorCode, PortalConstants.ADD_EDIT_USER, params, (new UBSPortalUserScreenNameException.MustNotExceedMaximumBytes(strErrorCode)));
				pSession.setAttribute(PortalConstants.ERROR, PortalConstants.FIRST_NAME);
				throw new UBSPortalUserScreenNameException.MustNotExceedMaximumBytes(strErrorCode);
			}
	
		//Email Address
			if (CommonUtil.isStringNullOrEmpty(strMail)) {
				params.put("strMail", strMail);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_EMAIL_ADDRESS));
				
				pSession.setAttribute(PortalConstants.ERROR, PortalConstants.MAIL_ADDRESS);
				
				if (isAdd) {
					throw new UBSPortalException(PortalErrors.ADD_USER_EMPTY_EMAIL_ADDRESS, PortalMessages.EMAIL_ADDRESS_INVALID);
				} else {
					throw new UBSPortalException(PortalErrors.EDIT_USER_EMPTY_EMAIL_ADDRESS, PortalMessages.EMAIL_ADDRESS_INVALID);
				}
			} else if (CommonUtil.getStringBytes(strMail) > PortalConstants.STRING_BYTE_MAX_75) {
				params.put("strMail.getBytes().length", CommonUtil.getStringBytes(strMail));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_EMAIL_ADDRESS));
				
				pSession.setAttribute(PortalConstants.ERROR, PortalConstants.MAIL_ADDRESS);
				
				if (isAdd) {
					throw new UBSPortalException(PortalErrors.ADD_USER_TOO_LONG_EMAIL_ADDRESS, PortalMessages.EMAIL_ADDRESS_INVALID);
				} else {
					throw new UBSPortalException(PortalErrors.EDIT_USER_TOO_LONG_EMAIL_ADDRESS, PortalMessages.EMAIL_ADDRESS_INVALID);
				}
			} else if (!Validator.isEmailAddress(strMail)) {
				params.put("strMail", strMail);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EMAIL_ADDRESS));
				
				pSession.setAttribute(PortalConstants.ERROR, PortalConstants.MAIL_ADDRESS);
				
				if (isAdd) {
					throw new UBSPortalException(PortalErrors.ADD_USER_EMAIL_ADDRESS_INVALID, PortalMessages.EMAIL_ADDRESS_INVALID);
				} else {
					throw new UBSPortalException(PortalErrors.EDIT_USER_EMAIL_ADDRESS_INVALID, PortalMessages.EMAIL_ADDRESS_INVALID);
				}
			}
			
		//Password
			
			//Checking
			if(CommonUtil.isStringNullOrEmpty(strPassword1) && isAdd){//if password is empty
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PASSWORD));
				log.warn(PortalErrors.ADD_USER_EMPTY_PASSWORD, PortalConstants.ADD_EDIT_USER, params, (new UserPasswordException.MustNotBeNull(PortalConstants.LONG_ZERO)));
				
				pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
				throw new UserPasswordException.MustNotBeNull(PortalConstants.LONG_ZERO);
			}else if (!CommonUtil.isStringNullOrEmpty(strPassword1)) {
				String strErrorCode = PortalConstants.STRING_EMPTY;
				
				if(strPassword1.length() < PortalConstants.STRING_BYTE_MIN_8){ //if password is short
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_SHORT, PortalConstants.ERROR_LOG_PARAM_PASSWORD));
					if(isAdd){
						strErrorCode = PortalErrors.ADD_USER_SHORT_PASSWORD;
					}else{
						strErrorCode = PortalErrors.EDIT_USER_SHORT_PASSWORD;
					}
					
					log.warn(strErrorCode, PortalConstants.ADD_EDIT_USER, params, (new UserPasswordException.MustBeLonger(PortalConstants.LONG_ZERO, PortalConstants.STRING_BYTE_MIN_8)));
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
					throw new UserPasswordException.MustBeLonger(PortalConstants.LONG_ZERO, PortalConstants.STRING_BYTE_MIN_8);
				}else if (CommonUtil.getStringBytes(strPassword1) > PortalConstants.STRING_BYTE_MAX_40) { // if password is too long
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PASSWORD));
					strErrorCode = PortalConstants.STRING_EMPTY;
					
					if(isAdd){
						strErrorCode = PortalErrors.ADD_USER_TOO_LONG_PASSWORD;
					}else{
						strErrorCode = PortalErrors.EDIT_USER_TOO_LONG_PASSWORD;
					}
					
					log.warn(strErrorCode, PortalConstants.ADD_EDIT_USER, params, (new UBSPortalUserPasswordException.MustNotExceedMaximumBytes(strErrorCode)));
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
					throw new UBSPortalUserPasswordException.MustNotExceedMaximumBytes(strErrorCode);
				}else if (!Validator.isPassword(strPassword1)){ // if password contains special characters
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PASSWORD));
					strErrorCode = PortalConstants.STRING_EMPTY;
					
					if(isAdd){
						strErrorCode = PortalErrors.ADD_USER_ALPHANUM_PASSWORD;
					}else{
						strErrorCode = PortalErrors.EDIT_USER_ALPHANUM_PASSWORD;
					}
					
					log.warn(strErrorCode, PortalConstants.ADD_EDIT_USER, params, (new UBSPortalUserPasswordException.MustNotContainSpecialCharacters(strErrorCode)));
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
					throw new UBSPortalUserPasswordException.MustNotContainSpecialCharacters(strErrorCode);
				}
			}
				
		
			strPassword2 = strPassword1;
				
			strUserRole = ParamUtil.getString(actionRequest, PortalConstants.ROLE_NAME);
			role = RoleLocalServiceUtil.getRole(lCompanyId, strUserRole);
			if (role.getRoleId() > PortalConstants.INT_ZERO) {
				lRoleIds = new long[PortalConstants.INT_ONE];
				lRoleIds[PortalConstants.INT_ZERO] = role.getRoleId();
			} else {
				lRoleIds = null;
			}
			
		// Organization
			if(strUserRole.equalsIgnoreCase(PortalConstants.GENERAL_USER) || strUserRole.equalsIgnoreCase(PortalConstants.GROUP_ADMINISTRATOR)){	
				
				if(strUserRole.equalsIgnoreCase(PortalConstants.GENERAL_USER) && (arrUserOrganization.length != 1))
				{
					hasOrganization = false;
				}
				
				if(!hasOrganization){
					params.put(PortalConstants.COND5, strUserRole);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
					String strErrorCode = PortalConstants.STRING_EMPTY;
					
					if(isAdd && arrUserOrganization.length == 0){
						strErrorCode = PortalErrors.ADD_USER_EMPTY_GROUP;
					} else if(isAdd && strUserRole.equalsIgnoreCase(PortalConstants.GENERAL_USER) && (arrUserOrganization.length != 1)) {
						strErrorCode = PortalErrors.ADD_USER_INVALID_GROUP;
					} else {
						strErrorCode = PortalErrors.EDIT_USER_EMPTY_GROUP;
					}
					
					log.warn(strErrorCode, PortalConstants.ADD_EDIT_USER, params, (new NoSuchOrganizationException(strErrorCode)));
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.GROUP_NAME);
					throw new NoSuchOrganizationException(strErrorCode);
				}
			}
			
			format = new SimpleDateFormat(PortalConstants.DATE_FORMAT);
			
			// Expiration
//			if (strExpirationDate != null && strExpirationDate != PortalConstants.STRING_EMPTY) {
			if (!CommonUtil.isStringNullOrEmpty(strExpirationDate)) {
				String strErrorCode = PortalConstants.STRING_EMPTY;
				
				if (strExpirationDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					if (isAdd) {
						strErrorCode = PortalErrors.ADD_USER_INVALID_EXPIRATION_DATE;
					} else {
						strErrorCode = PortalErrors.EDIT_USER_INVALID_EXPIRATION_DATE;
					}
					
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXPIRATION_DATE));
					log.warn(strErrorCode, PortalConstants.ADD_EDIT_USER, params, (new UBSPortalException(strErrorCode, PortalMessages.EXPIRY_DATE_INVALID)));
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.EXPIRATION_DATE);
					throw new UBSPortalException(strErrorCode, PortalMessages.EXPIRY_DATE_INVALID);
				} else {
					dteExpirationDate = format.parse(strExpirationDate);
					now.set(Calendar.HOUR, 0);
					now.set(Calendar.MINUTE, 0);
					now.set(Calendar.SECOND, 0);
					now.set(Calendar.HOUR_OF_DAY, 0);
					now.set(Calendar.MILLISECOND, 0);
					if (dteExpirationDate.before(now.getTime())) {
						params.put( PortalConstants.COND6, dteExpirationDate);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_PASSED, PortalConstants.ERROR_LOG_PARAM_EXPIRATION_DATE));
						if(isAdd){
							strErrorCode = PortalErrors.ADD_USER_INVALID_EXPIRATION_DATE;
						}else{
							strErrorCode = PortalErrors.EDIT_USER_INVALID_EXPIRATION_DATE;
						}
						
						log.warn(strErrorCode, PortalConstants.ADD_EDIT_USER, params, (new UBSPortalException(strErrorCode, PortalMessages.EXPIRY_DATE_INVALID)));
						pSession.setAttribute(PortalConstants.ERROR, PortalConstants.EXPIRATION_DATE);
						throw new UBSPortalException(strErrorCode, PortalMessages.EXPIRY_DATE_INVALID);
					}
				}
			} 
			
			//PwdGenerator.getPassword() is set for screenName (unique)
			//Used so that screenName will not be null
			
			String strScreenName = null;
			while (true) {
				strScreenName = PwdGenerator.getPassword();
				try {
					if(CommonUtil.isObjectNull(UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), strScreenName))) {
						break;
					}
				} catch (PortalException e) {
					break;
				} catch (SystemException e) {
					break;
				}
			}
			
			if (isAdd) {
				user = UserServiceUtil.addUser(themeDisplay.getCompanyId(),
						bAutoPassword, strPassword1, strPassword2, bAutoScreenName,
						strScreenName, strEmailAddress, lfacebookId, strOpenId,
						LocaleUtil.getDefault(), strFirstName, strFirstName,
						strFirstName, iPrefixId, iSuffixId, bMale, iBirthdayMonth,
						iBirthdayDay, iBirthdayYear, strJobTitle, lGroupIds,
						lOrganizationIds, lRoleIds, lUserGroupIds, bSendEmail,
						serviceContext);
	
			} else {
				boolean bReset = PortalConstants.FALSE;
				if(user.isPasswordReset()){
					bReset = PortalConstants.TRUE;
				}
				try{
					user = UserServiceUtil.updateUser(user.getUserId(),
							StringPool.BLANK, strPassword1,strPassword2,
							bReset, strReminderQueryQuestion, strReminderQueryAnswer,
							strScreenName, strEmailAddress, lfacebookId, strOpenId,
							strLanguageId, strTimeZoneId, strGreeting, strComments,
							strFirstName, strFirstName, strFirstName, iPrefixId,
							iSuffixId, bMale, iBirthdayMonth, iBirthdayDay,
							iBirthdayYear, strTextSn, strAimSn, strFacebookSn, strIcqSn,
							strJabberSn, strMsnSn, lGroupIds,
							lOrganizationIds, lRoleIds, userGroupRolesList,
							lUserGroupIds, serviceContext);
					
					user = UserServiceUtil.updateLockoutById(user.getUserId(), bAccountLock);
				}catch(UserPasswordException e){
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_PASSWORD_SAME_OLD);
					log.warn(PortalErrors.EDIT_USER_PASSWORD_OLD_SAME, PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
					throw new PasswordExpiredException(PortalErrors.EDIT_USER_PASSWORD_OLD_SAME);
				}
	
			}
			
			// If add/edit is successful
			if (user != null) {
				if(dteExpirationDate != null){
					user.getExpandoBridge().setAttribute(PortalConstants.ACC_EXPIRATION_DATE, dteExpirationDate);
				}else{
					user.getExpandoBridge().setAttribute(PortalConstants.ACC_EXPIRATION_DATE, new Date(0));
				}
	
				user.getExpandoBridge().setAttribute(PortalConstants.EMAIL_ADDRESS, strMail.toLowerCase());
				
				boolean bChangeOrg = false;
				
				if (strUserRole.equalsIgnoreCase(PortalConstants.GENERAL_USER) || strUserRole.equalsIgnoreCase(PortalConstants.GROUP_ADMINISTRATOR)) {
					// Clear user organization
					OrganizationLocalServiceUtil.clearUserOrganizations(user.getUserId());
					
					List<Organization> userOrgList = OrganizationLocalServiceUtil.getUserOrganizations(user.getUserId());
					
					// Loop selected organization
					for (String  strUserOrg : arrUserOrganization)
					{
						// get the organizations information
						organization = OrganizationLocalServiceUtil.getOrganization(lCompanyId, strUserOrg);
						// add as the users organization
						UserLocalServiceUtil.addOrganizationUser(organization.getOrganizationId(), user.getUserId());
					}
				}
				
				List<Organization> userOrgList = OrganizationLocalServiceUtil.getUserOrganizations(user.getUserId());
								
				int emailEvent = PortalConstants.INT_ZERO;
				HttpSession httpSession = ControllerHelper.getHttpSession(actionRequest);
				Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
				long lUserId = PortalConstants.LONG_ZERO;
				Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
				int iUserRole = PortalConstants.INT_ZERO;
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
				}
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				if (isAdd) {
					emailEvent = PortalConstants.EMAIL_EVENT_ADD_ACCOUNT;
				} else {
					if (!CommonUtil.isStringNullOrEmpty(strPassword1) && (iUserRole == UserRole.OVERALL_ADMIN.getInteger()|| iUserRole == UserRole.GROUP_ADMIN.getInteger())) {
						MailUtil.sendEmail(PortalConstants.EMAIL_EVENT_PASSWORD_CHANGE, user.getUserId(), null);
					}
					
					emailEvent = PortalConstants.EMAIL_EVENT_UPDATE_ACCOUNT;
				}
				
				if (iUserRole == UserRole.SYSTEM_ADMIN.getInteger() ||
						iUserRole == UserRole.OVERALL_ADMIN.getInteger() || 
						iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
					MailUtil.sendEmail(emailEvent, lUserId, user.getFirstName());
				}
			}
		
		}catch(Exception e){
			if (e instanceof UBSPortalException) {
				String errorCode = ((UBSPortalException) e).getErrorCode();
				if (errorCode.equals(PortalErrors.ORM_EXCEPTION)) {
					log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.ADD_EDIT_USER, params, e);
				} else {
					if (errorCode.equals(PortalErrors.ADD_USER_EMPTY_EMAIL_ADDRESS)
							|| errorCode.equals(PortalErrors.EDIT_USER_EMPTY_EMAIL_ADDRESS)
							|| errorCode.equals(PortalErrors.ADD_USER_TOO_LONG_EMAIL_ADDRESS)
							|| errorCode.equals(PortalErrors.EDIT_USER_TOO_LONG_EMAIL_ADDRESS)
							|| errorCode.equals(PortalErrors.ADD_USER_EMAIL_ADDRESS_INVALID)
							|| errorCode.equals(PortalErrors.EDIT_USER_EMAIL_ADDRESS_INVALID)
							|| errorCode.equals(PortalErrors.ADD_USER_INVALID_EXPIRATION_DATE)
							|| errorCode.equals(PortalErrors.EDIT_USER_INVALID_EXPIRATION_DATE)) {
						log.warn(errorCode, PortalConstants.ADD_EDIT_USER, params, e);
					} else {
						log.debug(errorCode, PortalConstants.ADD_EDIT_USER, params, e);
					}
				}
			} if(isAdd){
				if(e instanceof EmailAddressException){
					params.put(PortalConstants.STR_EMAIL, strEmailAddress);
					log.warn(e.getMessage(), PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.USER_ID);
				}else if(e instanceof UserEmailAddressException.MustNotBeDuplicate){
					params.put(PortalConstants.STR_EMAIL, strEmailAddress);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_ALREADY_EXISTS, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					log.warn(PortalErrors.ADD_USER_TAKEN_USER_ID, PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.USER_ID);
				}else if(e instanceof UserScreenNameException.MustNotBeNull){
					params.put(PortalConstants.STR_FIRSTNAME, strFirstName);
					log.warn(PortalErrors.ADD_USER_EMPTY_USERNAME, PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.FIRST_NAME);
				}else if(e instanceof UserScreenNameException.MustNotBeDuplicate){
					params.put(PortalConstants.STR_EMAIL, strEmailAddress);
					log.warn(PortalErrors.ADD_USER_TAKEN_USER_ID, PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.FIRST_NAME);
				}else if(e instanceof UserPasswordException){
					log.warn(PortalErrors.ADD_USER_ALPHANUM_PASSWORD, PortalConstants.ADD_EDIT_USER, null, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
				}else if(e instanceof UBSPortalUserPasswordException){
					log.warn(e.getMessage(), PortalConstants.ADD_EDIT_USER, null, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
				}else if(e instanceof PasswordExpiredException) {
					log.warn(e.getMessage(), PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
				}else if(e instanceof NoSuchOrganizationException) {
					if (!e.getMessage().equals(PortalErrors.ADD_USER_EMPTY_GROUP)
							&& !e.getMessage().equals(PortalErrors.EDIT_USER_EMPTY_GROUP) 
							&& !e.getMessage().equals(PortalErrors.ADD_USER_INVALID_GROUP)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_GROUP));
					}
					log.warn(e.getMessage(), PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.GROUP_NAME);
				}else if(e instanceof PortalException){
					params.put(PortalConstants.ADD, PortalConstants.ADD_USER);
					params.put(PortalConstants.STR_EMAIL, strEmailAddress);
					params.put(PortalConstants.STR_FIRSTNAME, strFirstName);
					params.put(PortalConstants.SERVICE_CONTEXT, serviceContext);
					params.put(PortalConstants.ORGANIZATION, organization);
					log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.ADD_EDIT_USER, params, e);
					
					if (CommonUtil.isStringNullOrEmpty(String.valueOf(actionRequest.getAttribute(PortalConstants.ERROR)))) {
						pSession.setAttribute(PortalConstants.ERROR, PortalConstants.USER_ID);						
					}
				}else if(e instanceof SystemException){
					params.put(PortalConstants.EDIT, PortalConstants.EDIT_USER);
					params.put(PortalConstants.STR_EMAIL, strEmailAddress);
					params.put(PortalConstants.STR_FIRSTNAME, strFirstName);
					params.put(PortalConstants.SERVICE_CONTEXT, serviceContext);
					params.put(PortalConstants.ORGANIZATION, organization);
					log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.ADD_EDIT_USER, params, e);
					
					if (CommonUtil.isStringNullOrEmpty(String.valueOf(actionRequest.getAttribute(PortalConstants.ERROR)))) {
						pSession.setAttribute(PortalConstants.ERROR, PortalConstants.USER_ID);						
					}
				}
			}else{
				if(e instanceof UserScreenNameException.MustNotBeDuplicate){
					params.put(PortalConstants.STR_EMAIL, strEmailAddress);
					log.warn(PortalErrors.ADD_USER_TAKEN_USER_ID, PortalConstants.ADD_EDIT_USER, params, (new UserScreenNameException.MustNotBeDuplicate(PortalConstants.LONG_ZERO, PortalConstants.STRING_EMPTY)));
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.FIRST_NAME);
				}else if(e instanceof UserPasswordException){
					log.warn(PortalErrors.EDIT_USER_PASSWORD_OLD_SAME, PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.PASSWORD);
				}else if(e instanceof NoSuchOrganizationException) {
					if (!e.getMessage().equals(PortalErrors.ADD_USER_EMPTY_GROUP)
							&& !e.getMessage().equals(PortalErrors.EDIT_USER_EMPTY_GROUP) 
							&& !e.getMessage().equals(PortalErrors.ADD_USER_INVALID_GROUP)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_GROUP));
					}
					log.warn(e.getMessage(), PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.GROUP_NAME);
				} else if (e instanceof UserScreenNameException.MustNotBeNull) {
					params.put(PortalConstants.STR_FIRSTNAME, strFirstName);
					log.warn(PortalErrors.EDIT_USER_EMPTY_USERNAME, PortalConstants.ADD_EDIT_USER, params, e);
					pSession.setAttribute(PortalConstants.ERROR, PortalConstants.FIRST_NAME);
				}
			}
			
			pSession.setAttribute("userDetails", userDetails);
			throw e;
		}
		finally{
			params.clear();
			params = null;
			lRoleIds = null;
		}
	}
	
	protected static void deleteUser(ActionRequest actionRequest) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		long[] lDeleteUserIds = StringUtil.split(
				ParamUtil.getString(actionRequest, PortalConstants.DELETE_USER_IDS), PortalConstants.LONG_ZERO);

		try {
			PortletCommonUtil.isDBConnected();
			
			for (long lDeleteUserId : lDeleteUserIds) {
				User user = UserLocalServiceUtil.getUser(lDeleteUserId);
				ProjectUsersLocalServiceUtil.deleteProjectUser(user.getEmailAddress());
				UserLocalServiceUtil.deleteUser(lDeleteUserId);
			}
			
			actionRequest.getPortletSession(false).setAttribute(PortalConstants.PARAM_IS_FROM_DELETE_USER, true, PortletSession.PORTLET_SCOPE);
		} catch (NoSuchUserException e) {
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.DELETE_USER_F, params, e);	
		} catch (SystemException e) {
			params.put(PortalConstants.LONG_DELETE_USER_IDS, lDeleteUserIds);
			
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.DELETE_USER_F, params, e);	
		} catch (PortalException e) {
			params.put(PortalConstants.LONG_DELETE_USER_IDS, lDeleteUserIds);
			
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.DELETE_USER_F, params, e);	
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				actionRequest.setAttribute("fromDeleteUser", true);
				log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.DELETE_USER_F, params, e);
				throw e;
			}
		} finally {
			params.clear();
			params = null;
		}
	}
	
	protected static List<User> searchUser(ActionRequest actionRequest) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession();
		Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
		String strRole = PortalConstants.STRING_EMPTY;
		
		if (!CommonUtil.isObjectNull(oUserRole)) {
			strRole = oUserRole.toString();
		}
		
		boolean bHasEmailAddress = PortalConstants.FALSE;// (strEmailAddress != null && strEmailAddress != "");
		String strEmailAddress = ParamUtil.getString(actionRequest, PortalConstants.USER_ID_REQUEST);
		
		if(ParamUtil.getString(actionRequest, PortalConstants.USER_ID_REQUEST) == null || ParamUtil.getString(actionRequest, PortalConstants.USER_ID_REQUEST) == PortalConstants.STRING_EMPTY){
			strEmailAddress = PortalConstants.STRING_EMPTY;
			bHasEmailAddress = PortalConstants.FALSE;
		}else{
			strEmailAddress = ParamUtil.getString(actionRequest, PortalConstants.USER_ID_REQUEST);
			strEmailAddress = modifySearch(strEmailAddress);
			bHasEmailAddress = PortalConstants.TRUE;
		}
		
		
		boolean bHasScreenName = PortalConstants.FALSE;//(strScreenName != null && strScreenName != "");
		String strScreenName = ParamUtil.getString(actionRequest, PortalConstants.USER_NAME_REQUEST);
		if(ParamUtil.getString(actionRequest,  PortalConstants.USER_NAME_REQUEST) == null || ParamUtil.getString(actionRequest,  PortalConstants.USER_NAME_REQUEST) == PortalConstants.STRING_EMPTY){
			strScreenName = PortalConstants.STRING_EMPTY;
			bHasScreenName = PortalConstants.FALSE;
		}else{
			strScreenName = ParamUtil.getString(actionRequest, PortalConstants.USER_NAME_REQUEST);
			bHasScreenName = PortalConstants.TRUE;
		}
		
		boolean bHasMailAddress = PortalConstants.FALSE;
		String strMailAddress = ParamUtil.getString(actionRequest, PortalConstants.MAIL_ADDRESS_REQUEST);
		if (!CommonUtil.isStringNullOrEmpty(strMailAddress)) {
			bHasMailAddress = PortalConstants.TRUE;
		}
		
		
		DateFormat format = new SimpleDateFormat(PortalConstants.DATE_FORMAT);
		
		String strLastLoginDateLow = ParamUtil.getString(actionRequest, PortalConstants.LAST_LOGIN_DATE_LOW);
		String strLastLoginDateHigh = ParamUtil.getString(actionRequest, PortalConstants.LAST_LOGIN_DATE_HIGH);
		String strExpirationDateLow = ParamUtil.getString(actionRequest, PortalConstants.EXPIRATION_DATE_LOW);
		String strExpirationDateHigh = ParamUtil.getString(actionRequest, PortalConstants.EXPIRATION_DATE_HIGH);
		String strGroupName = PortalConstants.STRING_EMPTY;// ParamUtil.getString(actionRequest, "group_name");
		boolean bHasGroup = (strGroupName != null && strGroupName != PortalConstants.STRING_EMPTY);
		
		if(ParamUtil.getString(actionRequest, PortalConstants.GROUP_NAME_REQUEST) == null || ParamUtil.getString(actionRequest, PortalConstants.GROUP_NAME_REQUEST).isEmpty()){
			strGroupName = PortalConstants.STRING_EMPTY;
			bHasGroup = PortalConstants.FALSE;
		}else{
			strGroupName = ParamUtil.getString(actionRequest, PortalConstants.GROUP_NAME_REQUEST);
			bHasGroup = PortalConstants.TRUE;
		}
		
		int iAccountLock = ParamUtil.getInteger(actionRequest, PortalConstants.ACCOUNT_LOCK_REQUEST);
		
		pSession.setAttribute(PortalConstants.GROUP_NAME_REQUEST, strGroupName, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.USER_ID_REQUEST, strEmailAddress, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.USER_NAME_REQUEST, strScreenName, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.MAIL_ADDRESS_REQUEST, strMailAddress, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.LAST_LOGIN_LOW, strLastLoginDateLow, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.LAST_LOGIN_HIGH, strLastLoginDateHigh, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.EXPIRY_LOW, strExpirationDateLow, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.EXPIRY_HIGH, strExpirationDateHigh, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.ACCOUNT_LOCK_REQUEST, iAccountLock, PortletSession.PORTLET_SCOPE);
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long lCompanyId = themeDisplay.getCompanyId();

		Set<User> userList = new LinkedHashSet<User>();
		pSession.setAttribute(PortalConstants.IS_SEARCHED, PortalConstants.TRUE, PortletSession.PORTLET_SCOPE);
		
		if (CommonUtil.getStringBytes(strEmailAddress) > PortalConstants.STRING_BYTE_MAX_75){
			params.put(PortalConstants.COND1, CommonUtil.getStringBytes(strEmailAddress));
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_USER_ID));
			log.warn(PortalErrors.SEARCH_USER_INVALID_USER_ID, PortalConstants.SEARCH_USER_F, params, (new UBSPortalUserEmailAddressException.MustNotExceedMaximumBytes(null)));
			throw new UBSPortalUserEmailAddressException.MustNotExceedMaximumBytes(null);
		}
		
		if (CommonUtil.getStringBytes(strScreenName) > PortalConstants.STRING_BYTE_MAX_40) {
			params.put(PortalConstants.COND7, CommonUtil.getStringBytes(strScreenName));
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_USERNAME));
			log.warn(PortalErrors.SEARCH_USER_INVALID_NAME, PortalConstants.SEARCH_USER_F, params, (new UBSPortalUserScreenNameException.MustNotExceedMaximumBytes(null)));
			throw new UBSPortalUserScreenNameException.MustNotExceedMaximumBytes(null);
		}
		
		if (CommonUtil.getStringBytes(strMailAddress) > PortalConstants.STRING_BYTE_MAX_75) {
			params.put("strMailAddress.getBytes().length", CommonUtil.getStringBytes(strMailAddress));
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_EMAIL_ADDRESS));
			log.warn(PortalErrors.SEARCH_USER_TOO_LONG_EMAIL_ADDRESS, PortalConstants.SEARCH_USER_F, params, new UBSPortalException(PortalErrors.SEARCH_USER_TOO_LONG_EMAIL_ADDRESS, PortalMessages.EMAIL_ADDRESS_INVALID));
			throw new UBSPortalException(PortalErrors.SEARCH_USER_TOO_LONG_EMAIL_ADDRESS, PortalMessages.EMAIL_ADDRESS_INVALID);
		}
		
		//Last Login Date Checking
		Date dteLastLoginDateLow = null;
		if (!CommonUtil.isStringNullOrEmpty(strLastLoginDateLow)) {
			if (strLastLoginDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				params.put(PortalConstants.LAST_LOGIN_DATE_LOW, strLastLoginDateLow);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_LAST_LOGIN_DATE));
				log.warn(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE, PortalConstants.SEARCH_USER_F, params, new ParseException(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE));
				throw new ParseException(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE);
			}
			dteLastLoginDateLow = format.parse(strLastLoginDateLow);
		}
		
		Calendar dteLastLoginDateHigh = null;
		if (!CommonUtil.isStringNullOrEmpty(strLastLoginDateHigh)) {
			if (strLastLoginDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				params.put(PortalConstants.LAST_LOGIN_DATE_HIGH, strLastLoginDateHigh);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_LAST_LOGIN_DATE));
				log.warn(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE, PortalConstants.SEARCH_USER_F, params, new ParseException(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE));
				throw new ParseException(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE);
			}
			dteLastLoginDateHigh = Calendar.getInstance();
			dteLastLoginDateHigh.setTime(format.parse(strLastLoginDateHigh));
		}
		
		boolean bHasLastLoginDateLow = (dteLastLoginDateLow != null);
		boolean bHasLastLoginDateHigh = (dteLastLoginDateHigh != null);
		
		if(bHasLastLoginDateLow && bHasLastLoginDateHigh){
			if (dteLastLoginDateHigh.getTime().before(dteLastLoginDateLow)){
				params.put(PortalConstants.DATE_LAST_LOGIN_LOW, dteLastLoginDateLow);
				params.put(PortalConstants.DATE_LAST_LOGIN_HIGH, dteLastLoginDateHigh);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_LAST_LOGIN_DATE, PortalConstants.ERROR_LOG_PARAM_LAST_LOGIN_DATE.toLowerCase()));
				log.warn(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE, PortalConstants.SEARCH_USER_F, params, new ParseException(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE));
				throw new ParseException(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE);
			}
		}
		
		//Expiration Date Checking
		Date dteExpirationDateLow = null;
		if (!CommonUtil.isStringNullOrEmpty(strExpirationDateLow)) {
			if (strExpirationDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				params.put(PortalConstants.EXPIRATION_DATE_LOW, strExpirationDateLow);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXPIRATION_DATE));
				log.warn(PortalErrors.SEARCH_USER_INVALID_DATE, PortalConstants.SEARCH_USER_F, params, new ParseException(PortalErrors.SEARCH_USER_INVALID_DATE));
				throw new ParseException(PortalErrors.SEARCH_USER_INVALID_DATE);
			}
			dteExpirationDateLow = format.parse(strExpirationDateLow);
		}
		
		Date dteExpirationDateHigh = null;
		if (!CommonUtil.isStringNullOrEmpty(strExpirationDateHigh)) {
			if (strExpirationDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				params.put(PortalConstants.EXPIRATION_DATE_HIGH, strExpirationDateHigh);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_EXPIRATION_DATE));
				log.warn(PortalErrors.SEARCH_USER_INVALID_DATE, PortalConstants.SEARCH_USER_F, params, new ParseException(PortalErrors.SEARCH_USER_INVALID_DATE));
				throw new ParseException(PortalErrors.SEARCH_USER_INVALID_DATE);
			}
			dteExpirationDateHigh = format.parse(strExpirationDateHigh);
		}
		
		boolean bHasExpirationDateLow = (dteExpirationDateLow != null);
		boolean bHasExpirationDateHigh = (dteExpirationDateHigh != null);
		
		if(bHasExpirationDateLow && bHasExpirationDateHigh){
			if (dteExpirationDateHigh.before(dteExpirationDateLow)){
				params.put(PortalConstants.DATE_EXPIRATION_LOW, dteExpirationDateLow);
				params.put(PortalConstants.DATE_EXPIRATION_HIGH, dteExpirationDateHigh);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_EXPIRATION_DATE, PortalConstants.ERROR_LOG_PARAM_EXPIRATION_DATE.toLowerCase()));
				log.warn(PortalErrors.SEARCH_USER_INVALID_DATE, PortalConstants.SEARCH_USER_F, params, new ParseException(PortalErrors.SEARCH_USER_INVALID_DATE));
				throw new ParseException(PortalErrors.SEARCH_USER_INVALID_DATE);
			}
		}
		
		int iUserRole = 0;
		if (!CommonUtil.isStringNullOrEmpty(strRole)) {
			iUserRole = Integer.parseInt(strRole);

			if(iUserRole == UserRole.SYSTEM_ADMIN.getInteger()){
				strRole = PortalConstants.OVERALL_ADMINISTRATOR;
			}else if(iUserRole == UserRole.OVERALL_ADMIN.getInteger()){
				strRole = PortalConstants.GENERAL_USER;
			}else if(iUserRole == UserRole.ADMINISTRATOR.getInteger()){
				strRole = PortalConstants.SYSTEM_ADMINISTRATOR;
			}else if(iUserRole == UserRole.GROUP_ADMIN.getInteger()){
				strRole = PortalConstants.GENERAL_USER;
			}
		}
		
		Role role = null;
		long lRoleId = PortalConstants.LONG_ZERO;
		Collection<User> uListByRole = null;
		Collection<User> uListByEmail = null;
		Collection<User> uListByScreenName = null;
		Collection<User> uListByMailAddress = null;
		long lOrgId = PortalConstants.LONG_ZERO;
		Collection<User> uListByOrganization = null;
		Iterator<User> it = null;
		DateFormat formatter = null;
		
		try{
			PortletCommonUtil.isDBConnected();
			if(userList == null || userList.isEmpty()){
				//Getting all users based on the role
				
				if(iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()){
					// get general users
					role = RoleServiceUtil.getRole(lCompanyId, PortalConstants.GENERAL_USER);
					lRoleId = role.getRoleId();
					uListByRole = UserLocalServiceUtil.getRoleUsers(lRoleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
					userList.addAll(uListByRole);
					
					// get group admins
					role = RoleServiceUtil.getRole(lCompanyId, PortalConstants.GROUP_ADMINISTRATOR);
					lRoleId = role.getRoleId();
					uListByRole = UserLocalServiceUtil.getRoleUsers(lRoleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
					userList.addAll(uListByRole);
				}else{
					role = RoleServiceUtil.getRole(lCompanyId, strRole);
					lRoleId = role.getRoleId();
					uListByRole = UserLocalServiceUtil.getRoleUsers(lRoleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
					userList.addAll(uListByRole);
				}
				
				
				//For group name
				if(bHasGroup){
					lOrgId = OrganizationServiceUtil.getOrganizationId(lCompanyId, strGroupName);
					uListByOrganization = UserServiceUtil.getOrganizationUsers(lOrgId);
					if (!CommonUtil.isCollectionNullOrEmpty(uListByOrganization)) {
						userList.retainAll(uListByOrganization);
					} else {
						userList = new LinkedHashSet<User>();
					}
				}
				
				//For user id
				if(bHasEmailAddress) {
					uListByEmail = UserLocalServiceUtil.search(lCompanyId, null, null, null, null, strEmailAddress, PortalConstants.INT_ZERO, null, PortalConstants.FALSE, QueryUtil.ALL_POS, QueryUtil.ALL_POS, (new UserEmailAddressComparator()));
					if (!CommonUtil.isCollectionNullOrEmpty(uListByEmail)) {
						userList.retainAll(uListByEmail);
						
						userList = filterUserByUserID(userList, strEmailAddress);
					} else {
						userList = new LinkedHashSet<User>();
					}
				}
				
				//For screen name
				if(bHasScreenName){
					uListByScreenName = UserLocalServiceUtil.search(lCompanyId, strScreenName, null, null, null, null, PortalConstants.INT_ZERO, null, PortalConstants.FALSE, QueryUtil.ALL_POS, QueryUtil.ALL_POS, (new UserEmailAddressComparator()));
					if (!CommonUtil.isCollectionNullOrEmpty(uListByScreenName)) {
						userList.retainAll(uListByScreenName);
						
						userList = filterUserByUserName(userList, strScreenName);
					} else {
						userList = new LinkedHashSet<User>();
					}
				}
				
				//For email address
				if (bHasMailAddress) {
					for (User user : userList) {
						String strUserMail = GetterUtil.getString(ExpandoValueLocalServiceUtil.getData(user.getCompanyId(),
								User.class.getName(), PortalConstants.CUSTOM_FIELDS, PortalConstants.EMAIL_ADDRESS, user.getUserId()));
						if (uListByMailAddress == null || uListByMailAddress.isEmpty()) {
							uListByMailAddress = new ArrayList<User>();
						}
						
						if (strUserMail.toLowerCase().contains(strMailAddress.toLowerCase())) {
							uListByMailAddress.add(user);
						}
					}
					
					if (!CommonUtil.isCollectionNullOrEmpty(uListByMailAddress)) {
						userList.retainAll(uListByMailAddress);
					} else {
						userList = new LinkedHashSet<User>();
					}
				}
				
				//For Last Login Date Low
				if(bHasLastLoginDateLow){
					formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT);
					it = userList.iterator();
					while(it.hasNext()){
						User u = it.next();
						Date dteLastLoginDate = u.getLastLoginDate();
						
						if (dteLastLoginDate != null) {
							if(dteLastLoginDateLow.after(dteLastLoginDate)){
								it.remove();
							}
						}
					}
				}
				
				//For Last Login Date High
				if(bHasLastLoginDateHigh){
					formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT);
					it = userList.iterator();
					while(it.hasNext()){
						User u = it.next();
						Date dteLastLoginDate = u.getLastLoginDate();
						
						if (dteLastLoginDate != null) {
							dteLastLoginDateHigh.set(Calendar.HOUR_OF_DAY, 23);
							dteLastLoginDateHigh.set(Calendar.MINUTE, 59);
							dteLastLoginDateHigh.set(Calendar.SECOND, 59);
							if(dteLastLoginDateHigh.getTime().before(dteLastLoginDate)){
								it.remove();
							}
						} else {
							it.remove();
						}
					}
				}
				
				//For Expiration Date Low
				if(bHasExpirationDateLow){
					formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT);
					it = userList.iterator();
					Calendar defaultDate = Calendar.getInstance();
					defaultDate.setTime(new Date(0));
					
					while(it.hasNext()){
						User u = it.next();
						
						Calendar dteExpirationDate = Calendar.getInstance();
						dteExpirationDate.setTime(GetterUtil.getDate(u.getExpandoBridge().getAttribute(PortalConstants.ACC_EXPIRATION_DATE), formatter));
						
						boolean bIsDefault = false;
						
						if (dteExpirationDate.get(Calendar.YEAR) == defaultDate.get(Calendar.YEAR)
								&& dteExpirationDate.get(Calendar.MONTH) == defaultDate.get(Calendar.MONTH)
								&& dteExpirationDate.get(Calendar.DAY_OF_MONTH) == defaultDate.get(Calendar.DAY_OF_MONTH)) {
							bIsDefault = true;
						}
						
						if(!bIsDefault && dteExpirationDateLow.after(dteExpirationDate.getTime())){
							it.remove();
						}
					}
				}
				
				//For Expiration Date High
				if(bHasExpirationDateHigh){
					formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT);
					it = userList.iterator();
					Calendar defaultDate = Calendar.getInstance();
					defaultDate.setTime(new Date(0));
					
					while(it.hasNext()){
						User u = it.next();
						Calendar dteExpirationDate = Calendar.getInstance();
						dteExpirationDate.setTime(GetterUtil.getDate(u.getExpandoBridge().getAttribute(PortalConstants.ACC_EXPIRATION_DATE), formatter));
						
						if(dteExpirationDate != null
								&& ((dteExpirationDate.get(Calendar.YEAR) == defaultDate.get(Calendar.YEAR)
								&& dteExpirationDate.get(Calendar.MONTH) == defaultDate.get(Calendar.MONTH)
								&& dteExpirationDate.get(Calendar.DAY_OF_MONTH) == defaultDate.get(Calendar.DAY_OF_MONTH))
								|| dteExpirationDateHigh.before(dteExpirationDate.getTime()))) {
							it.remove();
						}
					}
				}
				
				//For lockout value
				if((iAccountLock == PortalConstants.ACCOUNT_LOCK_FALSE) || (iAccountLock == PortalConstants.ACCOUNT_LOCK_TRUE)){
					it = userList.iterator();
					while(it.hasNext()){
						User u = it.next();
						if((iAccountLock == PortalConstants.ACCOUNT_LOCK_FALSE) && (u.getLockout()) // if searched lockout is false and user lockout is true
							|| ((iAccountLock == PortalConstants.ACCOUNT_LOCK_TRUE) && (!u.getLockout())) // if searched lockout is true and user lockout is false
							){ 
							it.remove();
						}
					}
				}
			}
			
		} catch (ParseException e) {
			log.warn(e.getMessage(), PortalConstants.SEARCH_USER_F, params, e);
			throw e;
		} catch (PortalException e) {
			params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
			params.put(PortalConstants.STR_ROLE, strRole);
			params.put(PortalConstants.STR_GROUP_NAME, strGroupName);
			params.put(PortalConstants.LONG_ORG_ID, lOrgId);
			params.put(PortalConstants.LIST_BY_ROLE, uListByRole);
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.SEARCH_USER_F, params, e);	
		} catch (SystemException e) {
			params.put(PortalConstants.LONG_ROLE_ID, lRoleId);
			params.put(PortalConstants.STR_ROLE, strRole);
			params.put(PortalConstants.STR_EMAIL, strEmailAddress);
			params.put(PortalConstants.STR_SCREEN_NAME, strScreenName);
			params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
			params.put(PortalConstants.STR_GROUP_NAME, strGroupName);
			params.put(PortalConstants.LONG_ORG_ID, lOrgId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.SEARCH_USER_F, params, e);	
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				pSession.setAttribute(PortalConstants.IS_SEARCHED, PortalConstants.TRUE);
				log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.SEARCH_USER_F, params, e);
				throw e;
			}
		}
		finally{
			params.clear();
			params = null;
			role = null;
			uListByRole = null;
			uListByEmail = null;
			uListByScreenName = null;
			uListByOrganization = null;
			it = null;
			formatter = null;
		}
		
		return new ArrayList<User> (userList);
	}
	
	private static Set<User> filterUserByUserName (Set<User> userList, String userName) {
		Set<User> usersList = new LinkedHashSet<User>();
		
		if (userList != null && !CommonUtil.isStringNullOrEmpty(userName)) {
			for (User user : userList) {
				if (user.getFirstName().toLowerCase().contains(userName.toLowerCase())) {
					
					usersList.add(user);
				}
			}
		} else {
			usersList = userList;
		}
		
		return usersList;
	}
	
	public static String modifySearch(String s){
		String ret = StringUtil.replace(s, "%", PortalConstants.PERCENT);
		
		return ret;
	}
	
	private static Set<User> filterUserByUserID (Set<User> userList, String userId) {
		Set<User> usersList = new LinkedHashSet<User>();
				
		if (userList != null && !CommonUtil.isStringNullOrEmpty(userId)) {
			for (User user : userList) {
				if (user.getEmailAddress().toLowerCase().contains(userId.toLowerCase())) {
					
					usersList.add(user);
				}
			}
		} else {
			usersList = userList;
		}
		
		return usersList;
	}
	
	protected static List<User> clearSearchUser (ActionRequest actionRequest) throws Exception {
		Map<String, Object> params 	= new HashMap<String, Object>();
		HttpSession session 		= ControllerHelper.getHttpSession(actionRequest);
		Object oUserRole 			= session.getAttribute(PortalConstants.USER_ROLE);
		Object oUserID				= session.getAttribute(PortalConstants.USER_ID);
		String strRole 				= PortalConstants.STRING_EMPTY;
		long lCompanyId 			= PortalConstants.LONG_ZERO;
		List<User> userList 		= null;
		List<User> tempUserList 	= null;
		List<User> tempUserList2 	= new ArrayList<User>();
		int iUserRole 				= 0;
		long iUserId 				= 0;
		
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isObjectNull(oUserRole)) {
					strRole = oUserRole.toString();
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strRole)) {
					iUserRole = Integer.parseInt(strRole);

					if(iUserRole == UserRole.SYSTEM_ADMIN.getInteger()){
						strRole = PortalConstants.OVERALL_ADMINISTRATOR;
					}else if(iUserRole == UserRole.OVERALL_ADMIN.getInteger()){
						strRole = PortalConstants.GENERAL_USER;
					}else if(iUserRole == UserRole.ADMINISTRATOR.getInteger()){
						strRole = PortalConstants.SYSTEM_ADMINISTRATOR;
					}else if(iUserRole == UserRole.GROUP_ADMIN.getInteger()){
						strRole = PortalConstants.GROUP_ADMINISTRATOR;
					}
				}
				
				ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				lCompanyId 		= themeDisplay.getCompanyId();
				Role role 		= null;
				long lRoleId 	= PortalConstants.LONG_ZERO;
				
				// if Group Admin is logged in
				if(iUserRole == UserRole.GROUP_ADMIN.getInteger()){
					tempUserList = null;
					iUserId = Long.parseLong(oUserID.toString());
					List<Organization> organizationList = OrganizationLocalServiceUtil.getUserOrganizations(iUserId);
					
					for (Organization o : organizationList)
					{
						tempUserList = UserLocalServiceUtil.getOrganizationUsers(o.getOrganizationId());
						
						if(userList == null){
							userList = tempUserList;
						}else{
							userList.addAll(tempUserList);
						}
					}
					
					if(userList != null){
						// create has set. Set will contains only unique objects
					 	HashSet<User> hashSet = new HashSet(userList);
						for (User user_item : hashSet) {
				       		tempUserList2.add(user_item);
				       	}
						
						userList = tempUserList2;
					}
				} else if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()){
					// if overall user is logged in
					strRole = PortalConstants.GENERAL_USER;
					role = RoleServiceUtil.getRole(lCompanyId, strRole);
					lRoleId = role.getRoleId();
					userList = UserLocalServiceUtil.getRoleUsers(lRoleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
					
					strRole = PortalConstants.GROUP_ADMINISTRATOR;
					role = RoleServiceUtil.getRole(lCompanyId, strRole);
					lRoleId = role.getRoleId();
					tempUserList = UserLocalServiceUtil.getRoleUsers(lRoleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
					
					if(userList == null){
						userList = tempUserList;
					}else{
						userList.addAll(tempUserList);
					}
					
				} else {
					role = RoleServiceUtil.getRole(lCompanyId, strRole);
					lRoleId = role.getRoleId();
					userList = UserLocalServiceUtil.getRoleUsers(lRoleId, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
				}
				
				PortletSession pSession = actionRequest.getPortletSession();
				pSession.removeAttribute(PortalConstants.IS_SEARCHED);
				pSession.removeAttribute(PortalConstants.GROUP_NAME_REQUEST);
				pSession.removeAttribute(PortalConstants.USER_ID_REQUEST);
				pSession.removeAttribute(PortalConstants.USER_NAME_REQUEST);
				pSession.removeAttribute(PortalConstants.MAIL_ADDRESS_REQUEST);
				pSession.removeAttribute(PortalConstants.LAST_LOGIN_LOW);
				pSession.removeAttribute(PortalConstants.LAST_LOGIN_HIGH);
				pSession.removeAttribute(PortalConstants.EXPIRY_LOW);
				pSession.removeAttribute(PortalConstants.EXPIRY_HIGH);
				pSession.removeAttribute(PortalConstants.ACCOUNT_LOCK_REQUEST);
				PortalUtil.getHttpServletRequest(actionRequest).getSession().removeAttribute("searchedUser");
			}
		} catch (NumberFormatException e) {
			params.put(PortalConstants.STR_ROLE, strRole);
			log.debug(PortalErrors.NUMBER_FORMAT_EXCEPTION, PortalConstants.CLEAR_SEARCH_USER_F, params, e);	
		} catch (SystemException e) {
			params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
			params.put(PortalConstants.STR_ROLE, strRole);
			params.put(PortalConstants.LIST_BY_ROLE, userList);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.CLEAR_SEARCH_USER_F, params, e);	
		} catch (PortalException e) {
			params.put(PortalConstants.LONG_COMPANY_ID, lCompanyId);
			params.put(PortalConstants.STR_ROLE, strRole);
			params.put(PortalConstants.LIST_BY_ROLE, userList);
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.CLEAR_SEARCH_USER_F, params, e);	
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.CLEAR_SEARCH_USER_F, params, e);
				throw e;
			}
		} finally {
			params.clear();
			params = null;
		}
		
		return userList;
	}
	
	private static long[] getLongArray(PortletRequest portletRequest, String name) {
		String value = portletRequest.getParameter(name);
		if (value == null)
			return null;

		return StringUtil.split(GetterUtil.getString(value), PortalConstants.LONG_ZERO);
	}
	
	/*--- GROUP MANAGEMENT ---*/
	
	protected static void addUpdateGroup(ActionRequest actionRequest) throws Exception {
		
		Map<String, Object> params = new HashMap<String, Object>();
		final long lGroupId = ParamUtil
				.getLong(actionRequest, PortalConstants.ORGANIZATION_ID);
		final long lParentOrganizationId = ParamUtil.getLong(actionRequest,
				PortalConstants.PARENT_ORG_SEARCH_CONTAINER_PK,
				OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID);
		final String sGroupName = ParamUtil.getString(actionRequest, PortalConstants.NAME);
		final boolean bCxsuite = ParamUtil.getBoolean(actionRequest, PortalConstants.CXSUITE);
		final boolean bAndroid = ParamUtil.getBoolean(actionRequest, PortalConstants.ANDROID);
		final boolean bIOS = ParamUtil.getBoolean(actionRequest, PortalConstants.IOS);
		final boolean bVex = ParamUtil.getBoolean(actionRequest, PortalConstants.VEX);
		
		PortletSession pSession = actionRequest.getPortletSession(false);
		pSession.setAttribute(PortalConstants.NAME, sGroupName, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.CXSUITE, bCxsuite, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.ANDROID, bAndroid, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.IOS, bIOS, PortletSession.PORTLET_SCOPE);
		pSession.setAttribute(PortalConstants.VEX, bVex, PortletSession.PORTLET_SCOPE);
		
		if (CommonUtil.getStringBytes(sGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
			params.put(PortalConstants.COND8, CommonUtil.getStringBytes(sGroupName));
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
			
			if (lGroupId <= PortalConstants.LONG_ZERO) {
				log.warn(PortalErrors.ADD_GROUP_EXCEEDS_BYTE_SIZE, PortalConstants.ADD_UPDATE_GROUP, params, (new OrganizationNameException()));
			} else {
				log.warn(PortalErrors.EDIT_GROUP_EXCEEDS_BYTE_SIZE, PortalConstants.ADD_UPDATE_GROUP, params, (new OrganizationNameException()));
			}
			
			throw new OrganizationNameException();
		}

		final int iStatusId = ParamUtil.getInteger(actionRequest, PortalConstants.STATUS_ID);
		final String sType = ParamUtil.getString(actionRequest, PortalConstants.TYPE);
		final long lRegionId = ParamUtil.getLong(actionRequest, PortalConstants.REGION_ID);
		final long lCountryId = ParamUtil.getLong(actionRequest, PortalConstants.COUNTRY_ID);
		final String sComments = ParamUtil.getString(actionRequest, PortalConstants.COMMENTS);
		final boolean bSite = ParamUtil.getBoolean(actionRequest, PortalConstants.SITE);
		final List<Address> lstAddresses = UsersAdminUtil
				.getAddresses(actionRequest);
		final List<EmailAddress> lstEmailAddresses = UsersAdminUtil
				.getEmailAddresses(actionRequest);
		final List<OrgLabor> lstOrgLabors = UsersAdminUtil
				.getOrgLabors(actionRequest);
		final List<Phone> lstPhones = UsersAdminUtil.getPhones(actionRequest);
		final List<Website> lstwebsites = UsersAdminUtil
				.getWebsites(actionRequest);

		final ServiceContext scServiceContext = ServiceContextFactory
				.getInstance(Organization.class.getName(), actionRequest);

		Organization organization = null;
		List<Project> projectList = null;
		
		try{
			PortletCommonUtil.isDBConnected();
			
			if (CommonUtil.isStringNullOrEmpty(sGroupName)) {
				params.put(PortalConstants.GROUP_NAME_R, sGroupName);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
				
				throw new RequiredOrganizationException(PortalErrors.EDIT_GROUP_EMPTY);
			}
			
			if (lGroupId <= PortalConstants.INT_ZERO) {

				// Add organization

				organization = OrganizationServiceUtil.addOrganization(
						lParentOrganizationId, sGroupName, sType, lRegionId, lCountryId,
						iStatusId, sComments, bSite, lstAddresses,
						lstEmailAddresses, lstOrgLabors, lstPhones, lstwebsites,
						scServiceContext);

				organization.getExpandoBridge().setAttribute(PortalConstants.CXSERVICE_USE_AUTH,
						bCxsuite);
				organization.getExpandoBridge().setAttribute(
						PortalConstants.ANDROIDSERVICE_USE_AUTH, bAndroid);
				organization.getExpandoBridge().setAttribute(
						PortalConstants.IOSSERVICE_USE_AUTH, bIOS);
				organization.getExpandoBridge().setAttribute(
						PortalConstants.VEXSERVICE_USE_AUTH, bVex);
			} else {

				// Update organization
				projectList = ProjectLocalServiceUtil.getProjectsByOwnerGroup(lGroupId);
				organization = OrganizationLocalServiceUtil.getOrganization(lGroupId);
				
				boolean bIsCxSuite = false;
				boolean bIsAndroid = false;
				boolean bIsIOS = false;
				boolean bIsVex = false;
				
				if (projectList != null && !projectList.isEmpty()) {
					for (Project p : projectList) {
						if (p.getType() == ProjectType.CX_SUITE.getInteger()) {
							bIsCxSuite = true;
						} else if (p.getType() == ProjectType.ANDROID.getInteger()) {
							bIsAndroid = true;
						} else if (p.getType() == ProjectType.IOS.getInteger()){
							bIsIOS = true;
						} else {
							bIsVex = true;
						}
					}
					
					boolean bCxSuiteAccess = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(PortalConstants.CXSERVICE_USE_AUTH));
					boolean bAndroidAccess = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(PortalConstants.ANDROIDSERVICE_USE_AUTH));
					boolean bIOSAccess = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(PortalConstants.IOSSERVICE_USE_AUTH));
					boolean bVexAccess = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(PortalConstants.VEXSERVICE_USE_AUTH));
					
					if (bIsCxSuite && (bCxSuiteAccess && !bCxsuite)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_GROUP_ASSIGNED_TO_PROJECTS);
						throw new RequiredOrganizationException(PortalErrors.EDIT_GROUP_WITH_PROJECT);
					}
					
					if (bIsAndroid && (bAndroidAccess && !bAndroid)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_GROUP_ASSIGNED_TO_PROJECTS);
						throw new RequiredOrganizationException(PortalErrors.EDIT_GROUP_WITH_PROJECT);
					}
					
					if (bIsIOS && (bIOSAccess && !bIOS)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_GROUP_ASSIGNED_TO_PROJECTS);
						throw new RequiredOrganizationException(PortalErrors.EDIT_GROUP_WITH_PROJECT);
					}
					
					if (bIsVex && (bVexAccess && !bVex)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_GROUP_ASSIGNED_TO_PROJECTS);
						throw new RequiredOrganizationException(PortalErrors.EDIT_GROUP_WITH_PROJECT);
					}
				}
				
				organization = null;

				/*organization = OrganizationServiceUtil.updateOrganization(lGroupId,
						lParentOrganizationId, sGroupName, sType, lRegionId, lCountryId,
						iStatusId, sComments, bSite, lstAddresses,
						lstEmailAddresses, lstOrgLabors, lstPhones, lstwebsites,
						scServiceContext);*/
				
				/*organization = OrganizationServiceUtil.updateOrganization(lGroupId, lParentOrganizationId,
						sGroupName, sType, lRegionId, lCountryId, iStatusId, sComments,
						bSite, scServiceContext);*/
				
				organization = OrganizationServiceUtil.updateOrganization(lGroupId, lParentOrganizationId,
						sGroupName, sType, lRegionId, lCountryId, iStatusId, sComments, false,
						null, bSite, lstAddresses, lstEmailAddresses, lstOrgLabors, lstPhones,
						lstwebsites, scServiceContext);

				organization.getExpandoBridge().setAttribute(PortalConstants.CXSERVICE_USE_AUTH, bCxsuite);
				organization.getExpandoBridge().setAttribute(PortalConstants.ANDROIDSERVICE_USE_AUTH, bAndroid);
				organization.getExpandoBridge().setAttribute(PortalConstants.IOSSERVICE_USE_AUTH, bIOS);
				organization.getExpandoBridge().setAttribute(PortalConstants.VEXSERVICE_USE_AUTH, bVex);
			}
		}catch(Exception e){
			if (e instanceof UBSPortalException) {
				if (((UBSPortalException) e).getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}
			} else if (lGroupId <= PortalConstants.INT_ZERO){
				if (e instanceof RequiredOrganizationException){
					params.put(PortalConstants.S_GROUP_NAME, sGroupName);
					log.warn(PortalErrors.ADD_GROUP_EMPTY, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}else if (e instanceof OrganizationNameException){
					params.put(PortalConstants.S_GROUP_NAME, sGroupName);
					log.warn(PortalErrors.ADD_GROUP_EXCEEDS_BYTE_SIZE, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}else if (e instanceof DuplicateOrganizationException){
					 params.put(PortalConstants.S_GROUP_NAME, sGroupName);
					 log.warn(PortalErrors.ADD_GROUP_ALREADY_EXISTS, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}else if (e instanceof NoSuchOrganizationException){
					 params.put("lGroupId", lGroupId);
					 log.debug(PortalErrors.ADD_GROUP_EMPTY, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}else if (e instanceof SystemException ){
						params.put(PortalConstants.LONG_PARENT_ORG_ID, lParentOrganizationId);
						params.put(PortalConstants.S_GROUP_NAME, sGroupName);
						params.put(PortalConstants.S_TYPE, sType);
						params.put(PortalConstants.LONG_REGION_ID, lRegionId);
						params.put(PortalConstants.LONG_COUNTRY_ID, lCountryId);
						params.put(PortalConstants.INT_STATUS_ID, iStatusId);
						params.put(PortalConstants.S_COMMENTS, sComments);
						params.put(PortalConstants.B_SITE, bSite);
						params.put(PortalConstants.L_ADDRESSES, lstAddresses);
						params.put(PortalConstants.L_EMAIL, lstEmailAddresses);
						params.put(PortalConstants.L_ORG_LABORS, lstOrgLabors);
						params.put(PortalConstants.L_PHONES, lstPhones);
						params.put(PortalConstants.L_WEBSITES, lstwebsites);
						params.put(PortalConstants.SCSERVICE_CONTEXT, scServiceContext);
						log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}else if (e instanceof PortalException ){
						params.put(PortalConstants.LONG_PARENT_ORG_ID, lParentOrganizationId);
						params.put(PortalConstants.S_GROUP_NAME, sGroupName);
						params.put(PortalConstants.S_TYPE, sType);
						params.put(PortalConstants.LONG_REGION_ID, lRegionId);
						params.put(PortalConstants.LONG_COUNTRY_ID, lCountryId);
						params.put(PortalConstants.INT_STATUS_ID, iStatusId);
						params.put(PortalConstants.S_COMMENTS, sComments);
						params.put(PortalConstants.B_SITE, bSite);
						params.put(PortalConstants.L_ADDRESSES, lstAddresses);
						params.put(PortalConstants.L_EMAIL, lstEmailAddresses);
						params.put(PortalConstants.L_ORG_LABORS, lstOrgLabors);
						params.put(PortalConstants.L_PHONES, lstPhones);
						params.put(PortalConstants.L_WEBSITES, lstwebsites);
						params.put(PortalConstants.SCSERVICE_CONTEXT, scServiceContext);
						
						log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.ADD_UPDATE_GROUP, params, e);			
					}
			
			}else {
				if (e instanceof RequiredOrganizationException){
					if (e.getMessage().equals(PortalErrors.EDIT_GROUP_WITH_PROJECT)) {
						params.put("cxSuite", bCxsuite);
						params.put("android", bAndroid);
						params.put("ios", bIOS);
						params.put("vex", bVex);
						log.warn(PortalErrors.EDIT_GROUP_WITH_PROJECT, PortalConstants.ADD_UPDATE_GROUP, params, e);
					} else {
						params.put(PortalConstants.S_GROUP_NAME, sGroupName);
						log.warn(PortalErrors.EDIT_GROUP_EMPTY, PortalConstants.ADD_UPDATE_GROUP, params, e);
					}
				}else if (e instanceof OrganizationNameException){
					params.put(PortalConstants.S_GROUP_NAME, sGroupName);
					log.warn(PortalErrors.EDIT_GROUP_EXCEEDS_BYTE_SIZE, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}else if (e instanceof DuplicateOrganizationException){
					 params.put(PortalConstants.S_GROUP_NAME, sGroupName);
					 log.warn(PortalErrors.EDIT_GROUP_ALREADY_EXISTS, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}else if (e instanceof NoSuchOrganizationException){
					 params.put("lGroupId", lGroupId);
					 log.debug(PortalErrors.ADD_GROUP_EMPTY, PortalConstants.ADD_UPDATE_GROUP, params, e);
				}else if (e instanceof SystemException ){
					params.put(PortalConstants.LONG_PARENT_ORG_ID, lParentOrganizationId);
					params.put(PortalConstants.S_GROUP_NAME, sGroupName);
					params.put(PortalConstants.S_TYPE, sType);
					params.put(PortalConstants.LONG_REGION_ID, lRegionId);
					params.put(PortalConstants.LONG_COUNTRY_ID, lCountryId);
					params.put(PortalConstants.INT_STATUS_ID, iStatusId);
					params.put(PortalConstants.S_COMMENTS, sComments);
					params.put(PortalConstants.B_SITE, bSite);
					params.put(PortalConstants.L_ADDRESSES, lstAddresses);
					params.put(PortalConstants.L_EMAIL, lstEmailAddresses);
					params.put(PortalConstants.L_ORG_LABORS, lstOrgLabors);
					params.put(PortalConstants.L_PHONES, lstPhones);
					params.put(PortalConstants.L_WEBSITES, lstwebsites);
					params.put(PortalConstants.SCSERVICE_CONTEXT, scServiceContext);
					log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.ADD_UPDATE_GROUP, params, e);
			}else if (e instanceof PortalException ){
					params.put(PortalConstants.LONG_PARENT_ORG_ID, lParentOrganizationId);
					params.put(PortalConstants.S_GROUP_NAME, sGroupName);
					params.put(PortalConstants.S_TYPE, sType);
					params.put(PortalConstants.LONG_REGION_ID, lRegionId);
					params.put(PortalConstants.LONG_COUNTRY_ID, lCountryId);
					params.put(PortalConstants.INT_STATUS_ID, iStatusId);
					params.put(PortalConstants.S_COMMENTS, sComments);
					params.put(PortalConstants.B_SITE, bSite);
					params.put(PortalConstants.L_ADDRESSES, lstAddresses);
					params.put(PortalConstants.L_EMAIL, lstEmailAddresses);
					params.put(PortalConstants.L_ORG_LABORS, lstOrgLabors);
					params.put(PortalConstants.L_PHONES, lstPhones);
					params.put(PortalConstants.L_WEBSITES, lstwebsites);
					params.put(PortalConstants.SCSERVICE_CONTEXT, scServiceContext);
					
					log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.ADD_UPDATE_GROUP, params, e);			
				}
			}
			
			throw e;
		} finally {
			params.clear();
			params = null;
		}
	}
	
	protected static void deleteGroup(ActionRequest actionRequest) throws Exception {

		Map<String, Object> params = new HashMap<String, Object>();
		final long[] lDeleteOrganizationIds = StringUtil
				.split(ParamUtil.getString(actionRequest,
						PortalConstants.DELETE_ORG_IDS), PortalConstants.LONG_ZERO);
		List<Project> projectList = null;
		
		try{
			PortletCommonUtil.isDBConnected();
			
			for ( long lDeleteOrganizationId: lDeleteOrganizationIds) {
				projectList = ProjectLocalServiceUtil.getProjectsByOwnerGroup(lDeleteOrganizationId);
				
				if (projectList != null && !projectList.isEmpty()) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_GROUP_ASSIGNED_TO_PROJECTS);
					throw new RequiredOrganizationException(PortalErrors.DELETE_GROUP_WITH_USERS);
				}
				OrganizationServiceUtil.deleteOrganization(lDeleteOrganizationId);
			}
			
			actionRequest.setAttribute("fromDeleteGroup", PortalConstants.TRUE);
		}catch(Exception e){
			 if (e instanceof SystemException ){
				params.put(PortalConstants.LONG_DELETE_ORG_IDS, lDeleteOrganizationIds);
				log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.DELETE_GROUP, params, e);			
			}else if (e instanceof PortalException ){
				params.put(PortalConstants.LONG_DELETE_ORG_ID, lDeleteOrganizationIds);
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.DELETE_GROUP, params, e);			
			}else if (e instanceof RequiredOrganizationException){
				params.put(PortalConstants.LONG_DELETE_ORG_ID, lDeleteOrganizationIds);
				log.debug(PortalErrors.DELETE_GROUP_WITH_USERS, PortalConstants.DELETE_GROUP, params, e);	
			} else if (e instanceof UBSPortalException) {
				log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.DELETE_GROUP, params, e);
				actionRequest.setAttribute("fromDeleteGroup", PortalConstants.TRUE);
			}
			 
			 throw e;
		} finally {
			params.clear();
			params = null;
		}
	}
	
	protected static List<Organization> searchGroup(ActionRequest actionRequest)
			throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		String sGroupName = ParamUtil.getString(actionRequest,PortalConstants.GROUP_NAME_R);
		String strNoOfUsers = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NO_USERS);
		boolean bCxsuite = ParamUtil.getBoolean(actionRequest, PortalConstants.CXSUITE);
		boolean bAndroid = ParamUtil.getBoolean(actionRequest, PortalConstants.ANDROID);
		boolean bIOS = ParamUtil.getBoolean(actionRequest, PortalConstants.IOS);
		boolean bVex = ParamUtil.getBoolean(actionRequest, PortalConstants.VEX);
		List<Organization> orgs = null;
		ThemeDisplay tdThemeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		PortletSession portletSession = actionRequest.getPortletSession(false);
		
		try{
			PortletCommonUtil.isDBConnected();
			portletSession.setAttribute(PortalConstants.IS_SEARCHED, PortalConstants.TRUE, PortletSession.PORTLET_SCOPE);
			portletSession.setAttribute(PortalConstants.GROUP_NAME_R, sGroupName, PortletSession.PORTLET_SCOPE);
			portletSession.setAttribute(PortalConstants.PARAM_NO_USERS, strNoOfUsers, PortletSession.PORTLET_SCOPE);
			portletSession.setAttribute(PortalConstants.PARAM_CX_SERVICE_USE_AUTH, bCxsuite, PortletSession.PORTLET_SCOPE);
			portletSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH, bAndroid, PortletSession.PORTLET_SCOPE);
			portletSession.setAttribute(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH, bIOS, PortletSession.PORTLET_SCOPE);
			portletSession.setAttribute(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH, bVex, PortletSession.PORTLET_SCOPE);
			
			if (CommonUtil.getStringBytes(sGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
				params.put(PortalConstants.COND9, CommonUtil.getStringBytes(sGroupName));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
				log.warn(PortalErrors.SEARCH_GROUP_INVALID_NAME, PortalConstants.SEARCH_GROUP, params, (new OrganizationNameException()));
				throw new OrganizationNameException();
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strNoOfUsers)) {
				try {
					int iNoOfUsers = Integer.parseInt(strNoOfUsers);
					
					if (iNoOfUsers < PortalConstants.INT_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NO_OF_USERS));
						throw new UBSPortalException(PortalErrors.SEARCH_GROUP_INVALID_NO_OF_USERS, PortalMessages.NO_OF_USERS_INVALID);
					}
				} catch (NumberFormatException e) {
					params.put("strNoOfUsers", strNoOfUsers);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NO_OF_USERS));
					log.warn(PortalErrors.SEARCH_GROUP_INVALID_NO_OF_USERS, PortalConstants.SEARCH_GROUP, params, (new UBSPortalException(PortalErrors.SEARCH_GROUP_INVALID_NO_OF_USERS, PortalMessages.NO_OF_USERS_INVALID)));
					throw new UBSPortalException(PortalErrors.SEARCH_GROUP_INVALID_NO_OF_USERS, PortalMessages.NO_OF_USERS_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(sGroupName)
					|| !CommonUtil.isStringNullOrEmpty(strNoOfUsers)
					|| (bCxsuite || bAndroid || bIOS|| bVex)) {
				orgs = filterGroup(sGroupName, strNoOfUsers, bCxsuite, bAndroid, bIOS, bVex);
			}
		}catch(Exception e){
			if (e instanceof SystemException ){
				params.put("tdThemeDisplay.getCompanyId()", tdThemeDisplay.getCompanyId());
				params.put("OrganizationConstants.ANY_PARENT_ORGANIZATION_ID", OrganizationConstants.ANY_PARENT_ORGANIZATION_ID);
				params.put(PortalConstants.S_GROUP_NAME, sGroupName);
				params.put("QueryUtil.ALL_POS", QueryUtil.ALL_POS);
				log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.SEARCH_GROUP, params, e);			
			} else if (e instanceof UBSPortalException) {
				if (((UBSPortalException) e).getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					portletSession.setAttribute(PortalConstants.IS_SEARCHED, PortalConstants.TRUE, PortletSession.PORTLET_SCOPE);
					log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.SEARCH_GROUP, params, e);
				}
			}
		
			throw e;
		} finally {
			params.clear();
			params = null;
		}

		return orgs;
	}
	
	private static List<Organization> filterGroup (String groupName, String strNoOfUsers, boolean bCxSuite, boolean bAndroid, boolean bIOS, boolean bVex) {
		List<Organization> orgList = new ArrayList<Organization>();
		List<Organization> organizationList = null;
		List<Organization> tempList = null;
		
		try {
			organizationList = OrganizationLocalServiceUtil.getOrganizations(PortalConstants.INT_ZERO, OrganizationLocalServiceUtil.getOrganizationsCount());
		} catch (SystemException e) {
			
		}
		
		if (!CommonUtil.isListNullOrEmpty(organizationList)) {
			orgList.addAll(organizationList);
		}
		
		groupName = StringUtil.replace(groupName, "%", PortalConstants.PERCENT);
		
		if (!CommonUtil.isListNullOrEmpty(orgList) && !CommonUtil.isStringNullOrEmpty(groupName)) {
			tempList = new ArrayList<Organization>();
			
			for (Organization organization : orgList) {
				if (organization.getName().toLowerCase().contains(groupName.toLowerCase())) {
					tempList.add(organization);
				}
			}
			
			orgList = tempList;
		}
		
		if (!CommonUtil.isListNullOrEmpty(orgList) && !CommonUtil.isStringNullOrEmpty(strNoOfUsers)) {
			tempList = new ArrayList<Organization>();
			int iSearchedNoOfUsers = Integer.parseInt(strNoOfUsers);
			
			for (Organization organization : orgList) {
				try {
					int iOrgNoOfUsers = OrganizationUtil.getUsersSize(organization.getOrganizationId());
					
					if (iOrgNoOfUsers == iSearchedNoOfUsers) {
						tempList.add(organization);
					}
				} catch (SystemException e) {
					log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.SEARCH_GROUP, null, e);
				};
			}
			
			orgList = tempList;
		}
		
		if (!CommonUtil.isListNullOrEmpty(orgList) && (bCxSuite || bAndroid || bIOS || bVex)) {
			tempList = new ArrayList<Organization>();
			
			for (Organization organization : orgList) {
				boolean blnCxSuiteValue = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(PortalConstants.PARAM_CX_SERVICE_USE_AUTH));
				boolean blnAndroidValue = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH));
				boolean blnIOSValue = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH));
				boolean blnVexValue = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH));
				if ((bCxSuite && blnCxSuiteValue)
						|| (bAndroid && blnAndroidValue)
						|| (bIOS && blnIOSValue) || (bVex && blnVexValue)) {
					tempList.add(organization);
				}
			}
		}
		
		return tempList;
	}
	
	protected static List<Organization> clearSearchGroup (ActionRequest actionRequest) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Organization> orgList = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				orgList = OrganizationLocalServiceUtil.getOrganizations(PortalConstants.INT_ZERO, OrganizationLocalServiceUtil.getOrganizationsCount());
				
				actionRequest.setAttribute(PortalConstants.GROUP_NAME_R, PortalConstants.STRING_EMPTY);
				actionRequest.setAttribute(PortalConstants.PARAM_NO_USERS, PortalConstants.STRING_EMPTY);
				actionRequest.setAttribute(PortalConstants.PARAM_CX_SERVICE_USE_AUTH, PortalConstants.STRING_EMPTY);
				actionRequest.setAttribute(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH, PortalConstants.STRING_EMPTY);
				actionRequest.setAttribute(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH, PortalConstants.STRING_EMPTY);
				actionRequest.setAttribute(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH, PortalConstants.STRING_EMPTY);
			}
		} catch (SystemException e) {
			params.put("orgList", orgList);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, "clearSearchGroup", params, e);
			throw e;
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				log.debug(PortalErrors.ORM_EXCEPTION, "clearSearchGroup", params, e);
			}
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return orgList;
	}
}
