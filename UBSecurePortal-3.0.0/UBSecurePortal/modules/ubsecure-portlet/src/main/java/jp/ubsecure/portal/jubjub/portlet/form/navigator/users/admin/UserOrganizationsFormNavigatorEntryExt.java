package jp.ubsecure.portal.jubjub.portlet.form.navigator.users.admin;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.users.admin.web.servlet.taglib.ui.UserOrganizationsFormNavigatorEntry;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property = {
		PortalConstants.KEY_FORM_NAVIGATOR_ENTRY_ORDER + PortalConstants.ENTRY_ORDER_USER_ORGANIZATIONS
	},
	service = FormNavigatorEntry.class
)
public class UserOrganizationsFormNavigatorEntryExt extends UserOrganizationsFormNavigatorEntry {
	@Override
	public String getFormNavigatorId() {
		return PortalConstants.USERS_ADMIN_PREFIX + super.getFormNavigatorId();
	}

	@Override
	public boolean isVisible(User user, User selUser) {
		boolean visible = GetterUtil.getBoolean(PropsUtil.get(PortalConstants.USERS_ADMIN_ORGANIZATIONS_VISIBLE), true);
		return visible && super.isVisible(user, selUser);
	}
}