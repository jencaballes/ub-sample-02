package jp.ubsecure.portal.jubjub.portlet.service.cxsuite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxClientType;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResolverSoapProxy;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResponseDiscovery;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.CliScanArgs;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.LocalCodeContainer;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.ScanResponse;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.SourceCodeSettings;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.SourceLocationType;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoapProxy;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseLoginData;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponsePresetList;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectConfig;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Preset;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectConfiguration;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSettings;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScheduleSettings;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScheduleType;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.io.FileUtils;

import com.liferay.portal.kernel.util.FileUtil;

/**
 * Class for the management of scans requested to 
 * (CxSuite Source Code Diagnosis) 
 * 
 * Major functions are:
 * request a source code scan to CxSuite application
 * get the status of source code scan in CxSuite
 * request reports after scanning is done
 * process CxSuite Results as report file and upload to YCloud storage
 * 
 * @author ASI558
 *
 */
public class CxScanManager {
	private static final CxSDKWebServiceSoapProxy cxSDKWebServiceSoapProxy = new CxSDKWebServiceSoapProxy();
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(CxScanManager.class);

	
	/**
	 * get the resolver url of CxSuite from Configuration file
	 * @return the resolver url of CxSuite
	 */
	public static String getResolverUrl(){
		Map<String, Object> params = new HashMap<String, Object>();
		String strUrl = null;
		try {
			strUrl = ConfigurationFileParser.getConfig(PortalConstants.CX_RESOLVERURL);
		} catch (UBSPortalException e) {
			params.put("strUrl", strUrl);
			log.debug(PortalErrors.CXSUITE_LOGIN_ERROR, PortalConstants.METHOD_GET_RESOLVER_URL, params, e);
		} finally {
			params.clear();
			params = null;
		}
		return strUrl;
	}
	
	/**
	 * Retrieves the SDK web service URL which is needed to call the CxSuite scan-related functions
	 * 
	 * @return the SDK web service URL returned after logging in to CxSuite
	 * @throws UBSPortalException the Exception to be thrown to calling function
	 */
	public static String getWebServiceUrl() {
		Map<String, Object> params = new HashMap<String, Object>();
		String strUrl = null;
		CxWSResolverSoapProxy cxWSResolverSoapProxy = null;
		CxWSResponseDiscovery responseValue = null;
		String strEndpoint = null;
		
		try {
			strEndpoint = ConfigurationFileParser.getConfig(PortalConstants.CX_RESOLVERURL);
			cxWSResolverSoapProxy = new CxWSResolverSoapProxy();
			cxWSResolverSoapProxy.setEndpoint(strEndpoint);
			int apiVersion = PortalConstants.API_VERSION;
	        responseValue = cxWSResolverSoapProxy.getWebServiceUrl(CxClientType.SDK, apiVersion);
	        if (responseValue == null) {
	        	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
	        	throw new UBSPortalException(PortalErrors.CX_WEBSERVICE_URL_ERROR, PortalErrors.CX_PROCESSING_EXCEPTION);
	        }

	        strUrl = responseValue.getServiceURL();
		} catch (RemoteException re) {
			params.put("responseValue", responseValue);
			log.debug(PortalErrors.CX_WEBSERVICE_URL_ERROR, PortalConstants.METHOD_GET_WEB_SERVICE_URL, params, re);
		} catch (UBSPortalException ubse) {
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_GET_WEB_SERVICE_URL, params, ubse);
		} catch (Exception e) {
			params.put("strEndpoint", strEndpoint);
			params.put("responseValue", responseValue);
			params.put("cxWSResolverSoapProxy", cxWSResolverSoapProxy);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_WEB_SERVICE_URL, params, e);
		} finally {
			params.clear();
			params = null;
			cxWSResolverSoapProxy = null;
			responseValue = null;
			strEndpoint = null;
		}
		
		return strUrl;
	}
	
	public static String login (String strUser, String strPass) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strSessionId = null;
		
		try {
			log.info(PortalConstants.API_FUNCTION_GET_WEB_SERVICE_URL, PortalConstants.BEFORE + PortalConstants.API_CALL_LOGIN);
			String strEndpoint = getWebServiceUrl();
			log.info(PortalConstants.API_FUNCTION_GET_WEB_SERVICE_URL, PortalConstants.AFTER + PortalConstants.API_CALL_LOGIN);
			
			if (CommonUtil.isStringNullOrEmpty(strEndpoint)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_WEB_SERVICE_URL));
				throw new UBSPortalException(PortalErrors.CX_LOGIN_INVALID_URL, PortalErrors.CX_PROCESSING_EXCEPTION);
			}
			
			cxSDKWebServiceSoapProxy.setEndpoint(strEndpoint);
			
			Credentials applicationCredentials = new Credentials();
			applicationCredentials.setUser(strUser);		
			applicationCredentials.setPass(strPass);
			
			log.info(PortalConstants.API_FUNCTION_LOGIN, PortalConstants.BEFORE + PortalConstants.API_CALL_LOGIN);
			CxWSResponseLoginData cxWSResponseLoginData = cxSDKWebServiceSoapProxy.login(applicationCredentials, PortalConstants.LANGUAGE_ID);
			log.info(PortalConstants.API_FUNCTION_LOGIN, PortalConstants.AFTER + PortalConstants.API_CALL_LOGIN);
			
			if (CommonUtil.isObjectNull(cxWSResponseLoginData)) {
				params.put("cxWSResponseLoginData", cxWSResponseLoginData);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
				throw new UBSPortalException(PortalErrors.CX_LOGIN_RESPONSE_IS_NULL, PortalErrors.CX_PROCESSING_EXCEPTION);
			}
			
			if (!cxWSResponseLoginData.isIsSuccesfull()) {
				params.put("cxWSResponseLoginData.isIsSuccesfull()", cxWSResponseLoginData.isIsSuccesfull());
				params.put("cxWSResponseLoginData.getErrorMessage()", cxWSResponseLoginData.getErrorMessage());
				
				if (cxWSResponseLoginData.getErrorMessage().equalsIgnoreCase(PortalConstants.CX_LICENSE_IS_EXPIRED_JP)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_CX_LICENSE_EXPIRED);
					throw new UBSPortalException(PortalErrors.CX_LOGIN_EXPIRED_LICENSE, PortalErrors.CX_PROCESSING_EXCEPTION);
				}
				
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_CX_LOGIN_FAILED);
				throw new UBSPortalException(PortalErrors.CX_LOGIN_WEB_SERVICE_API_FAILED, PortalErrors.CX_PROCESSING_EXCEPTION);
			}
			
			strSessionId = cxWSResponseLoginData.getSessionId();
		} catch (RemoteException re) {
			log.debug(PortalErrors.CXSUITE_LOGIN_ERROR, PortalConstants.METHOD_LOGIN, null, re);
			throw new UBSPortalException(PortalErrors.CXSUITE_LOGIN_ERROR, PortalErrors.CX_PROCESSING_EXCEPTION, re);
		} catch (UBSPortalException e) {
			params.put("lcid", PortalConstants.LANGUAGE_ID);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_LOGIN, params, e);
			throw e;
		} catch (Exception e) {
			params.put("lcid", PortalConstants.LANGUAGE_ID);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_LOGIN, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return strSessionId;
	}
	
	/**
	 * 	 
	 * @param strSessionId the session id used as id to request the CxSuite
	 * @param lCxScanId the scan id of the item being scanned
	 * @return true/false if deletion is successful or not
	 * @throws UBSPortalException the exception thrown
	 * 
	 */
	public static void deleteCxProject(String strSessionId, long lCxProjectId, long lPortalProjectId, int projectType) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			long[] lProjectIds = {lCxProjectId};
			
			if (CommonUtil.isStringNullOrEmpty(strSessionId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID);
			}
			
			log.info(PortalConstants.API_FUNCTION_DELETE_PROJECTS, PortalConstants.BEFORE + PortalConstants.API_CALL_DELETE_PROJECT);
			CxWSBasicRepsonse response = cxSDKWebServiceSoapProxy.deleteProjects(strSessionId, lProjectIds);
			log.info(PortalConstants.API_FUNCTION_DELETE_PROJECTS, PortalConstants.AFTER + PortalConstants.API_CALL_DELETE_PROJECT);
			
			if (CommonUtil.isObjectNull(response)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
				throw new UBSPortalException(PortalErrors.DELETE_PROJECT_WEB_SERVICE_API_FAILED, PortalErrors.CX_PROCESSING_EXCEPTION);
			}
			
			if (!response.isIsSuccesfull()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_WEB_SERVICE_DELETE_PROJECT_FAILED);
				throw new UBSPortalException(PortalErrors.DELETE_PROJECT_WEB_SERVICE_API_FAILED, response.getErrorMessage());
			}
			
			File projectDirectory = new File(FileProcessUtil.locateProjectDirectory(lPortalProjectId));
			if (projectDirectory.exists()) {
				FileUtil.deltree(projectDirectory);
			}
			
			FileProcessUtil.deleteReportFilesByProjectId(lPortalProjectId);
				
		} catch (UBSPortalException ubse) {
			String strErrorMessage = ubse.getErrorMessage();
			String strApiMessage = PortalConstants.STRING_EMPTY;
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				strApiMessage = PortalMessages.CX_DELETE_PROJECT_FAILED;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				strApiMessage = PortalMessages.IOS_DELETE_PROJECT_FAILED;
			}
			if (!CommonUtil.isStringNullOrEmpty(strErrorMessage) && !strErrorMessage.equals(strApiMessage)) {
				if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
					int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
					strErrorMessage = strApiMessage + strErrorMessage.substring(index);
				} else {
					strErrorMessage = strApiMessage + PortalConstants.STRING_SPACE_OPEN_PARENTHESIS + strErrorMessage + PortalConstants.STRING_CLOSE_PARENTHESIS;
				}
			} else {
				strErrorMessage = strApiMessage;
			}
			
			ubse.setErrorMessage(strErrorMessage);
			
			params.put("strSessionId", strSessionId);
			params.put("lCxProjectId", lCxProjectId);
			params.put("lPortalProjectId", lPortalProjectId);
			
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_DELETE_CX_PROJECT, params, ubse);
			throw ubse;	
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_DELETE_CX_PROJECT, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.CX_DELETE_PROJECT_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.IOS_DELETE_PROJECT_FAILED, e);
			}
		} finally {
			params.clear();
			params = null;
		}
	}
	//deleteCxProject BLOCK
	
	//updateCxProjectName BLOCK
	
	/**
	 * updates the project name in CxSuite
	 * @param strSessionId the sessionId from CxSuite
	 * @param lProjectId the id of the project
	 * @param strProjectName the name of the project
	 * @return true/false if the project name in CxSuite was successfully updated or not
	 * @throws UBSPortalException
	 */
	public static boolean updateCxProjectName(String strSessionId, long lProjectId, String strProjectName, long lPresetId, int projectType) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bIsSuccessful = false;
		String strProjectConfigErrorMsg = PortalConstants.STRING_EMPTY;
		try {
			if (CommonUtil.isStringNullOrEmpty(strSessionId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID);
			}
			
			if (lProjectId < 1) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			
			log.info(PortalConstants.API_FUNCTION_GET_PROJECT_CONFIGURATION, PortalConstants.BEFORE + PortalConstants.API_CALL_PROJECT_CONFIG_RETRIEVAL);
			CxWSResponseProjectConfig projectConfigResponse = cxSDKWebServiceSoapProxy.getProjectConfiguration(strSessionId, lProjectId);
			log.info(PortalConstants.API_FUNCTION_GET_PROJECT_CONFIGURATION, PortalConstants.AFTER + PortalConstants.API_CALL_PROJECT_CONFIG_RETRIEVAL);
			
			if (CommonUtil.isObjectNull(projectConfigResponse)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
				throw new UBSPortalException(PortalErrors.INVALID_CX_PROJECT_CONFIG);
			}
			
			ProjectConfiguration projectConfig = projectConfigResponse.getProjectConfig();
			
			if (CommonUtil.isObjectNull(projectConfig)) {
				strProjectConfigErrorMsg = projectConfigResponse.getErrorMessage();
				if(CommonUtil.isStringNullOrEmpty(strProjectConfigErrorMsg)){
					strProjectConfigErrorMsg  = PortalConstants.CX_PROJECT_CONFIG_MSG;
				}
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_PROJECT_CONFIG)
						+ PortalConstants.COMMA_SPACE + PortalConstants.CX_SERVER_ERROR_MSG + strProjectConfigErrorMsg);
				throw new UBSPortalException(PortalErrors.INVALID_CX_PROJECT_CONFIG);
			}
			
			ProjectSettings projectSettings = projectConfig.getProjectSettings();
			
			if (CommonUtil.isObjectNull(projectSettings)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_PROJECT_SETTINGS));
				throw new UBSPortalException(PortalErrors.INVALID_CX_PROJECT_SETTINGS);
			}
			
			projectSettings.setProjectName(strProjectName);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				projectSettings.setPresetID(lPresetId);
			}
			projectConfig.setProjectSettings(projectSettings);
			
			log.info(PortalConstants.API_FUNCTION_UPDATE_PROJECT_INCREMENTAL_CONFIGURATION, PortalConstants.BEFORE + PortalConstants.API_CALL_UPDATE_PROJECT);
			CxWSBasicRepsonse basicResponse = cxSDKWebServiceSoapProxy.updateProjectIncrementalConfiguration(strSessionId, lProjectId, projectConfig);
			log.info(PortalConstants.API_FUNCTION_UPDATE_PROJECT_INCREMENTAL_CONFIGURATION, PortalConstants.AFTER + PortalConstants.API_CALL_UPDATE_PROJECT);
			
			if (CommonUtil.isObjectNull(basicResponse)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
				throw new UBSPortalException(PortalErrors.PROJECT_CONFIG_UPDATE_ERROR);
			}
			
			bIsSuccessful = basicResponse.isIsSuccesfull();
			
			if (!bIsSuccessful) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_WEB_SERVICE_UPDATE_PROJECT_FAILED);
				throw new UBSPortalException(PortalErrors.PROJECT_CONFIG_UPDATE_ERROR, basicResponse.getErrorMessage());
			}
		} catch (RemoteException e) {
			log.debug(PortalErrors.CX_SERVER_ERROR, PortalConstants.METHOD_UPDATE_CX_PROJECT_NAME, params, e);	
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.CX_SERVER_ERROR, PortalMessages.CX_UPDATE_PROJECT_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.CX_SERVER_ERROR, PortalMessages.IOS_UPDATE_PROJECT_FAILED, e);
			}
		} catch (UBSPortalException e) {
			String strErrorMessage = e.getErrorMessage();
			String strApiMessage = PortalConstants.STRING_EMPTY;
			
			if(e.getErrorCode().equals(PortalErrors.INVALID_CX_PROJECT_CONFIG)){
				strApiMessage = strProjectConfigErrorMsg;
			}else if (projectType == ProjectType.CX_SUITE.getInteger()) {
				strApiMessage = PortalMessages.CX_UPDATE_PROJECT_FAILED;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				strApiMessage = PortalMessages.IOS_UPDATE_PROJECT_FAILED;
			}
			if (!CommonUtil.isStringNullOrEmpty(strErrorMessage) && !strErrorMessage.equals(strApiMessage)) {
				if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
					int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
					strErrorMessage = strApiMessage + strErrorMessage.substring(index);
				} else {
					strErrorMessage = strApiMessage + PortalConstants.STRING_SPACE_OPEN_PARENTHESIS + strErrorMessage + PortalConstants.STRING_CLOSE_PARENTHESIS;
				}
			} else if (e.getErrorCode().equals(PortalErrors.INVALID_CX_PROJECT_CONFIG)){
				strErrorMessage = strProjectConfigErrorMsg;
			} else {
				strErrorMessage = strApiMessage;
			}
			
			e.setErrorMessage(strErrorMessage);
			params.put("strSessionId", strSessionId);
			params.put("strProjectName", strProjectName);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_UPDATE_CX_PROJECT_NAME, params, e);
			throw e;
		} catch (Exception e) {
			params.put("strSessionId", strSessionId);
			params.put("strProjectName", strProjectName);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_UPDATE_CX_PROJECT_NAME, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.CX_UPDATE_PROJECT_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.IOS_UPDATE_PROJECT_FAILED, e);
			}
		} finally {
			params.clear();
			params = null;
		}
		return bIsSuccessful;
	}
	
	/**
	 * Request a source code scan to CxSuite application and 
	 * register a project to CxSuite at the same time 
	 * because per CxSuite API, 
	 * "The current version of the CxSuite API does not provide a method for creating a 
	 * project. Instead, first use the scan method with a new project name, which will 
	 * create the project (and immediately run a scan); then, you can configure the
	 * project."
	 * 
	 * @param scan scan object which contains the project id of the project registered in Portal
	 * @param srcfile the source file to be scanned
	 * @param strSessionId the session id from CxSuite
	 * @param strProjectName the project name of the source code to be scanned
	 * @return the Scan Object which was set with necessary information to be used in Portal
	 * @throws UBSPortalException the Exception to be returned to calling function
	 */
	public static Scan registerCxScan(Scan scan, File srcfile, String strSessionId, String strProjectName, boolean reRunFlag, long presetId, int projectType, long projectId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		byte[] zippedBytes = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		long lScanId = PortalConstants.LONG_ZERO;
		String strFileName = null;
		File uploadedZippedSourceFile = null;
		List<String> cxConfigKeys = null;
		Map<String, String> cxConfigurationValue = null;
		String configValue = null;	
		long scanConfigurationId = PortalConstants.LONG_ZERO;
		String strCxRunId = null;
		String strFilePath = null;
		int scanStatus= 0;
		String strCxAndroidProjectId = null;
				
		try {
			if (scan == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SCAN_PARAM);
			}
			
			if (!CommonUtil.isValidFile(srcfile)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SOURCE_FILE);
			}
							
			if (CommonUtil.isStringNullOrEmpty(strSessionId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SESSION_ID);
			}
			
			if (CommonUtil.isStringNullOrEmpty(strProjectName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
				throw new UBSPortalException(PortalErrors.INVALID_CX_PROJECTNAME);
			}
			
			if (CommonUtil.isOutOfRange(projectType, ProjectType.CX_SUITE.getInteger(), ProjectType.IOS.getInteger())) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}
		
			lProjectId = scan.getProjectId();
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_PROJECT_ID);
			}
			lScanId = scan.getScanId();
			if (lScanId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SCAN_ID);
			}
			strFileName = scan.getFileName();
			if (CommonUtil.isStringNullOrEmpty(strFileName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SOURCE_FILE);
			}
			
			if (!reRunFlag) {
				uploadedZippedSourceFile = FileProcessUtil.uploadSourceFile(lProjectId, lScanId, srcfile, strFileName);
				
				if (!CommonUtil.isValidAndExistingFile(uploadedZippedSourceFile)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
					throw new UBSPortalException(PortalErrors.UNSUCCESSFUL_CX_SOURCE_FILE_UPLOAD);
				}

				strFilePath = uploadedZippedSourceFile.getAbsolutePath();
				
				if (CommonUtil.isStringNullOrEmpty(strFilePath)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
					throw new UBSPortalException(PortalErrors.UNSUCCESSFUL_CX_SOURCE_FILE_UPLOAD);
				}

				scan.setFilePath(strFilePath);
			} else {
				if (CommonUtil.isValidAndExistingFile(srcfile)) {
					strFilePath = srcfile.getAbsolutePath();
				}
				if (CommonUtil.isStringNullOrEmpty(strFilePath)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
					throw new UBSPortalException(PortalErrors.UNSUCCESSFUL_CX_SOURCE_FILE_UPLOAD);
				}

				scan.setFilePath(strFilePath);
			}
			
			// get config values
			String configKeySignatureId = PortalConstants.STRING_EMPTY;
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				configKeySignatureId = PortalConstants.CX_SIGNATURE_ID;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				configKeySignatureId = PortalConstants.IOS_SIGNATURE_ID;
			}
			
			cxConfigKeys = new ArrayList<String>();
			cxConfigKeys.add(configKeySignatureId); 
			cxConfigKeys.add(PortalConstants.CX_SCAN_CONFIG_ID); 
			cxConfigurationValue = ConfigurationFileParser.getConfig(cxConfigKeys);
			
			if (cxConfigurationValue == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
				throw new UBSPortalException(PortalErrors.UNABLE_TO_GET_CX_CONFIG_VALUES);
			}

			if (cxConfigurationValue.containsKey(PortalConstants.CX_SCAN_CONFIG_ID)) {
				configValue = cxConfigurationValue.get(PortalConstants.CX_SCAN_CONFIG_ID);
				if (CommonUtil.isStringNullOrEmpty(configValue)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
					throw new UBSPortalException(PortalErrors.INVALID_SCAN_CONFIG_ID);
				}

				scanConfigurationId = Long.parseLong(configValue);
				configValue = null;
				
				//use default preset id if presetId value is 0
				if (presetId == PortalConstants.LONG_ZERO) {
					configValue = cxConfigurationValue.get(configKeySignatureId);
					if (CommonUtil.isStringNullOrEmpty(configValue)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
						throw new UBSPortalException(PortalErrors.INVALID_SIGNATURE_ID);
					}

					presetId = Long.parseLong(cxConfigurationValue.get(configKeySignatureId));
					configValue = null;
				}
				
				List<String> scanResponseList = createScan(presetId, strProjectName, scanConfigurationId, srcfile, strFilePath, scan, strSessionId, projectType, projectId);
				
				if(!CommonUtil.isListNullOrEmpty(scanResponseList)){
					strCxRunId = scanResponseList.get(0);
					strCxAndroidProjectId = scanResponseList.get(1);
				}
				
				scan.setCxRunId(strCxRunId);
				scan.setCxAndroidScanId(strCxRunId);
				scan.setCxAndroidProjectId(strCxAndroidProjectId);
				scan.setStatus(PortalConstants.CX_CURRENT_STATUS_QUEUED);
				scan.setModifiedDate(new Date());
				ScanLocalServiceUtil.updateScan(scan);

			} else {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CONFIG_VALUE));
				throw new UBSPortalException(PortalErrors.UNABLE_TO_GET_CX_CONFIG_VALUES);
			}
		} catch (UBSPortalException ubse) {
			String strErrorMessage = ubse.getErrorMessage();
			String strApiMessage = PortalConstants.STRING_EMPTY;
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				strApiMessage = PortalMessages.CX_REGISTER_SCAN_FAILED;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				strApiMessage = PortalMessages.IOS_REGISTER_SCAN_FAILED;
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
				if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
					int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
					strErrorMessage = strApiMessage + strErrorMessage.substring(index);
				} else {
					strErrorMessage = strApiMessage + PortalConstants.STRING_SPACE_OPEN_PARENTHESIS + strErrorMessage + PortalConstants.STRING_CLOSE_PARENTHESIS;
				}
			} else {
				strErrorMessage = strApiMessage;
			}
			
			ubse.setErrorMessage(strErrorMessage);
			params.put("scan", scan);
			params.put("srcfile", srcfile);
			params.put("strSessionId", strSessionId);	
			params.put("strProjectName", strProjectName);
			params.put("zippedBytes", zippedBytes);
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strFileName", strFileName);
			params.put("uploadedZippedSourceFile", uploadedZippedSourceFile);
			params.put("strFilePath", strFilePath);
			params.put("cxConfigurationValue", cxConfigurationValue);
			params.put("configValue", configValue);
			params.put("strCxRunId", strCxRunId);
			params.put("scanStatus", scanStatus);
			params.put("strCxAndroidProjectId", strCxAndroidProjectId);
			params.put("cxConfigurationValue", cxConfigurationValue);
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_REGISTER_CX_SCAN, params, ubse);
			throw ubse;
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_REGISTER_CX_SCAN, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.CX_REGISTER_SCAN_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.IOS_REGISTER_SCAN_FAILED, e);
			}
		}  finally { 
			zippedBytes = null;
			strFileName = null;
			uploadedZippedSourceFile = null;
			cxConfigKeys = null;
			cxConfigurationValue = null;
			configValue = null;	
			strCxRunId = null;
			strFilePath = null;
			strCxAndroidProjectId = null;
			params.clear();
			params = null;
		}		
		
		return scan;
	}
	
	/**
	 * scan the project to CxSuite
	 * @param strSessionId the session variable that set the session id of the user
	 * @param cliScanArgs the scan argument needed in scan request
	 * @return the value returned from scan request
	 * @throws UBSPortalException
	 */
	private static List<String> scanCx(long lProjectId, String strSessionId, CliScanArgs cliScanArgs, int projectType) throws UBSPortalException {
		Map<String, Object> params 	= new HashMap<String, Object>();
		String strCxRunId 				= null;
		Long lCXProjectID 				= PortalConstants.LONG_ZERO;
		ScanResponse scanResponse 		= null;
		List<String> scanResponseList 	= null;
			
		try {
			if (CommonUtil.isStringNullOrEmpty(strSessionId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SESSION_ID);
			}
			
			if (cliScanArgs == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CLISCANARGS));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SCAN_ARGS);
			}

			scanResponse = CxSDKWebServiceUtil.scan(cliScanArgs, strSessionId);
			if (scanResponse.getScanResult() == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
				throw new UBSPortalException(PortalErrors.CX_SCAN_EXECUTION_ERROR);
			} else if (!scanResponse.getScanResult().getIsSuccesfull()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_WEB_SERVICE_SCAN_FAILED);
				throw new UBSPortalException(PortalErrors.CX_SCAN_EXECUTION_ERROR, scanResponse.getScanResult().getErrorMessage());
			} else {
				strCxRunId = scanResponse.getScanResult().getRunId();
				lCXProjectID = scanResponse.getScanResult().getProjectID();
				
				if (strCxRunId == null) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RUN_ID));
					throw new UBSPortalException(PortalErrors.UNABLE_TO_GET_CXSUITE_RUN_ID);
				}
				
				scanResponseList = new ArrayList<String>();
				scanResponseList.add(strCxRunId);
				scanResponseList.add(String.valueOf(lCXProjectID));
			}
		} catch (UBSPortalException ubse) {
			String strErrorMessage = ubse.getErrorMessage();
			String strApiMessage = PortalConstants.STRING_EMPTY;
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				strApiMessage = PortalMessages.CX_REGISTER_SCAN_FAILED;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				strApiMessage = PortalMessages.IOS_REGISTER_SCAN_FAILED;
			}
			if (!CommonUtil.isStringNullOrEmpty(strErrorMessage) && !strErrorMessage.equals(strApiMessage)) {
				if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
					int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
					strErrorMessage = strApiMessage + strErrorMessage.substring(index);
				} else {
					strErrorMessage = strApiMessage + PortalConstants.STRING_SPACE_OPEN_PARENTHESIS + strErrorMessage + PortalConstants.STRING_CLOSE_PARENTHESIS;
				}
				
			} else {
				strErrorMessage = strApiMessage;
			}
			
			ubse.setErrorMessage(strErrorMessage);
			params.put("strSessionId", strSessionId);
			params.put("cliScanArgs", cliScanArgs);
			params.put("response", scanResponse);
			params.put("strCxRunId", strCxRunId);
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_SCAN_CX, params, ubse);
			throw ubse;
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_SCAN_CX, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.CX_REGISTER_SCAN_FAILED, e);
		} finally {
			scanResponse = null;
			params.clear();
			params = null;
		}
		
		return scanResponseList;
	}			
	
	/**
	 * @param strSessionId the session id used as id to request the CxSuite
	 * @param strRunId the run id of the item being scanned
	 * @return true/false if cancel scan is successful or not
	 * @throws UBSPortalException the exception thrown
	 * 
	 */
	public static void cancelCxScan(String strSessionId, String strRunId, int projectType) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (CommonUtil.isStringNullOrEmpty(strRunId)) { 
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_RUN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_RUN_ID);
			}
			if (CommonUtil.isStringNullOrEmpty(strSessionId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID);
			}
			if (CommonUtil.isOutOfRange(projectType, ProjectType.CX_SUITE.getInteger(), ProjectType.IOS.getInteger())) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}
			
			log.info(PortalConstants.API_FUNCTION_CANCEL_SCAN, PortalConstants.BEFORE + PortalConstants.API_CALL_CANCEL_SCAN);
			CxWSBasicRepsonse basicResponse = cxSDKWebServiceSoapProxy.cancelScan(strSessionId, strRunId);
			log.info(PortalConstants.API_FUNCTION_CANCEL_SCAN, PortalConstants.AFTER + PortalConstants.API_CALL_CANCEL_SCAN);
			
			if (CommonUtil.isObjectNull(basicResponse)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
				throw new UBSPortalException(PortalErrors.STOP_SCAN_WEB_SERVICE_API_FAILED, PortalErrors.CX_PROCESSING_EXCEPTION);
			}
			
			if (!basicResponse.isIsSuccesfull()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_WEB_SERVICE_STOP_SCAN_FAILED);
				throw new UBSPortalException(PortalErrors.STOP_SCAN_WEB_SERVICE_API_FAILED, basicResponse.getErrorMessage());
			}
		} catch (UBSPortalException ubse) {
			String strErrorMessage = ubse.getErrorMessage();
			String strApiMessage = PortalConstants.STRING_EMPTY;
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				strApiMessage = PortalMessages.CX_CANCEL_SCAN_FAILED;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				strApiMessage = PortalMessages.IOS_CANCEL_SCAN_FAILED;
			}
			if (!CommonUtil.isStringNullOrEmpty(strErrorMessage) && !strErrorMessage.equals(strApiMessage)) {
				if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
					int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
					strErrorMessage = strApiMessage + strErrorMessage.substring(index);
				} else {
					strErrorMessage = strApiMessage + PortalConstants.STRING_SPACE_OPEN_PARENTHESIS + strErrorMessage + PortalConstants.STRING_CLOSE_PARENTHESIS;
				}
			} else {
				strErrorMessage = strApiMessage;
			}
			
			ubse.setErrorMessage(strErrorMessage);
			
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_CANCEL_CX_SCAN, params, ubse);
			throw ubse;
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_CANCEL_CX_SCAN, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.CX_CANCEL_SCAN_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.IOS_CANCEL_SCAN_FAILED, e);
			}
		} finally {
			params.clear();
			params = null;
		}
		
	}
	
	/**
	 * rescan the request sent to CxSuite for projects currently being scanned
	 * @param existingProjectId the project Id of the project set in Portal
	 * @param filePath the file path of the source file
	 * @param strSessionId the sessionId in CxSuite
	 * @param projectName the name of the project
	 * @return sessionId to CxSuite
	 * @throws UBSPortalException
	 */	
	public static String reRunCxScan(Scan scan, String filePath, String strSessionId, String projectName, long presetId, int projectType, long projectid) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File file = null;
		Scan scanObject = null;
		String newRunId = null;
		String previousRunId = null;
		
		try {
			if (scan == null) {
				params.put("scan", scan);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SCAN_PARAM);
			}
			if (CommonUtil.isStringNullOrEmpty(filePath)) {
				params.put("filePath", CommonUtil.isStringNullOrEmpty(filePath));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
				throw new UBSPortalException(PortalErrors.UNABLE_TO_GET_UPLOADED_SOURCE_FILE);
			}
			if (CommonUtil.isStringNullOrEmpty(strSessionId)) {
				params.put("strSessionId", CommonUtil.isStringNullOrEmpty(strSessionId));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SESSION_ID);
			}
			if (CommonUtil.isStringNullOrEmpty(projectName)) { 
				params.put("projectName", CommonUtil.isStringNullOrEmpty(projectName));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
				throw new UBSPortalException(PortalErrors.INVALID_CX_PROJECTNAME);
			}
			
			if (CommonUtil.isOutOfRange(projectType, ProjectType.CX_SUITE.getInteger(), ProjectType.IOS.getInteger())) {
				params.put("projectType", projectType);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}
			
			file = FileUtils.getFile(filePath);
			previousRunId = scan.getCxRunId();
			if (CommonUtil.isStringNullOrEmpty(previousRunId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_RUN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_RUN_ID);
			}
			
			if (CommonUtil.isValidAndExistingFile(file)) { 
				scanObject = registerCxScan(scan,file,strSessionId, projectName, true, presetId, projectType, projectid);
				
				if (CommonUtil.isObjectNull(scanObject)) {
					params.put("scanObject", scanObject);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN));
					throw new UBSPortalException(PortalErrors.INVALID_CX_SCAN_PARAM);
				} else {					
					newRunId = scanObject.getCxRunId();
					if (newRunId != null) {
						cancelCxScan(strSessionId, previousRunId, projectType);
					}
				}				
			} else {
				params.put("file valid", CommonUtil.isValidAndExistingFile(file));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.UNABLE_TO_GET_UPLOADED_SOURCE_FILE);
			}
		} catch (UBSPortalException ubse) {
			String strErrorMessage = ubse.getErrorMessage();
			String strApiMessage = PortalConstants.STRING_EMPTY;
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				strApiMessage = PortalMessages.CX_REEXECUTE_SCAN_FAILED;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				strApiMessage = PortalMessages.IOS_REEXECUTE_SCAN_FAILED;
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strErrorMessage) && !strErrorMessage.equals(strApiMessage)) {
				if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
					int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
					strErrorMessage = strApiMessage + strErrorMessage.substring(index);
				} else {
					strErrorMessage = strApiMessage + PortalConstants.STRING_SPACE_OPEN_PARENTHESIS + strErrorMessage + PortalConstants.STRING_CLOSE_PARENTHESIS;
				}
			} else {
				strErrorMessage = strApiMessage;
			}
			ubse.setErrorMessage(strErrorMessage);
			params.put("scan", scan);
			params.put("filePath", filePath);
			params.put("strSessionId", strSessionId);
			params.put("projectName", projectName);
			params.put("scanObject", scanObject);
			params.put("file", file);
			
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_RERUN_CX_SCAN, params, ubse);
			throw ubse;
		} catch (Exception e) {
			params.put("scan", scan);
			params.put("filePath", filePath);
			params.put("strSessionId", strSessionId);
			params.put("projectName", projectName);
			params.put("scanObject", scanObject);
			params.put("file", file);
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_RERUN_CX_SCAN, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.CX_REEXECUTE_SCAN_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.IOS_REEXECUTE_SCAN_FAILED, e);
			}
		} finally {
			params.clear();
			params = null;
			file = null;
			previousRunId = null;
			scanObject = null;
		}
		
		return newRunId;
	}
	
	/**
	 * @param strSessionId the session id used as id to request the CxSuite
	 * @param lCxScanId the scan id of the item being scanned
	 * @return true/false if deletion is successful or not
	 * @throws UBSPortalException the exception thrown
	 * 
	 */
	public static void deleteCxScan(String strSessionId, long lCxScanId, long lProjectId, long lScanId, int projectType) throws UBSPortalException {		
		Map<String, Object> params = new HashMap<String, Object>();
		File scanDirectory = null;
		
		try {
			
			if (CommonUtil.isStringNullOrEmpty(strSessionId)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SESSION_ID);
			}
			if (lCxScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SCAN_ID);
			}			
			if (CommonUtil.isOutOfRange(projectType, ProjectType.CX_SUITE.getInteger(), ProjectType.VEX.getInteger())) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}
			long[] lScanIds = {lCxScanId};
			
			scanDirectory = new File(FileProcessUtil.locateScanDirectory(lProjectId, lScanId));
			if (scanDirectory.exists()) {
				FileUtil.deltree(scanDirectory);
			}
			
			try {
				log.info(PortalConstants.API_FUNCTION_DELETE_SCANS, PortalConstants.BEFORE + PortalConstants.API_CALL_DELETE_SCAN);
				CxWSBasicRepsonse basicResponse = cxSDKWebServiceSoapProxy.deleteScans(strSessionId, lScanIds);
				log.info(PortalConstants.API_FUNCTION_DELETE_SCANS, PortalConstants.AFTER + PortalConstants.API_CALL_DELETE_SCAN);
				
				if (CommonUtil.isObjectNull(basicResponse)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
					throw new UBSPortalException(PortalErrors.DELETE_SCAN_WEB_SERVICE_API_FAILED, PortalErrors.CX_PROCESSING_EXCEPTION);
				}
				
				if (!basicResponse.isIsSuccesfull()) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_WEB_SERVICE_DELETE_SCAN_FAILED);
					throw new UBSPortalException(PortalErrors.DELETE_SCAN_WEB_SERVICE_API_FAILED, basicResponse.getErrorMessage());
				}
			} catch (UBSPortalException e) {
				String errMessage = e.getErrorMessage().toLowerCase();
				if (errMessage.contains(PortalConstants.CX_CANNOT_RETRIEVE_SCAN_DETAILS_ERROR_MSG_EN.toLowerCase()) ||
						errMessage.contains(PortalConstants.CX_CANNOT_RETRIEVE_SCAN_DETAILS_ERROR_MSG_JP)) {
					params.put("lCxScanId", lCxScanId);
					params.put("lScanId", lScanId);
					params.put("response.getErrorMessage()", errMessage);
					log.warn(e.getErrorCode(), PortalConstants.METHOD_DELETE_CX_SCAN, params, null);
				} else {
					throw e;
				}
			}
			
			FileProcessUtil.deleteReportFilesByScanId(lScanId);
			
		} catch (UBSPortalException ubse) {
			String strErrorMessage = ubse.getErrorMessage();
			String strApiMessage = PortalConstants.STRING_EMPTY;
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				strApiMessage = PortalMessages.CX_DELETE_SCAN_FAILED;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				strApiMessage = PortalMessages.IOS_DELETE_SCAN_FAILED;
			}
			if (!CommonUtil.isStringNullOrEmpty(strErrorMessage) && !strErrorMessage.equals(strApiMessage)) {
				if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
					int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
					strErrorMessage = strApiMessage + strErrorMessage.substring(index);
				} else {
					strErrorMessage = strApiMessage + PortalConstants.STRING_SPACE_OPEN_PARENTHESIS + strErrorMessage + PortalConstants.STRING_CLOSE_PARENTHESIS;
				}
			} else {
				strErrorMessage = strApiMessage;
			}
			
			ubse.setErrorMessage(strErrorMessage);
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			params.put("strSessionId", strSessionId);
			
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_DELETE_CX_SCAN, params, ubse);
			throw ubse;
		} catch (Exception e) {
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			params.put("strSessionId", strSessionId);
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_DELETE_CX_SCAN, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.CX_DELETE_SCAN_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.IOS_DELETE_SCAN_FAILED, e);
			}
		} finally {
			params.clear();
			params = null;
			scanDirectory = null;
		}
					
	}

	public static CxSDKWebServiceSoapProxy getSoapProxy() {
		
		if (cxSDKWebServiceSoapProxy.getEndpoint() == null) {
			cxSDKWebServiceSoapProxy.setEndpoint(getWebServiceUrl());
		}
		
		return cxSDKWebServiceSoapProxy;
	}
	
	public static boolean isConnectedToCx() throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean isConnected = false;
		CxWSResolverSoapProxy cxWSResolverSoapProxy = null;
		CxWSResponseDiscovery responseValue = null;
	
		try {
			cxWSResolverSoapProxy = new CxWSResolverSoapProxy();
			responseValue = cxWSResolverSoapProxy.getWebServiceUrl(CxClientType.SDK, PortalConstants.API_VERSION);
			if (responseValue == null || responseValue.getServiceURL() == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_WEB_SERVICE_API_FAILED);
			} else if (!responseValue.isIsSuccesfull()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_WEB_SERVICE_REGENERATE_REPORT_FAILED);
				throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_WEB_SERVICE_API_FAILED, responseValue.getErrorMessage());
			} else {
				isConnected = true;
			}
		} catch (RemoteException e) {
			isConnected = false;
			params.put("cxWSResolverSoapProxy", cxWSResolverSoapProxy);
			params.put("responseValue", responseValue);
			log.debug(PortalErrors.CX_SERVER_ERROR, "isConnectedToCx", null, e);
		} catch (UBSPortalException e) {
			isConnected = false;
			params.put("cxWSResolverSoapProxy", cxWSResolverSoapProxy);
			params.put("responseValue", responseValue);
			log.debug(PortalErrors.CX_SERVER_ERROR, "isConnectedToCx", params, e);
			throw e;
		} finally {
			cxWSResolverSoapProxy = null;
			responseValue = null;
			params.clear();
			params = null;
		}
		
		return isConnected;
	}
	
	private static byte [] getBytes (File srcFile, int projectType) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		InputStream inputStream = null;
		long lFileLength = srcFile.length();
		byte [] zippedBytes = new byte[(int) lFileLength];
		
		try {
			inputStream = new FileInputStream(srcFile);

			int offset = 0;
			int limit = 1;
			int numRead = 0;
			
			if (lFileLength > PortalConstants.MIN_BYTE_SIZE_PER_CHUNK) {
				limit = PortalConstants.MIN_BYTE_SIZE_PER_CHUNK;
				while ((lFileLength - limit > limit * 2) && limit < PortalConstants.MAX_BYTE_SIZE_PER_CHUNK) {
					limit *= 2;
				}
			}
			
			while(offset < zippedBytes.length && (numRead = inputStream.read(zippedBytes, offset, limit)) >= 0) {
				offset += numRead;
				
				if (lFileLength - offset < limit) {
					limit = (int) (lFileLength - offset);
				}
			}

			inputStream.close();
			
			Runtime runtime = Runtime.getRuntime();
			runtime.gc();
		} catch (FileNotFoundException e) {
			params.put("srcFile", srcFile);
			log.debug(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalConstants.METHOD_GET_BYTES, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalMessages.CX_REGISTER_SCAN_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalMessages.IOS_REGISTER_SCAN_FAILED, e);
			}
		} catch (IOException e) {
			params.put("inputStream", inputStream);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_BYTES, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.CX_REGISTER_SCAN_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.IOS_REGISTER_SCAN_FAILED, e);
			}
		} finally {
			params.clear();
			params = null;
			inputStream = null;
		}
		
		return zippedBytes;
	}
	
	private static List<String> createScan (long presetId, String strProjectName, long scanConfigurationId, File srcfile, String strFilePath, Scan scan, String strSessionId, int projectType, long projectId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.ProjectSettings projectSettings = null;
		LocalCodeContainer localCodeContainer = null;
		SourceCodeSettings sourceCodeSettings = null;
		ScheduleSettings scheduleSettings = null;
		CliScanArgs cliScanArgs = null;
		boolean bIsPrivateScan = false;
		boolean bIsIncremental = false;
		byte [] zippedBytes = null;
		String strCxRunId = null;
		List<String> scanResponseList = null;
		
		try {
			projectSettings = new jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.ProjectSettings();
			projectSettings.setProjectID(PortalConstants.LONG_ZERO);
			projectSettings.setProjectName(Long.toString(projectId) + "_" + Long.toString(scan.getScanId()) + "_" + strProjectName);
			projectSettings.setPresetID(presetId);
			projectSettings.setAssociatedGroupID(PortalConstants.STRING_EMPTY);
			projectSettings.setScanConfigurationID(scanConfigurationId);
			projectSettings.setDescription(PortalConstants.STRING_EMPTY);
			
			localCodeContainer = new LocalCodeContainer();
			sourceCodeSettings = new SourceCodeSettings();
			scheduleSettings = new ScheduleSettings();
			cliScanArgs = new CliScanArgs();

			zippedBytes = getBytes(srcfile, projectType);		
			
			localCodeContainer.setFileName(scan.getFileName());
			DataSource dataSource = new ByteArrayDataSource(zippedBytes);
			DataHandler dataHandler = new DataHandler(dataSource);
			localCodeContainer.setZippedFile(dataHandler);

			sourceCodeSettings.setSourceOrigin(SourceLocationType.Local);
			sourceCodeSettings.setPackagedCode(localCodeContainer);
			
			scheduleSettings.setSchedule(ScheduleType.Now);
			
			cliScanArgs.setPrjSettings(projectSettings);
			cliScanArgs.setSrcCodeSettings(sourceCodeSettings);
			cliScanArgs.setIsIncremental(bIsIncremental);
			cliScanArgs.setIsPrivateScan(bIsPrivateScan);
			
			scan.setFileName(localCodeContainer.getFileName());
			scan.setHashValue(FileUtil.getMD5Checksum(srcfile));
			scan.setFilePath(strFilePath);
			
			scanResponseList = scanCx(scan.getProjectId(), strSessionId, cliScanArgs, projectType);
			
			if(!CommonUtil.isListNullOrEmpty(scanResponseList)){
				strCxRunId = scanResponseList.get(0);
			}
		} catch (IOException e) {
			params.put("HashValue", srcfile);
			log.debug(PortalErrors.INVALID_FILE_HASH_VALUE,  PortalConstants.METHOD_CREATE_SCAN, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.INVALID_FILE_HASH_VALUE, PortalMessages.CX_REGISTER_SCAN_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.INVALID_FILE_HASH_VALUE, PortalMessages.IOS_REGISTER_SCAN_FAILED, e);
			}
		} catch (UBSPortalException e) {
			params.put("presetId", presetId);
			params.put("strProjectName", strProjectName);
			params.put("scanConfigurationId", scanConfigurationId);
			params.put("srcfile", srcfile);
			params.put("strFilePath", strFilePath);
			params.put("scan", scan);
			params.put("strSessionId", strSessionId);
			params.put("projectSettings", projectSettings);
			params.put("localCodeContainer", localCodeContainer);
			params.put("sourceCodeSettings", sourceCodeSettings);
			params.put("scheduleSettings", scheduleSettings);
			params.put("cliScanArgs", cliScanArgs);
			params.put("zippedBytes", zippedBytes);
			params.put("strCxRunId", strCxRunId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_SCAN, params, e);
			throw e;
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_CREATE_SCAN, params, e);
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.CX_REGISTER_SCAN_FAILED, e);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.IOS_REGISTER_SCAN_FAILED, e);
			}
		} finally {
			projectSettings = null; 
			localCodeContainer = null;
			sourceCodeSettings = null;
			scheduleSettings = null;
			cliScanArgs = null;
			params.clear();
			params = null;
		}
		
		return scanResponseList;
	}
	
	public static List<Long> getPresetList (String sessionId) {
		List<Long> presetList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			CxWSResponsePresetList cxWSResponsePresetList = cxSDKWebServiceSoapProxy.getPresetList(sessionId);
			
			if (!CommonUtil.isObjectNull(cxWSResponsePresetList)) {
				if (!cxWSResponsePresetList.isIsSuccesfull()) {
					params.put("cxWSResponsePresetList.isIsSuccesfull()", cxWSResponsePresetList.isIsSuccesfull());
					params.put("cxWSResponsePresetList.getErrorMessage()", cxWSResponsePresetList.getErrorMessage());
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_WEB_SERVICE_GET_PRESET_LIST_FAILED);
					throw new UBSPortalException(PortalErrors.GET_PRESET_LIST_WEB_SERVICE_API_FAILED, cxWSResponsePresetList.getErrorMessage());
				}
				
				Preset[] presetArray = cxWSResponsePresetList.getPresetList();
				
				if (!CommonUtil.isObjectNull(presetArray)) {
					presetList = new ArrayList<Long>();
					
					for (Preset preset : presetArray) {
						presetList.add(preset.getID());
					}
				}
			}
		} catch (RemoteException e) {
			log.debug(PortalErrors.CX_SERVER_ERROR, PortalConstants.METHOD_GET_PRESET_LIST, params, e);
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_PRESET_LIST, params, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_PRESET_LIST, params, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return presetList;
	}
}