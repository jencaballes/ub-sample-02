package jp.ubsecure.portal.jubjub.portlet.bag;

import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.deploy.hot.CustomJspBag;
import com.liferay.portal.kernel.url.URLContainer;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_CONTEXT_ID + PortalConstants.CONTEXT_ID_UBSECURE_CUSTOM_JSP_BAG,
		PortalConstants.KEY_CONTEXT_NAME + PortalConstants.CONTEXT_NAME_UBSECURE_CUSTOM_JSP_BAG,
		PortalConstants.KEY_SERVICE_RANKING + PortalConstants.SERVICE_RANKING_100
	}
)
public class UBSecurePortalCustomJspBag implements CustomJspBag {
	private List<String> customJsps;
	private Bundle bundle;

	@Override
	public String getCustomJspDir() {
		return PortalConstants.CUSTOM_JSPS;
	}

	@Override
	public List<String> getCustomJsps() {
		return customJsps;
	}

	@Override
	public URLContainer getURLContainer() {
		return urlContainer;
	}

	@Override
	public boolean isCustomJspGlobal() {
		return true;
	}

	@Activate
	protected void activate (BundleContext bundleContext) {
		bundle = bundleContext.getBundle();
		
		customJsps = new ArrayList<>();
		
		Enumeration<URL> entries = bundle.findEntries(getCustomJspDir(), PortalConstants.CUSTOM_JSP_ENTRY_JSP, true);
		
		while (entries.hasMoreElements()) {
			URL url = entries.nextElement();
			
			customJsps.add(url.getPath());
		}
		
		entries = bundle.findEntries(getCustomJspDir(), PortalConstants.CUSTOM_JSP_ENTRY_JSPF, true);
		
		while (entries.hasMoreElements()) {
			URL url = entries.nextElement();
			
			customJsps.add(url.getPath());
		}
	}
	
	private URLContainer urlContainer = new URLContainer () {
		@Override
		public URL getResource(String name) {
			return bundle.getEntry(name);
		}
		
		@Override
		public Set<String> getResources (String path) {
			Set<String> paths = new HashSet<>();
			
			for (String entry : customJsps) {
				if (entry.startsWith(path)) {
					paths.add(entry);
				}
			}
			
			return paths;
		}
	};
}
