package jp.ubsecure.portal.jubjub.portlet.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.exception.NoSuchOrganizationException;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PwdGenerator;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.jabberwock.cli.bridge.jubjub.ApiInitInfo;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.exception.NoSuchProjectException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectItem;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsersItem;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

public class VexProjectMgmtController {
private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexProjectMgmtController.class);
	
	public static List<Object> getProjects (ActionRequest actionRequest, int iUserAction) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = new ArrayList<Object>();
		String strProjectId = null;
		String strGroupName = null;
		String strCaseNumber = null;
		String strProjectName = null;
		String strProjectEndDateLow = null;
		String strProjectEndDateHigh = null;
		String strStatus = null;
		String strScanCount = null;
		String strCaseName = PortalConstants.STRING_EMPTY;
		String strTargetUrl=PortalConstants.STRING_EMPTY;
		String strProductionenvironmentUrl=PortalConstants.STRING_EMPTY;
		String webInspectionSignatureSetGroup = PortalConstants.STRING_EMPTY;
		int serverFiles = 1;
		int serverSettings = 1;
		String serverFilesSignatureSetGroup = PortalConstants.STRING_EMPTY;
		String serverSettingsSignatureSetGroup = PortalConstants.STRING_EMPTY;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		int iStart = PortalConstants.INT_ZERO;
		Object oUserId = null;
		Map<String, Object> searchedProject = new HashMap<String, Object>();
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			iStart = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_START);
			
			if (iUserAction == PortalConstants.USER_EVENT_SEARCH_PROJECT) {
				strProjectId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_ID).trim();
				strGroupName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_OWNER_GROUP).trim();
				strCaseNumber = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NUMBER).trim();
				strProjectName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
				strProjectEndDateLow = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE_LOW).trim();
				strProjectEndDateHigh = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE_HIGH).trim();
				strStatus = ParamUtil.getString(actionRequest, PortalConstants.PARAM_STATUS).trim();
				strScanCount = ParamUtil.getString(actionRequest, PortalConstants.PARAM_NO_OF_SCANS).trim();
				strCaseName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NAME).trim();
				strTargetUrl= ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_URL).trim();
				strProductionenvironmentUrl= ParamUtil.getString(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL).trim();
				webInspectionSignatureSetGroup = ParamUtil.getString(actionRequest, PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				serverFiles = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SERVER_FILES);
				serverSettings = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SERVER_SETTINGS);
				serverFilesSignatureSetGroup = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
				serverSettingsSignatureSetGroup = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
			}
			
			HttpSession session = ControllerHelper.getHttpSession(actionRequest);
			oUserId = session.getAttribute(PortalConstants.USER_ID);
		
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId != PortalConstants.LONG_ZERO) {
					user = UserLocalServiceUtil.getUser(lUserId);
					
					if (CommonUtil.isObjectNull(user)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
						throw new UBSPortalException(PortalErrors.INVALID_USER, PortalMessages.USER_INVALID);
					}
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
				try {
					long lProjectId = Long.parseLong(strProjectId);
					
					if (lProjectId < PortalConstants.LONG_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
					}
				} catch (NumberFormatException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strGroupName) && CommonUtil.getStringBytes(strGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
				throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_GROUP_NAME_TOO_LONG, PortalMessages.GROUP_NAME_TOO_LONG);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
				if (!Validator.isAlphanumericName(strCaseNumber)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
				}
				
				if (strCaseNumber.contains(PortalConstants.STRING_BLANK_SPACE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
				}
				
				if (CommonUtil.getStringBytes(strCaseNumber) > PortalConstants.STRING_BYTE_MAX_40) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_TOO_LONG, PortalMessages.CASE_NUMBER_TOO_LONG);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
				if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
				}
				
				if (!ControllerHelper.isCxProjectNameValid(strProjectName)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_NAME_INVALID, PortalMessages.PROJECT_NAME_INVALID);
				}
			}
			
			try {
				DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				Date dteProjectEndDateLow = null;
				Calendar dteProjectEndDateHigh = null;
				
				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
					if (strProjectEndDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
					}
					
					dteProjectEndDateLow = format.parse(strProjectEndDateLow);
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
					if (strProjectEndDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
					}
					
					dteProjectEndDateHigh = Calendar.getInstance();
					dteProjectEndDateHigh.setTime(format.parse(strProjectEndDateHigh));
					dteProjectEndDateHigh.set(Calendar.HOUR_OF_DAY, 23);
					dteProjectEndDateHigh.set(Calendar.MINUTE, 59);
					dteProjectEndDateHigh.set(Calendar.SECOND, 59);
				}
				
				if (!CommonUtil.isObjectNull(dteProjectEndDateLow)
						&& !CommonUtil.isObjectNull(dteProjectEndDateHigh)
						&& dteProjectEndDateHigh.getTime().before(dteProjectEndDateLow)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE.toLowerCase()));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
				}
			} catch (ParseException e) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
				throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
				try {
					int iScanCount = Integer.parseInt(strScanCount);
					
					if (iScanCount < PortalConstants.INT_ZERO) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NO_OF_SCANS));
						throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_SCAN_COUNT_INVALID, PortalMessages.SCAN_COUNT_INVALID);
					}
				} catch (NumberFormatException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NO_OF_SCANS));
					throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_SCAN_COUNT_INVALID, PortalMessages.SCAN_COUNT_INVALID);
				}
			}
			
			searchedProject.put(PortalConstants.PARAM_PROJECT_ID, strProjectId);
			searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
			searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, strCaseNumber);
			searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
			searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_LOW, strProjectEndDateLow);
			searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_HIGH, strProjectEndDateHigh);
			searchedProject.put(PortalConstants.PARAM_STATUS, strStatus);
			searchedProject.put(PortalConstants.PARAM_NO_OF_SCANS, strScanCount);
			searchedProject.put(PortalConstants.PARAM_CASE_NAME, strCaseName);
			searchedProject.put(PortalConstants.PARAM_TARGET_URL, strTargetUrl);
			searchedProject.put(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL, strProductionenvironmentUrl);
			searchedProject.put(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP, webInspectionSignatureSetGroup);
			searchedProject.put(PortalConstants.PARAM_SERVER_FILES, serverFiles);
			searchedProject.put(PortalConstants.PARAM_SERVER_SETTINGS, serverSettings);
			searchedProject.put(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP, serverFilesSignatureSetGroup);
			searchedProject.put(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP, serverSettingsSignatureSetGroup);
			
			ProjectLocalServiceUtil.clearCache();
			projectList = ProjectLocalServiceUtil.getProjects(searchedProject, user, ProjectType.VEX.getInteger(), iStart);
		} catch (PortalException pe) {
			params.put("lUserId", lUserId);
			params.put("user", user);
			params.put("searchedProject", searchedProject);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}
			
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("user", user);
				params.put("searchedProject", searchedProject);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_PROJECTS, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_PROJECTS, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
				searchedProject.clear();
				searchedProject = null;
			}
		}
		
		return projectList;
	}
	
	public static List<ProjectUsersItem> getProjectUsers (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		long lProjectId = PortalConstants.LONG_ZERO;
		List<ProjectUsersItem> resultList = new ArrayList<ProjectUsersItem>();
		List<User> userList = new ArrayList<User>();
		List<ProjectUsers> projectUsersList = new ArrayList<ProjectUsers>();
		ProjectUsersItem item = null;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
			
			if (!projectUsersList.isEmpty()) {
				userList = UserLocalServiceUtil.getUsers(0, UserLocalServiceUtil.getUsersCount());
				
				for (ProjectUsers projectUsers : projectUsersList) {
					for (User user : userList) {
						if (projectUsers.getUserId().equals(user.getEmailAddress())) {
							item = new ProjectUsersItem();
							
							item.setUserId(user.getUserId());
							item.setEmailAddress(user.getEmailAddress());
							item.setUserName(user.getFirstName());
							
							resultList.add(item);
						}
					}
				}
			}
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECT_USERS, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT_USERS, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECT_USERS, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_GET_PROJECT_USERS, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(userList)) {
				userList = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
				projectUsersList = null;
			}
		}
		
		return resultList;
	}
	
	public static Project getProject (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Project project = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			ProjectLocalServiceUtil.clearCache();
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}

			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_GET_PROJECT, params, nspe);
			throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_GET_PROJECT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return project;
	}
	
	public static Project getProject(ResourceRequest resourceRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Project project = null;
		long lProjectId = 0L;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			lProjectId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			ProjectLocalServiceUtil.clearCache();
			project = ProjectLocalServiceUtil.getProject(lProjectId);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_GET_PROJECT, params, nspe);
			throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_GET_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_GET_PROJECT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return project;
	}
	
	public static boolean updateProject (ActionRequest actionRequest, int userAction) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		long lProjectId = PortalConstants.LONG_ZERO;
		String strCaseNumber = null;
		String strProjectName = null;
		long lOwnerGroup = PortalConstants.LONG_ZERO;
		Date dteProjectEndDate = null;
		DateFormat formatter = null;
		long [] lUsersArr = null;
		Project project = null;
		boolean bSuccess = false;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		String strSelectedUsers = null;
		String [] strUsersArr = null;
		boolean bIsCaseNumberValid = false;
		Organization organization = null;
		String strDate = null;
		long lCxAndroidProjectId = PortalConstants.LONG_ZERO;
		long lPresetId = PortalConstants.LONG_ZERO;
		String strCaseName = PortalConstants.STRING_EMPTY;
		String strTargetUrl=PortalConstants.STRING_EMPTY;
		String strProductionenvironmentUrl=PortalConstants.STRING_EMPTY;
		StringBuilder tempTargetURL = null;
		StringBuilder tempProductionEnvURL = null;
		String webInspectionSignatureSetGroup = PortalConstants.STRING_EMPTY;
		int serverFiles = 1;
		int serverSettings = 1;
		String serverFilesSignatureSetGroup = PortalConstants.STRING_EMPTY;
		String serverSettingsSignatureSetGroup = PortalConstants.STRING_EMPTY;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
			
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			strCaseNumber = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NUMBER).trim();
			strProjectName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME).trim();	
			lOwnerGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_OWNER_GROUP);
			strDate = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE);
			strSelectedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
			lUsersArr = ParamUtil.getLongValues(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
			strCaseName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NAME).trim();
			webInspectionSignatureSetGroup = ParamUtil.getString(actionRequest, PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP);
			serverFiles = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SERVER_FILES);
			serverSettings = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SERVER_SETTINGS);
			serverFilesSignatureSetGroup = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP);
			serverSettingsSignatureSetGroup = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP);
			
			int targetUrlListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_URL_LIST_SIZE);
			int productionEnvUrlListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL_LIST_SIZE);	
						
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				if (userAction == PortalConstants.USER_EVENT_UPDATE_PROJECT) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				project = ProjectLocalServiceUtil.createProjectObj();
				lProjectId = ProjectLocalServiceUtil.createProjectId();
				project.setProjectId(lProjectId);
				project.setStatus(ProjectStatus.NOT_YET_COMPLETE.getInteger());
				project.setProjectCreateDate(new Date());
				project.setPresetId(lPresetId);
			} else {
				ProjectLocalServiceUtil.clearCache();
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				
				if (CommonUtil.isObjectNull(project)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
				}
				
				if (project.getType() != ProjectType.VEX.getInteger()) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
				if (!Validator.isAlphanumericName(strCaseNumber)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
					
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
					}
					
					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
				}
				
				if (strCaseNumber.contains(PortalConstants.STRING_BLANK_SPACE)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
					
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
					}
					
					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
				}
				
				if (CommonUtil.getStringBytes(strCaseNumber) > PortalConstants.STRING_BYTE_MAX_40) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
					
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_CASE_NUMBER_TOO_LONG, PortalMessages.CASE_NUMBER_TOO_LONG);
					}
					
					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_TOO_LONG, PortalMessages.CASE_NUMBER_TOO_LONG);
				}
				
				bIsCaseNumberValid = ProjectLocalServiceUtil.isCaseNumberValid(strCaseNumber, lProjectId);
				
				if (!bIsCaseNumberValid) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_ALREADY_EXISTS, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
					
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_CASE_NUMBER_ALREADY_EXISTS, PortalMessages.CASE_NUMBER_ALREADY_EXIST);
					}
					
					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_ALREADY_EXISTS, PortalMessages.CASE_NUMBER_ALREADY_EXIST);
				}
			} else {
				while (!bIsCaseNumberValid) {
					strCaseNumber = PwdGenerator.getPassword();
					
					bIsCaseNumberValid = ProjectLocalServiceUtil.isCaseNumberValid(strCaseNumber, lProjectId);
				}
			}

			if (CommonUtil.isStringNullOrEmpty(strCaseName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_CASE_NAME));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_CASE_NAME, PortalMessages.NO_CASE_NAME);
				}
				
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_CASE_NAME, PortalMessages.NO_CASE_NAME);
			}
			
			int targetURLCount = 0;
			tempTargetURL = new StringBuilder();	
			for (int i = 0; i <= targetUrlListSize; i++) {
				String protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_URL_PROTOCOL_GROUP + i).trim();
				String host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_URL_HOST + i).trim();
				String port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_URL_PORT + i).trim();
				
				if (CommonUtil.isStringNullOrEmpty(protocol) || CommonUtil.isStringNullOrEmpty(host) && CommonUtil.isStringNullOrEmpty(port)) {
					
					continue;
					
				} else if (!CommonUtil.isStringNullOrEmpty(protocol) && !CommonUtil.isStringNullOrEmpty(host) && !CommonUtil.isStringNullOrEmpty(port)) {
					
					if (!Validator.isHostName(host)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_TARGET_URL_HOST));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_HOST_INVALID);
						}
						
						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_HOST_INVALID);
					}

					if (!Validator.isNumber(port)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_TARGET_URL_PORT));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_PORT_INVALID);
						}
						
						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_PORT_INVALID);
					}
					
					if(host.equals("localhost") || host.equals("127.0.0.1")){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_TARGET_URL_HOST));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_HOST_LOCALHOST_INVALID);
						}
						
						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_HOST_LOCALHOST_INVALID);
					}
					
					if(protocol.equals("1"))
						tempTargetURL.append("http://" + host + ":" + port + ";");
					else if(protocol.equals("2"))
						tempTargetURL.append("https://" + host + ":" + port + ";");

					targetURLCount++;
					
				}else if (!CommonUtil.isStringNullOrEmpty(host) || !CommonUtil.isStringNullOrEmpty(port)) {
					
					if (!CommonUtil.isStringNullOrEmpty(host)) {
						if (!Validator.isHostName(host)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_TARGET_URL_HOST));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_HOST_INVALID);
							}
							
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_HOST_INVALID);
						}
						
						if(host.equals("localhost") || host.equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_TARGET_URL_HOST));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_HOST_LOCALHOST_INVALID);
							}
							
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_HOST_LOCALHOST_INVALID);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(port)) {				
						if (!Validator.isNumber(port)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_TARGET_URL_PORT));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_PORT_INVALID);
							}
							
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_TARGET_URL_INVALID, PortalMessages.PROJECT_TARGET_URL_PORT_INVALID);
						}
					}
					
				}
			}
			
			if (targetURLCount != PortalConstants.INT_ZERO) {
				strTargetUrl = tempTargetURL.toString();
			}
			
			if (CommonUtil.isStringNullOrEmpty(strProjectName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_PROJECT_NAME, PortalMessages.NO_PROJECT_NAME);
				}
				
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_PROJECT_NAME, PortalMessages.NO_PROJECT_NAME);
			}
			
			if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
				}

				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
			}
			
			if (!ControllerHelper.isCxProjectNameValid(strProjectName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_PROJECT_NAME_INVALID, PortalMessages.PROJECT_NAME_INVALID);
				}

				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_INVALID, PortalMessages.PROJECT_NAME_INVALID);
			}
			
			int productionEnvURLCount = 0;
			tempProductionEnvURL = new StringBuilder();	
			for (int i = 0; i <= productionEnvUrlListSize; i++) {
				String protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL_PROTOCOL_GROUP + i).trim();
				String host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL_HOST + i).trim();
				String port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL_PORT + i).trim();
				
				if (CommonUtil.isStringNullOrEmpty(protocol) || CommonUtil.isStringNullOrEmpty(host) && CommonUtil.isStringNullOrEmpty(port)) {
					
					continue;
					
				} else if (!CommonUtil.isStringNullOrEmpty(protocol) && !CommonUtil.isStringNullOrEmpty(host) && !CommonUtil.isStringNullOrEmpty(port)) {
					
					if (!Validator.isHostName(host)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_HOST));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_INVALID);
						}
						
						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_INVALID);
					}

					if (!Validator.isNumber(port)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_PORT));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_PORT_INVALID);
						}
						
						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_PORT_INVALID);
					}
					
					if(host.equals("localhost") || host.equals("127.0.0.1")){
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_HOST));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_LOCALHOST_INVALID);
						}
						
						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_LOCALHOST_INVALID);
					}
					
					if(protocol.equals("1"))
						tempProductionEnvURL.append("http://" + host + ":" + port + ";");
					else if(protocol.equals("2"))
						tempProductionEnvURL.append("https://" + host + ":" + port + ";");
					
					productionEnvURLCount++;
					
				}else if (!CommonUtil.isStringNullOrEmpty(host) || !CommonUtil.isStringNullOrEmpty(port)) {
					
					
					if (!CommonUtil.isStringNullOrEmpty(host)) {
						if (!Validator.isHostName(host)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_HOST));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_INVALID);
							}
							
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_INVALID);
						}
						
						if(host.equals("localhost") || host.equals("127.0.0.1")){
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_HOST));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_LOCALHOST_INVALID);
							}
							
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_HOST_LOCALHOST_INVALID);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(port)) {				
						if (!Validator.isNumber(port)) {
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PRODUCTION_ENVIRONMENT_URL_PORT));
							
							if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_PORT_INVALID);
							}
							
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID, PortalMessages.PROJECT_PRODUCTION_ENVIRONMENT_URL_PORT_INVALID);
						}
					}
					
				}
			}
			
			if (productionEnvURLCount != PortalConstants.INT_ZERO) {
				strProductionenvironmentUrl = tempProductionEnvURL.toString();
			}
			
			if (lOwnerGroup == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_GROUP));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_OWNER_GROUP, PortalMessages.NO_OWNER_GROUP);
				}

				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_OWNER_GROUP, PortalMessages.NO_OWNER_GROUP);
			}
			
			try { 
				organization = OrganizationLocalServiceUtil.getOrganization(lOwnerGroup);
			
				if (CommonUtil.isObjectNull(organization)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_GROUP));
					
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_OWNER_GROUP_DOES_NOT_EXIST, PortalMessages.OWNER_GROUP_DOES_NOT_EXIST);
					}

					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_OWNER_GROUP_DOES_NOT_EXIST, PortalMessages.OWNER_GROUP_DOES_NOT_EXIST);
				}
			} catch (NoSuchOrganizationException e) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_GROUP));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_OWNER_GROUP_DOES_NOT_EXIST, PortalMessages.OWNER_GROUP_DOES_NOT_EXIST);
				}
				
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_OWNER_GROUP_DOES_NOT_EXIST, PortalMessages.OWNER_GROUP_DOES_NOT_EXIST);
			}
			
			if (CommonUtil.isStringNullOrEmpty(strDate)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_PROJECT_END_DATE, PortalMessages.NO_PROJECT_END_DATE);
				}

				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_PROJECT_END_DATE, PortalMessages.NO_PROJECT_END_DATE);
			}
			
			if (strDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
				}
				
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
			}
			
			dteProjectEndDate = formatter.parse(ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE));
			
			if (!CommonUtil.isStringNullOrEmpty(strSelectedUsers)) {
				strUsersArr = strSelectedUsers.split(PortalConstants.DELIMITER_COMMA);
			}
			
			if (CommonUtil.isObjectNull(strUsersArr)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_PROJECT_USER.toLowerCase()));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_PROJECT_USERS, PortalMessages.NO_PROJECT_USERS);
				}

				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_PROJECT_USERS, PortalMessages.NO_PROJECT_USERS);
			}
			
			int usersArrLength = strUsersArr.length;
			
			if (usersArrLength <= PortalConstants.INT_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NOT_SELECTED, PortalConstants.ERROR_LOG_PARAM_PROJECT_USER.toLowerCase()));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_PROJECT_USERS, PortalMessages.NO_PROJECT_USERS);
				}

				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_PROJECT_USERS, PortalMessages.NO_PROJECT_USERS);
			}
			
			lUsersArr = new long[usersArrLength];
			
			for (int index = PortalConstants.INT_ZERO; index < usersArrLength; index++) {
				lUsersArr[index] 		= Long.parseLong(strUsersArr[index]);
				List<Role> userRole 	= null;
				
				try {
					User user = UserLocalServiceUtil.getUser(lUsersArr[index]);
					
					if (CommonUtil.isObjectNull(user)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT_USER));
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.PROJECT_USER_DOES_NOT_EXIST);
						}

						throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.PROJECT_USER_DOES_NOT_EXIST);
					}
					
					userRole = user.getRoles();
				} catch (NoSuchUserException e) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT_USER));
					
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.PROJECT_USER_DOES_NOT_EXIST);
					}
					
					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.PROJECT_USER_DOES_NOT_EXIST);
				}
				
				List<Organization> userOrganizationList = OrganizationLocalServiceUtil.getUserOrganizations(lUsersArr[index]);
				
				if (CommonUtil.isListNullOrEmpty(userOrganizationList)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_DOES_NOT_BELONG_TO_GROUP);
					
					if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
						throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
					}

					throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
				} else {
					// flag if organization is found 
					boolean orgIsFound = false; 
					
					for(Role r : userRole)
					{
						if(r.getName().equals(PortalConstants.GROUP_ADMINISTRATOR)){
							// find the organization 
							for(Organization org : userOrganizationList){
								if(org.getOrganizationId() == lOwnerGroup){
									orgIsFound = true; 
									break;
								}											
							}
						}else{
							orgIsFound = (userOrganizationList.get(0).getOrganizationId() == lOwnerGroup)? true : false;
						}
					}
					
					if (false == orgIsFound) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_DOES_NOT_BELONG_TO_GROUP);
						
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
						} else {
							throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
						}
					}
				}
				
			}
			
			String strPresetId = ConfigurationFileParser.getConfig(PortalConstants.VEX_SIGNATURE_ID);
			
			if (!CommonUtil.isStringNullOrEmpty(strPresetId)) {
				lPresetId = Long.parseLong(strPresetId);
			}
			
			if (CommonUtil.isStringNullOrEmpty(webInspectionSignatureSetGroup)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_GROUP));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_WEB_INSPECTION_SIGNATURE_SET_GROUP, PortalMessages.NO_WEB_INSPECTION_SIGNATURE_SET_GROUP);
				}
				
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_WEB_INSPECTION_SIGNATURE_SET_GROUP, PortalMessages.NO_WEB_INSPECTION_SIGNATURE_SET_GROUP);
			}
			
			if (CommonUtil.isStringNullOrEmpty(serverFilesSignatureSetGroup)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_GROUP));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_SERVER_FILE_SIGNATURE_SET_GROUP, PortalMessages.NO_SERVER_FILE_SIGNATURE_SET_GROUP);
				}
				
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_SERVER_FILE_SIGNATURE_SET_GROUP, PortalMessages.NO_SERVER_FILE_SIGNATURE_SET_GROUP);
			}
			
			if (CommonUtil.isStringNullOrEmpty(serverSettingsSignatureSetGroup)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_EMPTY, PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_GROUP));
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_NO_SERVER_SETTING_SIGNATURE_SET_GROUP, PortalMessages.NO_SERVER_SETTING_SIGNATURE_SET_GROUP);
				}
				
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_NO_SERVER_SETTING_SIGNATURE_SET_GROUP, PortalMessages.NO_SERVER_SETTING_SIGNATURE_SET_GROUP);
			}
			
			project.setCaseNumber(strCaseNumber);
			project.setProjectName(strProjectName);
			project.setOwnerGroup(lOwnerGroup);
			project.setProjectEndDate(dteProjectEndDate);
			project.setType(ProjectType.VEX.getInteger());
			project.setPresetId(lPresetId);
			project.setCaseName(strCaseName);
			project.setTargetUrl(strTargetUrl);
			project.setProductionEnvironmentUrl(strProductionenvironmentUrl);
			project.setSelectedWebSignature(webInspectionSignatureSetGroup);
			project.setGetServerFiles(serverFiles);
			project.setGetServerSettings(serverSettings);
			project.setSelectedServerFilesSignature(serverFilesSignatureSetGroup);
			project.setSelectedServerSettingsSignature(serverSettingsSignatureSetGroup);
			project.setCxAndroidProjectId(Long.toString(lProjectId));
			bSuccess = true;
			
			if (userAction == PortalConstants.USER_EVENT_UPDATE_PROJECT) {
				if (CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId()) || !Validator.isNumber(project.getCxAndroidProjectId()) ) {
					params.put("CxAndroidProjectId", Long.toString(lProjectId));
					bSuccess = false;
				}
			}
			
			if (!bSuccess) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_PROJECT_REGISTRATION_FAILED);
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					throw new UBSPortalException(PortalErrors.ADD_PROJECT_FAILED, PortalMessages.ADD_PROJECT_FAILED);
				}

				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_FAILED, PortalMessages.UPDATE_PROJECT_FAILED);
			}
			
			bSuccess = ProjectLocalServiceUtil.updateProject(null, project, null, null, lUsersArr, true);
			
			if (bSuccess) {
				Object oUserId = session.getAttribute(PortalConstants.USER_ID);
				long lUserId = PortalConstants.LONG_ZERO;
				int emailEvent = PortalConstants.INT_ZERO;
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
				}
				
				if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
					emailEvent = PortalConstants.EMAIL_EVENT_ADD_PROJECT;
				} else {
					emailEvent = PortalConstants.EMAIL_EVENT_UPDATE_PROJECT;
				}
				
				MailUtil.sendEmail(emailEvent, lUserId, lProjectId);
			}
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_PROJECT, params, nspe);
			throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (NoSuchUserException nsue) {
			params.put("lUsersArr", lUsersArr);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			
			if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
				log.debug(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_PROJECT, params, nsue);
				throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
			} else {
				log.debug(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST, PortalConstants.METHOD_UPDATE_PROJECT, params, nsue);
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, nsue);
			}
		} catch (ParseException pe) {
			params.put("strDate", strDate);
			if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
				log.debug(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID, PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID, pe);
			} else {
				log.debug(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID, PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID, pe);
			}
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("strCaseNumber", strCaseNumber);
			params.put("lOwnerGroup", lOwnerGroup);
			params.put("lUsersArr", lUsersArr);
			params.put("project", project);
			params.put("presetId", lPresetId);
			params.put("strCaseName", strCaseName);
			params.put("strTargetUrl", strTargetUrl);
			params.put("strProductionenvironmentUrl", strProductionenvironmentUrl);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_UPDATE_PROJECT, params, pe);
				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
			}
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("strCaseNumber", strCaseNumber);
			params.put("strProjectName", strProjectName);
			params.put("lOwnerGroup", lOwnerGroup);
			params.put("dteProjectEndDate", dteProjectEndDate);
			params.put("lUsersArr", lUsersArr);
			params.put("presetId", lPresetId);
			params.put("strCaseName", strCaseName);
			params.put("strTargetUrl", strTargetUrl);
			params.put("strProductionenvironmentUrl", strProductionenvironmentUrl);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_UPDATE_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			String strErrorCode = ubspe.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("strCaseNumber", strCaseNumber);
				params.put("strProjectName", strProjectName);
				params.put("lOwnerGroup", lOwnerGroup);
				params.put("dteProjectEndDate", dteProjectEndDate);
				params.put("lUsersArr", lUsersArr);
				params.put("presetId", lPresetId);
				params.put("strCaseName", strCaseName);
				params.put("strTargetUrl", strTargetUrl);
				params.put("strProductionenvironmentUrl", strProductionenvironmentUrl);
			}
			
			if (ControllerHelper.isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_UPDATE_PROJECT, params, ubspe);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_UPDATE_PROJECT, params, ubspe);
			}
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean deleteProject (ActionRequest actionRequest) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		boolean bSuccess = false;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		long lCxProjectId = PortalConstants.LONG_ZERO;
		long lCxAndroidScanId = PortalConstants.LONG_ZERO;
		List<Scan> cxScanList = null;
		String strCxAndroidScanId = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		User user = null;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		
		try {
			
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}

			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			oUserId = session.getAttribute(PortalConstants.USER_ID);
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}
				
				user = UserLocalServiceUtil.getUser(lUserId);
				
				if (CommonUtil.isObjectNull(user)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
				}
			}
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.DELETE_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
			ProjectLocalServiceUtil.clearCache();
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			boolean bIsProjectUser = false;
			
			if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
				bIsProjectUser = true;
			} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
				long[] userOrgIdList = user.getOrganizationIds();
				for (long userOrgId : userOrgIdList) {
					if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
						bIsProjectUser = true;
						break;
					}
				}
			} else {
				if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
					for (ProjectUsers pu : projectUsersList) {
						if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
							bIsProjectUser = true;
							break;
						}
					}
				}
			}
			
			if (!bIsProjectUser) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
				throw new UBSPortalException(PortalErrors.DELETE_PROJECT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}
			
			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.DELETE_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(project.getCxAndroidProjectId())) {
				lCxProjectId = Long.parseLong(project.getCxAndroidProjectId());
			}
			
			//get scan list, cancel running scan/s, delete existing scan/s
			cxScanList = ScanLocalServiceUtil.getScansByProjectId(lProjectId);
			ApiInitInfo info = VexScanMgmtController.setLoginCredentials();
			for (Scan cxScan: cxScanList) {
				if (cxScan != null) {
					strCxAndroidScanId = cxScan.getCxAndroidScanId();
					
					if (!CommonUtil.isStringNullOrEmpty(strCxAndroidScanId)) {
						lCxAndroidScanId = Long.valueOf(strCxAndroidScanId);
						if (lCxAndroidScanId > PortalConstants.LONG_ZERO) {
							//Stops any ongoing crawl/scan and delete it from Vex Server.
							VexScanMgmtController.deleteScan(info, strCxAndroidScanId, cxScan.getStatus());
						}
					}
				}
			}
						
			bSuccess = ProjectLocalServiceUtil.deleteRelatedToProject(lProjectId, lCxProjectId, ProjectType.VEX.getInteger(), null);
		} catch (NoSuchProjectException nspe) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_DELETE_PROJECT, params, nspe);
			throw new UBSPortalException(PortalErrors.DELETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, nspe);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_DELETE_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_DELETE_PROJECT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_DELETE_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_DELETE_PROJECT, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			project = null;
			session = null;
			cxScanList = null;
			strCxAndroidScanId = null;
		}
		
		return bSuccess;
	}
		
	public static boolean completeProject (ActionRequest actionRequest) throws UBSPortalException {
		boolean bSuccess = false;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		User user = null;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			oUserId = session.getAttribute(PortalConstants.USER_ID);
			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}

				user = UserLocalServiceUtil.getUser(lUserId);
				
				if (CommonUtil.isObjectNull(user)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
				}
			}
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			ProjectLocalServiceUtil.clearCache();
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}
			
			boolean bIsProjectUser = false;
			
			if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
				bIsProjectUser = true;
			}
			
			List<ProjectUsers> projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
			
			if (CommonUtil.isListNullOrEmpty(projectUsersList) && !bIsProjectUser) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
				throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}

			if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
				long[] userOrgIdList = user.getOrganizationIds();
				for (long userOrgId : userOrgIdList) {
					if (String.valueOf(project.getOwnerGroup()).equals(String.valueOf(userOrgId))) {
						bIsProjectUser = true;
						break;
					}
				}
			} else {
				if (!CommonUtil.isListNullOrEmpty(projectUsersList)) {
					for (ProjectUsers pu : projectUsersList) {
						if (pu.getUserId().equalsIgnoreCase(user.getEmailAddress())) {
							bIsProjectUser = true;
							break;
						}
					}
				}
			}
			
			if (!bIsProjectUser) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
				throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			if (isProjectComplete(lProjectId)) {
				bSuccess = ProjectLocalServiceUtil.completeProject(lProjectId);
			}
		} catch (NoSuchProjectException e) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_COMPLETE_PROJECT, params, e);
			throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, e);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_COMPLETE_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}
			
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_COMPLETE_PROJECT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_COMPLETE_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_COMPLETE_PROJECT, params, ubspe);
			throw ubspe;
		}  finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	public static boolean openProject (ActionRequest actionRequest) throws UBSPortalException {
		boolean bSuccess = false;
		long lProjectId = PortalConstants.LONG_ZERO;
		Project project = null;
		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = null;
		long lUserId = PortalConstants.LONG_ZERO;
		Object oUserRole = null;
		int iUserRole = PortalConstants.INT_ZERO;
		User user = null;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			oUserId = session.getAttribute(PortalConstants.USER_ID);
			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}

				user = UserLocalServiceUtil.getUser(lUserId);
				
				if (CommonUtil.isObjectNull(user)) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST);
				}
			}
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.OPEN_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			ProjectLocalServiceUtil.clearCache();
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			if (CommonUtil.isObjectNull(project)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
				throw new UBSPortalException(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST);
			}
			
			if (project.getType() != ProjectType.VEX.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INCORRECT, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.OPEN_PROJECT_PROJECT_TYPE_INVALID, PortalMessages.PROJECT_TYPE_NOT_VEX);
			}
			
			if (iUserRole != UserRole.OVERALL_ADMIN.getInteger() && iUserRole != UserRole.GROUP_ADMIN.getInteger()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_USER_NO_RIGHTS_ACTION);
				throw new UBSPortalException(PortalErrors.OPEN_PROJECT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			if (project.getStatus() == ProjectStatus.COMPLETE.getInteger()) {
				bSuccess = ProjectLocalServiceUtil.openProject(project);
			}
			
			if (bSuccess) {
				MailUtil.sendEmail(PortalConstants.USER_EVENT_OPEN_PROJECT, lUserId, lProjectId);
			}else{
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_OPEN_PROJECT_FAILED);
				log.debug(PortalErrors.OPEN_PROJECT_FAILED, PortalConstants.METHOD_OPEN_PROJECT, params, new UBSPortalException(PortalErrors.OPEN_PROJECT_FAILED, PortalMessages.OPEN_PROJECT_FAILED));
			}
			
		} catch (NoSuchProjectException e) {
			params.put("lProjectId", lProjectId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_PROJECT));
			log.debug(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST, PortalConstants.METHOD_OPEN_PROJECT, params, e);
			throw new UBSPortalException(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST, PortalMessages.PROJECT_DOES_NOT_EXIST, e);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			
			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_OPEN_PROJECT, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_OPEN_PROJECT, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_OPEN_PROJECT, params, se);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, se);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
				params.put("project", project);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_OPEN_PROJECT, params, ubspe);
			throw ubspe;
		}  finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bSuccess;
	}
	
	private static boolean isProjectComplete (long lProjectId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Scan> scanList = null;
		boolean bFlag = false;
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			scanList = ScanLocalServiceUtil.getScansByProjectId(lProjectId);
			
			if (scanList.size() == PortalConstants.INT_ZERO) {
				return true;
			}
			
			for (Scan scan : scanList) {
				if (scan.getStatus() == ScanStatus.REPORT_MAKING.getInteger()) {
					List<Report> reportList = ReportLocalServiceUtil.getReports(scan.getScanId());
					
					if (!CommonUtil.isListNullOrEmpty(reportList) && reportList.size() < PortalConstants.REPORT_COUNT_LIMIT) {
						return false;
					}
				} else if (scan.getStatus() == ScanStatus.SCAN_WAITING.getInteger() ||
					scan.getStatus() == ScanStatus.SCANNING.getInteger() ||
					scan.getStatus() == ScanStatus.FAILURE.getInteger() ||
					scan.getStatus() == ScanStatus.CRAWLING_WAITING.getInteger() ||
					scan.getStatus() == ScanStatus.CRAWLING.getInteger() ||
					scan.getStatus() == ScanStatus.CRAWLING_FAILURE.getInteger()) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_COMPLETE_PROJECT_FAILED);
					log.debug(PortalErrors.COMPLETE_PROJECT_PROJECT_CANNOT_BE_COMPLETED, PortalConstants.METHOD_IS_PROJECT_COMPLETE, params, new UBSPortalException(PortalErrors.COMPLETE_PROJECT_PROJECT_CANNOT_BE_COMPLETED, PortalMessages.COMPLETE_PROJECT_FAILED));
					return false;
				} else {
					bFlag = true;
				}
			}
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);

			if (CommonUtil.isPortalError(pe.getMessage())) {
				String message = CommonUtil.getCxErrorMessage(pe.getMessage());
				log.debug(pe.getMessage(), PortalConstants.METHOD_IS_PROJECT_COMPLETE, params, pe);
				throw new UBSPortalException(pe.getMessage(), message, pe);
			}

			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_IS_PROJECT_COMPLETE, params, pe);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, pe);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lProjectId", lProjectId);
			}
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_IS_PROJECT_COMPLETE, params, ubspe);
			throw ubspe;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return bFlag;
	}
	
	public static List<ProjectItem> updateProjectTargerUrlForValidation(ActionRequest actionRequest) {
		String protocol=PortalConstants.STRING_EMPTY;
		String host=PortalConstants.STRING_EMPTY;
		String port=PortalConstants.STRING_EMPTY;
		List<ProjectItem> projectItemArr = new ArrayList<>();
		
		int targetUrlListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_URL_LIST_SIZE);
		for (int i = 1; i <= targetUrlListSize; i++) {
			protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_URL_PROTOCOL_GROUP + i).trim();
			host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_URL_HOST + i).trim();
			port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_URL_PORT + i).trim();
			
			if(CommonUtil.isStringNullOrEmpty(protocol))
				continue;
			
			ProjectItem projectItem = new ProjectItem();
			projectItem.setTargetUrlprotocol(protocol);
			projectItem.setTargetUrlhost(host);
			projectItem.setTargetUrlport(port);
			projectItemArr.add(projectItem);
		}
			
		return projectItemArr;
	}
	
	public static List<ProjectItem> updateProjectProductionEnvUrlForValidation(ActionRequest actionRequest) {
		String protocol=PortalConstants.STRING_EMPTY;
		String host=PortalConstants.STRING_EMPTY;
		String port=PortalConstants.STRING_EMPTY;
		List<ProjectItem> projectItemArr = new ArrayList<>();
		
		int productionEnvUrlListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL_LIST_SIZE);
		for (int i = 1; i <= productionEnvUrlListSize; i++) {
			protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL_PROTOCOL_GROUP + i).trim();
			host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL_HOST + i).trim();
			port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL_PORT + i).trim();
			
			if(CommonUtil.isStringNullOrEmpty(protocol))
				continue;
			
			ProjectItem projectItem = new ProjectItem();
			projectItem.setProductionEnvironmentUrlprotocol(protocol);
			projectItem.setProductionEnvironmentUrlhost(host);
			projectItem.setProductionEnvironmentUrlport(port);
			projectItemArr.add(projectItem);
		}
			
		return projectItemArr;
	}
	
	public static List<ProjectItem> getProjectURLs(Project project, String URLType) throws UBSPortalException {
		List<ProjectItem> projectItemArr = new ArrayList<>();
		Map<String, Object> params = new HashMap<String, Object>();
		String[] array = null;
		
		if (!CommonUtil.isStringNullOrEmpty(project.getTargetUrl()) && URLType.equals(PortalConstants.PARAM_PROJECT_TARGET_URLS)) {
			array = project.getTargetUrl().split(";");
			for (String url : array) {
				try {
					URL temp = new URL(url);
					
					ProjectItem projectItem = new ProjectItem();
					if(temp.getProtocol().equals("http"))
						projectItem.setTargetUrlprotocol("1");
					else if(temp.getProtocol().equals("https"))
						projectItem.setTargetUrlprotocol("2");
					
					projectItem.setTargetUrlhost(temp.getHost());
					projectItem.setTargetUrlport(String.valueOf(temp.getPort()));
					projectItemArr.add(projectItem);
				} catch (MalformedURLException e) {
					params.put("lProjectId",project.getProjectId());
					log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, e);
					throw new UBSPortalException(PortalErrors.MALFORMEDURL_EXCEPTION, PortalMessages.MALFORMEDURL_EXCEPTION, e);
				} 
			}
		}else if (!CommonUtil.isStringNullOrEmpty(project.getProductionEnvironmentUrl()) && URLType.equals(PortalConstants.PARAM_PROJECT_PRODUCTION_ENVIRONMENT_URLS)) {
			array = project.getProductionEnvironmentUrl().split(";");
			for (String url : array) {
				try {
					URL temp = new URL(url);
					
					ProjectItem projectItem = new ProjectItem();
					if(temp.getProtocol().equals("http"))
						projectItem.setProductionEnvironmentUrlprotocol("1");
					else if(temp.getProtocol().equals("https"))
						projectItem.setProductionEnvironmentUrlprotocol("2");
					
					projectItem.setProductionEnvironmentUrlhost(temp.getHost());
					projectItem.setProductionEnvironmentUrlport(String.valueOf(temp.getPort()));
					projectItemArr.add(projectItem);
				} catch (MalformedURLException e) {
					params.put("lProjectId",project.getProjectId());
					log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, e);
					throw new UBSPortalException(PortalErrors.MALFORMEDURL_EXCEPTION, PortalMessages.MALFORMEDURL_EXCEPTION, e);
				} 
			}
		}
		
		return projectItemArr;
	}
}
