package jp.ubsecure.portal.jubjub.portlet.form.navigator.organization;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.users.admin.web.servlet.taglib.ui.OrganizationServicesFormNavigatorEntry;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property = {
		PortalConstants.KEY_FORM_NAVIGATOR_ENTRY_ORDER + PortalConstants.ENTRY_ORDER_ORGANIZATION_SERVICES
	},
	service = FormNavigatorEntry.class
)
public class OrganizationServicesFormNavigatorEntryExt extends OrganizationServicesFormNavigatorEntry {
	@Override
	public String getFormNavigatorId() {
		return PortalConstants.ORGANIZATION_PREFIX + super.getFormNavigatorId();
	}

	@Override
	public boolean isVisible(User user, Organization organization) {
		boolean visible = GetterUtil.getBoolean(PropsUtil.get(PortalConstants.ORGANIZATION_SERVICES_VISIBLE), true);
		return visible && super.isVisible(user, organization);
	}
}
