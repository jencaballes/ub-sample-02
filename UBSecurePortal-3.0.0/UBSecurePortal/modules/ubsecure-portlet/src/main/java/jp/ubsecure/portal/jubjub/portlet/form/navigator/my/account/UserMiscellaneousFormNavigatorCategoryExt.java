package jp.ubsecure.portal.jubjub.portlet.form.navigator.my.account;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorCategory;
import com.liferay.users.admin.web.servlet.taglib.ui.UserMiscellaneousFormNavigatorCategory;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property = {
		PortalConstants.KEY_FORM_NAVIGATOR_CATEGORY_ORDER + PortalConstants.CATEGORY_ORDER_USER_MISCELLANEOUS
	},
	service = FormNavigatorCategory.class
)
public class UserMiscellaneousFormNavigatorCategoryExt extends UserMiscellaneousFormNavigatorCategory {
	@Override
	public String getFormNavigatorId() {
		return PortalConstants.MY_ACCOUNT_PREFIX + super.getFormNavigatorId();
	}
}