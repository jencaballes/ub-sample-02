/**
 * CxWSResponseConfigSetList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810;

public class CxWSResponseConfigSetList  extends jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse  implements java.io.Serializable {
    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ConfigurationSet[] configSetList;

    public CxWSResponseConfigSetList() {
    }

    public CxWSResponseConfigSetList(
           boolean isSuccesfull,
           java.lang.String errorMessage,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ConfigurationSet[] configSetList) {
        super(
            isSuccesfull,
            errorMessage);
        this.configSetList = configSetList;
    }


    /**
     * Gets the configSetList value for this CxWSResponseConfigSetList.
     * 
     * @return configSetList
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ConfigurationSet[] getConfigSetList() {
        return configSetList;
    }


    /**
     * Sets the configSetList value for this CxWSResponseConfigSetList.
     * 
     * @param configSetList
     */
    public void setConfigSetList(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ConfigurationSet[] configSetList) {
        this.configSetList = configSetList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CxWSResponseConfigSetList)) return false;
        CxWSResponseConfigSetList other = (CxWSResponseConfigSetList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.configSetList==null && other.getConfigSetList()==null) || 
             (this.configSetList!=null &&
              java.util.Arrays.equals(this.configSetList, other.getConfigSetList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getConfigSetList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConfigSetList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConfigSetList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CxWSResponseConfigSetList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSResponseConfigSetList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("configSetList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ConfigSetList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ConfigurationSet"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ConfigurationSet"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
