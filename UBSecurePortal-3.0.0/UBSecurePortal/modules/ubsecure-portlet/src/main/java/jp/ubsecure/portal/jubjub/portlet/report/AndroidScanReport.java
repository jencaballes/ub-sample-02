package jp.ubsecure.portal.jubjub.portlet.report;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.googlecode.jcsv.CSVStrategy;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import com.liferay.portal.kernel.util.StringUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.MetaCsvEntry;
import jp.ubsecure.portal.jubjub.portlet.model.ReportItem;
import jp.ubsecure.portal.jubjub.portlet.model.VulnCsvEntry;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.YCloudUtil;

public class AndroidScanReport {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(AndroidScanReport.class);

	public static void downloadCSVFromStorage (List<Object> reportList, String folderLocation) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strBucketName = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String [] strNames = null;
		String strFile = PortalConstants.STRING_EMPTY;
		String strDownloadLocation = PortalConstants.STRING_EMPTY;
		File reportFilePath = null;
		String strReportFileCompleteName = PortalConstants.STRING_EMPTY;
		
		try {
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				for (Object object : reportList) {
					ReportItem report = (ReportItem) object;
					
					strBucketName = report.getBucketName(); 
					strFileName =  report.getReportName();
					strNames = StringUtil.split(strFileName, File.separator);
					strFile = strNames[1];
					strDownloadLocation = strNames[0] + PortalConstants.STR_FWD_SLASH + strFile;
					
					reportFilePath = new File(folderLocation);
					reportFilePath.mkdirs();
					
					if (reportFilePath.isDirectory()) {
						strReportFileCompleteName = reportFilePath.getAbsolutePath() + File.separator + strFile;
						YCloudUtil.getInstance().downloadObject(strBucketName, strDownloadLocation, strReportFileCompleteName);
					}
				}
			}
		} catch (UBSPortalException e) {
			params.put("strBucketName", strBucketName);
			params.put("strDownloadLocation", strDownloadLocation);
			params.put("strReportFileCompleteName", strReportFileCompleteName);
			log.debug(e.getErrorCode(), "downloadXMLFromStorage", params, e);
			throw new UBSPortalException(e.getErrorCode(), PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}
	}
	
	public static void generateAndroidVulnSummary (List<Object> reportList, String folderLocation) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File vulnSummaryCsvFile = null;
		CSVWriter<VulnCsvEntry> writer = null;
		VulnCsvEntry entry = null;
		List<VulnCsvEntry> csvValueList = null;
		Writer fileWriter = null;
		int iNumber = 1;
		
		try {
			if (!masterFileExists()) {
				params.put("masterFileExists()", masterFileExists());
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_VULN_MASTER_FILE));
				throw new UBSPortalException(PortalErrors.VUlN_MASTER_FILE_ERROR);
			}
			
			vulnSummaryCsvFile = new File(folderLocation
					+ File.separator
					+ PortalConstants.VULNCSV);
					
				new File(vulnSummaryCsvFile.getParent()).mkdirs();
			
			CSVStrategy strategy = new CSVStrategy(',', '\"', '\0', false, false);
			List<VulnCsvEntry> vulnCsvEntryList = new ArrayList<VulnCsvEntry>();
			
			entry = new VulnCsvEntry();
			entry.setNumber(PortalConstants.HEADER_NUMBER);
			entry.setRiskLevel(PortalConstants.HEADER_SEVERITY);
			entry.setVulnerabilityName(PortalConstants.HEADER_VULN_NAME);
			entry.setSignatureId(PortalConstants.HEADER_SIGNATURE_ID);
			entry.setCategory(PortalConstants.HEADER_CATEGORY);
			entry.setFileName(PortalConstants.HEADER_FILE_NAME);
			entry.setTrigger1(PortalConstants.HEADER_TRIGGER1);
			entry.setTrigger2(PortalConstants.HEADER_TRIGGER2);
			entry.setTrigger3(PortalConstants.HEADER_TRIGGER3);
			entry.setReportId(PortalConstants.HEADER_REPORTID);
			entry.setExclusionFlag(PortalConstants.HEADER_FLAG);
			entry.setComment(PortalConstants.HEADER_COMMENT);
			vulnCsvEntryList.add(entry);
			
			for (Object item : reportList) {
				ReportItem report = new ReportItem();
				report = (ReportItem) item;
				String reportName = report.getReportName();
				
				if (!CommonUtil.isStringNullOrEmpty(reportName)
						&& reportName.contains(PortalConstants.VULNCSV)) {
					csvValueList = vulnCsvReader(folderLocation
							+ File.separator
							+ reportName.split("/")[1]);
					
					for (VulnCsvEntry entryItem : csvValueList) {
						entryItem.setNumber(String.valueOf(iNumber));
						vulnCsvEntryList.add(entryItem);
						iNumber++;
					}
				}
			}
			
			if (!CommonUtil.canWrite(vulnSummaryCsvFile.getParent())) {
				params.put("vulnSummaryCsvFile", vulnSummaryCsvFile);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(vulnSummaryCsvFile), PortalConstants.SHIFT_JIS));
			writer = new CSVWriterBuilder<VulnCsvEntry>(fileWriter).strategy(strategy).entryConverter(new VulnCsvEntryParser()).build();
			writer.writeAll(vulnCsvEntryList);
			writer.flush();
			
			fileWriter.close();
			writer.close();
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_ANDROID_VULN_SUMMARY, params, e);
			throw e;
		} catch (FileNotFoundException e) {
			log.debug(PortalErrors.INVALID_FILE, PortalConstants.METHOD_GENERATE_ANDROID_VULN_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.INVALID_FILE, e);
		} catch (UnsupportedEncodingException e) {
			log.debug(PortalErrors.UNSUPPORTED_ENCODING_EXCEPTION, PortalConstants.METHOD_GENERATE_ANDROID_VULN_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNSUPPORTED_ENCODING_EXCEPTION, e);
		} catch (IOException e) {
			log.debug(PortalErrors.INVALID_FILE, PortalConstants.METHOD_GENERATE_ANDROID_VULN_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.INVALID_FILE, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_ANDROID_VULN_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		} finally {
			vulnSummaryCsvFile = null;
			writer = null;
			entry = null;
			fileWriter = null;
			params.clear();
			params = null;
			csvValueList = null;
		}
	}
	
	public static void generateAndroidMetaSummary (List<Object> reportList, String folderLocation) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File metaSummaryCsvFile = null;
		MetaCsvEntry entry = null;
		List<MetaCsvEntry> csvValueList = null;
		Writer fileWriter = null;
		FileOutputStream outputStream = null;
		OutputStreamWriter osWriter = null;
		CSVWriter<MetaCsvEntry> writer = null;
		
		try {
			if (!masterFileExists()) {
				params.put("masterFileExists()", masterFileExists());
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_VULN_MASTER_FILE));
				throw new UBSPortalException(PortalErrors.VUlN_MASTER_FILE_ERROR);
			}
			
			metaSummaryCsvFile = new File(folderLocation
					+ File.separator
					+ PortalConstants.METACSV);
					
				new File(metaSummaryCsvFile.getParent()).mkdirs();
			
			CSVStrategy strategy = new CSVStrategy(',', '\"', '\0', false, false);
			List<MetaCsvEntry> metaCsvEntryList = new ArrayList<MetaCsvEntry>();
			
			entry = new MetaCsvEntry();
			entry.setCaseNumber(PortalConstants.HEADER_CASE_NUMBER);
			entry.setCxAndroidScanId(PortalConstants.HEADER_SCAN_ID);
			entry.setProjectName(PortalConstants.HEADER_PROJECT_NAME);
			entry.setReportId(PortalConstants.HEADER_REPORTID);
			entry.setHighCount(PortalConstants.HEADER_HIGH);
			entry.setMediumCount(PortalConstants.HEADER_MED);
			entry.setLowCount(PortalConstants.HEADER_LOW);
			entry.setInfoCount(PortalConstants.HEADER_INFO);
			entry.setScanDate(PortalConstants.HEADER_SCAN_START_TIME);
			entry.setScanTime(PortalConstants.HEADER_SCAN_EXEC);
			entry.setSignatureSetName(PortalConstants.HEADER_SIGNATURE);
			entry.setExcludedPackageNameList(PortalConstants.HEADER_EXCLUSION_PKG);
			entry.setEngineVersion(PortalConstants.HEADER_VEX_VERSION);
			entry.setOwnerGroup(PortalConstants.HEADER_GROUP_NAME);
			metaCsvEntryList.add(entry);
			
			for (Object item : reportList) {
				ReportItem report = new ReportItem();
				report = (ReportItem) item;
				String reportName = report.getReportName();
				
				if (!CommonUtil.isStringNullOrEmpty(reportName)
						&& reportName.contains(PortalConstants.METACSV)) {
					csvValueList = metaCsvReader(folderLocation
							+ File.separator
							+ reportName.split("/")[1]);
					
					for (MetaCsvEntry entryItem : csvValueList) {
						metaCsvEntryList.add(entryItem);
					}
				}
			}
			
			if (!CommonUtil.canWrite(metaSummaryCsvFile.getParent())) {
				params.put("metaSummaryCsvFile", metaSummaryCsvFile);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			outputStream = new FileOutputStream(metaSummaryCsvFile);
			osWriter = new OutputStreamWriter(outputStream, PortalConstants.SHIFT_JIS);

			fileWriter = new BufferedWriter(osWriter);
			writer = new CSVWriterBuilder<MetaCsvEntry>(fileWriter).strategy(strategy).entryConverter(new MetaCsvEntryParser()).build();
			writer.writeAll(metaCsvEntryList);
			writer.flush();

			outputStream.close();
			osWriter.close();
			fileWriter.close();
			writer.close();
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_ANDROID_META_SUMMARY, params, e);
			throw e;
		} catch (FileNotFoundException e) {
			log.debug(PortalErrors.INVALID_FILE, PortalConstants.METHOD_GENERATE_ANDROID_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.INVALID_FILE, e);
		} catch (UnsupportedEncodingException e) {
			log.debug(PortalErrors.UNSUPPORTED_ENCODING_EXCEPTION, PortalConstants.METHOD_GENERATE_ANDROID_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNSUPPORTED_ENCODING_EXCEPTION, e);
		} catch (IOException e) {
			log.debug(PortalErrors.INVALID_FILE, PortalConstants.METHOD_GENERATE_ANDROID_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.INVALID_FILE, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_ANDROID_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		} finally {
			metaSummaryCsvFile = null;
			writer = null;
			entry = null;
			fileWriter = null;
			outputStream = null;
			osWriter = null;
			params.clear();
			params = null;
			csvValueList = null;
		}
	}
	
	private static List<VulnCsvEntry> vulnCsvReader (String csvFilePath) {
		Map<String, Object> params = new HashMap<String, Object>();
		String line = PortalConstants.STRING_EMPTY;
		int iLineNumber = 0;
		VulnCsvEntry entry = null;
		List<VulnCsvEntry> vulnCsvEntryList = new ArrayList<VulnCsvEntry>();
		List<String> valueList = null;
		
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFilePath), PortalConstants.SHIFT_JIS))) {
			if (CommonUtil.isStringNullOrEmpty(csvFilePath)) {
				params.put("csvFilePath", csvFilePath);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
				throw new UBSPortalException(PortalErrors.INVALID_FILE_PATH);
			}
			
			while ((line = bufferedReader.readLine()) != null) {
				iLineNumber++;
				
				if (iLineNumber > 1) {
					valueList = splitValuesByComma(line);
					
					entry = new VulnCsvEntry();
					entry.setNumber(valueList.get(0));
					entry.setRiskLevel(valueList.get(1));
					entry.setVulnerabilityName(valueList.get(2));
					entry.setSignatureId(valueList.get(3));
					entry.setCategory(valueList.get(4));
					entry.setFileName(valueList.get(5));
					entry.setTrigger1(valueList.get(6));
					entry.setTrigger2(valueList.get(7));
					entry.setTrigger3(valueList.get(8));
					entry.setReportId(valueList.get(9));
					entry.setExclusionFlag(valueList.get(10));
					entry.setComment(valueList.get(11));
					vulnCsvEntryList.add(entry);
					
					entry = null;
				}
			}
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_VULN_CSV_READER, params, e);
		} catch (FileNotFoundException e) {
			log.debug(PortalErrors.INVALID_FILE, PortalConstants.METHOD_VULN_CSV_READER, params, e);
		} catch (IOException e) {
			log.debug(PortalErrors.INVALID_FILE, PortalConstants.METHOD_VULN_CSV_READER, params, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_VULN_CSV_READER, params, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return vulnCsvEntryList;
	}
	
	private static List<MetaCsvEntry> metaCsvReader (String csvFilePath) {
		Map<String, Object> params = new HashMap<String, Object>();
		String line = PortalConstants.STRING_EMPTY;
		int iLineNumber = 0;
		MetaCsvEntry entry = null;
		List<MetaCsvEntry> metaCsvEntryList = new ArrayList<MetaCsvEntry>();
		List<String> valueList = null;
		
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFilePath), PortalConstants.SHIFT_JIS))) {
			if (CommonUtil.isStringNullOrEmpty(csvFilePath)) {
				params.put("csvFilePath", csvFilePath);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
				throw new UBSPortalException(PortalErrors.INVALID_FILE_PATH);
			}
			
			while ((line = bufferedReader.readLine()) != null) {
				iLineNumber++;
				
				if (iLineNumber > 1) {
					valueList = splitValuesByComma(line);
					
					entry = new MetaCsvEntry();
					entry.setCaseNumber(valueList.get(0));
					entry.setCxAndroidScanId(valueList.get(1));
					entry.setProjectName(valueList.get(2));
					entry.setReportId(valueList.get(3));
					entry.setHighCount(valueList.get(4));
					entry.setMediumCount(valueList.get(5));
					entry.setLowCount(valueList.get(6));
					entry.setInfoCount(valueList.get(7));
					entry.setScanDate(valueList.get(8));
					entry.setScanTime(valueList.get(9));
					entry.setSignatureSetName(valueList.get(10));
					entry.setExcludedPackageNameList(valueList.get(11));
					entry.setEngineVersion(valueList.get(12));
					entry.setOwnerGroup(valueList.get(13));
					metaCsvEntryList.add(entry);
					
					entry = null;
				}
			}
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_META_CSV_READER, params, e);
		} catch (FileNotFoundException e) {
			log.debug(PortalErrors.INVALID_FILE, PortalConstants.METHOD_META_CSV_READER, params, e);
		} catch (IOException e) {
			log.debug(PortalErrors.INVALID_FILE, PortalConstants.METHOD_META_CSV_READER, params, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_META_CSV_READER, params, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return metaCsvEntryList;
	}
	
	private static List<String> splitValuesByComma (String strLine) throws UnsupportedEncodingException {
		List<String> valueList = new ArrayList<String>();
		String tempValue = PortalConstants.STRING_EMPTY;
		char currChar;
		char prevChar = '\0';
		int iLineLength = 0;
		boolean bOpenQuote = false;
		
		if (!CommonUtil.isStringNullOrEmpty(strLine)) {
			iLineLength = strLine.length();
			
			for (int index = 0; index < iLineLength; index++) {
				currChar = strLine.charAt(index);
				
				if (currChar == '"') {
					bOpenQuote = !bOpenQuote;
				} else if (currChar == PortalConstants.CHAR_COMMA) {
					if (bOpenQuote) {
						tempValue += currChar;
					} else {
						valueList.add(tempValue);
						tempValue = PortalConstants.STRING_EMPTY;
					}
				} else {
					tempValue += currChar;
				}
				
				prevChar = currChar;
			}
			
			if (!CommonUtil.isStringNullOrEmpty(tempValue)) {
				valueList.add(tempValue);
			}
			
			if (prevChar == PortalConstants.CHAR_COMMA) {
				valueList.add(PortalConstants.STR_SPACE);
			}
		}
		
		return valueList;
	}
	
	private static boolean masterFileExists() {
		return VulnerabilityMasterParser.getvulnerabilityMasterFile().exists();
	}
}