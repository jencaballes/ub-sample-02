/**
 * StopDataRetentionResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810;

public class StopDataRetentionResponse  implements java.io.Serializable {
    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse stopDataRetentionResult;

    public StopDataRetentionResponse() {
    }

    public StopDataRetentionResponse(
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse stopDataRetentionResult) {
           this.stopDataRetentionResult = stopDataRetentionResult;
    }


    /**
     * Gets the stopDataRetentionResult value for this StopDataRetentionResponse.
     * 
     * @return stopDataRetentionResult
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse getStopDataRetentionResult() {
        return stopDataRetentionResult;
    }


    /**
     * Sets the stopDataRetentionResult value for this StopDataRetentionResponse.
     * 
     * @param stopDataRetentionResult
     */
    public void setStopDataRetentionResult(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse stopDataRetentionResult) {
        this.stopDataRetentionResult = stopDataRetentionResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StopDataRetentionResponse)) return false;
        StopDataRetentionResponse other = (StopDataRetentionResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.stopDataRetentionResult==null && other.getStopDataRetentionResult()==null) || 
             (this.stopDataRetentionResult!=null &&
              this.stopDataRetentionResult.equals(other.getStopDataRetentionResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStopDataRetentionResult() != null) {
            _hashCode += getStopDataRetentionResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StopDataRetentionResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", ">StopDataRetentionResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stopDataRetentionResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "StopDataRetentionResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSBasicRepsonse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
