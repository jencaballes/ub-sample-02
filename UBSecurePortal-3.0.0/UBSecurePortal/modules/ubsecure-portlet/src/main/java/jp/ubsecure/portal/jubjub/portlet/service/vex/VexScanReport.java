package jp.ubsecure.portal.jubjub.portlet.service.vex;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.googlecode.jcsv.CSVStrategy;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.util.StringUtil;

import jp.ubsecure.jabberwock.cli.util.Message;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.DetectionResultSummaryData;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.VexVulnSummaryData;
import jp.ubsecure.portal.jubjub.portlet.model.VexVulnSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.report.VexMetaSummaryEntryParser;
import jp.ubsecure.portal.jubjub.portlet.report.VexVulnSummaryEntryParser;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.ReportItem;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.VexMetaSummaryData;
import jp.ubsecure.portal.jubjub.portlet.model.VexMetaSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.VexScanSettingLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.vex.MetaSummaryHandler;
import jp.ubsecure.portal.jubjub.portlet.service.vex.VulnerabilitySummaryHandler;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.YCloudUtil;
/**
 * Class for Report Generation of Results 
 * after scanning a source code File 
 * to Vex.
 * 
 * Reports are:  
 * 	1. XML report: REPORT.xml
 * 		- this is generated from Vex
 *  2. DOC report: REPORT.doc		
 *  	- this is generated from Vex
 *  3. Vulnerability CSV: VULN.csv - not yet implemented
 *  	- this is generated based from XML report and Vulnerability Master File
 *  4. Scan meta-information CSV: META.csv - not yet implemented
 *  	- this is generated based from XML report in which 
 *  		each level of vulnerability (High, Medium, Low, Information) is counted
 *  		 and counts are stored in DB
 * 	
 * Once the report generation process is done, the report is stored in YCloud storage.
 * 					 
 * @author ASI-1199
 *
 */
public class VexScanReport extends DefaultHandler {
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexScanReport.class);	
	private static YCloudUtil ycloudUtil = YCloudUtil.getInstance();
	
	public static void downloadSingleXMLFromStorage (Report report, String folderLocation) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strBucketName = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String [] strNames = null;
		String strFile = PortalConstants.STRING_EMPTY;
		String strDownloadLocation = PortalConstants.STRING_EMPTY;
		File reportFilePath = null;
		String strReportFileCompleteName = PortalConstants.STRING_EMPTY;
		
		try {
			if (!CommonUtil.isObjectNull(report)) {
					strBucketName = report.getReportBucketName(); 
					strFileName =  report.getReportName();
					strNames = StringUtil.split(strFileName, File.separator);
					strFile = strNames[1];
					strDownloadLocation = strNames[0] + PortalConstants.STR_FWD_SLASH + strFile;
					
					reportFilePath = new File(folderLocation);
					reportFilePath.mkdirs();
					
					if (reportFilePath.isDirectory()) {
						strReportFileCompleteName = reportFilePath.getAbsolutePath() + File.separator + strFile;
						ycloudUtil.downloadObject(strBucketName, strDownloadLocation, strReportFileCompleteName);
					}
			}
		} catch (UBSPortalException e) {
			params.put("strBucketName", strBucketName);
			params.put("strDownloadLocation", strDownloadLocation);
			params.put("strReportFileCompleteName", strReportFileCompleteName);
			log.debug(e.getErrorCode(), "downloadXMLFromStorage", params, e);
			throw new UBSPortalException(e.getErrorCode(), PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}
	}

	public static List<DetectionResultSummaryData> getDetectionResultSummaryData (File xmlReport) throws UBSPortalException {
		DetectionResultSummaryHandler detectionResultSummaryHandler = null;
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			
			detectionResultSummaryHandler = new DetectionResultSummaryHandler();
			parser.parse(xmlReport, detectionResultSummaryHandler);
		} catch (ParserConfigurationException e) {
			log.debug(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalConstants.METHOD_GET_CX_DETECTION_RESULT_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (SAXException e) {
			log.debug(PortalErrors.SAX_EXCEPTION, PortalConstants.METHOD_GET_CX_DETECTION_RESULT_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.SAX_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (IOException e) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_CX_DETECTION_RESULT_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_CX_DETECTION_RESULT_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (FactoryConfigurationError e) {
			log.debug(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalConstants.METHOD_GET_CX_DETECTION_RESULT_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		}
		
		return detectionResultSummaryHandler.detectionResultList;
	}
	
	public static List<VexVulnSummaryData> getVexVulnSummaryData (File xmlReport, Date startDate, Date endDate) throws UBSPortalException {
		VulnerabilitySummaryHandler vulnSummaryHandler = null;
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			
			vulnSummaryHandler = new VulnerabilitySummaryHandler(startDate, endDate);
			parser.parse(xmlReport, vulnSummaryHandler);
		} catch (ParserConfigurationException e) {
			log.debug(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (SAXException e) {
			log.debug(PortalErrors.SAX_EXCEPTION, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.SAX_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (IOException e) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (FactoryConfigurationError e) {
			log.debug(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		}
		
		return vulnSummaryHandler.csvVulnSummaryDataList;
	}
	
	public static VexMetaSummaryData getVexMetaSummaryData(File xmlReport) throws UBSPortalException {
		MetaSummaryHandler metaSummaryHandler = null;
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			
			metaSummaryHandler = new MetaSummaryHandler();
			parser.parse(xmlReport, metaSummaryHandler);
		} catch (ParserConfigurationException e) {
			log.debug(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalConstants.GET_CX_META_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (SAXException e) {
			log.debug(PortalErrors.SAX_EXCEPTION, PortalConstants.GET_CX_META_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.SAX_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (IOException e) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.GET_CX_META_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.GET_CX_META_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (FactoryConfigurationError e) {
			log.debug(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		}

		return metaSummaryHandler.csvMetaSummaryData;
	}
	
	public static void generateVexVulnSummary(List<Object> reportList, String folderLocation, Date startDate, Date endDate) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File vexVulnSummaryCsvFile = null;
		CSVStrategy strategy = null;
		List<VexVulnSummaryEntry> summaryCsvEntryList = new ArrayList<VexVulnSummaryEntry>();
		List<VexVulnSummaryData> summaryCsvDataList = new ArrayList<VexVulnSummaryData>();
		int num = PortalConstants.INT_ONE;
		Writer fileWriter = null;
		CSVWriter<VexVulnSummaryEntry> writer = null;
		
		try {
			vexVulnSummaryCsvFile = new File(folderLocation
					+ File.separator
					+ PortalConstants.VULNCSV);
			
			new File(vexVulnSummaryCsvFile.getParent()).mkdirs();
			
			strategy = new CSVStrategy(',', '"', '\0', false, false);
			
			// Setting Headers
		    VexVulnSummaryEntry entry = new VexVulnSummaryEntry();
		    	
			entry.setNumber(PortalConstants.HEADER_NUMBER); 								//No
			entry.setRiskDegree(PortalConstants.HEADER_DEGREE_OF_RISK); 					//蜊ｱ髯ｺ蠎ｦ
			entry.setVulnCategory(PortalConstants.HEADER_VULN_CATEGORY); 					//閼�蠑ｱ諤ｧ繧ｫ繝�繧ｴ繝ｪ
			entry.setVulnName(PortalConstants.HEADER_VULN_NAME); 							//閼�蠑ｱ諤ｧ蜷�
			entry.setDetectionUrl(PortalConstants.HEADER_DETECTION_URL);					//讀懷�ｺURL
			entry.setSignatureID(PortalConstants.HEADER_SIGNATURE_ID); 						//繧ｷ繧ｰ繝阪メ繝｣ID
			entry.setDetectionParameters(PortalConstants.HEADER_DETECTION_PARAM); 			//讀懷�ｺ繝代Λ繝｡繝ｼ繧ｿ
			entry.setOriginalValue(PortalConstants.HEADER_ORIGINAL_VALUE);					//蜈�蛟､
			entry.setChangeValue(PortalConstants.HEADER_CHANGE_VALUE); 						//螟画峩蛟､
			entry.setDetectionTrigger(PortalConstants.HEADER_DETECTION_TRIGGER); 			//讀懷�ｺ繝医Μ繧ｬ	
			entry.setConditionStatus(PortalConstants.HEADER_CORRESPONDANCE_SITUATION); 		//蟇ｾ蠢懈婿豕�
			entry.setExpectedDate(PortalConstants.HEADER_PLANNED_RESPONSE_DATE); 			//蟇ｾ蠢應ｺ亥ｮ壽律
			entry.setReportId(PortalConstants.HEADER_REPORT_ID);							//Report ID
			
			summaryCsvEntryList.add(entry);
			
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				for (Object object : reportList) {
					ReportItem report = (ReportItem) object;
					String reportName = PortalConstants.STRING_EMPTY;
					String[] reportNameArr = report.getReportName().replaceAll(Pattern.quote(PortalConstants.STRING_BACK_SLASH), PortalConstants.STR_FWD_SLASH).split(PortalConstants.STR_FWD_SLASH);
					reportName = (reportNameArr.length > 0) ? reportNameArr[1] : PortalConstants.STRING_EMPTY;
					File xmlReport = new File(folderLocation + File.separator + reportName);
					
					summaryCsvDataList = VexScanReport.getVexVulnSummaryData(xmlReport, startDate, endDate);
					
					if (!CommonUtil.isListNullOrEmpty(summaryCsvDataList)) {
						for (VexVulnSummaryData data :summaryCsvDataList) {
							if (data != null) {
								String conditionStatus = data.getConditionStatus();
								conditionStatus = conditionStatus.replace("\n", ". ");
								
								VexVulnSummaryEntry vulnDataEntry = new VexVulnSummaryEntry();
								vulnDataEntry.setNumber(String.valueOf(num++));
								vulnDataEntry.setRiskDegree(data.getRiskDegree());
								vulnDataEntry.setVulnCategory(data.getVulnCategory());
								vulnDataEntry.setVulnName(data.getVulnName());
								vulnDataEntry.setDetectionUrl(data.getDetectionUrl());
								vulnDataEntry.setSignatureID(data.getSignatureID());
								vulnDataEntry.setDetectionParameters(data.getDetectionParameters());
								vulnDataEntry.setOriginalValue(data.getOriginalValue());
								vulnDataEntry.setChangeValue(data.getChangeValue());
								vulnDataEntry.setDetectionTrigger(data.getDetectionTrigger());
								vulnDataEntry.setConditionStatus(conditionStatus);
								vulnDataEntry.setExpectedDate(PortalConstants.STRING_EMPTY);
								vulnDataEntry.setReportId(String.valueOf(report.getReportId()));
								
								summaryCsvEntryList.add(vulnDataEntry);
							}
						}
					}
				}
			}
			
			if (!CommonUtil.canWrite(vexVulnSummaryCsvFile.getParent())) {
				params.put("vexVulnSummaryCsvFile", vexVulnSummaryCsvFile);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(vexVulnSummaryCsvFile),PortalConstants.SHIFT_JIS));
			writer = new CSVWriterBuilder<VexVulnSummaryEntry>(fileWriter).strategy(strategy).entryConverter(new VexVulnSummaryEntryParser()).build();			
			writer.writeAll(summaryCsvEntryList);
		} catch (UBSPortalException e) {
			params.put("reportList", reportList);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e); //METHOD_GENERATE_VEX_VULN_SUMMARY		= "generateVexVulnSummary";
			throw new UBSPortalException(e.getErrorCode(), PortalErrors.CX_PROCESSING_EXCEPTION, e); //VEX_PROCESSING_EXCEPTION 			= "S.Vex.ProcessException.0001";
		} catch (IOException e) {
			params.put("fileWriter", fileWriter);
			params.put("writer", writer);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e); //METHOD_GENERATE_VEX_VULN_SUMMARY		= "generateVexVulnSummary";
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e); //VEX_PROCESSING_EXCEPTION 			= "S.Vex.ProcessException.0001";
		} catch (Exception e) {
			params.put("reportList", reportList);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e); //METHOD_GENERATE_VEX_VULN_SUMMARY		= "generateVexVulnSummary";
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e); //VEX_PROCESSING_EXCEPTION 			= "S.Vex.ProcessException.0001";
		} finally {
			try {
				if (!CommonUtil.isObjectNull(fileWriter)) {
					fileWriter.close();
				}
				
				if (!CommonUtil.isObjectNull(writer)) {
					writer.close();
				}
			} catch (IOException e) {
				params.put("fileWriter", fileWriter);
				params.put("writer", writer);
				log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e); //METHOD_GENERATE_VEX_VULN_SUMMARY		= "generateVexVulnSummary";
				throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e); //VEX_PROCESSING_EXCEPTION 			= "S.Vex.ProcessException.0001";
			} catch (Exception e) {
				log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e); //METHOD_GENERATE_VEX_VULN_SUMMARY		= "generateVexVulnSummary";
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e); //VEX_PROCESSING_EXCEPTION 			= "S.Vex.ProcessException.0001";
			}
			
			summaryCsvEntryList = null;
			summaryCsvDataList = null;
			params.clear();
			params = null;
		}
	}

	public static void generateVexMetaSummary(List<Object> reportList, String folderLocation, Date startDate, Date endDate) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File vexMetaSummaryCsvFile = null;
		CSVStrategy strategy = null;
		List<VexMetaSummaryEntry> summaryCsvEntryList = new ArrayList<VexMetaSummaryEntry>();
		VexMetaSummaryData data = null;
		Writer fileWriter = null;
		CSVWriter<VexMetaSummaryEntry> writer = null;
		
		try {
			vexMetaSummaryCsvFile = new File(folderLocation
				+ File.separator
				+ PortalConstants.METACSV);
				
			new File(vexMetaSummaryCsvFile.getParent()).mkdirs();
			
			strategy = new CSVStrategy(',', '"', '\0', false, false);
			
			// Setting Headers
			VexMetaSummaryEntry entry = new VexMetaSummaryEntry();
			
			entry.setCompanyName(PortalConstants.HEADER_SUMMARY_COMPANY_NAME);				//莨夂､ｾ蜷�
			entry.setProjectName(PortalConstants.HEADER_PROJECT_NAME);						//繝励Ο繧ｸ繧ｧ繧ｯ繝亥錐	
			entry.setReportID(PortalConstants.HEADER_REPORT_ID); 							//Report ID
			entry.setImplEnvironment(PortalConstants.HEADER_IMPLEMENTATION_ENV);			//螳滓命迺ｰ蠅�
			entry.setProposalNum(PortalConstants.HEADER_PROPOSAL_NUMBER);					//譯井ｻｶ逡ｪ蜿ｷ
			entry.setScanID(PortalConstants.HEADER_SCAN_ID); 							    //繧ｹ繧ｭ繝｣繝ｳID	
			entry.setDiagnosisStartDate(PortalConstants.HEADER_DIAGNOSIS_START_DATE); 		//險ｺ譁ｭ髢句ｧ区律
			entry.setDiagnosisEndDate(PortalConstants.HEADER_DIAGNOSIS_END_DATE); 			//險ｺ譁ｭ邨ゆｺ�譌･
			entry.setHighRiskCount(PortalConstants.HEADER_HIGH_RISK_NUM); 					//蜊ｱ髯ｺ蠎ｦ鬮倥�ｮ謨ｰ	
			entry.setMediumRiskCount(PortalConstants.HEADER_MEDIUM_RISK_NUM); 				//蜊ｱ髯ｺ蠎ｦ荳ｭ縺ｮ謨ｰ	
			entry.setLowRiskCount(PortalConstants.HEADER_LOW_RISK_NUM); 					//蜊ｱ髯ｺ蠎ｦ菴弱�ｮ謨ｰ	
			entry.setInfoRiskCount(PortalConstants.HEADER_RISK_INFO_COUNT); 				//蜊ｱ髯ｺ蠎ｦ諠�蝣ｱ縺ｮ謨ｰ
			entry.setWebSignatureName(PortalConstants.HEADER_WEB_SIGNATURE_NAME); 			//菴ｿ逕ｨ縺励◆繧ｷ繧ｰ繝阪メ繝｣繧ｻ繝�繝亥錐(Web)
			entry.setServerSettingsSignatureName(PortalConstants.HEADER_SERVER_SETTING_NAME); //菴ｿ逕ｨ縺励◆繧ｷ繧ｰ繝阪メ繝｣繧ｻ繝�繝亥錐(SeverSettings)
			entry.setServerFilesSignatureName(PortalConstants.HEADER_SERVER_FILE_NAME); 	//菴ｿ逕ｨ縺励◆繧ｷ繧ｰ繝阪メ繝｣繧ｻ繝�繝亥錐(SeverFiles)	
			entry.setVexVersion(PortalConstants.HEADER_VEX_VERSION); 						//Vex縺ｮ繝舌�ｼ繧ｸ繝ｧ繝ｳ
			
			summaryCsvEntryList.add(entry);
			
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				for (Object object : reportList) {
					ReportItem report 				= (ReportItem) object;
					long scanId 					= report.getScanId();
					long lOwnerGroup				= PortalConstants.LONG_ZERO;
					Date startTime 					= null;
					Date endTime 					= null;
					VexScanSetting vexScanSetting 	= VexScanSettingLocalServiceUtil.getVexScanSetting(scanId);
					Scan scanItem					= ControllerHelper.getScan(scanId);
					Project projectItem				= ProjectLocalServiceUtil.getProject(scanItem.getProjectId());
					Organization organizationObj	= null;
					
					if(CommonUtil.isObjectNull(vexScanSetting) == false){
						startTime 					= vexScanSetting.getStarttime();
						endTime 					= vexScanSetting.getEndtime();
					}
					if(CommonUtil.isObjectNull(projectItem) == false){
						lOwnerGroup = projectItem.getOwnerGroup(); 
						organizationObj = OrganizationLocalServiceUtil.getOrganization(lOwnerGroup);
					}
					
					
					String reportName = PortalConstants.STRING_EMPTY;
					String[] reportNameArr = report.getReportName().replaceAll(Pattern.quote(PortalConstants.STRING_BACK_SLASH), PortalConstants.STR_FWD_SLASH).split(PortalConstants.STR_FWD_SLASH);
					reportName = (reportNameArr.length > 0) ? reportNameArr[1] : PortalConstants.STRING_EMPTY;
					File xmlReport = new File(folderLocation + File.separator + reportName);
					
					data = VexScanReport.getVexMetaSummaryData(xmlReport);
				
					VexMetaSummaryEntry metaDataEntry = new VexMetaSummaryEntry();

					metaDataEntry.setCompanyName(organizationObj.getName());
					metaDataEntry.setProjectName(data.getProjectName());
					metaDataEntry.setImplEnvironment(data.getImplEnvironment());
					metaDataEntry.setProposalNum(projectItem.getProjectName());
					metaDataEntry.setScanID(String.valueOf(scanId));
					metaDataEntry.setHighRiskCount(data.getHighRiskCount());
					metaDataEntry.setMediumRiskCount(data.getMediumRiskCount());
					metaDataEntry.setLowRiskCount(data.getLowRiskCount());
					metaDataEntry.setInfoRiskCount(data.getInfoRiskCount());
					
					if((CommonUtil.isObjectNull(startTime) == false) && (CommonUtil.isObjectNull(endTime)==false)){
						SimpleDateFormat dateformat = new SimpleDateFormat("yyyy/MM/dd");
						metaDataEntry.setDiagnosisStartDate(dateformat.format(startTime));
						metaDataEntry.setDiagnosisEndDate(dateformat.format(endTime));
					}
					
					metaDataEntry.setWebSignatureName(vexScanSetting.getSelectedwebsignature());
					metaDataEntry.setServerSettingsSignatureName(vexScanSetting.getSelectedserversettingssignature());
					metaDataEntry.setServerFilesSignatureName(vexScanSetting.getSelectedserverfilessignature());
					
					String cliVersion = Message.get("Command.cliVersion.number");
										
					metaDataEntry.setVexVersion(cliVersion);

					summaryCsvEntryList.add(metaDataEntry);
				}
			}
			
			if (!CommonUtil.canWrite(vexMetaSummaryCsvFile.getParent())) {
				params.put("vexMetaSummaryCsvFile", vexMetaSummaryCsvFile);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(vexMetaSummaryCsvFile),PortalConstants.SHIFT_JIS));
			writer = new CSVWriterBuilder<VexMetaSummaryEntry>(fileWriter).strategy(strategy).entryConverter(new VexMetaSummaryEntryParser()).build();			
			writer.writeAll(summaryCsvEntryList);
		} catch (UBSPortalException e) {
			params.put("reportList", reportList);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw e;
		} catch (IOException e) {
			params.put("fileWriter", fileWriter);
			params.put("writer", writer);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (Exception e) {
			params.put("reportList", reportList);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			try {
				if (!CommonUtil.isObjectNull(fileWriter)) {
					fileWriter.close();
				}
				
				if (!CommonUtil.isObjectNull(writer)) {
					writer.close();
				}
			} catch (IOException e) {
				params.put("fileWriter", fileWriter);
				params.put("writer", writer);
				log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			} catch (Exception e) {
				log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			}
			
			params.clear();
			params = null;
		}
	}
	
	public static void downloadXMLFromStorage (List<Object> reportList, String folderLocation) throws UBSPortalException {
		Map<String, Object> params 			= new HashMap<String, Object>();
		String strBucketName 				= PortalConstants.STRING_EMPTY;
		String strFileName 					= PortalConstants.STRING_EMPTY;
		String strFile 						= PortalConstants.STRING_EMPTY;
		String strDownloadLocation 			= PortalConstants.STRING_EMPTY;
		String strReportFileCompleteName 	= PortalConstants.STRING_EMPTY;
		String [] strNames 					= null;
		File reportFilePath 				= null;
		
		try {
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				for (Object object : reportList) {
					ReportItem report 	= (ReportItem) object;
					
					strBucketName 		= report.getBucketName(); 
					strFileName 		= report.getReportName();
					strNames 			= StringUtil.split(strFileName, File.separator);
					
					if(strNames.length > 0) {
						strFile = strNames[1];
						strDownloadLocation = strNames[0] + PortalConstants.STR_FWD_SLASH + strFile;
						
						reportFilePath = new File(folderLocation);
						reportFilePath.mkdirs();
						
						if (reportFilePath.isDirectory()) {
							strReportFileCompleteName = reportFilePath.getAbsolutePath() + File.separator + strFile;
							ycloudUtil.downloadObject(strBucketName, strDownloadLocation, strReportFileCompleteName);
						}
					}
				}
			}
		} catch (UBSPortalException e) {
			params.put("strBucketName", strBucketName);
			params.put("strDownloadLocation", strDownloadLocation);
			params.put("strReportFileCompleteName", strReportFileCompleteName);
			log.debug(e.getErrorCode(), "downloadXMLFromStorage", params, e);
			throw new UBSPortalException(e.getErrorCode(), PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}
	}
}