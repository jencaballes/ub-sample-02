/**
 * CxSDKWebServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.CxScanManager;

public class CxSDKWebServiceLocator extends org.apache.axis.client.Service implements jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebService {

    public CxSDKWebServiceLocator() {
    }


    public CxSDKWebServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CxSDKWebServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CxSDKWebServiceSoap
    // private java.lang.String CxSDKWebServiceSoap_address = "http://211.121.203.117/cxwebinterface/SDK/CxSDKWebService.asmx";
	private java.lang.String CxSDKWebServiceSoap_address = CxScanManager.getWebServiceUrl();

    public java.lang.String getCxSDKWebServiceSoapAddress() {
        return CxSDKWebServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CxSDKWebServiceSoapWSDDServiceName = "CxSDKWebServiceSoap";

    public java.lang.String getCxSDKWebServiceSoapWSDDServiceName() {
        return CxSDKWebServiceSoapWSDDServiceName;
    }

    public void setCxSDKWebServiceSoapWSDDServiceName(java.lang.String name) {
        CxSDKWebServiceSoapWSDDServiceName = name;
    }

    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoap getCxSDKWebServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CxSDKWebServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCxSDKWebServiceSoap(endpoint);
    }

    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoap getCxSDKWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoapStub _stub = new jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoapStub(portAddress, this);
            _stub.setPortName(getCxSDKWebServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCxSDKWebServiceSoapEndpointAddress(java.lang.String address) {
        CxSDKWebServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoapStub _stub = new jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoapStub(new java.net.URL(CxSDKWebServiceSoap_address), this);
                _stub.setPortName(getCxSDKWebServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CxSDKWebServiceSoap".equals(inputPortName)) {
            return getCxSDKWebServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxSDKWebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxSDKWebServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CxSDKWebServiceSoap".equals(portName)) {
            setCxSDKWebServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
