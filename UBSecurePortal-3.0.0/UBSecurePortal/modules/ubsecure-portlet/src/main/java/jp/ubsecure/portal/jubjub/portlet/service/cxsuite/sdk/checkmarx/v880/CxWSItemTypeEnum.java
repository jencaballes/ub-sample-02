/**
 * CxWSItemTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class CxWSItemTypeEnum implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CxWSItemTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Project = "Project";
    public static final java.lang.String _Task = "Task";
    public static final java.lang.String _Scan = "Scan";
    public static final java.lang.String _Preset = "Preset";
    public static final java.lang.String _Configuration = "Configuration";
    public static final java.lang.String _Users = "Users";
    public static final java.lang.String _Roles = "Roles";
    public static final java.lang.String _Other = "Other";
    public static final java.lang.String _SystemSettings = "SystemSettings";
    public static final java.lang.String _Ignore_Path = "Ignore_Path";
    public static final java.lang.String _ResultComment = "ResultComment";
    public static final java.lang.String _ResultSeverity = "ResultSeverity";
    public static final java.lang.String _ResultStatus = "ResultStatus";
    public static final java.lang.String _AuditUser = "AuditUser";
    public static final CxWSItemTypeEnum Project = new CxWSItemTypeEnum(_Project);
    public static final CxWSItemTypeEnum Task = new CxWSItemTypeEnum(_Task);
    public static final CxWSItemTypeEnum Scan = new CxWSItemTypeEnum(_Scan);
    public static final CxWSItemTypeEnum Preset = new CxWSItemTypeEnum(_Preset);
    public static final CxWSItemTypeEnum Configuration = new CxWSItemTypeEnum(_Configuration);
    public static final CxWSItemTypeEnum Users = new CxWSItemTypeEnum(_Users);
    public static final CxWSItemTypeEnum Roles = new CxWSItemTypeEnum(_Roles);
    public static final CxWSItemTypeEnum Other = new CxWSItemTypeEnum(_Other);
    public static final CxWSItemTypeEnum SystemSettings = new CxWSItemTypeEnum(_SystemSettings);
    public static final CxWSItemTypeEnum Ignore_Path = new CxWSItemTypeEnum(_Ignore_Path);
    public static final CxWSItemTypeEnum ResultComment = new CxWSItemTypeEnum(_ResultComment);
    public static final CxWSItemTypeEnum ResultSeverity = new CxWSItemTypeEnum(_ResultSeverity);
    public static final CxWSItemTypeEnum ResultStatus = new CxWSItemTypeEnum(_ResultStatus);
    public static final CxWSItemTypeEnum AuditUser = new CxWSItemTypeEnum(_AuditUser);
    public java.lang.String getValue() { return _value_;}
    public static CxWSItemTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CxWSItemTypeEnum enumeration = (CxWSItemTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CxWSItemTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CxWSItemTypeEnum.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSItemTypeEnum"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
