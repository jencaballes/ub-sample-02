package jp.ubsecure.portal.jubjub.portlet.service.cxsuite;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.DetectionResultSummaryData;

/**
 * Handler for the Detection Result summary report
 * for all scans in Vex.
 * 
 * XML report is parsed using SAX parser to generate the Detection Result summary
 * report of a completed scan.
 * 
 * @author ASI-1199
 *
 */
public class DetectionResultSummaryHandler extends DefaultHandler {
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(DetectionResultSummaryHandler.class);
	
	private String number;
	private String detectionresultid;
	private String riskLevel;
	private String category;
	private String overview;
	private String functionname;
	private String url;
	private String parametername;
	private DetectionResultSummaryData detectionResult;
	List<DetectionResultSummaryData> detectionResultList;
	private StringBuilder data = null;
	boolean bNumber= false;;
	boolean bdetectionresultid= false;;
	boolean briskLevel= false;;
	boolean bcategory= false;;
	boolean boverview= false;;
	boolean bfunctionname= false;;
	boolean burl= false;;
	boolean bparametername= false;
	boolean bIsScanResult = true;
	
	public DetectionResultSummaryHandler () {
		this.detectionResultList = new ArrayList<DetectionResultSummaryData>();
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_DEFINITION)){
			bIsScanResult = false;
		}
			
		if(bIsScanResult){
			if (qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_FUNCTION_NAME)) {
				if (detectionResultList == null){
					detectionResultList = new ArrayList<>();
				}
				bfunctionname = true;
			}
			else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_URL)){
				burl = true;
			}else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_CATEGORY)){
				bcategory = true;
			}else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_SIGNATURE_ID)){
				bdetectionresultid = true;
			}else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_RISK_LEVEL)){
				briskLevel = true;
			}else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_SUMMARY)){
				boverview = true;
			}else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_AUDIT_ID)){
				bNumber = true;
			}else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_TARGET_NAME)){
				bparametername = true;
			}
		}
		data = new StringBuilder();
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_DEFINITION)){
			bIsScanResult = true;
		}
		if(bIsScanResult){
			if (qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_FUNCTION_NAME) && bfunctionname) {
				detectionResult = new DetectionResultSummaryData();
				detectionResult.setFunctionname(data.toString());
				functionname = data.toString();
			}
			else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_URL) && burl){
				detectionResult.setUrl(data.toString());
				url = data.toString();
				burl = false;
			}
			else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_CATEGORY) && bcategory){
				detectionResult.setCategory(data.toString());
				category = data.toString();
				bcategory = false;
			}
			else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_SIGNATURE_ID) && bdetectionresultid){
				detectionResult.setDetectionresultid(data.toString());
				detectionresultid = data.toString();
				bdetectionresultid = false;
			}
			else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_RISK_LEVEL) && briskLevel){
				detectionResult.setRisklevel(data.toString());
				riskLevel = data.toString();
				briskLevel = false;
			}
			else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_SUMMARY) && boverview){
				detectionResult.setOverview(data.toString());
				overview = data.toString();
				boverview = false;
			}
			else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_AUDIT_ID) && bNumber){
				detectionResult.setScanresultid(data.toString());
				bNumber = false;
			}
			else if(qName.equalsIgnoreCase(PortalConstants.VEX_TAG_ATTRIB_TARGET_NAME) && bparametername){
				detectionResult.setParametername(data.toString());
				bparametername = false;
			}
			
			if (qName.equals(PortalConstants.VEX_TAG_ATTRIB_TARGET)) {
				if(detectionResult.getFunctionname() == null){
					detectionResult.setFunctionname(functionname);
				}
				if(detectionResult.getUrl() == null){
					detectionResult.setUrl(url);
				}
				
				if(detectionResult.getDetectionresultid() == null){
					detectionResult.setDetectionresultid(detectionresultid);
				}
				
				if(detectionResult.getRisklevel() == null){
					detectionResult.setRisklevel(riskLevel);
				}
				
				if(detectionResult.getCategory() == null){
					detectionResult.setCategory(category);
				}
				
				if(detectionResult.getOverview() == null){
					detectionResult.setOverview(overview);
				}
				
				detectionResultList.add(detectionResult);
				detectionResult = new DetectionResultSummaryData();
			}
			
			if(qName.equals("target_list")){
				detectionresultid = null;
				riskLevel = null;
				category = null;
				overview = null;
			}
		}
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		data.append(new String(ch, start, length));
	}
}
