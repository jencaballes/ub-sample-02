/**
 * CxSDKWebServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public interface CxSDKWebServiceSoap extends java.rmi.Remote {
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID scan(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CliScanArgs args) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID branchProjectById(java.lang.String sessionId, long originProjectId, java.lang.String newBranchProjectName) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID scanWithOriginName(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CliScanArgs args, java.lang.String origName) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse cancelScan(java.lang.String sessionID, java.lang.String runId) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse deleteProjects(java.lang.String sessionID, long[] projectIDs) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse deleteScans(java.lang.String sessionID, long[] scanIDs) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID scanWithScheduling(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CliScanArgs args, java.lang.String schedulingData) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID scanWithSchedulingWithCron(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CliScanArgs args, java.lang.String cronString, long utcEpochStartTime, long utcEpochEndTime) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse updateScanComment(java.lang.String sessionID, long scanID, java.lang.String comment) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponsePresetList getPresetList(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseConfigSetList getConfigurationSetList(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectsDisplayData getProjectsDisplayData(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectScannedDisplayData getProjectScannedDisplayData(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectConfig getProjectConfiguration(java.lang.String sessionID, long projectID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse updateProjectIncrementalConfiguration(java.lang.String sessionID, long projectID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectConfiguration projectConfiguration) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseLoginData login(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials applicationCredentials, int lcid) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseLoginData ssoLogin(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials encryptedCredentials, int lcid) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseLoginData loginWithToken(java.lang.String token, int lcid) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse logout(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseGroupList getAssociatedGroupsList(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseScanStatus getStatusOfSingleScan(java.lang.String sessionID, java.lang.String runId) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseScansDisplayData getScansDisplayDataForAllProjects(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseScanSummary getScanSummary(java.lang.String sessionID, long scanID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse updateProjectConfiguration(java.lang.String sessionID, long projectID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectConfiguration projectConfiguration) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse deleteUser(java.lang.String sessionID, int userID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseUserData getAllUsers(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSCreateReportResponse createScanReport(java.lang.String sessionID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSReportRequest reportRequest) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSReportStatusResponse getScanReportStatus(java.lang.String sessionID, long reportID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseScanResults getScanReport(java.lang.String sessionID, long reportID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse executeDataRetention(java.lang.String sessionID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxDataRetentionConfiguration dataRetentionConfiguration) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse stopDataRetention(java.lang.String sessionID) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseTeamLdapGroupMappingData getTeamLdapGroupsMapping(java.lang.String sessionId, java.lang.String teamId) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse setTeamLdapGroupsMapping(java.lang.String sessionId, java.lang.String teamId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSLdapGroupMapping[] ldapGroups) throws java.rmi.RemoteException;
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse isValidProjectName(java.lang.String sessionID, java.lang.String projectName, java.lang.String groupId) throws java.rmi.RemoteException;
}
