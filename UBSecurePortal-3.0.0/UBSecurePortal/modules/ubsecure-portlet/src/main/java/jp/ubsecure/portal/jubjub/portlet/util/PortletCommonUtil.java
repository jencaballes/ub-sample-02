package jp.ubsecure.portal.jubjub.portlet.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import com.liferay.portal.kernel.dao.jdbc.DataAccess;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;

public class PortletCommonUtil {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(CommonUtil.class);

	public static String convertHexToString (String hexValue) {
		Map<String, Object> params = new HashMap<String, Object>();
		String result = PortalConstants.STRING_EMPTY;
		Hex hexString = new Hex();
		int i = 0;
		String temp = PortalConstants.STRING_EMPTY;
		String converted = PortalConstants.STRING_EMPTY;
		
		try {
			while (i < hexValue.length()) {
				if (hexValue.charAt(i) == PortalConstants.CHAR_PERCENT) {
					converted += hexValue.substring(i+1, (i+3));
					i += 3;
				} else {
					temp = hexValue.charAt(i) + PortalConstants.STRING_EMPTY;
					converted += new String(hexString.encode(temp.getBytes()));
					i++;
				}
			}
			
			result = new String(hexString.decode(converted.getBytes()));
		} catch (DecoderException e) {
			params.put("hexValue", hexValue);
			log.debug(PortalErrors.DECODER_EXCEPTION, PortalConstants.METHOD_CONVERT_HEX_TO_STRING, params, e);
		}
		
		return result;
	}
	
	public static boolean isDBConnected () throws UBSPortalException {
		boolean bConnected = false;
		Connection conn = null;
		
		try {
			conn = DataAccess.getConnection();
			
			if (conn == null) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			Statement statement = conn.createStatement();
			statement.executeQuery("SELECT * FROM user_ LIMIT 1");
			
			bConnected = true;
			conn.close();
		} catch (SQLException e) {
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_CHECK_DB_CONNECTION, null, e);
			throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION, e);
		} catch (UBSPortalException e) {
			log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_CHECK_DB_CONNECTION, null, e);
			throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION, e);
		}
		
		return bConnected;
	}
	
	public static long getTimeDifference(Calendar calStart, Calendar calEnd, ChronoUnit timeUnit){
		long difference = 0;
		if(null == calStart || null == calEnd){
			return difference;
		}
		try{
	        DateFormat datetimeFormatter= new SimpleDateFormat("yyyy/MM/dd HH:mm");
	        Date dateStart = calStart.getTime();    
	        Date dateEnd = calEnd.getTime(); 
	        String dtestartTime = datetimeFormatter.format(dateStart);
	        String dteEndTime = datetimeFormatter.format(dateEnd);
	        
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
	        LocalDateTime startTime = LocalDateTime.parse(dtestartTime, formatter);
	        LocalDateTime endTime = LocalDateTime.parse(dteEndTime, formatter);
	  
	        difference = startTime.until(endTime, timeUnit); 	
		}catch(Exception e){
			//do nothing
		}
		return difference;
	}

}
