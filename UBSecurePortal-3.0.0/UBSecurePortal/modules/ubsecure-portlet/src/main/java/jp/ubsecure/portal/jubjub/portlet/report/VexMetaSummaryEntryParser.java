package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.IOSMetaSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.model.VexMetaSummaryEntry;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class VexMetaSummaryEntryParser implements CSVEntryConverter<VexMetaSummaryEntry> {

private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexMetaSummaryEntryParser.class);
	
	@Override
	public String[] convertEntry(VexMetaSummaryEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (entry == null) {
			params.put("entry", entry);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException(PortalErrors.FILE_PROCESS_ERROR);					
		}
		
		String[] columns = new String[15];
		
		columns[0] = entry.getCompanyName();
	    columns[1] = entry.getProjectName();
	    columns[2] = entry.getImplEnvironment();
	    columns[3] = entry.getProposalNum();
	    columns[4] = entry.getScanID();
	    columns[5] = entry.getDiagnosisStartDate();
	    columns[6] = entry.getDiagnosisEndDate();
	    columns[7] = entry.getHighRiskCount();
	    columns[8] = entry.getMediumRiskCount();
	    columns[9] = entry.getLowRiskCount();
	    columns[10] = entry.getInfoRiskCount();
	    columns[11] = entry.getWebSignatureName();
	    columns[12] = entry.getServerSettingsSignatureName();
	    columns[13] = entry.getServerFilesSignatureName();
	    columns[14] = entry.getVexVersion();
	    
	    for (int index = 0; index < columns.length; index++) {
	    	if (columns[index] == null) {
	    		columns[index] = PortalConstants.STRING_EMPTY;
	    	}
	    }
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}
}
