package jp.ubsecure.portal.jubjub.portlet.form.navigator.my.account;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorEntry;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.users.admin.web.servlet.taglib.ui.UserSmsFormNavigatorEntry;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property = {
		PortalConstants.KEY_FORM_NAVIGATOR_ENTRY_ORDER + PortalConstants.ENTRY_ORDER_USER_SMS
	},
	service = FormNavigatorEntry.class
)
public class UserSmsFormNavigatorEntryExt extends UserSmsFormNavigatorEntry {
	@Override
	public String getFormNavigatorId() {
		return PortalConstants.MY_ACCOUNT_PREFIX + super.getFormNavigatorId();
	}

	@Override
	public boolean isVisible(User user, User selUser) {
		boolean visible = GetterUtil.getBoolean(PropsUtil.get(PortalConstants.MY_ACCOUNT_SMS_VISIBLE), true);
		return visible && super.isVisible(user, selUser);
	}
}