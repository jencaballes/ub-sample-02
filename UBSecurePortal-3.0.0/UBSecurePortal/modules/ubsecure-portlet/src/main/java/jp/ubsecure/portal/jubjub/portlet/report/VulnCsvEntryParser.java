package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.VulnCsvEntry;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class VulnCsvEntryParser implements CSVEntryConverter<VulnCsvEntry> {
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VulnCsvEntryParser.class);
	
	@Override
	public String[] convertEntry(VulnCsvEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (CommonUtil.isObjectNull(entry)) {
			params.put("entry", entry);
			log.debug(PortalErrors.VUlN_MASTER_VALUE_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException();					
		}
		
		String[] columns = new String[12];
	    columns[0] = entry.getNumber();
	    columns[1] = entry.getRiskLevel();
	    columns[2] = entry.getVulnerabilityName();
	    columns[3] = entry.getSignatureId();
	    columns[4] = entry.getCategory();
	    columns[5] = entry.getFileName();
	    columns[6] = entry.getTrigger1();
	    columns[7] = entry.getTrigger2();
	    columns[8] = entry.getTrigger3();
	    columns[9] = entry.getReportId();
	    columns[10] = entry.getExclusionFlag();
	    columns[11] = entry.getComment();
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}
}
