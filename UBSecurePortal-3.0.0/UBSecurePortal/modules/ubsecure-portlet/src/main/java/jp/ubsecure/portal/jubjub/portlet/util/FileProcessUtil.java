package jp.ubsecure.portal.jubjub.portlet.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import zip.ZipUtils;

public class FileProcessUtil {
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(FileProcessUtil.class);
	private static YCloudUtil ycloudUtil = YCloudUtil.getInstance();
	
	/***
	 * Parse apk file for android
	 * @param apkFile
	 * @return tApkFile
	 * @throws UBSPortalException
	 * @throws IOException
	 */
	public static byte[] parseAndroidApk(File apkFile) throws UBSPortalException {
		byte[] tApkFile = null;
		byte[] buffer = new byte[1024];
		FileInputStream inputStream = null;
		ByteArrayOutputStream outputStream = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (!CommonUtil.isValidFile(apkFile)) {
				params.put("apkFile valid", CommonUtil.isValidFile(apkFile));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);
			}
			
			inputStream = new FileInputStream(apkFile);
			outputStream = new ByteArrayOutputStream();
			
			int len = 0;
			while ((len = inputStream.read(buffer)) > 0){
				outputStream.write(buffer, 0, len);
			}
			
			tApkFile = outputStream.toByteArray();
			outputStream.close();
			inputStream.close();
			
		} catch(UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_PARSE_ANDROID_APK, params, e);
			throw e;
		} catch(FileNotFoundException e) {
			params.put("apkFile valid", CommonUtil.isValidFile(apkFile));
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_FILE));
			
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_PARSE_ANDROID_APK, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
		} catch(IOException e) {
			params.put("apkFile valid", CommonUtil.isValidFile(apkFile));
			
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_PARSE_ANDROID_APK, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
		} catch(Exception e) {
			params.put("apkFile valid", CommonUtil.isValidFile(apkFile));
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_PARSE_ANDROID_APK, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		} finally {
			inputStream = null;	
			outputStream = null;	
			buffer = null;
			params.clear();
			params = null;
		}
		
		return tApkFile;
	}
	
	/***
	 * Parse checklist file for Android
	 * @param checklistFile
	 * @return tChecklist
	 * @throws UBSPortalException
	 * @throws IOException
	 */
	public static byte[] parseAndroidChecklist(File checklistFile) throws UBSPortalException {
		byte[] tChecklist = null;
		byte[] buffer = new byte[1024];
		FileInputStream inputStream = null;
		ByteArrayOutputStream outputStream = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (!CommonUtil.isValidFile(checklistFile)){
				params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);
			}
			
			inputStream = new FileInputStream(checklistFile);
			outputStream = new ByteArrayOutputStream();
			
			int len = 0;
			while ((len = inputStream.read(buffer)) > 0){
				outputStream.write(buffer, 0, len);
				
			}
			
			tChecklist = outputStream.toByteArray();
			outputStream.close();
			inputStream.close();
			
		} catch(UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_PARSE_ANDROID_CHECKLIST, params, e);
			throw e;
		} catch(FileNotFoundException e) {
			params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_FILE));
			
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_PARSE_ANDROID_CHECKLIST, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
		} catch(IOException e) {
			params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
			
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_PARSE_ANDROID_CHECKLIST, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
		} catch(Exception e) {
			params.put("checklistFile valid", CommonUtil.isValidFile(checklistFile));
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_PARSE_ANDROID_CHECKLIST, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		} finally {
			inputStream = null;	
			outputStream = null;	
			params = null;
			buffer = null;
		}
	
		return tChecklist;
	}
	
	
	/**
	 * Get the size of the file
	 * @param file
	 * @return dBytes / PortalConstants.MEGABYTE_EQUIVALENT
	 * @throws UBSPortalException
	 */
	public static double getFileSize(File file) throws UBSPortalException {
		double dBytes = PortalConstants.DOUBLE_ZERO;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (!CommonUtil.isValidFile(file)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);
			}
			
			dBytes = file.length();
			
		} catch(UBSPortalException e) {
			params.put("file", file);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_FILE_SIZE, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
	
		return dBytes / PortalConstants.MEGABYTE_EQUIVALENT;
	}		

////	public static void writeFile(byte[] byteContents, File reportFile) throws UBSPortalException {
////		FileOutputStream fileOutputStream;
////		Map<String, Object> params = new HashMap<String, Object>();
////		
////		try {
////			
////			if (!CommonUtil.isValidBytes(byteContents)){
////				params.put("byteContents valid", CommonUtil.isValidBytes(byteContents));
////				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
////				throw new UBSPortalException(PortalErrors.INVALID_FILE);
////			}
////			
////			if (reportFile == null){
////				params.put("reportFile valid", reportFile);
////				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
////				throw new UBSPortalException(PortalErrors.INVALID_FILE);
////			}
////			
////			fileOutputStream = new FileOutputStream(reportFile);									
////			fileOutputStream.write(byteContents, 0, byteContents.length);		
////			fileOutputStream.close();
////			
////		} catch (FileNotFoundException e) {
////			params.put("reportFile", reportFile);
////			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_FILE));
////			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_WRITE_FILE, params, e);
////			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
////		} catch (IOException e) {
////			params.put("byteContents", byteContents);
////			params.put("reportFile", reportFile);
////			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_WRITE_FILE, params, e);
////			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
////		} catch (Exception e) {
////			params.put("byteContents", byteContents);
////			params.put("reportFile", reportFile);
////	        log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_WRITE_FILE, params, e);
////	        throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
////		} finally {
////			params.clear();
////			params = null;
////		}
////	}
////	
////	@SuppressWarnings("unchecked")
////	public static List<FileHeader> unzipAndroidReport(File zipFile, String destination) throws UBSPortalException {
////		List<FileHeader> headerList = null;
////		Map<String, Object> params = new HashMap<String, Object>();
////		
////	    try {
////	    	
////	    	if (!CommonUtil.isValidFile(zipFile)) {
////				params.put("zipFile valid", CommonUtil.isValidFile(zipFile));
////				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
////				throw new UBSPortalException(PortalErrors.INVALID_FILE);
////			}
////	    	if (CommonUtil.isStringNullOrEmpty(destination)) {
////	    		params.put("destination", destination);
////	    		params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
////				throw new UBSPortalException(PortalErrors.INVALID_FILE_PATH);
////			}
////	    	
////	    	new File(destination).mkdirs();
////	    	
////	        ZipFile zip = new ZipFile(zipFile);
////	        zip.setFileNameCharset("Shift-JIS");
////	        headerList = zip.getFileHeaders();
////
////	        zip.extractAll(destination);
////	        
////	    } catch (UBSPortalException e) {
////			log.debug(e.getErrorCode(), PortalConstants.METHOD_UNZIP_ANDROID_REPORT, params, e);
////			throw e;
////		} catch (ZipException e) {
////			params.put("zipFile valid", CommonUtil.isValidFile(zipFile));
////			params.put("destination", destination);
////			
////	        log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_UNZIP_ANDROID_REPORT, params, e);
////	        throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
////	    } catch (Exception e) {
////	    	params.put("zipFile valid", CommonUtil.isValidFile(zipFile));
////			params.put("destination", destination);
////	    	
////	        log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_UNZIP_ANDROID_REPORT, params, e);
////	        throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
////	    } finally {
////	    	params.clear();
////	    	params = null;
////	    }
////	    
////	    return headerList;
////	}
	
	public static String createUploadDirectory(long lProjectId, long lScanId) throws UBSPortalException {
		String reportDirectory = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			
			
			reportDirectory = getUploadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultUploadDirectory();
			}
			
			reportDirectory += File.separator
							+ PortalConstants.PROJECT_PREFIX
							+ String.valueOf(lProjectId)
							+ File.separator
							+ PortalConstants.SCAN_PREFIX
							+ String.valueOf(lScanId)
							+ File.separator;
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_UPLOAD_DIRECTORY, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return reportDirectory;
	}
	
	public static String getDefaultUploadDirectory(){
		String reportDirectory = PropsUtil.get(PropsKeys.LIFERAY_HOME)
				+ File.separator
				+ PortalConstants.DATA_FOLDER
				+ File.separator
				+ PortalConstants.UPLOAD_FOLDER;
		return reportDirectory;
	}
	
	public static String createTempUploadDirectory(long lProjectId) throws UBSPortalException {
		String tempDirectory = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			
			String reportDirectory = getUploadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultUploadDirectory();
			}
			
			tempDirectory = reportDirectory
				+ File.separator
				+ "temp"
				+ File.separator;
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_UPLOAD_DIRECTORY, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return tempDirectory;
	}
	
////	public static File createReportFile(long lProjectId, long lScanId, String strRegistrationDate, long lReportId, String strReportName) throws UBSPortalException {
////		File reportFile = null;
////		Map<String, Object> params = new HashMap<String, Object>();
////		
////		try {
////			
////			if (lProjectId <= PortalConstants.LONG_ZERO) {
////				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
////				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
////			}
////			if (lScanId <= PortalConstants.LONG_ZERO) {
////				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
////				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
////			}
////			if (CommonUtil.isStringNullOrEmpty(strRegistrationDate)) {
////				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
////				throw new UBSPortalException(PortalErrors.INVALID_SCAN_REGISTRATION_DATE);
////			}
////			if (lReportId <= PortalConstants.LONG_ZERO) {
////				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT_ID));
////				throw new UBSPortalException(PortalErrors.INVALID_REPORT_ID);
////			}
////			if (CommonUtil.isStringNullOrEmpty(strReportName)) {
////				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
////				throw new UBSPortalException(PortalErrors.INVALID_REPORT_NAME);
////			}
////			
////			reportFile = new File(PropsUtil.get(PropsKeys.LIFERAY_HOME)
////				+ File.separator
////				+ PortalConstants.DATA_FOLDER
////				+ File.separator
////				+ PortalConstants.UPLOAD_FOLDER
////				+ File.separator
////				+ PortalConstants.PROJECT_PREFIX
////				+ String.valueOf(lProjectId)
////				+ File.separator
////				+ PortalConstants.SCAN_PREFIX
////				+ String.valueOf(lScanId)
////				+ File.separator 
////    			+ strRegistrationDate
////    			+ PortalConstants.STR_DOT
////    			+ String.valueOf(lScanId)
////    			+ PortalConstants.STR_DOT
////    			+ String.valueOf(lReportId)
////    			+ PortalConstants.STR_DOT
////    			+ strReportName);
////			
////			new File(reportFile.getParent()).mkdirs();
////			
////		} catch (UBSPortalException e) {
////			params.put("lProjectId", lProjectId);
////			params.put("lScanId", lScanId);
////			params.put("strRegistrationDate", strRegistrationDate);
////			params.put("lReportId", lReportId);
////			params.put("strReportName", strReportName);
////			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_REPORT_FILE, params, e);
////			throw e;
////		} finally {
////			params.clear();
////			params = null;
////		}
////		
////		return reportFile;
////	}

	public static File createDownloadReportPath(long lProjectId, long lScanId) throws UBSPortalException {
		File reportFile = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String reportDirectory = PortalConstants.STRING_EMPTY;
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}			
			
			reportDirectory = getDownloadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultDownloadDirectory();
			}
			
			reportDirectory += File.separator
							+ PortalConstants.PROJECT_PREFIX
							+ String.valueOf(lProjectId)
							+ File.separator
							+ PortalConstants.SCAN_PREFIX
							+ String.valueOf(lScanId)
							+ File.separator;
			
			reportFile = new File(reportDirectory);
			reportFile.mkdirs();
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_DOWNLOAD_REPORT_PATH, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return reportFile;
	}
	
	public static String getDefaultDownloadDirectory(){
		String reportDirectory = PropsUtil.get(PropsKeys.LIFERAY_HOME)
								+ File.separator
								+ PortalConstants.DATA_FOLDER
								+ File.separator
								+ PortalConstants.DOWNLOAD_FOLDER;
		return reportDirectory;
	}
		
	public static File createDownloadReferencePath(long lProjectId, long lScanId) throws UBSPortalException {
		File reportFile = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}			
			
			String reportDirectory = getDownloadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultDownloadDirectory();
			}
			
			reportFile = new File(reportDirectory
				+ File.separator
				+ PortalConstants.PROJECT_PREFIX
				+ String.valueOf(lProjectId)
				+ File.separator
				+ PortalConstants.SCAN_PREFIX
				+ String.valueOf(lScanId)
				+ File.separator
				);
			reportFile.mkdirs();
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_DOWNLOAD_REFERENCE_PATH, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return reportFile;
	}
	
	public static File createCrawlSettingYAMLPath(long lProjectId, long lScanId) throws UBSPortalException {
		File ymlFile = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}			
			
			String reportDirectory = getUploadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultUploadDirectory();
			}
			
			ymlFile = new File(reportDirectory
				+ File.separator
				+ PortalConstants.PROJECT_PREFIX
				+ String.valueOf(lProjectId)
				+ File.separator
				+ PortalConstants.SCAN_PREFIX
				+ String.valueOf(lScanId)
				+ File.separator
				);
			ymlFile.mkdirs();
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_CRAWL_SETTING_YAML_PATH, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return ymlFile;
	}
	
	public static File createTempProjectYAMLPath(long lProjectId) throws UBSPortalException {
		File ymlFile = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
						
			String reportDirectory = getUploadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultUploadDirectory();
			}
			
			ymlFile = new File(reportDirectory
				+ File.separator
				+ PortalConstants.PROJECT_PREFIX
				+ String.valueOf(lProjectId)
				+ File.separator
				);
			ymlFile.mkdirs();
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_CREATE_CRAWL_SETTING_YAML_PATH, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return ymlFile;
	}
	
	public static String getReportFullName(long lProjectId, long lScanId, String strCaseNumber, String strProcess, String strFileName) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strCompleteFileName = null;
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			if (CommonUtil.isStringNullOrEmpty(strFileName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
				throw new UBSPortalException(PortalErrors.INVALID_REPORT_NAME);
			}
			strCaseNumber = CommonUtil.isStringNullOrEmpty(strCaseNumber) ? PortalConstants.STRING_EMPTY : strCaseNumber;
			strProcess = CommonUtil.isStringNullOrEmpty(strProcess) ? PortalConstants.STRING_EMPTY : strProcess
					+ PortalConstants.STR_UNDERSCORE; 
			
			strCompleteFileName = getDownloadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(strCompleteFileName)){
				strCompleteFileName = getDefaultDownloadDirectory();
			}
			
			strCompleteFileName += 
				 File.separator
				+ PortalConstants.PROJECT_PREFIX
				+ String.valueOf(lProjectId)
				+ File.separator
				+ PortalConstants.SCAN_PREFIX
				+ String.valueOf(lScanId)
				+ File.separator
				+ PortalConstants.DOWNLOAD_REPORT_PREFIX
				+ strCaseNumber
				+ PortalConstants.STR_UNDERSCORE
				+ String.valueOf(lScanId)
				+ PortalConstants.STR_UNDERSCORE
				+ strProcess
    			+ DateUtil.getCurrentDate(PortalConstants.DOWNLOAD_DATE_FORMAT, Locale.JAPAN)
    			+ PortalConstants.STR_UNDERSCORE
    			+ strFileName
    			+ PortalConstants.STR_DOT
    			+ PortalConstants.CSV_FILE_EXTENSION;
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strFileName", strFileName);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_REPORT_FULL_NAME, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return strCompleteFileName;
	}
	
	public static String getReportFullNameForNonCSV(long lProjectId, long lScanId, String strCaseNumber, String strProcess, String strFileName) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strCompleteFileName = null;
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			if (CommonUtil.isStringNullOrEmpty(strFileName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
				throw new UBSPortalException(PortalErrors.INVALID_REPORT_NAME);
			}
			strCaseNumber = CommonUtil.isStringNullOrEmpty(strCaseNumber) ? PortalConstants.STRING_EMPTY : strCaseNumber;
			strProcess = CommonUtil.isStringNullOrEmpty(strProcess) ? PortalConstants.STRING_EMPTY : strProcess
					+ PortalConstants.STR_UNDERSCORE; 
			
			strCompleteFileName = getDownloadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(strCompleteFileName)){
				strCompleteFileName = getDefaultDownloadDirectory();
			}
			
			strCompleteFileName += 
				 File.separator
				+ PortalConstants.PROJECT_PREFIX
				+ String.valueOf(lProjectId)
				+ File.separator
				+ PortalConstants.SCAN_PREFIX
				+ String.valueOf(lScanId)
				+ File.separator
				+ PortalConstants.DOWNLOAD_REPORT_PREFIX
				+ strCaseNumber
				+ PortalConstants.STR_UNDERSCORE
				+ String.valueOf(lScanId)
				+ PortalConstants.STR_UNDERSCORE
				+ strProcess
    			+ DateUtil.getCurrentDate(PortalConstants.DOWNLOAD_DATE_FORMAT, Locale.JAPAN)
    			+ PortalConstants.STR_UNDERSCORE
    			+ strFileName;
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strFileName", strFileName);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_REPORT_FULL_NAME, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return strCompleteFileName;
	}
	
	public static String getReportFileName(long lProjectId, long lScanId, String strCaseNumber, String strProcess, String strFileName) throws UBSPortalException {
		String strReportFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			if (CommonUtil.isStringNullOrEmpty(strFileName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
				throw new UBSPortalException(PortalErrors.INVALID_REPORT_NAME);
			}
			
			strCaseNumber = CommonUtil.isStringNullOrEmpty(strCaseNumber) ? PortalConstants.STRING_EMPTY : strCaseNumber;
			strProcess = CommonUtil.isStringNullOrEmpty(strProcess) ? PortalConstants.STRING_EMPTY : strProcess + PortalConstants.STR_UNDERSCORE; 
			
			strReportFileName = PortalConstants.DOWNLOAD_REPORT_PREFIX
				+ strCaseNumber
				+ PortalConstants.STR_UNDERSCORE
				+ String.valueOf(lScanId)
				+ PortalConstants.STR_UNDERSCORE
    			+ strProcess
    			+ DateUtil.getCurrentDate(PortalConstants.DOWNLOAD_DATE_FORMAT, Locale.JAPAN)
    			+ PortalConstants.STR_UNDERSCORE
    			+ strFileName
    			+ PortalConstants.STR_DOT
    			+ PortalConstants.CSV_FILE_EXTENSION
    			;
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strFileName", strFileName);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_REPORT_FILE_NAME, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return strReportFileName;
	}
	
	public static String getReportFileNameForNonCSV(long lProjectId, long lScanId, String strCaseNumber, String strProcess, String strFileName) throws UBSPortalException {
		String strReportFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			if (CommonUtil.isStringNullOrEmpty(strFileName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
				throw new UBSPortalException(PortalErrors.INVALID_REPORT_NAME);
			}
			
			strCaseNumber = CommonUtil.isStringNullOrEmpty(strCaseNumber) ? PortalConstants.STRING_EMPTY : strCaseNumber;
			strProcess = CommonUtil.isStringNullOrEmpty(strProcess) ? PortalConstants.STRING_EMPTY : strProcess + PortalConstants.STR_UNDERSCORE; 
			
			strReportFileName = PortalConstants.DOWNLOAD_REPORT_PREFIX
				+ strCaseNumber
				+ PortalConstants.STR_UNDERSCORE
				+ String.valueOf(lScanId)
				+ PortalConstants.STR_UNDERSCORE
    			+ strProcess
    			+ DateUtil.getCurrentDate(PortalConstants.DOWNLOAD_DATE_FORMAT, Locale.JAPAN)
    			+ PortalConstants.STR_UNDERSCORE
    			+ strFileName
    			;
			
		} catch (UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strFileName", strFileName);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_REPORT_FILE_NAME, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return strReportFileName;
	}
	
	public static String getReportZipName(long lScanId, String strCaseNumber, String strProcess) throws UBSPortalException {
		String strZipFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			
			strCaseNumber = CommonUtil.isStringNullOrEmpty(strCaseNumber) ? PortalConstants.STRING_EMPTY : strCaseNumber;
			strProcess = CommonUtil.isStringNullOrEmpty(strProcess) ? PortalConstants.STRING_EMPTY : strProcess
	    			+ PortalConstants.STR_UNDERSCORE; 
			
			strZipFileName = PortalConstants.DOWNLOAD_REPORT_PREFIX
				+ strCaseNumber
				+ PortalConstants.STR_UNDERSCORE
				+ String.valueOf(lScanId)
				+ PortalConstants.STR_UNDERSCORE
    			+ strProcess
    			+ DateUtil.getCurrentDate(PortalConstants.DOWNLOAD_DATE_FORMAT, Locale.JAPAN)
    			+ PortalConstants.STR_DOT
    			+ PortalConstants.ZIP_FILE_EXTENSION;
			
		} catch (UBSPortalException e) {
			params.put("lScanId", lScanId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_REPORT_ZIP_NAME, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return strZipFileName;
	}
	
	public static String getReportName(long lScanId, String strCaseNumber, String strProcess, String extension) throws UBSPortalException {
		String strZipFileName = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			
			strCaseNumber = CommonUtil.isStringNullOrEmpty(strCaseNumber) ? PortalConstants.STRING_EMPTY : strCaseNumber;
			strProcess = CommonUtil.isStringNullOrEmpty(strProcess) ? PortalConstants.STRING_EMPTY : strProcess
	    			+ PortalConstants.STR_UNDERSCORE; 
			
			strZipFileName = PortalConstants.DOWNLOAD_REPORT_PREFIX
				+ strCaseNumber
				+ PortalConstants.STR_UNDERSCORE
				+ String.valueOf(lScanId)
				+ PortalConstants.STR_UNDERSCORE
    			+ strProcess
    			+ DateUtil.getCurrentDate(PortalConstants.DOWNLOAD_DATE_FORMAT, Locale.JAPAN)
    			+ PortalConstants.STR_DOT
    			+ extension;
			
		} catch (UBSPortalException e) {
			params.put("lScanId", lScanId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_REPORT_ZIP_NAME, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return strZipFileName;
	}
	
	public static File uploadSourceFile(long lProjectId, long lScanId, File sourceFile, String strFileName) throws UBSPortalException {
		InputStream inputStream = null;
		File uploadedSrc = null;
		String strUploadDirectory = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}

			if (!CommonUtil.isValidFile(sourceFile)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);

			}
			if (CommonUtil.isStringNullOrEmpty(strFileName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
				throw new UBSPortalException(PortalErrors.INVALID_FILE_NAME);
			}
			
			strUploadDirectory = FileProcessUtil.createUploadDirectory(lProjectId, lScanId);
			
			if (!CommonUtil.canWrite(strUploadDirectory)) {
				params.put("strUploadDirectory", strUploadDirectory);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			uploadedSrc = new File(strUploadDirectory, strFileName);
			new File(uploadedSrc.getParent()).mkdirs();
			

			inputStream = new FileInputStream(sourceFile);
			FileUtil.write(uploadedSrc, inputStream);
			
		} catch(UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("sourceFile", sourceFile);
			params.put("strFileName", strFileName);
			params.put("strUploadDirectory", strUploadDirectory);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_UPLOAD_SOURCE_FILE, params, e);
			throw e;
		} catch(FileNotFoundException e) {
			params.put("sourceFile", sourceFile);
			params.put("strFileName", strFileName);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_UPLOAD_SOURCE_FILE, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
		} catch(IOException e) {
			params.put("uploadedSrc", uploadedSrc);
			params.put("strFileName", strFileName);
			params.put("inputStream", inputStream);
			params.put("sourceFile", sourceFile);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_UPLOAD_SOURCE_FILE, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
		} catch(Exception e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("sourceFile", sourceFile);
			params.put("strFileName", strFileName);
			params.put("strUploadDirectory", strUploadDirectory);
			params.put("uploadedSrc", uploadedSrc);
			params.put("inputStream", inputStream);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_UPLOAD_SOURCE_FILE, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return uploadedSrc;
	}
	
	public static File uploadCertificateFile(long lProjectId, File sourceFile, String strFileName) throws UBSPortalException {
		InputStream inputStream = null;
		File uploadedSrc = null;
		String strUploadDirectory = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}

			if (!CommonUtil.isValidFile(sourceFile)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);

			}
			if (CommonUtil.isStringNullOrEmpty(strFileName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
				throw new UBSPortalException(PortalErrors.INVALID_FILE_NAME);
			}
			
			strUploadDirectory = FileProcessUtil.createTempUploadDirectory(lProjectId);
			
			if (!CommonUtil.canWrite(strUploadDirectory)) {
				params.put("strUploadDirectory", strUploadDirectory);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			uploadedSrc = new File(strUploadDirectory, strFileName);
			new File(uploadedSrc.getParent()).mkdirs();
			

			inputStream = new FileInputStream(sourceFile);
			FileUtil.write(uploadedSrc, inputStream);
			
		} catch(UBSPortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("sourceFile", sourceFile);
			params.put("strFileName", strFileName);
			params.put("strUploadDirectory", strUploadDirectory);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_UPLOAD_SOURCE_FILE, params, e);
			throw e;
		} catch(FileNotFoundException e) {
			params.put("sourceFile", sourceFile);
			params.put("strFileName", strFileName);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_UPLOAD_SOURCE_FILE, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
		} catch(IOException e) {
			params.put("uploadedSrc", uploadedSrc);
			params.put("strFileName", strFileName);
			params.put("inputStream", inputStream);
			params.put("sourceFile", sourceFile);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_UPLOAD_SOURCE_FILE, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, e);
		} catch(Exception e) {
			params.put("lProjectId", lProjectId);
			params.put("sourceFile", sourceFile);
			params.put("strFileName", strFileName);
			params.put("strUploadDirectory", strUploadDirectory);
			params.put("uploadedSrc", uploadedSrc);
			params.put("inputStream", inputStream);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_UPLOAD_SOURCE_FILE, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return uploadedSrc;
	}
	
	public static String locateProjectDirectory(long lProjectId) throws UBSPortalException {
		String strProjectDirectory = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lProjectId", lProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			
			String reportDirectory = getUploadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultUploadDirectory();
			}
			
			strProjectDirectory = reportDirectory
				+ File.separator
				+ PortalConstants.PROJECT_PREFIX
				+ String.valueOf(lProjectId);
			
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_LOCATE_PROJECT_DIRECTORY, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return strProjectDirectory;
	}
	
	public static String locateScanDirectory(long lProjectId, long lScanId) throws UBSPortalException {
		String strScanDirectory = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lProjectId", lProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			
			String reportDirectory = getUploadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultUploadDirectory();
			}
			
			strScanDirectory = reportDirectory
				+ File.separator
				+ PortalConstants.PROJECT_PREFIX
				+ String.valueOf(lProjectId)
				+ File.separator
				+ PortalConstants.SCAN_PREFIX
				+ String.valueOf(lScanId);
			
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_LOCATE_SCAN_DIRECTORY, params, e);
			throw e;
		} finally {
			params.clear();
			params = null;
		}
		
		return strScanDirectory;
	}
	
	/**
	 * downloads the reports from YCloud for user to download
	 * @param lReportId the report id in Portal
	 * @param lScanId the scan id in Portal
	 * @return the byte array format of zipped reports
	 * @throws UBSPortalException the custom exception for errors encountered while processing 
	 */
	public static byte[] getZippedReports(long lProjectId, List<Report> reportList, String strProcess) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strBucketName = null;
		String strFileName = null;
		Project project = null;
		String strCaseNumber = null;
		FileInputStream fileInputStream = null;	
		BufferedInputStream bufferedInputStream = null;	
		ZipOutputStream zipOutputStream = null;	
		
		byte[] zippedReports = null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		byte bytes[] = null;
		
		String strReportFullName = null;
		String strReportDbName = null;
		String strReportDownloadName = null;
		
		int bytesRead = 0;
		int iReportType = 0;	
		long lScanId = 0L;
		String strNames[] = null;
		String strFolderName = null;
		String strFile = null;
		String downloadLocation = null;
		
		boolean bDownloadSuccess = false;
		Path reportFilePath = null;
        byte[] reportBytes = null;
        boolean bHasDownload = false;
		
		try {
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID, PortalMessages.CX_ANDROID_PROJECT_ID_INVALID);
			}
			
			if (CommonUtil.isListNullOrEmpty(reportList)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT));
				throw new UBSPortalException(PortalErrors.CX_REPORT_DB_ERROR, PortalErrors.CX_REPORT_DB_ERROR);
			}
			
			// Create the ZIP file
			byteArrayOutputStream = new ByteArrayOutputStream();
			zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
			bytes = new byte[PortalConstants.READ_SPAN];
			for (Report reportItem: reportList) {
				strBucketName = reportItem.getReportBucketName();
				strReportDbName = reportItem.getReportName();
				
				strNames = StringUtil.split(strReportDbName, File.separator);
				strFolderName = strNames[0];
				strFile = strNames[1];
				downloadLocation = strFolderName + PortalConstants.STR_FWD_SLASH + strFile;
				
				lScanId = reportItem.getScanId();
				iReportType = reportItem.getReportType();
				
				switch (iReportType) {
					case PortalConstants.METACSVTYPE: 
					{
						strFileName = PortalConstants.SUMMARY_CSV;
						break;
					}
					case PortalConstants.VULNCSVTYPE:
					{
						strFileName = PortalConstants.DETAIL_CSV;
						break;
					}
					default: break;
				
				}
				
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				strCaseNumber = project.getCaseNumber();
				
				FileProcessUtil.createDownloadReportPath(lProjectId, lScanId);
				strReportFullName = FileProcessUtil.getReportFullName(lProjectId, lScanId, strCaseNumber, strProcess, strFileName);
				strReportDownloadName = FileProcessUtil.getReportFileName(lProjectId, lScanId, strCaseNumber, strProcess, strFileName);
				
				bDownloadSuccess = ycloudUtil.downloadObject(strBucketName, downloadLocation, strReportFullName);
				
				if (bDownloadSuccess) {
					// Compress the files
					fileInputStream = new FileInputStream(strReportFullName);
			        bufferedInputStream = new BufferedInputStream(fileInputStream);
			 
			        // Add ZIP entry to output stream.
			        reportFilePath = Paths.get(strReportFullName);
			        reportBytes = Files.readAllBytes(reportFilePath);
			        if(CommonUtil.isValidBytes(reportBytes)){
			        	bHasDownload = true;
			        	zipOutputStream.putNextEntry(new ZipEntry(strReportDownloadName));
				            
			            while ((bytesRead = bufferedInputStream.read(bytes)) != -1) {
			            	zipOutputStream.write(bytes, 0, bytesRead);
			            }
			        } else {
			        	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
						log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
			        }
				} else {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
					log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
				}
	            
	            //every downloaded file is added in the zip and then input streams are close, proceed to another file if there is still another file
				
				if (zipOutputStream != null) {
					zipOutputStream.closeEntry();
				}
	            
	            if (bufferedInputStream != null) {
	            	bufferedInputStream.close();	
	            }
	            
	            if (fileInputStream != null) {
	            	fileInputStream.close();	
	            }
	            
	            FileUtil.delete(strReportFullName);
			}
			
			zipOutputStream.flush();
			byteArrayOutputStream.flush();
			
			 //below are close outside the loop
			zipOutputStream.close();
			byteArrayOutputStream.close();
		} catch (IOException ioe) {
			params.put("strReportFullName", strReportFullName);
			params.put("fileInputStream", fileInputStream);
			params.put("strReportDownloadName", strReportDownloadName);
			params.put("bufferedInputStream", bufferedInputStream);
			params.put("zipOutputStream", zipOutputStream);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, ioe);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, PortalErrors.DOWNLOAD_REPORT_FAILED, ioe);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, pe);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, PortalErrors.DOWNLOAD_REPORT_FAILED, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, se);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, PortalErrors.DOWNLOAD_REPORT_FAILED, se);
		} catch (UBSPortalException ubse) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCaseNumber", strCaseNumber);
			params.put("strProcess", strProcess);
			params.put("strFileName", strFileName);
			params.put("strReportFullName", strReportFullName);
			params.put("strReportDownloadName", strReportDownloadName);
			params.put("strBucketName", strBucketName);
			params.put("strReportDbName", strReportDbName);
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, ubse);
			throw ubse;
		} catch (Exception e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCaseNumber", strCaseNumber);
			params.put("strProcess", strProcess);
			params.put("strFileName", strFileName);
			params.put("strReportFullName", strReportFullName);
			params.put("strReportDownloadName", strReportDownloadName);
			params.put("strBucketName", strBucketName);
			params.put("strReportDbName", strReportDbName);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, e);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, PortalErrors.DOWNLOAD_REPORT_FAILED, e);
		} finally {
			if (!CommonUtil.isStringNullOrEmpty(strReportFullName)) {
				deleteFilesScanProjectFolders(new File(strReportFullName).getParent());
			}
			
			strBucketName = null;
			strFileName = null;
			project = null;
			strCaseNumber = null;
			strProcess = null;
			fileInputStream = null;	
			bufferedInputStream = null;	
			zipOutputStream = null;	
			bytes = null;
			strReportFullName = null;
			params.clear();
			params = null;
		}
		
		if (byteArrayOutputStream != null && byteArrayOutputStream.size() != 0 && bHasDownload) {
			zippedReports = byteArrayOutputStream.toByteArray();
		}
		byteArrayOutputStream = null;
		
		return zippedReports;
	}
		
	/**
	 * downloads the reports from YCloud for user to download
	 * @param lReportId the report id in Portal
	 * @param lScanId the scan id in Portal
	 * @return the byte array format of zipped reports
	 * @throws UBSPortalException the custom exception for errors encountered while processing 
	 */
	public static byte[] getIndividualReports(long lProjectId, long lScanId, List<Report> reportList, String strProcess) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strBucketName = null;
		String strFileName = null;
		Project project = null;
		String strCaseNumber = null;
		FileInputStream fileInputStream = null;	
		BufferedInputStream bufferedInputStream = null;	
		ZipOutputStream zipOutputStream = null;	
		FileOutputStream fileOutputStream = null;
		
		byte[] zippedReports = null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		byte bytes[] = null;
		
		String strReportFullName = null;
		String strReportDbName = null;
		String strReportDownloadName = null;
		
		int bytesRead = 0;
		int iReportType = 0;	
		String strNames[] = null;
		String strFolderName = null;
		String strFile = null;
		String downloadLocation = null;
		
		boolean bDownloadSuccess = false;
		Path reportFilePath = null;
        byte[] reportBytes = null;
        boolean bHasDownload = false;
		
		try {
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID, PortalMessages.CX_ANDROID_PROJECT_ID_INVALID);
			}
			
			if (CommonUtil.isListNullOrEmpty(reportList)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT));
				throw new UBSPortalException(PortalErrors.CX_REPORT_DB_ERROR, PortalErrors.CX_REPORT_DB_ERROR);
			}
			
			// Create the ZIP file
			byteArrayOutputStream = new ByteArrayOutputStream();
			zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
			bytes = new byte[PortalConstants.READ_SPAN];
			for (Report reportItem: reportList) {
				strBucketName = reportItem.getReportBucketName();
				strReportDbName = reportItem.getReportName();
				
				if(strReportDbName.contains(File.separator)){
					strNames = StringUtil.split(strReportDbName, File.separator);
					strFolderName = strNames[0];
					strFile = strNames[1];
					downloadLocation = strFolderName + PortalConstants.STR_FWD_SLASH + strFile;
				}else{
					downloadLocation = strReportDbName;
				}
				
				iReportType = reportItem.getReportType();
				
				switch (iReportType) {
					case PortalConstants.METACSVTYPE: 
					{
						strFileName = PortalConstants.SUMMARY_CSV;
						break;
					}
					case PortalConstants.VULNCSVTYPE:
					{
						strFileName = PortalConstants.DETAIL_CSV;
						break;
					}
					case PortalConstants.XMLTYPE:
					{
						strFileName = PortalConstants.CXSUITE_REPORT_XML;
						break;
					}
					case PortalConstants.WORDTYPE:
					{
						strFileName = PortalConstants.CXSUITE_REPORT_WORD;
						break;
					}
					case PortalConstants.XLSTYPE:
					{
						strFileName = PortalConstants.VEX_REPORT_XLS;
						break;
					}
					default: break;
				
				}
				
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				strCaseNumber = project.getCaseNumber();
				
				FileProcessUtil.createDownloadReportPath(lProjectId, lScanId);
				if(iReportType == PortalConstants.METACSVTYPE || iReportType == PortalConstants.VULNCSVTYPE){
					strReportFullName = FileProcessUtil.getReportFullName(lProjectId, lScanId, strCaseNumber, strProcess, strFileName);
					strReportDownloadName = FileProcessUtil.getReportFileName(lProjectId, lScanId, strCaseNumber, strProcess, strFileName);
				}else{
					strReportFullName = FileProcessUtil.getReportFullNameForNonCSV(lProjectId, lScanId, strCaseNumber, strProcess, strFileName);
					strReportDownloadName = FileProcessUtil.getReportFileNameForNonCSV(lProjectId, lScanId, strCaseNumber, strProcess, strFileName);
				}
				
				bDownloadSuccess = ycloudUtil.downloadObject(strBucketName, downloadLocation, strReportFullName);
				
				if (bDownloadSuccess) {
					// Compress the files
					fileInputStream = new FileInputStream(strReportFullName);
			        bufferedInputStream = new BufferedInputStream(fileInputStream);
			 
			        // Add downloaded file to output stream.
			        reportFilePath = Paths.get(strReportFullName);
			        reportBytes = Files.readAllBytes(reportFilePath);
//			        if(CommonUtil.isValidBytes(reportBytes)){
//			        	bHasDownload = true;
//			        	fileOutputStream = new FileOutputStream(strReportDownloadName);
//				            
//			            while ((bytesRead = bufferedInputStream.read(bytes)) != -1) {
//			            	fileOutputStream.write(bytes, 0, bytesRead);
//			            }
//			        } else {
//			        	params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
//						log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
//			        }
				} else {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
					log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
				}
	            
	            //every downloaded file is added in the zip and then input streams are close, proceed to another file if there is still another file
				
				if (zipOutputStream != null) {
					zipOutputStream.closeEntry();
				}
	            
	            if (bufferedInputStream != null) {
	            	bufferedInputStream.close();	
	            }
	            
	            if (fileInputStream != null) {
	            	fileInputStream.close();	
	            }
	            
	            FileUtil.delete(strReportFullName);
			}
			
			zipOutputStream.flush();
			byteArrayOutputStream.flush();
			
			 //below are close outside the loop
			zipOutputStream.close();
			byteArrayOutputStream.close();
		} catch (IOException ioe) {
			params.put("strReportFullName", strReportFullName);
			params.put("fileInputStream", fileInputStream);
			params.put("strReportDownloadName", strReportDownloadName);
			params.put("bufferedInputStream", bufferedInputStream);
			params.put("zipOutputStream", zipOutputStream);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, ioe);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, PortalErrors.DOWNLOAD_REPORT_FAILED, ioe);
		} catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, pe);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, PortalErrors.DOWNLOAD_REPORT_FAILED, pe);
		} catch (SystemException se) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, se);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, PortalErrors.DOWNLOAD_REPORT_FAILED, se);
		} catch (UBSPortalException ubse) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCaseNumber", strCaseNumber);
			params.put("strProcess", strProcess);
			params.put("strFileName", strFileName);
			params.put("strReportFullName", strReportFullName);
			params.put("strReportDownloadName", strReportDownloadName);
			params.put("strBucketName", strBucketName);
			params.put("strReportDbName", strReportDbName);
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, ubse);
			throw ubse;
		} catch (Exception e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCaseNumber", strCaseNumber);
			params.put("strProcess", strProcess);
			params.put("strFileName", strFileName);
			params.put("strReportFullName", strReportFullName);
			params.put("strReportDownloadName", strReportDownloadName);
			params.put("strBucketName", strBucketName);
			params.put("strReportDbName", strReportDbName);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ZIPPED_REPORTS, params, e);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, PortalErrors.DOWNLOAD_REPORT_FAILED, e);
		} finally {
			if (!CommonUtil.isStringNullOrEmpty(strReportFullName)) {
				deleteFilesScanProjectFolders(new File(strReportFullName).getParent());
			}
			
			strBucketName = null;
			strFileName = null;
			project = null;
			strCaseNumber = null;
			strProcess = null;
			fileInputStream = null;	
			bufferedInputStream = null;	
			zipOutputStream = null;	
			bytes = null;
			strReportFullName = null;
			params.clear();
			params = null;
		}
		
		return reportBytes;
	}

	public static byte[] getAnalyzedZippedReport(long lProjectId, Report report) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strBucketName = null;
		String strCaseNumber = null;
		
		Project project = null;
		
		byte[] zippedReports = null;
		
		String strReportFullName = null;
		String strReportDbName = null;
		String strReportDownloadName = null;
		String strName[] = null;
		
		long lScanId = PortalConstants.LONG_ZERO;
		Path downloadedReportFilePath = null;
		try {
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}		
			if (CommonUtil.isObjectNull(report)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT));
				throw new UBSPortalException(PortalErrors.CX_REPORT_DB_ERROR);
			}
			
			strBucketName = report.getReportBucketName();
			strReportDbName = report.getReportName();
			lScanId = report.getScanId();
			strName = StringUtil.split(strReportDbName, File.separator);
			
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			strCaseNumber = project.getCaseNumber();
			
			FileProcessUtil.createDownloadReportPath(lProjectId, lScanId);
			strReportDownloadName = strName[1];
			
			String reportDirectory = getDownloadBasePathByProjectId(lProjectId);
			if(CommonUtil.isStringNullOrEmpty(reportDirectory)){
				reportDirectory = getDefaultDownloadDirectory();
			}
			
			strReportFullName = reportDirectory
					+ File.separator
					+ PortalConstants.PROJECT_PREFIX
					+ String.valueOf(lProjectId)
					+ File.separator
					+ strReportDownloadName;
			
			if (ycloudUtil.downloadObject(strBucketName, strName[0] +PortalConstants.STR_FWD_SLASH + strName[1], strReportFullName)) {
				downloadedReportFilePath = Paths.get(strReportFullName);
				if (downloadedReportFilePath != null) {
					zippedReports = Files.readAllBytes(downloadedReportFilePath);
					if (!CommonUtil.isValidBytes(zippedReports)) {
						params.put("zippedReports valid", CommonUtil.isValidBytes(zippedReports));
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
						log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
					}
				} else {
					params.put("downloadedReportFilePath", downloadedReportFilePath);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
					log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
				}
			} else {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
			}
			
		
		} catch (UBSPortalException ubse) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCaseNumber", strCaseNumber);
			params.put("strReportFullName", strReportFullName);
			params.put("strReportDownloadName", strReportDownloadName);
			params.put("strBucketName", strBucketName);
			params.put("strReportDbName", strReportDbName);
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, ubse);
			throw ubse;
		} catch (Exception e) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("strCaseNumber", strCaseNumber);
			params.put("strReportFullName", strReportFullName);
			params.put("strReportDownloadName", strReportDownloadName);
			params.put("strBucketName", strBucketName);
			params.put("strReportDbName", strReportDbName);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, e);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, e);
		} finally {
			File reportFile = new File(strReportFullName);
			
			if (!CommonUtil.isObjectNull(reportFile)) {
				FileProcessUtil.deleteFileDirectory(reportFile);
			}
			
			if (!CommonUtil.isStringNullOrEmpty(reportFile.getAbsolutePath())) {
				deleteFilesScanProjectFolders(reportFile.getAbsolutePath());
			}
			
			reportFile = null;
			strBucketName = null;
			strCaseNumber = null;
			strReportFullName = null;
			params.clear();
			params = null;
		}
		
		return zippedReports;
	}
	
	public static boolean deleteReportFilesByScanId(long lScanId) throws UBSPortalException {
		boolean bDeleted = false;
		Map<String, Object> params = new HashMap<String, Object>();
		List<Report> reportList = null;
		String strBucketName = null;
		String strReportDbName = null;
		String strNames[] = null;
		String strFolderName = null;
		String strFileName = null;	
		int strNameLength = 0;
		
		try {
			reportList = ReportLocalServiceUtil.getReportByScanId(lScanId);
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				for (Report reportItem : reportList) {
					strReportDbName = reportItem.getReportName();
					strBucketName = reportItem.getReportBucketName();
					
					strNames = StringUtil.split(strReportDbName, File.separator);
					strNameLength = strNames.length;
					if (strNameLength == 2) {
						strFolderName = strNames[0];
						strFileName = strNames[1];
					}
					
					try {
						bDeleted = ycloudUtil.deleteFile(strBucketName, strFolderName 
									+ PortalConstants.STR_FWD_SLASH + strFileName);
					} catch (UBSPortalException e) {
						//log only so that it can continue with other files
						params.put("strBucketName", strBucketName);
						params.put("strFolderName", strFolderName);
						params.put("strFileName", strFileName);
						log.debug(e.getErrorCode(), PortalConstants.METHOD_DELETE_REPORT_FILES_BY_SCAN_ID, params, e);
					}
				}
			}
		} catch (PortalException e) {
			params.put("lScanId", lScanId);
			log.debug(PortalErrors.YCLOUD_REPORT_DELETE_ERROR, PortalConstants.METHOD_DELETE_REPORT_FILES_BY_SCAN_ID, params, e);
			throw new UBSPortalException(PortalErrors.DELETE_REPORT_FAILED, PortalMessages.DELETE_REPORT_FAILED, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return bDeleted;
	}
	
	public static void deleteReportFilesByProjectId(long lProjectId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Report> reportList = null;
		List<Scan> scanList = null;		
		long lScanId = PortalConstants.LONG_ZERO;
		String strBucketName = null;
		String strReportDbName = null;
		String strNames[] = null;
		String strFolderName = null;
		String strFileName = null;	
		int strNameLength = 0;
		boolean bDeleted = false;
		
		try {
			scanList = ScanLocalServiceUtil.getScansByProjectId(lProjectId);
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				for (Scan scanItem : scanList) {
					lScanId = scanItem.getScanId();
					reportList = ReportLocalServiceUtil.getReportByScanId(lScanId);
					if (!CommonUtil.isListNullOrEmpty(reportList)) {
						for (Report reportItem : reportList) {
							strReportDbName = reportItem.getReportName();
							strBucketName = reportItem.getReportBucketName();
							
							strNames = StringUtil.split(strReportDbName, File.separator);
							strNameLength = strNames.length;
							if (strNameLength == 2) {
								strFolderName = strNames[0];
								strFileName = strNames[1];
							}
							
							try {
								bDeleted = ycloudUtil.deleteFile(strBucketName, strFolderName 
											+ PortalConstants.STR_FWD_SLASH + strFileName);
								if (!bDeleted) {
									//log only so that it can continue with other files
									params.put("strBucketName", strBucketName);
									params.put("strFolderName", strFolderName);
									params.put("strFileName", strFileName);
									log.debug(PortalErrors.DELETE_REPORT_FAILED, PortalConstants.METHOD_DELETE_REPORT_FILES_BY_PROJECT_ID, params, new UBSPortalException(PortalErrors.YCLOUD_REPORT_DELETE_ERROR));
								}
							} catch (UBSPortalException e) {
								//log only so that it can continue with other files
								params.put("strBucketName", strBucketName);
								params.put("strFolderName", strFolderName);
								params.put("strFileName", strFileName);
								log.debug(e.getErrorCode(), PortalConstants.METHOD_DELETE_REPORT_FILES_BY_PROJECT_ID, params, e);
							}
						}
					}
				}
			}
		} catch (PortalException e) {
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.YCLOUD_REPORT_DELETE_ERROR, PortalConstants.METHOD_DELETE_REPORT_FILES_BY_PROJECT_ID, params, e);
			throw new UBSPortalException(PortalErrors.DELETE_REPORT_FAILED, PortalMessages.DELETE_REPORT_FAILED, e);
		} finally {
			params.clear();
			params = null;
		}
	}

	public static void deleteFileDirectory(File file) {
		if (file != null && file.isFile()) {
			FileUtil.deltree(file.getParent());
		} else if (file != null && file.isDirectory()) {
			FileUtil.deltree(file);
		}
	}
		
	public static byte[] getVulnFilePath(int projectType) throws UBSPortalException{
		Map<String, Object> params = new HashMap<String, Object>();
		
		byte vulnBytes[] = null;
		Path downloadVulnListPath = null;
		String vulnListDirectory = null;
		
		
		try {
			String vulnList = PortalConstants.STRING_EMPTY;
			
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				vulnList = PortalConstants.VULNLISTPDF;
			} else if (projectType == ProjectType.IOS.getInteger()) {
				vulnList = PortalConstants.VULNLIST_IOS_PDF;
			}
			
			String baseDirectory = getResourceBasePath();
			if(CommonUtil.isStringNullOrEmpty(baseDirectory)){
				baseDirectory = getDefaultResourceBasePath();
			}
			
			vulnListDirectory = baseDirectory
					+ File.separator
					+ vulnList;
			downloadVulnListPath = Paths.get(vulnListDirectory);
			vulnBytes = Files.readAllBytes(downloadVulnListPath);
		} catch (NoSuchFileException e) {
			params.put("downloadVulnListPath", downloadVulnListPath);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_FILE));
			log.debug(PortalErrors.DOWNLOAD_VULN_LIST_FAILED, PortalConstants.METHOD_GET_VULN_FILE_PATH, params, e);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_VULN_LIST_FAILED, PortalMessages.NO_DESCRIPTION, e);
		} catch (IOException e) {
			params.put("downloadVulnListPath", downloadVulnListPath);
			log.debug(PortalErrors.DOWNLOAD_VULN_LIST_FAILED, PortalConstants.METHOD_GET_VULN_FILE_PATH, params, e);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_VULN_LIST_FAILED, PortalMessages.NO_DESCRIPTION, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return vulnBytes;
	}
	
	public static byte[] getManualFilePath (String filename) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		
		byte manualBytes[] = null;
		Path downloadManualPath = null;
		String manualDirectory = null;
		
		
		try {
			// TODO
			/*manualDirectory = System.getProperty(PortalConstants.CATALINA_BASE)
					+ File.separator
					+ PortalConstants.WEBAPPS_FOLDER
					+ File.separator
					+ PortalConstants.UBSECURE_FOLDER
					+ File.separator
					+ PortalConstants.DOC_FOLDER
					+ File.separator
					+ filename;*/
			String baseDirectory = getResourceBasePath();
			if(CommonUtil.isStringNullOrEmpty(baseDirectory)){
				baseDirectory = getDefaultResourceBasePath();
			}
			
			manualDirectory = baseDirectory
					+ File.separator
					+ filename;
			
			downloadManualPath = Paths.get(manualDirectory);
			
			manualBytes = Files.readAllBytes(downloadManualPath);
		} catch (NoSuchFileException e) {
			params.put("downloadManualPath", downloadManualPath);
			log.debug(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL, PortalConstants.METHOD_GET_MANUAL_FILE_PATH, params, e);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL, PortalMessages.NO_MANUAL, e);
		} catch (IOException e) {
			params.put("downloadManualPath", downloadManualPath);
			log.debug(PortalErrors.DOWNLOAD_MANUAL_FAILED, PortalConstants.METHOD_GET_MANUAL_FILE_PATH, params, e);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_MANUAL_FAILED, PortalMessages.DOWNLOAD_MANUAL_FAILED, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_MANUAL_FILE_PATH, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.DOWNLOAD_MANUAL_FAILED, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return manualBytes;
	}
	
	/**
	 * Deletes all files inside the directory and its parent directory
	 * i.e. report files, scan folder, project folder
	 * @param directory location of the summary folder to be deleted
	 */
	public static void deleteFilesSummaryFolder (String directory) {
		File folder = new File (directory);
		File [] files = null;
		
		if (!CommonUtil.isObjectNull(folder)) {
			files = folder.listFiles();
			
			if (!CommonUtil.isObjectNull(files) && files.length > PortalConstants.INT_ZERO) {
				for (File file : files) {
					FileProcessUtil.deleteFileDirectory(file);
				}
			}
			
			FileProcessUtil.deleteFileDirectory(folder);
		}
	}
	
	/**
	 * Deletes files, scan and project folders
	 * @param scanDirectory location of the scan folder
	 */
	public static void deleteFilesScanProjectFolders (String scanDirectory) {
		File scanFolder = new File(scanDirectory);
		File [] reports = null;
		
		if (!CommonUtil.isObjectNull(scanFolder)) {
			reports = scanFolder.listFiles();
			
			if (!CommonUtil.isObjectNull(reports) && reports.length > PortalConstants.INT_ZERO) {
				for (File report : reports) {
					report.delete();
				}
			}
			
			reports = scanFolder.listFiles();
			
			if (CommonUtil.isObjectNull(reports) || reports.length == PortalConstants.INT_ZERO) {
				scanFolder.delete();
			}
		}
		
		File projectFolder = new File(scanFolder.getParent());
		File [] scanFolders = null;
		
		if (!CommonUtil.isObjectNull(projectFolder)) {
			scanFolders = projectFolder.listFiles();
			
			if (CommonUtil.isObjectNull(scanFolders) || scanFolders.length == PortalConstants.INT_ZERO) {
				projectFolder.delete();
			}
		}
	}
	
	public static boolean renameFile(String source, String target)  
	{  
	  return new File(source).renameTo(new File(target));  
	}  
	
	public static boolean copyFile(String source, String target){
		boolean isSucccess = false;
		Map<String,Object> params = new HashMap<String, Object>();
    	params.put("SourceFile", source);
    	params.put("TargetFile", target);
		try {
			File targetFile = new File(target);
			File sourceFile = new File(source);
			Files.copy(sourceFile.toPath(), targetFile.toPath());
			isSucccess = targetFile.exists();
		} catch (IOException e) {
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
		} catch(Exception e){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
    	} catch(Throwable t){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, t);
    	} 
		return isSucccess;
	}
	
	public static String getNewFileWithNewExtension (String source, String newExtension)  
	{  
	  String target;  
	  String currentExtension = getFileExtension(source);  
	  
	  if (currentExtension.equals("")){  
	     target = source + "." + newExtension;  
	  }  
	  else {  
	     target = source.replaceAll("." + currentExtension, newExtension);  
	  }  
	  return target;
	}  

	public static String getFileExtension(String fileExtension) {  
	  String ext = "";  
	  int i = fileExtension.lastIndexOf('.');  
	  if (i > 0 &&  i < fileExtension.length() - 1) {  
	     ext = fileExtension.substring(i + 1).toLowerCase();  
	  }  
	  return ext;  
	} 
	
	public static boolean deleteDirectory(File directory){
		Map<String,Object> params = new HashMap<String, Object>();
    	params.put("Directory", directory.getAbsolutePath().toString());
		try {
			org.apache.commons.io.FileUtils.deleteDirectory(directory);
		} catch (IOException e) {
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
		}
		return directory.exists();
	}

    public static boolean hasFilesInFolder(File folder){
    	boolean hasFiles = false;
    	Map<String,Object> params = new HashMap<String, Object>();
    	params.put("Source", folder.getAbsolutePath().toString());
    	try{
		if(folder.exists()){
			File []  folderFiles = folder.listFiles();
			if(null != folderFiles && folderFiles.length > 0){
				hasFiles = true;
			}
		}
    	}catch(Exception e){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
    	}catch(Throwable t){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, t);
    	}
		return hasFiles;
	}
     
    public static String getFileNameOnly(String target){
    	return target.substring(0, target.length()-4);	
    }
    
    public static boolean isUnix() {
    	String OS = System.getProperty("os.name").toLowerCase();
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}
    
    public static boolean unzipVexFile(String source){
    	Map<String,Object> params = new HashMap<String, Object>();
    	params.put("ZipFile", source);
    	boolean isSuccess = false;
        try {
        	String zipFilePath = source;
        	String target = zipFilePath.replaceFirst("[\\\\/]$", "").replaceAll("\"", "");
        	String destination = getFileNameOnly(target);
        	File unzipFile = null;
        	if(isUnix()){
        		String command = "unzip \""+ source +"\" -d \""+ destination +"\"";
        		ProcessBuilder processBuilder = new ProcessBuilder("/bin/bash", "-c", command);
				Process process = processBuilder.start();
				BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String line;
				while ((line = reader.readLine()) != null){
				    System.out.println("Unzipping: " + line);
				}
				process.waitFor();
        	}else{
				ZipUtils.deCompress(destination, new File(target));
        	}
        	unzipFile = new File(destination);
			isSuccess = hasFilesInFolder(unzipFile);
		} catch (IOException e) {
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
		} catch(Exception e){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
    	} catch(Throwable t){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, t);
    	}
        return isSuccess;
    }
	
    public static String getDefaultResourceBasePath(){
    	String defaultPath = PropsUtil.get(PropsKeys.LIFERAY_HOME)
			+ File.separator
			+ PortalConstants.DATA_FOLDER
			+ File.separator
			+ PortalConstants.RESOURCE_FOLDER;
    	return defaultPath;
    }
    
    public static String getResourceBasePath () {
		return ConfigurationFileParser.getResourceBasePath(PortalConstants.RESOURCE_PATH, PortalConstants.CONFIG_KEY_RESOURCE_PATH);
	}
    
	public static String getCxUploadBasePath () {
		return getBasePath(PortalConstants.CX_UPLOAD_PATH, PortalConstants.CONFIG_KEY_CX_UPLOAD_PATH);
	}
	
	public static String getCxDownloadBasePath () {
		return getBasePath(PortalConstants.CX_DOWNLOAD_PATH, PortalConstants.CONFIG_KEY_CX_DOWNLOAD_PATH);
	}
	
	public static String getAndroidUploadBasePath () {
		return getBasePath(PortalConstants.ANDROID_UPLOAD_PATH, PortalConstants.CONFIG_KEY_ANDROID_UPLOAD_PATH);
	}
	
	public static String getAndroidDownloadBasePath () {
		return getBasePath(PortalConstants.ANDROID_DOWNLOAD_PATH, PortalConstants.CONFIG_KEY_ANDROID_DOWNLOAD_PATH);
	}
	
	public static String getIOSUploadBasePath () {
		return getBasePath(PortalConstants.IOS_UPLOAD_PATH, PortalConstants.CONFIG_KEY_IOS_UPLOAD_PATH);
	}
	
	public static String getIOSDownloadBasePath () {
		return getBasePath(PortalConstants.IOS_DOWNLOAD_PATH, PortalConstants.CONFIG_KEY_IOS_DOWNLOAD_PATH);
	}
	
	public static String getVexUploadBasePath () {
		return getBasePath(PortalConstants.VEX_UPLOAD_PATH, PortalConstants.CONFIG_KEY_VEX_UPLOAD_PATH);
	}
	
	public static String getVexDownloadBasePath () {
		return getBasePath(PortalConstants.VEX_DOWNLOAD_PATH, PortalConstants.CONFIG_KEY_VEX_DOWNLOAD_PATH);
	}
	
	private static String getBasePath(String configKeyName, String configKey){
		String configValue = PortalConstants.STRING_EMPTY;
		Map<String, Object> params = new HashMap<String, Object>();
		try {
			params.put(configKeyName, configKey);
			configValue = ConfigurationFileParser.getConfig(configKey);
			if(CommonUtil.isStringNullOrEmpty(configValue)){
				throw new UBSPortalException(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.ERROR_RETRIEVAL_BASE_PATH, null);
			}
		} catch (UBSPortalException e) {
			log.debug(PortalErrors.CONFIG_FILE_ERROR, PortalConstants.METHOD_VEX_GET_BASE_PATH, params, e);
		} catch (Exception e){
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_VEX_GET_BASE_PATH, params, e);
		}
		
		return configValue;
	}
	
	public static String getUploadBasePathByProjectType(int iProjectType){
		return getBasePathByProjectTypeOrId(iProjectType, 0, false);
	}
	
	public static String getUploadBasePathByProjectId(long lProjectId){
		return getBasePathByProjectTypeOrId(0, lProjectId, false);
	}
	
	public static String getDownloadBasePathByProjectType(int iProjectType) {
		return getBasePathByProjectTypeOrId(iProjectType, 0, true);
	}
	
	public static String getDownloadBasePathByProjectId(long lProjectId){
		return getBasePathByProjectTypeOrId(0, lProjectId, true);
	}
	
	private static String getBasePathByProjectTypeOrId(int iProjectType, long lProjectId, boolean isDownloadDirectory)  {
		Project project = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String basePath = PortalConstants.STRING_EMPTY;
		try{
			int type = iProjectType;
			if(lProjectId > PortalConstants.LONG_ZERO){
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				type = project.getType();
			}
			
			switch(type){
				case PortalConstants.PROJECT_TYPE_CX_SUITE: 
					basePath = isDownloadDirectory ? getCxDownloadBasePath() : getCxUploadBasePath();
					break;	
				case PortalConstants.PROJECT_TYPE_ANDROID: 
					basePath = isDownloadDirectory ? getAndroidDownloadBasePath() : getAndroidUploadBasePath();
					break;	
				case PortalConstants.PROJECT_TYPE_IOS: 
					basePath = isDownloadDirectory ? getIOSDownloadBasePath() : getIOSUploadBasePath();
					break;	
				case PortalConstants.PROJECT_TYPE_VEX: 
					basePath = isDownloadDirectory ? getVexDownloadBasePath() : getVexUploadBasePath();
					break;	
			}
		}catch (PortalException pe) {
			params.put("lProjectId", lProjectId);
			params.put("project", project);
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_VEX_GET_BASE_PATH, params, pe);
		}  finally {
			params.clear();
			params = null;
		}
		return basePath;
	}
	
	public static boolean hasSpecificFileInFolder(File folder, String fileName){
    	boolean hasSpecificFile = false;
    	Map<String,Object> params = new HashMap<String, Object>();
    	params.put("Source", folder.getAbsolutePath().toString());
    	try{
    		if(folder.exists() && !CommonUtil.isStringNullOrEmpty(fileName)){
    			File []  folderFiles = folder.listFiles();
    			if(null != folderFiles && folderFiles.length > 0){
    				for(File file: folderFiles){
    					if(file.getName().equals(fileName)){
    						hasSpecificFile = true;
    						break;
    					}
    				}
    			}
    		}
    	}catch(Exception e){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, e);
    	}catch(Throwable t){
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.ERROR_LOG_PARAM_FILE, params, t);
    	}
		return hasSpecificFile;
	}
	
}
