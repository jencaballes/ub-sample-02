package jp.ubsecure.portal.jubjub.hook.action;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.users.admin.web.portlet.action.EditUserMVCActionCommand;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.MailUtil;

@Component(
		immediate = true,
		property = {
			PortalConstants.KEY_JAVAX_PORTLET_NAME + PortalConstants.PORTLET_NAME_MY_ACCOUNT,
			PortalConstants.KEY_MVC_COMMAND_NAME + PortalConstants.COMMAND_NAME_EDIT_USER,
			PortalConstants.KEY_SERVICE_RANKING + PortalConstants.SERVICE_RANKING_200
		},
		service = MVCActionCommand.class
	)
public class ChangePasswordStrutsAction extends EditUserMVCActionCommand {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ChangePasswordStrutsAction.class);
	
	@Reference(target = "(&(" + PortalConstants.KEY_MVC_COMMAND_NAME + PortalConstants.COMMAND_NAME_EDIT_USER + ")(" + PortalConstants.KEY_JAVAX_PORTLET_NAME + PortalConstants.PORTLET_NAME_MY_ACCOUNT + ")"
			+ "(" + PortalConstants.KEY_COMPONENT_NAME + PortalConstants.COMPONENT_NAME_EDIT_USER + "))")
	protected MVCActionCommand mvcActionCommand;
	
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		HttpSession session = null;
		long lUserId = PortalConstants.LONG_ZERO;
		SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
		SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		try {
			session = ControllerHelper.getHttpSession(actionRequest);
			lUserId = Long.parseLong(session.getAttribute(PortalConstants.USER_ID).toString());
			log.info(PortalMessages.CHANGE_PWD, lUserId);
			
			StrutsActionManager.changeP(mvcActionCommand, actionRequest, actionResponse);
			
			SessionMessages.add(actionRequest, PortalConstants.SUCCESS_PWD_EDIT);
		
			Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			int iUserRole = PortalConstants.INT_ZERO;
					
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
				
				if (iUserRole == UserRole.GEN_USER.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
					MailUtil.sendEmail(PortalConstants.EMAIL_EVENT_PASSWORD_CHANGE, lUserId, null);
				}
			}
		} catch (Exception e) {
			if (e instanceof UBSPortalException) {
				if (((UBSPortalException) e).getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					SessionErrors.add(actionRequest, PortalConstants.ORM_EXCEPTION);
				}
			} else if (e instanceof IllegalArgumentException && SessionErrors.isEmpty(actionRequest)) {
				SessionMessages.add(actionRequest, PortalConstants.SUCCESS_PWD_EDIT);

				Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
				int iUserRole = PortalConstants.INT_ZERO;
				
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
					
					if (iUserRole == UserRole.GEN_USER.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						MailUtil.sendEmail(PortalConstants.EMAIL_EVENT_PASSWORD_CHANGE, lUserId, null);
					}
				}
			} else {
				SessionErrors.add(actionRequest, e.getClass());
			}
		}
	}
}
