package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.IOSMetaSummaryEntry;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class IOSMetaSummaryEntryParser implements CSVEntryConverter<IOSMetaSummaryEntry> {

private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(IOSMetaSummaryEntryParser.class);
	
	@Override
	public String[] convertEntry(IOSMetaSummaryEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (entry == null) {
			params.put("entry", entry);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException(PortalErrors.FILE_PROCESS_ERROR);					
		}
		
		String[] columns = new String[12];
		columns[0] = entry.getCompanyName();
	    columns[1] = entry.getProjectName();
	    columns[2] = entry.getCaseNumber();
	    columns[3] = entry.getScanId();
	    columns[4] = entry.getExecutionDate();
	    columns[5] = entry.getHighCount();
	    columns[6] = entry.getMediumCount();
	    columns[7] = entry.getLowCount();
	    columns[8] = entry.getInfoCount();
	    columns[9] = entry.getExecutionTime();
	    columns[10] = entry.getLOC();
	    columns[11] = entry.getPreset();
	    
	    for (int index = 0; index < columns.length; index++) {
	    	if (columns[index] == null) {
	    		columns[index] = PortalConstants.STRING_EMPTY;
	    	}
	    }
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}
}
