package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class CxSDKWebServiceSoapProxy implements jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoap {
  private String _endpoint = null;
  private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoap cxSDKWebServiceSoap = null;
  
  public CxSDKWebServiceSoapProxy() {
    _initCxSDKWebServiceSoapProxy();
  }
  
  public CxSDKWebServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initCxSDKWebServiceSoapProxy();
  }
  
  private void _initCxSDKWebServiceSoapProxy() {
    try {
      cxSDKWebServiceSoap = (new jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceLocator()).getCxSDKWebServiceSoap();
      if (cxSDKWebServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)cxSDKWebServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)cxSDKWebServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (cxSDKWebServiceSoap != null)
      ((javax.xml.rpc.Stub)cxSDKWebServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxSDKWebServiceSoap getCxSDKWebServiceSoap() {
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap;
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID scan(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CliScanArgs args) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.scan(sessionId, args);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID branchProjectById(java.lang.String sessionId, long originProjectId, java.lang.String newBranchProjectName) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.branchProjectById(sessionId, originProjectId, newBranchProjectName);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID scanWithOriginName(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CliScanArgs args, java.lang.String origName) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.scanWithOriginName(sessionId, args, origName);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse cancelScan(java.lang.String sessionID, java.lang.String runId) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.cancelScan(sessionID, runId);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse deleteProjects(java.lang.String sessionID, long[] projectIDs) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.deleteProjects(sessionID, projectIDs);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse deleteScans(java.lang.String sessionID, long[] scanIDs) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.deleteScans(sessionID, scanIDs);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID scanWithScheduling(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CliScanArgs args, java.lang.String schedulingData) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.scanWithScheduling(sessionId, args, schedulingData);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseRunID scanWithSchedulingWithCron(java.lang.String sessionId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CliScanArgs args, java.lang.String cronString, long utcEpochStartTime, long utcEpochEndTime) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.scanWithSchedulingWithCron(sessionId, args, cronString, utcEpochStartTime, utcEpochEndTime);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse updateScanComment(java.lang.String sessionID, long scanID, java.lang.String comment) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.updateScanComment(sessionID, scanID, comment);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponsePresetList getPresetList(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getPresetList(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseConfigSetList getConfigurationSetList(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getConfigurationSetList(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectsDisplayData getProjectsDisplayData(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getProjectsDisplayData(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectScannedDisplayData getProjectScannedDisplayData(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getProjectScannedDisplayData(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectConfig getProjectConfiguration(java.lang.String sessionID, long projectID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getProjectConfiguration(sessionID, projectID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse updateProjectIncrementalConfiguration(java.lang.String sessionID, long projectID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectConfiguration projectConfiguration) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.updateProjectIncrementalConfiguration(sessionID, projectID, projectConfiguration);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseLoginData login(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials applicationCredentials, int lcid) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.login(applicationCredentials, lcid);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseLoginData ssoLogin(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.Credentials encryptedCredentials, int lcid) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.ssoLogin(encryptedCredentials, lcid);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseLoginData loginWithToken(java.lang.String token, int lcid) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.loginWithToken(token, lcid);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse logout(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.logout(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseGroupList getAssociatedGroupsList(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getAssociatedGroupsList(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseScanStatus getStatusOfSingleScan(java.lang.String sessionID, java.lang.String runId) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getStatusOfSingleScan(sessionID, runId);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseScansDisplayData getScansDisplayDataForAllProjects(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getScansDisplayDataForAllProjects(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseScanSummary getScanSummary(java.lang.String sessionID, long scanID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getScanSummary(sessionID, scanID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse updateProjectConfiguration(java.lang.String sessionID, long projectID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectConfiguration projectConfiguration) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.updateProjectConfiguration(sessionID, projectID, projectConfiguration);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse deleteUser(java.lang.String sessionID, int userID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.deleteUser(sessionID, userID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseUserData getAllUsers(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getAllUsers(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSCreateReportResponse createScanReport(java.lang.String sessionID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSReportRequest reportRequest) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.createScanReport(sessionID, reportRequest);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSReportStatusResponse getScanReportStatus(java.lang.String sessionID, long reportID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getScanReportStatus(sessionID, reportID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseScanResults getScanReport(java.lang.String sessionID, long reportID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getScanReport(sessionID, reportID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse executeDataRetention(java.lang.String sessionID, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxDataRetentionConfiguration dataRetentionConfiguration) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.executeDataRetention(sessionID, dataRetentionConfiguration);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse stopDataRetention(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.stopDataRetention(sessionID);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseTeamLdapGroupMappingData getTeamLdapGroupsMapping(java.lang.String sessionId, java.lang.String teamId) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.getTeamLdapGroupsMapping(sessionId, teamId);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse setTeamLdapGroupsMapping(java.lang.String sessionId, java.lang.String teamId, jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSLdapGroupMapping[] ldapGroups) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.setTeamLdapGroupsMapping(sessionId, teamId, ldapGroups);
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSBasicRepsonse isValidProjectName(java.lang.String sessionID, java.lang.String projectName, java.lang.String groupId) throws java.rmi.RemoteException{
    if (cxSDKWebServiceSoap == null)
      _initCxSDKWebServiceSoapProxy();
    return cxSDKWebServiceSoap.isValidProjectName(sessionID, projectName, groupId);
  }
  
  
}