package jp.ubsecure.portal.jubjub.portlet.form.navigator.users.admin;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorCategory;
import com.liferay.users.admin.web.servlet.taglib.ui.UserIdentificationFormNavigatorCategory;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property = {
		PortalConstants.KEY_FORM_NAVIGATOR_CATEGORY_ORDER + PortalConstants.CATEGORY_ORDER_USER_IDENTIFICATION
	},
	service = FormNavigatorCategory.class
)
public class UserIdentificationFormNavigatorCategoryExt extends UserIdentificationFormNavigatorCategory {
	@Override
	public String getFormNavigatorId() {
		return PortalConstants.USERS_ADMIN_PREFIX + super.getFormNavigatorId();
	}
}