package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.CsvMetaSummaryEntry;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class CsvMetaSummaryEntryParser implements CSVEntryConverter<CsvMetaSummaryEntry> {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(CsvMetaSummaryEntryParser.class);
	
	@Override
	public String[] convertEntry(CsvMetaSummaryEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (entry == null) {
			params.put("entry", entry);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException(PortalErrors.FILE_PROCESS_ERROR);					
		}
		
		String[] columns = new String[15];
		columns[0] = entry.getCompanyName();
	    columns[1] = entry.getProjectName();
	    columns[2] = entry.getAttribute();
	    columns[3] = entry.getCaseNumber();
	    columns[4] = entry.getScanId();
	    columns[5] = entry.getExecutionDate();
	    columns[6] = entry.getProcess();
	    columns[7] = entry.getHighCount();
	    columns[8] = entry.getMediumCount();
	    columns[9] = entry.getLowCount();
	    columns[10] = entry.getInfoCount();
	    columns[11] = entry.getExecutionTime();
	    columns[12] = entry.getLOC();
	    columns[13] = entry.getPreset();
	    columns[14] = entry.getRequestReview();
	    
	    for (int index = 0; index < columns.length; index++) {
	    	if (columns[index] == null) {
	    		columns[index] = PortalConstants.STRING_EMPTY;
	    	}
	    }
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}
}
