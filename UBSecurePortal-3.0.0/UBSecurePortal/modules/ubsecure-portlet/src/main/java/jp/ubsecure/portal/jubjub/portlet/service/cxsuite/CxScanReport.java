package jp.ubsecure.portal.jubjub.portlet.service.cxsuite;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.googlecode.jcsv.CSVStrategy;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.StringUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectAttribute;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.CsvMetaSummaryData;
import jp.ubsecure.portal.jubjub.portlet.model.CsvMetaSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.model.CsvVulnSummaryData;
import jp.ubsecure.portal.jubjub.portlet.model.CsvVulnSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.model.IOSMetaSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.model.IOSVulnSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.ReportItem;
import jp.ubsecure.portal.jubjub.portlet.model.ResultItem;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.VexMetaSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.model.VexVulnSummaryEntry;
import jp.ubsecure.portal.jubjub.portlet.report.CsvMetaSummaryEntryParser;
import jp.ubsecure.portal.jubjub.portlet.report.CsvVulnSummaryEntryParser;
import jp.ubsecure.portal.jubjub.portlet.report.IOSMetaSummaryEntryParser;
import jp.ubsecure.portal.jubjub.portlet.report.IOSVulnSummaryEntryParser;
import jp.ubsecure.portal.jubjub.portlet.report.VexMetaSummaryEntryParser;
import jp.ubsecure.portal.jubjub.portlet.report.VexVulnSummaryEntryParser;
import jp.ubsecure.portal.jubjub.portlet.report.VulnerabilityMasterParser;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.YCloudUtil;

/**
 * Class for Report Generation of Results 
 * after scanning a source code File 
 * to CxSuite Source Code Diagnosis tool.
 * 
 * Reports are:  
 * 	1. XML report: REPORT.xml
 * 		- this is generated from CxSuite tool
 *  2. PDF report: REPORT.pdf		
 *  	- this is generated from CxSuite tool
 *  3. Vulnerability CSV: VULN.csv 
 *  	- this is generated based from XML report and Vulnerability Master File
 *  4. Scan meta-information CSV: META.csv 
 *  	- this is generated based from XML report in which 
 *  		each level of vulnerability (High, Medium, Low, Information) is counted
 *  		 and counts are stored in DB
 * 	
 * Once the report generation process is done, the report is stored in YCloud storage.
 * 					 
 * @author ASI558
 *
 */
public class CxScanReport extends DefaultHandler {
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(CxScanReport.class);	
	private static YCloudUtil ycloudUtil = YCloudUtil.getInstance();
	
	/**
	 * returns the PDF file of Cx Scan Report
	 * @param lProjectId the project id in Portal
	 * @param lScanId the scan id in Portal
	 * @param report the report value
	 * @return the byte array format of XML report file
	 * @throws UBSPortalException the custom exception for errors encountered while processing
	 */
	public static byte[] getReferenceInfoReport(long lProjectId, long lScanId, Report report) throws UBSPortalException{
		Map<String, Object> params = new HashMap<String, Object>();
		String strBucketName = null;
		String strFileName = null;
		byte[] referenceInfoReport = null;
		File reportFilePath = null;	
		String strReportFileCompleteName = null;
		FileInputStream fileInputStream = null;
		boolean bIsSuccess = false;
		String strNames[] = null;
		String strFile = null;
		Path downloadedReportFilePath = null;
		String downloadLocation = null;
		
		try {
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID, PortalErrors.CX_PROCESSING_EXCEPTION);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID, PortalErrors.CX_PROCESSING_EXCEPTION);
			}
			if (report == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_REPORT));
				throw new UBSPortalException(PortalErrors.INVALID_CX_REPORT_PARAMETER, PortalErrors.CX_PROCESSING_EXCEPTION);
			}
			strBucketName = report.getReportBucketName(); 
			strFileName =  report.getReportName();
			strNames = StringUtil.split(strFileName, File.separator);
			strFile = strNames[1];
			downloadLocation = strNames[0] + PortalConstants.STR_FWD_SLASH + strNames[1];
			
			reportFilePath = FileProcessUtil.createDownloadReferencePath(lProjectId, lScanId);
			
			if (reportFilePath.isDirectory()) {
				strReportFileCompleteName = reportFilePath.getAbsolutePath() + File.separator + strFile;
				
				bIsSuccess = ycloudUtil.downloadObject(strBucketName, downloadLocation , strReportFileCompleteName);
				
				if (bIsSuccess) {
					downloadedReportFilePath = Paths.get(strReportFileCompleteName);
					referenceInfoReport = Files.readAllBytes(downloadedReportFilePath);
					if (!CommonUtil.isValidBytes(referenceInfoReport)) {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
						log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
					}
					FileUtil.delete(strReportFileCompleteName);
				} else {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
					log.debug(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalConstants.METHOD_GET_ANALYZED_ZIPPPED_REPORT, params, new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT));
				}
			}
		} catch (IOException ioe) {
			params.put("fileInputStream", strReportFileCompleteName);
			params.put("getBytes", fileInputStream);
			log.debug(PortalErrors.CX_GENERATE_SCAN_REPORT_ERROR, PortalConstants.METHOD_GET_REFERENCE_INFO_REPORT, params, ioe);
			throw new UBSPortalException(PortalErrors.CX_GENERATE_SCAN_REPORT_ERROR, PortalErrors.CX_PROCESSING_EXCEPTION, ioe);
		} catch (UBSPortalException ubse) {
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("report", report);
			params.put("createDownloadReportPath", reportFilePath);
			params.put("downloadObject", bIsSuccess);
			params.put("strBucketName", strBucketName);
			params.put("strFileName", strFileName);
			params.put("strReportFileCompleteName", strReportFileCompleteName);
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_GET_REFERENCE_INFO_REPORT, params, ubse);
			throw ubse;
		} catch (Exception e) {
			params.put("fileInputStream", strReportFileCompleteName);
			params.put("getBytes", fileInputStream);
			params.put("lProjectId", lProjectId);
			params.put("lScanId", lScanId);
			params.put("report", report);
			params.put("createDownloadReportPath", reportFilePath);
			params.put("downloadObject", bIsSuccess);
			params.put("strBucketName", strBucketName);
			params.put("strFileName", strFileName);
			params.put("strReportFileCompleteName", strReportFileCompleteName);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_REFERENCE_INFO_REPORT, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			FileProcessUtil.deleteFilesScanProjectFolders(new File(strReportFileCompleteName).getParent());
			
			strBucketName = null;
			strFileName = null;
			reportFilePath = null;	
			fileInputStream = null;
			params.clear();
			params = null;
			ycloudUtil.close();
		}
		
		return referenceInfoReport;
	}
			
	/**
	 * regenerate Cx report in Portal
	 * @param reportList the list of report records in DB
	 * @param strRegistrationDate registration date in string format
	 * @param lCxAndroidProjectId the cx project id
	 * @param lCxAndroidScanId the cx scan id
	 * @param lProjectId the portal project id
	 * @param lScanId the portal scan id
	 * @param strSessionId the session id in CxSuite
	 * @return true/false if generation is successful or not
	 * @throws UBSPortalException the custom exception for errors encountered while processing
	 */
	public static void reGenerateCxReport(ResultItem requestedScanItem, String strSessionId, int projectType) throws UBSPortalException { 
		Map<String, Object> params = new HashMap<String, Object>();
		long lScanId = PortalConstants.LONG_ZERO;
			
		String cxScanId = null;
		long lProjectId = PortalConstants.LONG_ZERO;
		boolean isDeleted = false;
		
		try {
			if (requestedScanItem == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_PARAMETER);
			}
			
			if (CommonUtil.isStringNullOrEmpty(strSessionId)) { 
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SESSION_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_SESSION_ID);
			}
			
			lScanId = requestedScanItem.getScanId();
			cxScanId = requestedScanItem.getCxAndroidScanId();
			lProjectId = requestedScanItem.getProjectId();
			
			if (Long.valueOf(cxScanId) <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID);
			}
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			if (CommonUtil.isOutOfRange(requestedScanItem.getType(), 1, PortalConstants.PROJECT_TYPE_LIMIT)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_TYPE));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_TYPE);
			}
			
			log.info(PortalConstants.API_FUNCTION_GET_WEB_SERVICE_URL, PortalConstants.BEFORE + PortalConstants.API_CALL_CX_CONNECTION_CHECKING);
			if (CxScanManager.isConnectedToCx()) {
				isDeleted = ScanLocalServiceUtil.isDeletedScanInCxServer(lScanId);
					
				if (isDeleted) {
					params.put("isDeleted", isDeleted);
					params.put("lScanId", lScanId);
					params.put("cxScanId", cxScanId);
					params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_SCAN_ALREADY_DELETED_FROM_CXSUITE);
					
					if (projectType == ProjectType.CX_SUITE.getInteger()) {
						log.debug(PortalErrors.CX_SCAN_ID_IS_NOT_FOUND_IN_CX_SERVER, PortalConstants.METHOD_REGENERATE_CX_REPORT, params, new UBSPortalException(PortalErrors.REGENERATE_REPORT_WEB_SERVICE_API_FAILED, PortalMessages.CX_REGENERATE_REPORT_FAILED));
						throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_WEB_SERVICE_API_FAILED, PortalMessages.CX_REGENERATE_REPORT_FAILED);
					} else if (projectType == ProjectType.IOS.getInteger()) {
						log.debug(PortalErrors.CX_SCAN_ID_IS_NOT_FOUND_IN_CX_SERVER, PortalConstants.METHOD_REGENERATE_CX_REPORT, params, new UBSPortalException(PortalErrors.REGENERATE_REPORT_WEB_SERVICE_API_FAILED, PortalMessages.IOS_REGENERATE_REPORT_FAILED));
						throw new UBSPortalException(PortalErrors.REGENERATE_REPORT_WEB_SERVICE_API_FAILED, PortalMessages.IOS_REGENERATE_REPORT_FAILED);
					}
					
				} else {
					CxScanReportUpdater cxReportUpdater = new CxScanReportUpdater();
					cxReportUpdater.init(strSessionId, lScanId, Long.valueOf(cxScanId), lProjectId, true);
					cxReportUpdater.generatePDF();
					
					setRegenerationReports(lScanId);
				}
				
			}
			log.info(PortalConstants.API_FUNCTION_GET_WEB_SERVICE_URL, PortalConstants.AFTER + PortalConstants.API_CALL_CX_CONNECTION_CHECKING);
			
		} catch (NumberFormatException e) {
			params.put("cxScanId", cxScanId);
			log.debug(PortalErrors.CX_GENERATE_SCAN_REPORT_ERROR, PortalConstants.METHOD_REGENERATE_CX_REPORT, params, e);	
			if (projectType == ProjectType.CX_SUITE.getInteger()) {
				throw new UBSPortalException(PortalErrors.CX_GENERATE_SCAN_REPORT_ERROR, PortalMessages.CX_REGENERATE_REPORT_FAILED);
			} else if (projectType == ProjectType.IOS.getInteger()) {
				throw new UBSPortalException(PortalErrors.CX_GENERATE_SCAN_REPORT_ERROR, PortalMessages.IOS_REGENERATE_REPORT_FAILED);
			}
		} catch (UBSPortalException ubse) {
			if (ubse.getErrorCode().equals(PortalErrors.REGENERATE_REPORT_WEB_SERVICE_API_FAILED)) {
				String strErrorMessage = ubse.getErrorMessage();
				String strApiMessage = PortalConstants.STRING_EMPTY;
				
				if (projectType == ProjectType.CX_SUITE.getInteger()) {
					strApiMessage = PortalMessages.CX_REGENERATE_REPORT_FAILED;
				} else if (projectType == ProjectType.IOS.getInteger()) {
					strApiMessage = PortalMessages.IOS_REGENERATE_REPORT_FAILED;
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strErrorMessage) && !strErrorMessage.equals(strApiMessage)) {
					if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
						int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
						strErrorMessage = strApiMessage + strErrorMessage.substring(index);
					} else {
						strErrorMessage = strApiMessage + PortalConstants.STRING_SPACE_OPEN_PARENTHESIS + strErrorMessage + PortalConstants.STRING_CLOSE_PARENTHESIS;
					}
				} else {
					strErrorMessage = strApiMessage;
				}
				
				ubse.setErrorMessage(strErrorMessage);
			}
			params.put("requestedScanItem", requestedScanItem);
			params.put("strSessionId", strSessionId);
			params.put("cxScanId", cxScanId);
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			log.debug(ubse.getErrorCode(), PortalConstants.METHOD_REGENERATE_CX_REPORT, params, ubse);			
			throw ubse;
		} catch (Exception e) {
			params.put("requestedScanItem", requestedScanItem);
			params.put("strSessionId", strSessionId);
			params.put("cxScanId", cxScanId);
			params.put("lScanId", lScanId);
			params.put("lProjectId", lProjectId);
			log.debug(PortalErrors.CX_GENERATE_SCAN_REPORT_ERROR, PortalConstants.METHOD_REGENERATE_CX_REPORT, params, e);	
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.REGENERATE_REPORT_FAILED);
		} finally {
			strSessionId = null;
			params.clear();
			params = null;
		}		
		
	}
	
	private static void setRegenerationReports(long lScanId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Report> reportList = null;
		Scan scan = null;
		
		try {			
			scan = ScanLocalServiceUtil.getScan(lScanId);
			scan.setStatus(ScanStatus.REGENERATING.getInteger());
			scan.setModifiedDate(new Date());
			ScanLocalServiceUtil.updateScan(scan);
		} catch (PortalException e) {
			params.put("reportList", reportList);
			log.debug(PortalErrors.CX_REGENERATE_SCAN_REPORT_ERROR, "reGenerateCxReport", params, e);
			throw new UBSPortalException(PortalErrors.CX_SCAN_REPORT_ERROR, PortalErrors.CX_PROCESSING_EXCEPTION);
		} catch (SystemException e) {
			params.put("reportList", reportList);
			log.debug(PortalErrors.CX_REGENERATE_SCAN_REPORT_ERROR, "reGenerateCxReport", params, e);
			throw new UBSPortalException(PortalErrors.CX_SCAN_REPORT_ERROR, PortalErrors.CX_PROCESSING_EXCEPTION);
		} finally {
			params.clear();
			params = null;
		}
	}

	public static void generateCxVulnSummary(List<Object> reportList, String folderLocation, Date startDate, Date endDate) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File cxVulnSummaryCsvFile = null;
		CSVStrategy strategy = null;
		List<CsvVulnSummaryEntry> summaryCsvEntryList = new ArrayList<CsvVulnSummaryEntry>();
		List<CsvVulnSummaryData> summaryCsvDataList = new ArrayList<CsvVulnSummaryData>();
		int num = PortalConstants.INT_ONE;
		Writer fileWriter = null;
		CSVWriter<CsvVulnSummaryEntry> writer = null;
		List<Long> reportsList = new ArrayList<Long>();
		
		try {
			cxVulnSummaryCsvFile = new File(folderLocation
					+ File.separator
					+ PortalConstants.VULNCSV);
			
			new File(cxVulnSummaryCsvFile.getParent()).mkdirs();
			
			strategy = new CSVStrategy(',', '"', '\0', false, false);
			
			//set header
		    CsvVulnSummaryEntry entry = new CsvVulnSummaryEntry();
			entry.setNumber(PortalConstants.HEADER_NUMBER);
			entry.setGroupName(PortalConstants.HEADER_SUMMARY_GROUP_NAME);
			entry.setProjectName(PortalConstants.HEADER_PROJECT_NAME);
			entry.setCaseNumber(PortalConstants.HEADER_CASE_NUMBER);
			entry.setAttribute(PortalConstants.HEADER_ATTRIBUTE);
			entry.setScanId(PortalConstants.HEADER_SCAN_ID);
			entry.setProcess(PortalConstants.HEADER_SUMMARY_PROCESS);
			entry.setPreset(PortalConstants.HEADER_SUMMARY_PRESET);
			entry.setExecutionDate(PortalConstants.HEADER_SUMMARY_DIAGNOSIS_DATE);
			entry.setSeverity(PortalConstants.HEADER_SEVERITY);
			entry.setVulnerabilityName(PortalConstants.HEADER_VULN_NAME);
			entry.setQueryName(PortalConstants.HEADER_QUERY_NAME);
			entry.setInputName(PortalConstants.HEADER_NAME_INPUT);
			entry.setReportId(PortalConstants.HEADER_REPORTID);
			summaryCsvEntryList.add(entry);
			
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				for (Object object : reportList) {
					boolean bIsFirstReport = true;
					ReportItem report = (ReportItem) object;
					String reportName = PortalConstants.STRING_EMPTY;
					String[] reportNameArr = report.getReportName().replaceAll(Pattern.quote(PortalConstants.STRING_BACK_SLASH), PortalConstants.STR_FWD_SLASH).split(PortalConstants.STR_FWD_SLASH);
					reportName = (reportNameArr.length > 0) ? reportNameArr[1] : PortalConstants.STRING_EMPTY;
					File xmlReport = new File(folderLocation + File.separator + reportName);
					
					summaryCsvDataList = CxScanReport.getCxVulnSummaryData(xmlReport, startDate, endDate);
					
					long lScanId = report.getScanId();
					
					if (!CommonUtil.isListNullOrEmpty(reportsList)) {
						for (long scanId : reportsList) {
							if (scanId == lScanId) {
								bIsFirstReport = false;
								break;
							}
						}
					}
					
					if (bIsFirstReport) {
						reportsList.add(lScanId);
					}
					
					String strCaseNumber = report.getCaseNumber();
					String strGroupName = report.getGroupName();
					String strAttribute = PortalConstants.STRING_EMPTY;
					int iAttribute = report.getAttribute();
					
					switch (iAttribute) {
						case 1:
							strAttribute = ProjectAttribute.NEW.getString();
							break;
						
						case 2:
							strAttribute = ProjectAttribute.REGULAR.getString();
							break;
						
						default:
							break;
					}
					
					String strProcess = PortalConstants.STRING_EMPTY;
					int iProcess = report.getProcess();

					switch (iProcess) {
						case 1: {
							strProcess = PortalConstants.PROCESS_UT;
							break;
						}
						case 2: {
							strProcess = PortalConstants.PROCESS_ST;
							break;
						}
						default:{
							strProcess = PortalConstants.STRING_EMPTY;
							break;
						}
					}
					
					if (!CommonUtil.isListNullOrEmpty(summaryCsvDataList)) {
						for (CsvVulnSummaryData data :summaryCsvDataList) {
							if (data != null) {
								CsvVulnSummaryEntry vulnDataEntry = new CsvVulnSummaryEntry();
								vulnDataEntry.setNumber(String.valueOf(num));
								vulnDataEntry.setGroupName(strGroupName);
								vulnDataEntry.setProjectName(data.getProjectName());
								vulnDataEntry.setCaseNumber(strCaseNumber);
								vulnDataEntry.setAttribute(strAttribute);
								vulnDataEntry.setScanId(String.valueOf(lScanId));
								vulnDataEntry.setProcess(strProcess);
								vulnDataEntry.setPreset(data.getPreset());
								vulnDataEntry.setExecutionDate(data.getExecutionDate());
								vulnDataEntry.setSeverity(data.getSeverity());
								vulnDataEntry.setVulnerabilityName(VulnerabilityMasterParser.getVulnerabilityName(data.getQueryName()));//from master file
								vulnDataEntry.setQueryName(data.getQueryName());
								vulnDataEntry.setInputName(data.getInputName());
								vulnDataEntry.setReportId(String.valueOf(report.getReportId()));
								
								if (report.getReviewFlag() == 1 && !bIsFirstReport) {
									vulnDataEntry.setRequestReview(PortalConstants.AFTER_REVIEW);
								} else {
									vulnDataEntry.setRequestReview(PortalConstants.STR_SPACE);
								}
								
								summaryCsvEntryList.add(vulnDataEntry);
								num++;
							}
						}
					}
				}
			}
			
			if (!CommonUtil.canWrite(cxVulnSummaryCsvFile.getParent())) {
				params.put("cxVulnSummaryCsvFile", cxVulnSummaryCsvFile);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			if (num > PortalConstants.INT_ONE) {
				entry.setRequestReview(PortalConstants.HEADER_REQUEST_REVIEW);
				summaryCsvEntryList.set(0, entry);
			}
			
			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(cxVulnSummaryCsvFile),PortalConstants.SHIFT_JIS));
			writer = new CSVWriterBuilder<CsvVulnSummaryEntry>(fileWriter).strategy(strategy).entryConverter(new CsvVulnSummaryEntryParser()).build();			
			writer.writeAll(summaryCsvEntryList);
		} catch (UBSPortalException e) {
			params.put("reportList", reportList);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
			throw new UBSPortalException(e.getErrorCode(), PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (IOException e) {
			params.put("fileWriter", fileWriter);
			params.put("writer", writer);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (Exception e) {
			params.put("reportList", reportList);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			try {
				if (!CommonUtil.isObjectNull(fileWriter)) {
					fileWriter.close();
				}
				
				if (!CommonUtil.isObjectNull(writer)) {
					writer.close();
				}
			} catch (IOException e) {
				params.put("fileWriter", fileWriter);
				params.put("writer", writer);
				log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			} catch (Exception e) {
				log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			}
			
			summaryCsvEntryList = null;
			summaryCsvDataList = null;
			reportsList = null;
			params.clear();
			params = null;
		}
	}
	
	public static void generateCxMetaSummary(List<Object> reportList, String folderLocation, Date startDate, Date endDate) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File cxMetaSummaryCsvFile = null;
		Writer fileWriter = null;
		CsvMetaSummaryEntry entry = null;
		CsvMetaSummaryData data = null;
		CSVWriter<CsvMetaSummaryEntry> writer = null;
		List<CsvMetaSummaryEntry> summaryCsvEntryList = new ArrayList<CsvMetaSummaryEntry>();
		CSVStrategy strategy = null;
		List<Long> reportsList = new ArrayList<Long>();
		
		try {
			cxMetaSummaryCsvFile = new File(folderLocation
				+ File.separator
				+ PortalConstants.METACSV);
				
			new File(cxMetaSummaryCsvFile.getParent()).mkdirs();
			
			strategy = new CSVStrategy(',', '"', '\0', false, false);
			
			//set header
		    entry = new CsvMetaSummaryEntry();
			entry.setCompanyName(PortalConstants.HEADER_SUMMARY_COMPANY_NAME);
			entry.setProjectName(PortalConstants.HEADER_PROJECT_NAME);
			entry.setAttribute(PortalConstants.HEADER_ATTRIBUTE);
			entry.setCaseNumber(PortalConstants.HEADER_CASE_NUMBER);
			entry.setScanId(PortalConstants.HEADER_SCAN_ID);
			entry.setExecutionDate(PortalConstants.HEADER_SUMMARY_DIAGNOSIS_DATE);
			entry.setProcess(PortalConstants.HEADER_PROCESS);
			entry.setHighCount(PortalConstants.HEADER_HIGH);
			entry.setMediumCount(PortalConstants.HEADER_MED);
			entry.setLowCount(PortalConstants.HEADER_LOW);
			entry.setInfoCount(PortalConstants.HEADER_INFO);
			entry.setExecutionTime(PortalConstants.HEADER_SCAN_EXEC);
			entry.setLOC(PortalConstants.HEADER_LOC);
			entry.setPreset(PortalConstants.HEADER_PRESET);
			entry.setRequestReview(PortalConstants.HEADER_REQUEST_REVIEW);
			summaryCsvEntryList.add(entry);
			
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				//data
				for (Object object : reportList) {
					boolean bIsFirstReport = true;
					ReportItem report = (ReportItem) object;
					String reportName = PortalConstants.STRING_EMPTY;
					String[] reportNameArr = report.getReportName().replaceAll(Pattern.quote(PortalConstants.STRING_BACK_SLASH), PortalConstants.STR_FWD_SLASH).split(PortalConstants.STR_FWD_SLASH);
					reportName = (reportNameArr.length > 0) ? reportNameArr[1] : PortalConstants.STRING_EMPTY;
					File xmlReport = new File(folderLocation + File.separator + reportName);

					data = CxScanReport.getCxMetaSummaryData(xmlReport);
				
					long lScanId = report.getScanId();
					boolean bExist = false;
					
					if (!CommonUtil.isListNullOrEmpty(reportsList)) {
						for (long scanId : reportsList) {
							if (scanId == lScanId) {
								bExist = true;
								break;
							}
						}
					}
					
					if (bExist) {
						bIsFirstReport = false;
					} else {
						reportsList.add(lScanId);
					}
					
					int iAttribute = report.getAttribute();
					String strAttribute = PortalConstants.STRING_EMPTY;
					switch (iAttribute) {
						case 1:
							strAttribute = ProjectAttribute.NEW.getString();
							break;
							
						case 2:
							strAttribute = ProjectAttribute.REGULAR.getString();
							break;
						
						default:
							break;
					}
					
					int iProcess = report.getProcess();
					String strProcess = PortalConstants.STRING_EMPTY;
					
					switch (iProcess) {
						case 1:
							strProcess = PortalConstants.PROCESS_UT;
							break;
							
						case 2:
							strProcess = PortalConstants.PROCESS_ST;
							break;
							
						default:
							break;
					}
					
					entry = new CsvMetaSummaryEntry();
					entry.setCompanyName(report.getGroupName());
					entry.setProjectName(data.getProjectName());
					entry.setAttribute(strAttribute);
					entry.setCaseNumber(report.getCaseNumber());
					entry.setScanId(String.valueOf(lScanId));
					entry.setExecutionDate(data.getExecutionDate());
					entry.setProcess(strProcess);
					entry.setHighCount(data.getHighCount());
					entry.setMediumCount(data.getMediumCount());
					entry.setLowCount(data.getLowCount());
					entry.setInfoCount(data.getInfoCount());
					entry.setExecutionTime(data.getExecutionTime());
					entry.setLOC(data.getLOC());
					entry.setPreset(data.getPreset());
					
					if (report.getReviewFlag() == 1 && !bIsFirstReport) {
						entry.setRequestReview(PortalConstants.AFTER_REVIEW);
					}
					
					DateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
					Date dteStart = df.parse(data.getExecutionDate());
					
					if ((dteStart.after(startDate) && dteStart.before(endDate)) || (dteStart.equals(startDate) || dteStart.equals(endDate))) {
						summaryCsvEntryList.add(entry);
					}
				}
			}
			
			if (!CommonUtil.canWrite(cxMetaSummaryCsvFile.getParent())) {
				params.put("cxMetaSummaryCsvFile", cxMetaSummaryCsvFile);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(cxMetaSummaryCsvFile),PortalConstants.SHIFT_JIS));
			writer = new CSVWriterBuilder<CsvMetaSummaryEntry>(fileWriter).strategy(strategy).entryConverter(new CsvMetaSummaryEntryParser()).build();			
			writer.writeAll(summaryCsvEntryList);
		} catch (UBSPortalException e) {
			params.put("reportList", reportList);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw e;
		} catch (IOException e) {
			params.put("fileWriter", fileWriter);
			params.put("writer", writer);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (Exception e) {
			params.put("reportList", reportList);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			try {
				if (!CommonUtil.isObjectNull(fileWriter)) {
					fileWriter.close();
				}
				
				if (!CommonUtil.isObjectNull(writer)) {
					writer.close();
				}
			} catch (IOException e) {
				params.put("fileWriter", fileWriter);
				params.put("writer", writer);
				log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			} catch (Exception e) {
				log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			}
			
			params.clear();
			params = null;
			reportsList = null;
		}
	}
	
	public static void downloadXMLFromStorage (List<Object> reportList, String folderLocation) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strBucketName = PortalConstants.STRING_EMPTY;
		String strFileName = PortalConstants.STRING_EMPTY;
		String [] strNames = null;
		String strFile = PortalConstants.STRING_EMPTY;
		String strDownloadLocation = PortalConstants.STRING_EMPTY;
		File reportFilePath = null;
		String strReportFileCompleteName = PortalConstants.STRING_EMPTY;
		
		try {
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				for (Object object : reportList) {
					ReportItem report = (ReportItem) object;
					
					strBucketName = report.getBucketName(); 
					strFileName =  report.getReportName();
					strNames = StringUtil.split(strFileName, File.separator);
					strFile = strNames[1];
					strDownloadLocation = strNames[0] + PortalConstants.STR_FWD_SLASH + strFile;
					
					reportFilePath = new File(folderLocation);
					reportFilePath.mkdirs();
					
					if (reportFilePath.isDirectory()) {
						strReportFileCompleteName = reportFilePath.getAbsolutePath() + File.separator + strFile;
						ycloudUtil.downloadObject(strBucketName, strDownloadLocation, strReportFileCompleteName);
					}
				}
			}
		} catch (UBSPortalException e) {
			params.put("strBucketName", strBucketName);
			params.put("strDownloadLocation", strDownloadLocation);
			params.put("strReportFileCompleteName", strReportFileCompleteName);
			log.debug(e.getErrorCode(), "downloadXMLFromStorage", params, e);
			throw new UBSPortalException(e.getErrorCode(), PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}
	}
	
	public static List<CsvVulnSummaryData> getCxVulnSummaryData (File xmlReport, Date startDate, Date endDate) throws UBSPortalException {
		VulnerabilitySummaryHandler vulnSummaryHandler = null;
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			
			vulnSummaryHandler = new VulnerabilitySummaryHandler(startDate, endDate);
			parser.parse(xmlReport, vulnSummaryHandler);
		} catch (ParserConfigurationException e) {
			log.debug(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (SAXException e) {
			log.debug(PortalErrors.SAX_EXCEPTION, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.SAX_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (IOException e) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (FactoryConfigurationError e) {
			log.debug(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		}
		
		return vulnSummaryHandler.csvVulnSummaryDataList;
	}
		
	public static CsvMetaSummaryData getCxMetaSummaryData(File xmlReport) throws UBSPortalException {
		MetaSummaryHandler metaSummaryHandler = null;
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			
			metaSummaryHandler = new MetaSummaryHandler();
			parser.parse(xmlReport, metaSummaryHandler);
		} catch (ParserConfigurationException e) {
			log.debug(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalConstants.GET_CX_META_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.PARSE_CONFIGURATION_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (SAXException e) {
			log.debug(PortalErrors.SAX_EXCEPTION, PortalConstants.GET_CX_META_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.SAX_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (IOException e) {
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.GET_CX_META_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.GET_CX_META_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		} catch (FactoryConfigurationError e) {
			log.debug(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalConstants.METHOD_GET_CX_VULN_SUMMARY_DATA, null, e);
			throw new UBSPortalException(PortalErrors.FACTORY_CONFIGURATION_ERROR, PortalMessages.DOWNLOAD_SUMMARY_ERROR, e);
		}

		return metaSummaryHandler.csvMetaSummaryData;
	}
	
	public static void generateIOSVulnSummary(List<Object> reportList, String folderLocation, Date startDate, Date endDate) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File iOSVulnSummaryCsvFile = null;
		CSVStrategy strategy = null;
		List<IOSVulnSummaryEntry> summaryCsvEntryList = new ArrayList<IOSVulnSummaryEntry>();
		List<CsvVulnSummaryData> summaryCsvDataList = new ArrayList<CsvVulnSummaryData>();
		int num = PortalConstants.INT_ONE;
		Writer fileWriter = null;
		CSVWriter<IOSVulnSummaryEntry> writer = null;
		
		try {
			iOSVulnSummaryCsvFile = new File(folderLocation
					+ File.separator
					+ PortalConstants.VULNCSV);
			
			new File(iOSVulnSummaryCsvFile.getParent()).mkdirs();
			
			strategy = new CSVStrategy(',', '"', '\0', false, false);
			
			//set header
		    IOSVulnSummaryEntry entry = new IOSVulnSummaryEntry();
			entry.setNumber(PortalConstants.HEADER_NUMBER);
			entry.setGroupName(PortalConstants.HEADER_SUMMARY_GROUP_NAME);
			entry.setProjectName(PortalConstants.HEADER_PROJECT_NAME);
			entry.setCaseNumber(PortalConstants.HEADER_CASE_NUMBER);
			entry.setScanId(PortalConstants.HEADER_SCAN_ID);
			entry.setPreset(PortalConstants.HEADER_SUMMARY_PRESET);
			entry.setExecutionDate(PortalConstants.HEADER_SUMMARY_DIAGNOSIS_DATE);
			entry.setSeverity(PortalConstants.HEADER_SEVERITY);
			entry.setVulnerabilityName(PortalConstants.HEADER_VULN_NAME);
			entry.setQueryName(PortalConstants.HEADER_QUERY_NAME);
			entry.setInputName(PortalConstants.HEADER_NAME_INPUT);
			entry.setReportId(PortalConstants.HEADER_REPORTID);
			summaryCsvEntryList.add(entry);
			
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				for (Object object : reportList) {
					ReportItem report = (ReportItem) object;
					String reportName = PortalConstants.STRING_EMPTY;
					String[] reportNameArr = report.getReportName().replaceAll(Pattern.quote(PortalConstants.STRING_BACK_SLASH), PortalConstants.STR_FWD_SLASH).split(PortalConstants.STR_FWD_SLASH);
					reportName = (reportNameArr.length > 0) ? reportNameArr[1] : PortalConstants.STRING_EMPTY;
					File xmlReport = new File(folderLocation + File.separator + reportName);
					
					summaryCsvDataList = CxScanReport.getCxVulnSummaryData(xmlReport, startDate, endDate);
					
					long lScanId = report.getScanId();
					String strCaseNumber = report.getCaseNumber();
					String strGroupName = report.getGroupName();
					
					if (!CommonUtil.isListNullOrEmpty(summaryCsvDataList)) {
						for (CsvVulnSummaryData data :summaryCsvDataList) {
							if (data != null) {
								IOSVulnSummaryEntry vulnDataEntry = new IOSVulnSummaryEntry();
								vulnDataEntry.setNumber(String.valueOf(num));
								vulnDataEntry.setGroupName(strGroupName);
								vulnDataEntry.setProjectName(data.getProjectName());
								vulnDataEntry.setCaseNumber(strCaseNumber);
								vulnDataEntry.setScanId(String.valueOf(lScanId));
								vulnDataEntry.setPreset(data.getPreset());
								vulnDataEntry.setExecutionDate(data.getExecutionDate());
								vulnDataEntry.setSeverity(data.getSeverity());
								vulnDataEntry.setVulnerabilityName(VulnerabilityMasterParser.getVulnerabilityName(data.getQueryName()));//from master file
								vulnDataEntry.setQueryName(data.getQueryName());
								vulnDataEntry.setInputName(data.getInputName());
								vulnDataEntry.setReportId(String.valueOf(report.getReportId()));
								
								summaryCsvEntryList.add(vulnDataEntry);
								num++;
							}
						}
					}
				}
			}
			
			if (!CommonUtil.canWrite(iOSVulnSummaryCsvFile.getParent())) {
				params.put("iOSVulnSummaryCsvFile", iOSVulnSummaryCsvFile);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(iOSVulnSummaryCsvFile),PortalConstants.SHIFT_JIS));
			writer = new CSVWriterBuilder<IOSVulnSummaryEntry>(fileWriter).strategy(strategy).entryConverter(new IOSVulnSummaryEntryParser()).build();			
			writer.writeAll(summaryCsvEntryList);
		} catch (UBSPortalException e) {
			params.put("reportList", reportList);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
			throw new UBSPortalException(e.getErrorCode(), PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (IOException e) {
			params.put("fileWriter", fileWriter);
			params.put("writer", writer);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (Exception e) {
			params.put("reportList", reportList);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			try {
				if (!CommonUtil.isObjectNull(fileWriter)) {
					fileWriter.close();
				}
				
				if (!CommonUtil.isObjectNull(writer)) {
					writer.close();
				}
			} catch (IOException e) {
				params.put("fileWriter", fileWriter);
				params.put("writer", writer);
				log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			} catch (Exception e) {
				log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_VULN_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			}
			
			summaryCsvEntryList = null;
			summaryCsvDataList = null;
			params.clear();
			params = null;
		}
	}
	
	public static void generateIOSMetaSummary(List<Object> reportList, String folderLocation, Date startDate, Date endDate) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		File iOSMetaSummaryCsvFile = null;
		Writer fileWriter = null;
		IOSMetaSummaryEntry entry = null;
		CsvMetaSummaryData data = null;
		CSVWriter<IOSMetaSummaryEntry> writer = null;
		List<IOSMetaSummaryEntry> summaryCsvEntryList = new ArrayList<IOSMetaSummaryEntry>();
		CSVStrategy strategy = null;
		
		try {
			iOSMetaSummaryCsvFile = new File(folderLocation
				+ File.separator
				+ PortalConstants.METACSV);
				
			new File(iOSMetaSummaryCsvFile.getParent()).mkdirs();
			
			strategy = new CSVStrategy(',', '"', '\0', false, false);
			
			//set header
		    entry = new IOSMetaSummaryEntry();
			entry.setCompanyName(PortalConstants.HEADER_SUMMARY_COMPANY_NAME);
			entry.setProjectName(PortalConstants.HEADER_PROJECT_NAME);
			entry.setCaseNumber(PortalConstants.HEADER_CASE_NUMBER);
			entry.setScanId(PortalConstants.HEADER_SCAN_ID);
			entry.setExecutionDate(PortalConstants.HEADER_SUMMARY_DIAGNOSIS_DATE);
			entry.setHighCount(PortalConstants.HEADER_HIGH);
			entry.setMediumCount(PortalConstants.HEADER_MED);
			entry.setLowCount(PortalConstants.HEADER_LOW);
			entry.setInfoCount(PortalConstants.HEADER_INFO);
			entry.setExecutionTime(PortalConstants.HEADER_SCAN_EXEC);
			entry.setLOC(PortalConstants.HEADER_LOC);
			entry.setPreset(PortalConstants.HEADER_PRESET);
			summaryCsvEntryList.add(entry);
			
			if (!CommonUtil.isListNullOrEmpty(reportList)) {
				//data
				for (Object object : reportList) {
					ReportItem report = (ReportItem) object;
					String reportName = PortalConstants.STRING_EMPTY;
					String[] reportNameArr = report.getReportName().replaceAll(Pattern.quote(PortalConstants.STRING_BACK_SLASH), PortalConstants.STR_FWD_SLASH).split(PortalConstants.STR_FWD_SLASH);
					reportName = (reportNameArr.length > 0) ? reportNameArr[1] : PortalConstants.STRING_EMPTY;
					File xmlReport = new File(folderLocation + File.separator + reportName);
					
					data = CxScanReport.getCxMetaSummaryData(xmlReport);
				
					long lScanId = report.getScanId();
					
					entry = new IOSMetaSummaryEntry();
					entry.setCompanyName(report.getGroupName());
					entry.setProjectName(data.getProjectName());
					entry.setCaseNumber(report.getCaseNumber());
					entry.setScanId(String.valueOf(lScanId));
					entry.setExecutionDate(data.getExecutionDate());
					entry.setHighCount(data.getHighCount());
					entry.setMediumCount(data.getMediumCount());
					entry.setLowCount(data.getLowCount());
					entry.setInfoCount(data.getInfoCount());
					entry.setExecutionTime(data.getExecutionTime());
					entry.setLOC(data.getLOC());
					entry.setPreset(data.getPreset());
					
					DateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
					Date dteStart = df.parse(data.getExecutionDate());
					
					if ((dteStart.after(startDate) && dteStart.before(endDate)) || (dteStart.equals(startDate) || dteStart.equals(endDate))) {
						summaryCsvEntryList.add(entry);
					}
				}
			}
			
			if (!CommonUtil.canWrite(iOSMetaSummaryCsvFile.getParent())) {
				params.put("iOSMetaSummaryCsvFile", iOSMetaSummaryCsvFile);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_NO_WRITE_PERMISSION);
				throw new UBSPortalException(PortalErrors.NO_WRITE_PERMISSION);
			}
			
			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(iOSMetaSummaryCsvFile),PortalConstants.SHIFT_JIS));
			writer = new CSVWriterBuilder<IOSMetaSummaryEntry>(fileWriter).strategy(strategy).entryConverter(new IOSMetaSummaryEntryParser()).build();			
			writer.writeAll(summaryCsvEntryList);
		} catch (UBSPortalException e) {
			params.put("reportList", reportList);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw e;
		} catch (IOException e) {
			params.put("fileWriter", fileWriter);
			params.put("writer", writer);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (Exception e) {
			params.put("reportList", reportList);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			try {
				if (!CommonUtil.isObjectNull(fileWriter)) {
					fileWriter.close();
				}
				
				if (!CommonUtil.isObjectNull(writer)) {
					writer.close();
				}
			} catch (IOException e) {
				params.put("fileWriter", fileWriter);
				params.put("writer", writer);
				log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			} catch (Exception e) {
				log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_GENERATE_CX_META_SUMMARY, params, e);
				throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
			}
			
			params.clear();
			params = null;
		}
	}
}