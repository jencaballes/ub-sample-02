package jp.ubsecure.portal.jubjub.portlet;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.PwdGenerator;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.AndroidProjectMgmtController;
import jp.ubsecure.portal.jubjub.portlet.controller.AndroidScanMgmtController;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ReportType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsersItem;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.report.AndroidScanReport;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.android.AndroidProjectManager;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;
import jp.ubsecure.portal.util.AndroidResourceBasePath;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_PORTLET_DISPLAY_CATEGORY + PortalConstants.DISPLAY_CATEGORY_SAMPLE,
		PortalConstants.KEY_PORTLET_ICON + PortalConstants.ICON_ICON_PNG,
		PortalConstants.KEY_PORTLET_INSTANCEABLE + PortalConstants.INSTANCEABLE_FALSE,
		PortalConstants.KEY_PRIVATE_SESSION_ATTRIBUTES + PortalConstants.PRIVATE_SESSION_ATTRIBUTES_FALSE,
		PortalConstants.KEY_HEADER_PORTLET_CSS + PortalConstants.PORTLET_CSS_MAIN_CSS,
		PortalConstants.KEY_FOOTER_PORTLET_JAVASCRIPT + PortalConstants.PORTLET_JAVASCRIPT_JQUERY_JS + PortalConstants.COMMA
			+ PortalConstants.PORTLET_JAVASCRIPT_JQUERY_UI_JS + PortalConstants.COMMA
			+ PortalConstants.PORTLET_JAVASCRIPT_FILEDOWNLOAD_JS + PortalConstants.COMMA
			+ PortalConstants.PORTLET_JAVASCRIPT_BOOTSTRAP_JS + PortalConstants.COMMA
			+ PortalConstants.PORTLET_JAVASCRIPT_MAIN_JS,
		PortalConstants.KEY_PORTLET_CSS_CLASS_WRAPPER + PortalConstants.CSS_CLASS_WRAPPER_ANDROID_PORTLET,
		PortalConstants.KEY_PORTLET_DISPLAY_NAME + PortalConstants.DISPLAY_NAME_ANDROID_PORTLET,
		PortalConstants.KEY_INIT_VIEW_TEMPLATE + PortalConstants.ANDROID_PROJECT_LIST_JSP,
		PortalConstants.KEY_PORTLET_EXPIRATION_CACHE + PortalConstants.EXPIRATION_CACHE,
		PortalConstants.KEY_PORTLET_SUPPORTS_MIME_TYPE + PortalConstants.CONTENT_TYPE_TEXT_HTML,
		PortalConstants.KEY_PORTLET_RESOURCE_BUNDLE + PortalConstants.RESOURCE_BUNDLE_LANGUAGE_JP,
		PortalConstants.KEY_PORTLET_INFO_TITLE + PortalConstants.INFO_TITLE_ANDROID_PORTLET,
		PortalConstants.KEY_PORTLET_INFO_SHORT_TITLE + PortalConstants.INFO_SHORT_TITLE_ANDROID,
		PortalConstants.KEY_SECURITY_ROLE_REF + PortalConstants.ROLE_ADMINISTRATOR + PortalConstants.COMMA
			+ PortalConstants.ROLE_GUEST + PortalConstants.COMMA
			+ PortalConstants.ROLE_POWER_USER + PortalConstants.COMMA
			+ PortalConstants.ROLE_USER
	},
	service = Portlet.class
)
public class AndroidPortlet extends MVCPortlet {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(AndroidPortlet.class);
	
	/**
	 * Renders the init view template for Android which is the Android project list screen
	 */
	@Override
	public void doView (RenderRequest renderRequest, RenderResponse renderResponse) {
		renderRequest.setAttribute(PortalConstants.PARAM_VIEW_PROJECT_LIST, true);
		PortletSession pSession = renderRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(renderRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		String strAction = ParamUtil.getString(renderRequest, PortalConstants.ACTION_JAVAX_PORTLET);
		String strCur = ParamUtil.getString(renderRequest, PortalConstants.PARAM_CUR);
		
		if (!renderRequest.getParameterNames().hasMoreElements()) {
			pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_COL);
			pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
		}
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		session.removeAttribute(PortalConstants.PARAM_USER_ACTION);
		
		if (!strAction.equalsIgnoreCase(PortalConstants.ACTION_SEARCH_PROJECTS) && CommonUtil.isStringNullOrEmpty(strCur)) {
			if (pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH) != null) {
				pSession.removeAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_PROJECT_LIST, lUserId);
		
		try {
			super.doView(renderRequest, renderResponse);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	@Override
	public void processAction (ActionRequest actionRequest, ActionResponse actionResponse) {
		String action = ParamUtil.getString(actionRequest, PortalConstants.ACTION_JAVAX_PORTLET);
		PortletSession pSession = actionRequest.getPortletSession();
		int iCurrentScreen = PortalConstants.INT_ZERO;
		int iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		
		try {
			iCurrentScreen = getCurrentScreen(action, iScreenNo);
			
			if (iCurrentScreen > PortalConstants.INT_ZERO) {
				pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, iCurrentScreen);
			}
			
			Object oPrevScreen = pSession.getAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN);
			Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
			String strPrevScreen = PortalConstants.STRING_EMPTY;
			String strCurrScreen = PortalConstants.STRING_EMPTY;
			
			if (!CommonUtil.isObjectNull(oPrevScreen)) {
				strPrevScreen = oPrevScreen.toString();
			}
			
			if (!CommonUtil.isObjectNull(oCurrScreen)) {
				strCurrScreen = oCurrScreen.toString();
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strPrevScreen)
					&& !CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
				if (!strPrevScreen.equalsIgnoreCase(strCurrScreen)) {
					pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
					pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_COL);
					pSession.removeAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
				}
			}
			
			super.processAction(actionRequest, actionResponse);
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		} catch (Exception e) {
		}
	}
	
	public void serveResource (ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		int iUserAction = 0;
		int iScreenNo = 0;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		JSONArray jsonArray = null;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		try {
			iUserAction = ParamUtil.getInteger(originalRequest, PortalConstants.PARAM_USER_ACTION);
			iScreenNo = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_SCREEN_NO);
			
			if (iUserAction == 0) {
				iUserAction = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_USER_ACTION);
			}
			
			if (iUserAction == PortalConstants.USER_EVENT_SELECT_GROUP) { // on change owner group
				this.onChangeOwnerGroup(originalRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_FILTER_USERS) {
				this.filterUsers(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_ADD_USER) {
				this.addUser(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_REMOVE_USER) {
				this.removeUser(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_REPORT) { // download report
		        log.info(PortalMessages.USER_EVENT_DOWNLOAD_REPORT, lUserId);
				this.downloadReport(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_UPDATE_ACTION) {
				switch (iScreenNo) {
					case PortalConstants.SCREEN_SCAN_LIST:
						AndroidScanMgmtController.getScansForRefresh(resourceRequest, resourceResponse, getPortletConfig());	
						break;
					case PortalConstants.SCREEN_ENTIRE_SCAN_LIST:
						AndroidScanMgmtController.getEntireScansForRefresh(resourceRequest, resourceResponse, getPortletConfig());	
						break;
					default:
						break;
				}
			} else if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_SUMMARY) {
				log.info(PortalMessages.USER_EVENT_DOWNLOAD_SUMMARY, lUserId);
				this.downloadSummary(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_PROJECT_LIST) {
				log.info(PortalMessages.USER_EVENT_DOWNLOAD_SEARCHED_PROJECTS, lUserId);
				this.downloadProjectList(resourceRequest, resourceResponse);
			}
		} catch (UBSPortalException e) {
			
			if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_REPORT
					|| iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_SUMMARY
					|| iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_PROJECT_LIST) {
				jsonArray = JSONFactoryUtil.createJSONArray();
				try {
					String userAgent = request.getHeader("user-agent").toLowerCase();
					
					if (userAgent.contains("msie")
							|| (!userAgent.contains("firefox") && !userAgent.contains("chrome"))) {
						resourceResponse.setContentType("text/html");
					} else {
						resourceResponse.setContentType("application/json");
					}
					PrintWriter writer = resourceResponse.getWriter();
					
					jsonArray.put(false);
					writer.print(jsonArray);
					resourceResponse.flushBuffer();
					writer.close();
				} catch (IOException ioe) {
					log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_SERVE_RESOURCE, null, ioe);
				}
			}
			
			if (iUserAction != PortalConstants.USER_EVENT_UPDATE_ACTION) {
				if (SessionErrors.isEmpty(resourceRequest)) {
					SessionErrors.add(resourceRequest, e.getErrorMessage());
					SessionMessages.add(resourceRequest, PortalUtil.getPortletId(resourceRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				}
			} else {
				String strErrorCode = e.getErrorCode();
				
				if (strErrorCode.equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					resourceRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(resourceRequest)) {
						SessionErrors.add(resourceRequest, strErrorCode);
					}
				} else {
					if (!strErrorCode.equals(PortalErrors.ORM_EXCEPTION)) {
						
						if (!ControllerHelper.isUserInputError(strErrorCode)) {
							if (SessionErrors.isEmpty(resourceRequest)) {
								SessionErrors.add(resourceRequest, e.getErrorMessage());
							}
						}
					}
				}
			}
		}
	}
	
	private int getCurrentScreen (String action, int iScreenNo) {
		int iCurrentScreen = iScreenNo;
		
		if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_PROJECT_LIST)
				|| action.equalsIgnoreCase(PortalConstants.ACTION_ADD_PROJECT)
				|| action.equalsIgnoreCase(PortalConstants.ACTION_UPDATE_PROJECT)) {
			iCurrentScreen = PortalConstants.SCREEN_PROJECT_LIST;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_SCAN_LIST)
				|| action.equalsIgnoreCase(PortalConstants.ACTION_ADD_SCAN)) {
			iCurrentScreen = PortalConstants.SCREEN_SCAN_LIST;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST)) {
			iCurrentScreen = PortalConstants.SCREEN_ENTIRE_SCAN_LIST;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_EDIT_PROJECT)) {
			iCurrentScreen = PortalConstants.SCREEN_PROJECT_REGISTRATION;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_SCAN_REGISTRATION)) {
			iCurrentScreen = PortalConstants.SCREEN_SCAN_REGISTRATION;
		}
		
		return iCurrentScreen;
	}
	
	public void viewProjectList (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_PROJECT_LIST, lUserId);
	
		this.getProjects(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_PROJECT_LIST);
	}
	
	public void viewEditProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		List<Organization> organizationList = new ArrayList<Organization>();
		Object oUserRole = null;
		int iUserRole = 0;
		int iUserAction = 0;
		Project project = null;
		List<ProjectUsersItem> projectUsersList = new ArrayList<ProjectUsersItem>();
		List<User> orgUsersList = new ArrayList<User>();
		boolean bIsProjectUser = false;
		List<User> userList = new ArrayList<User>();
		List<String> packageNamesList = new ArrayList<String>();
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (iUserAction == PortalConstants.USER_EVENT_VIEW_CREATE_PROJECT) {
			log.info(PortalMessages.USER_EVENT_VIEW_CREATE_PROJECT, lUserId);
		} else {
			log.info(PortalMessages.USER_EVENT_VIEW_PROJECT_CHANGE, lUserId);
		}
		
		try {
			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			iUserAction = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_USER_ACTION);

			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			if (iUserAction == PortalConstants.USER_EVENT_VIEW_PROJECT_CHANGE) {
				project = AndroidProjectMgmtController.getProject(actionRequest);
				
				if (!CommonUtil.isObjectNull(project)) {
					packageNamesList = AndroidProjectManager.getExcludedPackageNames(Long.parseLong(project.getCxAndroidProjectId()));
					
					projectUsersList = AndroidProjectMgmtController.getProjectUsers(actionRequest);
					orgUsersList = ControllerHelper.getOrganizationUsers(project.getOwnerGroup());
					
					for (User user : orgUsersList) {
						bIsProjectUser = false;
						for (ProjectUsersItem projectUsers : projectUsersList) {
							if (user.getUserId() == projectUsers.getUserId()) {
								bIsProjectUser = true;
								break;
							}
						}
						
						if (!bIsProjectUser) {
							user.setFirstName(HtmlUtil.escapeAttribute(user.getFirstName()));
							userList.add(user);
						}
					}
				}
			}
			
			if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
				organizationList = ControllerHelper.getOrganizations(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
			} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
				organizationList = ControllerHelper.getUserOrganizations(lUserId, PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
			}
			
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_ORGANIZATION_LIST, organizationList);
			actionRequest.setAttribute(PortalConstants.PARAM_ORG_USERS_LIST, userList);
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_USERS_LIST, projectUsersList);
			actionRequest.setAttribute(PortalConstants.PARAM_PACKAGE_NAMES_LIST, packageNamesList);
			
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_PROJECT_REGISTRATION_JSP);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
			
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				if (iUserAction == PortalConstants.USER_EVENT_VIEW_CREATE_PROJECT) {
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_PROJECT_REGISTRATION_JSP);
				} else {
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_PROJECT_LIST_JSP);
				}
			} else if (ubspe.getErrorCode().equals(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.UPDATE_PROJECT_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.INVALID_PROJECT_ID) ||
					ubspe.getErrorCode().equals(PortalErrors.NO_SUCH_PROJECT_EXCEPTION) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else if (ubspe.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.ANDROID_EDIT_PROJECT_ONCLICK_ERROR, true);
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_PROJECT_REGISTRATION_JSP);
			}
		}
	}
	
	public void addProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		DateFormat formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
		Date projectEndDate = null;
		UploadPortletRequest uploadRequest = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_ADD_PROJECT, lUserId);
		
		try {
			uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			
			this.executeProjectAction(actionRequest, actionResponse, uploadRequest, PortalConstants.USER_EVENT_ADD_PROJECT);
		} catch (UBSPortalException e) {
			Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			int iUserRole = 0;
			int iFieldNumber = 0;
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			iFieldNumber = this.getFieldNumber(e.getErrorCode(), PortalConstants.USER_EVENT_ADD_PROJECT);
			
			try {
				String caseNumber = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_CASE_NUMBER).trim();
				String projectName = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
				long ownerGroup = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_OWNER_GROUP);
				String packageNames = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_LIBRARY_SETTING);
				String selectedUsers = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_SELECTED_USERS);
				String [] selectedUserArr = selectedUsers.split(PortalConstants.DELIMITER_COMMA);
				String [] packageNamesArr = packageNames.split(PortalConstants.DELIMITER_NEXT_LINE);
				List<String> packageNamesList = new ArrayList<String>();
				List<Organization> organizationList = new ArrayList<Organization>();
				List<ProjectUsersItem> projectUsersList = new ArrayList<ProjectUsersItem>();
				List<User> userList = new ArrayList<User>();
				List<User> orgUsersList = new ArrayList<User>();
				boolean bIsProjectUser = false;
				String strDate = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_PROJECT_END_DATE);
				String strUserListSortOrder = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_USER_LIST_SORT_ORDER);
				String strAvailableUsersSortOrder = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
				
				if (!CommonUtil.isStringNullOrEmpty(strDate) && !strDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					projectEndDate = formatter.parse(strDate);
				}
				
				Project project = ProjectLocalServiceUtil.createProjectObj();
				project.setCaseNumber(caseNumber);
				project.setProjectName(projectName);
				project.setOwnerGroup(ownerGroup);
				project.setProjectEndDate(projectEndDate);
				
				for (int i = 0; i < packageNamesArr.length; i++) {
					packageNamesList.add(packageNamesArr[i]);
				}
				
				if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() && !e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					organizationList = ControllerHelper.getOrganizations(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
				} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger() && !e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					organizationList = ControllerHelper.getUserOrganizations(lUserId, PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
				} 
				
				if (ownerGroup != PortalConstants.LONG_ZERO && !e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					orgUsersList = ControllerHelper.getOrganizationUsers(ownerGroup);
				}
				
				for (int i = 0; (i < selectedUserArr.length && (!CommonUtil.isStringNullOrEmpty(selectedUsers))); i++) {
					for (User user : orgUsersList) {
						if (user.getUserId() == Long.parseLong(selectedUserArr[i])) {
							ProjectUsersItem item = new ProjectUsersItem();
							
							item.setUserId(user.getUserId());
							item.setUserName(HtmlUtil.escapeAttribute(user.getFirstName()));
							item.setEmailAddress(user.getEmailAddress());
							
							projectUsersList.add(item);
							break;
						}
					}
				}
				
				if (orgUsersList != null) {
					for (User user : orgUsersList) {
						bIsProjectUser = false;
						for (ProjectUsersItem projectUsers : projectUsersList) {
							if (user.getUserId() == projectUsers.getUserId()) {
								bIsProjectUser = true;
								break;
							}
						}
						
						if (!bIsProjectUser) {
							user.setFirstName(HtmlUtil.escapeAttribute(user.getFirstName()));
							userList.add(user);
						}
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strUserListSortOrder)) {
					List<ProjectUsersItem> sortableUsers = new ArrayList<ProjectUsersItem>(projectUsersList);
					
					Collections.sort(sortableUsers, new Comparator<ProjectUsersItem>() {
						@Override
						public int compare(ProjectUsersItem u1, ProjectUsersItem u2) {
							int iCompare = 0;
							
							iCompare = u1.getUserName().compareToIgnoreCase(u2.getUserName());
							
							if (iCompare == 0) {
								iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
							}
							
							return iCompare;
						}
					});
					
					if (strUserListSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
						Collections.reverse(sortableUsers);
					}
					
					projectUsersList = new ArrayList<ProjectUsersItem>();
					projectUsersList = sortableUsers;
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
					List<User> sortableUsers = new ArrayList<User>(userList);
					
					Collections.sort(sortableUsers, new Comparator<User>() {
						@Override
						public int compare(User u1, User u2) {
							int iCompare = 0;
							
							iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
							
							if (iCompare == 0) {
								iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
							}
							
							return iCompare;
						}
					});
					
					if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
						Collections.reverse(sortableUsers);
					}
					
					userList = new ArrayList<User>();
					userList = sortableUsers;
				}
				
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionRequest.setAttribute(PortalConstants.PARAM_PACKAGE_NAMES_LIST, packageNamesList);
				actionRequest.setAttribute(PortalConstants.PARAM_ORGANIZATION_LIST, organizationList);
				actionRequest.setAttribute(PortalConstants.PARAM_ORG_USERS_LIST, userList);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_USERS_LIST, projectUsersList);
				actionRequest.setAttribute(PortalConstants.PARAM_FIELD_NUMBER, iFieldNumber);
				actionRequest.setAttribute(PortalConstants.PARAM_USER_LIST_SORT_ORDER, strUserListSortOrder);
				actionRequest.setAttribute(PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER, strAvailableUsersSortOrder);
				
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_PROJECT_REGISTRATION_JSP);
			} catch (UBSPortalException e1) {
				if (e1.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_PROJECT_REGISTRATION_JSP);
				}
			} catch (ParseException e1) {
				
			}
		}
	}
	
	public void updateProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		DateFormat formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
		Date projectEndDate = null;
		int iUserRole = 0;
		UploadPortletRequest uploadRequest = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_UPDATE_PROJECT, lUserId);

		try {
			uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			
			this.executeProjectAction(actionRequest, actionResponse, uploadRequest, PortalConstants.USER_EVENT_UPDATE_PROJECT);
		} catch (UBSPortalException e) {
			Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			iUserRole = 0;
			int iFieldNumber = 0;
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}

			if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			iFieldNumber = this.getFieldNumber(e.getErrorCode(), PortalConstants.USER_EVENT_UPDATE_PROJECT);
			
			if (e.getErrorCode().equals(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.UPDATE_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.UPDATE_PROJECT_PROJECT_TYPE_INVALID)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				try {
					long projectId = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_PROJECT_ID);
					String caseNumber = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_CASE_NUMBER).trim();
					String projectName = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
					long ownerGroup = ParamUtil.getLong(uploadRequest, PortalConstants.PARAM_OWNER_GROUP);
					boolean bNewFile = ParamUtil.getBoolean(uploadRequest, PortalConstants.PARAM_NEW_FILE);
					String fileName = null;
					if (!bNewFile) {
						fileName = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_FILE_NAME);
					}
					
					String packageNames = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_LIBRARY_SETTING);
					String selectedUsers = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_SELECTED_USERS);
					String [] selectedUserArr = selectedUsers.split(PortalConstants.DELIMITER_COMMA);
					String [] packageNamesArr = packageNames.split(PortalConstants.DELIMITER_NEXT_LINE);
					List<String> packageNamesList = new ArrayList<String>();
					List<Organization> organizationList = new ArrayList<Organization>();
					List<ProjectUsersItem> projectUsersList = new ArrayList<ProjectUsersItem>();
					List<User> userList = new ArrayList<User>();
					List<User> orgUsersList = new ArrayList<User>();
					boolean bIsProjectUser = false;
					String strDate = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_PROJECT_END_DATE);
					String strUserListSortOrder = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_USER_LIST_SORT_ORDER);
					String strAvailableUsersSortOrder = ParamUtil.getString(uploadRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
					
					if (!CommonUtil.isStringNullOrEmpty(strDate) && !strDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						projectEndDate = formatter.parse(strDate);
					}
					
					Project project = ProjectLocalServiceUtil.createProjectObj();
					project.setProjectId(projectId);
					project.setCaseNumber(caseNumber);
					project.setProjectName(projectName);
					project.setOwnerGroup(ownerGroup);
					project.setProjectEndDate(projectEndDate);
					project.setChecklistFileName(fileName);
					
					for (int i = 0; i < packageNamesArr.length; i++) {
						packageNamesList.add(packageNamesArr[i]);
					}
					
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() && !e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						organizationList = ControllerHelper.getOrganizations(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger() && !e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						organizationList = ControllerHelper.getUserOrganizations(lUserId, PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
					}
					
					if (ownerGroup != 0L && !e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						orgUsersList = ControllerHelper.getOrganizationUsers(ownerGroup);
					}
					
					for (int i = 0; (i < selectedUserArr.length && (selectedUsers != null && !selectedUsers.equals(""))); i++) {
						for (User user : orgUsersList) {
							if (user.getUserId() == Long.parseLong(selectedUserArr[i])) {
								ProjectUsersItem item = new ProjectUsersItem();
								
								item.setUserId(user.getUserId());
								item.setUserName(HtmlUtil.escapeAttribute(user.getFirstName()));
								item.setEmailAddress(user.getEmailAddress());
								
								projectUsersList.add(item);
								break;
							}
						}
					}
					
					if (orgUsersList != null) {
						for (User user : orgUsersList) {
							bIsProjectUser = false;
							for (ProjectUsersItem projectUsers : projectUsersList) {
								if (user.getUserId() == projectUsers.getUserId()) {
									bIsProjectUser = true;
									break;
								}
							}
							
							if (!bIsProjectUser) {
								user.setFirstName(HtmlUtil.escapeAttribute(user.getFirstName()));
								userList.add(user);
							}
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strUserListSortOrder)) {
						List<ProjectUsersItem> sortableUsers = new ArrayList<ProjectUsersItem>(projectUsersList);
						
						Collections.sort(sortableUsers, new Comparator<ProjectUsersItem>() {
							@Override
							public int compare(ProjectUsersItem u1, ProjectUsersItem u2) {
								int iCompare = 0;
								
								iCompare = u1.getUserName().compareToIgnoreCase(u2.getUserName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strUserListSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						projectUsersList = new ArrayList<ProjectUsersItem>();
						projectUsersList = sortableUsers;
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
						List<User> sortableUsers = new ArrayList<User>(userList);
						
						Collections.sort(sortableUsers, new Comparator<User>() {
							@Override
							public int compare(User u1, User u2) {
								int iCompare = 0;
								
								iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						userList = new ArrayList<User>();
						userList = sortableUsers;
					}

					actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
					actionRequest.setAttribute(PortalConstants.PARAM_PACKAGE_NAMES_LIST, packageNamesList);
					actionRequest.setAttribute(PortalConstants.PARAM_ORGANIZATION_LIST, organizationList);
					actionRequest.setAttribute(PortalConstants.PARAM_ORG_USERS_LIST, userList);
					actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_USERS_LIST, projectUsersList);
					actionRequest.setAttribute(PortalConstants.PARAM_FIELD_NUMBER, iFieldNumber);
					actionRequest.setAttribute(PortalConstants.PARAM_USER_LIST_SORT_ORDER, strUserListSortOrder);
					actionRequest.setAttribute(PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER, strAvailableUsersSortOrder);
					
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_PROJECT_REGISTRATION_JSP);
				} catch (UBSPortalException ubspe1) {
					if (ubspe1.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, ubspe1.getErrorMessage());
						
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, ubspe1.getErrorCode());
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, ubspe1.getErrorMessage());
						}
					}
				} catch (ParseException pe1) {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.PROJECT_END_DATE_INVALID);
					}
				}
			}
		}
	}
	
	public void searchProjects (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_SEARCH_PROJECT, lUserId);
		
		this.getProjects(actionRequest, actionResponse, PortalConstants.USER_EVENT_SEARCH_PROJECT);
	}
	
	public void clearFilterProjects (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_CLEAR_SEARCH_PROJECT, lUserId);
		
		this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
	}
	
	public void deleteProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_DELETE_PROJECT, lUserId);
		
		try {
			this.executeProjectAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_DELETE_PROJECT);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorMessage());
					}
				}
			}
			
			if (ubspe.getErrorCode().equals(PortalErrors.DELETE_PROJECT_USER_NO_RIGHTS)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			}
			
			JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
			
			if (!ubspe.getErrorCode().equals(PortalErrors.DELETE_PROJECT_USER_NO_RIGHTS)) {
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(jsonArray);
					response.flushBuffer();
				} catch (IOException e1) {
					
				}
			}
		}
	}
	
	public void completeProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		PortletSession pSession = actionRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_COMPLETE_PROJECT, lUserId);
		
		try {
			this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_COMPLETE_PROJECT);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.COMPLETE_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION) ||
					e.getErrorCode().equals(PortalErrors.COMPLETE_PROJECT_USER_NO_RIGHTS)) {
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put("redirect", true);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType("application/json");
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException ioe) {
					
				}
			}
		}
	}
	
	public void openProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		PortletSession pSession = actionRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_OPEN_PROJECT, lUserId);
		
		try {
			this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_OPEN_PROJECT);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.OPEN_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.OPEN_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION) ||
					e.getErrorCode().equals(PortalErrors.OPEN_PROJECT_USER_NO_RIGHTS)) {
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put("redirect", true);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType("application/json");
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException ioe) {
					
				}
			}
		}
	}
	
	public void viewScanList (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_SCAN_LIST, lUserId);
		
		try {
			Object oEditOnClickError = actionRequest.getPortletSession().getAttribute(PortalConstants.ANDROID_EDIT_PROJECT_ONCLICK_ERROR);
			if(oEditOnClickError != null && oEditOnClickError.equals(true)){
				String errorMsg = (String) actionRequest.getPortletSession().getAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG);
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, errorMsg, PortletSession.APPLICATION_SCOPE);
			}
			this.getScans(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_SCAN_LIST);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorMessage());
					}
				}
			}
			
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
		}
	}
	
	public void viewEntireScanList (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_ENTIRE_SCAN_LIST, lUserId);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_VIEW_ENTIRE_SCAN_LIST, true);
		
		int iAction = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_USER_ACTION);
		
		if (iAction == PortalConstants.USER_EVENT_VIEW_ENTIRE_SCAN_LIST) {
			session.removeAttribute(PortalConstants.PARAM_USER_ACTION);
		}
		
		try {
			this.getEntireScans(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_ENTIRE_SCAN_LIST);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_ENTIRE_SCAN_LIST_JSP);
			} else {
				if (ubspe.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, ubspe.getErrorMessage());
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorMessage());
					}
				}
	
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			}
		}
	}
	
	public void viewScanRegistration (ActionRequest actionRequest, ActionResponse actionResponse) {
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_CREATE_SCAN, lUserId);
		
		try {
			project = AndroidProjectMgmtController.getProject(actionRequest);
			
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_SCAN_REGISTRATION_JSP);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_SCAN_REGISTRATION_JSP);
			}
		}
	}
	
	public void addScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		Project project = null;
		UploadPortletRequest uploadRequest = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_ADD_SCAN, lUserId);
		
		try {
			uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			
			this.executeScanAction(actionRequest, actionResponse, uploadRequest, PortalConstants.USER_EVENT_ADD_SCAN);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}

			int iFieldNumber = 0;
			
			iFieldNumber = this.getFieldNumber(ubspe.getErrorCode(), PortalConstants.USER_EVENT_ADD_SCAN);
			
			if (ubspe.getErrorCode().equals(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.ADD_SCAN_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.ADD_SCAN_PROJECT_TYPE_INVALID)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				try {
					if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						project = AndroidProjectMgmtController.getProject(actionRequest);
					}
				
					actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
					actionRequest.setAttribute(PortalConstants.PARAM_FIELD_NUMBER, iFieldNumber);
					
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_SCAN_REGISTRATION_JSP);	
				} catch (UBSPortalException e) {
					if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
						
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, e.getErrorCode());
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, e.getErrorMessage());
						}
					}
				}
			}
		}
	}
	
	public void searchScans (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = 0;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_SEARCH_SCAN, lUserId);
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.getEntireScans(actionRequest, actionResponse, PortalConstants.USER_EVENT_SEARCH_SCAN);
			} catch (UBSPortalException e) {
				PortletSession pSession = actionRequest.getPortletSession();
				
				if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					pSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage(), PortletSession.APPLICATION_SCOPE);
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
					
					if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST);
					}
				}
			}
		} else {
			try {
				this.getScans(actionRequest, actionResponse, PortalConstants.USER_EVENT_SEARCH_SCAN);
			} catch (UBSPortalException e) {
				PortletSession pSession = actionRequest.getPortletSession();
				
				if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					pSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage(), PortletSession.APPLICATION_SCOPE);
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
					
					if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
					}
				}
			}
		}
	}
	
	public void clearFilterScans (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		int iScreenNo = PortalConstants.INT_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		
		log.info(PortalMessages.USER_EVENT_CLEAR_SEARCH_SCAN, lUserId);
		
		actionRequest.setAttribute(PortalConstants.PARAM_USER_ACTION, PortalConstants.USER_EVENT_CLEAR_SEARCH_SCAN);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST);
		} else {
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
		}
	}
	
	public void stopScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = 0;
		PortletSession pSession = actionRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				pSession.setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_STOP_SCAN, lUserId);
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		pSession.setAttribute(PortalConstants.PARAM_IS_FROM_STOP_SCAN, true);
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.executeEntireScanAction(actionRequest, actionResponse, PortalConstants.USER_EVENT_STOP_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					pSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.STOP_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.STOP_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
						writer.flush();
						writer.close();
					} catch (IOException e1) {
					
					}
				}

			}
		} else {
			try {
				this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_STOP_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					pSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.STOP_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.STOP_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
						writer.flush();
						writer.close();
					} catch (IOException e1) {
					
					}
				}
			}
		}
	}
	
	public void reexecuteScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = 0;
		PortletSession pSession = actionRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				pSession.setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_REEXECUTE_SCAN, lUserId);
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		pSession.setAttribute(PortalConstants.PARAM_IS_FROM_REEXECUTE_SCAN, true);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.executeEntireScanAction(actionRequest, actionResponse, PortalConstants.USER_EVENT_REEXECUTE_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					pSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
				}

				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
						writer.flush();
						writer.close();
					} catch (IOException e1) {
					
					}
				}
			}
		} else {
			try {
				this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_REEXECUTE_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					pSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
						writer.flush();
						writer.close();
					} catch (IOException e1) {
					
					}
				}
			}
		}
	}
	
	public void deleteScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = 0;
		PortletSession pSession = actionRequest.getPortletSession(false);
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				pSession.setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		pSession.setAttribute(PortalConstants.PARAM_IS_FROM_DELETE_SCAN, true);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.executeEntireScanAction(actionRequest, actionResponse, PortalConstants.USER_EVENT_DELETE_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					pSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
						writer.flush();
						writer.close();
					} catch (IOException e1) {
					
					}
				}
			}
		} else {
			try {
				this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_DELETE_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
					pSession.setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
						writer.flush();
						writer.close();
					} catch (IOException e1) {
					
					}
				}
			}
		}
	}
	
	private void getProjects (ActionRequest actionRequest, ActionResponse actionResponse, int userAction) {
		List<Object> projectList = null;
		Map<String, Object> searchedProject = new HashMap<String, Object>();
		
		try {
			projectList = AndroidProjectMgmtController.getProjects(actionRequest, userAction);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
			
			actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_HAS_ERROR, true);
			actionRequest.getPortletSession().removeAttribute(PortalConstants.PARAM_ACTION_SEARCH_PROJECT);
		}
		
		searchedProject.put(PortalConstants.PARAM_PROJECT_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_ID).trim());
		searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, ParamUtil.getString(actionRequest, PortalConstants.PARAM_OWNER_GROUP).trim());
		searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NUMBER).trim());
		searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME).trim());
		searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_LOW, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE_LOW).trim());
		searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_HIGH, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE_HIGH).trim());
		searchedProject.put(PortalConstants.PARAM_STATUS, ParamUtil.getString(actionRequest, PortalConstants.PARAM_STATUS).trim());
		searchedProject.put(PortalConstants.PARAM_NO_OF_SCANS, ParamUtil.getString(actionRequest, PortalConstants.PARAM_NO_OF_SCANS).trim());

		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_PROJECT_LIST, projectList);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_PROJECT, searchedProject);
		
		if (userAction == PortalConstants.USER_EVENT_SEARCH_PROJECT) {
			actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, true);
		}
		
		actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_PROJECT_LIST_JSP);
	}
	
	private void getScans (ActionRequest actionRequest, ActionResponse actionResponse, int userAction) throws UBSPortalException {
		String strScanId = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String [] strStatusArr = null;
		String strStatus = PortalConstants.STRING_EMPTY;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		DateFormat formatter = null;
		
		strScanId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID).trim();
		strFileName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_FILE_NAME).trim();
		strHashValue = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HASH_VALUE).trim();
		strScanManager = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
		strScanRegDateLow = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
		strScanRegDateHigh = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
		strStatusArr = ParamUtil.getParameterValues(actionRequest, PortalConstants.PARAM_STATUS);
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				long lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				
				if (lProjectId == 0L) {
					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
				}
				
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_PROJECT_ID, lProjectId, PortletSession.APPLICATION_SCOPE);
				
				if (userAction == PortalConstants.USER_EVENT_SEARCH_SCAN) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, true, PortletSession.APPLICATION_SCOPE);

					if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
						try {
							long lScanId = Long.parseLong(strScanId);
							
							if (lScanId < PortalConstants.LONG_ZERO) {
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
							}
						} catch (NumberFormatException e) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						if (!Validator.isAlphanumericName(strHashValue)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
						}
						
						if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
						}
					}
					
					try {
						formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
						
						if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
							if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
							}
							
							dteRegDateLow = formatter.parse(strScanRegDateLow);
						}
						
						if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
							if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
							}
							
							dteRegDateHigh = Calendar.getInstance();
							dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
							dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
							dteRegDateHigh.set(Calendar.MINUTE, 59);
							dteRegDateHigh.set(Calendar.SECOND, 59);
						}
						
						if (!CommonUtil.isObjectNull(dteRegDateLow) &&
								!CommonUtil.isObjectNull(dteRegDateHigh)) {
							if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
							}
						}
					} catch (ParseException e) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
					}
					
					if (!CommonUtil.isObjectNull(strStatusArr)) {
						for (String status : strStatusArr) {
							strStatus += status + PortalConstants.COMMA;
						}
					}
					
					Map<String, Object> searchedScan = new HashMap<String, Object>();
					searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
					searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
					searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
					
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_SCAN, searchedScan, PortletSession.APPLICATION_SCOPE);
				} else {
					actionRequest.getPortletSession().removeAttribute(PortalConstants.PARAM_SCAN, PortletSession.APPLICATION_SCOPE);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_SCAN_LIST_JSP);
		} catch (UBSPortalException e) {
			String strErrorCode = e.getErrorCode();
			
			if (strErrorCode.equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, e.getErrorMessage(), PortletSession.APPLICATION_SCOPE);
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, strErrorCode);
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			}
		}
	}
	
	private void getEntireScans (ActionRequest actionRequest, ActionResponse actionResponse, int userAction) throws UBSPortalException {
		String strScanId = null;
		String strProjectName = null;
		String strGroupName = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String [] strStatusArr = null;
		String strStatus = PortalConstants.STRING_EMPTY;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		DateFormat formatter =  null;
		int action = PortalConstants.INT_ZERO;
		
		strScanId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID).trim();
		strProjectName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
		strGroupName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_OWNER_GROUP).trim();
		strFileName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_FILE_NAME).trim();
		strHashValue = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HASH_VALUE).trim();
		strScanManager = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
		strScanRegDateLow = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
		strScanRegDateHigh = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
		strStatusArr = ParamUtil.getParameterValues(actionRequest, PortalConstants.PARAM_STATUS);
		action = ParamUtil.getInteger(actionRequest,  PortalConstants.PARAM_USER_ACTION);
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (userAction == PortalConstants.USER_EVENT_SEARCH_SCAN) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, true);
					
					if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
						try {
							long lScanId = Long.parseLong(strScanId);
							
							if (lScanId < PortalConstants.LONG_ZERO) {
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
							}
						} catch (NumberFormatException e) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
						if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
						if (CommonUtil.getStringBytes(strGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_GROUP_NAME_TOO_LONG, PortalMessages.GROUP_NAME_TOO_LONG);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
						if (!Validator.isAlphanumericName(strHashValue)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
						}
						
						if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
						}
					}
					
					try {
						formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
						
						if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
							if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
							}
							
							dteRegDateLow = formatter.parse(strScanRegDateLow);
						}
						
						if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
							if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
							}
							
							dteRegDateHigh = Calendar.getInstance();
							dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
							dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
							dteRegDateHigh.set(Calendar.MINUTE, 59);
							dteRegDateHigh.set(Calendar.SECOND, 59);
						}
						
						if (!CommonUtil.isObjectNull(dteRegDateLow)
								&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
							if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
							}
						}
					} catch (ParseException e) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
					}
					
					if (!CommonUtil.isObjectNull(strStatusArr)) {
						for (String status : strStatusArr) {
							strStatus += status + PortalConstants.COMMA;
						}
					}
					
					Map<String, Object> searchedScan = new HashMap<String, Object>();
					searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
					searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
					searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
					searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
					searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
					searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
					searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
					searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
				} else {
					actionRequest.getPortletSession().removeAttribute(PortalConstants.PARAM_SCAN);

					if (action == PortalConstants.USER_EVENT_CLEAR_SEARCH_SCAN) {
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_USER_ACTION, action);
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_ENTIRE_SCAN_LIST_JSP);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ANDROID_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_ANDROID_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
			
			if (ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				if (userAction == PortalConstants.USER_EVENT_VIEW_ENTIRE_SCAN_LIST) {
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.ANDROID_ENTIRE_SCAN_LIST_JSP);
				} else {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST);
				}
			}
		}
	}
	
	private void executeProjectAction (ActionRequest actionRequest, ActionResponse actionResponse, UploadPortletRequest uploadRequest, int userAction) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		
		switch (userAction) {
			case PortalConstants.USER_EVENT_ADD_PROJECT:
				if (AndroidProjectMgmtController.updateProject(uploadRequest, session, PortalConstants.USER_EVENT_ADD_PROJECT)) {
					
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.ADD_PROJECT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.ADD_PROJECT_FAILED);
					}
				}
				
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				
				break;
				
			case PortalConstants.USER_EVENT_UPDATE_PROJECT:
				if (AndroidProjectMgmtController.updateProject(uploadRequest, session, PortalConstants.USER_EVENT_UPDATE_PROJECT)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.UPDATE_PROJECT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.UPDATE_PROJECT_FAILED);
					}
				}
				
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				
				break;
				
			case PortalConstants.USER_EVENT_DELETE_PROJECT:
				if (AndroidProjectMgmtController.deleteProject(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.DELETE_PROJECT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.DELETE_PROJECT_FAILED);
					}
				}
				
				JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(jsonArray);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				}
				
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				
				break;
				
			default:
				break;
		}
	}
	
	private void executeScanAction (ActionRequest actionRequest, ActionResponse actionResponse, UploadPortletRequest uploadRequest, int userAction) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		JSONObject formDetailJson = null;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}

		switch (userAction) {
			case PortalConstants.USER_EVENT_COMPLETE_PROJECT:
				if (AndroidProjectMgmtController.completeProject(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.COMPLETE_PROJECT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.COMPLETE_PROJECT_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e) {
					// do nothing
				}
				
				break;
				
			case PortalConstants.USER_EVENT_OPEN_PROJECT:
				if (AndroidProjectMgmtController.openProject(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.OPEN_PROJECT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.OPEN_PROJECT_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e) {
					// do nothing
				}
				
				break;
				
			case PortalConstants.USER_EVENT_ADD_SCAN:
				if (AndroidScanMgmtController.addScan(uploadRequest, lUserId)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.ADD_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.ADD_SCAN_FAILED);
					}
				}
				
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
				
				break;
				
			case PortalConstants.USER_EVENT_STOP_SCAN:
				if (AndroidScanMgmtController.stopScan(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.STOP_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.STOP_SCAN_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				}
				
				break;
				
			case PortalConstants.USER_EVENT_REEXECUTE_SCAN:
				if (AndroidScanMgmtController.reexecuteScan(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.REEXECUTE_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.REEXECUTE_SCAN_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				}
				
				break;
				
			case PortalConstants.USER_EVENT_DELETE_SCAN:
				if (AndroidScanMgmtController.deleteScan(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						log.info(PortalMessages.USER_EVENT_DELETE_SCAN, lUserId, ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID));
						SessionMessages.add(actionRequest, PortalConstants.DELETE_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.DELETE_SCAN_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				}
				
				break;
				
			default:
				break;
		}
	}
	
	private void executeEntireScanAction (ActionRequest actionRequest, ActionResponse actionResponse, int userAction) throws UBSPortalException {
		JSONObject formDetailJson = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		switch (userAction) {	
			case PortalConstants.USER_EVENT_STOP_SCAN:
				try {
					if (AndroidScanMgmtController.stopScan(actionRequest)) {
						if (SessionMessages.isEmpty(actionRequest)) {
							SessionMessages.add(actionRequest, PortalConstants.STOP_SCAN_SUCCESSFUL);
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, PortalMessages.STOP_SCAN_FAILED);
						}
					}
					
					formDetailJson = JSONFactoryUtil.createJSONObject();
					formDetailJson.put(PortalConstants.REDIRECT, false);
					
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				} catch (UBSPortalException e1) {
					throw e1;
				}
				
				break;
				
			case PortalConstants.USER_EVENT_REEXECUTE_SCAN:
				if (AndroidScanMgmtController.reexecuteScan(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.REEXECUTE_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.REEXECUTE_SCAN_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				}
				
				break;
				
			case PortalConstants.USER_EVENT_DELETE_SCAN:
				if (AndroidScanMgmtController.deleteScan(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						log.info(PortalMessages.USER_EVENT_DELETE_SCAN, lUserId, ParamUtil.getLong(actionRequest, PortalConstants.PARAM_SCAN_ID));
						SessionMessages.add(actionRequest, PortalConstants.DELETE_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.DELETE_SCAN_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				}
				
				break;
			
			default:
				break;
		}
	}
	
	private void onChangeOwnerGroup (HttpServletRequest originalRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lSelectedGroup = 0L;
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		List<User> usersList = new ArrayList<User>();
		JSONObject formDetailJson = null;
		JSONArray orgUsers = JSONFactoryUtil.createJSONArray();
		JSONArray errorMsg = JSONFactoryUtil.createJSONArray();
		String strAvailableUsersSortOrder = null;
			
		try {
			lSelectedGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_SELECTED_GROUP);
			strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
			
			if (lSelectedGroup != 0L) {
				usersList = ControllerHelper.getOrganizationUsers(lSelectedGroup);
			}
			
			if (!CommonUtil.isListNullOrEmpty(usersList)) {
				if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
					List<User> sortableUsers = new ArrayList<User>(usersList);
					
					Collections.sort(sortableUsers, new Comparator<User>() {
						@Override
						public int compare(User u1, User u2) {
							int iCompare = 0;
							
							iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
							
							if (iCompare == 0) {
								iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
							}
							
							return iCompare;
						}
					});
					
					if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
						Collections.reverse(sortableUsers);
					}
					
					usersList = new ArrayList<User>();
					usersList = sortableUsers;
				}
				
				for (User u : usersList) {
					formDetailJson = JSONFactoryUtil.createJSONObject();
					formDetailJson.put("userId", u.getUserId());
					formDetailJson.put("emailAddress", u.getEmailAddress());
					formDetailJson.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
					orgUsers.put(formDetailJson);
				}
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put("DBConnError", true);
				errorMsg.put(formDetailJson);
			} else {
				throw ubspe;
			}
		}
		
		formDetailJson = JSONFactoryUtil.createJSONObject();
		formDetailJson.put("errorMsg", errorMsg);
		jsonArray.put(formDetailJson);
		
		formDetailJson = JSONFactoryUtil.createJSONObject();
		formDetailJson.put("orgUsers", orgUsers);
		jsonArray.put(formDetailJson);
		
		PrintWriter writer;
		
		try {
			writer = resourceResponse.getWriter();
			writer.print(jsonArray);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		}
		
	}
	
	private void filterUsers(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lSelectedGroup = 0L;
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		List<User> usersList = null;
		String strUserId = null;
		String strUserName = null;
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONArray availableUsers = JSONFactoryUtil.createJSONArray();
		JSONArray errorMsg = JSONFactoryUtil.createJSONArray();
		String strSelectedUsers = null;
		String [] strArray = null;
		long [] selectedUsers = null;
		String strAvailableUsersSortOrder = null;
		boolean bWithFilterUsername = false;
		boolean bWithFilterEmailAddress = false;
		List<User> availableUsersList = new ArrayList<User>();
		
		try {
			lSelectedGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_SELECTED_GROUP);
			strUserId = ParamUtil.getString(originalRequest, "userId");
			strUserName = ParamUtil.getString(originalRequest, "userName");
			strSelectedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
			strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
			
			strUserName = PortletCommonUtil.convertHexToString(strUserName);
			
			if (!CommonUtil.isStringNullOrEmpty(strUserName)) {
				bWithFilterUsername = true;
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strUserId)) {
				bWithFilterEmailAddress = true;
			}
			
			jsonObject.put("userIdTooLong", false);
			
			if (CommonUtil.getStringBytes(strUserId) > 75) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("userIdTooLong", true);
			}
			
			errorMsg.put(jsonObject);
			
			jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("userNameTooLong", false);
			
			if (CommonUtil.getStringBytes(strUserName) > 40) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("userNameTooLong", true);
			}
			
			errorMsg.put(jsonObject);
			
			if (!CommonUtil.isStringNullOrEmpty(strSelectedUsers)) {
				strArray = strSelectedUsers.split(PortalConstants.DELIMITER_COMMA);
				
				if (strArray != null) {
					selectedUsers = new long[strArray.length];
					
					for (int i = 0; i < strArray.length; i++) {
						selectedUsers[i] = Long.parseLong(strArray[i]);
					}
				}
			}
			
			if (lSelectedGroup != 0L) {
				usersList = ControllerHelper.getOrganizationUsers(lSelectedGroup);
				
				if (usersList != null) {
					for (User user : usersList) {
						boolean bSelected = false;
						
						if (selectedUsers != null) {
							for (int i = 0; i < selectedUsers.length; i++) {
								if (user.getUserId() == selectedUsers[i]) {
									bSelected = true;
									break;
								}
							}
						}
						
						if (((bWithFilterEmailAddress && user.getEmailAddress().toLowerCase().contains(strUserId.toLowerCase())) ||
								(bWithFilterUsername && user.getFirstName().toLowerCase().contains(strUserName.toLowerCase())))
								&& !bSelected) {
							availableUsersList.add(user);
						} else if (CommonUtil.isStringNullOrEmpty(strUserId) &&
								CommonUtil.isStringNullOrEmpty(strUserName) &&
								!bSelected) {
							availableUsersList.add(user);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
						List<User> sortableUsers = new ArrayList<User>(availableUsersList);
						
						Collections.sort(sortableUsers, new Comparator<User>() {
							@Override
							public int compare(User u1, User u2) {
								int iCompare = 0;
								
								iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						availableUsersList = new ArrayList<User>();
						availableUsersList = sortableUsers;
					}
					
					for (User u : availableUsersList) {
						jsonObject = JSONFactoryUtil.createJSONObject();
						jsonObject.put("userId", u.getUserId());
						jsonObject.put("emailAddress", u.getEmailAddress());
						jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
						availableUsers.put(jsonObject);
					}
				}
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("DBConnError", true);
				errorMsg.put(jsonObject);
			} else {
				throw ubspe;
			}
		}
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("errorMsg", errorMsg);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("availableUsers", availableUsers);
		jsonArray.put(jsonObject);
		
		try {
			PrintWriter writer;
			writer = resourceResponse.getWriter();
			writer.print(jsonArray);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		}
		
		
	}
	
	private void addUser(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lSelectedGroup = 0L;
		String strSelectedUsers = null;
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		String [] strArray = null;
		long [] selectedUsers = null;
		List<User> userList = null;
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		JSONArray usersList = JSONFactoryUtil.createJSONArray();
		JSONArray availableUsers = JSONFactoryUtil.createJSONArray();
		JSONArray errorMsg = JSONFactoryUtil.createJSONArray();
		String strUserId = null;
		String strUserName = null;
		String strUserListSortOrder = null;
		String strAvailableUsersSortOrder = null;
		List<User> selectedUserList = new ArrayList<User>();
		List<User> availableUserList = new ArrayList<User>();
		boolean bWithFilterUsername = false;
		boolean bWithFilterEmailAddress = false;
		
		lSelectedGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_SELECTED_GROUP);
		strSelectedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
		strUserId = ParamUtil.getString(originalRequest, PortalConstants.USER_ID).toLowerCase();
		strUserName = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USERNAME).toLowerCase();
		strUserListSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USER_LIST_SORT_ORDER);
		strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
		
		strUserName = PortletCommonUtil.convertHexToString(strUserName);
		
		if (!CommonUtil.isStringNullOrEmpty(strUserName)) {
			bWithFilterUsername = true;
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strUserId)) {
			bWithFilterEmailAddress = true;
		}
		
		try {
			if (!CommonUtil.isStringNullOrEmpty(strSelectedUsers)) {
				strArray = strSelectedUsers.split(PortalConstants.DELIMITER_COMMA);
				
				if (strArray != null) {
					selectedUsers = new long[strArray.length];
					for (int i = 0; i < strArray.length; i++) {
						selectedUsers[i] = Long.parseLong(strArray[i]);
					}
				}
			}
			
			if (lSelectedGroup != 0L) {
				userList = ControllerHelper.getOrganizationUsers(lSelectedGroup);
				
				if (selectedUsers != null) {
					for (int i = 0; i < selectedUsers.length; i++) {
						try {
							UserLocalServiceUtil.getUser(selectedUsers[i]);
						} catch (NoSuchUserException e) {
							jsonObject = JSONFactoryUtil.createJSONObject();
							jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_EXIST, true);
							errorMsg.put(jsonObject);
						} catch (PortalException e) {
							// do nothing
						} catch (SystemException e) {
							// do nothing
						}
						
						if (userList != null) {
							boolean bUserBelongToGroup = false;
							
							for (User user : userList) {
								bUserBelongToGroup = false;
								
								if (selectedUsers[i] == user.getUserId()) {
									selectedUserList.add(user);
									bUserBelongToGroup = true;
									break;
								}
							}
							
							if (!bUserBelongToGroup) {
								jsonObject = JSONFactoryUtil.createJSONObject();
								jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_BELONG_TO_GROUP, true);
								errorMsg.put(jsonObject);
							}
						}
					}
				}
				
				if (userList != null) {
					for (User user : userList) {
						boolean bSelected = false;
						
						for (User u : selectedUserList) {
							if (user.getUserId() == u.getUserId()) {
								bSelected = true;
								break;
							}
						}
						
						if (((bWithFilterEmailAddress && user.getEmailAddress().toLowerCase().contains(strUserId)) ||
								(bWithFilterUsername && user.getFirstName().toLowerCase().contains(strUserName)))
								&& !bSelected) {
							availableUserList.add(user);
						} else if (!bWithFilterEmailAddress &&
								!bWithFilterUsername &&
								!bSelected) {
							availableUserList.add(user);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strUserListSortOrder)) {
						List<User> sortableUsers = new ArrayList<User>(selectedUserList);
						
						Collections.sort(sortableUsers, new Comparator<User>() {
							@Override
							public int compare(User u1, User u2) {
								int iCompare = 0;
								
								iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strUserListSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						selectedUserList = new ArrayList<User>();
						selectedUserList = sortableUsers;
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
						List<User> sortableUsers = new ArrayList<User>(availableUserList);
						
						Collections.sort(sortableUsers, new Comparator<User>() {
							@Override
							public int compare(User u1, User u2) {
								int iCompare = 0;
								
								iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						availableUserList = new ArrayList<User>();
						availableUserList = sortableUsers;
					}
					
					for (User u : selectedUserList) {
						jsonObject = JSONFactoryUtil.createJSONObject();
						jsonObject.put("userId", u.getUserId());
						jsonObject.put("emailAddress", u.getEmailAddress());
						jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
						usersList.put(jsonObject);
					}
					
					for (User u : availableUserList) {
						jsonObject = JSONFactoryUtil.createJSONObject();
						jsonObject.put("userId", u.getUserId());
						jsonObject.put("emailAddress", u.getEmailAddress());
						jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
						availableUsers.put(jsonObject);
					}
				}
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("DBConnError", true);
				errorMsg.put(jsonObject);
			} else {
				throw ubspe;
			}
		}

		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("errorMsg", errorMsg);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("usersList", usersList);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("availableUsers", availableUsers);
		jsonArray.put(jsonObject);
		
		try {
			PrintWriter writer;
			writer = resourceResponse.getWriter();
			writer.print(jsonArray);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		}
	}
	
	private void removeUser(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lSelectedGroup = 0L;
		String strSelectedUsers = null;
		String [] strArray = null;
		long [] selectedUsers = null;
		String strRemovedUsers = null;
		long [] removedUsers = null;
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		List<User> userList = null;
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONArray errorMsg = JSONFactoryUtil.createJSONArray();
		JSONArray usersList = JSONFactoryUtil.createJSONArray();
		JSONArray availableUsers = JSONFactoryUtil.createJSONArray();
		String strUserId = null;
		String strUserName = null;
		String strUserListSortOrder = null;
		String strAvailableUsersSortOrder = null;
		List<User> selectedUserList = new ArrayList<User>();
		List<User> availableUserList = new ArrayList<User>();
		boolean bWithFilterUsername = false;
		boolean bWithFilterEmailAddress = false;

		lSelectedGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_SELECTED_GROUP);
		strSelectedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
		strRemovedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_REMOVED_USERS);
		strUserId = ParamUtil.getString(originalRequest, PortalConstants.USER_ID);
		strUserName = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USERNAME);
		strUserListSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USER_LIST_SORT_ORDER);
		strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
		
		strUserName = PortletCommonUtil.convertHexToString(strUserName);
		
		if (!CommonUtil.isStringNullOrEmpty(strUserName)) {
			bWithFilterUsername = true;
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strUserId)) {
			bWithFilterEmailAddress = true;
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strSelectedUsers)) {
			strArray = strSelectedUsers.split(PortalConstants.DELIMITER_COMMA);
			
			if (strArray != null) {
				selectedUsers = new long[strArray.length];
				
				for (int i = 0; i < strArray.length; i++) {
					selectedUsers[i] = Long.parseLong(strArray[i]);
				}
			}
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strRemovedUsers)) {
			strArray = null;
			strArray = strRemovedUsers.split(PortalConstants.DELIMITER_COMMA);
			
			if (strArray != null) {
				removedUsers = new long[strArray.length];
				
				for (int i = 0; i < strArray.length; i++) {
					removedUsers[i] = Long.parseLong(strArray[i]);
				}
			}
		}
		
		try {
			if (removedUsers != null) {
				for (int i = 0; i < removedUsers.length; i++) {
					jsonObject = JSONFactoryUtil.createJSONObject();
					jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_EXIST, false);
					
					try {
						UserLocalServiceUtil.getUser(removedUsers[i]);
					} catch (NoSuchUserException e) {
						jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_EXIST, true);
					} catch (PortalException e) {
						// do nothing
					} catch (SystemException e) {
						// do nothing
					}
					
					errorMsg.put(jsonObject);
					
					jsonObject = JSONFactoryUtil.createJSONObject();
					jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_BELONG_TO_GROUP, false);
					
					try {
						List<Organization> organizationList = ControllerHelper.getUserOrganizations(removedUsers[i], PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
						
						if (organizationList != null) {
							if (organizationList.get(0).getOrganizationId() != lSelectedGroup) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
							}
						}
					} catch (UBSPortalException e) {
						jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_BELONG_TO_GROUP, true);
					}
					
					errorMsg.put(jsonObject);
				}
			}
			
			if (lSelectedGroup != 0L) {
				userList = ControllerHelper.getOrganizationUsers(lSelectedGroup);
			}
			
			for (User u : userList) {
				boolean bRemoved = false;
				boolean bSelected = false;
				
				if (removedUsers != null) {
					for (int i = 0; i < removedUsers.length; i++) {
						if (u.getUserId() == removedUsers[i]) {
							bRemoved = true;
							break;
						}
					}
				}
				
				if (!bRemoved && selectedUsers != null) {
					for (int i = 0; i < selectedUsers.length; i++) {
						if (u.getUserId() == selectedUsers[i]) {
							bSelected = true;
							break;
						}
					}
				}
				
				if (!bRemoved && bSelected) {
					selectedUserList.add(u);
				} else if (bRemoved || !bSelected) {
					if (!bWithFilterEmailAddress &&
							!bWithFilterUsername) {
						availableUserList.add(u);
					} else if ((bWithFilterEmailAddress && u.getEmailAddress().toLowerCase().contains(strUserId.toLowerCase())) ||
							(bWithFilterUsername && u.getFirstName().toLowerCase().contains(strUserName.toLowerCase()))) {
						availableUserList.add(u);
					}
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strUserListSortOrder)) {
				List<User> sortableUsers = new ArrayList<User>(selectedUserList);
				
				Collections.sort(sortableUsers, new Comparator<User>() {
					@Override
					public int compare(User u1, User u2) {
						int iCompare = 0;
						
						iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
						
						if (iCompare == 0) {
							iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
						}
						
						return iCompare;
					}
				});
				
				if (strUserListSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
					Collections.reverse(sortableUsers);
				}
				
				selectedUserList = new ArrayList<User>();
				selectedUserList = sortableUsers;
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
				List<User> sortableUsers = new ArrayList<User>(availableUserList);
				
				Collections.sort(sortableUsers, new Comparator<User>() {
					@Override
					public int compare(User u1, User u2) {
						int iCompare = 0;
						
						iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
						
						if (iCompare == 0) {
							iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
						}
						
						return iCompare;
					}
				});
				
				if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
					Collections.reverse(sortableUsers);
				}
				
				availableUserList = new ArrayList<User>();
				availableUserList = sortableUsers;
			}
			
			for (User u : selectedUserList) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("userId", u.getUserId());
				jsonObject.put("emailAddress", u.getEmailAddress());
				jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
				usersList.put(jsonObject);
			}
			
			for (User u : availableUserList) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("userId", u.getUserId());
				jsonObject.put("emailAddress", u.getEmailAddress());
				jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
				availableUsers.put(jsonObject);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("DBConnError", true);
				errorMsg.put(jsonObject);
			} else {
				throw ubspe;
			}
		}
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("errorMsg", errorMsg);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("usersList", usersList);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("availableUsers", availableUsers);
		jsonArray.put(jsonObject);
		
		try {
			PrintWriter writer = resourceResponse.getWriter();
			writer.print(jsonArray);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		}
	}
	
	private void downloadReport (ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lProjectId = 0L;
		long lScanId = 0L;
		List<Report> reportList = null;
		int [] reportTypeArr = null;
		String strFileName = null;
		OutputStream sos = null;
		byte [] zip = null;
		Report report = null;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_DOWNLOAD_REPORT, lUserId);
		
		try {
			if (CommonUtil.isObjectNull(oLoggedIn) || !Boolean.parseBoolean(oLoggedIn.toString())) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			lProjectId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_PROJECT_ID);
			lScanId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_SCAN_ID);
			reportList = new ArrayList<Report>();
			reportTypeArr = new int [2];
			reportTypeArr[0] = ReportType.ANALYZE_REPORT.getInteger();
			
			reportList = ControllerHelper.getReports(lScanId, reportTypeArr, ProjectType.ANDROID.getInteger());
			
			if (CommonUtil.isListNullOrEmpty(reportList)) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalMessages.NO_REPORT);
			}
			
			report = reportList.get(0);
			
			zip = FileProcessUtil.getAnalyzedZippedReport(lProjectId, report);
			if (zip == null || zip.length == 0) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalMessages.NO_REPORT);
			}

			sos = resourceResponse.getPortletOutputStream();
	        resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_ZIP);
	        strFileName = PortalConstants.ANALYZE_REPORT;	        
	        resourceResponse.addProperty(PortalConstants.CONTENT_DISP, PortalConstants.ATTACHMENT+strFileName);
	        resourceResponse.addProperty(PortalConstants.SET_COOKIE, PortalConstants.FILE_DOWNLOAD_COOKIE);
	        
	        sos.write(zip);
	        sos.flush();
	        sos.close();
		} catch (IOException ioe) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, ioe);
		} catch (UBSPortalException ubspe) {
			throw ubspe;
		} finally {
			reportTypeArr = null;
			report = null;
		}
	}
	
	private void downloadSummary (ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest origRequest = PortalUtil.getOriginalServletRequest(request);
		String strStartDate = PortalConstants.STRING_EMPTY;
		String strEndDate = PortalConstants.STRING_EMPTY;
		Calendar calStartDate = null;
		Calendar calEndDate = null;
		DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
		String strFolderLocation = PortalConstants.STRING_EMPTY;
		boolean bFolderExist = true;
		String strSummarySuffix = PortalConstants.STRING_EMPTY;
		List<Object> reportList = null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		ZipOutputStream zipOutputStream = null;
		byte [] bytes = null;
		FileInputStream fileInputStream = null;
		BufferedInputStream bufferedInputStream = null;
		List<String> reportSummaryList = new ArrayList<String>();
		byte [] zippedReports = null;
		Calendar dateNow = Calendar.getInstance();
		
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		
		
		try {
			if (CommonUtil.isObjectNull(oLoggedIn) || !Boolean.parseBoolean(oLoggedIn.toString())) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			if (PortletCommonUtil.isDBConnected()) {
				strStartDate = ParamUtil.getString(origRequest, PortalConstants.PARAM_PERIOD_START_DATE);
				strEndDate = ParamUtil.getString(origRequest, PortalConstants.PARAM_PERIOD_END_DATE);
				
				if (CommonUtil.isStringNullOrEmpty(strStartDate)
						|| strStartDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_PERIOD_INVALID, PortalMessages.PERIOD_INVALID);
				}
				
				if (CommonUtil.isStringNullOrEmpty(strEndDate)
						|| strEndDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_PERIOD_INVALID, PortalMessages.PERIOD_INVALID);
				}
				
				try {
					calStartDate = Calendar.getInstance();
					calStartDate.setTime(format.parse(strStartDate));
					
					calEndDate = Calendar.getInstance();
					calEndDate.setTime(format.parse(strEndDate));
					calEndDate.set(Calendar.HOUR_OF_DAY, 23);
					calEndDate.set(Calendar.MINUTE, 59);
					calEndDate.set(Calendar.SECOND, 59);
					
					if (!CommonUtil.isObjectNull(calStartDate)
							&& !CommonUtil.isObjectNull(calEndDate)
							&& calEndDate.getTime().before(calStartDate.getTime())) {
						throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_PERIOD_INVALID, PortalMessages.PERIOD_INVALID);
					}
				} catch (ParseException e) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_PERIOD_INVALID, PortalMessages.PERIOD_INVALID, e);
				}
				
				strFolderLocation = FileProcessUtil.getAndroidDownloadBasePath();
				if(CommonUtil.isStringNullOrEmpty(strFolderLocation)){
					strFolderLocation = FileProcessUtil.getDefaultDownloadDirectory();
				}
								
				while(bFolderExist) {
					strSummarySuffix = PwdGenerator.getPassword();
					bFolderExist = ControllerHelper.folderExist(strFolderLocation, PortalConstants.SUMMARY_PREFIX + strSummarySuffix);
				}
				
				strFolderLocation += File.separator
						+ PortalConstants.SUMMARY_PREFIX + strSummarySuffix;
				new File (strFolderLocation).mkdirs();
				
				reportList = ReportLocalServiceUtil.getCSVReportsByProjectTypeStartDateEndDate(ProjectType.ANDROID.getInteger(), calStartDate, calEndDate);
				
				if (!CommonUtil.isListNullOrEmpty(reportList)) {
					AndroidScanReport.downloadCSVFromStorage(reportList, strFolderLocation);	// download reports from Ycloud storage
				}
				
				AndroidScanReport.generateAndroidVulnSummary(reportList, strFolderLocation);	// generate vuln.csv
				
				AndroidScanReport.generateAndroidMetaSummary(reportList, strFolderLocation);	// generate meta.csv
				
				// Create the ZIP file
				reportSummaryList.add(strFolderLocation
						+ File.separator
						+ PortalConstants.VULNCSV);
				reportSummaryList.add(strFolderLocation
						+ File.separator
						+ PortalConstants.METACSV);
				
				byteArrayOutputStream = new ByteArrayOutputStream();
				zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
				bytes = new byte[PortalConstants.READ_SPAN];
				String strFileName = PortalConstants.STRING_EMPTY;
				String strDate = PortalConstants.STRING_EMPTY + dateNow.get(Calendar.YEAR)
						+ String.format(PortalConstants.STRING_FORMAT_2LEADING_ZERO_DOUBLE, (dateNow.get(Calendar.MONTH) + 1))
						+ String.format(PortalConstants.STRING_FORMAT_2LEADING_ZERO_DOUBLE, dateNow.get(Calendar.DAY_OF_MONTH));
				
				for (String summaryName : reportSummaryList) {
					if (summaryName.contains(PortalConstants.VULNCSV)) {
						strFileName = PortalConstants.VULN_SUMMARY + strDate + PortalConstants.STR_DOT + PortalConstants.CSV_FILE_EXTENSION;
					} else if (summaryName.contains(PortalConstants.METACSV)) {
						strFileName = PortalConstants.META_SUMMARY + strDate + PortalConstants.STR_DOT + PortalConstants.CSV_FILE_EXTENSION;
					}
					
					fileInputStream = new FileInputStream(summaryName);
			        bufferedInputStream = new BufferedInputStream(fileInputStream);
			 
			        // Add ZIP entry to output stream.
			        Path reportFilePath = Paths.get(summaryName);
			        byte [] reportBytes = Files.readAllBytes(reportFilePath);
			        int bytesRead = PortalConstants.INT_ZERO;
			        
			        if(CommonUtil.isValidBytes(reportBytes)){
			        	zipOutputStream.putNextEntry(new ZipEntry(strFileName));
				            
						while ((bytesRead  = bufferedInputStream.read(bytes)) != -1) {
			            	zipOutputStream.write(bytes, 0, bytesRead);
			            }
			        } else {
						log.debug(PortalErrors.DOWNLOAD_SUMMARY_NO_SUMMARY, PortalConstants.METHOD_DOWNLOAD_SUMMARY, null, new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_NO_SUMMARY));
			        }
			        
			        if (zipOutputStream != null) {
						zipOutputStream.closeEntry();
					}
		            
		            if (bufferedInputStream != null) {
		            	bufferedInputStream.close();	
		            }
		            
		            if (fileInputStream != null) {
		            	fileInputStream.close();	
		            }
				}
				
				zipOutputStream.flush();
				byteArrayOutputStream.flush();
				
				zipOutputStream.close();
				byteArrayOutputStream.close();
				
				if (byteArrayOutputStream != null && byteArrayOutputStream.size() != 0) {
					zippedReports = byteArrayOutputStream.toByteArray();
				}
				
				byteArrayOutputStream = null;
				
				if (zippedReports == null || zippedReports.length == 0) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_NO_SUMMARY, PortalMessages.NO_SUMMARY);
				}

				OutputStream sos = resourceResponse.getPortletOutputStream();
		        resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_ZIP);
		        String strFile = URLEncoder.encode(PortalConstants.SUMMARY_ZIP + strDate + PortalConstants.STR_DOT + PortalConstants.ZIP_FILE_EXTENSION, PortalConstants.UTF_8);
		        resourceResponse.addProperty(PortalConstants.CONTENT_DISP, PortalConstants.ATTACHMENT + strFile);
		        resourceResponse.addProperty(PortalConstants.SET_COOKIE, PortalConstants.FILE_DOWNLOAD_COOKIE);
		        
		        sos.write(zippedReports);
		        sos.flush();
		        sos.close();
			}
		} catch (PortalException e) {
			throw new UBSPortalException(e.getMessage(), e.getMessage(), e);
		} catch (FileNotFoundException e) {
			throw new UBSPortalException(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (UBSPortalException e) {
			throw e;
		} finally {
			FileProcessUtil.deleteFilesSummaryFolder(strFolderLocation);
			
			reportList = null;
			bytes = null;
			reportSummaryList = null;
			zippedReports = null;
		}
	}
	
	private void downloadProjectList (ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
		int iUserRole = PortalConstants.INT_ZERO;
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		User user = null;
		long lUserId = PortalConstants.LONG_ZERO;
		List<Object> projectList = null;
		File projectListFile = null;
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					user = ControllerHelper.getUser(lUserId);
				}
				
				Map<String, Object> searchedProject = new HashMap<String, Object>();
				searchedProject.put(PortalConstants.PARAM_PROJECT_ID, ParamUtil.getString(originalRequest, PortalConstants.PARAM_PROJECT_ID).trim());
				searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, ParamUtil.getString(originalRequest, PortalConstants.PARAM_OWNER_GROUP).trim());
				searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, ParamUtil.getString(originalRequest, PortalConstants.PARAM_CASE_NUMBER).trim());
				searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, ParamUtil.getString(originalRequest, PortalConstants.PARAM_PROJECT_NAME).trim());
				searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_LOW, ParamUtil.getString(originalRequest, PortalConstants.PARAM_PROJECT_END_DATE_LOW).trim());
				searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_HIGH, ParamUtil.getString(originalRequest, PortalConstants.PARAM_PROJECT_END_DATE_HIGH).trim());
				searchedProject.put(PortalConstants.PARAM_STATUS, ParamUtil.getString(originalRequest, PortalConstants.PARAM_STATUS).trim());
				searchedProject.put(PortalConstants.PARAM_NO_OF_SCANS, ParamUtil.getString(originalRequest, PortalConstants.PARAM_NO_OF_SCANS).trim());
				
				if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
					projectList = ProjectLocalServiceUtil.getOverallAdminProjectsToDownload(searchedProject, ProjectType.ANDROID.getInteger(), user);
				} else if (iUserRole == UserRole.GEN_USER.getInteger()) {
					if (!CommonUtil.isObjectNull(user)) {
						projectList = ProjectLocalServiceUtil.getUserProjectsToDownload(user.getEmailAddress(), searchedProject, ProjectType.ANDROID.getInteger());
					}
				}
				
				String strFolderLocation = FileProcessUtil.getAndroidDownloadBasePath();
				if(CommonUtil.isStringNullOrEmpty(strFolderLocation)){
					strFolderLocation = FileProcessUtil.getDefaultDownloadDirectory();
				}
				
				projectListFile = new File (strFolderLocation
						+ File.separator
						+ PortalConstants.PROJECT_PREFIX
						+ Calendar.getInstance().getTimeInMillis()
						+ File.separator
						+ PortalConstants.PROJECTLISTCSV);
				
				new File(projectListFile.getParent()).mkdirs();
				
				ControllerHelper.createProjectListCsv(projectList, projectListFile);
				byte [] csv = Files.readAllBytes(Paths.get(projectListFile.getAbsolutePath()));
				
				if (csv == null || csv.length == 0) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_PROJECT_LIST_NO_LIST, PortalMessages.NO_PROJECT_LIST);
				}

				resourceResponse.reset();
				OutputStream sos = resourceResponse.getPortletOutputStream();
				resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_CSV); 
				resourceResponse.addProperty(PortalConstants.CONTENT_DISP, PortalConstants.ATTACHMENT + PortalConstants.PROJECTLISTCSV);
				resourceResponse.addProperty(PortalConstants.SET_COOKIE, PortalConstants.FILE_DOWNLOAD_COOKIE);
				sos.write(csv);
				sos.flush();
				sos.close();
			}
		} catch (UBSPortalException e) {
			throw e;
		} catch (PortalException e) {
			throw new UBSPortalException(e.getMessage(), e.getMessage(), e);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} finally {
			FileProcessUtil.deleteFileDirectory(projectListFile);
		}
	}
	
	private int getFieldNumber (String errorCode, int userAction) {
		int iFieldNumber = 0;
		
		if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT ||
				userAction == PortalConstants.USER_EVENT_UPDATE_PROJECT) {
			if (errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_ALREADY_EXISTS) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_INVALID) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_TOO_LONG) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_ALREADY_EXISTS) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_INVALID) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_TOO_LONG)) {
				iFieldNumber = PortalConstants.FIELD_CASE_NUMBER;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_NAME_INVALID) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_NAME_TOO_LONG) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_NAME) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_INVALID) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_TOO_LONG) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_NAME)) {
				iFieldNumber = PortalConstants.FIELD_PROJECT_NAME;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_OWNER_GROUP_DOES_NOT_EXIST) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_NO_OWNER_GROUP) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_OWNER_GROUP_DOES_NOT_EXIST) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_OWNER_GROUP)) {
				iFieldNumber = PortalConstants.FIELD_OWNER_GROUP;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_END_DATE) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_END_DATE)) {
				iFieldNumber = PortalConstants.FIELD_PROJECT_END_DATE;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_PACKAGE_NAME_INVALID) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_PACKAGE_NAME_TOO_LONG) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_PACKAGE_NAME_INVALID) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_PACKAGE_NAME_TOO_LONG)) {
				iFieldNumber = PortalConstants.FIELD_PACKAGE_NAME;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_FILE_EMPTY) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_FILE_SIZE_TOO_BIG_20) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_FILE_TYPE_INVALID) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_FILE_EMPTY) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_FILE_SIZE_TOO_BIG_20) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_FILE_TYPE_INVALID)) {
				iFieldNumber = PortalConstants.FIELD_CHECKLIST;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_USERS) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_USERS) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST)) {
				iFieldNumber = PortalConstants.FIELD_PROJECT_USERS;
			}
		} else if (userAction == PortalConstants.USER_EVENT_ADD_SCAN) {
			if (errorCode.equals(PortalErrors.ADD_SCAN_FILE_EMPTY) ||
					errorCode.equals(PortalErrors.ADD_SCAN_FILE_NAME_TOO_LONG) ||
					errorCode.equals(PortalErrors.ADD_SCAN_FILE_NOT_APK) ||
					errorCode.equals(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG_50)) {
				iFieldNumber = PortalConstants.FIELD_SCAN_FILE;
			}
		}
		
		return iFieldNumber;
	}
	
	private void redirect (ActionRequest actionRequest, ActionResponse actionResponse, String redirect) {
		ThemeDisplay td = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PortletURL actionURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(actionRequest), (String) actionRequest.getAttribute(WebKeys.PORTLET_ID), td.getLayout().getPlid(), PortletRequest.ACTION_PHASE);

		try {
			actionURL.setWindowState(WindowState.NORMAL);
			actionURL.setPortletMode(PortletMode.VIEW);
			actionURL.setParameter(PortalConstants.ACTION_JAVAX_PORTLET, redirect);
			
			if (redirect.equals(PortalConstants.ACTION_VIEW_SCAN_LIST)) {
				actionURL.setParameter(PortalConstants.PARAM_PROJECT_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_ID));
			}
			String url = actionResponse.encodeURL(actionURL.toString());
			actionRequest.setAttribute("javax.servlet.include.request_uri", url);
			
			Object oAction = actionRequest.getAttribute(PortalConstants.PARAM_USER_ACTION);
			int iAction = PortalConstants.INT_ZERO;
			
			if (!CommonUtil.isObjectNull(oAction)) {
				try {
					iAction = Integer.parseInt(oAction.toString());
				} catch (NumberFormatException e) {
					// do nothing
				}
			}
			
			if (redirect.equals(PortalConstants.ACTION_VIEW_SCAN_LIST)
					|| redirect.equals(PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST)) {
				HttpSession session = ControllerHelper.getHttpSession(actionRequest);
				
				Map<String, Object> searchedScan = new HashMap<String, Object>();
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID));
				
				if (redirect.equals(PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST)) {
					searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME));
					searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, ParamUtil.getString(actionRequest, PortalConstants.PARAM_OWNER_GROUP));
				}
				
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_FILE_NAME));
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, ParamUtil.getString(actionRequest, PortalConstants.PARAM_HASH_VALUE));
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_MANAGER));
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_LOW));
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_HIGH));
				String [] statusArr = ParamUtil.getParameterValues(actionRequest, PortalConstants.PARAM_STATUS);
				String status = PortalConstants.STRING_EMPTY;
				if (!CommonUtil.isObjectNull(statusArr)) {
					for (int index = 0; index < statusArr.length; index++) {
						status += statusArr[index] + PortalConstants.COMMA;
					}
				}
				
				searchedScan.put(PortalConstants.PARAM_STATUS, status);
				
				if (iAction == PortalConstants.USER_EVENT_CLEAR_SEARCH_SCAN) {
					session.setAttribute(PortalConstants.PARAM_USER_ACTION, iAction);
				}
				
				session.setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
				Object oIsFromSearch = actionRequest.getPortletSession().getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
				String strIsFromSearch = PortalConstants.STRING_EMPTY;
				
				if (!CommonUtil.isObjectNull(oIsFromSearch)) {
					strIsFromSearch = oIsFromSearch.toString();
					
					session.setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, strIsFromSearch);
				}
			}
			actionResponse.sendRedirect(url);
		} catch (WindowStateException e) {
			// do nothing
		} catch (PortletModeException e) {
			// do nothing
		} catch (IOException e) {
			// do nothing
		}
	}
}
