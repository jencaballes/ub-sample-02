/**
 * BranchProjectById.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class BranchProjectById  implements java.io.Serializable {
    private java.lang.String sessionId;

    private long originProjectId;

    private java.lang.String newBranchProjectName;

    public BranchProjectById() {
    }

    public BranchProjectById(
           java.lang.String sessionId,
           long originProjectId,
           java.lang.String newBranchProjectName) {
           this.sessionId = sessionId;
           this.originProjectId = originProjectId;
           this.newBranchProjectName = newBranchProjectName;
    }


    /**
     * Gets the sessionId value for this BranchProjectById.
     * 
     * @return sessionId
     */
    public java.lang.String getSessionId() {
        return sessionId;
    }


    /**
     * Sets the sessionId value for this BranchProjectById.
     * 
     * @param sessionId
     */
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }


    /**
     * Gets the originProjectId value for this BranchProjectById.
     * 
     * @return originProjectId
     */
    public long getOriginProjectId() {
        return originProjectId;
    }


    /**
     * Sets the originProjectId value for this BranchProjectById.
     * 
     * @param originProjectId
     */
    public void setOriginProjectId(long originProjectId) {
        this.originProjectId = originProjectId;
    }


    /**
     * Gets the newBranchProjectName value for this BranchProjectById.
     * 
     * @return newBranchProjectName
     */
    public java.lang.String getNewBranchProjectName() {
        return newBranchProjectName;
    }


    /**
     * Sets the newBranchProjectName value for this BranchProjectById.
     * 
     * @param newBranchProjectName
     */
    public void setNewBranchProjectName(java.lang.String newBranchProjectName) {
        this.newBranchProjectName = newBranchProjectName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BranchProjectById)) return false;
        BranchProjectById other = (BranchProjectById) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sessionId==null && other.getSessionId()==null) || 
             (this.sessionId!=null &&
              this.sessionId.equals(other.getSessionId()))) &&
            this.originProjectId == other.getOriginProjectId() &&
            ((this.newBranchProjectName==null && other.getNewBranchProjectName()==null) || 
             (this.newBranchProjectName!=null &&
              this.newBranchProjectName.equals(other.getNewBranchProjectName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSessionId() != null) {
            _hashCode += getSessionId().hashCode();
        }
        _hashCode += new Long(getOriginProjectId()).hashCode();
        if (getNewBranchProjectName() != null) {
            _hashCode += getNewBranchProjectName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BranchProjectById.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", ">BranchProjectById"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "sessionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originProjectId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "originProjectId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newBranchProjectName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "newBranchProjectName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
