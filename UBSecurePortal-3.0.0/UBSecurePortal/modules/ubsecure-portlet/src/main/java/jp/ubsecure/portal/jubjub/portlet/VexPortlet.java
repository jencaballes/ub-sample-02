package jp.ubsecure.portal.jubjub.portlet;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PwdGenerator;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.security.permission.AdvancedPermissionChecker;

import jp.ubsecure.jabberwock.cli.bridge.jubjub.ApiInitInfo;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.ApiAuditType;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.ApiSignatureSetInfo;
import jp.ubsecure.jabberwock.cli.bridge.jubjub.impl.GetSignatureSetList;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.controller.VexProjectMgmtController;
import jp.ubsecure.portal.jubjub.portlet.controller.VexScanMgmtController;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ReportType;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.UserRole;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.DetectionResultSummaryData;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectItem;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsersItem;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResult;
import jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting;
import jp.ubsecure.portal.jubjub.portlet.model.VexScanSettingItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexTargetInformationItem;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.VexDetectionResultLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.VexScanSettingLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.CxScanReport;
import jp.ubsecure.portal.jubjub.portlet.service.persistence.VexDetectionResultPK;
import jp.ubsecure.portal.jubjub.portlet.service.vex.VexScanReport;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

@Component(
		immediate = true,
		property = {
			PortalConstants.KEY_PORTLET_DISPLAY_CATEGORY + PortalConstants.DISPLAY_CATEGORY_SAMPLE,
			PortalConstants.KEY_PORTLET_ICON + PortalConstants.ICON_ICON_PNG,
			PortalConstants.KEY_PORTLET_INSTANCEABLE + PortalConstants.INSTANCEABLE_FALSE,
			PortalConstants.KEY_PRIVATE_SESSION_ATTRIBUTES + PortalConstants.PRIVATE_SESSION_ATTRIBUTES_FALSE,
			PortalConstants.KEY_HEADER_PORTLET_CSS + PortalConstants.PORTLET_CSS_MAIN_CSS,
			PortalConstants.KEY_FOOTER_PORTLET_JAVASCRIPT + PortalConstants.PORTLET_JAVASCRIPT_JQUERY_JS + PortalConstants.COMMA
				+ PortalConstants.PORTLET_JAVASCRIPT_JQUERY_UI_JS + PortalConstants.COMMA
				+ PortalConstants.PORTLET_JAVASCRIPT_FILEDOWNLOAD_JS + PortalConstants.COMMA
				+ PortalConstants.PORTLET_JAVASCRIPT_BOOTSTRAP_JS + PortalConstants.COMMA
				+ PortalConstants.PORTLET_JAVASCRIPT_MAIN_JS,
			PortalConstants.KEY_PORTLET_CSS_CLASS_WRAPPER + PortalConstants.CSS_CLASS_WRAPPER_VEX_PORTLET,
			PortalConstants.KEY_PORTLET_DISPLAY_NAME + PortalConstants.DISPLAY_NAME_VEX_PORTLET,
			PortalConstants.KEY_INIT_VIEW_TEMPLATE + PortalConstants.VEX_PROJECT_LIST_JSP,
			PortalConstants.KEY_PORTLET_EXPIRATION_CACHE + PortalConstants.EXPIRATION_CACHE,
			PortalConstants.KEY_PORTLET_SUPPORTS_MIME_TYPE + PortalConstants.CONTENT_TYPE_TEXT_HTML,
			PortalConstants.KEY_PORTLET_RESOURCE_BUNDLE + PortalConstants.RESOURCE_BUNDLE_LANGUAGE_JP,
			PortalConstants.KEY_PORTLET_INFO_TITLE + PortalConstants.INFO_TITLE_VEX_PORTLET,
			PortalConstants.KEY_PORTLET_INFO_SHORT_TITLE + PortalConstants.INFO_SHORT_TITLE_VEX,
			PortalConstants.KEY_SECURITY_ROLE_REF + PortalConstants.ROLE_ADMINISTRATOR + PortalConstants.COMMA
				+ PortalConstants.ROLE_GUEST + PortalConstants.COMMA
				+ PortalConstants.ROLE_POWER_USER + PortalConstants.COMMA
				+ PortalConstants.ROLE_USER
		},
		service = Portlet.class
	)

public class VexPortlet extends MVCPortlet{
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexPortlet.class);
	
	/**
	 * Renders the init view template for Vex which is the Vex project list screen
	 */
	@Override
	public void doView (RenderRequest renderRequest, RenderResponse renderResponse) {
		HttpSession session = ControllerHelper.getHttpSession(renderRequest);
		PortletSession pSession = renderRequest.getPortletSession(false);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		String strAction = ParamUtil.getString(renderRequest, PortalConstants.ACTION_JAVAX_PORTLET);
		String strCur = ParamUtil.getString(renderRequest, PortalConstants.PARAM_CUR);
		
		if (!renderRequest.getParameterNames().hasMoreElements()) {
			pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_COL);
			pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
		}
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		session.removeAttribute(PortalConstants.PARAM_USER_ACTION);
		
		if (!strAction.equalsIgnoreCase(PortalConstants.ACTION_SEARCH_PROJECTS) && CommonUtil.isStringNullOrEmpty(strCur)) {
			if (pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH) != null) {
				pSession.removeAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT);
			}
		}
		
		renderRequest.setAttribute(PortalConstants.PARAM_DO_VIEW_PROJECT_LIST, true);
		renderRequest.setAttribute(PortalConstants.PARAM_VIEW_PROJECT_LIST, true);
		
		log.info(PortalMessages.USER_EVENT_VIEW_PROJECT_LIST, lUserId);

		try {
			super.doView(renderRequest, renderResponse);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	@Override
	public void processAction (ActionRequest actionRequest, ActionResponse actionResponse) {
		String action = ParamUtil.getString(actionRequest, PortalConstants.ACTION_JAVAX_PORTLET);
		PortletSession pSession = actionRequest.getPortletSession();
		int iCurrentScreen = PortalConstants.INT_ZERO;
		int iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		
		try {
			iCurrentScreen = getCurrentScreen(action, iScreenNo);
			
			if (iCurrentScreen > PortalConstants.INT_ZERO) {
				pSession.setAttribute(PortalConstants.PARAM_CURRENT_SCREEN, iCurrentScreen);
			}
			
			Object oPrevScreen = pSession.getAttribute(PortalConstants.PARAM_PREVIOUS_SCREEN);
			Object oCurrScreen = pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
			String strPrevScreen = PortalConstants.STRING_EMPTY;
			String strCurrScreen = PortalConstants.STRING_EMPTY;
			
			if (!CommonUtil.isObjectNull(oPrevScreen)) {
				strPrevScreen = oPrevScreen.toString();
			}
			
			if (!CommonUtil.isObjectNull(oCurrScreen)) {
				strCurrScreen = oCurrScreen.toString();
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strPrevScreen)
					&& !CommonUtil.isStringNullOrEmpty(strCurrScreen)) {
				if (!strPrevScreen.equalsIgnoreCase(strCurrScreen)) {
					pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
					pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_COL);
					pSession.removeAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
					pSession.removeAttribute(PortalConstants.PARAM_SCAN_ID_REGENERATED_REPORT);
				}
			}
			
			super.processAction(actionRequest, actionResponse);
			
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	private int getCurrentScreen (String action, int iScreenNo) {
		int iCurrentScreen = iScreenNo;
		
		if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_PROJECT_LIST)
				|| action.equalsIgnoreCase(PortalConstants.ACTION_ADD_PROJECT)
				|| action.equalsIgnoreCase(PortalConstants.ACTION_UPDATE_PROJECT)) {
			iCurrentScreen = PortalConstants.SCREEN_PROJECT_LIST;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_SCAN_LIST)
				|| action.equalsIgnoreCase(PortalConstants.ACTION_ADD_SCAN)
				|| action.equalsIgnoreCase(PortalConstants.ACTION_UPDATE_SCAN)) {
			iCurrentScreen = PortalConstants.SCREEN_SCAN_LIST;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST)) {
			iCurrentScreen = PortalConstants.SCREEN_ENTIRE_SCAN_LIST;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_EDIT_PROJECT)) {
			iCurrentScreen = PortalConstants.SCREEN_PROJECT_REGISTRATION;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_EDIT_SCAN)) {
			iCurrentScreen = PortalConstants.SCREEN_SCAN_REGISTRATION;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_EDIT_SCAN)) {
			iCurrentScreen = PortalConstants.SCREEN_SCAN_SIMPLE_SETTING_REGISTRATION;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_EDIT_SCAN)) {
			iCurrentScreen = PortalConstants.SCREEN_SCAN_ADVANCED_SETTING_REGISTRATION;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_EDIT_SCAN)) {
			iCurrentScreen = PortalConstants.SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION;
		} else if (action.equalsIgnoreCase(PortalConstants.ACTION_VIEW_EDIT_SCAN)) {
			iCurrentScreen = PortalConstants.SCREEN_SCAN_AUTOCRAWLING_SETTING_REGISTRATION;
		} 
		
		return iCurrentScreen;
	}
	
	public void serveResource (ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		int iUserAction = PortalConstants.INT_ZERO;
		int iScreenNo = PortalConstants.INT_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		JSONArray jsonArray = null;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		try {
			iUserAction = ParamUtil.getInteger(originalRequest, PortalConstants.PARAM_USER_ACTION);
			iScreenNo = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_SCREEN_NO);

			if (iUserAction == PortalConstants.INT_ZERO) {
				iUserAction = ParamUtil.getInteger(resourceRequest, PortalConstants.PARAM_USER_ACTION);
			}

			if (iUserAction == PortalConstants.USER_EVENT_SELECT_GROUP) {
				this.onChangeOwnerGroup(originalRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_FILTER_USERS) {
				this.filterUsers(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_ADD_USER) {
				this.addUser(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_REMOVE_USER) {
				this.removeUser(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_SCREEN_TRANSITION_DIAGRAM) {
				this.downloadTransitionDiagram(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_REPORT) {
				this.downloadWordReport(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_UPDATE_ACTION) {
				switch (iScreenNo) {
					case PortalConstants.SCREEN_SCAN_LIST:
						VexScanMgmtController.getScansForRefresh(resourceRequest, resourceResponse, getPortletConfig());	
						break;
					case PortalConstants.SCREEN_ENTIRE_SCAN_LIST:
						VexScanMgmtController.getEntireScansForRefresh(resourceRequest, resourceResponse, getPortletConfig());	
						break;
					default:
						break;
				}
			} else if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_VULN) {
				log.info(PortalMessages.USER_EVENT_DOWNLOAD_DESCRIPTION, lUserId);
				this.downloadVulnList(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_SUMMARY) {
				log.info(PortalMessages.USER_EVENT_DOWNLOAD_SUMMARY, lUserId);
				this.downloadSummary(resourceRequest, resourceResponse);
			} else if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_PROJECT_LIST) {
				log.info(PortalMessages.USER_EVENT_DOWNLOAD_SEARCHED_PROJECTS, lUserId);
				this.downloadProjectList(resourceRequest, resourceResponse);
			}
		} catch (UBSPortalException ubspe) {
			
			if (iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_REPORT
					|| iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_REFERENCE
					|| iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_VULN
					|| iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_SUMMARY
					|| iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_PROJECT_LIST
					|| iUserAction == PortalConstants.USER_EVENT_DOWNLOAD_SCREEN_TRANSITION_DIAGRAM) {
				jsonArray = JSONFactoryUtil.createJSONArray();
				try {
					String userAgent = request.getHeader("user-agent").toLowerCase();
					
					if (userAgent.contains("msie")
							|| (!userAgent.contains("firefox") && !userAgent.contains("chrome"))) {
						resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_TEXT_HTML);
					} else {
						resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					}
					
					PrintWriter writer = resourceResponse.getWriter();
					
					jsonArray.put(false);
					writer.print(jsonArray);
					resourceResponse.flushBuffer();
					writer.close();
				} catch (IOException ioe) {
					log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_SERVE_RESOURCE, null, ioe);
				}
			}
			
			if (iUserAction != PortalConstants.USER_EVENT_UPDATE_ACTION) {
				if (SessionErrors.isEmpty(resourceRequest)) {
					SessionErrors.add(resourceRequest, ubspe.getErrorMessage());
					SessionMessages.add(resourceRequest, PortalUtil.getPortletId(resourceRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				}
			} else {
				String strErrorCode = ubspe.getErrorCode();
				
				if (strErrorCode.equals(PortalErrors.CX_SERVER_ERROR)) {
					resourceRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
					
					if (SessionErrors.isEmpty(resourceRequest)) {
						SessionErrors.add(resourceRequest, strErrorCode);
					}
				} else {
					if (!ControllerHelper.isUserInputError(strErrorCode)) {
						if (SessionErrors.isEmpty(resourceRequest)) {
							SessionErrors.add(resourceRequest, ubspe.getErrorMessage());
						}
					}
				}
			}
		}
	}
	
	public void viewProjectList (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_PROJECT_LIST, lUserId);
		
		this.getProjects(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_PROJECT_LIST);
	}
	
	public void viewRegisterScanSimpleSetting (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		PortletSession pSession = actionRequest.getPortletSession();
		long lUserId = PortalConstants.LONG_ZERO;
		long lScanId = PortalConstants.LONG_ZERO;

		Project project = null;
		Scan scan = null;
		VexScanSetting scanSetting = null;
		List<VexTargetInformationItem> targetInformationList = new ArrayList<>();
		List<VexLoginSettingItem> loginSettingList = null;
		StringBuilder tempStartURL = null;
		StringBuilder tempAuthorizedPatrolURL = null;
		String[] array = null;
		boolean isFormParamsVisible = PortalConstants.FALSE;
		List<VexLoginSettingItem> loginParams = new ArrayList<>();
		List<ProjectItem> targetURLs = null;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_REGISTER_SCAN_SIMPLE_SETTING, lUserId);
		
		
		try {
			pSession.removeAttribute(PortalConstants.PARAM_SCAN_ID);
			lScanId = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCAN_ID);
			project = VexProjectMgmtController.getProject(actionRequest);

			if (lScanId != PortalConstants.LONG_ZERO) {
				scan = VexScanMgmtController.getScan(lScanId);
				scanSetting = VexScanMgmtController.getVexScanSetting(lScanId);
				targetInformationList = VexScanMgmtController.getVexTargetInformationList(lScanId);
				loginSettingList = VexScanMgmtController.getVexLoginSettingList(lScanId);

				if (null != scanSetting) {
					if (!CommonUtil.isStringNullOrEmpty(scanSetting.getStarturl())) {
						tempStartURL = new StringBuilder();
						array = scanSetting.getStarturl().split(";");
						for (String url : array) {
							tempStartURL.append(url + System.lineSeparator());
						}
						scanSetting.setStarturl(tempStartURL.toString());
					}
	
					if (!CommonUtil.isStringNullOrEmpty(scanSetting.getAuthorizedpatrolurl())) {
						tempAuthorizedPatrolURL = new StringBuilder();
						array = scanSetting.getAuthorizedpatrolurl().split(";");
						for (String url : array) {
							tempAuthorizedPatrolURL.append(url + System.lineSeparator());
						}
						scanSetting.setAuthorizedpatrolurl(tempAuthorizedPatrolURL.toString());
					}
				}

				if (!CommonUtil.isListNullOrEmpty(loginSettingList)){
					for(VexLoginSettingItem item: loginSettingList){
						VexLoginSettingItem temp = new VexLoginSettingItem();
						temp.setParamname(item.getParamname());
						temp.setParamvalue(item.getParamvalue());
						if(item.getParamvalue().equals("null")){
							temp.setChecked(PortalConstants.INT_ZERO);
						}else{
							temp.setChecked(PortalConstants.INT_ONE);
						}
						loginParams.add(temp);
					}
				}
				
				actionRequest.setAttribute(PortalConstants.PARAM_SCAN, scan);
				actionRequest.setAttribute(PortalConstants.PARAM_SCAN_SETTING, scanSetting);
				actionRequest.setAttribute(PortalConstants.PARAM_TARGET_INFORMATION_LIST, targetInformationList);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
				isFormParamsVisible = true;
				actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
			} else {
				targetURLs = VexProjectMgmtController.getProjectURLs(project, PortalConstants.PARAM_PROJECT_TARGET_URLS);
				scanSetting = VexScanSettingLocalServiceUtil.createVexScanSettingObj();
				StringBuilder tempStartUrl = new StringBuilder();
				StringBuilder tempAuthorizedpatrolurl = new StringBuilder();
				
				if(!CommonUtil.isListNullOrEmpty(targetURLs)){
					for (ProjectItem url : targetURLs) {			
						if(!CommonUtil.isStringNullOrEmpty(url.getTargetUrlprotocol()) && (!CommonUtil.isStringNullOrEmpty(url.getTargetUrlhost()) || !CommonUtil.isStringNullOrEmpty(url.getTargetUrlport()) )) {
							VexTargetInformationItem item = new VexTargetInformationItem();
							item.setProtocol(url.getTargetUrlprotocol());
							item.setHost(url.getTargetUrlhost());
							if (!CommonUtil.isStringNullOrEmpty(url.getTargetUrlport())) {
								item.setPort(url.getTargetUrlport());
							}
							targetInformationList.add(item);
							
							if(url.getTargetUrlprotocol().equals("1")){
								tempStartUrl.append("http://" + url.getTargetUrlhost() + ":" + url.getTargetUrlport() + System.lineSeparator());
								tempAuthorizedpatrolurl.append("http://" + url.getTargetUrlhost() + ":" + url.getTargetUrlport() + System.lineSeparator());
							}else if(url.getTargetUrlprotocol().equals("2")){
								tempStartUrl.append("https://" + url.getTargetUrlhost() + ":" + url.getTargetUrlport() + System.lineSeparator());
								tempAuthorizedpatrolurl.append("http://" + url.getTargetUrlhost() + ":" + url.getTargetUrlport() + System.lineSeparator());
							}
						}
					}
				}
					
				scanSetting.setStarturl(tempStartUrl.toString());
				scanSetting.setAuthorizedpatrolurl(tempAuthorizedpatrolurl.toString());
				
				actionRequest.setAttribute(PortalConstants.PARAM_SCAN, null);
				actionRequest.setAttribute(PortalConstants.PARAM_SCAN_SETTING, scanSetting);
				actionRequest.setAttribute(PortalConstants.PARAM_TARGET_INFORMATION_LIST, targetInformationList);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, null);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, null);
				actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
			}
			
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_SIMPLE_SETTING_JSP);
		} catch (UBSPortalException ubspe) {			
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorMessage());
					}
				}
			}
			
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	public void registerScanSimpleSetting (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Project project = null;
		List<ApiSignatureSetInfo> webInspectionSignatureSetList =  null;
		List<ApiSignatureSetInfo> serverFilesSignatureSetList =  null;
		List<ApiSignatureSetInfo> serverSettingsSignatureSetList =  null;

		ApiInitInfo oInitInfo = VexScanMgmtController.setLoginCredentials();

		boolean isFormParamsVisible = PortalConstants.FALSE;
		List<VexLoginSettingItem> loginParams = new ArrayList<>();
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_REGISTER_SCAN_SIMPLE_SETTING, lUserId);

		int iUserAction = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_USER_ACTION);
		
		try {
				project = VexProjectMgmtController.getProject(actionRequest);
				
				webInspectionSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_WEB_SCAN, oInitInfo);
				serverFilesSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_FILES, oInitInfo);
				serverSettingsSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_SETTINGS, oInitInfo);
				int numOfParams = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
				if(numOfParams != PortalConstants.INT_ZERO)
					loginParams = VexScanMgmtController.getLoginInfoSetting(actionRequest, project.getProjectId());
				
				if (VexScanMgmtController.addVexScanSetting(actionRequest, iUserAction, lUserId)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.REGISTER_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.VEX_REGISTER_SCAN_FAILED);
					}
				}
			
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			String startURL = ParamUtil.getString(originalRequest, PortalConstants.PARAM_START_URL);
			String authorizedPatrolURL = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AUTHORIZED_PATROL_URL);
			int targetInfoListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFORMATION_LIST_SIZE);
			
			List<VexTargetInformationItem> targetInformationList = new ArrayList<>();
			
			for (int i = 0; i <= targetInfoListSize; i++) {
				String protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROTOCOL_GROUP + i);
				String host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HOST + i).trim();
				String port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PORT + i).trim();
				
				if(!CommonUtil.isStringNullOrEmpty(protocol) && (!CommonUtil.isStringNullOrEmpty(host) || !CommonUtil.isStringNullOrEmpty(port) )) {
					VexTargetInformationItem item = new VexTargetInformationItem();
					item.setScanid(i);
					item.setProtocol(protocol);
					item.setHost(host);
					if (!CommonUtil.isStringNullOrEmpty(port)) {
						item.setPort(port);
					}
					targetInformationList.add(item);
				}
			}
			
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			
			isFormParamsVisible = true;
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_START_URL, startURL);
			actionRequest.setAttribute(PortalConstants.PARAM_AUTHORIZED_PATROL_URL, authorizedPatrolURL);
			actionRequest.setAttribute(PortalConstants.PARAM_TARGET_INFORMATION_LIST, targetInformationList);
			actionRequest.setAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST, webInspectionSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST, serverFilesSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST, serverSettingsSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_SIMPLE_SETTING_JSP);
		}
	}
	
	public void registerScanTargetInformation(ActionRequest actionRequest, ActionResponse actionResponse){
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		List<VexTargetInformationItem> targetInfoItemList = null;
		Project project = null;
		PortletSession pSession = actionRequest.getPortletSession(); 
		int previousScreen = PortalConstants.INT_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		try {
			project = VexProjectMgmtController.getProject(actionRequest);
			previousScreen = (int) pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);

			if(previousScreen == PortalConstants.SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION){
				log.info(PortalMessages.USER_EVENT_REGISTER_SCAN_TARGET_INFORMATION, lUserId);
				targetInfoItemList = VexScanMgmtController.addVexScanTargetInformation(actionRequest, lUserId, project.getProjectId());
				pSession.setAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION, targetInfoItemList); 
			}else{
				targetInfoItemList = (List<VexTargetInformationItem>) pSession.getAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
				pSession.setAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION, targetInfoItemList);
			}
			
			String scanName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_NAME);
			if(!CommonUtil.isStringNullOrEmpty(scanName)){
				actionRequest.setAttribute(PortalConstants.PARAM_SCAN_NAME, scanName);
			}

			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			this.viewRegisterScanAutoCrawlingSetting(actionRequest, actionResponse);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
					targetInfoItemList = VexScanMgmtController.addVexScanTargetInformationForValidation(actionRequest);
					pSession.setAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION, targetInfoItemList); 
				}
			}
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_TARGET_INFORMATION_JSP);
		}
	}
	
	public void registerScanAdvancedSetting (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		long lScanId = PortalConstants.LONG_ZERO;
		Project project = null;
		List<ApiSignatureSetInfo> webInspectionSignatureSetList =  null;
		List<ApiSignatureSetInfo> serverFilesSignatureSetList =  null;
		List<ApiSignatureSetInfo> serverSettingsSignatureSetList =  null;
		PortletSession pSession = actionRequest.getPortletSession(); 

		ApiInitInfo oInitInfo = VexScanMgmtController.setLoginCredentials();
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		try {
			project = VexProjectMgmtController.getProject(actionRequest);
			lScanId = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCAN_ID);
			
			if(lScanId != PortalConstants.LONG_ZERO){
				actionRequest.setAttribute(PortalConstants.PARAM_SCAN_ID, lScanId);
			}
			
			webInspectionSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_WEB_SCAN, oInitInfo);
			serverFilesSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_FILES, oInitInfo);
			serverSettingsSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_SETTINGS, oInitInfo);
			
			VexScanSettingItem item = VexScanMgmtController.addVexScanAdvancedSetting(actionRequest);
			
			String scanName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_NAME);
			if(!CommonUtil.isStringNullOrEmpty(scanName)){
				actionRequest.setAttribute(PortalConstants.PARAM_SCAN_NAME, scanName);
				pSession.setAttribute(PortalConstants.PARAM_SCAN_NAME, scanName); 
			}
			
			pSession.setAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING, item); 
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST, webInspectionSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST, serverFilesSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST, serverSettingsSignatureSetList);
			this.viewRegisterScanTargetInformation(actionRequest, actionResponse);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
						
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST, webInspectionSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST, serverFilesSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST, serverSettingsSignatureSetList);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_ADVANCED_SETTING_JSP);
		}
	}
	
	public void viewRegisterScanAdvancedSetting (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		VexScanSettingItem advanceSettingItem = null;
		long lScanId = PortalConstants.LONG_ZERO;
		Project project = null;
		Scan scan = null;
		VexScanSetting scanSetting = null;
		PortletSession pSession = actionRequest.getPortletSession();
		int previousScreen = PortalConstants.INT_ZERO;
		String scanName = PortalConstants.STRING_EMPTY;
		ApiInitInfo oInitInfo = VexScanMgmtController.setLoginCredentials();
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_REGISTER_SCAN_ADVANCED_SETTING, lUserId);
			
		try {
			project = VexProjectMgmtController.getProject(actionRequest);
			lScanId = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCAN_ID);
			
			previousScreen = (int) pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
			if(previousScreen == PortalConstants.SCREEN_SCAN_AUTOCRAWLING_SETTING_REGISTRATION 
					|| previousScreen == PortalConstants.SCREEN_SCAN_ADVANCED_SETTING_REGISTRATION
					|| previousScreen == PortalConstants.SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION
					|| lScanId != PortalConstants.LONG_ZERO){
				if(lScanId != PortalConstants.LONG_ZERO){
					scan = VexScanMgmtController.getScan(lScanId);
					scanSetting = VexScanMgmtController.getVexScanSetting(lScanId);
					scanName = scan.getFileName();
					pSession.setAttribute(PortalConstants.PARAM_SCAN_ID, lScanId);
					actionRequest.setAttribute(PortalConstants.PARAM_SCAN_NAME, scanName);
					actionRequest.setAttribute(PortalConstants.PARAM_SCAN, scan);
					actionRequest.setAttribute(PortalConstants.PARAM_SCAN_SETTING, scanSetting);
					advanceSettingItem = new VexScanSettingItem();
					advanceSettingItem.setStarttime(scanSetting.getStarttime());
					advanceSettingItem.setEndtime(scanSetting.getEndtime());
					advanceSettingItem.setImplementationenvironment(scanSetting.getImplementationenvironment());
					advanceSettingItem.setSetemailnotification(scanSetting.getSetemailnotification());
					
				} else {
					advanceSettingItem = (VexScanSettingItem) pSession.getAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
					scanName = (String) pSession.getAttribute(PortalConstants.PARAM_SCAN_NAME);
					if(!CommonUtil.isStringNullOrEmpty(scanName)){
						actionRequest.setAttribute(PortalConstants.PARAM_SCAN_NAME, scanName);
					}
				}
			}else{
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
				pSession.removeAttribute(PortalConstants.PARAM_SCAN_NAME); 
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
				pSession.removeAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
				pSession.removeAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
				pSession.removeAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING);
			}
			
			if(CommonUtil.isStringNullOrEmpty(scanName)){
				scanName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_NAME);
				if(!CommonUtil.isStringNullOrEmpty(scanName)){
					actionRequest.setAttribute(PortalConstants.PARAM_SCAN_NAME, scanName);
				}
			}
			
			pSession.setAttribute(PortalConstants.PARAM_SCAN_NAME, scanName);
			pSession.setAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING, advanceSettingItem);
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_ADVANCED_SETTING_JSP);
		} catch (UBSPortalException ubspe) {			
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorMessage());
					}
				}
			}
			
			if (ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {

				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING, advanceSettingItem);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_ADVANCED_SETTING_JSP	);
			}
			
		}
	}
	
	
	
	public void viewRegisterScanTargetInformation (ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		long lScanId = PortalConstants.LONG_ZERO;
		Project project = null;
		VexScanSetting scanSetting = null;
		PortletSession pSession = actionRequest.getPortletSession();
		int totalNumOfParams = PortalConstants.INT_ZERO;
		List<VexLoginSettingItem> loginSettingItem = null;
		ArrayList<ArrayList<VexLoginSettingItem>> loginSettingList = new ArrayList<ArrayList<VexLoginSettingItem>>();
		List<VexTargetInformationItem> targetInfoItem = new ArrayList<>();
		List<VexTargetInformationItem> targetInfoList = new ArrayList<>();
		int previousScreen = PortalConstants.INT_ZERO;
		VexScanSettingItem autoCrawlingSetting = new VexScanSettingItem();
		VexScanSettingItem autoCrawlingSettingfromSession = new VexScanSettingItem();
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_REGISTER_SCAN_TARGET_INFORMATION, lUserId);
		
		try {
			project = VexProjectMgmtController.getProject(actionRequest); 
			lScanId = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCAN_ID);
			
			previousScreen = (int) pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
			if(previousScreen == PortalConstants.SCREEN_SCAN_AUTOCRAWLING_SETTING_REGISTRATION 
					|| previousScreen == PortalConstants.SCREEN_SCAN_ADVANCED_SETTING_REGISTRATION
					|| previousScreen == PortalConstants.SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION
					|| lScanId != PortalConstants.LONG_ZERO){
				if(lScanId != PortalConstants.LONG_ZERO){
					targetInfoItem = VexScanMgmtController.getVexTargetInformationList(lScanId);
				} else {
					targetInfoItem = (List<VexTargetInformationItem>) pSession.getAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
				}
				
				autoCrawlingSettingfromSession = (VexScanSettingItem) pSession.getAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING);
				
				if(CommonUtil.isListNullOrEmpty(targetInfoItem)){
					targetInfoItem = new ArrayList<>();
					List<ProjectItem> targetURLs = VexProjectMgmtController.getProjectURLs(project, PortalConstants.PARAM_PROJECT_TARGET_URLS);

					if(!CommonUtil.isListNullOrEmpty(targetURLs)){
						for (ProjectItem url : targetURLs) {			
							if(!CommonUtil.isStringNullOrEmpty(url.getTargetUrlprotocol()) && (!CommonUtil.isStringNullOrEmpty(url.getTargetUrlhost()) || !CommonUtil.isStringNullOrEmpty(url.getTargetUrlport()) )) {
								VexTargetInformationItem item = new VexTargetInformationItem();
								if(url.getTargetUrlprotocol().equals("1"))
									item.setProtocol("http://");
								else if(url.getTargetUrlprotocol().equals("2"))
									item.setProtocol("https://");
								
								item.setHost(url.getTargetUrlhost());
								if (!CommonUtil.isStringNullOrEmpty(url.getTargetUrlport())) {
									item.setPort(url.getTargetUrlport());
								}
								
								item.setHttpversion(1);
								item.setSetresponsecontentlength(1);
								item.setUseacceptencodingheader(1);
								item.setUnzipresponse(1);
								
								targetInfoItem.add(item);
							}
						}
					}
				} else {
					targetInfoList = new ArrayList<>();
					for (VexTargetInformationItem targetInfo : targetInfoItem) {
						VexTargetInformationItem item = new VexTargetInformationItem();
						
						item.setProtocol(targetInfo.getProtocol());
						item.setHost(targetInfo.getHost());
						item.setPort(targetInfo.getPort());
						item.setHttpversion(targetInfo.getHttpversion());
						item.setSetkeepaliveconnection(targetInfo.getSetkeepaliveconnection());
						item.setSetresponsecontentlength(targetInfo.getSetresponsecontentlength());
						item.setUseacceptencodingheader(targetInfo.getUseacceptencodingheader());
						item.setUnzipresponse(targetInfo.getUnzipresponse());
						item.setHttpprotocol(targetInfo.getHttpprotocol().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getHttpprotocol());
						item.setExternalproxyhost(targetInfo.getExternalproxyhost().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getExternalproxyhost());
						item.setExternalproxyport(targetInfo.getExternalproxyport());
						item.setExternalproxyauthid(targetInfo.getExternalproxyauthid().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getExternalproxyauthid());
						item.setExternalproxyauthpassword(targetInfo.getExternalproxyauthpassword().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getExternalproxyauthpassword());
						item.setUseclientcertificate(targetInfo.getUseclientcertificate());
						item.setCertificatefile(targetInfo.getCertificatefile().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getCertificatefile());
						item.setCertificatefilepassword(targetInfo.getCertificatefilepassword().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getCertificatefilepassword());
						item.setNtlmauthid(targetInfo.getNtlmauthid().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getNtlmauthid());
						item.setNtlmauthpassword(targetInfo.getNtlmauthpassword().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getNtlmauthpassword());
						item.setNtlmauthdomain(targetInfo.getNtlmauthdomain().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getNtlmauthdomain());
						item.setNtlmauthhost(targetInfo.getNtlmauthhost().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getNtlmauthhost());
						item.setDigestauthid(targetInfo.getDigestauthid().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getDigestauthid());
						item.setDigestauthpassword(targetInfo.getDigestauthpassword().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getDigestauthpassword());
						item.setBasicauthid(targetInfo.getBasicauthid().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getBasicauthid());
						item.setBasicauthpassword(targetInfo.getBasicauthpassword().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getBasicauthpassword());
						item.setAccessexclusionpath(targetInfo.getAccessexclusionpath().equalsIgnoreCase("null") ? PortalConstants.STRING_EMPTY : targetInfo.getAccessexclusionpath());
						
						targetInfoList.add(item);
					}
					targetInfoItem = targetInfoList;
				}

				if(previousScreen == PortalConstants.SCREEN_SCAN_AUTOCRAWLING_SETTING_REGISTRATION){
					autoCrawlingSetting = VexScanMgmtController.addUpdateAutoCrawlingSetting(actionRequest); 
					pSession.setAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING, autoCrawlingSetting);
				}else if(!CommonUtil.isObjectNull(autoCrawlingSettingfromSession)
						&& previousScreen == PortalConstants.SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION){
					pSession.setAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING, autoCrawlingSettingfromSession);
				}else{
					if(CommonUtil.isObjectNull(autoCrawlingSettingfromSession) && lScanId == PortalConstants.LONG_ZERO){
						autoCrawlingSetting = VexScanMgmtController.setAutoCrawlDefaultValuesForDetailSetting(autoCrawlingSetting, project);
					} else if (lScanId != PortalConstants.LONG_ZERO) {
						scanSetting = VexScanMgmtController.getVexScanSetting(lScanId);
						autoCrawlingSettingfromSession = (VexScanSettingItem) pSession.getAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
						autoCrawlingSetting = VexScanMgmtController.copyAutoCrawlValuesForDetailSetting(autoCrawlingSettingfromSession, scanSetting);
						loginSettingItem = VexScanMgmtController.getVexLoginSettingList(lScanId);
						loginSettingList = VexScanMgmtController.copyLoginSettingsForDetailSetting(loginSettingItem);
						totalNumOfParams = loginSettingList.size(); 
						pSession.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
						pSession.setAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS, totalNumOfParams);
					}
					pSession.setAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING, autoCrawlingSetting);
				}
			}else{
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
			}

			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			pSession.setAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION, targetInfoItem);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_TARGET_INFORMATION_JSP);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}

			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				pSession.setAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION, targetInfoItem);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_TARGET_INFORMATION_JSP);
			}
		}
	}
	
	public void viewRegisterScanLoginForm (ActionRequest actionRequest, ActionResponse actionResponse) {
		Project project = null;
		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession();
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		List<VexLoginSettingItem> loginParams = new ArrayList<>();
		boolean isFormParamsVisible = PortalConstants.FALSE;
		List<VexTargetInformationItem> targetInformationList = new ArrayList<>();
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_REGISTER_SCAN_LOGIN_SETTING, lUserId);
		
		try {
			project = VexProjectMgmtController.getProject(actionRequest); 
			int rowIndex = -1; //To add new login settings; Initialize row index before 0
			
			targetInformationList = (List<VexTargetInformationItem>) pSession.getAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
			
			if(CommonUtil.isListNullOrEmpty(targetInformationList))
				pSession.setAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION, new ArrayList<>());
			
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_INFO_INDEX, rowIndex);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_LOGIN_FORM_JSP);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			}else {
				int rowIndex = -1; 
				
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_INFO_INDEX, rowIndex);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_LOGIN_FORM_JSP);
			}
			
		}
	}
	
	public void viewRegisterScanAutoCrawlingSetting (ActionRequest actionRequest, ActionResponse actionResponse) {		
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession();
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		ArrayList <ArrayList<VexLoginSettingItem>> loginSettingList = null;
		int previousScreen = PortalConstants.INT_ZERO;
		int totalNumOfParams = PortalConstants.INT_ZERO;

		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}

		try {
			project = VexProjectMgmtController.getProject(actionRequest);
			previousScreen = (int) pSession.getAttribute(PortalConstants.PARAM_CURRENT_SCREEN);
			
			if(previousScreen == PortalConstants.SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION){
				log.info(PortalMessages.USER_EVENT_VIEW_REGISTER_SCAN_AUTOCRAWL_SETTING, lUserId);
			}
			
			if(previousScreen == PortalConstants.SCREEN_SCAN_AUTOCRAWLING_SETTING_REGISTRATION 
					|| previousScreen == PortalConstants.SCREEN_SCAN_ADVANCED_SETTING_REGISTRATION
					|| previousScreen == PortalConstants.SCREEN_SCAN_TARGET_INFORMATION_REGISTRATION){
				
				try{	
					loginSettingList = (ArrayList<ArrayList<VexLoginSettingItem>>) pSession.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
					totalNumOfParams = (int) pSession.getAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
				}catch(NullPointerException e){
					loginSettingList = new ArrayList<ArrayList<VexLoginSettingItem>>();
				}
				
			}else{
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
				pSession.removeAttribute(PortalConstants.PARAM_SCAN_NAME); 
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
				pSession.removeAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
				pSession.removeAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
				pSession.removeAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING);
			}
			
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			pSession.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
			pSession.setAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS, totalNumOfParams);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_AUTOCRAWLING_SETTING_JSP);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}

			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_AUTOCRAWLING_SETTING_JSP);
			}
		}
	}
	
	public void cancelRegisterScanDetailSetting (ActionRequest actionRequest, ActionResponse actionResponse) {		
			Project project = null;
			HttpSession session = ControllerHelper.getHttpSession(actionRequest);
			PortletSession pSession = actionRequest.getPortletSession();
			Object oUserId = session.getAttribute(PortalConstants.USER_ID);
			long lUserId = PortalConstants.LONG_ZERO;

			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
			}

			try {
				project = VexProjectMgmtController.getProject(actionRequest);
				
				pSession.removeAttribute(PortalConstants.PARAM_SCAN_ID); 
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
				pSession.removeAttribute(PortalConstants.PARAM_SCAN_NAME); 
				pSession.removeAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
				pSession.removeAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
				pSession.removeAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
				pSession.removeAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING);

				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				this.viewScanList(actionRequest, actionResponse);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
							e.getErrorMessage());

					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
				}

				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
						|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
						|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
						|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
				} else {
					actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
					this.viewScanList(actionRequest, actionResponse);
				}
			}
		}
	
	public void acquireLoginInformationSimpleSetting (ActionRequest actionRequest, ActionResponse actionResponse) {		
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		List<VexLoginSettingItem> loginParams = new ArrayList<>();
		boolean isFormParamsVisible = PortalConstants.FALSE;
		List<ApiSignatureSetInfo> webInspectionSignatureSetList =  null;
		List<ApiSignatureSetInfo> serverFilesSignatureSetList =  null;
		List<ApiSignatureSetInfo> serverSettingsSignatureSetList =  null;
	
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}

		log.info(PortalMessages.USER_EVENT_GET_LOGIN_INFORMATION, lUserId);

		try {
			ApiInitInfo oInitInfo = VexScanMgmtController.setLoginCredentials();
			project = VexProjectMgmtController.getProject(actionRequest); 
			webInspectionSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_WEB_SCAN, oInitInfo);
			serverFilesSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_FILES, oInitInfo);
			serverSettingsSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_SETTINGS, oInitInfo);
			loginParams = VexScanMgmtController.getLoginInfoSetting(actionRequest, project.getProjectId());
			
			isFormParamsVisible = PortalConstants.TRUE;
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}

			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} 		
		}
			String startURL = ParamUtil.getString(originalRequest, PortalConstants.PARAM_START_URL);
			String authorizedPatrolURL = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AUTHORIZED_PATROL_URL);
			int targetInfoListSize = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_TARGET_INFORMATION_LIST_SIZE);
			
			List<VexTargetInformationItem> targetInformationList = new ArrayList<>();
			
			for (int i = 0; i < targetInfoListSize; i++) {
				String protocol = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROTOCOL_GROUP + i);
				String host = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HOST + i).trim();
				String port = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PORT + i).trim();
				
				if(!CommonUtil.isStringNullOrEmpty(protocol) && (!CommonUtil.isStringNullOrEmpty(host) || !CommonUtil.isStringNullOrEmpty(port) )) {
					VexTargetInformationItem item = new VexTargetInformationItem();
					item.setScanid(i);
					item.setProtocol(protocol);
					item.setHost(host);
					if (!CommonUtil.isStringNullOrEmpty(port)) {
						item.setPort(port);
					}
					targetInformationList.add(item);
				}
			}
			
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_START_URL, startURL);
			actionRequest.setAttribute(PortalConstants.PARAM_AUTHORIZED_PATROL_URL, authorizedPatrolURL);
			actionRequest.setAttribute(PortalConstants.PARAM_TARGET_INFORMATION_LIST, targetInformationList);
			actionRequest.setAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST, webInspectionSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST, serverFilesSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST, serverSettingsSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REGISTER_SCAN_SIMPLE_SETTING_JSP);
	}
	
	public void acquireLoginInformationDetailSetting (ActionRequest actionRequest, ActionResponse actionResponse) {		
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		List<VexLoginSettingItem> loginParams = new ArrayList<>();
		ApiInitInfo info = null; 
		boolean isFormParamsVisible = PortalConstants.FALSE;
		String targetInfo = PortalConstants.STRING_EMPTY;
		String loginPath = PortalConstants.STRING_EMPTY;
		int rowIndex = -1; //To add new login settings; Initialize row index before 0
	
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
			info = VexScanMgmtController.setLoginCredentials();
		}

		log.info(PortalMessages.USER_EVENT_GET_LOGIN_INFORMATION, lUserId);

		try {
			int iUserAction = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_USER_ACTION);
			
			if(iUserAction == PortalConstants.USER_EVENT_UPDATE_LOGIN_INFORMATION)
				rowIndex = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_LOGIN_INFO_INDEX);
			
			project = VexProjectMgmtController.getProject(actionRequest); 
			
			targetInfo = ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_INFORMATION_GROUP);
			loginPath = ParamUtil.getString(actionRequest, PortalConstants.PARAM_LOGIN_PATH);
			
			loginParams = VexScanMgmtController.getCrawlLoginInfo(project.getProjectId(), targetInfo, loginPath); 
			isFormParamsVisible = PortalConstants.TRUE;
			
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PATH_LABEL, targetInfo + loginPath);
			actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_INFO_INDEX, rowIndex);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_LOGIN_FORM_JSP);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}

			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PATH_LABEL, targetInfo + loginPath);
				actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_INFO_INDEX, rowIndex);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_LOGIN_FORM_JSP);
			}
		}
	}
	
	public void addLoginInformation (ActionRequest actionRequest, ActionResponse actionResponse) {		
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession();
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		ArrayList <VexLoginSettingItem> addloginInfo = new ArrayList<>();
		ArrayList <ArrayList<VexLoginSettingItem>> loginSettingList = new ArrayList<ArrayList<VexLoginSettingItem>>();
		ApiInitInfo info = null; 
	
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
			info = VexScanMgmtController.setLoginCredentials();
		}

		log.info(PortalMessages.USER_EVENT_ADD_LOGIN_INFORMATION, lUserId);

		try {
			project = VexProjectMgmtController.getProject(actionRequest); 
			addloginInfo = VexScanMgmtController.addUpdateLoginSettingList(actionRequest);
			
			try{
				loginSettingList = (ArrayList<ArrayList<VexLoginSettingItem>>) pSession.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
				loginSettingList.add(addloginInfo);
			}catch(NullPointerException e){
				loginSettingList = new ArrayList<ArrayList<VexLoginSettingItem>>();
				loginSettingList.add(addloginInfo);
			}
			
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			pSession.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
			this.viewRegisterScanAutoCrawlingSetting(actionRequest,actionResponse);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}

			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else if (e.getErrorCode().equals(PortalErrors.UPDATE_LOGIN_SETTING_FAILED)){
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_LOGIN_FORM_JSP);
			} else {
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				this.acquireLoginInformationDetailSetting(actionRequest,actionResponse);
			}
		}
	}
	
	public void deleteLoginInformation (ActionRequest actionRequest, ActionResponse actionResponse) {		
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession();
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;

		int rowIndex = PortalConstants.INT_ZERO;
		ArrayList <ArrayList<VexLoginSettingItem>> loginSettingList = new ArrayList<ArrayList<VexLoginSettingItem>>();
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}

		log.info(PortalMessages.USER_EVENT_DELETE_LOGIN_INFORMATION, lUserId);

		try {
			project = VexProjectMgmtController.getProject(actionRequest); 
			rowIndex = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_LOGIN_INFO_INDEX);
			
			loginSettingList = (ArrayList<ArrayList<VexLoginSettingItem>>) pSession.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
			loginSettingList.remove(rowIndex);

			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			pSession.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
			this.viewRegisterScanAutoCrawlingSetting(actionRequest,actionResponse);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}

			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				pSession.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
				this.viewRegisterScanAutoCrawlingSetting(actionRequest,actionResponse);
			}
		}
	}
	
	public void editLoginInformation (ActionRequest actionRequest, ActionResponse actionResponse) {		
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession();
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		int rowIndex = PortalConstants.INT_ZERO;
		ArrayList <ArrayList<VexLoginSettingItem>> loginSettingList = new ArrayList<ArrayList<VexLoginSettingItem>>();
		ArrayList <VexLoginSettingItem> updateloginInfo = new ArrayList<>();
			
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}

		log.info(PortalMessages.USER_EVENT_UPDATE_LOGIN_INFORMATION, lUserId);

		try {
			project = VexProjectMgmtController.getProject(actionRequest); 
			rowIndex = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_LOGIN_INFO_INDEX);
			updateloginInfo = VexScanMgmtController.addUpdateLoginSettingList(actionRequest);
			
			loginSettingList = (ArrayList<ArrayList<VexLoginSettingItem>>) pSession.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
			loginSettingList.remove(rowIndex);
			loginSettingList.add(rowIndex, updateloginInfo);

			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			pSession.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
			this.viewRegisterScanAutoCrawlingSetting(actionRequest,actionResponse);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}

			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else if (e.getErrorCode().equals(PortalErrors.UPDATE_LOGIN_SETTING_FAILED)){
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_LOGIN_FORM_JSP);
			} else {
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				pSession.setAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST, loginSettingList);
				this.acquireLoginInformationDetailSetting(actionRequest,actionResponse);
			}
		}
	}
	
	public void viewEditLoginInformation (ActionRequest actionRequest, ActionResponse actionResponse) {		
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession();
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		int rowIndex = PortalConstants.INT_ZERO;
		ArrayList <ArrayList<VexLoginSettingItem>> loginSettingList = new ArrayList<ArrayList<VexLoginSettingItem>>();
		List<VexLoginSettingItem> loginParams = new ArrayList<VexLoginSettingItem>();
		boolean isFormParamsVisible = PortalConstants.TRUE;
	
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}

		log.info(PortalMessages.USER_EVENT_VIEW_REGISTER_SCAN_LOGIN_SETTING, lUserId);

		try {
			project = VexProjectMgmtController.getProject(actionRequest); 
			rowIndex = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_LOGIN_INFO_INDEX);
			loginSettingList = (ArrayList<ArrayList<VexLoginSettingItem>>) pSession.getAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
			
			for(VexLoginSettingItem item : loginSettingList.get(rowIndex)){
					loginParams.add(item);
			}	

			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_INFO_INDEX, rowIndex);
			actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
			actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_LOGIN_FORM_JSP);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}

			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_INFO_INDEX, rowIndex);
				actionRequest.setAttribute(PortalConstants.PARAM_LOGIN_PARAMETERS, loginParams);
				actionRequest.setAttribute(PortalConstants.PARAM_SHOW_DISPLAY_FORM_PARAMS, isFormParamsVisible);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_LOGIN_FORM_JSP);
			}
		}
	}
	
	public void registerScanDetailSetting (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		PortletSession pSession = actionRequest.getPortletSession();
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Project project = null;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_REGISTER_AUTOCRAWLING_SETTING, lUserId);

		int iUserAction = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_USER_ACTION);
		
		try {				
				project = VexProjectMgmtController.getProject(actionRequest);
				
				if (VexScanMgmtController.addVexScanDetailSetting(actionRequest, iUserAction, lUserId)) {					
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.REGISTER_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.VEX_REGISTER_SCAN_FAILED);
					}
				}
			
			pSession.removeAttribute(PortalConstants.PARAM_PROJECT_ADVANCED_SCAN_SETTING);
			pSession.removeAttribute(PortalConstants.PARAM_SCAN_NAME); 
			pSession.removeAttribute(PortalConstants.PARAM_PROJECT_TARGET_INFORMATION);
			pSession.removeAttribute(PortalConstants.PARAM_AUTOCRAWL_SETTING);
			pSession.removeAttribute(PortalConstants.PARAM_LOGIN_SETTING_LIST);
			pSession.removeAttribute(PortalConstants.PARAM_LOGIN_INFO_NUM_OF_PARAMS);
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG,
						e.getErrorMessage());

				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)
					|| e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)
					|| e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_AUTOCRAWLING_SETTING_JSP);
			}
		}
	}
	
	public void viewEditProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		List<Organization> organizationList = new ArrayList<Organization>();
		Object oUserId = null;
		Object oUserRole = null;
		long lUserId = PortalConstants.LONG_ZERO;
		int iUserRole = PortalConstants.INT_ZERO;
		int iUserAction = PortalConstants.INT_ZERO;
		Project project = null;
		List<ProjectUsersItem> projectUsersList = new ArrayList<ProjectUsersItem>();
		List<User> orgUsersList = new ArrayList<User>();
		boolean bIsProjectUser = false;
		List<User> userList = new ArrayList<User>();
		List<ProjectItem> targetURLs = null;
		List<ProjectItem> productionEnvURLs = null;
		List<ApiSignatureSetInfo> webInspectionSignatureSetList =  new ArrayList<ApiSignatureSetInfo>();
		List<ApiSignatureSetInfo> serverFilesSignatureSetList =  new ArrayList<ApiSignatureSetInfo>();
		List<ApiSignatureSetInfo> serverSettingsSignatureSetList =  new ArrayList<ApiSignatureSetInfo>();
		ApiInitInfo oInitInfo = VexScanMgmtController.setLoginCredentials();
		GetSignatureSetList getSignatureSetList = new GetSignatureSetList(oInitInfo);
		
		try {
			HttpSession session = ControllerHelper.getHttpSession(actionRequest);
			oUserId = session.getAttribute(PortalConstants.USER_ID);
			oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			iUserAction = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_USER_ACTION);
			webInspectionSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_WEB_SCAN, oInitInfo);
			serverFilesSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_FILES, oInitInfo);
			serverSettingsSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_SETTINGS, oInitInfo);
			webInspectionSignatureSetList = getSignatureSetList.execute(ApiAuditType.TYPE_WEB_SCAN);
			serverFilesSignatureSetList = getSignatureSetList.execute(ApiAuditType.TYPE_SERVER_FILES);
			serverSettingsSignatureSetList = getSignatureSetList.execute(ApiAuditType.TYPE_SERVER_SETTINGS);
			
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
			}
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			if (iUserAction == PortalConstants.USER_EVENT_VIEW_CREATE_PROJECT) {
				log.info(PortalMessages.USER_EVENT_VIEW_CREATE_PROJECT, lUserId);
			} else {
				log.info(PortalMessages.USER_EVENT_VIEW_PROJECT_CHANGE, lUserId);
			}
			
			if (iUserAction == PortalConstants.USER_EVENT_VIEW_PROJECT_CHANGE) {
				project = VexProjectMgmtController.getProject(actionRequest);
				targetURLs = VexProjectMgmtController.getProjectURLs(project, PortalConstants.PARAM_PROJECT_TARGET_URLS);
				productionEnvURLs = VexProjectMgmtController.getProjectURLs(project, PortalConstants.PARAM_PROJECT_PRODUCTION_ENVIRONMENT_URLS);
				
				if (!CommonUtil.isObjectNull(project)) {
					projectUsersList = VexProjectMgmtController.getProjectUsers(actionRequest);
					
					orgUsersList = ControllerHelper.getOrganizationUsers(project.getOwnerGroup());
					
					for (User user : orgUsersList) {
						bIsProjectUser = false;
						for (ProjectUsersItem projectUsers : projectUsersList) {
							if (user.getUserId() == projectUsers.getUserId()) {
								bIsProjectUser = true;
								break;
							}
						}
						
						if (!bIsProjectUser) {
							user.setFirstName(HtmlUtil.escapeAttribute(user.getFirstName()));
							userList.add(user);
						}
					}
				}
			}
			
			if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
				organizationList = ControllerHelper.getOrganizations(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
			} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
				organizationList = ControllerHelper.getUserOrganizations(lUserId , PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
			} else if (iUserRole == UserRole.GEN_USER.getInteger()) {
				if (CommonUtil.isObjectNull(project)) {
					User user = ControllerHelper.getUser(lUserId);
					ProjectUsersItem projectUsersItem = new ProjectUsersItem();
					projectUsersItem.setUserId(user.getUserId());
					projectUsersItem.setEmailAddress(user.getEmailAddress());
					projectUsersItem.setUserName(HtmlUtil.escapeAttribute(user.getFirstName()));
					
					projectUsersList.add(projectUsersItem);
				}
				
				organizationList = ControllerHelper.getUserOrganizations(lUserId, PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
			}
			
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionRequest.setAttribute(PortalConstants.PARAM_ORGANIZATION_LIST, organizationList);
			actionRequest.setAttribute(PortalConstants.PARAM_ORG_USERS_LIST, userList);
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_USERS_LIST, projectUsersList);
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_TARGET_URLS, targetURLs);
			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_PRODUCTION_ENVIRONMENT_URLS, productionEnvURLs);
			actionRequest.setAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST, webInspectionSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST, serverFilesSignatureSetList);
			actionRequest.setAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST, serverSettingsSignatureSetList);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_PROJECT_REGISTRATION_JSP);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
			
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				if (iUserAction == PortalConstants.USER_EVENT_VIEW_PROJECT_CHANGE) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_PROJECT_REGISTRATION_JSP);
				}
			} else if (ubspe.getErrorCode().equals(PortalErrors.UPDATE_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.UPDATE_PROJECT_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.INVALID_PROJECT_ID) ||
					ubspe.getErrorCode().equals(PortalErrors.NO_SUCH_PROJECT_EXCEPTION) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			} else {
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_PROJECT_REGISTRATION_JSP);
			}
		}
	}
	
	public void addUpdateProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		DateFormat formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
		Date projectEndDate = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		int iUserAction = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_USER_ACTION);
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (iUserAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
			log.info(PortalMessages.USER_EVENT_ADD_PROJECT, lUserId);
		} else if (iUserAction == PortalConstants.USER_EVENT_UPDATE_PROJECT) {
			log.info(PortalMessages.USER_EVENT_UPDATE_PROJECT, lUserId);
		}
		
		try {
			this.executeProjectAction(actionRequest, actionResponse, iUserAction);
		} catch (UBSPortalException e) {
			Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
			int iUserRole = PortalConstants.INT_ZERO;
			int iFieldNumber = PortalConstants.INT_ZERO;
			
			if (!CommonUtil.isObjectNull(oUserRole)) {
				iUserRole = Integer.parseInt(oUserRole.toString());
			}
			
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				String strErrorMessage = e.getErrorMessage();
				
				if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
					if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
						int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
						String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
						strErrorMessage = strErrorMessage.substring(index);
						
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, strErrorMessageKey);
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, strErrorMessage);
						}
					}
				}
			}
			
			iFieldNumber = this.getFieldNumber(e.getErrorCode(), iUserAction);
			
			try {
				HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
				HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
				
				long lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
				String caseNumber = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NUMBER).trim();
				String projectName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
				long ownerGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_OWNER_GROUP);
				String selectedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
				String [] selectedUserArr = selectedUsers.split(PortalConstants.DELIMITER_COMMA);
				List<Organization> organizationList = new ArrayList<Organization>();
				List<ProjectUsersItem> projectUsersList = new ArrayList<ProjectUsersItem>();
				List<User> userList = new ArrayList<User>();
				List<User> orgUsersList = new ArrayList<User>();
				boolean bIsProjectUser = false;
				String strDate = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE);
				String strUserListSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USER_LIST_SORT_ORDER);
				String strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
				String strCaseName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NAME).trim();
				List<ProjectItem> targetURLs = VexProjectMgmtController.updateProjectTargerUrlForValidation(actionRequest);
				List<ProjectItem> productionEnvURLs = VexProjectMgmtController.updateProjectProductionEnvUrlForValidation(actionRequest);
				
				ApiInitInfo oInitInfo = VexScanMgmtController.setLoginCredentials();
				List<ApiSignatureSetInfo> webInspectionSignatureSetList =  VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_WEB_SCAN, oInitInfo);
				List<ApiSignatureSetInfo> serverFilesSignatureSetList =  VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_FILES, oInitInfo);
				List<ApiSignatureSetInfo> serverSettingsSignatureSetList = VexScanMgmtController.getSignatureList(ApiAuditType.TYPE_SERVER_SETTINGS, oInitInfo);
								
				if (!CommonUtil.isStringNullOrEmpty(strDate) && !strDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					projectEndDate = formatter.parse(strDate);
				}
				
				Project project = ProjectLocalServiceUtil.createProjectObj();
				project.setProjectId(lProjectId);
				project.setCaseNumber(caseNumber);
				project.setProjectName(projectName);
				project.setOwnerGroup(ownerGroup);
				project.setProjectEndDate(projectEndDate);
				project.setCaseName(strCaseName);
				
				if (!e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					if (iUserRole == UserRole.OVERALL_ADMIN.getInteger()) {
						organizationList = ControllerHelper.getOrganizations(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
					} else if (iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
						organizationList = ControllerHelper.getUserOrganizations(lUserId, PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
					}
					
					if (ownerGroup != PortalConstants.LONG_ZERO) {
						orgUsersList = ControllerHelper.getOrganizationUsers(ownerGroup);
					}
					
					for (int i = 0; (i < selectedUserArr.length && (selectedUsers != null && !selectedUsers.equals(PortalConstants.STRING_EMPTY))); i++) {
						for (User user : orgUsersList) {
							if (user.getUserId() == Long.parseLong(selectedUserArr[i])) {
								ProjectUsersItem item = new ProjectUsersItem();
								
								item.setUserId(user.getUserId());
								item.setUserName(HtmlUtil.escapeAttribute(user.getFirstName()));
								item.setEmailAddress(user.getEmailAddress());
								
								projectUsersList.add(item);
								break;
							}
						}
					}
					
					if (!CommonUtil.isListNullOrEmpty(orgUsersList)) {
						for (User user : orgUsersList) {
							bIsProjectUser = false;
							for (ProjectUsersItem projectUsers : projectUsersList) {
								if (user.getUserId() == projectUsers.getUserId()) {
									bIsProjectUser = true;
									break;
								}
							}
							
							if (!bIsProjectUser) {
								user.setFirstName(HtmlUtil.escapeAttribute(user.getFirstName()));
								userList.add(user);
							}
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strUserListSortOrder)) {
						List<ProjectUsersItem> sortableUsers = new ArrayList<ProjectUsersItem>(projectUsersList);
						
						Collections.sort(sortableUsers, new Comparator<ProjectUsersItem>() {
							@Override
							public int compare(ProjectUsersItem u1, ProjectUsersItem u2) {
								int iCompare = 0;
								
								iCompare = u1.getUserName().compareToIgnoreCase(u2.getUserName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strUserListSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						projectUsersList = new ArrayList<ProjectUsersItem>();
						projectUsersList = sortableUsers;
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
						List<User> sortableUsers = new ArrayList<User>(userList);
						
						Collections.sort(sortableUsers, new Comparator<User>() {
							@Override
							public int compare(User u1, User u2) {
								int iCompare = 0;
								
								iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						userList = new ArrayList<User>();
						userList = sortableUsers;
					}
				}
				
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
				actionRequest.setAttribute(PortalConstants.PARAM_ORGANIZATION_LIST, organizationList);
				actionRequest.setAttribute(PortalConstants.PARAM_ORG_USERS_LIST, userList);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_USERS_LIST, projectUsersList);
				actionRequest.setAttribute(PortalConstants.PARAM_FIELD_NUMBER, iFieldNumber);
				actionRequest.setAttribute(PortalConstants.PARAM_USER_LIST_SORT_ORDER, strUserListSortOrder);
				actionRequest.setAttribute(PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER, strAvailableUsersSortOrder);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_TARGET_URLS, targetURLs);
				actionRequest.setAttribute(PortalConstants.PARAM_PROJECT_PRODUCTION_ENVIRONMENT_URLS, productionEnvURLs);
				actionRequest.setAttribute(PortalConstants.PARAM_WEB_INSPECTION_SIGNATURE_SET_LIST, webInspectionSignatureSetList);
				actionRequest.setAttribute(PortalConstants.PARAM_SERVER_FILES_SIGNATURE_SET_LIST, serverFilesSignatureSetList);
				actionRequest.setAttribute(PortalConstants.PARAM_SERVER_SETTINGS_SIGNATURE_SET_LIST, serverSettingsSignatureSetList);
				
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_PROJECT_REGISTRATION_JSP);
				} catch (UBSPortalException e1) {
					if (e1.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_PROJECT_REGISTRATION_JSP);
					}
				} catch (ParseException e1) {
					// do nothing
				}
		}
	}
	
	public void deleteProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_DELETE_PROJECT, lUserId);
		
		try {
			this.executeProjectAction(actionRequest, actionResponse, PortalConstants.USER_EVENT_DELETE_PROJECT);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					String strErrorMessage = ubspe.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
			}
			
			if (ubspe.getErrorCode().equals(PortalErrors.DELETE_PROJECT_USER_NO_RIGHTS)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			}
			
			JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
			
			if (!ubspe.getErrorCode().equals(PortalErrors.DELETE_PROJECT_USER_NO_RIGHTS)) {
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(jsonArray);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				}
			}
		}
	}
	
	public void completeProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oIsFromSearch = session.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_COMPLETE_PROJECT, lUserId);
		
		try {
			this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_COMPLETE_PROJECT);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.COMPLETE_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.COMPLETE_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.COMPLETE_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION) ||
					e.getErrorCode().equals(PortalErrors.COMPLETE_PROJECT_USER_NO_RIGHTS)) {
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, true);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException ioe) {
					
				}
			}
		}
	}
	
	public void openProject (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oIsFromSearch = session.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_OPEN_PROJECT, lUserId);
		
		try {
			this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_OPEN_PROJECT);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.OPEN_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.OPEN_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.OPEN_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION) ||
					e.getErrorCode().equals(PortalErrors.OPEN_PROJECT_USER_NO_RIGHTS)) {
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, true);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}
	}
	
	public void searchProjects (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_SEARCH_PROJECT, lUserId);
		
		this.getProjects(actionRequest, actionResponse, PortalConstants.USER_EVENT_SEARCH_PROJECT);
	}
	
	public void clearFilterProjects (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_CLEAR_SEARCH_PROJECT, lUserId);
		
		this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
	}
		
	public void viewScanList (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		PortletSession pSession = actionRequest.getPortletSession();
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_SCAN_LIST, lUserId);
		
		try {
			pSession.removeAttribute(PortalConstants.PARAM_SCAN_ID);
			this.getScans(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_SCAN_LIST);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorMessage());
					}
				}
			}
			
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
		}
	}
	
	public void viewEntireScanList (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_ENTIRE_SCAN_LIST, lUserId);
		
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_VIEW_ENTIRE_SCAN_LIST, true);
		
		try {
			this.getEntireScans(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_ENTIRE_SCAN_LIST);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_ENTIRE_SCAN_LIST_JSP);
			} else {
				if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorMessage());
					}
				}
				
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			}
		}
	}
	
	public void viewScanRegistration (ActionRequest actionRequest, ActionResponse actionResponse) {
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;

		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_CREATE_SCAN, lUserId);
		
		try {
			project = VexProjectMgmtController.getProject(actionRequest);

			actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_SCAN_REGISTRATION_JSP);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_SCAN_REGISTRATION_JSP);
			}
		}
	}
	
	public void addScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		Project project = null;
		UploadPortletRequest uploadRequest = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_ADD_SCAN, lUserId);
		
		try {
			uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			
			this.executeScanAction(actionRequest, actionResponse, uploadRequest, PortalConstants.USER_EVENT_ADD_SCAN);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				String strErrorMessage = ubspe.getErrorMessage();
				
				if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
					if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
						int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
						String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
						strErrorMessage = strErrorMessage.substring(index);
						
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, strErrorMessageKey);
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, strErrorMessage);
						}
					}
				}
			}

			int iFieldNumber = 0;
			
			iFieldNumber = this.getFieldNumber(ubspe.getErrorCode(), PortalConstants.USER_EVENT_ADD_SCAN);
			
			if (ubspe.getErrorCode().equals(PortalErrors.ADD_SCAN_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.ADD_SCAN_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.ADD_SCAN_PROJECT_TYPE_INVALID)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				try {
					if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						project = VexProjectMgmtController.getProject(actionRequest);
					}
				
					actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
					actionRequest.setAttribute(PortalConstants.PARAM_FIELD_NUMBER, iFieldNumber);
					
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_SCAN_REGISTRATION_JSP);	
				} catch (UBSPortalException e) {
					if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
						
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, e.getErrorCode());
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, e.getErrorMessage());
						}
					}
				}
			}
		}
	}
	
	public void uploadScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		Project project = null;
		UploadPortletRequest uploadRequest = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		PortletSession pSession = actionRequest.getPortletSession(); 
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_UPLOAD_SCAN, lUserId);
		Scan uploadScan = null;
		try {
			uploadScan = (Scan) pSession.getAttribute(PortalConstants.PARAM_SCAN);
			uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			
			this.executeScanAction(actionRequest, actionResponse, uploadRequest, PortalConstants.USER_EVENT_UPLOAD_SCAN);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				String strErrorMessage = ubspe.getErrorMessage();
				
				if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
					if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
						int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
						String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
						strErrorMessage = strErrorMessage.substring(index);
						
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, strErrorMessageKey);
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, strErrorMessage);
						}
					}
				}
			}
			int iFieldNumber = 0;
			iFieldNumber = this.getFieldNumber(ubspe.getErrorCode(), PortalConstants.USER_EVENT_UPLOAD_SCAN);
			
			if (ubspe.getErrorCode().equals(PortalErrors.UPLOAD_SCAN_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.UPLOAD_SCAN_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.UPLOAD_SCAN_PROJECT_TYPE_INVALID)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				try {
					if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						project = VexProjectMgmtController.getProject(actionRequest);
					}
					
					if(ubspe.getErrorCode().equals(PortalErrors.UPLOAD_SCAN_FILE_ALREADY_UPLOADED)){
						if(null !=uploadScan){
							uploadScan.setFileName(ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_NAME));
						}
						actionRequest.setAttribute(PortalConstants.PARAM_SCAN, uploadScan);
					}
					actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
					actionRequest.setAttribute(PortalConstants.PARAM_FIELD_NUMBER, iFieldNumber);
					
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_SCAN_REGISTRATION_JSP);	
				} catch (UBSPortalException e) {
					if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
						
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, e.getErrorCode());
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, e.getErrorMessage());
						}
					}
				}
			}
		}
	}
	
	public void importScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		Project project = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_IMPORT_SCAN, lUserId);
		
		try {
			this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_IMPORT_SCAN);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				String strErrorMessage = ubspe.getErrorMessage();
				
				if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
					if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
						int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
						String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
						strErrorMessage = strErrorMessage.substring(index);
						
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, strErrorMessageKey);
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, strErrorMessage);
						}
					}
				}
			}
			int iFieldNumber = 0;
			iFieldNumber = this.getFieldNumber(ubspe.getErrorCode(), PortalConstants.USER_EVENT_IMPORT_SCAN);
			
			if (ubspe.getErrorCode().equals(PortalErrors.IMPORT_SCAN_PROJECT_DOES_NOT_EXIST) ||
					ubspe.getErrorCode().equals(PortalErrors.IMPORT_SCAN_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.IMPORT_SCAN_PROJECT_TYPE_INVALID)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				try {
					if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						project = VexProjectMgmtController.getProject(actionRequest);
					}
				
					actionRequest.setAttribute(PortalConstants.PARAM_PROJECT, project);
					actionRequest.setAttribute(PortalConstants.PARAM_FIELD_NUMBER, iFieldNumber);
					
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_SCAN_REGISTRATION_JSP);	
				} catch (UBSPortalException e) {
					if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
						
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, e.getErrorCode());
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, e.getErrorMessage());
						}
					}
				}
			}
		}
	}
	
	public void abortCrawl (ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_ABORT_CRAWL, PortalConstants.ABORT_CRAWL_SUCCESSFUL, PortalMessages.ABORT_CRAWL_FAILED, PortalMessages.USER_EVENT_ABORT_CRAWL);
	}
	
	public void reCrawl (ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_RECRAWL, PortalConstants.RECRAWL_SUCCESSFUL, PortalMessages.RECRAWL_FAILED, PortalMessages.USER_EVENT_RECRAWL);
	}
	
	public void interruptedCrawl(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_INTERRUPTED_CRAWL,PortalConstants.INTERRUPTED_CRAWL_SUCCESSFUL, PortalMessages.INTERRUPTED_CRAWL_FAILED, PortalMessages.USER_EVENT_INTERRUPTED_CRAWL);
	}
	
	public void cancelCrawl(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_CANCEL_CRAWL,PortalConstants.CANCEL_CRAWL_SUCCESSFUL, PortalMessages.CANCEL_CRAWL_FAILED, PortalMessages.USER_EVENT_CANCEL_CRAWL);
	}
	
	public void restartCrawl(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_RESTART_CRAWL,PortalConstants.RESTART_CRAWL_SUCCESSFUL, PortalMessages.RESTART_CRAWL_FAILED, PortalMessages.USER_EVENT_RESTART_CRAWL);
	}
	
	public void reexecuteVexScan (ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_REEXECUTE_SCAN, PortalConstants.REEXECUTE_SCAN_SUCCESSFUL, PortalMessages.REEXECUTE_SCAN_FAILED, PortalMessages.USER_EVENT_REEXECUTE_SCAN);
	}
	
	public void stopVexScan (ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_STOP_SCAN, PortalConstants.STOP_SCAN_SUCCESSFUL, PortalMessages.STOP_SCAN_FAILED, PortalMessages.USER_EVENT_STOP_SCAN);
	}
	
	public void interruptedScan(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_INTERRUPTED_SCAN,PortalConstants.INTERRUPTED_SCAN_SUCCESSFUL, PortalMessages.INTERRUPTED_SCAN_FAILED, PortalMessages.USER_EVENT_INTERRUPTED_SCAN);
	}
	
	public void cancelScan(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_CANCEL_SCAN,PortalConstants.CANCEL_SCAN_SUCCESSFUL, PortalMessages.CANCEL_SCAN_FAILED, PortalMessages.USER_EVENT_CANCEL_SCAN);
	}
	
	public void restartScan(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_RESTART_SCAN,PortalConstants.RESTART_SCAN_SUCCESSFUL, PortalMessages.RESTART_SCAN_FAILED, PortalMessages.USER_EVENT_RESTART_SCAN);
	}
	
	public void copyCrawlingAndScan(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN,PortalConstants.COPY_CRAWL_RESULT_AND_SCAN_SUCCESSFUL, PortalMessages.COPY_CRAWL_RESULT_AND_SCAN_FAILED, PortalMessages.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN);
	}
	
	public void updateScanPatrolSettings(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_PERFORM_SCAN,PortalConstants.UPDATE_SCAN_SUCCESSFUL, PortalMessages.UPDATE_SCAN_FAILED, PortalMessages.USER_EVENT_PERFORM_SCAN);
	}
	
	public void copyAndScanPatrolSettings(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		copyCrawlingAndScan(actionRequest, actionResponse);
	}
	
	public void deleteVexScan(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		crawlScanHelper(actionRequest, actionResponse, PortalConstants.USER_EVENT_DELETE_SCAN,PortalConstants.DELETE_SCAN_SUCCESSFUL, PortalMessages.DELETE_SCAN_FAILED, PortalMessages.USER_EVENT_DELETE_SCAN);
	}
	
	private void crawlScanHelper(ActionRequest actionRequest, ActionResponse actionResponse, int iUserAction, String strSuccess, String strFail, String scanName) throws UBSPortalException{
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		ApiInitInfo info = null; 
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
			
			info = VexScanMgmtController.setLoginCredentials();
			log.info(scanName, lUserId);
		}
		
		try {
			if (VexScanMgmtController.manageVexCrawlScan(actionRequest, iUserAction, info)) {
				if (SessionMessages.isEmpty(actionRequest)) {
					SessionMessages.add(actionRequest, strSuccess);
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, strFail);
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(iUserAction == PortalConstants.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN || iUserAction == PortalConstants.USER_EVENT_PERFORM_SCAN){
			int iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
			if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST);
			}else{
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			}
		}else{
			formDetailJson(actionResponse);
		}
	}
	
	public void stopScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = PortalConstants.INT_ZERO;
		PortletSession pSession = actionRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_STOP_SCAN, lUserId);
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_STOP_SCAN, true);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.executeEntireScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_STOP_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					String strErrorMessage = e.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.STOP_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.STOP_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		} else {
			try {
				this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_STOP_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					String strErrorMessage = e.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.STOP_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.STOP_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		}
	}
	
	public void reexecuteScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = PortalConstants.INT_ZERO;
		PortletSession pSession = actionRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_REEXECUTE_SCAN, lUserId);
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_REEXECUTE_SCAN, true);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.executeEntireScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_REEXECUTE_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					String strErrorMessage = e.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		} else {
			try {
				this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_REEXECUTE_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					String strErrorMessage = e.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.REEXECUTE_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		}
	}
	
	public void regenerateReport (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = PortalConstants.INT_ZERO;
		PortletSession pSession = actionRequest.getPortletSession(false);
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		log.info(PortalMessages.USER_EVENT_REGENERATE_REPORT, lUserId);
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_REGENERATE_REPORT, true);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.executeEntireScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_REGENERATE_REPORT);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					String strErrorMessage = e.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.REGENERATE_REPORT_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.REGENERATE_REPORT_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		} else {
			try {
				this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_REGENERATE_REPORT);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					String strErrorMessage = e.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.REGENERATE_REPORT_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.REGENERATE_REPORT_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		}
	}
		
	public void deleteScan (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = PortalConstants.INT_ZERO;
		PortletSession pSession = actionRequest.getPortletSession(false);
		Object oIsFromSearch = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, PortletSession.APPLICATION_SCOPE);
		boolean bIsFromSearch = false;
		
		if (!CommonUtil.isObjectNull(oIsFromSearch)) {
			bIsFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
			
			if (bIsFromSearch) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, bIsFromSearch);
			}
		}
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_DELETE_SCAN, true);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.executeEntireScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_DELETE_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					String strErrorMessage = e.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		} else {
			try {
				this.executeScanAction(actionRequest, actionResponse, null, PortalConstants.USER_EVENT_DELETE_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					String strErrorMessage = e.getErrorMessage();
					
					if (!CommonUtil.isStringNullOrEmpty(strErrorMessage)) {
						if (strErrorMessage.contains(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS)) {
							int index = strErrorMessage.indexOf(PortalConstants.STRING_SPACE_OPEN_PARENTHESIS);
							String strErrorMessageKey = strErrorMessage.substring(PortalConstants.INT_ZERO, index);
							strErrorMessage = strErrorMessage.substring(index);
							
							actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_API_CALL_ERROR_MSG, strErrorMessage);
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessageKey);
							}
						} else {
							if (SessionErrors.isEmpty(actionRequest)) {
								SessionErrors.add(actionRequest, strErrorMessage);
							}
						}
					}
				}
				
				JSONObject formDetailJson = JSONFactoryUtil.createJSONObject();
				
				if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
						e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID)) {
					formDetailJson.put(PortalConstants.REDIRECT, true);
				} else if (e.getErrorCode().equals(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS)) {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				} else {
					formDetailJson.put(PortalConstants.REDIRECT, false);
				}
				
				if (!e.getErrorCode().equals(PortalErrors.DELETE_SCAN_USER_NO_RIGHTS)) {
					try {
						HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
						response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
						PrintWriter writer = response.getWriter();
						writer.print(formDetailJson);
						response.flushBuffer();
					} catch (IOException e1) {
						// do nothing
					}
				}
			}
		}
	}
	
	public void searchScans (ActionRequest actionRequest, ActionResponse actionResponse) {
		int iScreenNo = PortalConstants.INT_ZERO;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_SEARCH_SCAN, lUserId);
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			try {
				this.getEntireScans(actionRequest, actionResponse, PortalConstants.USER_EVENT_SEARCH_SCAN);
			} catch (UBSPortalException e) {
				if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorCode());
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, e.getErrorMessage());
					}
					
					if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST);
					}
				}
			}
		} else {
			try {
				this.getScans(actionRequest, actionResponse, PortalConstants.USER_EVENT_SEARCH_SCAN);
			} catch (UBSPortalException ubspe) {
				if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
					
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorCode());
					}
				} else {
					if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, ubspe.getErrorMessage());
						}
					}
				}
				
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			}
		}
	}
	
	public void clearFilterScans (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		int iScreenNo = PortalConstants.INT_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
		
		log.info(PortalMessages.USER_EVENT_CLEAR_SEARCH_SCAN, lUserId);
		
		actionRequest.setAttribute(PortalConstants.PARAM_USER_ACTION, PortalConstants.USER_EVENT_CLEAR_SEARCH_SCAN);
		
		if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST);
		} else {
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
		}
	}
	
	public void clearFilterDetectionResult (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_CLEAR_DETECTION_RESULT, lUserId);
		actionRequest.setAttribute(PortalConstants.PARAM_USER_ACTION, PortalConstants.USER_EVENT_CLEAR_DETECTION_RESULT);
		
		this.getDetectionResults(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_DETECTION_RESULT);
	}
	
	private void getProjects(ActionRequest actionRequest, ActionResponse actionResponse, int userAction) {
		List<Object> projectList = null;
		Map<String, Object> searchedProject = new HashMap<String, Object>();
		
		try {
			projectList = VexProjectMgmtController.getProjects(actionRequest, userAction);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
			
			actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_HAS_ERROR, true);
			actionRequest.getPortletSession().removeAttribute(PortalConstants.PARAM_ACTION_SEARCH_PROJECT);
		}
		
		if (userAction == PortalConstants.USER_EVENT_SEARCH_PROJECT) {
			searchedProject.put(PortalConstants.PARAM_PROJECT_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_ID).trim());
			searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, ParamUtil.getString(actionRequest, PortalConstants.PARAM_OWNER_GROUP).trim());
			searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NUMBER).trim());
			searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME).trim());
			searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_LOW, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE_LOW).trim());
			searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_HIGH, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_END_DATE_HIGH).trim());
			searchedProject.put(PortalConstants.PARAM_STATUS, ParamUtil.getString(actionRequest, PortalConstants.PARAM_STATUS).trim());
			searchedProject.put(PortalConstants.PARAM_NO_OF_SCANS, ParamUtil.getString(actionRequest, PortalConstants.PARAM_NO_OF_SCANS).trim());
			searchedProject.put(PortalConstants.PARAM_CASE_NAME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_CASE_NAME).trim());
			searchedProject.put(PortalConstants.PARAM_TARGET_URL, ParamUtil.getString(actionRequest, PortalConstants.PARAM_TARGET_URL).trim());
			searchedProject.put(PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PRODUCTION_ENVIRONMENT_URL).trim());
			
		}
		
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_PROJECT_LIST, projectList);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_PROJECT, searchedProject);
		
		if (userAction == PortalConstants.USER_EVENT_SEARCH_PROJECT) {
			actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, true);
		}
		
		actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_PROJECT_LIST_JSP);
	}
	
	private void getScans (ActionRequest actionRequest, ActionResponse actionResponse, int userAction) throws UBSPortalException {
		String strScanId = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String [] strStatusArr = null;
		String strStatus = PortalConstants.STRING_EMPTY;
		String strCxScanId = null;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		DateFormat formatter =  null;
		String strStartUrl = null;
		String strImplementationEnvironment = null;
		String strStartTime = null;
		String strEndTime = null;
		Calendar dteStartTime = null;
		Calendar dteEndTime = null;
		
		strScanId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID).trim();
		strFileName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_FILE_NAME).trim();
		strHashValue = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HASH_VALUE).trim();
		strScanManager = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
		strScanRegDateLow = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
		strScanRegDateHigh = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
		strStatusArr = ParamUtil.getParameterValues(actionRequest, PortalConstants.PARAM_STATUS);
		strCxScanId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CX_SCAN_ID).trim();
		strStartUrl = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_START_URL).trim();
		strImplementationEnvironment = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
		strStartTime= ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_START_TIME).trim();
		strEndTime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_END_TIME).trim();
		
		String strStartHour = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_START_HOUR).trim();
		String strEndHour = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_END_HOUR).trim();
		
		if(!CommonUtil.isStringNullOrEmpty(strStartHour)){
			strStartTime+=" "+strStartHour;
		}
		
		if(!CommonUtil.isStringNullOrEmpty(strEndHour)){
			strEndTime +=" "+strEndHour;
		}
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			long lProjectId = ParamUtil.getLong(actionRequest, PortalConstants.PARAM_PROJECT_ID);
			
			if (lProjectId == PortalConstants.LONG_ZERO) {
				throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
			}
			
			actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_PROJECT_ID, lProjectId);
			
			if (userAction == PortalConstants.USER_EVENT_SEARCH_SCAN) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, true);

				if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
					try {
						long lScanId = Long.parseLong(strScanId);
						
						if (lScanId < PortalConstants.LONG_ZERO) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
					if (!Validator.isAlphanumericName(strHashValue)) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
					
					if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
				}
				
				formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
				try {
					
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
						if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateLow = formatter.parse(strScanRegDateLow);
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
						if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateHigh = Calendar.getInstance();
						dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
						dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteRegDateHigh.set(Calendar.MINUTE, 59);
						dteRegDateHigh.set(Calendar.SECOND, 59);
					}
					
					if (!CommonUtil.isObjectNull(dteRegDateLow)
							&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
						if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
					}
				} catch (ParseException e) {
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
				}
				
					formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
					if (!CommonUtil.isStringNullOrEmpty(strStartTime) && !strStartTime.trim().equals("00:00")) {
							
						if (strStartTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
						}
						try {
							dteStartTime = Calendar.getInstance();
							dteStartTime.setTime(formatter.parse(strStartTime));
						}catch (ParseException e) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
						}
					} 
										
					if (!CommonUtil.isStringNullOrEmpty(strEndTime)&& !strEndTime.trim().equals("00:00")) {
						
						if (strEndTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
						}
						try {
							dteEndTime = Calendar.getInstance();
							dteEndTime.setTime(formatter.parse(strEndTime));
						}catch (ParseException e) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
						}
					}
					
				
					
					if(strStartTime.trim().equals("00:00")){
						strStartTime = null;
					}
					
					if(strEndTime.trim().equals("00:00")){
						strEndTime = null;
					}
								
				if (!CommonUtil.isObjectNull(strStatusArr)) {
					for (String status : strStatusArr) {
						strStatus += status + PortalConstants.COMMA;
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
					try {
						long lCxScanId = Long.parseLong(strCxScanId);
						
						if (lCxScanId < PortalConstants.LONG_ZERO) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
					}
				}
				
				Map<String, Object> searchedScan = new HashMap<String, Object>();
				
				if (!CommonUtil.isObjectNull(dteStartTime) && !CommonUtil.isObjectNull(dteEndTime)) {
					if (dteStartTime.getTime().after(dteEndTime.getTime())) {
						searchedScan.put(PortalConstants.PARAM_SCAN_START_TIME, strStartTime);
						searchedScan.put(PortalConstants.PARAM_SCAN_END_TIME, strEndTime);
						actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_AND_END_TIME_INVALID, PortalMessages.START_TIME_AND_END_TIME_INVALID);
					}
				}
				
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
				searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
				searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
				searchedScan.put(PortalConstants.PARAM_SCAN_START_URL, strStartUrl);
				searchedScan.put(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT, strImplementationEnvironment);
				searchedScan.put(PortalConstants.PARAM_SCAN_START_TIME, strStartTime);
				searchedScan.put(PortalConstants.PARAM_SCAN_END_TIME, strEndTime);
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
			} else {
				actionRequest.getPortletSession().removeAttribute(PortalConstants.PARAM_SCAN);
			}
			
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_SCAN_LIST_JSP);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, e.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, e.getErrorMessage());
				}
			}
			
			if (e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					e.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST) ||
					e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
			}
		}
	}
	
	private void getEntireScans(ActionRequest actionRequest, ActionResponse actionResponse, int userAction) throws UBSPortalException {
		String strScanId = null;
		String strProjectName = null;
		String strGroupName = null;
		String strFileName = null;
		String strHashValue = null;
		String strScanManager = null;
		String strScanRegDateLow = null;
		String strScanRegDateHigh = null;
		String [] strStatusArr = null;
		String strStatus = PortalConstants.STRING_EMPTY;
		String strCxScanId = null;
		Date dteRegDateLow = null;
		Calendar dteRegDateHigh = null;
		DateFormat formatter =  null;
		int action = PortalConstants.INT_ZERO;
		String strStartUrl = null;
		String strImplementationEnvironment = null;
		String strStartTime = null;
		String strEndTime = null;
		Calendar dteStartTime = null;
		Calendar dteEndTime = null;
		
		strScanId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID).trim();
		strProjectName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME).trim();
		strGroupName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_OWNER_GROUP).trim();
		strFileName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_FILE_NAME).trim();
		strHashValue = ParamUtil.getString(actionRequest, PortalConstants.PARAM_HASH_VALUE).trim();
		strScanManager = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_MANAGER).trim();
		strScanRegDateLow = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_LOW).trim();
		strScanRegDateHigh = ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_HIGH).trim();
		strStatusArr = ParamUtil.getParameterValues(actionRequest, PortalConstants.PARAM_STATUS);
		strCxScanId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_CX_SCAN_ID).trim();
		strStartUrl = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_START_URL).trim();
		strImplementationEnvironment = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT);
		strStartTime= ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_START_TIME).trim();
		strEndTime = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_END_TIME).trim();
		
		String strStartHour = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_START_HOUR).trim();
		String strEndHour = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_END_HOUR).trim();
		
		if(!CommonUtil.isStringNullOrEmpty(strStartHour)){
			strStartTime+=" "+strStartHour;
		}
		
		if(!CommonUtil.isStringNullOrEmpty(strEndHour)){
			strEndTime +=" "+strEndHour;
		}
		
		action = ParamUtil.getInteger(actionRequest,  PortalConstants.PARAM_USER_ACTION);
		
		try {
			if (!PortletCommonUtil.isDBConnected()) {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
			if (userAction == PortalConstants.USER_EVENT_SEARCH_SCAN) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, true);

				if (!CommonUtil.isStringNullOrEmpty(strScanId)) {
					try {
						long lScanId = Long.parseLong(strScanId);
						
						if (lScanId < PortalConstants.LONG_ZERO) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strProjectName)) {
					if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
					}
					
					if (!ControllerHelper.isCxProjectNameValid(strProjectName)) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_INVALID, PortalMessages.PROJECT_NAME_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strGroupName)) {
					if (CommonUtil.getStringBytes(strGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_GROUP_NAME_TOO_LONG, PortalMessages.GROUP_NAME_TOO_LONG);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strHashValue)) {
					if (!Validator.isAlphanumericName(strHashValue)) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
					
					if (strHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
					}
				}
				
				try {
					formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
					
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateLow)) {
						if (strScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateLow = formatter.parse(strScanRegDateLow);
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strScanRegDateHigh)) {
						if (strScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
						
						dteRegDateHigh = Calendar.getInstance();
						dteRegDateHigh.setTime(formatter.parse(strScanRegDateHigh));
						dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
						dteRegDateHigh.set(Calendar.MINUTE, 59);
						dteRegDateHigh.set(Calendar.SECOND, 59);
					}
					
					if (!CommonUtil.isObjectNull(dteRegDateLow)
							&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
						if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
						}
					}
				} catch (ParseException e) {
					throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
				}
				
				formatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
				if (!CommonUtil.isStringNullOrEmpty(strStartTime) && !strStartTime.trim().equals("00:00")) {
						
					if (strStartTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
					}
					try {
						dteStartTime = Calendar.getInstance();
						dteStartTime.setTime(formatter.parse(strStartTime));
					}catch (ParseException e) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strEndTime)&& !strEndTime.trim().equals("00:00")) {
					
					if (strEndTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
					}
					try {
						dteEndTime = Calendar.getInstance();
						dteEndTime.setTime(formatter.parse(strEndTime));
					}catch (ParseException e) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
					}
				}
				
				if (!CommonUtil.isObjectNull(dteStartTime) && !CommonUtil.isObjectNull(dteEndTime)) {
					if (dteStartTime.getTime().after(dteEndTime.getTime())) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_AND_END_TIME_INVALID, PortalMessages.START_TIME_AND_END_TIME_INVALID);
					}
				}
				
				if(strStartTime.trim().equals("00:00")){
					strStartTime = null;
				}
				
				if(strEndTime.trim().equals("00:00")){
					strEndTime = null;
				}
				
				if (!CommonUtil.isObjectNull(strStatusArr)) {
					for (String status : strStatusArr) {
						strStatus += status + PortalConstants.COMMA;
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(strCxScanId)) {
					try {
						long lCxScanId = Long.parseLong(strCxScanId);
						
						if (lCxScanId < PortalConstants.LONG_ZERO) {
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
						}
					} catch (NumberFormatException e) {
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
					}
				}
				
				Map<String, Object> searchedScan = new HashMap<String, Object>();
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, strScanId);
				searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, strProjectName);
				searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, strGroupName);
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, strFileName);
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, strHashValue);
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, strScanManager);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, strScanRegDateLow);
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, strScanRegDateHigh);
				searchedScan.put(PortalConstants.PARAM_STATUS, strStatus);
				searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, strCxScanId);
				searchedScan.put(PortalConstants.PARAM_SCAN_START_URL, strStartUrl);
				searchedScan.put(PortalConstants.PARAM_SCAN_IMPLEMENTATION_ENVIRONMENT, strImplementationEnvironment);
				searchedScan.put(PortalConstants.PARAM_SCAN_START_TIME, strStartTime);
				searchedScan.put(PortalConstants.PARAM_SCAN_END_TIME, strEndTime);
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
				
			} else {
				actionRequest.getPortletSession().removeAttribute(PortalConstants.PARAM_SCAN);
				
				if (action == PortalConstants.USER_EVENT_CLEAR_SEARCH_SCAN) {
					actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_USER_ACTION, action);
				}
			}
			
			actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_ENTIRE_SCAN_LIST_JSP);
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
			
			if (ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_TYPE_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID) ||
					ubspe.getErrorCode().equals(PortalErrors.GET_PROJECT_PROJECT_DOES_NOT_EXIST)) {
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
			} else {
				if (userAction == PortalConstants.USER_EVENT_VIEW_ENTIRE_SCAN_LIST) {
					actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_ENTIRE_SCAN_LIST_JSP);
				} else {
					this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST);
				}
			}
		}
	}
	
	private void onChangeOwnerGroup(HttpServletRequest originalRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lSelectedGroup = PortalConstants.LONG_ZERO;
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		List<User> usersList = new ArrayList<User>();
		JSONObject formDetailJson = null;
		JSONArray orgUsers = JSONFactoryUtil.createJSONArray();
		JSONArray errorMsg = JSONFactoryUtil.createJSONArray();
		String strAvailableUsersSortOrder = null;
		
		try {
			lSelectedGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_SELECTED_GROUP);
			strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
			
			if (lSelectedGroup > PortalConstants.LONG_ZERO) {
				usersList = ControllerHelper.getOrganizationUsers(lSelectedGroup);
			}
			
			if (!CommonUtil.isListNullOrEmpty(usersList)) {
				if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
					List<User> sortableUsers = new ArrayList<User>(usersList);
					
					Collections.sort(sortableUsers, new Comparator<User>() {
						@Override
						public int compare(User u1, User u2) {
							int iCompare = 0;
							
							iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
							
							if (iCompare == 0) {
								iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
							}
							
							return iCompare;
						}
					});
					
					if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
						Collections.reverse(sortableUsers);
					}
					
					usersList = new ArrayList<User>();
					usersList = sortableUsers;
				}
				
				for (User u : usersList) {
					formDetailJson = JSONFactoryUtil.createJSONObject();
					formDetailJson.put("userId", u.getUserId());
					formDetailJson.put("emailAddress", u.getEmailAddress());
					formDetailJson.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
					orgUsers.put(formDetailJson);
				}
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put("DBConnError", true);
				errorMsg.put(formDetailJson);
			} else {
				throw ubspe;
			}
		}
		
		formDetailJson = JSONFactoryUtil.createJSONObject();
		formDetailJson.put("errorMsg", errorMsg);
		jsonArray.put(formDetailJson);
		
		formDetailJson = JSONFactoryUtil.createJSONObject();
		formDetailJson.put("orgUsers", orgUsers);
		jsonArray.put(formDetailJson);

		try {
			PrintWriter writer = resourceResponse.getWriter();
			writer.print(jsonArray);
		} catch (IOException ioe) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, ioe);
		}
	}
	
	private void filterUsers(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lSelectedGroup = PortalConstants.LONG_ZERO;
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		List<User> usersList = null;
		String strUserId = null;
		String strUserName = null;
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONArray availableUsers = JSONFactoryUtil.createJSONArray();
		JSONArray errorMsg = JSONFactoryUtil.createJSONArray();
		String strSelectedUsers = null;
		String [] strArray = null;
		long [] selectedUsers = null;
		String strAvailableUsersSortOrder = null;
		boolean bWithFilterUsername = false;
		boolean bWithFilterEmailAddress = false;
		List<User> availableUsersList = new ArrayList<User>();
		
		try {
			lSelectedGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_SELECTED_GROUP);
			strUserId = ParamUtil.getString(originalRequest, PortalConstants.USER_ID);
			strUserName = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USERNAME);
			strSelectedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
			strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
			
			strUserName = PortletCommonUtil.convertHexToString(strUserName);
			
			if (!CommonUtil.isStringNullOrEmpty(strUserName)) {
				bWithFilterUsername = true;
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strUserId)) {
				bWithFilterEmailAddress = true;
			}
			
			jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("userIdTooLong", false);
			
			if (CommonUtil.getStringBytes(strUserId) > 75) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("userIdTooLong", true);
			}
			
			errorMsg.put(jsonObject);
			
			jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("userNameTooLong", false);
			
			if (CommonUtil.getStringBytes(strUserName) > 40) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("userNameTooLong", true);
			}
			
			errorMsg.put(jsonObject);
			
			if (!CommonUtil.isStringNullOrEmpty(strSelectedUsers)) {
				strArray = strSelectedUsers.split(PortalConstants.DELIMITER_COMMA);
				
				if (strArray != null) {
					selectedUsers = new long[strArray.length];
					
					for (int i = 0; i < strArray.length; i++) {
						selectedUsers[i] = Long.parseLong(strArray[i]);
					}
				}
			}
			
			if (lSelectedGroup != PortalConstants.LONG_ZERO) {
				usersList = ControllerHelper.getOrganizationUsers(lSelectedGroup);
				
				if (usersList != null) {
					for (User user : usersList) {
						boolean bSelected = false;
						
						if (selectedUsers != null) {
							for (int i = 0; i < selectedUsers.length; i++) {
								if (user.getUserId() == selectedUsers[i]) {
									bSelected = true;
									break;
								}
							}
						}
						
						if (((bWithFilterEmailAddress && user.getEmailAddress().toLowerCase().contains(strUserId.toLowerCase())) ||
								(bWithFilterUsername && user.getFirstName().toLowerCase().contains(strUserName.toLowerCase())))
								&& !bSelected) {
							availableUsersList.add(user);
						} else if (!bWithFilterEmailAddress &&
								!bWithFilterUsername &&
								!bSelected) {
							availableUsersList.add(user);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
						List<User> sortableUsers = new ArrayList<User>(availableUsersList);
						
						Collections.sort(sortableUsers, new Comparator<User>() {
							@Override
							public int compare(User u1, User u2) {
								int iCompare = 0;
								
								iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						availableUsersList = new ArrayList<User>();
						availableUsersList = sortableUsers;
					}
					
					for (User u : availableUsersList) {
						jsonObject = JSONFactoryUtil.createJSONObject();
						jsonObject.put("userId", u.getUserId());
						jsonObject.put("emailAddress", u.getEmailAddress());
						jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
						availableUsers.put(jsonObject);
					}
				}
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("DBConnError", true);
				errorMsg.put(jsonObject);
			} else {
				throw ubspe;
			}
		}
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("errorMsg", errorMsg);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("availableUsers", availableUsers);
		jsonArray.put(jsonObject);
		
		try {
			PrintWriter writer;
			writer = resourceResponse.getWriter();
			writer.print(jsonArray);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		}
	}

	private void addUser(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lSelectedGroup = PortalConstants.LONG_ZERO;
		String strSelectedUsers = null;
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		String [] strArray = null;
		long [] selectedUsers = null;
		List<User> userList = null;
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		JSONArray usersList = JSONFactoryUtil.createJSONArray();
		JSONArray availableUsers = JSONFactoryUtil.createJSONArray();
		JSONArray errorMsg = JSONFactoryUtil.createJSONArray();
		String strUserId = null;
		String strUserName = null;
		String strUserListSortOrder = null;
		String strAvailableUsersSortOrder = null;
		List<User> selectedUserList = new ArrayList<User>();
		List<User> availableUserList = new ArrayList<User>();
		boolean bWithFilterUsername = false;
		boolean bWithFilterEmailAddress = false;
		
		lSelectedGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_SELECTED_GROUP);
		strSelectedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
		strUserId = ParamUtil.getString(originalRequest, PortalConstants.USER_ID).toLowerCase();
		strUserName = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USERNAME).toLowerCase();
		strUserListSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USER_LIST_SORT_ORDER);
		strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
		
		strUserName = PortletCommonUtil.convertHexToString(strUserName);
		
		if (!CommonUtil.isStringNullOrEmpty(strUserName)) {
			bWithFilterUsername = true;
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strUserId)) {
			bWithFilterEmailAddress = true;
		}
		
		try {
			if (!CommonUtil.isStringNullOrEmpty(strSelectedUsers)) {
				strArray = strSelectedUsers.split(PortalConstants.DELIMITER_COMMA);
				
				if (strArray != null) {
					selectedUsers = new long[strArray.length];
					for (int i = 0; i < strArray.length; i++) {
						selectedUsers[i] = Long.parseLong(strArray[i]);
					}
				}
			}
			
			if (lSelectedGroup != PortalConstants.LONG_ZERO) {
				userList = ControllerHelper.getOrganizationUsers(lSelectedGroup);
				
				if (selectedUsers != null) {
					for (int i = 0; i < selectedUsers.length; i++) {
						try {
							UserLocalServiceUtil.getUser(selectedUsers[i]);
						} catch (NoSuchUserException e) {
							jsonObject = JSONFactoryUtil.createJSONObject();
							jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_EXIST, true);
							errorMsg.put(jsonObject);
						} catch (PortalException e) {
							// do nothing
						} catch (SystemException e) {
							// do nothing
						}
						
						if (userList != null) {
							boolean bUserBelongToGroup = false;
							
							for (User user : userList) {
								bUserBelongToGroup = false;
								if (selectedUsers[i] == user.getUserId()) {
									selectedUserList.add(user);
									bUserBelongToGroup = true;
									break;
								}
							}
							
							if (!bUserBelongToGroup) {
								jsonObject = JSONFactoryUtil.createJSONObject();
								jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_BELONG_TO_GROUP, true);
								errorMsg.put(jsonObject);
							}
						}
					}
				}
				
				if (userList != null) {
					for (User user : userList) {
						boolean bSelected = false;
						
						for (User u : selectedUserList) {
							if (user.getUserId() == u.getUserId()) {
								bSelected = true;
								break;
							}
						}
						
						if (((bWithFilterEmailAddress && user.getEmailAddress().toLowerCase().contains(strUserId)) ||
								(bWithFilterUsername && user.getFirstName().toLowerCase().contains(strUserName)))
								&& !bSelected) {
							availableUserList.add(user);
						} else if (!bWithFilterEmailAddress &&
								!bWithFilterUsername &&
								!bSelected) {
							availableUserList.add(user);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strUserListSortOrder)) {
						List<User> sortableUsers = new ArrayList<User>(selectedUserList);
						
						Collections.sort(sortableUsers, new Comparator<User>() {
							@Override
							public int compare(User u1, User u2) {
								int iCompare = 0;
								
								iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strUserListSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						selectedUserList = new ArrayList<User>();
						selectedUserList = sortableUsers;
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
						List<User> sortableUsers = new ArrayList<User>(availableUserList);
						
						Collections.sort(sortableUsers, new Comparator<User>() {
							@Override
							public int compare(User u1, User u2) {
								int iCompare = 0;
								
								iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
								
								if (iCompare == 0) {
									iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
								}
								
								return iCompare;
							}
						});
						
						if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
							Collections.reverse(sortableUsers);
						}
						
						availableUserList = new ArrayList<User>();
						availableUserList = sortableUsers;
					}
					
					for (User u : selectedUserList) {
						jsonObject = JSONFactoryUtil.createJSONObject();
						jsonObject.put("userId", u.getUserId());
						jsonObject.put("emailAddress", u.getEmailAddress());
						jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
						usersList.put(jsonObject);
					}
					
					for (User u : availableUserList) {
						jsonObject = JSONFactoryUtil.createJSONObject();
						jsonObject.put("userId", u.getUserId());
						jsonObject.put("emailAddress", u.getEmailAddress());
						jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
						availableUsers.put(jsonObject);
					}
				}
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("DBConnError", true);
				errorMsg.put(jsonObject);
			} else {
				throw ubspe;
			}
		}
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("errorMsg", errorMsg);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("usersList", usersList);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("availableUsers", availableUsers);
		jsonArray.put(jsonObject);
		
		try {
			PrintWriter writer;
			writer = resourceResponse.getWriter();
			writer.print(jsonArray);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		}
	}

	private void removeUser(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lSelectedGroup = PortalConstants.LONG_ZERO;
		String strSelectedUsers = null;
		String [] strArray = null;
		long [] selectedUsers = null;
		String strRemovedUsers = null;
		long [] removedUsers = null;
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		List<User> userList = null;
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONArray errorMsg = JSONFactoryUtil.createJSONArray();
		JSONArray usersList = JSONFactoryUtil.createJSONArray();
		JSONArray availableUsers = JSONFactoryUtil.createJSONArray();
		String strUserId = null;
		String strUserName = null;
		String strUserListSortOrder = null;
		String strAvailableUsersSortOrder = null;
		List<User> selectedUserList = new ArrayList<User>();
		List<User> availableUserList = new ArrayList<User>();
		boolean bWithFilterUsername = false;
		boolean bWithFilterEmailAddress = false;
		
		lSelectedGroup = ParamUtil.getLong(originalRequest, PortalConstants.PARAM_SELECTED_GROUP);
		strSelectedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_SELECTED_USERS);
		strRemovedUsers = ParamUtil.getString(originalRequest, PortalConstants.PARAM_REMOVED_USERS);
		strUserId = ParamUtil.getString(originalRequest, PortalConstants.USER_ID);
		strUserName = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USERNAME);
		strUserListSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_USER_LIST_SORT_ORDER);
		strAvailableUsersSortOrder = ParamUtil.getString(originalRequest, PortalConstants.PARAM_AVAILABLE_USERS_SORT_ORDER);
		
		strUserName = PortletCommonUtil.convertHexToString(strUserName);
		
		if (!CommonUtil.isStringNullOrEmpty(strUserName)) {
			bWithFilterUsername = true;
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strUserId)) {
			bWithFilterEmailAddress = true;
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strSelectedUsers)) {
			strArray = strSelectedUsers.split(PortalConstants.DELIMITER_COMMA);
			
			if (strArray != null) {
				selectedUsers = new long[strArray.length];
				
				for (int i = 0; i < strArray.length; i++) {
					selectedUsers[i] = Long.parseLong(strArray[i]);
				}
			}
		}
		
		if (!CommonUtil.isStringNullOrEmpty(strRemovedUsers)) {
			strArray = null;
			strArray = strRemovedUsers.split(PortalConstants.DELIMITER_COMMA);
			
			if (strArray != null) {
				removedUsers = new long[strArray.length];
				
				for (int i = 0; i < strArray.length; i++) {
					removedUsers[i] = Long.parseLong(strArray[i]);
				}
			}
		}
		
		try {
			if (removedUsers != null) {
				for (int i = 0; i < removedUsers.length; i++) {
					jsonObject = JSONFactoryUtil.createJSONObject();
					jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_EXIST, false);
					
					try {
						UserLocalServiceUtil.getUser(removedUsers[i]);
					} catch (NoSuchUserException e) {
						jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_EXIST, true);
					} catch (PortalException e) {
						// do nothing
					} catch (SystemException e) {
						// do nothing
					}
					
					errorMsg.put(jsonObject);
					
					jsonObject = JSONFactoryUtil.createJSONObject();
					jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_BELONG_TO_GROUP, false);
					
					try {
						List<Organization> organizationList = ControllerHelper.getUserOrganizations(removedUsers[i], PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
						
						if (organizationList != null) {
							if (organizationList.get(0).getOrganizationId() != lSelectedGroup) {
								throw new UBSPortalException(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP, PortalMessages.PROJECT_USER_DOES_NOT_BELONG_TO_GROUP);
							}
						}
					} catch (UBSPortalException e) {
						jsonObject.put(PortalConstants.PARAM_USER_DOES_NOT_BELONG_TO_GROUP, true);
					}
					
					errorMsg.put(jsonObject);
				}
			}
			
			if (lSelectedGroup != 0L) {
				userList = ControllerHelper.getOrganizationUsers(lSelectedGroup);
			}
			
			for (User u : userList) {
				boolean bRemoved = false;
				boolean bSelected = false;
				
				if (removedUsers != null) {
					for (int i = 0; i < removedUsers.length; i++) {
						if (u.getUserId() == removedUsers[i]) {
							bRemoved = true;
							break;
						}
					}
				}
				
				if (!bRemoved && selectedUsers != null) {
					for (int i = 0; i < selectedUsers.length; i++) {
						if (u.getUserId() == selectedUsers[i]) {
							bSelected = true;
							break;
						}
					}
				}
				
				if (!bRemoved && bSelected) {
					selectedUserList.add(u);
				} else if (bRemoved || !bSelected) {
					if (!bWithFilterUsername &&
							!bWithFilterEmailAddress) {
						availableUserList.add(u);
					} else if ((bWithFilterEmailAddress && u.getEmailAddress().toLowerCase().contains(strUserId.toLowerCase())) ||
							(bWithFilterUsername && u.getFirstName().toLowerCase().contains(strUserName.toLowerCase()))) {
						availableUserList.add(u);
					}
				}
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strUserListSortOrder)) {
				List<User> sortableUsers = new ArrayList<User>(selectedUserList);
				
				Collections.sort(sortableUsers, new Comparator<User>() {
					@Override
					public int compare(User u1, User u2) {
						int iCompare = 0;
						
						iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
						
						if (iCompare == 0) {
							iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
						}
						
						return iCompare;
					}
				});
				
				if (strUserListSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
					Collections.reverse(sortableUsers);
				}
				
				selectedUserList = new ArrayList<User>();
				selectedUserList = sortableUsers;
			}
			
			if (!CommonUtil.isStringNullOrEmpty(strAvailableUsersSortOrder)) {
				List<User> sortableUsers = new ArrayList<User>(availableUserList);
				
				Collections.sort(sortableUsers, new Comparator<User>() {
					@Override
					public int compare(User u1, User u2) {
						int iCompare = 0;
						
						iCompare = u1.getFirstName().compareToIgnoreCase(u2.getFirstName());
						
						if (iCompare == 0) {
							iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
						}
						
						return iCompare;
					}
				});
				
				if (strAvailableUsersSortOrder.equalsIgnoreCase(PortalConstants.SORT_DESCENDING)) {
					Collections.reverse(sortableUsers);
				}
				
				availableUserList = new ArrayList<User>();
				availableUserList = sortableUsers;
			}
			
			for (User u : selectedUserList) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("userId", u.getUserId());
				jsonObject.put("emailAddress", u.getEmailAddress());
				jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
				usersList.put(jsonObject);
			}
			
			for (User u : availableUserList) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("userId", u.getUserId());
				jsonObject.put("emailAddress", u.getEmailAddress());
				jsonObject.put("screenName", HtmlUtil.escapeAttribute(u.getFirstName()));
				availableUsers.put(jsonObject);
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				jsonObject = JSONFactoryUtil.createJSONObject();
				jsonObject.put("DBConnError", true);
				errorMsg.put(jsonObject);
			} else {
				throw ubspe;
			}
		} finally {
			selectedUserList = null;
			availableUserList = null;
		}
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("errorMsg", errorMsg);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("usersList", usersList);
		jsonArray.put(jsonObject);
		
		jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("availableUsers", availableUsers);
		jsonArray.put(jsonObject);
		
		try {
			PrintWriter writer = resourceResponse.getWriter();
			writer.print(jsonArray);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		}
	}

	/**
	 * Download transition diagram
	 * 
	 * @param resourceRequest
	 * @param resourceResponse
	 * @throws UBSPortalException
	 */
	private void downloadTransitionDiagram(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		resourceRequest.setAttribute("downloadReportType", ReportType.XLS_REPORT.getInteger());
		resourceRequest.setAttribute("isDownloadTransitionDiagram", true);
		downloadReport(resourceRequest, resourceResponse);
	}
	
	/**
	 * Download word report
	 * 
	 * @param resourceRequest
	 * @param resourceResponse
	 * @throws UBSPortalException
	 */
	private void downloadWordReport(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		resourceRequest.setAttribute("downloadReportType", ReportType.WORD_REPORT.getInteger());
		downloadReport(resourceRequest, resourceResponse);
	}
	
	private void downloadReport(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		long lProjectId = PortalConstants.LONG_ZERO;
		long lScanId = PortalConstants.LONG_ZERO;
		String strFileName = null;
		Project project = null;
		String strCaseNumber = null;
		Scan scan = null;
		byte [] file = null;
		OutputStream sos = null;
		List<Report> reportList = new ArrayList<Report>();
		int [] reportTypeArr = new int [2];
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		String contentType="";
		String extension = "";
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
				
		try {
			Object iReportType = resourceRequest.getAttribute("downloadReportType");
						
			if((int)iReportType == ReportType.XLS_REPORT.getInteger()){
				log.info(PortalMessages.USER_EVENT_DOWNLOAD_SCREEN_TRANSITION_DIAGRAM, lUserId);
				contentType = PortalConstants.CONTENT_TYPE_XLS;
				extension = "xls";
			}else{
				log.info(PortalMessages.USER_EVENT_DOWNLOAD_REPORT, lUserId);
				contentType = PortalConstants.CONTENT_TYPE_DOCX;
				extension = "docx";
			}
			
			if (CommonUtil.isObjectNull(oLoggedIn) || !Boolean.parseBoolean(oLoggedIn.toString())) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			lProjectId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_PROJECT_ID);
			lScanId = ParamUtil.getLong(resourceRequest, PortalConstants.PARAM_SCAN_ID);
			
			if(!CommonUtil.isObjectNull(iReportType)){
				reportTypeArr[0] =(int) iReportType;
			}else{
				reportTypeArr[0] = ReportType.CSV_META_INFORMATION.getInteger();
				reportTypeArr[1] = ReportType.CSV_VULNERABILITY.getInteger();
			}
			
			scan = ScanLocalServiceUtil.getScan(lScanId);
						
			reportList = ReportLocalServiceUtil.getReports(lScanId, reportTypeArr, scan.getStatus(), ProjectType.VEX.getInteger());
			
			if(reportList == null || reportList.size() == 0)
			{
				throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalMessages.NO_REPORT);
			}
			
			project = ProjectLocalServiceUtil.getProject(lProjectId);
			
			strCaseNumber = project.getCaseNumber();
			
			file = FileProcessUtil.getIndividualReports(lProjectId, lScanId, reportList, PortalConstants.STRING_EMPTY);
			if (file == null || file.length == 0) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_NO_REPORT, PortalMessages.NO_REPORT);
			} else {
		        sos = resourceResponse.getPortletOutputStream();
		        resourceResponse.setContentType(contentType);
		        strFileName = FileProcessUtil.getReportName(lScanId, strCaseNumber, PortalConstants.STRING_EMPTY, extension);	        
		        resourceResponse.addProperty(PortalConstants.CONTENT_DISP, PortalConstants.ATTACHMENT+strFileName);
		        resourceResponse.addProperty(PortalConstants.DOWNLOAD_OPTIONS, PortalConstants.NO_OPEN);
//		        resourceResponse.addProperty(PortalConstants.SET_COOKIE, PortalConstants.FILE_DOWNLOAD_COOKIE);
		        
		        sos.write(file);
		        sos.flush();
		        sos.close();
			}
		} catch (PortalException pe) {
			throw new UBSPortalException(pe.getMessage(), pe.getMessage(), pe);
		} catch (IOException ioe) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, ioe);
		} catch (UBSPortalException ubspe) {
			throw ubspe;
		} catch (SystemException e) {
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} finally {
			strFileName = null;
			project = null;
			strCaseNumber = null;
			reportList = null;
			reportTypeArr = null;
			scan = null;
			sos = null;
			file = null;
		}
	}
	
	private void downloadVulnList(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		byte[] pdf = null;
		OutputStream sos = null;
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_DOWNLOAD_REFERENCE, lUserId);
		
		try {
			PortletCommonUtil.isDBConnected();
			
			if (CommonUtil.isObjectNull(oLoggedIn) || !Boolean.parseBoolean(oLoggedIn.toString())) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_VULN_LIST_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			pdf = FileProcessUtil.getVulnFilePath(ProjectType.VEX.getInteger());
			
			if (pdf == null || pdf.length == 0) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_VULN_LIST_FAILED, PortalMessages.NO_DESCRIPTION);
			}

			sos = resourceResponse.getPortletOutputStream();
			resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_PDF); 
			resourceResponse.addProperty(PortalConstants.CONTENT_DISP, PortalConstants.ATTACHMENT + PortalConstants.VULNLISTPDF);
			sos.write(pdf);
			sos.flush();
			sos.close();
		} catch (IOException ioe) {
			params.put("sos", sos);
			log.debug(PortalErrors.IO_EXCEPTION, PortalConstants.METHOD_DOWNLOAD_VULN_LIST, params, ioe);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, ioe);
		} catch (UBSPortalException ubspe) {
			params.put("pdf", pdf);
			log.debug(ubspe.getErrorCode(), PortalConstants.METHOD_DOWNLOAD_VULN_LIST, params, ubspe);
			throw ubspe;				
		} catch (Exception e) {
			params.put("pdf", pdf);
			params.put("sos", sos);
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_DOWNLOAD_VULN_LIST, params, e);
		} finally {
			params.clear();
			params = null;
		}
	}
	
	private void downloadSummary (ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		HttpServletRequest request 		= PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest origRequest 	= PortalUtil.getOriginalServletRequest(request);
		String strStartDate 			= PortalConstants.STRING_EMPTY;
		String strEndDate 				= PortalConstants.STRING_EMPTY;
		Calendar calStartDate 			= null;
		Calendar calEndDate 			= null;
		DateFormat format 				= new SimpleDateFormat(PortalConstants.YYYYMMDD);
		String strFolderLocation 		= PortalConstants.STRING_EMPTY;
		boolean bFolderExist 			= true;
		boolean isFeatureAvailable		= true; //TODO remove the value when working with SWP-189
		String strSummarySuffix 		= PortalConstants.STRING_EMPTY;
		List<Object> reportList 		= null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		ZipOutputStream zipOutputStream = null;
		byte [] bytes 					= null;
		FileInputStream fileInputStream = null;
		BufferedInputStream bufferedInputStream = null;
		List<String> reportSummaryList	= new ArrayList<String>();
		byte [] zippedReports 			= null;
		Calendar dateNow 				= Calendar.getInstance();
		
		HttpSession session 			= ControllerHelper.getHttpSession(resourceRequest);
		Object oLoggedIn 				= session.getAttribute(PortalConstants.LOGGED_IN);
		
		try {
			
			if (CommonUtil.isObjectNull(oLoggedIn) || !Boolean.parseBoolean(oLoggedIn.toString())) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_USER_NO_RIGHTS, PortalMessages.USER_NO_RIGHTS_ACTION);
			}
			
			if (PortletCommonUtil.isDBConnected()) {
				strStartDate = ParamUtil.getString(origRequest, PortalConstants.PARAM_PERIOD_START_DATE);
				strEndDate = ParamUtil.getString(origRequest, PortalConstants.PARAM_PERIOD_END_DATE);
				
				if (CommonUtil.isStringNullOrEmpty(strStartDate)
						|| strStartDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_PERIOD_INVALID, PortalMessages.PERIOD_INVALID);
				}
				
				if (CommonUtil.isStringNullOrEmpty(strEndDate)
						|| strEndDate.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_PERIOD_INVALID, PortalMessages.PERIOD_INVALID);
				}
				
				//TODO remove the statement when working with SWP-189 in the future
				if (isFeatureAvailable){
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_NO_SUMMARY, PortalMessages.UNAVAILABLE_SUMMARY);
				}
				
				try {
					calStartDate = Calendar.getInstance();
					calStartDate.setTime(format.parse(strStartDate));
					
					calEndDate = Calendar.getInstance();
					calEndDate.setTime(format.parse(strEndDate));
					calEndDate.set(Calendar.HOUR_OF_DAY, 23);
					calEndDate.set(Calendar.MINUTE, 59);
					calEndDate.set(Calendar.SECOND, 59);
					
					if (!CommonUtil.isObjectNull(calStartDate)
							&& !CommonUtil.isObjectNull(calEndDate)
							&& calEndDate.getTime().before(calStartDate.getTime())) {
						throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_PERIOD_INVALID, PortalMessages.PERIOD_INVALID);
					}
				} catch (ParseException e) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_PERIOD_INVALID, PortalMessages.PERIOD_INVALID, e);
				}
				
				strFolderLocation = FileProcessUtil.getVexDownloadBasePath();
				if(CommonUtil.isStringNullOrEmpty(strFolderLocation)){
					strFolderLocation = FileProcessUtil.getDefaultDownloadDirectory();
				}
								
				while(bFolderExist) {
					strSummarySuffix = PwdGenerator.getPassword();
					bFolderExist = ControllerHelper.folderExist(strFolderLocation, PortalConstants.SUMMARY_PREFIX + strSummarySuffix);
				}
				
				strFolderLocation += File.separator
						+ PortalConstants.SUMMARY_PREFIX + strSummarySuffix;
				new File (strFolderLocation).mkdirs();
				
				reportList = ReportLocalServiceUtil.getReportsByReportTypeProjectTypeStartDateEndDate(ReportType.XML_REPORT.getInteger(), ProjectType.VEX.getInteger(), calStartDate, calEndDate);
				
				if (!CommonUtil.isListNullOrEmpty(reportList)) {
					// download reports
					VexScanReport.downloadXMLFromStorage(reportList, strFolderLocation);
				}
				
				//TODO changes needed for SWP-189 in the future
				
				// generate vuln.csv
				//VexScanReport.generateVexVulnSummary(reportList, strFolderLocation, calStartDate.getTime(), calEndDate.getTime());
				
				// generate meta.csv
				//VexScanReport.generateVexMetaSummary(reportList, strFolderLocation, calStartDate.getTime(), calEndDate.getTime());
				
				// Create the ZIP file
				reportSummaryList.add(strFolderLocation
						+ File.separator
						+ PortalConstants.VULNCSV);
				reportSummaryList.add(strFolderLocation
						+ File.separator
						+ PortalConstants.METACSV);
				
				byteArrayOutputStream = new ByteArrayOutputStream();
				zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
				bytes = new byte[PortalConstants.READ_SPAN];
				String strFileName = PortalConstants.STRING_EMPTY;
				String strDate = PortalConstants.STRING_EMPTY + dateNow.get(Calendar.YEAR)
						+ String.format(PortalConstants.STRING_FORMAT_2LEADING_ZERO_DOUBLE, (dateNow.get(Calendar.MONTH) + 1))
						+ String.format(PortalConstants.STRING_FORMAT_2LEADING_ZERO_DOUBLE, dateNow.get(Calendar.DAY_OF_MONTH));
				
				for (String summaryName : reportSummaryList) {
					if (summaryName.contains(PortalConstants.VULNCSV)) {
						strFileName = PortalConstants.VULN_SUMMARY + strDate + PortalConstants.STR_DOT + PortalConstants.CSV_FILE_EXTENSION;
					} else if (summaryName.contains(PortalConstants.METACSV)) {
						strFileName = PortalConstants.META_SUMMARY + strDate + PortalConstants.STR_DOT + PortalConstants.CSV_FILE_EXTENSION;
					}
					
					fileInputStream = new FileInputStream(summaryName);
			        bufferedInputStream = new BufferedInputStream(fileInputStream);
			        
			        // Add ZIP entry to output stream.
			        Path reportFilePath = Paths.get(summaryName);
			        byte [] reportBytes = Files.readAllBytes(reportFilePath);
			        int bytesRead = PortalConstants.INT_ZERO;
			        
			        if(CommonUtil.isValidBytes(reportBytes)){
			        	zipOutputStream.putNextEntry(new ZipEntry(strFileName));
				            
						while ((bytesRead  = bufferedInputStream.read(bytes)) != -1) {
			            	zipOutputStream.write(bytes, 0, bytesRead);
			            }
			        } else {
						log.debug(PortalErrors.DOWNLOAD_SUMMARY_NO_SUMMARY, PortalConstants.METHOD_DOWNLOAD_SUMMARY, null, new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_NO_SUMMARY));
			        }
			        
			        if (zipOutputStream != null) {
						zipOutputStream.closeEntry();
					}
		            
		            if (bufferedInputStream != null) {
		            	bufferedInputStream.close();	
		            }
		            
		            if (fileInputStream != null) {
		            	fileInputStream.close();	
		            }
				}
				
				zipOutputStream.flush();
				byteArrayOutputStream.flush();
				
				 //below are close outside the loop
				zipOutputStream.close();
				byteArrayOutputStream.close();
				
				if (byteArrayOutputStream != null && byteArrayOutputStream.size() != 0) {
					zippedReports = byteArrayOutputStream.toByteArray();
				}
				
				byteArrayOutputStream = null;
				
				if (zippedReports == null || zippedReports.length == 0) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_SUMMARY_NO_SUMMARY, PortalMessages.NO_SUMMARY);
				}

				OutputStream sos = resourceResponse.getPortletOutputStream();
		        resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_ZIP);
		        String strFile = URLEncoder.encode(PortalConstants.SUMMARY_ZIP + strDate + PortalConstants.STR_DOT + PortalConstants.ZIP_FILE_EXTENSION, PortalConstants.UTF_8);
		        resourceResponse.addProperty(PortalConstants.CONTENT_DISP, PortalConstants.ATTACHMENT + strFile);
		        resourceResponse.addProperty(PortalConstants.SET_COOKIE, PortalConstants.FILE_DOWNLOAD_COOKIE);
		        
		        sos.write(zippedReports);
		        sos.flush();
		        sos.close();
			}
		} catch (PortalException e) {
			throw new UBSPortalException(e.getMessage(), e.getMessage(), e);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new UBSPortalException(PortalErrors.FILE_NOT_FOUND_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} catch (UBSPortalException e) {
			throw e;
		} finally {
			FileProcessUtil.deleteFilesSummaryFolder(strFolderLocation);
			
			reportList = null;
			bytes = null;
			reportSummaryList = null;
			zippedReports = null;
		}
	}
	
	private void downloadProjectList (ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(resourceRequest);
		Object oUserRole = session.getAttribute(PortalConstants.USER_ROLE);
		int iUserRole = PortalConstants.INT_ZERO;
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		User user = null;
		long lUserId = PortalConstants.LONG_ZERO;
		List<Object> projectList = null;
		File projectListFile = null;
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isObjectNull(oUserRole)) {
					iUserRole = Integer.parseInt(oUserRole.toString());
				}
				
				if (!CommonUtil.isObjectNull(oUserId)) {
					lUserId = Long.parseLong(oUserId.toString());
					user = UserLocalServiceUtil.getUser(lUserId);
				}
				
				Map<String, Object> searchedProject = new HashMap<String, Object>();
				searchedProject.put(PortalConstants.PARAM_PROJECT_ID, ParamUtil.getString(originalRequest, PortalConstants.PARAM_PROJECT_ID).trim());
				searchedProject.put(PortalConstants.PARAM_OWNER_GROUP, ParamUtil.getString(originalRequest, PortalConstants.PARAM_OWNER_GROUP).trim());
				searchedProject.put(PortalConstants.PARAM_CASE_NUMBER, ParamUtil.getString(originalRequest, PortalConstants.PARAM_CASE_NUMBER).trim());
				searchedProject.put(PortalConstants.PARAM_PROJECT_NAME, ParamUtil.getString(originalRequest, PortalConstants.PARAM_PROJECT_NAME).trim());
				searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_LOW, ParamUtil.getString(originalRequest, PortalConstants.PARAM_PROJECT_END_DATE_LOW).trim());
				searchedProject.put(PortalConstants.PARAM_PROJECT_END_DATE_HIGH, ParamUtil.getString(originalRequest, PortalConstants.PARAM_PROJECT_END_DATE_HIGH).trim());
				searchedProject.put(PortalConstants.PARAM_STATUS, ParamUtil.getString(originalRequest, PortalConstants.PARAM_STATUS).trim());
				searchedProject.put(PortalConstants.PARAM_NO_OF_SCANS, ParamUtil.getString(originalRequest, PortalConstants.PARAM_NO_OF_SCANS).trim());
				
				if (iUserRole == UserRole.OVERALL_ADMIN.getInteger() || iUserRole == UserRole.GROUP_ADMIN.getInteger()) {
					projectList = ProjectLocalServiceUtil.getOverallAdminProjectsToDownload(searchedProject, ProjectType.VEX.getInteger(), user);
				} else if (iUserRole == UserRole.GEN_USER.getInteger()) {					
					if (!CommonUtil.isObjectNull(user)) {
						projectList = ProjectLocalServiceUtil.getUserProjectsToDownload(user.getEmailAddress(), searchedProject, ProjectType.VEX.getInteger());
					}
				}
				
				String strFolderLocation = FileProcessUtil.getVexDownloadBasePath();
				if(CommonUtil.isStringNullOrEmpty(strFolderLocation)){
					strFolderLocation = FileProcessUtil.getDefaultDownloadDirectory();
				}
				
				projectListFile = new File (strFolderLocation
						+ File.separator
						+ PortalConstants.PROJECT_PREFIX
						+ Calendar.getInstance().getTimeInMillis()
						+ File.separator
						+ PortalConstants.PROJECTLISTCSV);
				
				new File(projectListFile.getParent()).mkdirs();
				
				ControllerHelper.createProjectListCsv(projectList, projectListFile);
				byte [] csv = Files.readAllBytes(Paths.get(projectListFile.getAbsolutePath()));
				
				if (csv == null || csv.length == 0) {
					throw new UBSPortalException(PortalErrors.DOWNLOAD_PROJECT_LIST_NO_LIST, PortalMessages.NO_PROJECT_LIST);
				}

				resourceResponse.reset();
				OutputStream sos = resourceResponse.getPortletOutputStream();
				resourceResponse.setContentType(PortalConstants.CONTENT_TYPE_CSV);
				resourceResponse.addProperty(PortalConstants.CONTENT_DISP, PortalConstants.ATTACHMENT + PortalConstants.PROJECTLISTCSV);
				resourceResponse.addProperty(PortalConstants.SET_COOKIE, PortalConstants.FILE_DOWNLOAD_COOKIE);
				sos.write(csv);
				sos.flush();
				sos.close();
			}
		} catch (UBSPortalException e) {
			throw e;
		} catch (PortalException e) {
			throw new UBSPortalException(e.getMessage(), e.getMessage(), e);
		} catch (IOException e) {
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.COMMON_EXCEPTION, e);
		} finally {
			FileProcessUtil.deleteFileDirectory(projectListFile);
		}
	}
	
	private void executeProjectAction(ActionRequest actionRequest, ActionResponse actionResponse, int userAction) throws UBSPortalException {	
		switch (userAction) {
			case PortalConstants.USER_EVENT_ADD_PROJECT:
			case PortalConstants.USER_EVENT_UPDATE_PROJECT:
				if (VexProjectMgmtController.updateProject(actionRequest, userAction)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							SessionMessages.add(actionRequest, PortalConstants.ADD_PROJECT_SUCCESSFUL);
						} else {
							SessionMessages.add(actionRequest, PortalConstants.UPDATE_PROJECT_SUCCESSFUL);
						}
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT) {
							SessionErrors.add(actionRequest, PortalMessages.ADD_PROJECT_FAILED);
						} else {
							SessionErrors.add(actionRequest, PortalMessages.UPDATE_PROJECT_FAILED);
						}
					}
				}
				
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				break;
			
			case PortalConstants.USER_EVENT_DELETE_PROJECT:
				if (VexProjectMgmtController.deleteProject(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.DELETE_PROJECT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.DELETE_PROJECT_FAILED);
					}
				}
				
				JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(jsonArray);
					response.flushBuffer();
				} catch (IOException e1) {
					// do nothing
				}
				
				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_PROJECT_LIST);
				break;
				
			default:
				break;
		}
	}
	
	private void executeScanAction(ActionRequest actionRequest, ActionResponse actionResponse, UploadPortletRequest uploadRequest, int userAction) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		JSONObject formDetailJson = null;
		ApiInitInfo info = null;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
			info = VexScanMgmtController.setLoginCredentials();
		}
		
		switch (userAction) {
			case PortalConstants.USER_EVENT_COMPLETE_PROJECT:
				if (VexProjectMgmtController.completeProject(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.COMPLETE_PROJECT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.COMPLETE_PROJECT_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e) {
					// do nothing
				}
				
				break;
				
			case PortalConstants.USER_EVENT_OPEN_PROJECT:
				if (VexProjectMgmtController.openProject(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.OPEN_PROJECT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.OPEN_PROJECT_FAILED);
					}
				}
				
				formDetailJson = JSONFactoryUtil.createJSONObject();
				formDetailJson.put(PortalConstants.REDIRECT, false);
				
				try {
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
					PrintWriter writer = response.getWriter();
					writer.print(formDetailJson);
					response.flushBuffer();
				} catch (IOException e) {
					// do nothing
				}
				
				break;
				
			case PortalConstants.USER_EVENT_ADD_SCAN:

					if (VexScanMgmtController.addScan(uploadRequest, lUserId)) {
						if (SessionMessages.isEmpty(actionRequest)) {
							SessionMessages.add(actionRequest, PortalConstants.ADD_SCAN_SUCCESSFUL);
						}
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, PortalMessages.ADD_SCAN_FAILED);
						}
					}

				this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
				
				break;
			
			case PortalConstants.USER_EVENT_REGENERATE_REPORT:
				if (VexScanMgmtController.regenerateReport(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.REGENERATE_REPORT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.REGENERATE_REPORT_FAILED);
					}
				}
				formDetailJson(actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_RECRAWL:
				reCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_ABORT_CRAWL:
				abortCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_INTERRUPTED_CRAWL:
				interruptedCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_CANCEL_CRAWL:
				cancelCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_RESTART_CRAWL:
				restartCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_INTERRUPTED_SCAN:
				interruptedCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_CANCEL_SCAN:
				cancelScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_STOP_SCAN:
				stopVexScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_RESTART_SCAN:
				restartScan(actionRequest, actionResponse);
				break;
							
			case PortalConstants.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN:
				copyCrawlingAndScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_REEXECUTE_SCAN:
				reexecuteVexScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_DELETE_SCAN:
				deleteVexScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_UPLOAD_SCAN:
				Scan uploadScan = null;
				info = VexScanMgmtController.setLoginCredentials();
				uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
				String prevUploadFilePath = ParamUtil.getString(actionRequest, "filePath");
				
				uploadScan = VexScanMgmtController.uploadScan(actionRequest, uploadRequest, lUserId, prevUploadFilePath);
				
				if (!CommonUtil.isObjectNull(uploadScan)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.UPLOAD_SCAN_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) { 
						SessionErrors.add(actionRequest, PortalMessages.UPLOAD_SCAN_FAILED);
					}
				}

				actionRequest.setAttribute(PortalConstants.PARAM_SCAN, uploadScan);
				viewScanRegistration(actionRequest, actionResponse);
				
				break;
				
			case PortalConstants.USER_EVENT_IMPORT_SCAN:

					String filePath = ParamUtil.getString(actionRequest, "filePath");
					uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
					formDetailJson = null;
					formDetailJson = JSONFactoryUtil.createJSONObject();
					
					if (VexScanMgmtController.importScan(uploadRequest, lUserId, filePath, info)) {
						if (SessionMessages.isEmpty(actionRequest)) {
							SessionMessages.add(actionRequest, PortalConstants.IMPORT_SCAN_SUCCESSFUL);
						}
						this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
					} else {
						if (SessionErrors.isEmpty(actionRequest)) {
							SessionErrors.add(actionRequest, PortalMessages.IMPORT_SCAN_FAILED);
						}
						this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
					}

				break;
		}
	}
	
	private void executeEntireScanAction (ActionRequest actionRequest, ActionResponse actionResponse, UploadPortletRequest uploadRequest, int userAction) throws UBSPortalException {
	
		switch (userAction) {			
				
			case PortalConstants.USER_EVENT_REGENERATE_REPORT:
				if (VexScanMgmtController.regenerateReport(actionRequest)) {
					if (SessionMessages.isEmpty(actionRequest)) {
						SessionMessages.add(actionRequest, PortalConstants.REGENERATE_REPORT_SUCCESSFUL);
					}
				} else {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, PortalMessages.REGENERATE_REPORT_FAILED);
					}
				}
				
				formDetailJson(actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_RECRAWL:
				reCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_ABORT_CRAWL:
				abortCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_INTERRUPTED_CRAWL:
				interruptedCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_CANCEL_CRAWL:
				cancelCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_RESTART_CRAWL:
				restartCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_INTERRUPTED_SCAN:
				interruptedCrawl(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_CANCEL_SCAN:
				cancelScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_STOP_SCAN:
				stopVexScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_RESTART_SCAN:
				restartScan(actionRequest, actionResponse);
				break;
							
			case PortalConstants.USER_EVENT_COPY_CRAWL_RESULT_AND_SCAN:
				copyCrawlingAndScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_REEXECUTE_SCAN:
				reexecuteVexScan(actionRequest, actionResponse);
				break;
				
			case PortalConstants.USER_EVENT_DELETE_SCAN:
				deleteVexScan(actionRequest, actionResponse);
				break;
		}
	}
	
	private void formDetailJson (ActionResponse actionResponse){
		JSONObject formDetailJson = null;
		
		formDetailJson = JSONFactoryUtil.createJSONObject();
		formDetailJson.put(PortalConstants.REDIRECT, false);
		
		try {
			HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
			response.setContentType(PortalConstants.CONTENT_TYPE_JSON);
			PrintWriter writer = response.getWriter();
			writer.print(formDetailJson);
			response.flushBuffer();
		} catch (IOException e) {
			// do nothing
		}
	}
	
	private int getFieldNumber (String errorCode, int userAction) {
		int iFieldNumber = 0;
		
		if (userAction == PortalConstants.USER_EVENT_ADD_PROJECT ||
				userAction == PortalConstants.USER_EVENT_UPDATE_PROJECT) {
			if (errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_ALREADY_EXISTS) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_INVALID) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_TOO_LONG) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_ALREADY_EXISTS) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_INVALID) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_TOO_LONG)) {
				iFieldNumber = PortalConstants.FIELD_CASE_NUMBER;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_NAME_INVALID) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_NAME_TOO_LONG) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_NAME) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_INVALID) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_TOO_LONG) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_NAME)) {
				iFieldNumber = PortalConstants.FIELD_PROJECT_NAME;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_OWNER_GROUP_DOES_NOT_EXIST) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_NO_OWNER_GROUP) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_OWNER_GROUP_DOES_NOT_EXIST) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_OWNER_GROUP)) {
				iFieldNumber = PortalConstants.FIELD_OWNER_GROUP;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_END_DATE) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_END_DATE)) {
				iFieldNumber = PortalConstants.FIELD_PROJECT_END_DATE;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_NO_ATTRIBUTE) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_ATTRIBUTE)) {
				iFieldNumber = PortalConstants.FIELD_ATTRIBUTE;
			} else if (errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_USERS) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_USERS) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP) ||
					errorCode.equals(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST) ||
					errorCode.equals(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST)) {
				iFieldNumber = PortalConstants.FIELD_PROJECT_USERS;
			}
		} else if (userAction == PortalConstants.USER_EVENT_ADD_SCAN ||
				userAction == PortalConstants.USER_EVENT_UPDATE_SCAN) {
			if (errorCode.equals(PortalErrors.ADD_SCAN_FILE_EMPTY) ||
					errorCode.equals(PortalErrors.ADD_SCAN_FILE_NOT_ZIP) ||
					errorCode.equals(PortalErrors.ADD_SCAN_NO_FILE)) {
				iFieldNumber = PortalConstants.FIELD_SCAN_FILE;
			} else if (errorCode.equals(PortalErrors.ADD_SCAN_NO_PROCESS) ||
					errorCode.equals(PortalErrors.UPDATE_SCAN_NO_PROCESS)) {
				iFieldNumber = PortalConstants.FIELD_SCAN_PROCESS;
			}
		}
		
		return iFieldNumber;
	}
	
	private void redirect (ActionRequest actionRequest, ActionResponse actionResponse, String redirect) {
		ThemeDisplay td = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		PortletURL actionURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(actionRequest), (String) actionRequest.getAttribute(WebKeys.PORTLET_ID), td.getLayout().getPlid(), PortletRequest.ACTION_PHASE);
		
		try {
			actionURL.setWindowState(WindowState.NORMAL);
			actionURL.setPortletMode(PortletMode.VIEW);
			actionURL.setParameter(PortalConstants.ACTION_JAVAX_PORTLET, redirect);
			
			if (redirect.equals(PortalConstants.ACTION_VIEW_SCAN_LIST)) {
				actionURL.setParameter(PortalConstants.PARAM_PROJECT_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_ID));
			}
			String url = actionResponse.encodeURL(actionURL.toString());
			actionRequest.setAttribute("javax.servlet.include.request_uri", url);
			
			Object oAction = actionRequest.getAttribute(PortalConstants.PARAM_USER_ACTION);
			int iAction = PortalConstants.INT_ZERO;
			
			if (!CommonUtil.isObjectNull(oAction)) {
				try {
					iAction = Integer.parseInt(oAction.toString());
				} catch (NumberFormatException e) {
					// do nothing
				}
			}
			
			if (redirect.equals(PortalConstants.ACTION_VIEW_SCAN_LIST)
					|| redirect.equals(PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST)) {
				HttpSession session = ControllerHelper.getHttpSession(actionRequest);
				
				Map<String, Object> searchedScan = new HashMap<String, Object>();
				searchedScan.put(PortalConstants.PARAM_SCAN_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID));
				
				if (redirect.equals(PortalConstants.ACTION_VIEW_ENTIRE_SCAN_LIST)) {
					searchedScan.put(PortalConstants.PARAM_PROJECT_NAME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_NAME));
					searchedScan.put(PortalConstants.PARAM_OWNER_GROUP, ParamUtil.getString(actionRequest, PortalConstants.PARAM_OWNER_GROUP));
				}
				
				searchedScan.put(PortalConstants.PARAM_FILE_NAME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_FILE_NAME));
				searchedScan.put(PortalConstants.PARAM_HASH_VALUE, ParamUtil.getString(actionRequest, PortalConstants.PARAM_HASH_VALUE));
				searchedScan.put(PortalConstants.PARAM_SCAN_MANAGER, ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_MANAGER));
				searchedScan.put(PortalConstants.PARAM_REG_DATE_LOW, ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_LOW));
				searchedScan.put(PortalConstants.PARAM_REG_DATE_HIGH, ParamUtil.getString(actionRequest, PortalConstants.PARAM_REG_DATE_HIGH));
				searchedScan.put(PortalConstants.PARAM_START_TIME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_START_TIME));
				searchedScan.put(PortalConstants.PARAM_END_TIME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_END_TIME));
				String [] statusArr = ParamUtil.getParameterValues(actionRequest, PortalConstants.PARAM_STATUS);
				String status = PortalConstants.STRING_EMPTY;
				if (!CommonUtil.isObjectNull(statusArr)) {
					for (int index = 0; index < statusArr.length; index++) {
						status += statusArr[index] + PortalConstants.COMMA;
					}
				}
				
				searchedScan.put(PortalConstants.PARAM_STATUS, status);
				searchedScan.put(PortalConstants.PARAM_CX_SCAN_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_CX_SCAN_ID));
				
				if (iAction == PortalConstants.USER_EVENT_CLEAR_SEARCH_SCAN) {
					session.setAttribute(PortalConstants.PARAM_USER_ACTION, iAction);
				}
				
				session.setAttribute(PortalConstants.PARAM_SCAN, searchedScan);
				Object oIsFromSearch = actionRequest.getPortletSession().getAttribute(PortalConstants.PARAM_IS_FROM_SEARCH);
				String strIsFromSearch = PortalConstants.STRING_EMPTY;
				
				if (!CommonUtil.isObjectNull(oIsFromSearch)) {
					strIsFromSearch = oIsFromSearch.toString();
					
					session.setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, strIsFromSearch);
				}
			}
			actionResponse.sendRedirect(url);
		} catch (WindowStateException e) {
			// do nothing
		} catch (PortletModeException e) {
			// do nothing
		} catch (IOException e) {
			// do nothing
		}
	}
	
	public void copyCrawlingSetting (ActionRequest actionRequest, ActionResponse actionResponse) {
		long lScanId = PortalConstants.LONG_ZERO;
		int iScreenNo = PortalConstants.INT_ZERO;
		VexScanSetting scanSetting = null;
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_COPY_CRAWL_SETTING, lUserId);
		
		try {
			iScreenNo = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCREEN_NO);
			lScanId = ParamUtil.getInteger(actionRequest, PortalConstants.PARAM_SCAN_ID);
			scanSetting = VexScanMgmtController.getVexScanSetting(lScanId);
			
			// Check if Advance Scan
			if (scanSetting.getMaxnumdetectionlink() > 0) {
				viewRegisterScanAdvancedSetting(actionRequest, actionResponse);
			} else {
				viewRegisterScanSimpleSetting (actionRequest, actionResponse);
			}
		} catch (UBSPortalException ubspe) {			
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (!ubspe.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					if (SessionErrors.isEmpty(actionRequest)) {
						SessionErrors.add(actionRequest, ubspe.getErrorMessage());
					}
				}
			}
			
			this.redirect(actionRequest, actionResponse, PortalConstants.ACTION_VIEW_SCAN_LIST);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	public void copyPatrolSetting (ActionRequest actionRequest, ActionResponse actionResponse) {
		copyCrawlingSetting (actionRequest, actionResponse);
	}
	
	public void resendDetectionResult(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException{
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = 0L;
		ApiInitInfo info = null; 
		try{
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
				
				info = VexScanMgmtController.setLoginCredentials();
				log.info(PortalMessages.USER_EVENT_RESEND_DETECTION_RESULT, lUserId);
			}
			if (VexScanMgmtController.resendDetectionResult(actionRequest, info)) {
				if (SessionMessages.isEmpty(actionRequest)) {
					SessionMessages.add(actionRequest, PortalConstants.RESEND_SUCCESSFUL);
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, PortalMessages.RESEND_FAILED);
				}
			}
		}catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
			
			actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_HAS_ERROR, true);
			actionRequest.getPortletSession().removeAttribute(PortalConstants.PARAM_ACTION_SEARCH_PROJECT);
		}
		formDetailJson(actionResponse);
		//viewDetectionResult(actionRequest, actionResponse);
	}
			
	public void viewDetectionResult (ActionRequest actionRequest, ActionResponse actionResponse) {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_REVIEW_DETECTION_RESULT, lUserId);
		
		this.getDetectionResults(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_DETECTION_RESULT);
	}
	
	private void getDetectionResults(ActionRequest actionRequest, ActionResponse actionResponse, int userAction) {
		List<Object> detectionResultList = null;
		Map<String, Object> searchedDetectionResult = null;
		try {
			detectionResultList = VexScanMgmtController.getVexDetectionResultReview(actionRequest, userAction, actionResponse);
			if(null == detectionResultList){
				detectionResultList = new ArrayList<Object>();
			}
		} catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
			
			actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_HAS_ERROR, true);
			actionRequest.getPortletSession().removeAttribute(PortalConstants.PARAM_ACTION_SEARCH_PROJECT);
		}
		
		if (userAction == PortalConstants.USER_EVENT_SEARCH_DETECTION_REVIEW) {
			
			String strDetectionResultId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_RESULT_ID).trim();
			String strScanId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID).trim();
			String [] strRiskLevelArr = ParamUtil.getParameterValues(actionRequest, PortalConstants.PARAM_DETECTION_RISK_LEVEL);
			String strIsResend = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX).trim();
			String strReviewComment = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_REVIEW_COMMENT).trim();
			String strDetectionJudgement = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_JUDGEMENT).trim();
			String strParameterName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_PARAMETER_NAME).trim();
			String strUrl = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_URL).trim();
			String strfunctionName = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_FUNCTION_NAME).trim();
			String strOverview = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_OVERVIEW).trim();
			String strCategory = ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_CATEGORY).trim();

			String strRiskLevel = PortalConstants.STRING_EMPTY;
			
			if (!CommonUtil.isObjectNull(strRiskLevelArr)) {
				for (String riskLevel : strRiskLevelArr) {
					strRiskLevel += riskLevel + PortalConstants.COMMA;
				}

			}
			
			searchedDetectionResult = new HashMap<String, Object>();
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_RESULT_ID, strDetectionResultId);
			searchedDetectionResult.put(PortalConstants.PARAM_SCAN_ID, strScanId);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_RISK_LEVEL, strRiskLevel);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_CATEGORY, strCategory);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_OVERVIEW, strOverview);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_FUNCTION_NAME, strfunctionName);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_URL, strUrl);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_PARAMETER_NAME, strParameterName);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_JUDGEMENT, strDetectionJudgement);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_REVIEW_COMMENT, strReviewComment);
			searchedDetectionResult.put(PortalConstants.PARAM_DETECTION_IS_RESEND_IN_VEX, strIsResend);
						
		}
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_FILE_NAME, ParamUtil.getString(actionRequest, PortalConstants.PARAM_FILE_NAME));
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_SCAN_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID));
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_DETECTION_RESULT_LIST, detectionResultList);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_DETECTION_RESULT, searchedDetectionResult);
		actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_PROJECT_ID, ParamUtil.getString(actionRequest, PortalConstants.PARAM_PROJECT_ID));
				
		if (userAction == PortalConstants.USER_EVENT_SEARCH_DETECTION_REVIEW) {
			actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_IS_FROM_SEARCH, true);
		}
		
		actionResponse.setRenderParameter(PortalConstants.MVC_PATH, PortalConstants.VEX_REVIEW_DETECTION_RESULT_JSP);
	}
	
	public static List<Object> getDetectionResultsFromXml(ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException{
		List<Report> reportList = null;
		String strFolderLocation = null;
		List<Object> detectionResults = null;
		try {
			if (PortletCommonUtil.isDBConnected()) {
				
				String strScanId = ParamUtil.getString(actionRequest, PortalConstants.PARAM_SCAN_ID);
				long lScanId = Long.parseLong(strScanId);
				
				strFolderLocation = FileProcessUtil.getVexDownloadBasePath();
				if(CommonUtil.isStringNullOrEmpty(strFolderLocation)){
					strFolderLocation = FileProcessUtil.getDefaultDownloadDirectory();
				}
								
				strFolderLocation += File.separator + PortalConstants.REVIEW_DETECTION_PREFIX + strScanId;
				new File (strFolderLocation).mkdirs();
				
				int reportTypeArr[] = {ReportType.XML_REPORT.getInteger(),0};
				reportList = ReportLocalServiceUtil.getReports(lScanId,reportTypeArr, ScanStatus.COMPLETE.getInteger(), ProjectType.VEX.getInteger());
				
				if (!CommonUtil.isListNullOrEmpty(reportList)) {
					VexScanReport.downloadSingleXMLFromStorage(reportList.get(0),strFolderLocation);
				}
				
				Report report = reportList.get(0);
				String reportName = report.getReportName().replaceAll(Pattern.quote(PortalConstants.STRING_BACK_SLASH + PortalConstants.STRING_BACK_SLASH), PortalConstants.STR_FWD_SLASH);
				String [] splitReportName = reportName.split("/");
				if(splitReportName.length > 1){
					reportName = splitReportName[1];
				}else{
					reportName = reportName.split(PortalConstants.STRING_BACK_SLASH + PortalConstants.STRING_BACK_SLASH)[1];
				}
				File xmlReport = new File(strFolderLocation + File.separator + reportName);
				
				if(xmlReport.exists()){
					List<DetectionResultSummaryData> summaryData = VexScanReport.getDetectionResultSummaryData(xmlReport);
					VexScanReport.getDetectionResultSummaryData(xmlReport);
					 
					if(null != summaryData){
						detectionResults = new ArrayList<Object>();
						
						for(DetectionResultSummaryData data : summaryData){
							long number = Long.parseLong(data.getScanresultid().toString().substring(1, data.getScanresultid().length()));
							int iRiskLevel = VexPortlet.getRiskLevel(data.getRisklevel().toString());
							
							//If number is an audit scan result, set resend to 1.
//							if(data.getScanresultid().toString().startsWith("a")){
//								iResend = 1;
//							}
							
							VexDetectionResultPK vexDetectionResultPK = new VexDetectionResultPK();
							vexDetectionResultPK.setScanresultid(number);
							vexDetectionResultPK.setScanid(lScanId);
							
							VexDetectionResult vexDetectionResult = VexDetectionResultLocalServiceUtil.createVexDetectionResult(vexDetectionResultPK);
							vexDetectionResult.setCategory(data.getCategory());
							vexDetectionResult.setDetectionjudgment(PortalConstants.STR_DETECTION_JUDGEMENT);
							vexDetectionResult.setDetectionresultid(data.getDetectionresultid());
							vexDetectionResult.setFunctionname(data.getFunctionname());
							vexDetectionResult.setScanresultid(number);
							vexDetectionResult.setOverview(data.getOverview());
							vexDetectionResult.setParametername(data.getParametername());
							vexDetectionResult.setRisklevel(iRiskLevel);
							vexDetectionResult.setScanid(lScanId);
							vexDetectionResult.setUrl(data.getUrl());
							vexDetectionResult.setIsresendinvex(0);
							detectionResults.add(vexDetectionResult);
							
							VexDetectionResultLocalServiceUtil.addVexDetectionResult(vexDetectionResult);
						}
					}
				}
			}
		} catch (PortalException e) {
			throw new UBSPortalException(e.getMessage(), e.getMessage(), e);
		}  catch (UBSPortalException e) {
			throw e;
		} catch (NumberFormatException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_REVIEW, null, e);
		}finally {
			FileProcessUtil.deleteFilesSummaryFolder(strFolderLocation);
			reportList = null;
		}
		
		return detectionResults;
	}
	
	public void searchDetectionResult (ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_SEARCH_DETECTION_RESULT, lUserId);
		
		this.getDetectionResults(actionRequest, actionResponse, PortalConstants.USER_EVENT_SEARCH_DETECTION_REVIEW);
		
	}
	
	public void confirmDetectionResult (ActionRequest actionRequest, ActionResponse actionResponse) throws UBSPortalException {
		HttpSession session = ControllerHelper.getHttpSession(actionRequest);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		try{
			if (!CommonUtil.isObjectNull(oUserId)) {
				lUserId = Long.parseLong(oUserId.toString());
			}
	
			log.info(PortalMessages.USER_EVENT_CONFIRM_DETECTION_RESULT, lUserId);
			if(VexScanMgmtController.confirmDetectionResult(actionRequest)){
				if (SessionMessages.isEmpty(actionRequest)) {
					SessionMessages.add(actionRequest, PortalConstants.ADD_REVIEW_SCRUTINY_SUCCESSFUL);
				}
			}else{
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, PortalMessages.ADD_REVIEW_SCRUTINY_FAILED);
				}
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_DETECTION_CONFIRM_NUMBER, 
						ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_CONFIRM_NUMBER).trim());
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_DETECTION_CONFIRM_RESULT_ID,
						ParamUtil.getString(actionRequest, PortalConstants.PARAM_DETECTION_CONFIRM_RESULT_ID).trim());
			}
			
		}catch (UBSPortalException ubspe) {
			if (ubspe.getErrorCode().equals(PortalErrors.CX_SERVER_ERROR)) {
				actionRequest.getPortletSession().setAttribute(PortalConstants.PARAM_CX_SERVER_ERROR_MSG, ubspe.getErrorMessage());
				
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorCode());
				}
			} else {
				if (SessionErrors.isEmpty(actionRequest)) {
					SessionErrors.add(actionRequest, ubspe.getErrorMessage());
				}
			}
		}
		//formDetailJson(actionResponse);
		this.getDetectionResults(actionRequest, actionResponse, PortalConstants.USER_EVENT_VIEW_DETECTION_RESULT);
	}
	
	private static int getRiskLevel(String strRiskLevel){
		int iRiskLevel = 0;
		switch(strRiskLevel){
			case PortalConstants.SEVERITY_LOW_EN:
				iRiskLevel = 1;
				break;
			case PortalConstants.SEVERITY_MID_EN:
				iRiskLevel = 2;
				break;
			case PortalConstants.SEVERITY_HIGH_EN:
				iRiskLevel = 3;
				break;
			case PortalConstants.SEVERITY_REMARKS_JP:
				iRiskLevel = 4;
				break;
		}
		return iRiskLevel;
	}
}
