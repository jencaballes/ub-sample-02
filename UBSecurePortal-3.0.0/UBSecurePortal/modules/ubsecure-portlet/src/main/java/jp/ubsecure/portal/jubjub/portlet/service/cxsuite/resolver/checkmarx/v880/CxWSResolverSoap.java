/**
 * CxWSResolverSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880;

public interface CxWSResolverSoap extends java.rmi.Remote {
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResponseDiscovery getWebServiceUrl(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxClientType clientType, int APIVersion) throws java.rmi.RemoteException;
}
