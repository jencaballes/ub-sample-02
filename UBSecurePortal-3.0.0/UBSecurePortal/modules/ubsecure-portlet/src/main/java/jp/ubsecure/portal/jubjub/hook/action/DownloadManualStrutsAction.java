package jp.ubsecure.portal.jubjub.hook.action;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.struts.StrutsAction;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_PATH + PortalConstants.PATH_DOWNLOAD_MANUAL,
	},
	service = StrutsAction.class
)

public class DownloadManualStrutsAction extends BaseStrutsAction {
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(DownloadManualStrutsAction.class);

	public String execute (StrutsAction originalStrutsAction, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		String strReturn = PortalConstants.STRING_EMPTY;
		byte[] pdf = null;
		OutputStream sos = null;
		String strCurScreen = "";
		HttpSession httpSession = httpServletRequest.getSession(false);
		Object currentScreen = httpSession.getAttribute("Current_screen").toString();
		httpSession.removeAttribute("download_from");
		
		if (currentScreen !=null) {
			strCurScreen = currentScreen.toString();
		}
		
		httpSession.removeAttribute(PortalConstants.ERROR);
		Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
		long lUserId = PortalConstants.LONG_ZERO;
		ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String url = themeDisplay.getCDNBaseURL().toString() + "/web/guest";
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_DOWNLOAD_MANUAL, lUserId);
		
		try {
			PortletCommonUtil.isDBConnected();
			
			Object oLoggedIn = httpSession.getAttribute(PortalConstants.LOGGED_IN);
			boolean bLoggedIn = false;
			
			if (!CommonUtil.isObjectNull(oLoggedIn)) {
				bLoggedIn = Boolean.parseBoolean(oLoggedIn.toString());
			}
			
			String filename = PortalConstants.MANUAL_LOGIN_PDF;
			
			if (bLoggedIn) {
				String service = httpServletRequest.getParameter(PortalConstants.SERVICE);

				if (!CommonUtil.isStringNullOrEmpty(service)) {
					if (service.equalsIgnoreCase(PortalConstants.CXSUITE)) {
						filename = PortalConstants.MANUAL_SOURCE_CODE_DIAGNOSIS_PDF;
					} else if (service.equalsIgnoreCase(PortalConstants.ANDROID)) {
						filename = PortalConstants.MANUAL_ANDROID_STATIC_ANALYSIS_PDF;
					} else if (service.equalsIgnoreCase(PortalConstants.IOS)) {
						filename = PortalConstants.MANUAL_IOS_STATIC_ANALYSIS_PDF;
					}else if (service.equalsIgnoreCase(PortalConstants.VEX)) {
						filename = PortalConstants.MANUAL_VEX_DYNAMIC_ANALYSIS_PDF;
					}
					
				}
			}
			
			pdf = FileProcessUtil.getManualFilePath(filename);
			
			if (pdf == null || pdf.length == 0) {
				throw new UBSPortalException(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL, PortalMessages.NO_MANUAL);
			}
			
			sos = httpServletResponse.getOutputStream();
			httpServletResponse.setContentType(PortalConstants.CONTENT_TYPE_PDF); 
			httpServletResponse.setHeader(PortalConstants.CONTENT_DISP, PortalConstants.ATTACHMENT + filename);
			sos.write(pdf);
			sos.flush();
			sos.close();
		} catch (IOException ioe) {
			httpSession.setAttribute("download_from", strCurScreen);
			httpSession.setAttribute(PortalConstants.ERROR, PortalErrors.DOWNLOAD_MANUAL_FAILED);
			strReturn = "redirect: " + url;
		} catch (UBSPortalException ubspe) {
			httpSession.setAttribute("download_from", strCurScreen);
			httpSession.setAttribute(PortalConstants.ERROR, ubspe.getErrorCode());
			strReturn = "redirect: " + url;
		} finally {
			pdf = null;
		}
		
		return strReturn;
	}
	
}
