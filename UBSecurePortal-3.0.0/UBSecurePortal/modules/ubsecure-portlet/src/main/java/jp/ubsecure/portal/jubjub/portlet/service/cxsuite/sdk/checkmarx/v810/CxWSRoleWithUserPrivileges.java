/**
 * CxWSRoleWithUserPrivileges.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810;

public class CxWSRoleWithUserPrivileges  extends jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.Role  implements java.io.Serializable {
    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSItemAndCRUD[] itemsCRUD;

    public CxWSRoleWithUserPrivileges() {
    }

    public CxWSRoleWithUserPrivileges(
           java.lang.String name,
           java.lang.String ID,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSItemAndCRUD[] itemsCRUD) {
        super(
            name,
            ID);
        this.itemsCRUD = itemsCRUD;
    }


    /**
     * Gets the itemsCRUD value for this CxWSRoleWithUserPrivileges.
     * 
     * @return itemsCRUD
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSItemAndCRUD[] getItemsCRUD() {
        return itemsCRUD;
    }


    /**
     * Sets the itemsCRUD value for this CxWSRoleWithUserPrivileges.
     * 
     * @param itemsCRUD
     */
    public void setItemsCRUD(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSItemAndCRUD[] itemsCRUD) {
        this.itemsCRUD = itemsCRUD;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CxWSRoleWithUserPrivileges)) return false;
        CxWSRoleWithUserPrivileges other = (CxWSRoleWithUserPrivileges) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.itemsCRUD==null && other.getItemsCRUD()==null) || 
             (this.itemsCRUD!=null &&
              java.util.Arrays.equals(this.itemsCRUD, other.getItemsCRUD())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getItemsCRUD() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getItemsCRUD());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getItemsCRUD(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CxWSRoleWithUserPrivileges.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSRoleWithUserPrivileges"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemsCRUD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ItemsCRUD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSItemAndCRUD"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSItemAndCRUD"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
