package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.MetaCsvEntry;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class MetaCsvEntryParser implements CSVEntryConverter<MetaCsvEntry> {
	
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(MetaCsvEntryParser.class);
	
	@Override
	public String[] convertEntry(MetaCsvEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (CommonUtil.isObjectNull(entry)) {
			params.put("entry", entry);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException(PortalErrors.FILE_PROCESS_ERROR);
		}
		
		String[] columns = new String[14];
	    columns[0] = entry.getCaseNumber();
	    columns[1] = entry.getCxAndroidScanId();
	    columns[2] = entry.getProjectName();
	    columns[3] = entry.getReportId();
	    columns[4] = entry.getHighCount();
	    columns[5] = entry.getMediumCount();
	    columns[6] = entry.getLowCount();
	    columns[7] = entry.getInfoCount();
	    columns[8] = entry.getScanDate();
	    columns[9] = entry.getScanTime();
	    columns[10] = entry.getSignatureSetName();
	    columns[11] = entry.getExcludedPackageNameList();
	    columns[12] = entry.getEngineVersion();
	    columns[13] = entry.getOwnerGroup();
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}
}