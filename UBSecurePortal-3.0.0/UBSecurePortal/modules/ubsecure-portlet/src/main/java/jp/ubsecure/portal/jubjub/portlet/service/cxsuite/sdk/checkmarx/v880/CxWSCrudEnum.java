/**
 * CxWSCrudEnum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class CxWSCrudEnum implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CxWSCrudEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Create = "Create";
    public static final java.lang.String _Delete = "Delete";
    public static final java.lang.String _Update = "Update";
    public static final java.lang.String _View = "View";
    public static final java.lang.String _Run = "Run";
    public static final java.lang.String _Investigate = "Investigate";
    public static final CxWSCrudEnum Create = new CxWSCrudEnum(_Create);
    public static final CxWSCrudEnum Delete = new CxWSCrudEnum(_Delete);
    public static final CxWSCrudEnum Update = new CxWSCrudEnum(_Update);
    public static final CxWSCrudEnum View = new CxWSCrudEnum(_View);
    public static final CxWSCrudEnum Run = new CxWSCrudEnum(_Run);
    public static final CxWSCrudEnum Investigate = new CxWSCrudEnum(_Investigate);
    public java.lang.String getValue() { return _value_;}
    public static CxWSCrudEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CxWSCrudEnum enumeration = (CxWSCrudEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CxWSCrudEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CxWSCrudEnum.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSCrudEnum"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
