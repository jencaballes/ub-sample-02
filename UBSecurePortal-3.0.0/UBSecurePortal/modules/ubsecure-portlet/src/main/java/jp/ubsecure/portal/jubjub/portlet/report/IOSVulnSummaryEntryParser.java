package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.IOSVulnSummaryEntry;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class IOSVulnSummaryEntryParser implements CSVEntryConverter<IOSVulnSummaryEntry> {

private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(IOSVulnSummaryEntryParser.class);
	
	@Override
	public String[] convertEntry(IOSVulnSummaryEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (entry == null) {
			params.put("entry", entry);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException(PortalErrors.FILE_PROCESS_ERROR);					
		}
		
		String[] columns = new String[12];
		
		columns[0] = entry.getNumber();
	    columns[1] = entry.getGroupName();
	    columns[2] = entry.getProjectName();
	    columns[3] = entry.getCaseNumber();
	    columns[4] = entry.getScanId();
	    columns[5] = entry.getPreset();
	    columns[6] = entry.getExecutionDate();
	    columns[7] = entry.getSeverity();
	    columns[8] = entry.getVulnerabilityName();
	    columns[9] = entry.getQueryName();
	    columns[10] = entry.getInputName();
	    columns[11] = entry.getReportId();
	    
	    for (int index = 0; index < columns.length; index++) {
	    	if (columns[index] == null) {
	    		columns[index] = PortalConstants.STRING_EMPTY;
	    	}
	    }
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}
}
