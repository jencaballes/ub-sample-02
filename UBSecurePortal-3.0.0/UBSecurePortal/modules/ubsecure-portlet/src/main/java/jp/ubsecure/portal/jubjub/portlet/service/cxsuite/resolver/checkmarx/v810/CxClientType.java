/**
 * CxClientType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810;

public class CxClientType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected CxClientType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _None = "None";
    public static final java.lang.String _WebPortal = "WebPortal";
    public static final java.lang.String _CLI = "CLI";
    public static final java.lang.String _Eclipse = "Eclipse";
    public static final java.lang.String _VS = "VS";
    public static final java.lang.String _InteliJ = "InteliJ";
    public static final java.lang.String _Audit = "Audit";
    public static final java.lang.String _SDK = "SDK";
    public static final java.lang.String _Jenkins = "Jenkins";
    public static final java.lang.String _TFSBuild = "TFSBuild";
    public static final java.lang.String _Importer = "Importer";
    public static final java.lang.String _Other = "Other";
    public static final CxClientType None = new CxClientType(_None);
    public static final CxClientType WebPortal = new CxClientType(_WebPortal);
    public static final CxClientType CLI = new CxClientType(_CLI);
    public static final CxClientType Eclipse = new CxClientType(_Eclipse);
    public static final CxClientType VS = new CxClientType(_VS);
    public static final CxClientType InteliJ = new CxClientType(_InteliJ);
    public static final CxClientType Audit = new CxClientType(_Audit);
    public static final CxClientType SDK = new CxClientType(_SDK);
    public static final CxClientType Jenkins = new CxClientType(_Jenkins);
    public static final CxClientType TFSBuild = new CxClientType(_TFSBuild);
    public static final CxClientType Importer = new CxClientType(_Importer);
    public static final CxClientType Other = new CxClientType(_Other);
    public java.lang.String getValue() { return _value_;}
    public static CxClientType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        CxClientType enumeration = (CxClientType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static CxClientType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CxClientType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com", "CxClientType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
