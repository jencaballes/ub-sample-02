/**
 * CxWSResolver.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880;

public interface CxWSResolver extends javax.xml.rpc.Service {
    public java.lang.String getCxWSResolverSoapAddress();

    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResolverSoap getCxWSResolverSoap() throws javax.xml.rpc.ServiceException;

    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResolverSoap getCxWSResolverSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
