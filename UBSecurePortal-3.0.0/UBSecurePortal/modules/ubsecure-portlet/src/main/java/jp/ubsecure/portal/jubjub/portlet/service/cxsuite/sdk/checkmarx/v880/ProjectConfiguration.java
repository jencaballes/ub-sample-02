/**
 * ProjectConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class ProjectConfiguration  implements java.io.Serializable {
    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSettings projectSettings;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceCodeSettings sourceCodeSettings;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScheduleSettings scheduleSettings;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScanActionSettings scanActionSettings;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSProjectIssueTrackingSettings projectIssueTrackingSettings;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSProjectCustomField[] customFields;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.DataRetentionSettings dataRetentionSettings;

    public ProjectConfiguration() {
    }

    public ProjectConfiguration(
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSettings projectSettings,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceCodeSettings sourceCodeSettings,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScheduleSettings scheduleSettings,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScanActionSettings scanActionSettings,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSProjectIssueTrackingSettings projectIssueTrackingSettings,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSProjectCustomField[] customFields,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.DataRetentionSettings dataRetentionSettings) {
           this.projectSettings = projectSettings;
           this.sourceCodeSettings = sourceCodeSettings;
           this.scheduleSettings = scheduleSettings;
           this.scanActionSettings = scanActionSettings;
           this.projectIssueTrackingSettings = projectIssueTrackingSettings;
           this.customFields = customFields;
           this.dataRetentionSettings = dataRetentionSettings;
    }


    /**
     * Gets the projectSettings value for this ProjectConfiguration.
     * 
     * @return projectSettings
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSettings getProjectSettings() {
        return projectSettings;
    }


    /**
     * Sets the projectSettings value for this ProjectConfiguration.
     * 
     * @param projectSettings
     */
    public void setProjectSettings(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ProjectSettings projectSettings) {
        this.projectSettings = projectSettings;
    }


    /**
     * Gets the sourceCodeSettings value for this ProjectConfiguration.
     * 
     * @return sourceCodeSettings
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceCodeSettings getSourceCodeSettings() {
        return sourceCodeSettings;
    }


    /**
     * Sets the sourceCodeSettings value for this ProjectConfiguration.
     * 
     * @param sourceCodeSettings
     */
    public void setSourceCodeSettings(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.SourceCodeSettings sourceCodeSettings) {
        this.sourceCodeSettings = sourceCodeSettings;
    }


    /**
     * Gets the scheduleSettings value for this ProjectConfiguration.
     * 
     * @return scheduleSettings
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScheduleSettings getScheduleSettings() {
        return scheduleSettings;
    }


    /**
     * Sets the scheduleSettings value for this ProjectConfiguration.
     * 
     * @param scheduleSettings
     */
    public void setScheduleSettings(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScheduleSettings scheduleSettings) {
        this.scheduleSettings = scheduleSettings;
    }


    /**
     * Gets the scanActionSettings value for this ProjectConfiguration.
     * 
     * @return scanActionSettings
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScanActionSettings getScanActionSettings() {
        return scanActionSettings;
    }


    /**
     * Sets the scanActionSettings value for this ProjectConfiguration.
     * 
     * @param scanActionSettings
     */
    public void setScanActionSettings(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.ScanActionSettings scanActionSettings) {
        this.scanActionSettings = scanActionSettings;
    }


    /**
     * Gets the projectIssueTrackingSettings value for this ProjectConfiguration.
     * 
     * @return projectIssueTrackingSettings
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSProjectIssueTrackingSettings getProjectIssueTrackingSettings() {
        return projectIssueTrackingSettings;
    }


    /**
     * Sets the projectIssueTrackingSettings value for this ProjectConfiguration.
     * 
     * @param projectIssueTrackingSettings
     */
    public void setProjectIssueTrackingSettings(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSProjectIssueTrackingSettings projectIssueTrackingSettings) {
        this.projectIssueTrackingSettings = projectIssueTrackingSettings;
    }


    /**
     * Gets the customFields value for this ProjectConfiguration.
     * 
     * @return customFields
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSProjectCustomField[] getCustomFields() {
        return customFields;
    }


    /**
     * Sets the customFields value for this ProjectConfiguration.
     * 
     * @param customFields
     */
    public void setCustomFields(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSProjectCustomField[] customFields) {
        this.customFields = customFields;
    }


    /**
     * Gets the dataRetentionSettings value for this ProjectConfiguration.
     * 
     * @return dataRetentionSettings
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.DataRetentionSettings getDataRetentionSettings() {
        return dataRetentionSettings;
    }


    /**
     * Sets the dataRetentionSettings value for this ProjectConfiguration.
     * 
     * @param dataRetentionSettings
     */
    public void setDataRetentionSettings(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.DataRetentionSettings dataRetentionSettings) {
        this.dataRetentionSettings = dataRetentionSettings;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProjectConfiguration)) return false;
        ProjectConfiguration other = (ProjectConfiguration) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.projectSettings==null && other.getProjectSettings()==null) || 
             (this.projectSettings!=null &&
              this.projectSettings.equals(other.getProjectSettings()))) &&
            ((this.sourceCodeSettings==null && other.getSourceCodeSettings()==null) || 
             (this.sourceCodeSettings!=null &&
              this.sourceCodeSettings.equals(other.getSourceCodeSettings()))) &&
            ((this.scheduleSettings==null && other.getScheduleSettings()==null) || 
             (this.scheduleSettings!=null &&
              this.scheduleSettings.equals(other.getScheduleSettings()))) &&
            ((this.scanActionSettings==null && other.getScanActionSettings()==null) || 
             (this.scanActionSettings!=null &&
              this.scanActionSettings.equals(other.getScanActionSettings()))) &&
            ((this.projectIssueTrackingSettings==null && other.getProjectIssueTrackingSettings()==null) || 
             (this.projectIssueTrackingSettings!=null &&
              this.projectIssueTrackingSettings.equals(other.getProjectIssueTrackingSettings()))) &&
            ((this.customFields==null && other.getCustomFields()==null) || 
             (this.customFields!=null &&
              java.util.Arrays.equals(this.customFields, other.getCustomFields()))) &&
            ((this.dataRetentionSettings==null && other.getDataRetentionSettings()==null) || 
             (this.dataRetentionSettings!=null &&
              this.dataRetentionSettings.equals(other.getDataRetentionSettings())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProjectSettings() != null) {
            _hashCode += getProjectSettings().hashCode();
        }
        if (getSourceCodeSettings() != null) {
            _hashCode += getSourceCodeSettings().hashCode();
        }
        if (getScheduleSettings() != null) {
            _hashCode += getScheduleSettings().hashCode();
        }
        if (getScanActionSettings() != null) {
            _hashCode += getScanActionSettings().hashCode();
        }
        if (getProjectIssueTrackingSettings() != null) {
            _hashCode += getProjectIssueTrackingSettings().hashCode();
        }
        if (getCustomFields() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomFields());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomFields(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDataRetentionSettings() != null) {
            _hashCode += getDataRetentionSettings().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProjectConfiguration.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ProjectConfiguration"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("projectSettings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ProjectSettings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ProjectSettings"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceCodeSettings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceCodeSettings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SourceCodeSettings"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduleSettings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScheduleSettings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScheduleSettings"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scanActionSettings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScanActionSettings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScanActionSettings"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("projectIssueTrackingSettings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ProjectIssueTrackingSettings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSProjectIssueTrackingSettings"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customFields");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CustomFields"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSProjectCustomField"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSProjectCustomField"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataRetentionSettings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "DataRetentionSettings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "DataRetentionSettings"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
