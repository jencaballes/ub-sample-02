package jp.ubsecure.portal.jubjub.portlet.form.navigator.organization;

import java.util.Locale;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.servlet.taglib.ui.FormNavigatorCategory;
import com.liferay.users.admin.web.servlet.taglib.ui.OrganizationOrganizationInformationFormNavigatorCategory;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property ={
		PortalConstants.KEY_FORM_NAVIGATOR_CATEGORY_ORDER + PortalConstants.CATEGORY_ORDER_ORGANIZATION_ORGANIZATION_INFORMATION
	},
	service = FormNavigatorCategory.class
)
public class OrganizationOrganizationInformationFormNavigatorCategoryExt extends OrganizationOrganizationInformationFormNavigatorCategory {
	@Override
	public String getLabel(Locale locale) {
		return LanguageUtil.get(locale, PortalConstants.KEY_LABEL_GROUP_INFORMATION);
	}
	
	@Override
	public String getFormNavigatorId() {
		return PortalConstants.ORGANIZATION_PREFIX + super.getFormNavigatorId();
	}
}
