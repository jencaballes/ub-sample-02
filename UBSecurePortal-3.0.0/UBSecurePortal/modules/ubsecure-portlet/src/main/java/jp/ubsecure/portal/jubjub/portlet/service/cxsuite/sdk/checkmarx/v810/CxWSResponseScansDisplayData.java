/**
 * CxWSResponseScansDisplayData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810;

public class CxWSResponseScansDisplayData  extends jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSBasicRepsonse  implements java.io.Serializable {
    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ScanDisplayData[] scanList;

    public CxWSResponseScansDisplayData() {
    }

    public CxWSResponseScansDisplayData(
           boolean isSuccesfull,
           java.lang.String errorMessage,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ScanDisplayData[] scanList) {
        super(
            isSuccesfull,
            errorMessage);
        this.scanList = scanList;
    }


    /**
     * Gets the scanList value for this CxWSResponseScansDisplayData.
     * 
     * @return scanList
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ScanDisplayData[] getScanList() {
        return scanList;
    }


    /**
     * Sets the scanList value for this CxWSResponseScansDisplayData.
     * 
     * @param scanList
     */
    public void setScanList(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ScanDisplayData[] scanList) {
        this.scanList = scanList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CxWSResponseScansDisplayData)) return false;
        CxWSResponseScansDisplayData other = (CxWSResponseScansDisplayData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.scanList==null && other.getScanList()==null) || 
             (this.scanList!=null &&
              java.util.Arrays.equals(this.scanList, other.getScanList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getScanList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getScanList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getScanList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CxWSResponseScansDisplayData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSResponseScansDisplayData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scanList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScanList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScanDisplayData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScanDisplayData"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
