/**
 * SetTeamLdapGroupsMapping.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class SetTeamLdapGroupsMapping  implements java.io.Serializable {
    private java.lang.String sessionId;

    private java.lang.String teamId;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSLdapGroupMapping[] ldapGroups;

    public SetTeamLdapGroupsMapping() {
    }

    public SetTeamLdapGroupsMapping(
           java.lang.String sessionId,
           java.lang.String teamId,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSLdapGroupMapping[] ldapGroups) {
           this.sessionId = sessionId;
           this.teamId = teamId;
           this.ldapGroups = ldapGroups;
    }


    /**
     * Gets the sessionId value for this SetTeamLdapGroupsMapping.
     * 
     * @return sessionId
     */
    public java.lang.String getSessionId() {
        return sessionId;
    }


    /**
     * Sets the sessionId value for this SetTeamLdapGroupsMapping.
     * 
     * @param sessionId
     */
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }


    /**
     * Gets the teamId value for this SetTeamLdapGroupsMapping.
     * 
     * @return teamId
     */
    public java.lang.String getTeamId() {
        return teamId;
    }


    /**
     * Sets the teamId value for this SetTeamLdapGroupsMapping.
     * 
     * @param teamId
     */
    public void setTeamId(java.lang.String teamId) {
        this.teamId = teamId;
    }


    /**
     * Gets the ldapGroups value for this SetTeamLdapGroupsMapping.
     * 
     * @return ldapGroups
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSLdapGroupMapping[] getLdapGroups() {
        return ldapGroups;
    }


    /**
     * Sets the ldapGroups value for this SetTeamLdapGroupsMapping.
     * 
     * @param ldapGroups
     */
    public void setLdapGroups(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSLdapGroupMapping[] ldapGroups) {
        this.ldapGroups = ldapGroups;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetTeamLdapGroupsMapping)) return false;
        SetTeamLdapGroupsMapping other = (SetTeamLdapGroupsMapping) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sessionId==null && other.getSessionId()==null) || 
             (this.sessionId!=null &&
              this.sessionId.equals(other.getSessionId()))) &&
            ((this.teamId==null && other.getTeamId()==null) || 
             (this.teamId!=null &&
              this.teamId.equals(other.getTeamId()))) &&
            ((this.ldapGroups==null && other.getLdapGroups()==null) || 
             (this.ldapGroups!=null &&
              java.util.Arrays.equals(this.ldapGroups, other.getLdapGroups())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSessionId() != null) {
            _hashCode += getSessionId().hashCode();
        }
        if (getTeamId() != null) {
            _hashCode += getTeamId().hashCode();
        }
        if (getLdapGroups() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLdapGroups());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLdapGroups(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetTeamLdapGroupsMapping.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", ">SetTeamLdapGroupsMapping"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "sessionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("teamId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "teamId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ldapGroups");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ldapGroups"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSLdapGroupMapping"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSLdapGroupMapping"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
