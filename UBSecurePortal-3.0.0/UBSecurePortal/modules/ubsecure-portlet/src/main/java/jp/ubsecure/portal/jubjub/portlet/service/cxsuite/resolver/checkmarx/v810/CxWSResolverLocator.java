/**
 * CxWSResolverLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810;

import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.CxScanManager;

public class CxWSResolverLocator extends org.apache.axis.client.Service implements jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810.CxWSResolver {

    public CxWSResolverLocator() {
    }


    public CxWSResolverLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CxWSResolverLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CxWSResolverSoap
    //private java.lang.String CxWSResolverSoap_address = "http://192.168.10.23/Cxwebinterface/CxWSResolver.asmx";
    private java.lang.String CxWSResolverSoap_address = CxScanManager.getResolverUrl();

    public java.lang.String getCxWSResolverSoapAddress() {
        return CxWSResolverSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CxWSResolverSoapWSDDServiceName = "CxWSResolverSoap";

    public java.lang.String getCxWSResolverSoapWSDDServiceName() {
        return CxWSResolverSoapWSDDServiceName;
    }

    public void setCxWSResolverSoapWSDDServiceName(java.lang.String name) {
        CxWSResolverSoapWSDDServiceName = name;
    }

    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810.CxWSResolverSoap getCxWSResolverSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CxWSResolverSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCxWSResolverSoap(endpoint);
    }

    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810.CxWSResolverSoap getCxWSResolverSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810.CxWSResolverSoapStub _stub = new jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810.CxWSResolverSoapStub(portAddress, this);
            _stub.setPortName(getCxWSResolverSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCxWSResolverSoapEndpointAddress(java.lang.String address) {
        CxWSResolverSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810.CxWSResolverSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810.CxWSResolverSoapStub _stub = new jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v810.CxWSResolverSoapStub(new java.net.URL(CxWSResolverSoap_address), this);
                _stub.setPortName(getCxWSResolverSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CxWSResolverSoap".equals(inputPortName)) {
            return getCxWSResolverSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://Checkmarx.com", "CxWSResolver");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://Checkmarx.com", "CxWSResolverSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CxWSResolverSoap".equals(portName)) {
            setCxWSResolverSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
