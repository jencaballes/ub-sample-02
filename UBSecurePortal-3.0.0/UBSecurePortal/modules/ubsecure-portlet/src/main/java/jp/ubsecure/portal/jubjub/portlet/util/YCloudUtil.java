package jp.ubsecure.portal.jubjub.portlet.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;

import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ycloud.cs.Storage;
import jp.ycloud.cs.StorageExceptionBase;

/**
 * Class use for uploading and downloading of report files to YCloud Storage
 * 
 * @author ASI558
 *
 */
public class YCloudUtil {
	
	private static YCloudUtil yCloudUtil;
	private Storage ycloudstorage;
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(YCloudUtil.class);
	
	private YCloudUtil() throws UBSPortalException{
		//initialize the ycloud.properties
		URL resourceURL = null;
    	File file = null; 
    	URLClassLoader urlLoader = null;
    	ResourceBundle bundle = null;
    	
    	try{
    		String resourceBaseDirectory = FileProcessUtil.getResourceBasePath();
    		if(CommonUtil.isStringNullOrEmpty(resourceBaseDirectory)){
    			resourceBaseDirectory = FileProcessUtil.getDefaultResourceBasePath();
    		}
    		
    		file = new File(resourceBaseDirectory + File.separator);
    		resourceURL = file.toURI().toURL();
    		urlLoader = new URLClassLoader(new URL[] {resourceURL}); 
        	bundle = ResourceBundle.getBundle(PortalConstants.YCLOUD, Locale.getDefault(), urlLoader);
        	ycloudstorage = new Storage(bundle);
			ycloudstorage.setSslVerification(true);
    	} catch(MalformedURLException m){
    		log.debug(PortalErrors.CANNOT_GET_YCLOUD_CONFIG_FILE, PortalConstants.METHOD_YCLOUD_UTIL, null, m);
    		throw new UBSPortalException(PortalErrors.CANNOT_GET_YCLOUD_CONFIG_FILE, m);
    	} catch (StorageExceptionBase e) {
    		log.debug(PortalErrors.YCLOUD_RESPONSE_ERR, PortalConstants.METHOD_YCLOUD_UTIL, null, e);
			throw new UBSPortalException(PortalErrors.YCLOUD_RESPONSE_ERR, e);
		}
	}
	
	public static YCloudUtil getInstance(){
		try {
			if (null == yCloudUtil) {
				yCloudUtil = new YCloudUtil();
			}
		} catch (UBSPortalException e) {
			log.debug(PortalErrors.YCLOUD_INSTANCE_ERR, PortalConstants.METHOD_GET_INSTANCE, null, e);
		}
		return yCloudUtil;
	}
	
	/**
	 * downloads the file from Ycloud storage to Portal directory
	 * 
	 * @param strBucketName
	 * @param strFileName
	 * @param strReportDirectory
	 * @return
	 * @throws UBSPortalException
	 */
	public boolean downloadObject(String strBucketName, String strFileName,
			String strReportDirectory) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		HttpResponse response = null;
		boolean bIsSuccessful = false;
		File file = null;
		String strDownloadName = PortalConstants.STR_FWD_SLASH + strFileName;

		try {
			
			if (CommonUtil.isStringNullOrEmpty(strBucketName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_BUCKET_NAME));
				throw new UBSPortalException(PortalErrors.INVALID_BUCKETNAME);
			}
			if (CommonUtil.isStringNullOrEmpty(strFileName)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
				throw new UBSPortalException(PortalErrors.INVALID_FILENAME);
			}
			if (CommonUtil.isStringNullOrEmpty(strReportDirectory)) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
				throw new UBSPortalException(PortalErrors.INVALID_CX_REPORT_DIRECTORY);
			}
			
			//retry 5 times
			for (int downloadAttempt = 1; downloadAttempt <= 5; downloadAttempt++) {
				response = ycloudstorage.downloadObject(strBucketName,
						strDownloadName, strReportDirectory, null);
				if (response == null) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
					log.debug(PortalErrors.YCLOUD_RESPONSE_ERR, PortalConstants.METHOD_DOWNLOAD_OBJECT, null, new UBSPortalException(PortalErrors.YCLOUD_RESPONSE_ERR));
				} else if (response.getStatusLine() != null
						&& (response.getStatusLine().getStatusCode() == PortalConstants.HTTP_OK_STATUS)) {
					file = FileUtils.getFile(strReportDirectory);
					if (CommonUtil.isValidAndExistingFile(file)) {
						bIsSuccessful = true;
						break;
					} else {
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
						log.debug(PortalErrors.YCLOUD_RESPONSE_ERR, PortalConstants.METHOD_DOWNLOAD_OBJECT, null, new UBSPortalException(PortalErrors.YCLOUD_RESPONSE_ERR));
					}
				}
			}
		} catch (StorageExceptionBase e) {
			params.put("strBucketName", strBucketName);
			params.put("strDownloadName", strDownloadName);
			params.put("strReportDirectory", strReportDirectory);
			params.put("response", response);
			log.debug(PortalErrors.YCLOUD_RESPONSE_ERR, PortalConstants.METHOD_DOWNLOAD_OBJECT, params, e);
			throw new UBSPortalException(PortalErrors.DOWNLOAD_REPORT_FAILED, e);
		} catch (UBSPortalException ubse) {
			params.put("strBucketName", strBucketName);
			params.put("strFileName", strFileName);
			params.put("strReportDirectory", strReportDirectory);
			params.put("strDownloadName", strDownloadName);
			params.put("response", response);
			log.debug(PortalErrors.YCLOUD_RESPONSE_ERR, PortalConstants.METHOD_DOWNLOAD_OBJECT, params, ubse);
			throw ubse;
		} catch (Exception e) {
			params.put("strBucketName", strBucketName);
			params.put("strFileName", strFileName);
			params.put("strReportDirectory", strReportDirectory);
			params.put("strDownloadName", strDownloadName);
			params.put("response", response);
			log.debug(PortalErrors.YCLOUD_RESPONSE_ERR, PortalConstants.METHOD_DOWNLOAD_OBJECT, params, e);
		} finally {
			response = null;
			params.clear();
			params = null;
		}
		return bIsSuccessful;
	}
	
	/**
	 * @param strBucketName the bucket name where the report file is located
	 * @param strFileName the file name of the report file
	 * @return flag if report deletion is successful or not
	 * @throws UBSPortalException the exception thrown if report deletion encounters an error
	 */
	public boolean deleteFile(String strBucketName, String strFileName) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		HttpResponse response = null;
		boolean bIsSuccesful = false;
		
		try {
			if (strFileName == null || strFileName.isEmpty()) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILENAME));
				throw new UBSPortalException(PortalErrors.INVALID_CX_REPORT_NAME);
			}
			
			response = ycloudstorage.deleteObject(strBucketName, PortalConstants.STR_FWD_SLASH + strFileName, null);
			
			if (response == null) {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_NULL, PortalConstants.ERROR_LOG_PARAM_RESPONSE));
				throw new UBSPortalException(PortalErrors.YCLOUD_RESPONSE_ERR);
			} else  if (response.getStatusLine() != null && response.getStatusLine().getStatusCode() == PortalConstants.HTTP_NO_CONTENT) {
				bIsSuccesful = true;
			} else {
				params.put(PortalConstants.ERROR_LOG_MESSAGE, PortalConstants.ERROR_LOG_MESSAGE_REPORT_DELETION_FAILED);
				throw new UBSPortalException(PortalErrors.DELETE_REPORT_FAILED);
			}
			
		} catch (StorageExceptionBase se) {
			params.put("strBucketName: ", strBucketName);
			params.put("strFileName: ", strFileName);
			log.debug(PortalErrors.DELETE_REPORT_FAILED, PortalConstants.METHOD_DELETE_FILE, params, se);
			throw new UBSPortalException(PortalErrors.DELETE_REPORT_FAILED, PortalMessages.DELETE_REPORT_FAILED, se);
		} catch (UBSPortalException ubse) {
			params.put("strFileName", strFileName);
			params.put("response: ", response);
			log.debug(PortalErrors.DELETE_REPORT_FAILED, PortalConstants.METHOD_DELETE_FILE, params, ubse);
			throw new UBSPortalException(PortalErrors.DELETE_REPORT_FAILED, PortalMessages.DELETE_REPORT_FAILED, ubse);
		} finally {
			response = null;
			params.clear();
			params = null;
		}
		
		return bIsSuccesful;
	}
	
	public void close() {
		try{
			if (ycloudstorage != null) {
//				ycloudstorage.closeHttpClient();
			}
		} catch (Exception e) {
			log.debug(PortalErrors.YCLOUD_RESPONSE_ERR, "close", null, e);
		}
	}
}
