/**
 * CxWSProjectIssueTrackingSettings.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810;

public class CxWSProjectIssueTrackingSettings  implements java.io.Serializable {
    private long trackingSystemID;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSIssueTrackingParam[] params;

    public CxWSProjectIssueTrackingSettings() {
    }

    public CxWSProjectIssueTrackingSettings(
           long trackingSystemID,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSIssueTrackingParam[] params) {
           this.trackingSystemID = trackingSystemID;
           this.params = params;
    }


    /**
     * Gets the trackingSystemID value for this CxWSProjectIssueTrackingSettings.
     * 
     * @return trackingSystemID
     */
    public long getTrackingSystemID() {
        return trackingSystemID;
    }


    /**
     * Sets the trackingSystemID value for this CxWSProjectIssueTrackingSettings.
     * 
     * @param trackingSystemID
     */
    public void setTrackingSystemID(long trackingSystemID) {
        this.trackingSystemID = trackingSystemID;
    }


    /**
     * Gets the params value for this CxWSProjectIssueTrackingSettings.
     * 
     * @return params
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSIssueTrackingParam[] getParams() {
        return params;
    }


    /**
     * Sets the params value for this CxWSProjectIssueTrackingSettings.
     * 
     * @param params
     */
    public void setParams(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxWSIssueTrackingParam[] params) {
        this.params = params;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CxWSProjectIssueTrackingSettings)) return false;
        CxWSProjectIssueTrackingSettings other = (CxWSProjectIssueTrackingSettings) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.trackingSystemID == other.getTrackingSystemID() &&
            ((this.params==null && other.getParams()==null) || 
             (this.params!=null &&
              java.util.Arrays.equals(this.params, other.getParams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getTrackingSystemID()).hashCode();
        if (getParams() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParams());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParams(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CxWSProjectIssueTrackingSettings.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSProjectIssueTrackingSettings"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackingSystemID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "TrackingSystemID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "Params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSIssueTrackingParam"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSIssueTrackingParam"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
