package jp.ubsecure.portal.jubjub.portlet.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.googlecode.jcsv.CSVStrategy;
import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectStatus;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ProjectType;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.PresetListItem;
import jp.ubsecure.portal.jubjub.portlet.model.Project;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectItem;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectListEntry;
import jp.ubsecure.portal.jubjub.portlet.model.ProjectUsers;
import jp.ubsecure.portal.jubjub.portlet.model.Report;
import jp.ubsecure.portal.jubjub.portlet.model.ResponseModel;
import jp.ubsecure.portal.jubjub.portlet.model.ResultItem;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.model.ScanItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexDetectionResultItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexLoginSettingItem;
import jp.ubsecure.portal.jubjub.portlet.model.VexScanSetting;
import jp.ubsecure.portal.jubjub.portlet.report.ProjectListEntryParser;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ProjectUsersLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ReportLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.VexDetectionResultLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.CxScanManager;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser;
import jp.ubsecure.portal.jubjub.portlet.util.PortletCommonUtil;

public class ControllerHelper {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(ControllerHelper.class);
	
	public static User getUser (long lUserId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		User user = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}
				
				user = UserLocalServiceUtil.getUser(lUserId);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (NoSuchUserException e) {
			params.put("lUserId", lUserId);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_GET_USER, params, e);
			throw new UBSPortalException(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalMessages.USER_DOES_NOT_EXIST, e);
		} catch (PortalException e) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_USER, params, e);
			throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, e);
		} catch (SystemException e) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_USER, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lUserId", lUserId);
			}
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_USER, params, e);
			throw e;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return user;
	}
	
	public static List<Organization> getOrganizations (String strAttribute) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Organization> organizationList = null;
		List<Organization> resultList = new ArrayList<Organization>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				organizationList = OrganizationLocalServiceUtil.getOrganizations(0, OrganizationLocalServiceUtil.getOrganizationsCount());
				
				for (Organization organization : organizationList) {
					if (organization.getExpandoBridge().getAttribute(strAttribute) != null) {
						if (GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(strAttribute))) {
							resultList.add(organization);
						}
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (SystemException e) {
			params.put("strAttribute", strAttribute);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_ORGANIZATIONS, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_ORGANIZATIONS, null, e);
				throw e;
			}
		} finally {
			if (!CommonUtil.isListNullOrEmpty(organizationList)) {
				organizationList = null;
			}
			
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return resultList;
	}
	
	public static List<PresetListItem> getPresets() throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String> presetListMap = null;
		List<PresetListItem> presetList = null;
		Set<String> presets = null;
		PresetListItem item = null;
		String presetId = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				presetListMap = ConfigurationFileParser.getPresetConfig();
				
				if (!CommonUtil.isMapNullOrEmpty(presetListMap)) {
					presets = presetListMap.keySet();
					presetList = new ArrayList<PresetListItem>();
					for (String presetIdList : presets) {
						presetId = StringUtil.extractLast(presetIdList, PortalConstants.PRESET_PREFIX);
						item = new PresetListItem();
						item.setPresetId(Long.valueOf(presetId));
						item.setPresetName(presetListMap.get(presetIdList));
						
						presetList.add(item);
					}
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}			
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				log.debug(PortalErrors.ORM_EXCEPTION, PortalConstants.METHOD_GET_PRESETS, null, e);
				throw e;
			}
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(presetListMap)) {
				presetListMap = null;
			}
			
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return presetList;
	}
	
	public static List<Organization> getUserOrganizations (long lUserId , String strAttribute) throws UBSPortalException {
		List <Organization> userOrganizationList = new ArrayList<Organization>();
		List <Organization> tmpuserOrganizationList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (lUserId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_USER_ID));
					throw new UBSPortalException(PortalErrors.GET_USER_USER_ID_INVALID, PortalMessages.USER_ID_INVALID);
				}
				
				tmpuserOrganizationList = OrganizationLocalServiceUtil.getUserOrganizations(lUserId);
				
				// filter according to the attribute
				for (Organization organization : tmpuserOrganizationList) {
					if (organization.getExpandoBridge().getAttribute(strAttribute) != null) {
						if (GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute(strAttribute))) {
							userOrganizationList.add(organization);
						}
					}
				}
				
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
			
		} catch (SystemException e) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_USER_ORGANIZATIONS, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lUserId", lUserId);
			}
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_USER_ORGANIZATIONS, params, e);
			throw e;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return userOrganizationList;
	}
	
	public static List<User> getOrganizationUsers (long lOrganizationId) throws UBSPortalException {
		List<User> organizationUserList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (lOrganizationId == PortalConstants.LONG_ZERO) {
					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_GROUP_ID));
					throw new UBSPortalException(PortalErrors.GET_ORGANIZATION_ORGANIZATION_ID_INVALID, PortalMessages.ORGANIZATION_ID_INVALID);
				} else {
					organizationUserList = UserLocalServiceUtil.getOrganizationUsers(lOrganizationId);
				}
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (SystemException e) {
			params.put("lOrganizationId", lOrganizationId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_ORGANIZATION_USERS, params, e);
			throw new UBSPortalException(PortalErrors.SYSTEM_EXCEPTION, PortalMessages.SYSTEM_EXCEPTION, e);
		} catch (UBSPortalException e) {
			if (e.getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
				params = null;
			} else {
				params.put("lOrganizationId", lOrganizationId);
			}
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_ORGANIZATION_USERS, params, e);
			throw e;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return organizationUserList;
	}
	
//	public static List<ProjectUsers> getProjectUsers (long lProjectId, int iType) throws UBSPortalException {
//		Map<String, Object> params = new HashMap<String, Object>();
//		List<ProjectUsers> projectUserList = null;
//		
//		try {
//			if (PortletCommonUtil.isDBConnected()) {
//				if (lProjectId == PortalConstants.LONG_ZERO) {
//					params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
//					throw new UBSPortalException(PortalErrors.GET_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
//				} else {
//					projectUserList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
//				}
//			} else {
//				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
//			}
//		} catch (PortalException e) {
//			params.put("lProjectId", lProjectId);
//
//			if (CommonUtil.isPortalError(e.getMessage())) {
//				String message = null;
//				
//				if (iType == ProjectType.ANDROID.getInteger()) {
//					message = CommonUtil.getAndroidErrorMessage(e.getMessage());
//				} else {
//					message = CommonUtil.getCxErrorMessage(e.getMessage());
//				}
//				log.debug(e.getMessage(), PortalConstants.METHOD_GET_PROJECT_USERS, params, e);
//				throw new UBSPortalException(e.getMessage(), message, e);
//			} else {
//				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT_USERS, params, e);
//				throw new UBSPortalException(PortalErrors.PORTAL_EXCEPTION, PortalMessages.PORTAL_EXCEPTION, e);
//			}
//		} catch (UBSPortalException e) {
//			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_PROJECT_USERS, params, e);
//			throw e;
//		} finally {
//			if (!CommonUtil.isMapNullOrEmpty(params)) {
//				params.clear();
//				params = null;
//			}
//		}
//		
//		return projectUserList;
//	}
	
	public static ResponseModel getProjects (long lUserId, int iType, int start) {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = null;
		ResponseModel rModel = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				User user = UserLocalServiceUtil.getUser(lUserId);
				projectList = ProjectLocalServiceUtil.getProjects(null, user, iType, start);
				
				rModel = new ResponseModel();
				rModel.setStatus(true);
				rModel.setData(projectList);
				rModel.setMessage(null);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (SystemException e) {
			params.put("userId", lUserId);
			params.put("type", iType);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, e);
		} catch (PortalException e) {
			params.put("userId", lUserId);
			params.put("type", iType);
			
			if (CommonUtil.isPortalError(e.getMessage())) {
				log.debug(e.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, e);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, e);
			}
		} catch (UBSPortalException e) {
			rModel = new ResponseModel();
			rModel.setStatus(false);
			rModel.setData(null);
			rModel.setMessage(PortalConstants.ORM_EXCEPTION);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_PROJECTS, null, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return rModel;
	}
	
	public static ResponseModel getProjects (long lUserId, int iType, Map<String, Object> searchedProject, int start) {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> projectList = null;
		ResponseModel rModel = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				User user = UserLocalServiceUtil.getUser(lUserId);
				
				if (!CommonUtil.isMapNullOrEmpty(searchedProject)) {
					Object oProjectId = searchedProject.get(PortalConstants.PARAM_PROJECT_ID);
					Object oCaseNumber = searchedProject.get(PortalConstants.PARAM_CASE_NUMBER);
					Object oProjectName = searchedProject.get(PortalConstants.PARAM_PROJECT_NAME);
					Object oProjectEndDateLow = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_LOW);
					Object oProjectEndDateHigh = searchedProject.get(PortalConstants.PARAM_PROJECT_END_DATE_HIGH);
					Object oScanCount = searchedProject.get(PortalConstants.PARAM_NO_OF_SCANS);
					
					if (!CommonUtil.isObjectNull(oProjectId)) {
						String strProjectId = oProjectId.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strProjectId)) {
							try {
								long lProjectId = Long.parseLong(strProjectId);
								
								if (lProjectId < PortalConstants.LONG_ZERO) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
									throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
								}
							} catch (NumberFormatException e) {
								params.put("strProjectId", strProjectId);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
								throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_ID_INVALID, PortalMessages.PROJECT_ID_INVALID);
							}
						}
					}
					
					if (!CommonUtil.isObjectNull(oCaseNumber)) {
						String strCaseNumber = oCaseNumber.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strCaseNumber)) {
							if (!Validator.isAlphanumericName(strCaseNumber)) {
								params.put("strCaseNumber", strCaseNumber);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
								throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
							}
							
							if (strCaseNumber.contains(PortalConstants.STRING_BLANK_SPACE)) {
								params.put("strCaseNumber", strCaseNumber);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
								throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_INVALID, PortalMessages.CASE_NUMBER_INVALID);
							}
							
							if (CommonUtil.getStringBytes(strCaseNumber) > PortalConstants.STRING_BYTE_MAX_40) {
								params.put("strCaseNumber", strCaseNumber);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_CASE_NUMBER));
								throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_TOO_LONG, PortalMessages.CASE_NUMBER_TOO_LONG);
							}
						}
					}
					
					if (!CommonUtil.isObjectNull(oProjectName)) {
						String strProjectName = oProjectName.toString();
						
						if (CommonUtil.getStringBytes(strProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
							params.put("strProjectName", strProjectName);
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
								throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
						}
						
						if (!ControllerHelper.isCxProjectNameValid(strProjectName)) {
							params.put("strProjectName", strProjectName);
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
								throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_NAME_INVALID, PortalMessages.PROJECT_NAME_INVALID);
						}
					}
					
					String strProjectEndDateLow = null;
					String strProjectEndDateHigh = null;
					DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
					Date dteProjectEndDateLow = null;
					Date dteProjectEndDateHigh = null;
					
					if (!CommonUtil.isObjectNull(oProjectEndDateLow)) {
						strProjectEndDateLow = oProjectEndDateLow.toString();
					}
					
					if (!CommonUtil.isObjectNull(oProjectEndDateHigh)) {
						strProjectEndDateHigh = oProjectEndDateHigh.toString();
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateLow)) {
						try {
							dteProjectEndDateLow = format.parse(strProjectEndDateLow);
						} catch (ParseException e) {
							params.put("strProjectEndDateLow", strProjectEndDateLow);
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(strProjectEndDateHigh)) {
						try {
							dteProjectEndDateHigh = format.parse(strProjectEndDateHigh);
						} catch (ParseException e) {
							params.put("strProjectEndDateHigh", strProjectEndDateHigh);
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE));
							throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
						}
					}
					
					if (!CommonUtil.isObjectNull(dteProjectEndDateLow)
							&& !CommonUtil.isObjectNull(dteProjectEndDateHigh)) {
						if (dteProjectEndDateHigh.before(dteProjectEndDateLow)) {
							params.put("dteProjectEndDateLow", dteProjectEndDateLow);
							params.put("dteProjectEndDateHigh", dteProjectEndDateHigh);
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE, PortalConstants.ERROR_LOG_PARAM_PROJECT_END_DATE.toLowerCase()));
							throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID, PortalMessages.PROJECT_END_DATE_INVALID);
						}
					}
					
					if (!CommonUtil.isObjectNull(oScanCount)) {
						String strScanCount = oScanCount.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strScanCount)) {
							try {
								int iScanCount = Integer.parseInt(strScanCount);
								
								if (iScanCount < PortalConstants.INT_ZERO) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NO_OF_SCANS));
									throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_SCAN_COUNT_INVALID, PortalMessages.SCAN_COUNT_INVALID);
								}
							} catch (NumberFormatException e) {
								params.put("strScanCount", strScanCount);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_NO_OF_SCANS));
								throw new UBSPortalException(PortalErrors.SEARCH_PROJECT_SCAN_COUNT_INVALID, PortalMessages.SCAN_COUNT_INVALID);
							}
						}
					}
				}
				
				projectList = ProjectLocalServiceUtil.getProjects(searchedProject, user, iType, start);
				
				rModel = new ResponseModel();
				rModel.setStatus(true);
				rModel.setData(projectList);
				rModel.setMessage(null);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (SystemException e) {
			params.put("userId", lUserId);
			params.put("type", iType);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, e);
		} catch (PortalException e) {
			params.put("userId", lUserId);
			params.put("type", iType);
			
			if (CommonUtil.isPortalError(e.getMessage())) {
				log.debug(e.getMessage(), PortalConstants.METHOD_GET_PROJECTS, params, e);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS, params, e);
			}
		} catch (UBSPortalException e) {
			String strErrorCode = e.getErrorCode();
			
			rModel = new ResponseModel();
			rModel.setStatus(false);
			rModel.setData(null);
			rModel.setMessage(e.getErrorMessage());
			
			if (isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_PROJECTS, params, e);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_PROJECTS, params, e);
			}
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return rModel;
	}
	
	public static ResponseModel getProject (long lProjectId) {
		Map<String, Object> params = new HashMap<String, Object>();
		Project project = null;
		ResponseModel rModel = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				project = ProjectLocalServiceUtil.getProject(lProjectId);
				
				rModel = new ResponseModel();
				rModel.setStatus(true);
				rModel.setData(project);
				rModel.setMessage(null);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException e) {
			params.put("projectId", lProjectId);
			
			if (CommonUtil.isPortalError(e.getMessage())) {
				log.debug(e.getMessage(), PortalConstants.METHOD_GET_PROJECT, params, e);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, e);
			}
		} catch (SystemException e) {
			params.put("projectId", lProjectId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECT, params, e);
		} catch (UBSPortalException e) {
			rModel = new ResponseModel();
			
			rModel.setStatus(false);
			rModel.setData(null);
			rModel.setMessage(PortalConstants.ORM_EXCEPTION);
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_PROJECT, null, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return rModel;
	}
	
	public static ResponseModel getScans (long lProjectId, User user, Map<String, Object> searchedScan, int start) {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = null;
		List<ScanItem> returnList = new ArrayList<ScanItem>();
		ResponseModel rModel = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
					Object oSearchedScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
					Object oSearchedHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
					Object oSearchedCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
					Object oSearchedScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
					Object oSearchedScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
					Object oSearchedScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
					Object oSearchedScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
					
					if (!CommonUtil.isObjectNull(oSearchedScanId)) {
						String strSearchedScanId = oSearchedScanId.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strSearchedScanId)) {
							try {
								long lSearchedScanId = Long.parseLong(strSearchedScanId);
								
								if (lSearchedScanId < PortalConstants.LONG_ZERO) {
									params.put("strSearchedScanId", strSearchedScanId);
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
									throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID); 
								}
							} catch (NumberFormatException e) {
								params.put("strSearchedScanId", strSearchedScanId);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
							}
						}
					}
					
					if (!CommonUtil.isObjectNull(oSearchedHashValue)) {
						String strSearchedHashValue = oSearchedHashValue.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strSearchedHashValue)) {
							if (!Validator.isAlphanumericName(strSearchedHashValue)) {
								params.put("strSearchedHashValue", strSearchedHashValue);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
							}
							
							if (strSearchedHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
								params.put("strSearchedHashValue", strSearchedHashValue);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
							}
						}
					}
					
					String strSearchedScanRegDateLow = PortalConstants.STRING_EMPTY;
					String strSearchedScanRegDateHigh = PortalConstants.STRING_EMPTY;
					
					try {
						DateFormat formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
						Date dteRegDateLow = null;
						Calendar dteRegDateHigh = null;
						
						if (!CommonUtil.isObjectNull(oSearchedScanRegDateLow)) {
							strSearchedScanRegDateLow = oSearchedScanRegDateLow.toString();
							
							if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateLow)) {
								if (strSearchedScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
									params.put("strSearchedScanRegDateLow", strSearchedScanRegDateLow);
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
									throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID); 
								}
								
								dteRegDateLow = formatter.parse(strSearchedScanRegDateLow);
							}
						}
						
						if (!CommonUtil.isObjectNull(oSearchedScanRegDateHigh)) {
							strSearchedScanRegDateHigh = oSearchedScanRegDateHigh.toString();
							
							if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateHigh)) {
								if (strSearchedScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
									params.put("strSearchedScanRegDateHigh", strSearchedScanRegDateHigh);
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
									throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID); 
								}
								
								dteRegDateHigh = Calendar.getInstance();
								dteRegDateHigh.setTime(formatter.parse(strSearchedScanRegDateHigh));
								dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
								dteRegDateHigh.set(Calendar.MINUTE, 59);
								dteRegDateHigh.set(Calendar.SECOND, 59);
							}
						}
						
						if (!CommonUtil.isObjectNull(dteRegDateLow)
								&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
							if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
								params.put("strSearchedScanRegDateLow", strSearchedScanRegDateLow);
								params.put("strSearchedScanRegDateHigh", strSearchedScanRegDateHigh);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE.toLowerCase()));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
							}
						}
					} catch (ParseException e) {
						params.put("strSearchedScanRegDateLow", strSearchedScanRegDateLow);
						params.put("strSearchedScanRegDateHigh", strSearchedScanRegDateHigh);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID); 
					}
					
					if (!CommonUtil.isObjectNull(oSearchedCxScanId)) {
						String strSearchedCxScanId = oSearchedCxScanId.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strSearchedCxScanId)) {
							try {
								long lSearchedCxScanId = Long.parseLong(strSearchedCxScanId);
								
								if (lSearchedCxScanId < PortalConstants.LONG_ZERO) {
									params.put("strSearchedCxScanId", strSearchedCxScanId);
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
									throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID); 
								}
							} catch (NumberFormatException e) {
								params.put("strSearchedCxScanId", strSearchedCxScanId);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
							}
						}
					}
					String strScanStartTime = PortalConstants.STRING_EMPTY;
					String strScanEndTime = PortalConstants.STRING_EMPTY;
					DateFormat dateTimeFormatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
					Date dteStartTime = null;
					Calendar dteEndTime = null;
						
					if (!CommonUtil.isObjectNull(oSearchedScanStartTime)) {
						strScanStartTime = oSearchedScanStartTime.toString();
						strScanEndTime = oSearchedScanEndTime.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strScanStartTime)) {
							if (strScanStartTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
								params.put("strScanStartTime", strScanStartTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
							}else if (CommonUtil.isStringNullOrEmpty(strScanEndTime)) {
								params.put("strScanEndTime", strScanEndTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_END_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
							}
							try {
								dteStartTime = dateTimeFormatter.parse(strScanStartTime);
							}catch (ParseException e) {
								params.put("strScanStartTime", strScanStartTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID); 
							}
							
						}
					}
						
					if (!CommonUtil.isObjectNull(oSearchedScanEndTime)) {
						strScanEndTime = oSearchedScanEndTime.toString();
						strScanStartTime = oSearchedScanStartTime.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strScanEndTime)) {
							if (strScanEndTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
								params.put("strScanStartTime", strScanEndTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_END_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
							}else if (CommonUtil.isStringNullOrEmpty(strScanStartTime)) {
								params.put("strScanEndTime", strScanEndTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
							}
							
							try {
								dteEndTime = Calendar.getInstance();
								dteEndTime.setTime(dateTimeFormatter.parse(strScanEndTime));
								if (!CommonUtil.isObjectNull(dteStartTime) && !CommonUtil.isObjectNull(dteEndTime)) {
									if (dteStartTime.after(dteEndTime.getTime())) {
										params.put("strScanStartTime", strScanStartTime);
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, 
												PortalConstants.ERROR_LOG_PARAM_START_TIME, PortalConstants.ERROR_LOG_PARAM_START_TIME.toLowerCase()));
										throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_AND_END_TIME_INVALID, PortalMessages.START_TIME_AND_END_TIME_INVALID);
									}
								}
							}catch (ParseException e) {
								params.put("strScanEndTime", strScanEndTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_END_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
							}
							
						}
					}
				}
				
				scanList = ScanLocalServiceUtil.getScans(lProjectId, user, searchedScan, start);
				
				long lCompanyId = user.getCompanyId();
				
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
					if (scanList.size() == 0) {
						scanList = null;
					} else {
						for (Object item : scanList) {
							long lScanId 									= PortalConstants.LONG_ZERO;
							int scanProjectType								= PortalConstants.INT_ZERO;
							ScanItem scan 									= (ScanItem) item;
							List<VexLoginSettingItem> loginSettingList 		= null;
							VexScanSetting scanSetting 						= null;	
							
							try {
								user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
								
								if (!CommonUtil.isObjectNull(user)) {
									scan.setScanManager(user.getFirstName());
								} else {
									scan.setScanManager(PortalConstants.STRING_EMPTY);
								}
							} catch (NoSuchUserException e) {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							}
							
							//get Scan ID
							lScanId = scan.getScanId();
							scanProjectType = scan.getProjectType();
							
							try {
								if(scanProjectType == PortalConstants.PROJECT_TYPE_VEX){
									if(lScanId > PortalConstants.LONG_ZERO)
									{
										//Login Information Setting
										loginSettingList = VexScanMgmtController.getVexLoginSettingList(lScanId);
										
										//Get Vex Scan Settings
										scanSetting = VexScanMgmtController.getVexScanSetting(lScanId);
									}
									
									scan.setLoginSetting(loginSettingList);
									
									if(scanSetting != null)
									{
										scan.setMaximumDetectionLinks(scanSetting.getMaxnumdetectionlink());
									}
								} else {
									scan.setMaximumDetectionLinks(0);
									scan.setLoginSetting(loginSettingList);
								}
							} catch (Exception e) {
								scan.setMaximumDetectionLinks(0);
								scan.setLoginSetting(loginSettingList);
							} catch (Throwable e) {
								scan.setMaximumDetectionLinks(0);
								scan.setLoginSetting(loginSettingList);
							}
							
							returnList.add(scan);
						}
					}
				}
				
				rModel = new ResponseModel();
				rModel.setStatus(true);
				rModel.setData(returnList);
				rModel.setMessage(null);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException e) {
			params.put("projectId", lProjectId);
			params.put("user", user);
			params.put("searchedScan", searchedScan);
			if (CommonUtil.isPortalError(e.getMessage())) {
				log.debug(e.getMessage(), PortalConstants.METHOD_GET_SCANS, params, e);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_SCANS, params, e);
			}
		} catch (UBSPortalException e) {
			String strErrorCode = e.getErrorCode();
			
			rModel = new ResponseModel();
			rModel.setStatus(false);
			rModel.setData(null);
			rModel.setMessage(e.getErrorMessage());
			
			if (isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_SCANS, params, e);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_SCANS, params, e);
			}
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SCANS, null, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
		
		return rModel;
	}
	
	public static ResponseModel getEntireScans (int iType, Map<String, Object> searchedScan, int start) {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> scanList = null;
		List<ResultItem> returnList = new ArrayList<ResultItem>();
		ResponseModel rModel = null;

		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isMapNullOrEmpty(searchedScan)) {
					Object oSearchedScanId = searchedScan.get(PortalConstants.PARAM_SCAN_ID);
					Object oSearchedProjectName = searchedScan.get(PortalConstants.PARAM_PROJECT_NAME);
					Object oSearchedHashValue = searchedScan.get(PortalConstants.PARAM_HASH_VALUE);
					Object oSearchedGroupName = searchedScan.get(PortalConstants.PARAM_OWNER_GROUP);
					Object oSearchedScanRegDateLow = searchedScan.get(PortalConstants.PARAM_REG_DATE_LOW);
					Object oSearchedScanRegDateHigh = searchedScan.get(PortalConstants.PARAM_REG_DATE_HIGH);
					Object oSearchedCxScanId = searchedScan.get(PortalConstants.PARAM_CX_SCAN_ID);
					Object oSearchedScanStartTime= searchedScan.get(PortalConstants.PARAM_SCAN_START_TIME);
					Object oSearchedScanEndTime = searchedScan.get(PortalConstants.PARAM_SCAN_END_TIME);
					
					if (!CommonUtil.isObjectNull(oSearchedScanId)) {
						String strSearchedScanId = oSearchedScanId.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strSearchedScanId)) {
							try {
								long lSearchedScanId = Long.parseLong(strSearchedScanId);
								
								if (lSearchedScanId < PortalConstants.LONG_ZERO) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
									throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
								}
							} catch (NumberFormatException e) {
								params.put("strSearchedScanId", strSearchedScanId);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
							}
						}
					}
					
					if (!CommonUtil.isObjectNull(oSearchedProjectName)) {
						String strSearchedProjectName = oSearchedProjectName.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strSearchedProjectName)) {
							if (CommonUtil.getStringBytes(strSearchedProjectName) > PortalConstants.STRING_BYTE_MAX_100) {
								params.put("strSearchedProjectName", strSearchedProjectName);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_TOO_LONG, PortalMessages.PROJECT_NAME_TOO_LONG);
							}
							
							if (iType == ProjectType.CX_SUITE.getInteger() && !ControllerHelper.isCxProjectNameValid(strSearchedProjectName)) {
								params.put("strSearchedProjectName", strSearchedProjectName);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_NAME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_PROJECT_NAME_INVALID, PortalMessages.PROJECT_NAME_INVALID);
							}
						}
					}
					
					if (!CommonUtil.isObjectNull(oSearchedHashValue)) {
						String strSearchedHashValue = oSearchedHashValue.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strSearchedHashValue)) {
							if (!Validator.isAlphanumericName(strSearchedHashValue)) {
								params.put("strSearchedHashValue", strSearchedHashValue);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
							}
							
							if (strSearchedHashValue.contains(PortalConstants.STRING_BLANK_SPACE)) {
								params.put("strSearchedHashValue", strSearchedHashValue);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID, PortalMessages.HASH_VALUE_INVALID);
							}
						}
					}
					
					if (!CommonUtil.isObjectNull(oSearchedGroupName)) {
						String strSearchedGroupName = oSearchedGroupName.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strSearchedGroupName)) {
							if (CommonUtil.getStringBytes(strSearchedGroupName) > PortalConstants.STRING_BYTE_MAX_100) {
								params.put("strSearchedGroupName", strSearchedGroupName);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_TOO_LONG, PortalConstants.ERROR_LOG_PARAM_GROUP_NAME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_GROUP_NAME_TOO_LONG, PortalMessages.GROUP_NAME_TOO_LONG);
							}
						}
					}
					
					String strSearchedScanRegDateLow = PortalConstants.STRING_EMPTY;
					String strSearchedScanRegDateHigh = PortalConstants.STRING_EMPTY;
					
					try {
						DateFormat formatter = new SimpleDateFormat(PortalConstants.YYYYMMDD);
						Date dteRegDateLow = null;
						Calendar dteRegDateHigh = null;
						
						if (!CommonUtil.isObjectNull(oSearchedScanRegDateLow)) {
							strSearchedScanRegDateLow = oSearchedScanRegDateLow.toString();
							
							if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateLow)) {
								if (strSearchedScanRegDateLow.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
									params.put("strSearchedScanRegDateLow", strSearchedScanRegDateLow);
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
									throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID); 
								}
								
								dteRegDateLow = formatter.parse(strSearchedScanRegDateLow);
							}
						}
						
						if (!CommonUtil.isObjectNull(oSearchedScanRegDateHigh)) {
							strSearchedScanRegDateHigh = oSearchedScanRegDateHigh.toString();
							
							if (!CommonUtil.isStringNullOrEmpty(strSearchedScanRegDateHigh)) {
								if (strSearchedScanRegDateHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
									params.put("strSearchedScanRegDateHigh", strSearchedScanRegDateHigh);
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
									throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID); 
								}
								
								dteRegDateHigh = Calendar.getInstance();
								dteRegDateHigh.setTime(formatter.parse(strSearchedScanRegDateHigh));
								dteRegDateHigh.set(Calendar.HOUR_OF_DAY, 23);
								dteRegDateHigh.set(Calendar.MINUTE, 59);
								dteRegDateHigh.set(Calendar.SECOND, 59);
							}
						}
						
						if (!CommonUtil.isObjectNull(dteRegDateLow)
								&& !CommonUtil.isObjectNull(dteRegDateHigh)) {
							if (dteRegDateLow.after(dteRegDateHigh.getTime())) {
								params.put("strSearchedScanRegDateLow", strSearchedScanRegDateLow);
								params.put("strSearchedScanRegDateHigh", strSearchedScanRegDateHigh);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE.toLowerCase()));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID);
							}
						}
					} catch (ParseException e) {
						params.put("strSearchedScanRegDateLow", strSearchedScanRegDateLow);
						params.put("strSearchedScanRegDateHigh", strSearchedScanRegDateHigh);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_REGISTRATION_DATE));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID, PortalMessages.SCAN_REGISTRATION_DATE_INVALID); 
					}
					
					if (!CommonUtil.isObjectNull(oSearchedCxScanId)) {
						String strSearchedCxScanId = oSearchedCxScanId.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strSearchedCxScanId)) {
							try {
								long lSearchedCxScanId = Long.parseLong(strSearchedCxScanId);
								
								if (lSearchedCxScanId < PortalConstants.LONG_ZERO) {
									params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
									throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
								}
							} catch (NumberFormatException e) {
								params.put("strSearchedCxScanId", strSearchedCxScanId);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID, PortalMessages.CX_SCAN_ID_INVALID);
							}
						}
					}
					
					String strScanStartTime = PortalConstants.STRING_EMPTY;
					String strScanEndTime = PortalConstants.STRING_EMPTY;
					DateFormat dateTimeFormatter = new SimpleDateFormat(PortalConstants.DATE_FORMAT_HH_MM);
					Date dteStartTime = null;
					Calendar dteEndTime = null;
						
					if (!CommonUtil.isObjectNull(oSearchedScanStartTime)) {
						strScanStartTime = oSearchedScanStartTime.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strScanStartTime)) {
							if (strScanStartTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
								params.put("strScanStartTime", strScanStartTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID);
							}
							try {
								dteStartTime = dateTimeFormatter.parse(strScanStartTime);
							}catch (ParseException e) {
								params.put("strSearchedScanRegDateLow", strScanStartTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_START_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_TIME_INVALID, PortalMessages.START_TIME_INVALID); 
							}
							
						}
					}
						
					if (!CommonUtil.isObjectNull(oSearchedScanEndTime)) {
						strScanEndTime = oSearchedScanEndTime.toString();
						
						if (!CommonUtil.isStringNullOrEmpty(strScanEndTime)) {
							if (strScanEndTime.equalsIgnoreCase(PortalConstants.INVALID_DATE)) {
								params.put("strScanStartTime", strScanEndTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_END_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
							}
							try {
								dteEndTime = Calendar.getInstance();
								dteEndTime.setTime(dateTimeFormatter.parse(strScanEndTime));
								if (!CommonUtil.isObjectNull(dteStartTime) && !CommonUtil.isObjectNull(dteEndTime)) {
									if (dteStartTime.after(dteEndTime.getTime())) {
										params.put("strScanStartTime", strScanStartTime);
										params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_HIGH_PASSED_LOW, 
												PortalConstants.ERROR_LOG_PARAM_START_TIME, PortalConstants.ERROR_LOG_PARAM_START_TIME.toLowerCase()));
										throw new UBSPortalException(PortalErrors.SEARCH_SCAN_START_AND_END_TIME_INVALID, PortalMessages.START_TIME_AND_END_TIME_INVALID);
									}
								}
							}catch (ParseException e) {
								params.put("strScanEndTime", strScanEndTime);
								params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_END_TIME));
								throw new UBSPortalException(PortalErrors.SEARCH_SCAN_END_TIME_INVALID, PortalMessages.END_TIME_INVALID);
							}
							
						}
					}
				}
				
				scanList = ScanLocalServiceUtil.getEntireScans(iType, searchedScan, start);
				
				if (scanList != null && !scanList.isEmpty()) {
					if (scanList.size() == 0) {
						scanList = null;
					} else {
						List<User> userList = UserLocalServiceUtil.getUsers(PortalConstants.INT_ZERO, PortalConstants.INT_ONE);
						long lCompanyId = PortalConstants.LONG_ZERO;
						
						if (!CommonUtil.isListNullOrEmpty(userList)) {
							for (User user : userList) {
								lCompanyId = user.getCompanyId();
								break;
							}
						}
						
						for (Object item : scanList) {
							ResultItem scan = (ResultItem) item;
							User user = null;
							
							try {
								user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
								
								if (CommonUtil.isObjectNull(user)) {
									scan.setScanManager(PortalConstants.STRING_EMPTY);
								} else {
									scan.setScanManager(user.getFirstName());
								}
							} catch (NoSuchUserException e) {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							}
							
							returnList.add(scan);
						}
					}
				}
				
				rModel = new ResponseModel();
				rModel.setStatus(true);
				rModel.setData(returnList);
				rModel.setMessage(null);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException e) {
			params.put("type", iType);
			params.put("searchedScan", searchedScan);
			
			if (CommonUtil.isPortalError(e.getMessage())) {
				log.debug(e.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS, params, e);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, e);
			}
		} catch (UBSPortalException e) {
			String strErrorCode = e.getErrorCode();
			
			rModel = new ResponseModel();
			rModel.setStatus(false);
			rModel.setData(null);
			rModel.setMessage(e.getErrorMessage());
			
			if (isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, e);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_ENTIRE_SCANS, params, e);
			}
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_ENTIRE_SCANS, null, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(scanList)) {
				scanList = null;
			}
		}
		
		return rModel;
	}
	
	public static List<Report> getReports (long lScanId, int [] reportTypeArr, int type) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Report> reportList = null;
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				reportList = ReportLocalServiceUtil.getReports(lScanId, reportTypeArr, PortalConstants.INT_ZERO, type);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException e) {
			params.put("scanId", lScanId);
			params.put("reportTypeArr", reportTypeArr);
			
			if (CommonUtil.isPortalError(e.getMessage())) {
				log.debug(e.getMessage(), PortalConstants.METHOD_GET_REPORTS, params, e);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_REPORTS, params, e);
			}
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_REPORTS, null, e);
			throw e;
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return reportList;
	}
	
	public static boolean isCxProjectNameValid (String projectName) {
		boolean bIsValid = true;
		
		if (projectName.toLowerCase().startsWith(PortalConstants.STRING_CX_) ||
				projectName.contains(PortalConstants.STRING_ASTERISK) ||
				projectName.contains(PortalConstants.STRING_BACK_SLASH) ||
				projectName.contains(PortalConstants.STRING_PIPE) ||
				projectName.contains(PortalConstants.STRING_DOUBLE_QUOTE) ||
				projectName.contains(PortalConstants.STRING_COLON) ||
				projectName.contains(PortalConstants.STRING_SLASH) ||
				projectName.contains(PortalConstants.STRING_QUESTION_MARK) ||
				projectName.contains(PortalConstants.STRING_LESS_THAN) ||
				projectName.contains(PortalConstants.STRING_GREATER_THAN)) {
			bIsValid = false;
		}
		
		return bIsValid;
	}
	
	public static boolean isVexInputValid (String inputValue) {
		boolean bIsValid = true;
		
		if (inputValue.contains(PortalConstants.STRING_AMPERSAND) || 
				inputValue.contains(PortalConstants.STRING_LESS_THAN) ||
				inputValue.contains(PortalConstants.STRING_GREATER_THAN) ||
				inputValue.contains(PortalConstants.STRING_DOUBLE_QUOTE) ||
				inputValue.contains(PortalConstants.STRING_SINGLE_QUOTE)){
			bIsValid = false;
		}
		
		return bIsValid;
	}

//	public static String getJSPPage(String strAction, int iScreenNo) {
//		String jspPage = PortalConstants.STRING_EMPTY;
//		
//		if (strAction.equals(PortalConstants.PORTLET_METHOD_VIEW_PROJECT_LIST)) {
//			jspPage = PortalConstants.ANDROID_PROJECT_LIST_JSP;
//		} else if (strAction.equals(PortalConstants.PORTLET_METHOD_VIEW_EDIT_PROJECT) ||
//				strAction.equals(PortalConstants.PORTLET_METHOD_ADD_PROJECT) ||
//				strAction.equals(PortalConstants.PORTLET_METHOD_UPDATE_PROJECT)) {
//			jspPage = PortalConstants.ANDROID_PROJECT_REGISTRATION_JSP;
//		} else if (strAction.equals(PortalConstants.PORTLET_METHOD_VIEW_ENTIRE_SCAN_LIST)) {
//			jspPage = PortalConstants.ANDROID_ENTIRE_SCAN_LIST_JSP;
//		} else if (strAction.equals(PortalConstants.PORTLET_METHOD_SEARCH_PROJECTS)) {
//			jspPage = PortalConstants.ANDROID_PROJECT_LIST_JSP;
//		} else if (strAction.equals(PortalConstants.PORTLET_METHOD_VIEW_SCAN_LIST)) {
//			jspPage = PortalConstants.ANDROID_SCAN_LIST_JSP;
//		} else if (strAction.equals(PortalConstants.PORTLET_METHOD_DELETE_PROJECT)) {
//			jspPage = PortalConstants.ANDROID_PROJECT_LIST_JSP;
//		} else if (strAction.equals(PortalConstants.PORTLET_METHOD_SEARCH_SCANS) ||
//				strAction.equals(PortalConstants.PORTLET_METHOD_STOP_SCAN) ||
//				strAction.equals(PortalConstants.PORTLET_METHOD_REEXECUTE_SCAN) ||
//				strAction.equals(PortalConstants.PORTLET_METHOD_DELETE_SCAN)) {
//			if (iScreenNo == PortalConstants.SCREEN_ENTIRE_SCAN_LIST) {
//				jspPage = PortalConstants.ANDROID_ENTIRE_SCAN_LIST_JSP;
//			} else {
//				jspPage = PortalConstants.ANDROID_SCAN_LIST_JSP;
//			}
//		} else if (strAction.equals(PortalConstants.PORTLET_METHOD_VIEW_SCAN_REGISTRATION)) {
//			jspPage = PortalConstants.ANDROID_SCAN_REGISTRATION_JSP;
//		} else if (strAction.equals(PortalConstants.PORTLET_METHOD_COMPLETE_PROJECT)) {
//			jspPage = PortalConstants.ANDROID_SCAN_LIST_JSP;
//		}
//		
//		return jspPage;
//	}
	
	public static boolean canAccessScanList (String emailAddress, long lProjectId) {
		boolean bCanAccess = false;
		List<ProjectUsers> projectUsersList = null;
		
		try {
			if (!CommonUtil.isStringNullOrEmpty(emailAddress) && lProjectId != 0) {
				projectUsersList = ProjectUsersLocalServiceUtil.getProjectUsers(lProjectId);
				
				if (projectUsersList != null && !projectUsersList.isEmpty()) {
					for (ProjectUsers pu : projectUsersList) {
						if (pu.getUserId().equalsIgnoreCase(emailAddress)) {
							bCanAccess = true;
							break;
						}
					}
				}
			}
		} catch (PortalException e) {
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_REPORTS, null, e);
		}
		
		return bCanAccess;
	}

	public static Scan getScan(long scanId) {
		Scan scan = null;
		
		try {
			scan = ScanLocalServiceUtil.getScan(scanId);
		} catch (PortalException e) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_SCAN, null, e);
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SCAN, null, e);
		}
		
		return scan;
	}

	public static User getUserByEmailAddress(long lCompanyId, String scanManager) {
		Map<String, Object> params = new HashMap<String, Object>();
		User user = null;
		
		try {
			user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scanManager);
		} catch (NoSuchUserException e) {
			params.put("user", user);
			params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_DOES_NOT_EXIST, PortalConstants.ERROR_LOG_PARAM_USER));
			log.debug(PortalErrors.GET_USER_USER_DOES_NOT_EXIST, PortalConstants.METHOD_GET_USER_BY_EMAIL_ADDRESS, params, e);
		} catch (PortalException e) {
			params.put("lCompanyId", lCompanyId);
			params.put("scanManager", scanManager);
			log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_USER_BY_EMAIL_ADDRESS, params, e);
		} catch (SystemException e) {
			params.put("lCompanyId", lCompanyId);
			params.put("scanManager", scanManager);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_USER_BY_EMAIL_ADDRESS, params, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return user;
	}
	
	public static long getCompanyId () {
		long lCompanyId = PortalConstants.LONG_ZERO;
		
		try {
			List<User> userList = UserLocalServiceUtil.getUsers(PortalConstants.INT_ZERO, PortalConstants.INT_ONE);
			
			if (!CommonUtil.isListNullOrEmpty(userList)) {
				for (User user : userList) {
					lCompanyId = user.getCompanyId();
					break;
				}
			}
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_COMPANY_ID, null, e);
		}
		
		return lCompanyId;
	}
	
	public static int getProjectsCount (long lUserId, int type) {
		Map<String, Object> params = new HashMap<String, Object>();
		int iCount = PortalConstants.INT_ZERO;
		User user = null;
		boolean bIsOverallAdmin = false;
		
		try {
			user = ControllerHelper.getUser(lUserId);
			
			if (!CommonUtil.isObjectNull(user)) {
				List<Role> roleList = user.getRoles();
				
				if (!CommonUtil.isListNullOrEmpty(roleList)) {
					for (Role role : roleList) {
						if (role.getName().equals(PortalConstants.OVERALL_ADMINISTRATOR)) {
							bIsOverallAdmin = true;
							break;
						}
					}
				}
				
				if (bIsOverallAdmin) {
					iCount = ProjectLocalServiceUtil.getProjectsCount(type);
				} else {
					iCount = ProjectLocalServiceUtil.getUserProjectsCount(user.getEmailAddress(), type);
				}
			}
		} catch (UBSPortalException e) {
			params.put("lUserId", lUserId);
			params.put("type", type);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_PROJECTS_COUNT, params, e);
		} catch (SystemException e) {
			params.put("lUserId", lUserId);
			params.put("type", type);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_PROJECTS_COUNT, params, e);
		} catch (PortalException e) {
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_PROJECTS_COUNT, params, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return iCount;
	}
	
	public static int getSearchedProjectsCount (int type, Map<String, Object> searchedProject, long lUserId) {
		Map<String, Object> params = new HashMap<String, Object>();
		int iCount = PortalConstants.INT_ZERO;
		User user = null;
		boolean bIsOverallAdmin = false;
		
		try {
			user = ControllerHelper.getUser(lUserId);
			
			if (!CommonUtil.isObjectNull(user)) {
				List<Role> roleList = user.getRoles();
				
				if (!CommonUtil.isListNullOrEmpty(roleList)) {
					for (Role role : roleList) {
						if (role.getName().equals(PortalConstants.OVERALL_ADMINISTRATOR)) {
							bIsOverallAdmin = true;
							break;
						}
					}
				}
				
				if (bIsOverallAdmin) {
					iCount = ProjectLocalServiceUtil.getSearchedProjectsCount(type, searchedProject, user);
				} else {
					iCount = ProjectLocalServiceUtil.getSearchedUserProjectsCount(user.getEmailAddress(), type, searchedProject);
				}
			}
		} catch (UBSPortalException e) {
			params.put("type", type);
			params.put("searchedProject", searchedProject);
			params.put("lUserId", lUserId);
			log.debug(e.getErrorCode(), PortalConstants.METHOD_GET_SEARCHED_PROJECTS_COUNT, params, e);
		} catch (SystemException e) {
			params.put("type", type);
			params.put("searchedProject", searchedProject);
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_SEARCHED_PROJECTS_COUNT, params, e);
		} catch (PortalException e) {
			params.put("type", type);
			params.put("searchedProject", searchedProject);
			params.put("lUserId", lUserId);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_SEARCHED_PROJECTS_COUNT, params, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return iCount;
	}
	
	public static int getEntireScansCount (int type, Map<String, Object> searchedScan) {
		Map<String, Object> params = new HashMap<String, Object>();
		int iCount = PortalConstants.INT_ZERO;
		
		try {
			iCount = ScanLocalServiceUtil.getEntireScansCount(type, searchedScan);
		} catch (PortalException e) {
			params.put("type", type);
			params.put("searchedScan", searchedScan);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_ENTIRE_SCANS_COUNT, params, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return iCount;
	}
	
	public static int getScansCount (long lProjectId, Map<String, Object> searchedScan) {
		Map<String, Object> params = new HashMap<String, Object>();
		int iCount = PortalConstants.INT_ZERO;
		
		try {
			iCount = ScanLocalServiceUtil.getScansCount(lProjectId, searchedScan);
		} catch (PortalException e) {
			params.put("lProjectId", lProjectId);
			params.put("searchedScan", searchedScan);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_SCANS_COUNT, params, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return iCount;
	}
	
	public static List<Object> sortProjects (long lUserId, int iUserRole, Map<String, Object> searchedProject, String orderByCol, String orderByType, int iType, int start, int end) {
		List<Object> projectList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				projectList = ProjectLocalServiceUtil.sortOverallProjects(lUserId, iUserRole, searchedProject, orderByType, orderByCol, iType, start, end);
			}
		} catch (PortalException e) {
			params.put("projectList", projectList);
			log.debug(PortalErrors.PORTAL_EXCEPTION, "sortProjects", params, e);
		} catch (UBSPortalException e) {
			log.debug(PortalErrors.PORTAL_EXCEPTION, "sortProjects", params, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return projectList;
	}
	
	public static List<Object> sortUserProjects (long lUserId, Map<String, Object> searchedProject, String orderByCol, String orderByType, int iType, int start, int end) {
		List<Object> projectList = null;
		String strUserId = PortalConstants.STRING_EMPTY;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (lUserId != PortalConstants.LONG_ZERO) {
					User user = UserLocalServiceUtil.getUser(lUserId);
					
					if (!CommonUtil.isObjectNull(user)) {
						strUserId = user.getEmailAddress();
					}
				}
				projectList = ProjectLocalServiceUtil.sortUserProjects(strUserId, searchedProject, orderByType, orderByCol, iType, start, end);
			}
		} catch (PortalException e) {
			params.put("lUserId", lUserId);
			params.put("projectList", projectList);
			log.debug(PortalErrors.PORTAL_EXCEPTION, "sortUserProjects", params, e);
		} catch (SystemException e) {
			params.put("lUserId", lUserId);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, "sortUserProjects", params, e);
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), "sortUserProjects", params, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return projectList;
	}
	
	public static List<Object> sortEntireScans (Map<String, Object> searchedScan, String orderByCol, String orderByType, int iType, int start) {
		List<Object> scanList = null;
		List<Object> returnList = new ArrayList<Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				scanList = ScanLocalServiceUtil.sortEntireScans(iType, searchedScan, orderByCol, orderByType, start);
				
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
					List<User> userList = UserLocalServiceUtil.getUsers(PortalConstants.INT_ZERO, PortalConstants.INT_ONE);
					long lCompanyId = PortalConstants.LONG_ZERO;
					
					if (!CommonUtil.isListNullOrEmpty(userList)) {
						for (User user : userList) {
							lCompanyId = user.getCompanyId();
							break;
						}
					}
					
					for (Object item : scanList) {
						ResultItem scan = (ResultItem) item;
						User user = null;
						
						try {
							user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
							
							if (CommonUtil.isObjectNull(user)) {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							} else {
								scan.setScanManager(user.getFirstName());
							}
						} catch (NoSuchUserException e) {
							scan.setScanManager(PortalConstants.STRING_EMPTY);
						}
						
						returnList.add(scan);
					}
				}
			}
		} catch (PortalException e) {
			params.put("searchedScan", searchedScan);
			params.put("scanList", scanList);
			log.debug(PortalErrors.PORTAL_EXCEPTION, "sortEntireScans", params, e);
		} catch (SystemException e) {
			params.put("searchedScan", searchedScan);
			params.put("scanList", scanList);
			log.debug(PortalErrors.SYSTEM_EXCEPTION, "sortEntireScans", params, e);
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), "sortEntireScans", params, e);
		} finally {
			params.clear();
			params = null;
			scanList = null;
		}
		
		return returnList;
	}
	
	public static List<Object> sortScans (long lProjectId, User user, Map<String, Object> searchedScan, String orderByCol, String orderByType, int iType, int start) {
		List<Object> scanList = null;
		List<Object> returnList = new ArrayList<Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (PortletCommonUtil.isDBConnected()) {
				scanList = ScanLocalServiceUtil.sortScans(lProjectId, searchedScan, orderByCol, orderByType, start);
				
				long lCompanyId = user.getCompanyId();
				
				if (!CommonUtil.isListNullOrEmpty(scanList)) {
					for (Object item : scanList) {
						ScanItem scan = (ScanItem) item;
						try {
							user = UserLocalServiceUtil.getUserByEmailAddress(lCompanyId, scan.getScanManager());
							
							if (!CommonUtil.isObjectNull(user)) {
								scan.setScanManager(user.getFirstName());
							} else {
								scan.setScanManager(PortalConstants.STRING_EMPTY);
							}
						} catch (NoSuchUserException e) {
							scan.setScanManager(PortalConstants.STRING_EMPTY);
						} catch (SystemException e) {
							scan.setScanManager(PortalConstants.STRING_EMPTY);
						}
						
						returnList.add(scan);
					}
				}
			}
		} catch (PortalException e) {
			params.put("scanList", scanList);
			params.put("user", user);
			params.put("searchedScan", searchedScan);
			log.debug(PortalErrors.PORTAL_EXCEPTION, "sortScans", params, e);
		} catch (UBSPortalException e) {
			log.debug(e.getErrorCode(), "sortScans", params, e);
		} finally {
			params.clear();
			params = null;
			scanList = null;
		}
		
		return returnList;
	}
	
	public static boolean isUserInputError (String errorCode) {
		boolean bIsUserInputError = false;
		
		if (errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_INVALID)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_CASE_NAME)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_CASE_NAME)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_TARGET_URL)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_TARGET_URL)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PRODUCTION_ENVIRONMENT_URL)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_PRODUCTION_ENVIRONMENT_URL)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_TARGET_URL_INVALID)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_TARGET_URL_INVALID)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_PRODUCTION_ENVIRONMENT_URL_INVALID)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_INVALID)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_TOO_LONG)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_TOO_LONG)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_CASE_NUMBER_ALREADY_EXISTS)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_CASE_NUMBER_ALREADY_EXISTS)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_NAME)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_NAME)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_NAME_INVALID)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_NAME_INVALID)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_PRESET_ID)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PRESET_ID)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_PRESET_DOES_NOT_EXIST)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_PRESET_DOES_NOT_EXIST)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_OWNER_GROUP)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_OWNER_GROUP)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_OWNER_GROUP_DOES_NOT_EXIST)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_OWNER_GROUP_DOES_NOT_EXIST)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_ATTRIBUTE)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_ATTRIBUTE)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_END_DATE)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_END_DATE)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_PROJECT_END_DATE_INVALID)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_PROJECT_END_DATE_INVALID)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_USER_DOES_NOT_EXIST)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_EXIST)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_USER_DOES_NOT_BELONG_TO_GROUP)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_PROJECT_USERS)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_PROJECT_USERS)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_PACKAGE_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_PACKAGE_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_PACKAGE_NAME_INVALID)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_PACKAGE_NAME_INVALID)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_FILE_SIZE_TOO_BIG_20)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_FILE_EMPTY)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_FILE_TYPE_INVALID)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_WEB_INSPECTION_SIGNATURE_SET_GROUP)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_WEB_INSPECTION_SIGNATURE_SET_GROUP)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_SERVER_FILE_SIGNATURE_SET_GROUP)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_SERVER_FILE_SIGNATURE_SET_GROUP)
				|| errorCode.equals(PortalErrors.ADD_PROJECT_NO_SERVER_SETTING_SIGNATURE_SET_GROUP)
				|| errorCode.equals(PortalErrors.UPDATE_PROJECT_NO_SERVER_SETTING_SIGNATURE_SET_GROUP)
				
				|| errorCode.equals(PortalErrors.SEARCH_PROJECT_PROJECT_ID_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_PROJECT_GROUP_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_PROJECT_CASE_NUMBER_TOO_LONG)
				|| errorCode.equals(PortalErrors.SEARCH_PROJECT_PROJECT_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.SEARCH_PROJECT_PROJECT_NAME_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_PROJECT_PROJECT_END_DATE_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_PROJECT_SCAN_COUNT_INVALID)
				
				|| errorCode.equals(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG)
				|| errorCode.equals(PortalErrors.ADD_SCAN_NO_FILE)
				|| errorCode.equals(PortalErrors.ADD_SCAN_FILE_NOT_ZIP)
				|| errorCode.equals(PortalErrors.ADD_SCAN_FILE_EMPTY)
				|| errorCode.equals(PortalErrors.ADD_SCAN_NO_PROCESS)
				|| errorCode.equals(PortalErrors.UPDATE_SCAN_NO_PROCESS)
				|| errorCode.equals(PortalErrors.ADD_SCAN_FILE_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.ADD_SCAN_FILE_NOT_APK)
				|| errorCode.equals(PortalErrors.ADD_SCAN_FILE_SIZE_TOO_BIG_50)
				|| errorCode.equals(PortalErrors.ADD_VEX_SCAN_FAILED)

				|| errorCode.equals(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_SCAN_HASH_VALUE_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_SCAN_REGISTRATION_DATE_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_SCAN_CX_SCAN_ID_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_SCAN_GROUP_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.SEARCH_SCAN_PROJECT_NAME_TOO_LONG)
				|| errorCode.equals(PortalErrors.SEARCH_SCAN_PROJECT_NAME_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_SCAN_START_TIME_INVALID)
				|| errorCode.equals(PortalErrors.SEARCH_SCAN_END_TIME_INVALID)
				
				|| errorCode.equals(PortalErrors.ADD_VEX_SCAN_FAILED)
				
				|| errorCode.equals(PortalErrors.INVALID_FILE)) {
			bIsUserInputError = true;
		}
		
		return bIsUserInputError;
	}
	
	public static boolean folderExist (String location, String folderName) {
		File folderLocation = null;
		String [] folderList = null;
		boolean bExist = false;
		
		folderLocation = new File(location);
		folderList = folderLocation.list();
		
		if (!CommonUtil.isObjectNull(folderList)) {
			for (String folder : folderList) {
				if (folder.equalsIgnoreCase(folderName)) {
					bExist = true;
					break;
				}
			}
		}
		
		return bExist;
	}
	
	public static void createProjectListCsv (List<Object> projectList, File projectListFile) throws UBSPortalException {
		CSVStrategy strategy = new CSVStrategy(',', '"', '\0', false, false);
		List<ProjectListEntry> projectListEntryList = new ArrayList<ProjectListEntry>();
		ProjectListEntry entry = null;
		ProjectItem item = null;
		int iStatus = PortalConstants.INT_ZERO;
		String strStatus = PortalConstants.STRING_EMPTY;
		BufferedWriter fileWriter = null;
		DateFormat format = new SimpleDateFormat(PortalConstants.YYYYMMDD);
		
		try {
			entry = new ProjectListEntry();
			entry.setProjectId(PortalConstants.HEADER_PROJECT_ID);
			entry.setGroupName(PortalConstants.HEADER_PROJECT_GROUP_NAME);
			entry.setCaseNumber(PortalConstants.HEADER_CASE_NUMBER);
			entry.setProjectName(PortalConstants.HEADER_PROJECT_NAME);
			entry.setProjectEndDate(PortalConstants.HEADER_PROJECT_END_DATE);
			entry.setStatus(PortalConstants.HEADER_PROJECT_STATUS);
			entry.setScanCount(PortalConstants.HEADER_SCAN_COUNT);
			projectListEntryList.add(entry);
			
			if (!CommonUtil.isListNullOrEmpty(projectList)) {
				for (Object obj : projectList) {
					entry = new ProjectListEntry();
					item = new ProjectItem();
					item = (ProjectItem) obj;
					entry.setProjectId(String.valueOf(item.getProjectId()));
					entry.setGroupName(item.getOwnerGroupName());
					entry.setCaseNumber(item.getCaseNumber());
					entry.setProjectName(item.getProjectName());
					entry.setProjectEndDate(format.format(item.getProjectEndDate()));
					iStatus = item.getStatus();
					
					if (iStatus == ProjectStatus.COMPLETE.getInteger()) {
						strStatus = ProjectStatus.COMPLETE.getString();
					} else if (iStatus == ProjectStatus.NOT_YET_COMPLETE.getInteger()) {
						strStatus = ProjectStatus.NOT_YET_COMPLETE.getString();
					} else {
						strStatus = PortalConstants.STRING_EMPTY;
					}
					
					entry.setStatus(strStatus);
					entry.setScanCount(String.valueOf(item.getScanCount()));
					projectListEntryList.add(entry);
				}
			}
			
			fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(projectListFile),PortalConstants.SHIFT_JIS));
			CSVWriter<ProjectListEntry> writer = new CSVWriterBuilder<ProjectListEntry>(fileWriter).strategy(strategy).entryConverter(new ProjectListEntryParser()).build();			
			writer.writeAll(projectListEntryList);
			writer.flush();
			
			fileWriter.close();
			writer.close();
		} catch (IOException e) {
			log.debug(PortalErrors.IO_EXCEPTION, "createProjectListCsv", null, e);
			throw new UBSPortalException(PortalErrors.IO_EXCEPTION, PortalMessages.FILE_INVALID, e);
		}
	}
	
	public static boolean isZIPValid (String strContentType, File file) {
		boolean bIsValid = false;
		
		if (!CommonUtil.isStringNullOrEmpty(strContentType)) {
			strContentType = Normalizer.normalize(strContentType, Normalizer.Form.NFKD);
			
			if (strContentType.equalsIgnoreCase(PortalConstants.CONTENT_TYPE_ZIP) || strContentType.equalsIgnoreCase(PortalConstants.CONTENT_TYPE_GENERAL_ZIP)) {
				if (FileUtil.getExtension(file.getAbsolutePath()).equalsIgnoreCase(PortalConstants.ZIP_FILE_EXTENSION)) {
					bIsValid = true;
				}
			}
		}
		
		return bIsValid;
	}
	
	public static boolean isPresetValid (String sessionId, long presetId) {
		List<Long> presetList = null;
		boolean bIsPresetValid = false;
		
		presetList = CxScanManager.getPresetList(sessionId);
		
		if (!CommonUtil.isListNullOrEmpty(presetList)) {
			for (long preset : presetList) {
				if (presetId == preset) {
					bIsPresetValid = true;
					break;
				}
			}
		}
		
		return bIsPresetValid;
	}
	
	public static HttpSession getHttpSession (PortletRequest portletRequest) {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(portletRequest);
		HttpServletRequest origRequest = PortalUtil.getOriginalServletRequest(request);
		return origRequest.getSession(false);
	}

	public static int getDetectionResultsCount (long lscanId, Map<String, Object> searchedDetectionResultReview) {
		Map<String, Object> params = new HashMap<String, Object>();
		int iCount = PortalConstants.INT_ZERO;
		
		try {
			iCount =  VexDetectionResultLocalServiceUtil.getDetectionResultsCount(lscanId, searchedDetectionResultReview);
		} catch (PortalException e) {
			params.put("lscanId", lscanId);
			params.put("searchedDetectionResultReview", searchedDetectionResultReview);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_COUNT, params, e);
		} catch (Exception e){
			params.put("lscanId", lscanId);
			params.put("searchedDetectionResultReview", searchedDetectionResultReview);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULT_COUNT, params, e);
		}finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return iCount;
	}
	
	public static ResponseModel getDetectionResults (long lSearchedScanId, User user, Map<String, Object> searchedDetectionResultReview, int start, String orderByCol, String orderByType) {
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> detectionResultList = null;
		List<VexDetectionResultItem> returnList = new ArrayList<VexDetectionResultItem>();
		ResponseModel rModel = null;
		try {
			if (PortletCommonUtil.isDBConnected()) {
				if (!CommonUtil.isMapNullOrEmpty(searchedDetectionResultReview)) {
					try {
						if (lSearchedScanId < PortalConstants.LONG_ZERO) {
							params.put("strSearchedScanId", lSearchedScanId);
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID); 
						}
					} catch (NumberFormatException e) {
						params.put("strSearchedScanId", lSearchedScanId);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
					
				detectionResultList = VexDetectionResultLocalServiceUtil.getVexDetectionResultReviews(lSearchedScanId, user, searchedDetectionResultReview, start, orderByCol, orderByType);
				
				if (!CommonUtil.isListNullOrEmpty(detectionResultList)) {
					if (detectionResultList.size() == 0) {
						detectionResultList = null;
					} else {
						for (Object item : detectionResultList) {
							VexDetectionResultItem resultItem = (VexDetectionResultItem) item;
							returnList.add(resultItem);
						}
					}
				}
				
				rModel = new ResponseModel();
				rModel.setStatus(true);
				rModel.setData(returnList);
				rModel.setMessage(null);
			} else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		}
		}catch (PortalException e) {
			params.put("lSearchedScanId", lSearchedScanId);
			params.put("user", user);
			params.put("searchedDetectionResultReview", searchedDetectionResultReview);
			if (CommonUtil.isPortalError(e.getMessage())) {
				log.debug(e.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_RESULTS, params, e);
			} else {
				log.debug(PortalErrors.PORTAL_EXCEPTION, PortalConstants.METHOD_GET_VEX_DETECTION_RESULTS, params, e);
			}
		} catch (UBSPortalException e) {
			String strErrorCode = e.getErrorCode();
			
			rModel = new ResponseModel();
			rModel.setStatus(false);
			rModel.setData(null);
			rModel.setMessage(e.getErrorMessage());
			
			if (isUserInputError(strErrorCode)) {
				log.warn(strErrorCode, PortalConstants.METHOD_GET_VEX_DETECTION_RESULTS, params, e);
			} else {
				log.debug(strErrorCode, PortalConstants.METHOD_GET_VEX_DETECTION_RESULTS, params, e);
			}
		} catch (SystemException e) {
			log.debug(PortalErrors.SYSTEM_EXCEPTION, PortalConstants.METHOD_GET_VEX_DETECTION_RESULTS, null, e);
		} finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
			
			if (!CommonUtil.isListNullOrEmpty(detectionResultList)) {
				detectionResultList = null;
			}
		}
		
		return rModel;
	}
	
	public static List<Object> sortDetectionResults(long lSearchedScanId, User user, Map<String, Object> searchedDetectionResultReview, int start, String orderByCol, String orderByType){
		Map<String, Object> params = new HashMap<String, Object>();
		List<Object> detectionResultList = null;
		try {
			if (PortletCommonUtil.isDBConnected() && !CommonUtil.isMapNullOrEmpty(searchedDetectionResultReview)) {
				if (!CommonUtil.isMapNullOrEmpty(searchedDetectionResultReview)) {
					try {
						if (lSearchedScanId < PortalConstants.LONG_ZERO) {
							params.put("strSearchedScanId", lSearchedScanId);
							params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
							throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID); 
						}
					} catch (NumberFormatException e) {
						params.put("strSearchedScanId", lSearchedScanId);
						params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
						throw new UBSPortalException(PortalErrors.SEARCH_SCAN_SCAN_ID_INVALID, PortalMessages.SCAN_ID_INVALID);
					}
					detectionResultList = VexDetectionResultLocalServiceUtil.getVexDetectionResultReviews(lSearchedScanId, user, searchedDetectionResultReview, start, orderByCol, orderByType);
				}
			}
			else {
				throw new UBSPortalException(PortalErrors.ORM_EXCEPTION, PortalMessages.ORM_EXCEPTION);
			}
		} catch (PortalException e) {
			params.put("lscanId", lSearchedScanId);
			params.put("searchedDetectionResultReview", searchedDetectionResultReview);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_SORT_RESULTS, params, e);
		} catch (Exception e){
			params.put("lscanId", lSearchedScanId);
			params.put("searchedDetectionResultReview", searchedDetectionResultReview);
			log.debug(e.getMessage(), PortalConstants.METHOD_GET_VEX_DETECTION_SORT_RESULTS, params, e);
		}finally {
			if (!CommonUtil.isMapNullOrEmpty(params)) {
				params.clear();
				params = null;
			}
		}
		
		return detectionResultList;
	}
	
}
