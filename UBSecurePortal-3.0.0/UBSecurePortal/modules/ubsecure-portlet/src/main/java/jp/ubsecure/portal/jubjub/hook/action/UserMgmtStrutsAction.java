package jp.ubsecure.portal.jubjub.hook.action;

import java.io.PrintWriter;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.EmailAddressException;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.UserScreenNameException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PwdGenerator;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.Encrypter;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_JAVAX_PORTLET_NAME + PortalConstants.PORTLET_NAME_USERS_ADMIN,
		PortalConstants.KEY_MVC_COMMAND_NAME + PortalConstants.COMMAND_NAME_EDIT_USER,
		PortalConstants.KEY_SERVICE_RANKING + PortalConstants.SERVICE_RANKING_100
	},
	service = MVCActionCommand.class
)
public class UserMgmtStrutsAction extends BaseMVCActionCommand {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(UserMgmtStrutsAction.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		final String strCmd = ParamUtil.getString(actionRequest, Constants.CMD);
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		HttpSession httpSession = ControllerHelper.getHttpSession(actionRequest);
		long lUserId = Long.parseLong(httpSession.getAttribute(PortalConstants.USER_ID).toString());
		SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
		PortletSession portletSession = actionRequest.getPortletSession(false);
		
		try {
			if(strCmd != null && strCmd != PortalConstants.STRING_EMPTY){
				if (strCmd.equalsIgnoreCase(Constants.ADD)
						|| strCmd.equalsIgnoreCase(Constants.UPDATE)) {

					StrutsActionManager.addEditUser(actionRequest, strCmd);
					if (strCmd.equals(Constants.ADD)) {
						log.info(PortalMessages.USER_EVENT_ADD_USER, lUserId);
						SessionMessages.add(actionRequest, PortalConstants.SUCCESS_ADD);
					} else {
						log.info(PortalMessages.USER_EVENT_EDIT_USER, lUserId);
						SessionMessages.add(actionRequest, PortalConstants.SUCCESS_EDIT);
					}

				} else if (strCmd.equalsIgnoreCase(Constants.DELETE)) {
						log.info(PortalMessages.USER_EVENT_DELETE_USER, lUserId);
					StrutsActionManager.deleteUser(actionRequest);
					SessionMessages.add(actionRequest, PortalConstants.SUCCESS_DELETE);

				} else if (strCmd.equalsIgnoreCase(Constants.SEARCH)) {
						log.info(PortalMessages.USER_EVENT_SEARCH_USER, lUserId);
					List<User> userLists = StrutsActionManager.searchUser(actionRequest);
					portletSession.setAttribute(PortalConstants.USERLISTS, userLists, PortletSession.PORTLET_SCOPE);
					
				} else if (strCmd.equalsIgnoreCase(PortalConstants.CLEAR_SEARCH)) {
					log.info(PortalMessages.USER_EVENT_CLEAR_SEARCH_USER, lUserId);
					List<User> userLists = StrutsActionManager.clearSearchUser(actionRequest);
					
					portletSession.setAttribute(PortalConstants.USERLISTS, userLists, PortletSession.PORTLET_SCOPE);
					
					httpSession.removeAttribute("searchedUser");
					
				}
				

				if (!strCmd.equals(Constants.SEARCH)) {
					String redirect = PortalConstants.STRING_EMPTY;
					
					if (strCmd.equals(Constants.ADD) || strCmd.equals(Constants.UPDATE)) {
						redirect = ParamUtil.getString(actionRequest, PortalConstants.BACK_URL);
					}

					Key key = (Key) httpSession.getAttribute("key");
					boolean bIsEncrypted = false;
					
					try {
						bIsEncrypted = Encrypter.isEncrypted(redirect, key);
					} catch (Exception e) {
						if (e instanceof IllegalBlockSizeException ||
								e instanceof BadPaddingException) {
							bIsEncrypted = false;
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
						httpSession.removeAttribute("key");
						
						redirect = Encrypter.decrypt(redirect, key);
					}
					
					String [] redirectContent = redirect.split(PortalConstants.DELIMITER_125);
					
					for (int i = 0; i < redirectContent.length; i++) {
						if (!redirectContent[i].contains(PortalConstants.USERS_SEARCH_CONTAINER_PK) &&
								!redirectContent[i].contains("group_name") &&
								!redirectContent[i].contains("user_name") &&
								!redirectContent[i].contains("expirationDateLow") &&
								!redirectContent[i].contains("user_role") &&
								!redirectContent[i].contains("user_id") &&
								!redirectContent[i].contains("account_lock") &&
								!redirectContent[i].contains("expirationDateHigh")) {
							if (i == 0) {
								redirect = redirectContent[i];
							} else {
								redirect += PortalConstants.DELIMITER_125 + redirectContent[i];
							}
						}
					}
					
					sendRedirect(actionRequest, actionResponse, redirect);
				}
			}
			else{
				HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
				String strGenerator =  ParamUtil.getString(originalRequest, PortalConstants.PID);
	
				if(strGenerator != null && strGenerator != PortalConstants.STRING_EMPTY){
					HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
					response.setContentType(PortalConstants.APPLICATION_JASON);
					PrintWriter writer = response.getWriter(); 
					JSONObject objJsonObject = JSONFactoryUtil.createJSONObject();
		                      objJsonObject.put(PortalConstants.UBSP,PwdGenerator.getPassword());
		                    
		            writer.print(objJsonObject);
		            response.flushBuffer();
				}
			}

		} catch (Exception e) {
			httpSession.removeAttribute("searchedUser");
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			
			List<User> userLists = new ArrayList<User>();
			
			long userId = ParamUtil.getLong(actionRequest, "p_u_i_d");

			if (e instanceof UBSPortalException) {
				if (((UBSPortalException) e).getErrorCode().equals(PortalErrors.ORM_EXCEPTION)) {
					if (strCmd.equals(Constants.SEARCH)) {
						portletSession.setAttribute(PortalConstants.USERLISTS, userLists, PortletSession.PORTLET_SCOPE);
						SessionErrors.add(actionRequest, PortalConstants.ORM_EXCEPTION);
					}
						
					if (strCmd.equals(Constants.ADD)) {
						String redirect = ParamUtil.getString(actionRequest, PortalConstants.BACK_URL);
						
						Key key = (Key) httpSession.getAttribute("key");
						boolean bIsEncrypted = false;
						
						try {
							bIsEncrypted = Encrypter.isEncrypted(redirect, key);
						} catch (Exception e1) {
							if (e1 instanceof IllegalBlockSizeException ||
									e1 instanceof BadPaddingException) {
								bIsEncrypted = false;
							}
						}
						
						if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
							httpSession.removeAttribute("key");
							
							redirect = Encrypter.decrypt(redirect, key);
						}
						
						String [] redirectContent = redirect.split(PortalConstants.DELIMITER_125);
						
						for (int i = 0; i < redirectContent.length; i++) {
							if (!redirectContent[i].contains(PortalConstants.USERS_SEARCH_CONTAINER_PK) &&
									!redirectContent[i].contains("group_name") &&
									!redirectContent[i].contains("user_name") &&
									!redirectContent[i].contains("expirationDateLow") &&
									!redirectContent[i].contains("user_role") &&
									!redirectContent[i].contains("user_id") &&
									!redirectContent[i].contains("account_lock") &&
									!redirectContent[i].contains("expirationDateHigh")) {
								if (i == 0) {
									redirect = redirectContent[i];
								} else {
									redirect += PortalConstants.DELIMITER_125 + redirectContent[i];
								}
							}
						}
						
						redirect = HttpUtil.addParameter(redirect, actionResponse.getNamespace() + "p_u_i_d", userId);
						sendRedirect(actionRequest, actionResponse, redirect);
					}
				} else {
					SessionErrors.add(actionRequest, ((UBSPortalException) e).getErrorMessage());
					
					String redirect = ParamUtil.getString(actionRequest, PortalConstants.REDIRECT);
					
					Key key = (Key) httpSession.getAttribute("key");
					boolean bIsEncrypted = false;
					
					try {
						bIsEncrypted = Encrypter.isEncrypted(redirect, key);
					} catch (Exception e1) {
						if (e1 instanceof IllegalBlockSizeException ||
								e1 instanceof BadPaddingException) {
							bIsEncrypted = false;
						}
					}
					
					if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
						httpSession.removeAttribute("key");
						
						redirect = Encrypter.decrypt(redirect, key);
					}
					
					String [] redirectContent = redirect.split(PortalConstants.DELIMITER_125);
					
					for (int i = 0; i < redirectContent.length; i++) {
						if (!redirectContent[i].contains(PortalConstants.USERS_SEARCH_CONTAINER_PK) &&
								!redirectContent[i].contains("group_name") &&
								!redirectContent[i].contains("user_name") &&
								!redirectContent[i].contains("expirationDateLow") &&
								!redirectContent[i].contains("user_role") &&
								!redirectContent[i].contains("user_id") &&
								!redirectContent[i].contains("account_lock") &&
								!redirectContent[i].contains("expirationDateHigh")) {
							if (i == 0) {
								redirect = redirectContent[i];
							} else {
								redirect += PortalConstants.DELIMITER_125 + redirectContent[i];
							}
						}
					}
					
					redirect = HttpUtil.addParameter(redirect, actionResponse.getNamespace() + "p_u_i_d", userId);
					sendRedirect(actionRequest, actionResponse, redirect);
				}
			} else if (e instanceof ParseException) {
				String message = e.getMessage();
				if (message == null) {
					SessionErrors.add(actionRequest, e.getClass());
				} else {
					if (message.equals(PortalErrors.SEARCH_USER_INVALID_LAST_LOGIN_DATE)) {
						SessionErrors.add(actionRequest, PortalMessages.LAST_LOGIN_DATE_INVALID);
					} else if (message.equals(PortalErrors.SEARCH_USER_INVALID_DATE)) {
						SessionErrors.add(actionRequest, PortalMessages.EXPIRY_DATE_INVALID);
					}
				}
			} else {
				SessionErrors.add(actionRequest, e.getClass());
			}
			
			if ((((e instanceof EmailAddressException) ||
					(e instanceof UserScreenNameException) || (e instanceof ParseException) || (e instanceof UBSPortalException))
					&& strCmd.equals(Constants.SEARCH)) || (e instanceof NoSuchUserException) || (strCmd.equals(Constants.DELETE) && (e instanceof UBSPortalException))) {
				
				String redirect = ParamUtil.getString(actionRequest, PortalConstants.REDIRECT);
				portletSession.setAttribute(PortalConstants.USERLISTS, userLists, PortletSession.PORTLET_SCOPE);
				Key key = (Key) httpSession.getAttribute("key");
				boolean bIsEncrypted = false;
				
				try {
					bIsEncrypted = Encrypter.isEncrypted(redirect, key);
				} catch (Exception e1) {
					if (e1 instanceof IllegalBlockSizeException ||
							e1 instanceof BadPaddingException) {
						bIsEncrypted = false;
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
					httpSession.removeAttribute("key");
					
					redirect = Encrypter.decrypt(redirect, key);
				}
				
				String [] redirectContent = redirect.split(PortalConstants.DELIMITER_125);
				
				for (int i = 0; i < redirectContent.length; i++) {
					if (!redirectContent[i].contains(PortalConstants.USERS_SEARCH_CONTAINER_PK) &&
							!redirectContent[i].contains("group_name") &&
							!redirectContent[i].contains("user_name") &&
							!redirectContent[i].contains("expirationDateLow") &&
							!redirectContent[i].contains("user_role") &&
							!redirectContent[i].contains("user_id") &&
							!redirectContent[i].contains("account_lock") &&
							!redirectContent[i].contains("expirationDateHigh")) {
						if (i == 0) {
							redirect = redirectContent[i];
						} else {
							redirect += PortalConstants.DELIMITER_125 + redirectContent[i];
						}
					}
				}
				
				redirect = HttpUtil.addParameter(redirect, actionResponse.getNamespace() + "p_u_i_d", userId);
				sendRedirect(actionRequest, actionResponse, redirect);
			} else {
				String redirect = ParamUtil.getString(actionRequest, PortalConstants.REDIRECT);
				
				Key key = (Key) httpSession.getAttribute("key");
				boolean bIsEncrypted = false;
				
				try {
					bIsEncrypted = Encrypter.isEncrypted(redirect, key);
				} catch (Exception e1) {
					if (e1 instanceof IllegalBlockSizeException ||
							e1 instanceof BadPaddingException) {
						bIsEncrypted = false;
					}
				}
				
				if (!CommonUtil.isStringNullOrEmpty(redirect) && bIsEncrypted) {
					httpSession.removeAttribute("key");
					
					redirect = Encrypter.decrypt(redirect, key);
				}
				
				String [] redirectContent = redirect.split(PortalConstants.DELIMITER_125);
				
				for (int i = 0; i < redirectContent.length; i++) {
					if (!redirectContent[i].contains(PortalConstants.USERS_SEARCH_CONTAINER_PK) &&
							!redirectContent[i].contains("group_name") &&
							!redirectContent[i].contains("user_name") &&
							!redirectContent[i].contains("expirationDateLow") &&
							!redirectContent[i].contains("user_role") &&
							!redirectContent[i].contains("user_id") &&
							!redirectContent[i].contains("account_lock") &&
							!redirectContent[i].contains("expirationDateHigh")) {
						if (i == 0) {
							redirect = redirectContent[i];
						} else {
							redirect += PortalConstants.DELIMITER_125 + redirectContent[i];
						}
					}
				}
				
				redirect = HttpUtil.addParameter(redirect, actionResponse.getNamespace() + "p_u_i_d", userId);
				sendRedirect(actionRequest, actionResponse, redirect);
			}
		}
	}
}
