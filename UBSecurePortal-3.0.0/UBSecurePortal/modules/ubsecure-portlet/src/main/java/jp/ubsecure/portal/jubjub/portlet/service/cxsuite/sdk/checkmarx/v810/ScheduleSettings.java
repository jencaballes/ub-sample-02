/**
 * ScheduleSettings.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810;

public class ScheduleSettings  implements java.io.Serializable {
    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ScheduleType schedule;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.DayOfWeek[] scheduledDays;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime time;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime startSchedulingPeriod;

    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime endSchedulingPeriod;

    private int schedulingFrequency;

    public ScheduleSettings() {
    }

    public ScheduleSettings(
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ScheduleType schedule,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.DayOfWeek[] scheduledDays,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime time,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime startSchedulingPeriod,
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime endSchedulingPeriod,
           int schedulingFrequency) {
           this.schedule = schedule;
           this.scheduledDays = scheduledDays;
           this.time = time;
           this.startSchedulingPeriod = startSchedulingPeriod;
           this.endSchedulingPeriod = endSchedulingPeriod;
           this.schedulingFrequency = schedulingFrequency;
    }


    /**
     * Gets the schedule value for this ScheduleSettings.
     * 
     * @return schedule
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ScheduleType getSchedule() {
        return schedule;
    }


    /**
     * Sets the schedule value for this ScheduleSettings.
     * 
     * @param schedule
     */
    public void setSchedule(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.ScheduleType schedule) {
        this.schedule = schedule;
    }


    /**
     * Gets the scheduledDays value for this ScheduleSettings.
     * 
     * @return scheduledDays
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.DayOfWeek[] getScheduledDays() {
        return scheduledDays;
    }


    /**
     * Sets the scheduledDays value for this ScheduleSettings.
     * 
     * @param scheduledDays
     */
    public void setScheduledDays(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.DayOfWeek[] scheduledDays) {
        this.scheduledDays = scheduledDays;
    }


    /**
     * Gets the time value for this ScheduleSettings.
     * 
     * @return time
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime getTime() {
        return time;
    }


    /**
     * Sets the time value for this ScheduleSettings.
     * 
     * @param time
     */
    public void setTime(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime time) {
        this.time = time;
    }


    /**
     * Gets the startSchedulingPeriod value for this ScheduleSettings.
     * 
     * @return startSchedulingPeriod
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime getStartSchedulingPeriod() {
        return startSchedulingPeriod;
    }


    /**
     * Sets the startSchedulingPeriod value for this ScheduleSettings.
     * 
     * @param startSchedulingPeriod
     */
    public void setStartSchedulingPeriod(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime startSchedulingPeriod) {
        this.startSchedulingPeriod = startSchedulingPeriod;
    }


    /**
     * Gets the endSchedulingPeriod value for this ScheduleSettings.
     * 
     * @return endSchedulingPeriod
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime getEndSchedulingPeriod() {
        return endSchedulingPeriod;
    }


    /**
     * Sets the endSchedulingPeriod value for this ScheduleSettings.
     * 
     * @param endSchedulingPeriod
     */
    public void setEndSchedulingPeriod(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v810.CxDateTime endSchedulingPeriod) {
        this.endSchedulingPeriod = endSchedulingPeriod;
    }


    /**
     * Gets the schedulingFrequency value for this ScheduleSettings.
     * 
     * @return schedulingFrequency
     */
    public int getSchedulingFrequency() {
        return schedulingFrequency;
    }


    /**
     * Sets the schedulingFrequency value for this ScheduleSettings.
     * 
     * @param schedulingFrequency
     */
    public void setSchedulingFrequency(int schedulingFrequency) {
        this.schedulingFrequency = schedulingFrequency;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ScheduleSettings)) return false;
        ScheduleSettings other = (ScheduleSettings) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.schedule==null && other.getSchedule()==null) || 
             (this.schedule!=null &&
              this.schedule.equals(other.getSchedule()))) &&
            ((this.scheduledDays==null && other.getScheduledDays()==null) || 
             (this.scheduledDays!=null &&
              java.util.Arrays.equals(this.scheduledDays, other.getScheduledDays()))) &&
            ((this.time==null && other.getTime()==null) || 
             (this.time!=null &&
              this.time.equals(other.getTime()))) &&
            ((this.startSchedulingPeriod==null && other.getStartSchedulingPeriod()==null) || 
             (this.startSchedulingPeriod!=null &&
              this.startSchedulingPeriod.equals(other.getStartSchedulingPeriod()))) &&
            ((this.endSchedulingPeriod==null && other.getEndSchedulingPeriod()==null) || 
             (this.endSchedulingPeriod!=null &&
              this.endSchedulingPeriod.equals(other.getEndSchedulingPeriod()))) &&
            this.schedulingFrequency == other.getSchedulingFrequency();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSchedule() != null) {
            _hashCode += getSchedule().hashCode();
        }
        if (getScheduledDays() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getScheduledDays());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getScheduledDays(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTime() != null) {
            _hashCode += getTime().hashCode();
        }
        if (getStartSchedulingPeriod() != null) {
            _hashCode += getStartSchedulingPeriod().hashCode();
        }
        if (getEndSchedulingPeriod() != null) {
            _hashCode += getEndSchedulingPeriod().hashCode();
        }
        _hashCode += getSchedulingFrequency();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ScheduleSettings.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScheduleSettings"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("schedule");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "Schedule"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScheduleType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("scheduledDays");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "ScheduledDays"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "DayOfWeek"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "DayOfWeek"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "Time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxDateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startSchedulingPeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "StartSchedulingPeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxDateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endSchedulingPeriod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "EndSchedulingPeriod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxDateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("schedulingFrequency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "SchedulingFrequency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
