package jp.ubsecure.portal.jubjub.portlet.service.android;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.FileUtil;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.enumclass.ScanStatus;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.ScanLocalServiceUtil;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;
import jp.ubsecure.portal.snark.ProjectManager;
import jp.ubsecure.portal.snark.ScanManager;
import jp.ubsecure.portal.snark.SnarkException;

public class AndroidScanManager {
	
	private static UBSPortalDebugger log = UBSPortalDebugger.getInstance(AndroidScanManager.class);
	
	public static Scan registerAndroidScan(long lCxAndroidProjectId, Scan scan, File apkFile) throws UBSPortalException {
		byte[] tApk = null;
		long lCxAndroidScanId = PortalConstants.LONG_ZERO;
		String strHashValue = null;
		File uploadApk = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (lCxAndroidProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID);
			}
			
			if (CommonUtil.isObjectNull(scan)) {
				params.put("scan", scan);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN);
			}
			
			if (!CommonUtil.isValidFile(apkFile)) {
				params.put("apkFile valid", CommonUtil.isValidFile(apkFile));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);
			}
			
			uploadApk = FileProcessUtil.uploadSourceFile(scan.getProjectId(), scan.getScanId(), apkFile, scan.getFileName());
			
			tApk = FileProcessUtil.parseAndroidApk(uploadApk);
			if (!CommonUtil.isValidBytes(tApk)) {
				params.put("tApk valid", CommonUtil.isValidBytes(tApk));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR);
			}
			
			lCxAndroidScanId = ScanManager.scan(lCxAndroidProjectId, scan.getFileName(), tApk);
			
			if (lCxAndroidScanId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidScanId", lCxAndroidScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID); 
			}
			
			strHashValue = FileUtil.getMD5Checksum(uploadApk);			
			if (CommonUtil.isStringNullOrEmpty(strHashValue)) {
				params.put("strHashValue", strHashValue);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_HASH_VALUE));
				throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR);
			}
			
			scan.setCxAndroidScanId(String.valueOf(lCxAndroidScanId));
			scan.setHashValue(strHashValue);
			scan.setFilePath(uploadApk.getAbsolutePath());
			
		} catch (UBSPortalException e) {
			e.setErrorMessage(PortalMessages.ANDO_REGISTER_SCAN_FAILED);
			params.put("scan", scan);
			params.put("uploadApk valid", CommonUtil.isValidFile(uploadApk));
			
			FileProcessUtil.deleteFileDirectory(uploadApk);
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_REGISTER_ANDROID_SCAN, params, e);
			throw e;
		} catch (SnarkException e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("apkFile valid", CommonUtil.isValidFile(apkFile));
			params.put("tApk valid", CommonUtil.isValidBytes(tApk));
			
			FileProcessUtil.deleteFileDirectory(uploadApk);
			
			log.debug(PortalErrors.ANDROID_SERVER_ERROR, PortalConstants.METHOD_REGISTER_ANDROID_SCAN, params, e);
			throw new UBSPortalException(PortalErrors.ANDROID_SERVER_ERROR, PortalMessages.ANDO_REGISTER_SCAN_FAILED, e);		
		} catch (IOException e) {
			params.put("apkFile", apkFile);
			
			FileProcessUtil.deleteFileDirectory(uploadApk);
			
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_REGISTER_ANDROID_SCAN, params, e);
			throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR, PortalMessages.ANDO_REGISTER_SCAN_FAILED, e);
		} catch (Exception e) {
			params.put("scan", scan);
			params.put("apkFile valid", CommonUtil.isValidFile(apkFile));
			params.put("uploadApk valid", CommonUtil.isValidFile(uploadApk));
			params.put("lCxAndroidScanId", lCxAndroidScanId);
			params.put("strHashValue", strHashValue);
			params.put("tApk valid", CommonUtil.isValidBytes(tApk));
			
			FileProcessUtil.deleteFileDirectory(uploadApk);
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_REGISTER_ANDROID_SCAN, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.ANDO_REGISTER_SCAN_FAILED, e);
		} finally {
			tApk = null;
			strHashValue = null;
			uploadApk = null;
			params.clear();
			params = null;
		}
		
		return scan;
	}
	
	public static void stopAndroidScan(long lCxAndroidProjectId, long lCxAndroidScanId, long lProjectId, long lScanId) throws UBSPortalException {
		File[] fileList = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		try {
			if (lCxAndroidProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID);
			}
			
			if (lCxAndroidScanId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidScanId", lCxAndroidScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID);
			}
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lProjectId", lProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			
			try {
				ScanManager.stopScan(lCxAndroidProjectId, lCxAndroidScanId);					
			} catch (SnarkException e) {
				
				String s = e.getErrorCode();
				if (s.equalsIgnoreCase(PortalErrors.ANDROID_STOP_NON_EXISTING_SCAN)) {
					params.put("lCxAndroidProjectId", lCxAndroidProjectId);
					params.put("lCxAndroidScanId", lCxAndroidScanId);
					log.debug(PortalErrors.ANDROID_STOP_NON_EXISTING_SCAN, PortalConstants.METHOD_STOP_ANDROID_SCAN, params, e);
				} else {
					throw e;
				}
			}
			
			String strScanDirectory = FileProcessUtil.locateScanDirectory(lProjectId, lScanId);
			
			fileList = new File(strScanDirectory).listFiles();
			
			if (fileList != null) {
				for (File file : fileList) {
					if (file.isFile() && !FileUtil.getExtension(file.getName()).equals(PortalConstants.APK_FILE_EXTENSION)) {
						file.delete();
					}
				}				
			}
			
		} catch (UBSPortalException e) {
			e.setErrorMessage(PortalMessages.ANDO_STOP_SCAN_FAILED);
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_STOP_ANDROID_SCAN, params, e);
			throw e;	
		} catch (SnarkException e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("lCxAndroidScanId", lCxAndroidScanId);
			
			log.debug(PortalErrors.ANDROID_SERVER_ERROR, PortalConstants.METHOD_STOP_ANDROID_SCAN, params, e);
			throw new UBSPortalException(PortalErrors.ANDROID_SERVER_ERROR, PortalMessages.ANDO_STOP_SCAN_FAILED, e);
		} catch (Exception e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("lCxAndroidScanId", lCxAndroidScanId);
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_STOP_ANDROID_SCAN, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.ANDO_STOP_SCAN_FAILED, e);
		} finally {
			fileList = null;
			params.clear();
			params = null;
		}
	}
	
	public static long reRunAndroidScan(long lCxAndroidProjectId, long lCxAndroidScanId, long lProjectId, long lScanId, int iStatus) throws UBSPortalException {
		File uploadApk = null;
		long lNewCxAndroidScanId = PortalConstants.LONG_ZERO;
		byte[] tApk = null;
		String strFilePath = null;
		String strErrorCode = null;
		Map<String, Object> params = new HashMap<String, Object>();
		File[] fileList = null;
		
		try {
			if (lCxAndroidProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID);
			}
			
			if (lCxAndroidScanId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidScanId", lCxAndroidScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID);
			}
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lProjectId", lProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			
			strFilePath = ScanLocalServiceUtil.getFilePath(lScanId);
			if (CommonUtil.isStringNullOrEmpty(strFilePath)) {
				params.put("strFilePath", strFilePath);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE_PATH));
				throw new UBSPortalException(PortalErrors.INVALID_FILE_PATH);
			}
			
			uploadApk = new File(strFilePath);
			String strScanDirectory = FileProcessUtil.locateScanDirectory(lProjectId, lScanId);
			
			fileList = new File(strScanDirectory).listFiles();
			
			if (fileList != null) {
				for (File file : fileList) {
					if (file.isFile() && !FileUtil.getExtension(file.getName()).equals(PortalConstants.APK_FILE_EXTENSION)) {
						file.delete();
					}
				}
			}

			if (!CommonUtil.isValidAndExistingFile(uploadApk)) {
				params.put("uploadApk valid", CommonUtil.isValidFile(uploadApk));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.INVALID_FILE);
			}
			
			tApk =  FileProcessUtil.parseAndroidApk(uploadApk);			
			if (!CommonUtil.isValidBytes(tApk)) {
				params.put("tApk valid", CommonUtil.isValidBytes(tApk));
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_FILE));
				throw new UBSPortalException(PortalErrors.FILE_PROCESS_ERROR);
			}
			
			if (iStatus != ScanStatus.FAILURE.getInteger()) {
				try {
					ScanManager.stopScan(lCxAndroidProjectId, lCxAndroidScanId);					
				} catch (SnarkException e) {
					if (e.getErrorCode().equalsIgnoreCase(PortalErrors.ANDROID_STOP_NON_EXISTING_SCAN)) {
						params.put("lCxAndroidProjectId", lCxAndroidProjectId);
						params.put("lCxAndroidScanId", lCxAndroidScanId);
						log.debug(PortalErrors.ANDROID_STOP_NON_EXISTING_SCAN, PortalConstants.METHOD_RERUN_ANDROID_SCAN, params, e);
					} else {
						throw e;
					}
				}
			}
			
			lNewCxAndroidScanId = ScanManager.scan(lCxAndroidProjectId, uploadApk.getName(), tApk);
			
		} catch (UBSPortalException e) {
			e.setErrorMessage(PortalMessages.ANDO_REEXECUTE_SCAN_FAILED);
			params.put("apkFile valid", CommonUtil.isValidFile(uploadApk));
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_RERUN_ANDROID_SCAN, params, e);
			throw e;	
		} catch (PortalException e) {
			if (e.getMessage().startsWith("S.") || e.getMessage().startsWith("U.")) {
				strErrorCode = e.getMessage();
			} else {
				strErrorCode = PortalErrors.PORTAL_EXCEPTION;
			}
			
			log.debug(strErrorCode, PortalConstants.METHOD_RERUN_ANDROID_SCAN, params, e);
			throw new UBSPortalException(strErrorCode, PortalMessages.ANDO_REEXECUTE_SCAN_FAILED, e);
		} catch (SnarkException e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("uploadApk valid", CommonUtil.isValidFile(uploadApk));
			params.put("tApk valid", CommonUtil.isValidBytes(tApk));
			params.put("lProjectId", lProjectId);
			
			log.debug(PortalErrors.ANDROID_SERVER_ERROR, PortalConstants.METHOD_RERUN_ANDROID_SCAN, params, e);
			throw new UBSPortalException(PortalErrors.ANDROID_SERVER_ERROR, PortalMessages.ANDO_REEXECUTE_SCAN_FAILED, e);
		} catch (Exception e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("lScanId", lScanId);
			params.put("uploadApk valid", CommonUtil.isValidFile(uploadApk));
			
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, PortalConstants.METHOD_RERUN_ANDROID_SCAN, params, e);
			throw new UBSPortalException(PortalErrors.UNHANDLED_EXCEPTION, PortalMessages.ANDO_REEXECUTE_SCAN_FAILED, e);
		} finally {
			tApk = null;
			strFilePath = null;
			fileList = null;
			strErrorCode = null;
			params.clear();
			params = null;
		}
		
		return lNewCxAndroidScanId;
	}
	
	public static void deleteAndroidScan(long lCxAndroidProjectId, long lCxAndroidScanId, long lProjectId, long lScanId, boolean bExisting) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		String strErrorCode = null;
		
		try {
			if (lCxAndroidProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidProjectId", lCxAndroidProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_PROJECT_ID);
			}
			
			if (lCxAndroidScanId <= PortalConstants.LONG_ZERO) {
				params.put("lCxAndroidScanId", lCxAndroidScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_CXSUITE_ANDROID_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_CX_ANDROID_SCAN_ID);
			}
			
			if (lProjectId <= PortalConstants.LONG_ZERO) {
				params.put("lProjectId", lProjectId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_PROJECT_ID));
				throw new UBSPortalException(PortalErrors.INVALID_PROJECT_ID);
			}
			
			if (lScanId <= PortalConstants.LONG_ZERO) {
				params.put("lScanId", lScanId);
				params.put(PortalConstants.ERROR_LOG_MESSAGE, String.format(PortalConstants.ERROR_LOG_MESSAGE_PARAM_INVALID, PortalConstants.ERROR_LOG_PARAM_SCAN_ID));
				throw new UBSPortalException(PortalErrors.INVALID_SCAN_ID);
			}
			
			if (bExisting) {
				try {
					ScanManager.stopScan(lCxAndroidProjectId, lCxAndroidScanId);
					ProjectManager.deleteScan(lCxAndroidProjectId, lCxAndroidScanId);
				} catch (SnarkException e) {
					strErrorCode = e.getErrorCode();
					
					if (strErrorCode.equalsIgnoreCase(PortalErrors.ANDROID_STOP_NON_EXISTING_SCAN) || strErrorCode.equalsIgnoreCase(PortalErrors.ANDROID_DELETE_NON_EXISTING_SCAN)) {
						params.put("lCxAndroidProjectId", lCxAndroidProjectId);
						params.put("lCxAndroidScanId", lCxAndroidScanId);
						log.debug(strErrorCode, PortalConstants.METHOD_DELETE_ANDROID_SCAN, params, e);
					} else {
						throw e;
					}
				}				
			}
			
			File scanDirectory = new File(FileProcessUtil.locateScanDirectory(lProjectId, lScanId));
			if (scanDirectory.exists()) {
				FileUtil.deltree(scanDirectory);
			}

			FileProcessUtil.deleteReportFilesByScanId(lScanId);
		} catch (UBSPortalException e) {
			e.setErrorMessage(PortalMessages.ANDO_DELETE_SCAN_FAILED);
			
			log.debug(e.getErrorCode(), PortalConstants.METHOD_DELETE_ANDROID_SCAN, params, e);
			throw e;	
		} catch (SnarkException e) {
			params.put("lCxAndroidProjectId", lCxAndroidProjectId);
			params.put("lCxAndroidScanId", lCxAndroidScanId);
			
			log.debug(PortalErrors.ANDROID_SERVER_ERROR, PortalConstants.METHOD_DELETE_ANDROID_SCAN, params, e);
			throw new UBSPortalException(PortalErrors.ANDROID_SERVER_ERROR, PortalMessages.ANDO_DELETE_SCAN_FAILED, e);
		} finally {
			params.clear();
			params = null;
			strErrorCode = null;
		}
	}
}