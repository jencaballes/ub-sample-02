/**
 * GetProjectScannedDisplayDataResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880;

public class GetProjectScannedDisplayDataResponse  implements java.io.Serializable {
    private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectScannedDisplayData getProjectScannedDisplayDataResult;

    public GetProjectScannedDisplayDataResponse() {
    }

    public GetProjectScannedDisplayDataResponse(
           jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectScannedDisplayData getProjectScannedDisplayDataResult) {
           this.getProjectScannedDisplayDataResult = getProjectScannedDisplayDataResult;
    }


    /**
     * Gets the getProjectScannedDisplayDataResult value for this GetProjectScannedDisplayDataResponse.
     * 
     * @return getProjectScannedDisplayDataResult
     */
    public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectScannedDisplayData getGetProjectScannedDisplayDataResult() {
        return getProjectScannedDisplayDataResult;
    }


    /**
     * Sets the getProjectScannedDisplayDataResult value for this GetProjectScannedDisplayDataResponse.
     * 
     * @param getProjectScannedDisplayDataResult
     */
    public void setGetProjectScannedDisplayDataResult(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v880.CxWSResponseProjectScannedDisplayData getProjectScannedDisplayDataResult) {
        this.getProjectScannedDisplayDataResult = getProjectScannedDisplayDataResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetProjectScannedDisplayDataResponse)) return false;
        GetProjectScannedDisplayDataResponse other = (GetProjectScannedDisplayDataResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getProjectScannedDisplayDataResult==null && other.getGetProjectScannedDisplayDataResult()==null) || 
             (this.getProjectScannedDisplayDataResult!=null &&
              this.getProjectScannedDisplayDataResult.equals(other.getGetProjectScannedDisplayDataResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetProjectScannedDisplayDataResult() != null) {
            _hashCode += getGetProjectScannedDisplayDataResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetProjectScannedDisplayDataResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", ">GetProjectScannedDisplayDataResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getProjectScannedDisplayDataResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "GetProjectScannedDisplayDataResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://Checkmarx.com/v7", "CxWSResponseProjectScannedDisplayData"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
