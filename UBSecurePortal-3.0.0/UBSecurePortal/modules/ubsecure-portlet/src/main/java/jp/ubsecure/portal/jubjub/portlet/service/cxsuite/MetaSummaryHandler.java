package jp.ubsecure.portal.jubjub.portlet.service.cxsuite;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.model.CsvMetaSummaryData;
import jp.ubsecure.portal.jubjub.portlet.report.VulnerabilityMasterParser;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Handler for the Meta summary report
 * for all scans in Source Code Diagnosis and
 * iOS Application Diagnosis.
 * 
 * XML report is parsed using SAX parser to generate the Meta summary
 * report of all scans.
 * 
 * @author ASI632
 *
 */
public class MetaSummaryHandler extends DefaultHandler {
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(MetaSummaryHandler.class);
	
	private long highCount;
	private long mediumCount;
	private long lowCount;
	private long infoCount;
	private String severity;
	CsvMetaSummaryData csvMetaSummaryData;
	
	public MetaSummaryHandler () {
		this.highCount = PortalConstants.LONG_ZERO;
		this.mediumCount = PortalConstants.LONG_ZERO;
		this.lowCount = PortalConstants.LONG_ZERO;
		this.infoCount = PortalConstants.LONG_ZERO;
		this.csvMetaSummaryData = new CsvMetaSummaryData();
	}
	
	@Override
	public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase(PortalConstants.TAG_ATTRIB_CXXMLRESULT)) {
			csvMetaSummaryData.setProjectName(attributes.getValue(PortalConstants.TAG_ATTRIB_PROJECTNAME).trim());
			csvMetaSummaryData.setScanId(attributes.getValue(PortalConstants.TAG_ATTRIB_SCANID).trim());
			csvMetaSummaryData.setExecutionDate(attributes.getValue(PortalConstants.TAG_ATTRIB_SCANSTART).trim());
			csvMetaSummaryData.setExecutionTime(attributes.getValue(PortalConstants.TAG_ATTRIB_SCANTIME).trim());
			csvMetaSummaryData.setLOC(attributes.getValue(PortalConstants.TAG_ATTRIB_LOC).trim());
			csvMetaSummaryData.setPreset(attributes.getValue(PortalConstants.TAG_ATTRIB_PRESET).trim());
		} else if (qName.equalsIgnoreCase(PortalConstants.TAG_QUERY)) {
			String queryName = attributes.getValue(PortalConstants.TAG_ATTRIB_NAME).trim();
			try {
				severity = VulnerabilityMasterParser.getSeverity(queryName);
			} catch (UBSPortalException e) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("queryName", queryName);
				params.put("severity", severity);
				log.debug(e.getErrorCode(), "startElement", params, e);
			}
		} else if (qName.equalsIgnoreCase(PortalConstants.TAG_RESULT)) {
			String state = attributes.getValue(PortalConstants.TAG_ATTRIB_STATE.toLowerCase()).trim();
			
			if (!CommonUtil.isStringNullOrEmpty(state) && !state.equals(PortalConstants.EXCLUDED_STATE)) {
				if (PortalConstants.SEVERITY_HIGH_EN.equalsIgnoreCase(severity) 
						|| PortalConstants.SEVERITY_HIGH_JP.equalsIgnoreCase(severity)) { 
					highCount++;
				} else if (PortalConstants.SEVERITY_MID_EN.equalsIgnoreCase(severity)
						|| PortalConstants.SEVERITY_MID_JP.equalsIgnoreCase(severity)) {
					mediumCount++;
				} else if (PortalConstants.SEVERITY_LOW_EN.equalsIgnoreCase(severity)
						|| PortalConstants.SEVERITY_LOW_JP.equalsIgnoreCase(severity)) {
					lowCount++;
				} else if (PortalConstants.SEVERITY_INFO_EN.equalsIgnoreCase(severity)
						|| PortalConstants.SEVERITY_INFO_JP.equalsIgnoreCase(severity)) {
					infoCount++;
				} 
			}
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equalsIgnoreCase(PortalConstants.TAG_ATTRIB_CXXMLRESULT)) {
			csvMetaSummaryData.setHighCount(String.valueOf(highCount));
			csvMetaSummaryData.setMediumCount(String.valueOf(mediumCount));
			csvMetaSummaryData.setLowCount(String.valueOf(lowCount));
			csvMetaSummaryData.setInfoCount(String.valueOf(infoCount));
		}
	}
}
