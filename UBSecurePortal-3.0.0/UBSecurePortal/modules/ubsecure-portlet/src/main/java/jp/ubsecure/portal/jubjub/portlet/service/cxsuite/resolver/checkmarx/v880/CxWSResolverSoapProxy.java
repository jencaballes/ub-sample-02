package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880;

public class CxWSResolverSoapProxy implements jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResolverSoap {
  private String _endpoint = null;
  private jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResolverSoap cxWSResolverSoap = null;
  
  public CxWSResolverSoapProxy() {
    _initCxWSResolverSoapProxy();
  }
  
  public CxWSResolverSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initCxWSResolverSoapProxy();
  }
  
  private void _initCxWSResolverSoapProxy() {
    try {
      cxWSResolverSoap = (new jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResolverLocator()).getCxWSResolverSoap();
      if (cxWSResolverSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)cxWSResolverSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)cxWSResolverSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (cxWSResolverSoap != null)
      ((javax.xml.rpc.Stub)cxWSResolverSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResolverSoap getCxWSResolverSoap() {
    if (cxWSResolverSoap == null)
      _initCxWSResolverSoapProxy();
    return cxWSResolverSoap;
  }
  
  public jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxWSResponseDiscovery getWebServiceUrl(jp.ubsecure.portal.jubjub.portlet.service.cxsuite.resolver.checkmarx.v880.CxClientType clientType, int APIVersion) throws java.rmi.RemoteException{
    if (cxWSResolverSoap == null)
      _initCxWSResolverSoapProxy();
    return cxWSResolverSoap.getWebServiceUrl(clientType, APIVersion);
  }
  
  
}