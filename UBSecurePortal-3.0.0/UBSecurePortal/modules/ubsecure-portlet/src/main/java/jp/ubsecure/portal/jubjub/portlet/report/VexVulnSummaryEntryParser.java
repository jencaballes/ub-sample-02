package jp.ubsecure.portal.jubjub.portlet.report;

import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.model.VexVulnSummaryEntry;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class VexVulnSummaryEntryParser implements CSVEntryConverter<VexVulnSummaryEntry> {

private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(VexVulnSummaryEntryParser.class);
	
	@Override
	public String[] convertEntry(VexVulnSummaryEntry entry) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (entry == null) {
			params.put("entry", entry);
			log.debug(PortalErrors.FILE_PROCESS_ERROR, PortalConstants.METHOD_CONVERT_ENTRY, params, new IllegalArgumentException());
			throw new IllegalArgumentException(PortalErrors.FILE_PROCESS_ERROR);					
		}
		
		String[] columns = new String[13];
		
		columns[0] = entry.getNumber();
	    columns[1] = entry.getRiskDegree();
	    columns[2] = entry.getVulnCategory();
	    columns[3] = entry.getVulnName();
	    columns[4] = entry.getDetectionUrl();
	    columns[5] = entry.getSignatureID();
	    columns[6] = entry.getDetectionParameters();
	    columns[7] = entry.getOriginalValue();
	    columns[8] = entry.getChangeValue();
	    columns[9] = entry.getDetectionTrigger();
	    columns[10] = entry.getConditionStatus();
	    columns[11] = entry.getExpectedDate();
	    columns[12] = entry.getReportId();
	    
	    for (int index = 0; index < columns.length; index++) {
	    	if (columns[index] == null) {
	    		columns[index] = PortalConstants.STRING_EMPTY;
	    	}
	    }
	    
	    params.clear();
	    params = null;
	    
	    return columns;
	}
}
