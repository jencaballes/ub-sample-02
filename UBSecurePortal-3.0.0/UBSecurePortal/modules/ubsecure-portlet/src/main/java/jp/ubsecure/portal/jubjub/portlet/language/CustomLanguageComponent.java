package jp.ubsecure.portal.jubjub.portlet.language;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.UTF8Control;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;

@Component(
	property = {
		PortalConstants.KEY_LANGUAGE_ID + PortalConstants.LANGUAGE_JA_JP
	},
	service = ResourceBundle.class
)
public class CustomLanguageComponent extends ResourceBundle {
	ResourceBundle bundle = ResourceBundle.getBundle(PortalConstants.RESOURCE_BUNDLE_LANGUAGE_JP, UTF8Control.INSTANCE);
	
	@Override
	public Enumeration<String> getKeys() {
		return bundle.getKeys();
	}

	@Override
	protected Object handleGetObject(String key) {
		return bundle.getObject(key);
	}
}
