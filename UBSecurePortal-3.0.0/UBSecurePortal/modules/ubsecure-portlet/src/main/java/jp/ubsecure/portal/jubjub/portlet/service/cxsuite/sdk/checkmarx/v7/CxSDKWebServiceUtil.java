package jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.CliScanArgs;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.Scan;
import jp.ubsecure.portal.jubjub.portlet.service.cxsuite.sdk.checkmarx.v7.CxSDKWebServiceStub.ScanResponse;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;

public class CxSDKWebServiceUtil {
	
	static CxSDKWebServiceStub service;
	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(CxSDKWebServiceUtil.class);
	
	private static void getCxSDKWebServiceStub () throws UBSPortalException {
		try {
			if (service == null) {
				service = new CxSDKWebServiceStub();
				service._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, new Integer(180000));
			}
		} catch (AxisFault e) {
			log.debug(PortalErrors.CXSUITE_LOGIN_ERROR, PortalConstants.METHOD_GET_CX_SDK_WEB_SERVICE_STUB, null, e);
			throw new UBSPortalException(PortalErrors.CX_PROCESSING_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		}
	}
	
	public static ScanResponse scan (CliScanArgs cliScanArgs, String strSessionId) throws UBSPortalException {
		Map<String, Object> params = new HashMap<String, Object>();
		Scan scan = null;
		ScanResponse scanResponse = null;
		
		try {
			getCxSDKWebServiceStub();
			
			scan = new Scan();
			scan.setArgs(cliScanArgs);
			scan.setSessionId(strSessionId);
			
			log.info(PortalConstants.API_FUNCTION_SCAN, PortalConstants.BEFORE + PortalConstants.API_CALL_SCAN);
			scanResponse = service.scan(scan);
			log.info(PortalConstants.API_FUNCTION_SCAN, PortalConstants.AFTER + PortalConstants.API_CALL_SCAN);
			
			Runtime runtime = Runtime.getRuntime();
			runtime.gc();
		} catch (AxisFault e) {
			log.debug("AXIS FAULT IN SCAN()", "scan", params, e);
			throw new UBSPortalException(PortalErrors.CX_PROCESSING_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (RemoteException e) {
			params.put("response", scanResponse);
			log.debug("REMOTE EXCEPTION IN SCAN()", "scan", params, e);
			throw new UBSPortalException(PortalErrors.CX_PROCESSING_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} catch (Exception e) {
			log.debug(PortalErrors.UNHANDLED_EXCEPTION, "scan", params, e);
			throw new UBSPortalException(PortalErrors.CX_PROCESSING_EXCEPTION, PortalErrors.CX_PROCESSING_EXCEPTION, e);
		} finally {
			params.clear();
			params = null;
		}
		
		return scanResponse;
	}

}
