package jp.ubsecure.portal.jubjub.portlet;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.tools.jspc.resin.BatchJspCompiler;

import jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants;
import jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages;
import jp.ubsecure.portal.jubjub.portlet.log.UBSPortalDebugger;
import jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalException;
import jp.ubsecure.portal.jubjub.portlet.util.CommonUtil;
import jp.ubsecure.portal.jubjub.portlet.util.ConfigurationFileParser;
import jp.ubsecure.portal.jubjub.portlet.util.FileProcessUtil;

@Component(
	immediate = true,
	property = {
		PortalConstants.KEY_PORTLET_DISPLAY_CATEGORY + PortalConstants.DISPLAY_CATEGORY_SAMPLE,
		PortalConstants.KEY_PORTLET_ICON + PortalConstants.ICON_ICON_PNG,
		PortalConstants.KEY_PORTLET_INSTANCEABLE + PortalConstants.INSTANCEABLE_FALSE,
		PortalConstants.KEY_PRIVATE_SESSION_ATTRIBUTES + PortalConstants.PRIVATE_SESSION_ATTRIBUTES_FALSE,
		PortalConstants.KEY_HEADER_PORTLET_CSS + PortalConstants.PORTLET_CSS_MAIN_CSS,
		PortalConstants.KEY_FOOTER_PORTLET_JAVASCRIPT + PortalConstants.PORTLET_JAVASCRIPT_MAIN_JS,
		PortalConstants.KEY_PORTLET_CSS_CLASS_WRAPPER + PortalConstants.CSS_CLASS_WRAPPER_ANNOUNCEMENT_PORTLET,
		PortalConstants.KEY_PORTLET_DISPLAY_NAME + PortalConstants.DISPLAY_NAME_ANNOUNCEMENT_PORTLET,
		PortalConstants.KEY_INIT_VIEW_TEMPLATE + PortalConstants.ANNOUNCEMENT_JSP,
		PortalConstants.KEY_PORTLET_EXPIRATION_CACHE + PortalConstants.EXPIRATION_CACHE,
		PortalConstants.KEY_PORTLET_SUPPORTS_MIME_TYPE + PortalConstants.CONTENT_TYPE_TEXT_HTML,
		PortalConstants.KEY_PORTLET_RESOURCE_BUNDLE + PortalConstants.RESOURCE_BUNDLE_LANGUAGE_JP,
		PortalConstants.KEY_PORTLET_INFO_TITLE + PortalConstants.INFO_TITLE_ANNOUNCEMENT_PORTLET,
		PortalConstants.KEY_PORTLET_INFO_SHORT_TITLE + PortalConstants.INFO_SHORT_TITLE_ANNOUNCEMENT,
		PortalConstants.KEY_SECURITY_ROLE_REF + PortalConstants.ROLE_ADMINISTRATOR + PortalConstants.COMMA
			+ PortalConstants.ROLE_GUEST + PortalConstants.COMMA
			+ PortalConstants.ROLE_POWER_USER + PortalConstants.COMMA
			+ PortalConstants.ROLE_USER
	},
	service = Portlet.class
)
public class AnnouncementPortlet extends MVCPortlet {

	private static final UBSPortalDebugger log = UBSPortalDebugger.getInstance(AnnouncementPortlet.class);

	@Override
	public void doView (RenderRequest renderRequest, RenderResponse renderResponse) {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		HttpServletRequest origRequest = PortalUtil.getOriginalServletRequest(request);
		HttpSession session = origRequest.getSession(false);
		Object oLoggedIn = session.getAttribute(PortalConstants.LOGGED_IN);
		Object oUserId = session.getAttribute(PortalConstants.USER_ID);
		boolean bLoggedIn = false;
		long lUserId = PortalConstants.LONG_ZERO;
		String strAnnouncement = PortalConstants.STRING_EMPTY;
		Enumeration<String> paramNames = renderRequest.getParameterNames();
		PortletURL renderURL = renderResponse.createRenderURL();
		WindowState windowState = renderRequest.getWindowState();
		
		if (!CommonUtil.isObjectNull(oLoggedIn)) {
			bLoggedIn = Boolean.parseBoolean(oLoggedIn.toString());
		}
		
		if (!CommonUtil.isObjectNull(oUserId)) {
			lUserId = Long.parseLong(oUserId.toString());
		}
		
		log.info(PortalMessages.USER_EVENT_VIEW_ANNOUNCEMENT, lUserId);
		
		try {
			if (bLoggedIn) {
				while (paramNames.hasMoreElements()) {
					String paramName = paramNames.nextElement().toString();
					renderURL.setParameter(paramName, renderRequest.getParameter(paramName));
				}
				
				renderURL.setWindowState(WindowState.MAXIMIZED);
				
				Boolean bMaximized = windowState != WindowState.MAXIMIZED;
				
				renderRequest.setAttribute(PortalConstants.PARAM_ANNOUNCEMENT, PortalConstants.STRING_EMPTY);
				
				if (!bMaximized) {
					strAnnouncement = this.getAnnouncement();
					renderRequest.setAttribute(PortalConstants.PARAM_ANNOUNCEMENT, strAnnouncement);
				}
				
				renderRequest.setAttribute(PortalConstants.PARAM_IS_MAXIMIZED, bMaximized);
				
				renderRequest.setAttribute(PortalConstants.PARAM_MAXIMIZED_URL, renderURL.toString());
				
				include(PortalConstants.ANNOUNCEMENT_JSP, renderRequest, renderResponse);
			} else {
				strAnnouncement = this.getAnnouncement();
				renderRequest.setAttribute(PortalConstants.PARAM_ANNOUNCEMENT, strAnnouncement);
				renderRequest.setAttribute(PortalConstants.PARAM_IS_MAXIMIZED, Boolean.FALSE);
				super.doView(renderRequest, renderResponse);
			}
		} catch (IOException e) {
			// do nothing
		} catch (PortletException e) {
			// do nothing
		}
	}
	
	private String getAnnouncement () { 
		String strAnnouncement = PortalConstants.STRING_EMPTY;
		
		try {
			File configFile = new File(FileProcessUtil.getResourceBasePath() + File.separator + PortalConstants.CONFIG_ANNOUNCEMENT_FILE);
 			strAnnouncement = ConfigurationFileParser.getConfigFromFile(PortalConstants.CONFIG_KEY_ANNOUNCEMENT, configFile);
		} catch (UBSPortalException e) {
			// do nothing
		}
		
		return strAnnouncement;
	}

}
