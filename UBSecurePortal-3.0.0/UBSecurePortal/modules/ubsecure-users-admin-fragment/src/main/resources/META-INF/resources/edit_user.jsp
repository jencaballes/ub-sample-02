<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ include file="/init.jsp" %>

<%@ page import="com.liferay.portal.kernel.search.ParseException" %>
<%@ page import="com.liferay.portal.kernel.exception.PasswordExpiredException" %>
<%@ page import="com.liferay.portal.kernel.exception.SystemException" %>
<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>

<%@ page import="javax.crypto.BadPaddingException" %>
<%@ page import="javax.crypto.IllegalBlockSizeException" %>
<%@ page import="javax.crypto.KeyGenerator" %>
<%@ page import="java.security.Key" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalUserPasswordException" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalUserScreenNameException" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.Encrypter" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<%@ page import="org.apache.commons.codec.binary.Base64" %>

<%
boolean bShowDBConnError = false;

renderRequest.getPortletSession().removeAttribute("orderByType");
renderRequest.getPortletSession().removeAttribute("orderByCol");
request.getSession().removeAttribute("searchedUser");

try {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	bShowDBConnError = true;
}

String redirect = ParamUtil.getString(request, "redirect");

if (redirect.contains("usersSearchContainerPrimaryKeys")) {
	String [] redirectContent = redirect.split("_125_");
	
	for (int i = 0; i < redirectContent.length; i++) {
		if (!redirectContent[i].contains("usersSearchContainerPrimaryKeys")) {
			if (i == 0) {
				redirect = redirectContent[i];
			} else {
				redirect += "_125_" + redirectContent[i];
			}
		}
	}
}

String backURL = ParamUtil.getString(request, "backURL", redirect);

int iStringIndex = 0;
String stringURL = "";

if (backURL != null && !backURL.isEmpty()) {
	if (backURL.contains("orderByCol")) {
		iStringIndex = backURL.indexOf("orderByCol");
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		if (stringURL.contains("orderByType")) {
			iStringIndex = stringURL.indexOf("orderByType");
			
			if (iStringIndex >= 0) {
				backURL = stringURL.substring(0, iStringIndex);
			}
			
			iStringIndex = stringURL.indexOf("_125_", iStringIndex);
			
			if (iStringIndex >= 0) {
				backURL += stringURL.substring(iStringIndex + 5);
			}
		}
	}
	
	while (backURL.contains("user_id")) {
		iStringIndex = backURL.indexOf("user_id");
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
	
	while (backURL.contains("group_name")) {
		iStringIndex = backURL.indexOf("group_name");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
	
	while (backURL.contains("user_name")) {
		iStringIndex = backURL.indexOf("user_name");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
	
	while (backURL.contains("mail_address")) {
		iStringIndex = backURL.indexOf("mail_address");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
	
	while (backURL.contains("lastLoginLow")) {
		iStringIndex = backURL.indexOf("lastLoginLow");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
	
	while (backURL.contains("lastLoginHigh")) {
		iStringIndex = backURL.indexOf("lastLoginHigh");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
	
	while (backURL.contains("expiryLow")) {
		iStringIndex = backURL.indexOf("expiryLow");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
	
	while (backURL.contains("expiryHigh")) {
		iStringIndex = backURL.indexOf("expiryHigh");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
	
	while (backURL.contains("account_lock")) {
		iStringIndex = backURL.indexOf("account_lock");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
}

User selUser = PortalUtil.getSelectedUser(request);

Contact selContact = null;

if (selUser != null) {
	selContact = selUser.getContact();
}

PasswordPolicy passwordPolicy = null;

if (!bShowDBConnError && selUser == null) {
	passwordPolicy = PasswordPolicyLocalServiceUtil.getDefaultPasswordPolicy(company.getCompanyId());
}
else {
	passwordPolicy = selUser.getPasswordPolicy();
}

List<Group> groups = Collections.emptyList();

if (selUser != null) {
	groups = selUser.getGroups();

	if (filterManageableGroups) {
		groups = UsersAdminUtil.filterGroups(permissionChecker, groups);
	}
}

List<Organization> organizations = Collections.emptyList();

if (selUser != null) {
	organizations = selUser.getOrganizations();

	if (filterManageableOrganizations) {
		organizations = UsersAdminUtil.filterOrganizations(permissionChecker, organizations);
	}
}
else {
	String organizationIds = ParamUtil.getString(request, "organizationsSearchContainerPrimaryKeys");

	if (!bShowDBConnError && Validator.isNotNull(organizationIds)) {
		long[] organizationIdsArray = StringUtil.split(organizationIds, 0L);

		organizations = OrganizationLocalServiceUtil.getOrganizations(organizationIdsArray);
	}
}

List<Role> roles = Collections.emptyList();

if (selUser != null) {
	roles = selUser.getRoles();

	if (filterManageableRoles) {
		roles = UsersAdminUtil.filterRoles(permissionChecker, roles);
	}
}

List<UserGroupRole> organizationRoles = new ArrayList<UserGroupRole>();
List<UserGroupRole> siteRoles = new ArrayList<UserGroupRole>();

List<UserGroupRole> userGroupRoles = Collections.emptyList();

if (!bShowDBConnError && selUser != null) {
	userGroupRoles = UserGroupRoleLocalServiceUtil.getUserGroupRoles(selUser.getUserId());

	if (filterManageableUserGroupRoles) {
		userGroupRoles = UsersAdminUtil.filterUserGroupRoles(permissionChecker, userGroupRoles);
	}
}

for (UserGroupRole userGroupRole : userGroupRoles) {
	int roleType = userGroupRole.getRole().getType();

	if (roleType == RoleConstants.TYPE_ORGANIZATION) {
		organizationRoles.add(userGroupRole);
	}
	else if (roleType == RoleConstants.TYPE_SITE) {
		siteRoles.add(userGroupRole);
	}
}

List<UserGroup> userGroups = Collections.emptyList();

if (selUser != null) {
	userGroups = selUser.getUserGroups();

	if (filterManageableUserGroups) {
		userGroups = UsersAdminUtil.filterUserGroups(permissionChecker, userGroups);
	}
}

List<UserGroupGroupRole> inheritedSiteRoles = Collections.emptyList();

if (!bShowDBConnError && selUser != null) {
	inheritedSiteRoles = UserGroupGroupRoleLocalServiceUtil.getUserGroupGroupRolesByUser(selUser.getUserId());
}

List<Group> inheritedSites = null;

if (!bShowDBConnError) {
	inheritedSites = GroupLocalServiceUtil.getUserGroupsRelatedGroups(userGroups);
}

List<Group> organizationsRelatedGroups = Collections.emptyList();

if (!bShowDBConnError && !organizations.isEmpty()) {
	organizationsRelatedGroups = GroupLocalServiceUtil.getOrganizationsRelatedGroups(organizations);

	for (Group group : organizationsRelatedGroups) {
		if (!inheritedSites.contains(group)) {
			inheritedSites.add(group);
		}
	}
}

List<Group> allGroups = new ArrayList<Group>();

allGroups.addAll(groups);
allGroups.addAll(inheritedSites);
allGroups.addAll(organizationsRelatedGroups);
if (!bShowDBConnError) {
	allGroups.addAll(GroupLocalServiceUtil.getOrganizationsGroups(organizations));
	allGroups.addAll(GroupLocalServiceUtil.getUserGroupsGroups(userGroups));
}

List<Group> roleGroups = new ArrayList<Group>();

for (Group group : allGroups) {
	if (!bShowDBConnError && RoleLocalServiceUtil.hasGroupRoles(group.getGroupId())) {
		roleGroups.add(group);
	}
}

HttpSession hSession = ControllerHelper.getHttpSession(renderRequest);
hSession.setAttribute("Current_screen", "User_Portlet");
Object downloadFrom = hSession.getAttribute("download_from");
String strDownloadFrom = PortalConstants.STRING_EMPTY;
Object oKey = hSession.getAttribute("key");

Key key = KeyGenerator.getInstance("DES").generateKey();

byte [] encBackURL = null;
String strBackURL = "";
String strRedirect = "";

Key oldKey = null;

if (oKey != null) {
	oldKey = (Key) oKey;
	
	try {
		if (oldKey != null && Encrypter.isEncrypted(backURL.toString(), oldKey)) {
			strBackURL = Encrypter.decrypt(backURL, oldKey);
		}
	} catch (Exception e) {
		if (e instanceof IllegalBlockSizeException ||
				e instanceof BadPaddingException) {
			strBackURL = backURL;
		}
	}
	
	if (strBackURL == null || strBackURL.isEmpty()) {
		strBackURL = backURL;
	}
} else {
	strBackURL = backURL;
}

String formNavigatorId = PortalConstants.USERS_ADMIN_PREFIX + FormNavigatorConstants.FORM_NAVIGATOR_ID_USERS;

String usersListView = ParamUtil.get(request, "usersListView", UserConstants.LIST_VIEW_FLAT_USERS);
String toolbarItem = ParamUtil.getString(request, "toolbarItem", "view-all-users");

Object oManualDownloadError = hSession.getAttribute(PortalConstants.ERROR);
String strManualDownloadError = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oManualDownloadError)) {
	strManualDownloadError = oManualDownloadError.toString();
}

if (!CommonUtil.isObjectNull(downloadFrom)){
	strDownloadFrom = downloadFrom.toString();
}

%>
<%@ include file="/toolbar.jspf" %>

<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-create-user-account-because-the-maximum-number-of-users-has-been-reached" />

<c:if test="<%= !portletName.equals(myAccountPortletId) %>">

	<%
	String strHeader = "";
	
	if (selUser == null) {
		strHeader = "header-account-add";
	} else {
		strHeader = "header-account-change";
	}
	
	renderResponse.setTitle(LanguageUtil.get(request, strHeader));
	%>
	<div class="control-panel-header">
		<div class="control-panel-back">
			<a class="icon-circle-arrow-left previous-level" href="<%= HtmlUtil.escapeHREF(strBackURL) %>" title="<liferay-ui:message key="back" />">
				<span class="control-panel-back-text helper-hidden-accessible">
					<liferay-ui:message key="back" />
				</span>
			</a>
		</div>
		
		<liferay-ui:header
			backURL="<%= HtmlUtil.escape(strBackURL) %>"
			showBackURL = "<%= false %>"
			escapeXml="<%= false %>"
		    title="<%= strHeader %>"
		>
		</liferay-ui:header>
	</div>
</c:if>

<liferay-ui:error-marker key="errorSection" value="details" />
	<liferay-ui:success key="successPwdEdit" message="success-password-edit" />
	<liferay-ui:error exception="<%= SystemException.class %>" message="error-system-exception" />
	<liferay-ui:error exception="<%= PortalException.class %>" message="error-portal-exception" />
	<liferay-ui:error exception="<%= EmailAddressException.class %>" message="error-user-id-invalid" />
	<liferay-ui:error exception="<%= UserEmailAddressException.MustValidate.class %>" message="error-user-id-invalid" />
	<liferay-ui:error exception="<%= UserEmailAddressException.MustNotBeDuplicate.class %>" message="error-user-id-already-taken" />
	<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeDuplicate.class %>" focusField="firstName" message="error-user-id-already-taken" />
	<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeNull.class %>" focusField="firstName" message="error-username-invalid" />
	<liferay-ui:error exception="<%= UBSPortalUserScreenNameException.MustNotExceedMaximumBytes.class %>" focusField="firstName" message="error-username-invalid" />
	<liferay-ui:error exception="<%= UserPasswordException.MustNotBeNull.class %>" focusField="password" message="error-password-invalid" />
	
	<%
	if (selUser == null) {
		%>
		<liferay-ui:error exception="<%= UserPasswordException.MustBeLonger.class %>" focusField="password" message="error-password-invalid" />
		<liferay-ui:error exception="<%= UBSPortalUserPasswordException.MustNotExceedMaximumBytes.class %>" focusField="password" message="error-password-invalid" />
		<%
	} else {
		%>
		<liferay-ui:error exception="<%= UserPasswordException.MustBeLonger.class %>" focusField="password2" message="error-password-invalid" />
		<liferay-ui:error exception="<%= UBSPortalUserPasswordException.MustNotExceedMaximumBytes.class %>" focusField="password2" message="error-password-invalid" />
		<%
	}
	%>
	
	<liferay-ui:error exception="<%= NoSuchOrganizationException.class %>" message="error-user-should-be-a-member-of-a-group" />
  	<liferay-ui:error exception="<%= PasswordExpiredException.class %>" message="error-password-invalid" />  
	<liferay-ui:error exception="<%= java.text.ParseException.class %>" message="error-expiration-date-invalid" />
	<liferay-ui:error exception="<%= ParseException.class %>" message="error-expiration-date-invalid" />
	<liferay-ui:error key="ORMException" message="error-orm-exception" />
	<liferay-ui:error key="error-email-address-invalid" message="error-email-address-invalid" />
	<liferay-ui:error key="error-expiration-date-invalid" message="error-expiration-date-invalid" />
	<%
		
		if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("User_Portlet")) {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
			</div>
		<%
			hSession.removeAttribute(PortalConstants.ERROR);
		} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("User_Portlet")) {
			%>
				<div class="alert alert-danger">
					<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
				</div>
			<%
			hSession.removeAttribute(PortalConstants.ERROR);
		}
	%>
	
	<liferay-ui:error exception="<%= UserPasswordException.class %>">
	<%
	UserPasswordException upe = (UserPasswordException)errorException;
	%>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_ALREADY_USED %>">
		<liferay-ui:message key="error-password-invalid" />
	</c:if>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_CONTAINS_TRIVIAL_WORDS %>">
		<liferay-ui:message key="error-password-invalid" />
	</c:if>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_INVALID %>">
		<liferay-ui:message key="error-password-invalid" />
	</c:if>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_LENGTH %>">

		<liferay-ui:message key="error-password-invalid" />
	</c:if>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_NOT_CHANGEABLE %>">
		<liferay-ui:message key="error-password-invalid" />
	</c:if>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_SAME_AS_CURRENT %>">
		<liferay-ui:message key="error-new-password-same-as-old-password" />
	</c:if>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_TOO_TRIVIAL %>">
		<liferay-ui:message key="error-password-invalid" />
	</c:if>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORD_TOO_YOUNG %>">

		<liferay-ui:message key="error-password-invalid" />
	</c:if>

	<c:if test="<%= upe.getType() == UserPasswordException.PASSWORDS_DO_NOT_MATCH %>">
		<liferay-ui:message key="error-password-did-not-match" />
	</c:if>
	
</liferay-ui:error>

<portlet:actionURL name="/users_admin/edit_user" var="editUserActionURL" />

<portlet:renderURL var="editUserRenderURL">
	<portlet:param name="mvcRenderCommandName" value="/users_admin/edit_user" />
	<%-- <portlet:param name="backURL" value="<%= backURL %>" /> --%>
	<portlet:param name="backURL" value="<%= HtmlUtil.escape(strBackURL) %>" />
</portlet:renderURL>

<%
	byte [] encRedirect = Encrypter.encrypt(editUserRenderURL.toString());
	strRedirect = Base64.encodeBase64String(encRedirect);
	
	encBackURL = Encrypter.encrypt(strBackURL);
	strBackURL = Base64.encodeBase64String(encBackURL);
	
	hSession.setAttribute("key", key);
%>

<aui:form action="<%= editUserActionURL %>" cssClass="container-fluid-1280 portlet-users-admin-edit-user" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= (selUser == null) ? Constants.ADD : Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="<%= HtmlUtil.fromInputSafe(editUserRenderURL) %>" />
	<aui:input name="backURL" type="hidden" value="<%= HtmlUtil.escape(backURL) %>" />
	<aui:input name="p_u_i_d" type="hidden" value="<%= (selUser != null) ? selUser.getUserId() : 0 %>" />

	<%
	request.setAttribute("user.allGroups", allGroups);
	request.setAttribute("user.groups", groups);
	request.setAttribute("user.inheritedSiteRoles", inheritedSiteRoles);
	request.setAttribute("user.inheritedSites", inheritedSites);
	request.setAttribute("user.organizationRoles", organizationRoles);
	request.setAttribute("user.organizations", organizations);
	request.setAttribute("user.passwordPolicy", passwordPolicy);
	request.setAttribute("user.roleGroups", roleGroups);
	request.setAttribute("user.roles", roles);
	request.setAttribute("user.selContact", selContact);
	request.setAttribute("user.selUser", selUser);
	request.setAttribute("user.siteRoles", siteRoles);
	request.setAttribute("user.userGroups", userGroups);

	request.setAttribute("addresses.className", Contact.class.getName());
	request.setAttribute("emailAddresses.className", Contact.class.getName());
	request.setAttribute("phones.className", Contact.class.getName());
	request.setAttribute("websites.className", Contact.class.getName());

	if (selContact != null) {
		request.setAttribute("addresses.classPK", selContact.getContactId());
		request.setAttribute("emailAddresses.classPK", selContact.getContactId());
		request.setAttribute("phones.classPK", selContact.getContactId());
		request.setAttribute("websites.classPK", selContact.getContactId());
	}
	else {
		request.setAttribute("addresses.classPK", 0L);
		request.setAttribute("emailAddresses.classPK", 0L);
		request.setAttribute("phones.classPK", 0L);
		request.setAttribute("websites.classPK", 0L);
	}
	
	boolean blnButtonLabel = (selUser == null) ? true : false;
	
	try {
		if (oKey != null) {
			key = (Key) oKey;
		}
		
		if (oldKey != null && Encrypter.isEncrypted(backURL.toString(), oldKey)) {
			strBackURL = Encrypter.decrypt(backURL, key);
		}
	} catch (Exception e) {
		if (e instanceof IllegalBlockSizeException ||
				e instanceof BadPaddingException) {
			strBackURL = backURL;
		}
	}
	%>

	<%-- <liferay-ui:form-navigator
		backURL="<%= backURL %>"
		formModelBean="<%= selUser %>"
		id="<%= FormNavigatorConstants.FORM_NAVIGATOR_ID_USERS %>"
		markupView="lexicon"
	/> --%>
	
	<liferay-ui:form-navigator
		backURL="<%= CommonUtil.escapeJsPartOfString(backURL) %>"
		formModelBean="<%= selUser %>"
		id="<%= formNavigatorId %>"
		showButtons="<%= blnButtonLabel %>"
	/>
</aui:form>

<%
if (selUser != null) {
	PortalUtil.setPageSubtitle(selUser.getFullName(), request);
}
%>

<aui:script>
	function <portlet:namespace />createURL(href, value, onclick) {
		return '<a href="' + href + '"' + (onclick ? ' onclick="' + onclick + '" ' : '') + '>' + value + '</a>';
	}

	function <portlet:namespace />saveUser(cmd) {
		document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = cmd;

		submitForm(document.<portlet:namespace />fm);
	}
</aui:script>

<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery.fileDownload.js"></script>