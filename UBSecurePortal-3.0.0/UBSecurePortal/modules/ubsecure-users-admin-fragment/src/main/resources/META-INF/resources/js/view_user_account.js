$('.dropdown-toggle').click(function () {
	var classNames = $(this).closest('div').attr('class');
	
	 if (classNames.indexOf("manual-dropdown") > -1) {
		var uList = document.getElementsByClassName("direction-left");
		
		if (uList != null) {
			for (var i = 0; i < uList.length; i++) {
				var classNames = uList[i].getAttribute("class");
				
				if (classNames != null) {
					if (classNames.indexOf("dropdown-menu") > -1) {
						if (uList[i].innerText.indexOf("Android") > -1 || uList[i].innerText.indexOf("iOS") > -1) {
							$(uList[i]).addClass('manual-dropdown-menu');
							$(uList[i]).removeClass('dropdown-menu');
							break;
						}
					}
				}
			}
		}
	} else {
		var spanList = document.getElementsByClassName('taglib-text');
		
		if (spanList != null) {
			for (var i = 0; i < spanList.length; i++) {
				var manualID = $(spanList[i]).closest('a').attr('id');
				if (manualID.toLowerCase().indexOf('manual') > -1) {
					$(spanList[i]).addClass('taglib-text-icon');
					$(spanList[i]).closest('a').css('display', 'block');
					$(spanList[i]).closest('a').css('padding-left', '20px');
					$(spanList[i]).closest('a').css('color', '#ffffff');
					$(spanList[i]).closest('a').hover(function() {
						$(this).css('background-color', '#d5d5ce');
					},
					function () {
						$(this).css('background-color', '#8a8a7b');
					});
					$(spanList[i]).removeClass('taglib-text');
					break;
				}
			}
		}
	}
});

function reload_session() {
	location.reload();
}

function downloadCxManual (url, service) {
	$.fileDownload(url, {
		data: {service: service},
		successCallback: function (url) {
		},
		failCallback: function (html, url) {
			location.reload();
		}
	});
}
