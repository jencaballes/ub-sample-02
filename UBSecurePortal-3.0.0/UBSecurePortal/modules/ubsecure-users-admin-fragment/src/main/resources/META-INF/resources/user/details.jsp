<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.kernel.service.OrganizationLocalServiceUtil"%>
<%@ include file="/init.jsp" %>

<%@ page import="com.liferay.expando.kernel.service.ExpandoValueLocalServiceUtil" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<%@ page import="com.liferay.portal.kernel.model.User" %>

<%
User selUser = (User)request.getAttribute("user.selUser");
long userId = ParamUtil.getLong(request, "p_u_i_d");

String strEmailAddressUserID = "";
String strScreenPortal = "";
String strError = null;
List<Organization> organizationList = null;
List<Organization> currOrganization = null;
int iUserRole = 0;
int iUserId = 0;

HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
Object oUserId = httpSession.getAttribute(PortalConstants.USER_ID);
PortletSession pSession = renderRequest.getPortletSession();

if (!CommonUtil.isObjectNull(oUserRole)) {
	iUserRole = Integer.parseInt(oUserRole.toString());
}

if (!CommonUtil.isObjectNull(oUserId)) {
	iUserId = Integer.parseInt(oUserId.toString());
}

Object objError = pSession.getAttribute(PortalConstants.ERROR);
if (Validator.isNotNull(objError)) {
	strError = String.valueOf(objError);
}

Calendar cDate = CalendarFactoryUtil.getCalendar();
Date now = new Date();
cDate.setTime(now);

boolean bShowDBConnError = false;
LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();

try {
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	bShowDBConnError = true;
}

String strMailAddress = PortalConstants.STRING_EMPTY;

if (selUser != null) {
	strMailAddress = GetterUtil.getString(ExpandoValueLocalServiceUtil.getData(selUser.getCompanyId(),
			User.class.getName(), "CUSTOM_FIELDS", "emailAddress", selUser.getUserId()));
}
		
if (selUser == null) {
	selUser = PortalUtil.getSelectedUser(request);
}

Contact selContact = (Contact)request.getAttribute("user.selContact");
PasswordPolicy passwordPolicy = (PasswordPolicy)request.getAttribute("user.passwordPolicy");

Calendar birthday = CalendarFactoryUtil.getCalendar();

birthday.set(Calendar.MONTH, Calendar.JANUARY);
birthday.set(Calendar.DATE, 1);
birthday.set(Calendar.YEAR, 1970);

if (selContact != null) {
	birthday.setTime(selContact.getBirthday());
}

Map<String, Object> userDetails = (HashMap<String, Object>) pSession.getAttribute("userDetails");

String strEmailAddress = PortalConstants.STRING_EMPTY;
String strUsername = PortalConstants.STRING_EMPTY;
String strPassword = PortalConstants.STRING_EMPTY;
String strSelectedOrg = PortalConstants.STRING_EMPTY;
String strSelectedExpiryDate = PortalConstants.STRING_EMPTY;
boolean bAccountLock = false;
boolean bHasLockoutInput = false;

if (!CommonUtil.isMapNullOrEmpty(userDetails)) {
	Object oEmailAddress = userDetails.get(PortalConstants.EMAIL_ADDRESS);
	Object oFirstName = userDetails.get(PortalConstants.FIRST_NAME);
	Object oMailAddress = userDetails.get(PortalConstants.MAIL_ADDRESS);
	Object oPassword = userDetails.get(PortalConstants.PASSWORD);
	Object oGroupname = userDetails.get(PortalConstants.GROUP_NAME);
	Object oExpirationDate = userDetails.get(PortalConstants.EXPIRATION_DATE);
	Object oLockout = userDetails.get(PortalConstants.ACCOUNT_LOCK);
	
	if (!CommonUtil.isObjectNull(oEmailAddress)) {
		strEmailAddress = oEmailAddress.toString();
		
		if (selUser != null) {
			selUser.setEmailAddress(oEmailAddress.toString());
		}
	}
	
	if (!CommonUtil.isObjectNull(oFirstName)) {
		strUsername = oFirstName.toString();
		
		if (selUser != null) {
			selUser.setFirstName(oFirstName.toString());
		}
	}
	
	if (!CommonUtil.isObjectNull(oMailAddress)) {
		strMailAddress = oMailAddress.toString();
	}
	
	if (!CommonUtil.isObjectNull(oPassword)) {
		strPassword = oPassword.toString();
		
		if (selUser != null) {
			selUser.setPassword(oPassword.toString());
		}
	}
	
	if (!CommonUtil.isObjectNull(oGroupname)) {
		strSelectedOrg = oGroupname.toString();
	}
	
	if (!CommonUtil.isObjectNull(oExpirationDate)) {
		strSelectedExpiryDate = oExpirationDate.toString();
		
		try{
			DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			Calendar dteExpirationDate = null;
			
			dteExpirationDate = Calendar.getInstance();
			dteExpirationDate.setTime(formatter.parse(strSelectedExpiryDate));
			
		}catch(java.text.ParseException ps){
			strSelectedExpiryDate = null;
		}
	}
	
	if (!CommonUtil.isObjectNull(oLockout)) {
		bHasLockoutInput = true;
		bAccountLock = Boolean.valueOf(oLockout.toString());
	}
}

pSession.removeAttribute("userDetails");
%>

<liferay-ui:error-marker key="<%= WebKeys.ERROR_SECTION %>" value="details" />

<aui:model-context bean="<%= selUser %>" model="<%= User.class %>" />

<div class="row">
	<aui:fieldset cssClass="col-md-6">
		<table>
			
			<%-- USERID BUTTON --%>
			<% if (selUser == null) {
			%>
			<tr>
			<%-- USERID --%>
	            <td style="padding: 10px 20px 10px 0px; width:100px;">	
	            	 <div style="width:120px;">	
	            	 	<label style="position: absolute; margin-left: 56px; color:red;">*</label>
	                 	<liferay-ui:message key="label-user-id">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
	                 </div>
	    		</td>					          
	            <td style="padding: 12px 20px 10px 0px">		            			            
	                <aui:input cssClass="clearable"  label="" name="emailAddress" showRequiredLabel="<%=false %>" type="text" value="<%= strEmailAddress %>" id="genEmailInput" />
				</td>	
	            <td style="padding: 10px 0px 10px 0px">
	            	<aui:button value="button-id-generation" onclick="genEmailAddress()" id="genEmailBtn" style="width:120px;margin-left:20px;margin-bottom:8px;height:31px;"></aui:button>	
	            </td>			           
			</tr>
			<% }
			   else{
			%>
			
			<tr>
	            <td  style="padding: 10px 20px 10px 0px; width:100px;">	
	               <div style="width:120px;">	               		            						            	
	    			<liferay-ui:message key="label-user-id">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
	    			</div>				         
	    		</td>					          
	            <td style="padding: 12px 0px 10px 0px; ">		            			            
	               <aui:input type="hidden" inlineLabel="left"  name="emailAddress" value="<%= selUser.getEmailAddress() %>"/>
					<liferay-ui:message key="<%= selUser.getEmailAddress() %>"></liferay-ui:message>	
	            </td>				           
			</tr>
			
			<% } 
			%>
			
			<%-- USERNAME --%>
			
			<tr>
	            <td style="padding: 10px 20px 10px 0px; width:100px;">	
	            	<div style="width:120px;">	       
	            	<label style="position: absolute; margin-left: 56px; color:red;">*</label>        		            						            	
	    			<liferay-ui:message key="label-user-name">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
	    			</div>		
	    		</td>					          
	            <td style="padding: 12px 0px 10px 0px">		            			            
	                <aui:input cssClass="clearable"  label="" name="firstName" showRequiredLabel="<%=false %>" type="text" value="<%= strUsername %>" />
	            </td>				           
	        </tr>
		
			<%-- EMAIL ADDRESS --%>
			<tr>
				<td style="padding: 10px 20px 10px 0px; width:100px;">	
	            	 <div style="width:120px;">	
	            	 	<label style="position: absolute; margin-left: 100px; color:red;">*</label>
	                 	<liferay-ui:message key="label-mail-address">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
	                 </div>
	    		</td>					          
	            <td style="padding: 12px 20px 10px 0px">		            			            
	                <aui:input cssClass="clearable"  label="" name="mailAddress" showRequiredLabel="<%=false %>" type="text" value="<%= strMailAddress %>" />
				</td>			           
			</tr>
			
			<tr>
			<%-- PASSWORD INPUT --%>
	            <td style="padding: 10px 20px 10px 0px; width:100px;">	
	               <div style="width:120px;">	        
	               <%
	               if (selUser == null) {
	               %><label style="position: absolute; margin-left: 64px; color:red;">*</label>
	               <%
	               }
	               %>       		            						            	
	    			<liferay-ui:message key="label-password">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
	    			</div>	
	    	     </td>					          
	            <td style="padding: 12px 20px 10px 0px">		
	            	<% if (selUser == null) {%>            			            
	                <aui:input  cssClass="clearable"  label="" name="password" showRequiredLabel="<%=false %>" type="text" id="genPassInput"/>
	           		<%}
	            	else{
	           		%>
	           		 <aui:input cssClass="clearable"  label="" name="password2" showRequiredLabel="<%=false %>" type="text" id="genPassInput"/>
	          		<%}
	          		%>
	            </td>				           
	 
				
			<%-- PASSWORD BUTTON --%>
					
				<td style="padding: 10px 0px 10px 0px">
					<aui:button type="button" onclick="genPassword()" value="button-password-generation"  id="genPassBtn" style="width:120px;margin-left:20px;margin-bottom:8px;height:31px;" ></aui:button>
				</td>
			 </tr>
			 
			 <%-- ROLE --%>
			<tr>
	            <td style="padding: 10px 20px 10px 0px; width:100px;">	
	               <div style="width:120px;">	 
	                <label style="position: absolute; margin-left: 28px; color:red;">*</label>              		            						            	
	    			<liferay-ui:message key="label-user-role">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
	    			</div>	
	    	    </td>					          
	            <td style="padding: 12px 0px 10px 0px">		            			            
	              <aui:select name="rolename" label="" cssClass="clearable">
					<%
						List<Role> selUserRole = null;
						String strUser = null;
					
						if (selUser != null) {
							selUserRole = selUser.getRoles();
							for (Role r : selUserRole) {
								strUser = r.getName();
							}
						}
						
						if (iUserRole == 1) {//System Administrator
							%>
							<aui:option value="Overall Administrator">
								<liferay-ui:message key="label-overall-administrator">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
							</aui:option>
							<%
						} else if (iUserRole == 2) {//Overall Administrator
							if(strUser == null){
							%>
								<aui:option value="General User">
									<liferay-ui:message key="label-gen-user">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
								</aui:option>
								<aui:option value="Group Administrator">
									<liferay-ui:message key="label-group-administrator">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
								</aui:option>
							<%
							}else{
							%>
								<aui:option value="General User"  selected="<%=strUser.equals("General User")%>">
									<liferay-ui:message key="label-gen-user" >&nbsp; &nbsp; &nbsp;</liferay-ui:message>
								</aui:option>
								<aui:option value="Group Administrator" selected="<%=strUser.equals("Group Administrator")%>">
									<liferay-ui:message key="label-group-administrator" >&nbsp; &nbsp; &nbsp;</liferay-ui:message>
								</aui:option>
							<%
							}
						} else if (iUserRole == 3) {//Group Administrator
							%>
								<aui:option value="General User">
									<liferay-ui:message key="label-gen-user">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
								</aui:option>
							<%
						} else if (iUserRole == 5) {//Super Administrator
							%>
							<aui:option value="System Administrator">
								<liferay-ui:message key="label-system-administrator">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
							</aui:option>
							<%
						}
							
						
					%>
			 	</aui:select>
	            </td>				           
	        </tr>
			
			<%-- ORGANIZATION --%>
			
			
			<%
			if(iUserRole == 2 || iUserRole == 3 ) {
			%>
				<tr>
				    <td style="padding: 10px 20px 10px 0px; width:100px;">	
				        <div style="width:120px;">	   
				            <label style="position: absolute; margin-left: 81px; color:red;">*</label>
				            <liferay-ui:message key="label-belonging-group">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
				    	</div>		            
				    </td>					          
				    
					<td style="padding: 12px 0px 10px 0px">		            			            
						<%
				        int totalOrganization = 0;
				            
				        if (!bShowDBConnError) {
				            totalOrganization = OrganizationLocalServiceUtil.getOrganizationsCount();
				        }
				            	
						if(totalOrganization > 0) {
							if (!bShowDBConnError && iUserRole == 2) {
								organizationList = OrganizationLocalServiceUtil.getOrganizations(0, totalOrganization);
							}else if (!bShowDBConnError && iUserRole == 3) {
								organizationList = OrganizationLocalServiceUtil.getUserOrganizations(iUserId);
							}
							
							boolean ismultipleSelect = (iUserRole == 2)? true : false;
				        %>	            			            
							<aui:select name="groupname" label=""  cssClass="clearable" multiple="<%=ismultipleSelect%>" style="height:50px;">
								<%
									boolean selected = false;
								
									if (!bShowDBConnError) {
										if (selUser != null) {
											selected = false;
											
											if (strSelectedOrg == null || strSelectedOrg.isEmpty()) {
												OrganizationLocalServiceUtil.getUserOrganizations(user.getUserId());
												currOrganization = OrganizationLocalServiceUtil.getUserOrganizations(selUser.getUserId());
												
												if (!CommonUtil.isObjectNull(currOrganization) && currOrganization.size() > 0) {
													strSelectedOrg = currOrganization.get(0).getName();
												}
											}
										} else {
											selected = true;
										}
									}
									%>
										<aui:option selected= "<%= selected %>"></aui:option>								
									<%
									if (!CommonUtil.isObjectNull(currOrganization) && currOrganization.size() > 0) {
										List<String> listUserOrg = new ArrayList<String>();
										
										for(Organization userOrg : currOrganization)
										{
											listUserOrg.add(userOrg.getName());
										}
										
										for (Organization o : organizationList) {
											if (listUserOrg.contains(o.getName())) {
												selected = true;
											} else {
												selected = false;
											}
											%>
											<aui:option selected="<%= selected %>" value="<%= HtmlUtil.escapeAttribute(o.getName()) %>"> <%= HtmlUtil.escapeAttribute(o.getName()) %></aui:option>
											<%
										}
									}else{
										for (Organization o : organizationList) {
											%>
											<aui:option selected="false" value="<%= HtmlUtil.escapeAttribute(o.getName()) %>"> <%= HtmlUtil.escapeAttribute(o.getName()) %></aui:option>
											<%
										}
									}
								%>
							</aui:select>
						 <%
						} else {
							%>	    
							 <aui:input cssClass="clearable"  label="" name="groupname" type="text" disabled="<%= true %>"/>
							<%
						}
						%>           
				    </td>				           
				</tr>
			<%
			}
			%>
			
			<%-- EXPIRATION DATE --%>
			<%
				DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
				Calendar dteExpirationDate = null;
				int year = 0;
				int month = -1;
				int day = 0;
				
				if(selUser != null){	
					if (strSelectedExpiryDate == null || strSelectedExpiryDate.isEmpty()) {
						dteExpirationDate = Calendar.getInstance();
						dteExpirationDate.setTime(GetterUtil.getDate(selUser.getExpandoBridge().getAttribute("acctexpirationdate"), formatter));
					} else {
						dteExpirationDate = Calendar.getInstance();
						dteExpirationDate.setTime(formatter.parse(strSelectedExpiryDate));
					}
					
					Date dteDefault = new Date(0);
					
					if (dteExpirationDate != null && !dteExpirationDate.getTime().equals(dteDefault)) {
						year = dteExpirationDate.get(Calendar.YEAR);
						month = dteExpirationDate.get(Calendar.MONTH);
						day = dteExpirationDate.get(Calendar.DAY_OF_MONTH);
					}
			%>
			
			<tr>
	            <td style="padding: 10px 20px 10px 0px; width:100px;">	
	              <div style="width:120px;">	               		            						            	
	    			<liferay-ui:message key="label-expiration-date">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
	    			</div>
	    		<td style="padding: 12px 0px 10px 0px; ">		            			            
	             	<div class="form-group date-input-text">
	             		<liferay-ui:input-date name="expirationDate" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= day %>" />
	             	</div>
	            </td>				          
	        </tr>
			<%
				}
				else{
					if (strSelectedExpiryDate != null && !strSelectedExpiryDate.isEmpty()) {
						dteExpirationDate = Calendar.getInstance();
						dteExpirationDate.setTime(formatter.parse(strSelectedExpiryDate));
						year = dteExpirationDate.get(Calendar.YEAR);
						month = dteExpirationDate.get(Calendar.MONTH);
						day = dteExpirationDate.get(Calendar.DAY_OF_MONTH);
					}
			%>
			
			<tr>
	            <td style="padding: 10px 20px 10px 0px; width:100px;">	
	               <div style="width:120px;">	               		            						            	
	    			<liferay-ui:message key="label-expiration-date">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
	    			</div>				            
	            </td>					          
	            <td style="padding: 12px 0px 10px 0px">		            			            
	        		<div class="form-group date-input-text">
	        			<liferay-ui:input-date name="expirationDate" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= day %>" /> 
	        		</div>
	            </td>				           
	        </tr>
			<%	} 
			%>
			
			<%-- ACCOUNT LOCK --%>
			<%
				if(selUser != null){
					boolean lockout = selUser.getLockout();
					
					if (bHasLockoutInput) {
						lockout = bAccountLock;
					}
			%>
			
					<tr>
			            <td style="padding: 10px 20px 10px 0px; width:100px;">	
			                 <div style="width:120px;">	               		            						            	
			    			<liferay-ui:message key="label-account-lock">&nbsp; &nbsp; &nbsp;</liferay-ui:message>
			    			</div>
			    	    </td>					          
			            <td style="padding: 12px 0px 10px 0px">		            			            
			                <aui:input cssClass="clearable"  label="" name="accountlock" showRequiredLabel="<%=false %>" type="checkbox" value="<%= lockout %>" style="color:#555;" />
			            </td>				           
			        </tr>
			<%
				}
			%>
			<%
			organizationList = null;
			currOrganization = null;
			%>
		</table>
	</aui:fieldset>
</div>

<input id="error" type="hidden" value="<%=strError%>"/>

<portlet:actionURL name="/users_admin/edit_user" var="editUserActionURL" />
<script>
	define._amd = define.amd;
	define.amd = false;
</script>
<script type="text/javascript"
    src="<%= request.getContextPath() %>/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript">
	
	$(document).ready(function () {
		var pageTitle = $(document).find('title').text();
		var titleBar = pageTitle.split('-');
		var firstName = titleBar[0].split(' '); 
		if (titleBar.length == 3) {
			$(this).attr("title", firstName[0] + " -" + titleBar[1] + "-" + titleBar[2]);			
		}
		
		var email = $("#<portlet:namespace />genEmailInput");
		var portal = $("#<portlet:namespace />firstName");
		var ubsp = $("#<portlet:namespace />genPassInput");
		var group = $("#<portlet:namespace />groupname");
		var expirationDate = $("#<portlet:namespace />expirationDate");
		var mailAddress = $("#<portlet:namespace />mailAddress");
		
		var strError = $("#error").val();
		if (strError == "userId") {
			email.focus();
		} else if (strError == "firstName") {
			portal.focus();
		} else if (strError == "password") {
			ubsp.focus();
		} else if (strError == "mailAddress") {
			mailAddress.focus();
		} else if (strError == "groupname") {
			group.focus();
		} else if (strError == "expirationDate") {
			expirationDate.focus();
		} else {
			email.focus();
		}
		
	});
	
	function genPassword() {
	     $.ajax({
	        url : '<%= editUserActionURL %>',
	        type : 'POST',
	        data: {pid:"getPass"},
	        dataType: 'JSON',
	        success : function(data) {
	          	$('#<portlet:namespace />genPassInput').val(data.ubsp);
	          
	        },
	        error :  function(data, status, code) {
	        }
	    }); 
	}
	
	function genEmailAddress() {
	     $.ajax({
	        url : '<%= editUserActionURL %>',
	        type : 'POST',
	        data: {pid:"getPass"},
	        dataType: 'JSON',
	        success : function(data) {
	          	$('#<portlet:namespace />genEmailInput').val(data.ubsp+"@secureportal.local");
	        },
	        error :  function(data, status, code) {
	        }
	    }); 
	}
</script>

<script>
	define.amd = define._amd;
</script>