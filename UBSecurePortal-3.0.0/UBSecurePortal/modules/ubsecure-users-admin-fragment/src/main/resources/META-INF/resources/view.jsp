<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>

<%
String toolbarItem = ParamUtil.getString(request, "toolbarItem", "view-all-users");

String redirect = ParamUtil.getString(request, "redirect");
String viewUsersRedirect = ParamUtil.getString(request, "viewUsersRedirect");
String backURL = ParamUtil.getString(request, "backURL", redirect);

int status = ParamUtil.getInteger(request, "status", WorkflowConstants.STATUS_APPROVED);

String usersListView = ParamUtil.get(request, "usersListView", UserConstants.LIST_VIEW_FLAT_USERS);

PortletURL portletURL = renderResponse.createRenderURL();

portletURL.setParameter("toolbarItem", toolbarItem);
portletURL.setParameter("usersListView", usersListView);

if (Validator.isNotNull(viewUsersRedirect)) {
	portletURL.setParameter("viewUsersRedirect", viewUsersRedirect);
}

String portletURLString = portletURL.toString();

request.setAttribute("view.jsp-portletURL", portletURL);

request.setAttribute("view.jsp-usersListView", usersListView);

long organizationGroupId = 0;

int inactiveUsersCount = 0;
int usersCount = 0;

long organizationId = ParamUtil.getLong(request, "organizationId", OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID);

Organization organization = null;

if (organizationId != 0) {
	organization = OrganizationServiceUtil.getOrganization(organizationId);
}

if (organization != null) {
	inactiveUsersCount = UserLocalServiceUtil.getOrganizationUsersCount(organizationId, WorkflowConstants.STATUS_INACTIVE);
	usersCount = UserLocalServiceUtil.getOrganizationUsersCount(organizationId, WorkflowConstants.STATUS_APPROVED);
}
else {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();

	if (!usersListView.equals(UserConstants.LIST_VIEW_FLAT_USERS)) {
		userParams.put("noOrganizations", Boolean.TRUE);
		userParams.put("usersOrgsCount", 0);
	}

	inactiveUsersCount = UserLocalServiceUtil.searchCount(company.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
	usersCount = UserLocalServiceUtil.searchCount(company.getCompanyId(), null, WorkflowConstants.STATUS_APPROVED, userParams);
}

Object oIsFromSearch = renderRequest.getAttribute("isSearched");
boolean bFromSearch = false;

if (oIsFromSearch != null) {
	bFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
}

Object oIsFromDeleteUser = renderRequest.getAttribute("isFromDeleteUser");
boolean bIsFromDeleteUser = false;

if (oIsFromDeleteUser != null) {
	bIsFromDeleteUser = Boolean.parseBoolean(oIsFromDeleteUser.toString());
}

Object oIsFromDeleteGroup = renderRequest.getAttribute("fromDeleteGroup");
boolean bIsFromDeleteGroup = false;

if (oIsFromDeleteGroup != null) {
	bIsFromDeleteGroup = Boolean.parseBoolean(oIsFromDeleteGroup.toString());
}

boolean bShowDBConnError = false;

try {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	if (!bFromSearch && !bIsFromDeleteUser && !bIsFromDeleteGroup) {
		bShowDBConnError = true;
	}
}
%>

<%@ include file="/toolbar.jspf" %>

<c:choose>
	<c:when test="<%= usersListView.equals(UserConstants.LIST_VIEW_TREE) %>">

		<%
		request.setAttribute("view.jsp-backURL", backURL);
		request.setAttribute("view.jsp-inactiveUsersCount", inactiveUsersCount);
		request.setAttribute("view.jsp-organization", organization);
		request.setAttribute("view.jsp-organizationGroupId", organizationGroupId);
		request.setAttribute("view.jsp-organizationId", organizationId);
		request.setAttribute("view.jsp-portletURL", portletURL);
		request.setAttribute("view.jsp-status", status);
		request.setAttribute("view.jsp-toolbarItem", toolbarItem);
		request.setAttribute("view.jsp-usersCount", usersCount);
		request.setAttribute("view.jsp-usersListView", usersListView);
		request.setAttribute("view.jsp-viewUsersRedirect", viewUsersRedirect);
		%>

		<liferay-util:include page="/view_tree.jsp" servletContext="<%= application %>" />
	</c:when>
	<c:when test="<%= portletName.equals(UsersAdminPortletKeys.MY_ORGANIZATIONS) || usersListView.equals(UserConstants.LIST_VIEW_FLAT_ORGANIZATIONS) %>">
		<liferay-util:include page="/view_flat_organizations.jsp" servletContext="<%= application %>" />
	</c:when>
	<c:when test="<%= usersListView.equals(UserConstants.LIST_VIEW_FLAT_USERS) %>">

		<%
		request.setAttribute("view.jsp-backURL", backURL);
		request.setAttribute("view.jsp-inactiveUsersCount", inactiveUsersCount);
		request.setAttribute("view.jsp-status", status);
		request.setAttribute("view.jsp-usersListView", usersListView);
		request.setAttribute("view.jsp-viewUsersRedirect", viewUsersRedirect);
		%>

		<liferay-util:include page="/view_flat_users.jsp" servletContext="<%= application %>" />
	</c:when>
</c:choose>

<aui:script>
	function <portlet:namespace />deleteOrganization(organizationId, grpName) {
		$('.table').removeClass('table-hover');

		$("tr td:nth-child(1)").each(function(){
			
			if ($(this).text().trim() == grpName) {
				$(this).closest('tr').children('td').css('background-color', '#EDF8FD');
			}
		});
		
		<portlet:namespace />doDeleteOrganization('<%= Organization.class.getName() %>', organizationId);
	}

	function <portlet:namespace />deleteOrganizations() {
		<portlet:namespace />doDeleteOrganization(
			'<%= Organization.class.getName() %>',
			Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, '<portlet:namespace />allRowIds', '<portlet:namespace />rowIdsOrganization')
		);
	}

	function <portlet:namespace />deleteUsers(cmd) {
		if (((cmd == '<%= Constants.DEACTIVATE %>') && confirm('<%= UnicodeLanguageUtil.get(request, "are-you-sure-you-want-to-deactivate-the-selected-users") %>')) || ((cmd == '<%= Constants.DELETE %>') && confirm('<%= UnicodeLanguageUtil.get(request, "are-you-sure-you-want-to-permanently-delete-the-selected-users") %>')) || (cmd == '<%= Constants.RESTORE %>')) {
			var form = AUI.$(document.<portlet:namespace />fm);

			form.attr('method', 'post');
			form.fm('<%= Constants.CMD %>').val(cmd);
			form.fm('redirect').val(form.fm('usersRedirect').val());
			form.fm('deleteUserIds').val(Liferay.Util.listCheckedExcept(form, '<portlet:namespace />allRowIds', '<portlet:namespace />rowIdsUser'));

			submitForm(form, '<portlet:actionURL name="/users_admin/edit_user" />');
		}
	}

	function <portlet:namespace />doDeleteOrganization(className, ids) {
		var status = <%= WorkflowConstants.STATUS_INACTIVE %>;

		<portlet:namespace />getUsersCount(
			className,
			ids,
			status,
			function(responseData) {
				var count = parseInt(responseData, 10);

				if (count > 0) {
					status = <%= WorkflowConstants.STATUS_APPROVED %>;

					<portlet:namespace />getUsersCount(
						className,
						ids,
						status,
						function(responseData) {
							count = parseInt(responseData, 10);

							if (count > 0) {
								if (confirm('<%= UnicodeLanguageUtil.get(request, "confirm-group-delete") %>')) {
									<portlet:namespace />doDeleteOrganizations(ids);
								}
							}
							else {
								var message;

								if (ids && (ids.toString().split(',').length > 1)) {
									message = '<%= UnicodeLanguageUtil.get(request, "confirm-group-delete") %>';
								}
								else {
									message = '<%= UnicodeLanguageUtil.get(request, "confirm-group-delete") %>';
								}

								if (confirm(message)) {
									<portlet:namespace />doDeleteOrganizations(ids);
								}
							}
						}
					);
				}
				else if (confirm('<%= UnicodeLanguageUtil.get(request, "confirm-group-delete") %>')) {
					<portlet:namespace />doDeleteOrganizations(ids);
				}
			}
		);
	}

	function <portlet:namespace />doDeleteOrganizations(organizationIds) {
		var form = AUI.$(document.<portlet:namespace />fm);

		form.attr('method', 'post');
		form.fm('<%= Constants.CMD %>').val('<%= Constants.DELETE %>');
		form.fm('redirect').val(form.fm('organizationsRedirect').val());
		form.fm('deleteOrganizationIds').val(organizationIds);

		submitForm(form, '<portlet:actionURL name="/users_admin/edit_organization" />');
	}

	function <portlet:namespace />getUsersCount(className, ids, status, callback) {
		AUI.$.ajax(
			'<liferay-portlet:resourceURL id="/users_admin/get_users_count" />',
			{
				data: {
					className: className,
					ids: ids,
					status: status
				},
				success: callback
			}
		);
	}

	function <portlet:namespace />search() {
		console.log("here");
		var form = AUI.$(document.<portlet:namespace />fm);

		form.attr('method', 'post');
		form.fm('<%= Constants.CMD %>').val('');

		submitForm(form, '<%= portletURLString %>');
	}

	function <portlet:namespace />showUsers(status) {

		<%
		PortletURL showUsersURL = renderResponse.createRenderURL();

		showUsersURL.setParameter("mvcRenderCommandName", "/users_admin/view");
		showUsersURL.setParameter("toolbarItem", toolbarItem);
		showUsersURL.setParameter("usersListView", usersListView);

		organizationId = ParamUtil.getLong(request, "organizationId", OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID);

		if (organizationId != OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID) {
			showUsersURL.setParameter("organizationId", String.valueOf(organizationId));
		}

		if (Validator.isNotNull(viewUsersRedirect)) {
			showUsersURL.setParameter("viewUsersRedirect", viewUsersRedirect);
		}
		%>

		location.href = Liferay.Util.addParams('<portlet:namespace />status=' + status.value, '<%= HtmlUtil.escapeJS(showUsersURL.toString()) %>');
	}
	
	Liferay.provide(
		window,
		'<portlet:namespace />deleteUsers',
		function(userId, emailAdd) {
			$('.table').removeClass('table-hover');

			$("tr td:nth-child(2)").each(function(){
				
				if ($(this).text().trim() == emailAdd) {
					$(this).closest('tr').children('td').css('background-color', '#EDF8FD');
				}
			});
			
			if (confirm('<%= UnicodeLanguageUtil.get(request, "confirm-user-delete") %>')) {
				document.<portlet:namespace />fm.method = "post";
				document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = "<%= Constants.DELETE %>";
				document.<portlet:namespace />fm.<portlet:namespace />redirect.value = document.<portlet:namespace />fm.<portlet:namespace />usersRedirect.value;
				document.<portlet:namespace />fm.<portlet:namespace />deleteUserIds.value = userId;

				submitForm(document.<portlet:namespace />fm, '<portlet:actionURL name="/users_admin/edit_user" />');
			} else {
				$('.table').addClass('table-hover');
				
				$('.lfr-search-container').find('tbody').find('tr').each(function(index) {
					$(document).find('td').css('background-color', '#FFFFFF');
				});
			}
		},
		['liferay-util-list-fields']
	);
	
	
	function <portlet:namespace />deleteUsers(userId) {
		document.<portlet:namespace />fm.method = "post";
		document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = "<%= Constants.DELETE %>";
		document.<portlet:namespace />fm.<portlet:namespace />redirect.value = document.<portlet:namespace />fm.<portlet:namespace />usersRedirect.value;
		document.<portlet:namespace />fm.<portlet:namespace />deleteUserIds.value = userId;

		submitForm(document.<portlet:namespace />fm, "<portlet:actionURL><portlet:param name="struts_action" value="/users_admin/edit_user" /></portlet:actionURL>");
	}
	
	function <portlet:namespace />searchByGroupname() {
		var form = AUI.$(document.<portlet:namespace />fm);

		form.attr('method', 'post');
		form.fm('<%= Constants.CMD %>').val('<%= Constants.SEARCH %>');
		form.fm('redirect').val(form.fm('organizationsRedirect').val());
		
		submitForm(form, '<portlet:actionURL name="/users_admin/edit_organization" />');
	}
	
	function <portlet:namespace />searchUser() {
		var form = AUI.$(document.<portlet:namespace />fm);

		form.attr('method', 'post');
		form.fm('<%= Constants.CMD %>').val('<%= Constants.SEARCH %>');
		//form.fm('redirect').val(form.fm('usersRedirect').val());
		form.fm('backURL').val(form.fm('usersRedirect').val());
		
		submitForm(form, '<portlet:actionURL name="/users_admin/edit_user" />');
	}
	
		Liferay.provide(
			window,
			'<portlet:namespace />getUsersCount',
			function(className, ids, status, callback) {
				var A = AUI();
	
				A.io.request(
					'<%= themeDisplay.getPathMain() %>/users_admin/get_users_count',
					{
						data: {
							className: className,
							ids: ids,
							status: status
						},
						on: {
							success: callback
						}
					}
				);
			},
			['aui-io']
		);
		
		Liferay.provide(
			window,
			'<portlet:namespace />clearSearchUser',
			function() {
					document.<portlet:namespace />fm.method = "post";
					document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = "clearSearch";
					document.<portlet:namespace />fm.<portlet:namespace />group_name.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />user_id.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />user_name.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />mail_address.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />lastLoginDateLow.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />lastLoginDateHigh.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />expirationDateLow.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />expirationDateHigh.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />account_lock.value = "";
					//document.<portlet:namespace />fm.<portlet:namespace />redirect.value = document.<portlet:namespace />fm.<portlet:namespace />backURL.value;
	
					submitForm(document.<portlet:namespace />fm, '<portlet:actionURL name="/users_admin/edit_user" />');
			},
			['liferay-util-list-fields']
		);
		
		Liferay.provide(
			window,
			'<portlet:namespace />clearSearchGroup',
			function(groupName) {
					document.<portlet:namespace />fm.method = "post";
					document.<portlet:namespace />fm.<portlet:namespace /><%= Constants.CMD %>.value = "clearSearch";
					document.<portlet:namespace />fm.<portlet:namespace />groupName.value = "";
					document.<portlet:namespace />fm.<portlet:namespace />redirect.value = document.<portlet:namespace />fm.<portlet:namespace />organizationsRedirect.value;
	
					submitForm(document.<portlet:namespace />fm, '<portlet:actionURL name="/users_admin/edit_organization" />');
			},
			['liferay-util-list-fields']
		);
</aui:script>