<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ include file="/init.jsp" %>

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="com.liferay.portal.kernel.service.persistence.OrganizationUtil" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors" %>
<%@ page import="com.liferay.portal.kernel.theme.ThemeDisplay" %>

<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Comparator" %>
<%@ page import="java.util.Enumeration" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.GroupItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<%@ page import="org.apache.commons.beanutils.BeanComparator" %>

<%@ page import="javax.portlet.PortletSession" %>

<%@ page import="com.liferay.portal.kernel.util.PortalUtil" %>

<%
boolean bShowDBConnError = false;

try {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	bShowDBConnError = true;
}

HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
httpSession.setAttribute("Current_screen", "Group_Portlet");
Object downloadFrom = httpSession.getAttribute("download_from");
String strDownloadFrom = PortalConstants.STRING_EMPTY;

String toolbarItem = ParamUtil.getString(request, "toolbarItem", "view-all-organizations");

String displayStyle = ParamUtil.getString(request, "displayStyle", "list");

String usersListView = (String)request.getAttribute("view.jsp-usersListView");

PortletURL portletURL = (PortletURL)request.getAttribute("view.jsp-portletURL");

String keywords = ParamUtil.getString(request, "keywords");

ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
String curURL = td.getURLCurrent();

PortletSession pSession = renderRequest.getPortletSession(false);

if (curURL != null && !curURL.isEmpty()) {
	if (curURL.toLowerCase().contains("saveuserslistview")) {
		pSession.removeAttribute("orderByCol");
		pSession.removeAttribute("orderByType");
		request.getSession().removeAttribute("searchedGroup");
		
	}
}

LinkedHashMap<String, Object> organizationParams = new LinkedHashMap<String, Object>();
List<Organization> organizationList = null;
List<Organization> organizations = null;
List<Organization> removeList = null;
String[] valueArr = null;
Map<String, String[]> params = null;

Object oFromSearch = pSession.getAttribute("isSearched", PortletSession.PORTLET_SCOPE);
pSession.removeAttribute("isSearched", PortletSession.PORTLET_SCOPE);
Object param = null;
String strGroupName = null;
String strNoOfUsers = null;
String strSearchedService = null;
boolean bSearchedServiceCx = false;
boolean bSearchedServiceAndroid = false;
boolean bSearchedServiceIOS = false;
boolean bSearchedServiceVex = false;
boolean bIsFromSearch = false;
boolean bIsSearched = false;
boolean bNoGroupName = false;
boolean bNoNoOfUsers = false;
boolean bNoServices = false;
boolean bNoGroupList = false;
boolean bShowPaginationError = false;
int hookDelta = PortalConstants.PAGINATION_DELTA;
boolean bHasSearchInput = false;
boolean bAndroidSelected = false;
boolean bCxSelected = false;
boolean bIOSSelected = false;
boolean bVexSelected = false;

int start = 0;
int end = 0;
int curPage = 0;

String strSearchInfo = PortalConstants.STRING_EMPTY;
String strSearchInfoConnector = PortalConstants.STRING_EMPTY;
String strSearchInfoComma = LanguageUtil.get(request, PortalConstants.KEY_FILTER_LIST_COMMA);

Map<String, Object> searchedGroup = new HashMap<String, Object>();

if (oFromSearch != null && Boolean.parseBoolean(oFromSearch.toString())) {
	bIsFromSearch = true;
	param = pSession.getAttribute("groupName", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("groupName", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strGroupName = (String) param;
		currentURLObj.setParameter("groupName", strGroupName);
		
		if (strGroupName != null && !strGroupName.isEmpty()) {
			searchedGroup.put("groupName", strGroupName);
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_GROUP_NAME, strGroupName, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	param = pSession.getAttribute("noOfUsers", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("noOfUsers", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strNoOfUsers = (String) param;
		currentURLObj.setParameter("noOfUsers", strNoOfUsers);
		
		if (strNoOfUsers != null && !strNoOfUsers.isEmpty()) {
			searchedGroup.put("noOfUsers", strNoOfUsers);
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_USER_COUNT, strNoOfUsers, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	String strValue = "";
	
	param = pSession.getAttribute(PortalConstants.PARAM_CX_SERVICE_USE_AUTH, PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute(PortalConstants.PARAM_CX_SERVICE_USE_AUTH, PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		bSearchedServiceCx = Boolean.parseBoolean(param.toString());
		currentURLObj.setParameter(PortalConstants.PARAM_CX_SERVICE_USE_AUTH, String.valueOf(bSearchedServiceCx));
	}
	
	param = pSession.getAttribute(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH, PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH, PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		bSearchedServiceAndroid = Boolean.parseBoolean(param.toString());
		currentURLObj.setParameter(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH, String.valueOf(bSearchedServiceAndroid));
	}
	
	param = pSession.getAttribute(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH, PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH, PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		bSearchedServiceIOS = Boolean.parseBoolean(param.toString());
		currentURLObj.setParameter(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH, String.valueOf(bSearchedServiceIOS));
	}
	
	param = pSession.getAttribute(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH, PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH, PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		bSearchedServiceVex = Boolean.parseBoolean(param.toString());
		currentURLObj.setParameter(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH, String.valueOf(bSearchedServiceVex));
	}
	
	if (bSearchedServiceCx) {
		bCxSelected = true;
		strValue = LanguageUtil.get(request, "label-cx-suite");
	}
	
	if (bSearchedServiceAndroid) {
		bAndroidSelected = true;
		
		if (strValue != null && !strValue.isEmpty()) {
			strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + LanguageUtil.get(request, "label-android");
		} else {
			strValue = LanguageUtil.get(request, "label-android");
		}
	}
	
	if (bSearchedServiceIOS) {
		bIOSSelected = true;
		
		if (strValue != null && !strValue.isEmpty()) {
			strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + LanguageUtil.get(request, "label-ios");
		} else {
			strValue = LanguageUtil.get(request, "label-ios");
		}
	}
	
	if (bSearchedServiceVex) {
		bVexSelected = true;
		
		if (strValue != null && !strValue.isEmpty()) {
			strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + LanguageUtil.get(request, "label-vex");
		} else {
			strValue = LanguageUtil.get(request, "label-vex");
		}
	}
	
	if (strValue != null && !strValue.isEmpty()) {
		bHasSearchInput = true;
		strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_SERVICE, strValue, false);
		strSearchInfoConnector = strSearchInfoComma;
	}
	
	request.getSession().setAttribute("searchedGroup", searchedGroup);
	currentURLObj.setParameter("isSearched", String.valueOf(String.valueOf(oFromSearch)));
}

params = null;
valueArr = null;
Map<String, String[]> params1 = currentURLObj.getParameterMap();

boolean showList = true;

if (filterManageableOrganizations) {
	List<Organization> userOrganizations = user.getOrganizations(true);

	if (userOrganizations.isEmpty()) {
		showList = false;
	}
	else {
		organizationParams.put("organizationsTree", userOrganizations);
	}
}

OrganizationSearch searchContainer = new OrganizationSearch(renderRequest, "cur", currentURLObj);

String orderByCol = ParamUtil.getString(request, "orderByCol");
String orderByType = ParamUtil.getString(request, "orderByType");

searchContainer.setOrderByCol(orderByCol);

if (orderByType == null || orderByType.isEmpty()) {
	Object oOrderByType = pSession.getAttribute("orderByType");
	
	if (oOrderByType != null) {
		orderByType = oOrderByType.toString();
		
		if (orderByType.isEmpty()) {
			searchContainer.setOrderByType("asc");
		} else {
			searchContainer.setOrderByType(orderByType);
		}
	}
	
	Object oOrderByCol = pSession.getAttribute("orderByCol");
	
	if (oOrderByCol != null) {
		orderByCol = oOrderByCol.toString();
		
		if (!orderByCol.isEmpty()) {
			searchContainer.setOrderByCol(orderByCol);
			pSession.setAttribute("orderByCol", orderByCol);
		}
	}
	
} else {
	searchContainer.setOrderByType(orderByType);
	pSession.setAttribute("orderByType", orderByType);
	pSession.setAttribute("orderByCol", orderByCol);
}

List<GroupItem> orgList = null;

Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
String strManualDownloadError = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oManualDownloadError)) {
	strManualDownloadError = oManualDownloadError.toString();
}

if (!CommonUtil.isObjectNull(downloadFrom)){
	strDownloadFrom = downloadFrom.toString();
}


%>

<c:choose>
	<c:when test="<%= showList %>">
		<aui:form action="<%= portletURL.toString() %>" cssClass="container-fluid-1280 control-panel-form" method="post" name="fm" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "search();" %>'>
			<liferay-portlet:renderURLParams varImpl="portletURL" />
			<aui:input name="<%= Constants.CMD %>" type="hidden" />
			<aui:input name="toolbarItem" type="hidden" value="<%= toolbarItem %>" />
			<aui:input name="redirect" type="hidden" value="<%= portletURL.toString() %>" />

			<liferay-ui:search-container
				id="organizations"
				searchContainer="<%= searchContainer %>"
				var="organizationSearchContainer"
				emptyResultsMessage="error-no-group"
			>
				
				<div style="color: rgb(59, 137, 175); font-weight: bold; font-size:9px; height:4px;">
					<liferay-ui:message key="header-group-list" ></liferay-ui:message>					
				</div>
				<div style="height:8px;">
					<hr style="height:2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
				</div>	

				<%
				if (bShowDBConnError) {
					%>
					<div class="alert alert-danger">
						<liferay-ui:message key="error-orm-exception" />
					</div>
					<%
				} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("Group_Portlet")) {
					%>
					<div class="alert alert-danger">
						<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
					</div>
				<%
					httpSession.removeAttribute(PortalConstants.ERROR);
				} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("Group_Portlet")) {
					%>
						<div class="alert alert-danger">
							<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
						</div>
					<%
					httpSession.removeAttribute(PortalConstants.ERROR);
				}
				
				/* organizationList = (List<Organization>)renderRequest.getAttribute("orgLists"); */
				organizationList = (List<Organization>) pSession.getAttribute("orgLists", PortletSession.PORTLET_SCOPE);
				pSession.removeAttribute("orgLists", PortletSession.PORTLET_SCOPE);
				
				if (Validator.isNull(organizationList)) {
					bNoGroupList = true;
					if (request.getParameter("cur") == null) {
						curPage = 1;
					} else {
						curPage = Integer.parseInt(request.getParameter("cur").toString());
					}
					
					start = (curPage - 1) * hookDelta;
					end = curPage * hookDelta;
					
					valueArr = null;
					removeList = new ArrayList<Organization>();
					
					organizations = OrganizationLocalServiceUtil.getOrganizations(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
					organizationList = new ArrayList<Organization>(organizations);
					
					params = currentURLObj.getParameterMap();
					
					if (params != null && params.get("isSearched") != null) {
						valueArr = params.get("isSearched");
						bIsSearched = Boolean.parseBoolean(valueArr[0]);
						if (bIsSearched) {
							valueArr = params.get("groupName");
							if (Validator.isNotNull(valueArr)) {
								strGroupName = valueArr[0];
								
								if (strGroupName != null && !strGroupName.isEmpty()) {
									bHasSearchInput = true;
									strSearchInfo += strSearchInfoConnector + PortalConstants.FILTER_GROUP_BY_GROUP_NAME + strGroupName + PortalConstants.STRING_NIHONGO_CLOSE_SQUARE_BRACE;
									strSearchInfoConnector = PortalConstants.STRING_NIHONGO_COMMA;
								}
							}
							
							if (Validator.isNull(strGroupName) || strGroupName.isEmpty()) {
								bNoGroupName = true;
							}
							
							if (bNoGroupName) {
								currentURLObj.removePublicRenderParameter("groupName");
							}
							
							valueArr = params.get("noOfUsers");
							if (Validator.isNotNull(valueArr)) {
								strNoOfUsers = valueArr[0];
								
								if (strNoOfUsers != null && !strNoOfUsers.isEmpty()) {
									bHasSearchInput = true;
									strSearchInfo += strSearchInfoConnector + PortalConstants.FILTER_GROUP_BY_NO_OF_USERS + strNoOfUsers + PortalConstants.STRING_NIHONGO_CLOSE_SQUARE_BRACE;
									strSearchInfoConnector = PortalConstants.STRING_NIHONGO_COMMA;
								}
							}
							
							if (Validator.isNull(strNoOfUsers) || strNoOfUsers.isEmpty()) {
								bNoNoOfUsers = true;
							}
							
							if (bNoNoOfUsers) {
								currentURLObj.removePublicRenderParameter("noOfUsers");
							}
							
							String strValue = "";
							
							valueArr = params.get(PortalConstants.PARAM_CX_SERVICE_USE_AUTH);
							if (Validator.isNotNull(valueArr)) {
								bSearchedServiceCx = Boolean.parseBoolean(valueArr[0]);
							}
							
							valueArr = params.get(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
							if (Validator.isNotNull(valueArr)) {
								bSearchedServiceAndroid = Boolean.parseBoolean(valueArr[0]);
							}
							
							valueArr = params.get(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH);
							if (Validator.isNotNull(valueArr)) {
								bSearchedServiceIOS = Boolean.parseBoolean(valueArr[0]);
							}
							
							valueArr = params.get(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
							if (Validator.isNotNull(valueArr)) {
								bSearchedServiceVex = Boolean.parseBoolean(valueArr[0]);
							}
							
							if (bSearchedServiceCx) {
								bCxSelected = true;
								strValue = PortalConstants.STRING_SERVICE_CXSUITE;
							}
							
							if (bSearchedServiceAndroid) {
								bAndroidSelected = true;
								
								if (strValue != null && !strValue.isEmpty()) {
									strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + PortalConstants.STRING_SERVICE_ANDROID;
								} else {
									strValue = PortalConstants.STRING_SERVICE_ANDROID;
								}
							}
							
							if (bSearchedServiceIOS) {
								bIOSSelected = true;
								
								if (strValue != null && !strValue.isEmpty()) {
									strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + PortalConstants.STRING_SERVICE_IOS;
								} else {
									strValue = PortalConstants.STRING_SERVICE_IOS;
								}
							}
							
							if (bSearchedServiceVex) {
								bVexSelected = true;
								
								if (strValue != null && !strValue.isEmpty()) {
									strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + PortalConstants.STRING_SERVICE_VEX;
								} else {
									strValue = PortalConstants.STRING_SERVICE_VEX;
								}
							}
							
							if (strValue != null && !strValue.isEmpty()) {
								bHasSearchInput = true;
								strSearchInfo += strSearchInfoConnector + PortalConstants.FILTER_GROUP_BY_SERVICE + strValue + PortalConstants.STRING_NIHONGO_CLOSE_SQUARE_BRACE;
								strSearchInfoConnector = PortalConstants.STRING_NIHONGO_COMMA;
							}
							
							if (!bSearchedServiceCx && !bSearchedServiceAndroid && !bSearchedServiceIOS && !bSearchedServiceVex) {
								bNoServices = true;
							}
							
							if (bNoServices) {
								currentURLObj.removePublicRenderParameter(PortalConstants.PARAM_CX_SERVICE_USE_AUTH);
								currentURLObj.removePublicRenderParameter(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
								currentURLObj.removePublicRenderParameter(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH);
								currentURLObj.removePublicRenderParameter(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
							}
						}
					} else {
						searchedGroup = (Map<String, Object>) request.getSession().getAttribute("searchedGroup");
						if (searchedGroup != null && !searchedGroup.isEmpty()) {
							bIsSearched = true;
							
							Object oGroupName = searchedGroup.get("groupName");
							Object oNoOfUsers = searchedGroup.get("noOfUsers");
							Object oServiceCx = searchedGroup.get(PortalConstants.PARAM_CX_SERVICE_USE_AUTH);
							Object oServiceAndroid = searchedGroup.get(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH);
							Object oServiceIOS = searchedGroup.get(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH);
							Object oServiceVex = searchedGroup.get(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH);
							
							if (oGroupName != null) {
								strGroupName = oGroupName.toString();
								
								if (strGroupName != null && !strGroupName.isEmpty()) {
									bHasSearchInput = true;
									strSearchInfo += strSearchInfoConnector + PortalConstants.FILTER_GROUP_BY_GROUP_NAME + strGroupName + PortalConstants.STRING_NIHONGO_CLOSE_SQUARE_BRACE;
									strSearchInfoConnector = PortalConstants.STRING_NIHONGO_COMMA;
									currentURLObj.setParameter("groupName", strGroupName);
								}
							}
							
							if (oNoOfUsers != null) {
								strNoOfUsers = oNoOfUsers.toString();
								
								if (strNoOfUsers != null && !strNoOfUsers.isEmpty()) {
									bHasSearchInput = true;
									strSearchInfo += strSearchInfoConnector + PortalConstants.FILTER_GROUP_BY_NO_OF_USERS + strNoOfUsers + PortalConstants.STRING_NIHONGO_CLOSE_SQUARE_BRACE;
									strSearchInfoConnector = PortalConstants.STRING_NIHONGO_COMMA;
									currentURLObj.setParameter("noOfUsers", strNoOfUsers);
								}
							}
							
							String strValue = "";
							
							if (oServiceCx != null) {
								bSearchedServiceCx = Boolean.parseBoolean(oServiceCx.toString());
							}
							
							if (oServiceAndroid != null) {
								bSearchedServiceAndroid = Boolean.parseBoolean(oServiceAndroid.toString());
							}
							
							if (oServiceIOS != null) {
								bSearchedServiceIOS = Boolean.parseBoolean(oServiceIOS.toString());
							}
							
							if (oServiceVex != null) {
								bSearchedServiceVex = Boolean.parseBoolean(oServiceVex.toString());
							}
							
							if (bSearchedServiceCx) {
								bCxSelected = true;
								strValue = PortalConstants.STRING_SERVICE_CXSUITE;
							}
							
							if (bSearchedServiceAndroid) {
								bAndroidSelected = true;
								
								if (strValue != null && !strValue.isEmpty()) {
									strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + PortalConstants.STRING_SERVICE_ANDROID;
								} else {
									strValue = PortalConstants.STRING_SERVICE_ANDROID;
								}
							}
							
							if (bSearchedServiceIOS) {
								bIOSSelected = true;
								
								if (strValue != null && !strValue.isEmpty()) {
									strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + PortalConstants.STRING_SERVICE_IOS;
								} else {
									strValue = PortalConstants.STRING_SERVICE_IOS;
								}
							}
							
							if (bSearchedServiceVex) {
								bVexSelected = true;
								
								if (strValue != null && !strValue.isEmpty()) {
									strValue += PortalConstants.COMMA + PortalConstants.STR_SPACE + PortalConstants.STRING_SERVICE_VEX;
								} else {
									strValue = PortalConstants.STRING_SERVICE_VEX;
								}
							}
							
							if (strValue != null && !strValue.isEmpty()) {
								bHasSearchInput = true;
								strSearchInfo += strSearchInfoConnector + PortalConstants.FILTER_GROUP_BY_SERVICE + strValue + PortalConstants.STRING_NIHONGO_CLOSE_SQUARE_BRACE;
								strSearchInfoConnector = PortalConstants.STRING_NIHONGO_COMMA;
								currentURLObj.setParameter(PortalConstants.PARAM_CX_SERVICE_USE_AUTH, String.valueOf(bSearchedServiceCx));
								currentURLObj.setParameter(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH, String.valueOf(bSearchedServiceAndroid));
								currentURLObj.setParameter(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH, String.valueOf(bSearchedServiceIOS));
								currentURLObj.setParameter(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH, String.valueOf(bSearchedServiceVex));
							}
						}
					}
					
					if(bIsSearched && !organizationList.isEmpty()) {
						try {
							if (Validator.isNotNull(strGroupName) && !strGroupName.isEmpty()) {
								for (Organization org : organizationList) {
									if (!org.getName().toLowerCase().contains(strGroupName.toLowerCase())) {
										removeList.add(org);
									}
								}
							}
							
							if (!removeList.isEmpty()) {
								organizationList.removeAll(removeList);
							}
							
							removeList = new ArrayList<Organization>();
							if (Validator.isNotNull(strNoOfUsers) && !strNoOfUsers.isEmpty()) {
								int iNoOfUsers = 0;
								
								try {
									iNoOfUsers = Integer.parseInt(strNoOfUsers);
								} catch (NumberFormatException e) {
									
								}
								
								for (Organization org : organizationList) {
									int iNumOfUsers = OrganizationUtil.getUsersSize(org.getOrganizationId());
									
									if (iNumOfUsers != iNoOfUsers) {
										removeList.add(org);
									}
								}
							}
							
							if (!removeList.isEmpty()) {
								organizationList.removeAll(removeList);
							}
							
							removeList = new ArrayList<Organization>();
							
							if (bSearchedServiceCx || bSearchedServiceAndroid || bSearchedServiceIOS || bSearchedServiceVex) {
								for (Organization org : organizationList) {
									boolean blnCxSuiteValue = GetterUtil.getBoolean(org.getExpandoBridge().getAttribute(PortalConstants.PARAM_CX_SERVICE_USE_AUTH));
									boolean blnAndroidValue = GetterUtil.getBoolean(org.getExpandoBridge().getAttribute(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH));
									boolean blnIOSValue = GetterUtil.getBoolean(org.getExpandoBridge().getAttribute(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH));
									boolean blnVexValue = GetterUtil.getBoolean(org.getExpandoBridge().getAttribute(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH));
									
									if ((bSearchedServiceCx && blnCxSuiteValue)
											|| (bSearchedServiceAndroid && blnAndroidValue)
											|| (bSearchedServiceIOS && blnIOSValue)
											|| (bSearchedServiceVex && blnVexValue)){
										removeList.add(org);
									}
								}
							}
							
							if (!removeList.isEmpty()) {
								organizationList.retainAll(removeList);
							}
							
						} catch (Exception e){
							e.printStackTrace();
						}
					}
					
					int size = ListUtil.subList(organizationList, start, end).size();
					if (size == 0 && Validator.isNotNull(organizationList) && !organizationList.isEmpty()) {
						--curPage;
						start = (curPage - 1) * hookDelta;
						end = curPage * hookDelta;
						request.setAttribute("index-overlap", "");
						
						if (SessionErrors.isEmpty(renderRequest) && SessionMessages.isEmpty(renderRequest)) {
							bShowPaginationError = true;
						}
					}
				}
				
				if (!Validator.isNull(organizationList)) {
					if (request.getParameter("cur") == null) {
						curPage = 1;
					} else {
						curPage = Integer.parseInt(request.getParameter("cur").toString());
					}
					
					orgList = new ArrayList<GroupItem>();
					for (Organization org : organizationList) {
						GroupItem groupItem = new GroupItem();
						groupItem.setGroupId(org.getOrganizationId());
						groupItem.setGroupName(org.getName());
						
						int iNumOfUsers = 0;
						iNumOfUsers = OrganizationUtil.getUsersSize(org.getOrganizationId());
						
						String strNumOfUsers = "" + iNumOfUsers;
						
						groupItem.setNoOfUsers(iNumOfUsers);
						
						boolean blnCxSuiteValue = GetterUtil.getBoolean(org.getExpandoBridge().getAttribute(PortalConstants.PARAM_CX_SERVICE_USE_AUTH));
						boolean blnAndroidValue = GetterUtil.getBoolean(org.getExpandoBridge().getAttribute(PortalConstants.PARAM_ANDROID_SERVICE_USE_AUTH));
						boolean blnIOSValue = GetterUtil.getBoolean(org.getExpandoBridge().getAttribute(PortalConstants.PARAM_IOS_SERVICE_USE_AUTH));
						boolean blnVexValue = GetterUtil.getBoolean(org.getExpandoBridge().getAttribute(PortalConstants.PARAM_VEX_SERVICE_USE_AUTH));
						int iService = 0;
						
						if (!blnCxSuiteValue && blnAndroidValue && !blnIOSValue && !blnVexValue) {
							iService = 1;
						} else if (!blnCxSuiteValue && blnAndroidValue && blnIOSValue && !blnVexValue){
							iService = 2;
						} else if (!blnCxSuiteValue && blnAndroidValue && blnIOSValue && blnVexValue){
							iService = 3;
						} else if (!blnCxSuiteValue && blnAndroidValue && !blnIOSValue && blnVexValue){
							iService = 4;
						} else if (!blnCxSuiteValue && !blnAndroidValue && blnIOSValue && !blnVexValue){
							iService = 5;
						} else if (!blnCxSuiteValue && !blnAndroidValue && blnIOSValue && blnVexValue){
							iService = 6;
						} else if (!blnCxSuiteValue && !blnAndroidValue && !blnIOSValue && blnVexValue){
							iService = 7;
						} else if (blnCxSuiteValue && !blnAndroidValue && !blnIOSValue && !blnVexValue){
							iService = 8;
						} else if (blnCxSuiteValue && blnAndroidValue && !blnIOSValue && !blnVexValue){
							iService = 9;
						} else if (blnCxSuiteValue && blnAndroidValue && blnIOSValue && !blnVexValue){
							iService = 10;
						} else if (blnCxSuiteValue && blnAndroidValue && blnIOSValue && blnVexValue){
							iService = 11;
						} else if (blnCxSuiteValue && blnAndroidValue && !blnIOSValue && blnVexValue){
							iService = 12;
						} else if (blnCxSuiteValue && !blnAndroidValue && blnIOSValue && !blnVexValue){
							iService = 13;
						} else if (blnCxSuiteValue && !blnAndroidValue && blnIOSValue && blnVexValue){
							iService = 14;
						} else if (blnCxSuiteValue && !blnAndroidValue && !blnIOSValue && blnVexValue){
							iService = 15;
						}
						
						groupItem.setServices(iService);
						
						orgList.add(groupItem);
					}
				}
				
				if (orgList == null) {
					orgList = new ArrayList<GroupItem>();
				}
				
				List<GroupItem> sortableGroups = new ArrayList<GroupItem>(orgList);
				
				if (Validator.isNotNull(orderByCol) && !orderByCol.isEmpty()) {
					if (orderByCol.equalsIgnoreCase("groupName")) {
						Collections.sort(sortableGroups, new Comparator<GroupItem>() {
							@Override
							public int compare(GroupItem g1, GroupItem g2) {
								int iCompare = 0;
								
								iCompare = g1.getGroupName().compareToIgnoreCase(g2.getGroupName());
								
								return iCompare;
							}
						});
					} else {
						BeanComparator comparator = new BeanComparator(orderByCol);
						Collections.sort(sortableGroups, comparator);
					}
					
					if (orderByType.equalsIgnoreCase("desc")) {
						Collections.reverse(sortableGroups);
					}
				}
				
				orgList = new ArrayList<GroupItem>();
				orgList = sortableGroups;
				
				if (bShowPaginationError) {
				%>
					<div class="alert alert-danger">
						<liferay-ui:message key="next-pagination-error" />
					</div>
				<%
				}
				%>
				
				<liferay-ui:error-marker key="errorSection" value="details"/>
				<liferay-ui:error key="error-orm-exception" message="error-orm-exception" />
				<liferay-ui:error exception="<%= RequiredOrganizationException.class %>" message="error-delete-group-with-user" />
				<liferay-ui:error exception="<%= OrganizationNameException.class %>" message="error-group-search-byte-size" />
				<liferay-ui:error exception="<%= NoSuchOrganizationException.class %>" message="error-group-does-not-exist" />
				<liferay-ui:error key="error-no-of-users-invalid" message="error-no-of-users-invalid" />
			    <liferay-ui:success key="successDelete" message="success-group-delete" />
			    <liferay-ui:success key="successAdd" message="success-group-add" />
			    <liferay-ui:success key="successEdit" message="success-group-edit" />	
			    <liferay-ui:error key="ORMException" message="error-orm-exception" />
				
				<%
			    	String redirect = currentURL;
			    	
			    	portletURL.setParameter("cur", String.valueOf(curPage));
			    %>
			    
				<aui:input disabled="<%= true %>" name="organizationsRedirect" type="hidden" value="<%= portletURL.toString() %>" />
				<aui:input name="deleteOrganizationIds" type="hidden" />

				<portlet:renderURL var="addGroupURL">
					<portlet:param name="mvcRenderCommandName" value="/users_admin/edit_organization" />
					<portlet:param name="redirect" value="<%= redirect %>" />
					<portlet:param name="organizationId" value="<%= String.valueOf(0) %>" />
				</portlet:renderURL>
				
				<aui:button-row>
					<aui:button name="newRegistrationBtn" value="button-new-registration" href="<%= addGroupURL %>" style="width:120px; color:#404040;" />
				</aui:button-row>
					
				<div style="height: 15px; padding-top: -10px;">
					<hr style="height: 1px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
				</div>
				
				<aui:button-row>
					<%
					String taglibClearSearchURL = "javascript:" + renderResponse.getNamespace() + "clearSearchGroup();";
					
					if ((bIsFromSearch || bIsSearched) && bHasSearchInput) {
						%>
						<%-- <aui:button style="width:150px; color: #404040; float:right;" value="button-clear-filter" href="<%= taglibClearSearchURL %>" /> --%>
						<button type="button" class="btn" onClick="<%= taglibClearSearchURL %>" style="width:150px; color: #404040; float:right;"><liferay-ui:message key="button-clear-filter" /></button>
						<%
					} else {
						%>
						<aui:button style="width:150px; color: #404040; float:right;" value="button-clear-filter" disabled="true" />
						<%
					}
					%>
					<a data-toggle="modal" data-target="#groupFilterModal"><span class="btn" style="width:120px; color:#404040; float:right; margin-right:5px;"><liferay-ui:message key="button-filter" /></span></a>
				</aui:button-row>
				
				<div>
					<%
					
			    	if ((bIsFromSearch || bIsSearched) && (strSearchInfo != null && !strSearchInfo.isEmpty())) {
			    		%>
			    			<div class="alert alert-info">
			    				<liferay-ui:message arguments="<%= HtmlUtil.escape(strSearchInfo) %>" key="<%= PortalConstants.KEY_FILTER_LIST %>" localizeKey="<%= false %>" />
			    			</div>
			    		<%
			    	}
			    
					request.setAttribute("groupName", "");
					
					String taglibSearchURL = renderResponse.getNamespace() + "searchByGroupname();";
					%>
					<div class="modal fade" id="groupFilterModal" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false" style="width: auto; display: none;">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<liferay-ui:message key="header-group-filter" />
								</div> <!-- End modal header -->
								
								<div class="modal-body">
									<table>
										<tr>
								            <td class="filter-modal-label">
								            	<liferay-ui:message key="label-group-name"></liferay-ui:message>
								            </td>
								            <td class="filter-modal-input">
								                <aui:input id="groupName" name="groupName" type="text" label="" value="<%= strGroupName %>" />
								            </td>						
										 </tr>
										 
										 <tr>
								            <td class="filter-modal-label">
								            	<liferay-ui:message key="label-no-of-users"></liferay-ui:message>
								            </td>
								            <td class="filter-modal-input">
								                <aui:input id="noOfUsers" name="noOfUsers" type="text" label="" value="<%= strNoOfUsers %>" />
								            </td>						
										 </tr>
										 <tr>
								            <td class="filter-modal-label">
								            	<liferay-ui:message key="label-available-services"></liferay-ui:message>
								            </td>
								            <td class="filter-modal-input">
								                <aui:input type="checkbox" name="cxsuite" label="label-cx-suite" value="<%=bCxSelected%>" />
								                <aui:input type="checkbox" name="android" label="label-android" value="<%=bAndroidSelected%>" />
								                <aui:input type="checkbox" name="ios" label="label-ios" value="<%=bIOSSelected%>" />
												<aui:input type="checkbox" name="vex" label="label-vex" value="<%=bVexSelected%>" />
								            </td>						
										 </tr>
									</table>
								</div> <!-- End modal body -->
								
								<div class="modal-footer">
									<button type="button" class="btn" id="filterGroupBtn" onClick="<%= taglibSearchURL %>" style="width:120px; color: #404040;"><liferay-ui:message key="button-filter" /></button>
									<button type="button" class="btn" data-dismiss="modal" style="width:120px; color:#404040;"><liferay-ui:message key="button-cancel" /></button>
								</div> <!-- End modal footer -->
							</div> <!-- End modal content -->
						</div> <!-- End modal dialog -->
					</div> <!-- End modal -->
				</div>
					
				<c:if test="<%= usersListView.equals(UserConstants.LIST_VIEW_FLAT_ORGANIZATIONS) %>">
					<div id="breadcrumb">
						<liferay-ui:breadcrumb showCurrentGroup="<%= false %>" showGuestGroup="<%= false %>" showLayout="<%= false %>" showPortletBreadcrumb="<%= true %>" />
					</div>
				</c:if>

				<%
				OrganizationSearchTerms searchTerms = (OrganizationSearchTerms)organizationSearchContainer.getSearchTerms();
				
				long parentOrganizationId = OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID;

				if (Validator.isNotNull(keywords)) {
					parentOrganizationId = OrganizationConstants.ANY_PARENT_ORGANIZATION_ID;
				}
				else {
					parentOrganizationId = ParamUtil.getLong(request, "parentOrganizationId", OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID);
				}
				
				List<Organization> orgLists = null;
				orgLists = (List<Organization>)pSession.getAttribute("orgLists");
				%>

				<%-- <liferay-ui:organization-search-container-results organizationParams="<%= organizationParams %>" parentOrganizationId="<%= parentOrganizationId %>" /> --%>

				<liferay-ui:search-container-results>
					<c:choose>
						<c:when test="<%= !bShowDBConnError && bNoGroupList && !bIsFromSearch && !bIsSearched %>">
							<%
							results = ListUtil.subList(orgList, start, end);
							
							organizationSearchContainer.setResults(results);
							organizationSearchContainer.setTotal(orgList.size());
							
							organizationList = null;
							organizations = null;
							removeList = null;
							valueArr = null;
							params = null;
							orgList = null;
							%>
						</c:when>
						
						<c:otherwise>
							<% 
							if (orgList != null) {
								sortableGroups = new ArrayList<GroupItem>(orgList);
								
								if (Validator.isNotNull(orderByCol)) {
									if (orderByCol.equalsIgnoreCase("groupName")) {
										Collections.sort(sortableGroups, new Comparator<GroupItem>() {
											@Override
											public int compare(GroupItem g1, GroupItem g2) {
												int iCompare = 0;
												
												iCompare = g1.getGroupName().compareToIgnoreCase(g2.getGroupName());
												
												return iCompare;
											}
										});
									} else {
										BeanComparator comparator = new BeanComparator(orderByCol);
										Collections.sort(sortableGroups, comparator);
									}
									
									if (orderByType.equalsIgnoreCase("desc")) {
										Collections.reverse(sortableGroups);
									}
								}
								
								orgList = new ArrayList<GroupItem>();
								orgList = sortableGroups;
								
								if (end == 0) {
									end = hookDelta;
								}
								
								results = ListUtil.subList(orgList, start, end); 
		
								organizationSearchContainer.setResults(results);
								organizationSearchContainer.setTotal(orgList.size());
							}
		
							organizationList = null;
							%>
							<%-- <%@ include file="/html/portlet/users_admin/organization_search_results_database.jspf" %> --%>
						</c:otherwise>
					</c:choose>
				</liferay-ui:search-container-results>
				
				<%-- <liferay-ui:search-container-row
					className="com.liferay.portal.kernel.model.Organization"
					escapedModel="<%= true %>"
					keyProperty="organizationId"
					modelVar="organization"
				> --%>
				
				<liferay-ui:search-container-row
					className="jp.ubsecure.portal.jubjub.portlet.model.GroupItem"
					escapedModel="<%= true %>"
					keyProperty="organizationId"
					modelVar="organization"
				>
					<liferay-portlet:renderURL varImpl="rowURL">
						<portlet:param name="mvcRenderCommandName" value="/users_admin/view" />
						<portlet:param name="toolbarItem" value="view-all-organizations" />
						<portlet:param name="redirect" value="<%= organizationSearchContainer.getIteratorURL().toString() %>" />
						<portlet:param name="organizationId" value="<%= String.valueOf(organization.getGroupId()) %>" />
						<portlet:param name="usersListView" value="<%= UserConstants.LIST_VIEW_TREE %>" />
						
					</liferay-portlet:renderURL>

					<%
					/* if (!OrganizationPermissionUtil.contains(permissionChecker, organization.getGroupId(), ActionKeys.VIEW)) {
						rowURL = null;
					} */
					%>

					<%@ include file="/organization/search_columns.jspf" %>

					<liferay-ui:search-container-column-jsp
						cssClass="entry-action-column"
						name="label-action"
						path="/organization_action.jsp"
					/>
				</liferay-ui:search-container-row>

				<liferay-ui:search-iterator />
			</liferay-ui:search-container>
		</aui:form>
	</c:when>
	<c:otherwise>
		<div class="alert alert-info">
			<liferay-ui:message key="you-do-not-belong-to-an-organization-and-are-not-allowed-to-view-other-organizations" />
		</div>
	</c:otherwise>
</c:choose>

<%-- <c:if test="<%= hasAddOrganizationPermission %>">
	<liferay-frontend:add-menu>
		<portlet:renderURL var="viewUsersURL">
			<portlet:param name="toolbarItem" value="<%= toolbarItem %>" />
			<portlet:param name="usersListView" value="<%= usersListView %>" />
		</portlet:renderURL>

		<%
		for (String organizationType : PropsValues.ORGANIZATIONS_TYPES) {
		%>

			<portlet:renderURL var="addOrganizationURL">
				<portlet:param name="mvcRenderCommandName" value="/users_admin/edit_organization" />
				<portlet:param name="redirect" value="<%= viewUsersURL %>" />
				<portlet:param name="type" value="<%= organizationType %>" />
			</portlet:renderURL>

			<liferay-frontend:add-menu-item title="<%= LanguageUtil.get(request, organizationType) %>" url="<%= addOrganizationURL.toString() %>" />

		<%
		}
		%>

	</liferay-frontend:add-menu>
</c:if> --%>

<aui:script>
	Liferay.Util.toggleSearchContainerButton('#<portlet:namespace />delete', '#<portlet:namespace /><%= searchContainerReference.getId(request, "organizationSearchContainer") %>SearchContainer', document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');
</aui:script>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>

<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.10.1.min.js" ></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/main.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery.fileDownload.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#<portlet:namespace />groupName").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	 
		$("#<portlet:namespace />noOfUsers").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	
		
		$(function() {
			var focusedElement = null;
			$(document).on('focus', 'input', function() {
				if (focusedElement == $(this)) return;
				focusedElement = $(this);
				setTimeout(function() {
					focusedElement.select();
				}, 50);
			});
		});
	});
</script>

<script>
	define.amd = define._amd;
</script>