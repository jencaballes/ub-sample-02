<%@ include file="/init.jsp" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.UserItem" %>

<%

String strValue = "";

ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
UserItem user4 = (UserItem)row.getObject();
String strRole = "";

	if (user4 != null) {
		strRole = user4.getRole();
		
		if (strRole != null || !strRole.isEmpty()) {
			if (strRole.equals("Overall Administrator")) {
				strValue = "label-overall-administrator";
			} else if (strRole.equals("System Administrator")) {
				strValue = "label-system-administrator";
			} else if (strRole.equals("General User")) {
				strValue = "label-gen-user";
			} else if (strRole.equals("Group Administrator")) {
				strValue = "label-group-administrator";
			}
		}
	}
%>
	
<liferay-ui:message key="<%= strValue %>" />