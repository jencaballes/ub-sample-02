$('.dropdown-toggle').click(function () {
	$('.table').removeClass('table-hover');
	$('.table').find('td').css('background-color', '#FFF');
	$(this).closest('tr').children('td').css('background-color', '#EDF8FD');
});

$(document).click(function(e) {
	$('.table').find('td').removeAttr('style');
	$('.table').addClass('table-hover');
});