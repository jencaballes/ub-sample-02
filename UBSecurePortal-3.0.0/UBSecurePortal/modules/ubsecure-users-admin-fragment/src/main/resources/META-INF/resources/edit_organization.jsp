<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ include file="/init.jsp" %>

<%@ page import="javax.crypto.BadPaddingException" %>
<%@ page import="javax.crypto.IllegalBlockSizeException" %>
<%@ page import="javax.crypto.KeyGenerator" %>
<%@ page import="java.security.Key" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.Encrypter" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<%@ page import="org.apache.commons.codec.binary.Base64" %>

<%
String toolbarItem = ParamUtil.getString(request, "toolbarItem", "view-all-organizations");
String usersListView = ParamUtil.get(request, "usersListView", UserConstants.LIST_VIEW_FLAT_ORGANIZATIONS);
		 
renderRequest.getPortletSession().removeAttribute("orderByType");
renderRequest.getPortletSession().removeAttribute("orderByCol");
request.getSession().removeAttribute("searchedGroup");

String redirect = ParamUtil.getString(request, "redirect");

String backURL = ParamUtil.getString(request, "backURL", redirect);

int iStringIndex = 0;
String stringURL = "";

if (backURL != null && !backURL.isEmpty()) {
	if (backURL.contains("orderByCol")) {
		iStringIndex = backURL.indexOf("orderByCol");
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		if (stringURL.contains("orderByType")) {
			iStringIndex = stringURL.indexOf("orderByType");
			
			if (iStringIndex >= 0) {
				backURL = stringURL.substring(0, iStringIndex);
			}
			
			iStringIndex = stringURL.indexOf("_125_", iStringIndex);
			
			if (iStringIndex >= 0) {
				backURL += stringURL.substring(iStringIndex + 5);
			}
		}
	}
	
	while (backURL.contains("groupName")) {
		iStringIndex = backURL.indexOf("groupName");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}

	while (backURL.contains("noOfUsers")) {
		iStringIndex = backURL.indexOf("noOfUsers");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		backURL = stringURL;
	}

	while (backURL.contains("availableServices")) {
		iStringIndex = backURL.indexOf("availableServices");
		stringURL = "";
		
		if (iStringIndex >= 0) {
			stringURL = backURL.substring(0, iStringIndex);
		}
		
		iStringIndex = backURL.indexOf("_125_", iStringIndex);
		
		if (iStringIndex >= 0) {
			stringURL += backURL.substring(iStringIndex + 5);
		}
		
		backURL = stringURL;
	}
}

long organizationId = ParamUtil.getLong(request, "organizationId");

Organization organization = OrganizationServiceUtil.fetchOrganization(organizationId);

long parentOrganizationId = ParamUtil.getLong(request, "parentOrganizationSearchContainerPrimaryKeys", (organization != null) ? organization.getParentOrganizationId() : OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID);
String type = BeanParamUtil.getString(organization, request, "type");

boolean bShowDBConnError = false;

try {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	bShowDBConnError = true;
}

HttpSession hSession = ControllerHelper.getHttpSession(renderRequest);
hSession.setAttribute("Current_screen", "Group_Portlet");
Object downloadFrom = hSession.getAttribute("download_from");
String strDownloadFrom = PortalConstants.STRING_EMPTY;
Object oKey = hSession.getAttribute("key");

Key key = KeyGenerator.getInstance("DES").generateKey();

String strBackURL = "";
String strRedirect = "";

Key oldKey = null;

if (oKey != null) {
	oldKey = (Key) oKey;
	
	try {
		if (oldKey != null && Encrypter.isEncrypted(backURL.toString(), oldKey)) {
			strBackURL = Encrypter.decrypt(backURL.toString(), oldKey);
		}
	} catch (Exception e) {
		if (e instanceof IllegalBlockSizeException ||
				e instanceof BadPaddingException) {
			strBackURL = backURL;
		}
	}
	
	if (strBackURL == null || strBackURL.isEmpty()) {
		strBackURL = backURL;
	}
} else {
	strBackURL = backURL;
}

String headerTitle = null;

if (organization != null) {
	headerTitle = LanguageUtil.format(request, "header-group-change", organization.getName(), false);
}
else if (Validator.isNotNull(type)) {
	headerTitle = LanguageUtil.format(request, "header-group-add", type);
}
else {
	headerTitle = LanguageUtil.get(request, "header-group-add");
}

renderResponse.setTitle(headerTitle);

Object oManualDownloadError = hSession.getAttribute(PortalConstants.ERROR);
String strManualDownloadError = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oManualDownloadError)) {
	strManualDownloadError = oManualDownloadError.toString();
}

if (!CommonUtil.isObjectNull(downloadFrom)){
	strDownloadFrom = downloadFrom.toString();
}

%>

<%@ include file="/toolbar.jspf" %>

<div class="control-panel-header">
	<div class="control-panel-back">
		<a class="icon-circle-arrow-left previous-level" href="<%= HtmlUtil.escapeHREF(strBackURL) %>" title="<liferay-ui:message key="back" />">
			<span class="control-panel-back-text helper-hidden-accessible">
				<liferay-ui:message key="back" />
			</span>
		</a>
	</div>
	
	<liferay-ui:header
		backURL="<%= HtmlUtil.escape(strBackURL) %>"
		showBackURL = "<%= false %>"
		escapeXml="<%= false %>"
	    title="<%= headerTitle %>"
	>
	</liferay-ui:header>
</div>

<liferay-ui:error-marker key="errorSection" value="details"/>
	<%
		
		if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("Group_Portlet")) {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
			</div>
		<%
			hSession.removeAttribute(PortalConstants.ERROR);
		} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("Group_Portlet")) {
			%>
				<div class="alert alert-danger">
					<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
				</div>
			<%
			hSession.removeAttribute(PortalConstants.ERROR);
		}
	%>
<liferay-ui:error key="<%= PortalErrors.ADD_GROUP_ALREADY_EXISTS %>" message="error-group-name-taken" />
<liferay-ui:error key="<%= PortalErrors.ADD_GROUP_EXCEEDS_BYTE_SIZE %>" message="error-group-valid-name" />
<liferay-ui:error key="<%=PortalErrors.EDIT_GROUP_EMPTY  %>" message="error-group-valid-name" />
<liferay-ui:error key="<%=PortalErrors.EDIT_GROUP_WITH_PROJECT  %>" message="error-update-group-with-project" />
<liferay-ui:error exception="<%= NoSuchOrganizationException.class %>" message="error-group-does-not-exist" />
<liferay-ui:error exception="<%= RequiredOrganizationException.class %>" message="error-group-valid-name" />
<liferay-ui:error key="ORMException" message="error-orm-exception" />

<portlet:actionURL name="/users_admin/edit_organization" var="editOrganizationActionURL" />

<portlet:renderURL var="editOrganizationRenderURL">
	<portlet:param name="mvcRenderCommandName" value="/users_admin/edit_organization" />
	<portlet:param name="backURL" value="<%= HtmlUtil.escape(backURL) %>" />
</portlet:renderURL>

<%
	byte [] encBackURL = null;

	byte [] encRedirect = Encrypter.encrypt(editOrganizationRenderURL.toString());
	strRedirect = Base64.encodeBase64String(encRedirect);
	
	encBackURL = Encrypter.encrypt(strBackURL);
	strBackURL = Base64.encodeBase64String(encBackURL);
	
	hSession.setAttribute("key", key);
%>

<aui:form action="<%= editOrganizationActionURL %>" cssClass="container-fluid-1280 portlet-users-admin-edit-organization" method="post" name="fm">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= (organization == null) ? Constants.ADD : Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="<%= HtmlUtil.fromInputSafe(editOrganizationRenderURL) %>" />
	<aui:input name="backURL" type="hidden" value="<%= HtmlUtil.fromInputSafe(backURL) %>" />
	<aui:input name="organizationId" type="hidden" value="<%= organizationId %>" />

	<%
	request.setAttribute("addresses.className", Organization.class.getName());
	request.setAttribute("addresses.classPK", organizationId);
	request.setAttribute("emailAddresses.className", Organization.class.getName());
	request.setAttribute("emailAddresses.classPK", organizationId);
	request.setAttribute("phones.className", Organization.class.getName());
	request.setAttribute("phones.classPK", organizationId);
	request.setAttribute("websites.className", Organization.class.getName());
	request.setAttribute("websites.classPK", organizationId);
	%>

	<liferay-util:buffer var="htmlTop">
		<%-- <c:if test="<%= organization != null %>">

			<%
			long logoId = organization.getLogoId();
			%>

			<div class="organization-info">
				<div class="float-container">
					<img alt="<%= HtmlUtil.escapeAttribute(organization.getName()) %>" class="organization-logo" src="<%= themeDisplay.getPathImage() %>/organization_logo?img_id=<%= logoId %>&t=<%= WebServerServletTokenUtil.getToken(logoId) %>" />

					<span class="organization-name"><%= HtmlUtil.escape(organization.getName()) %></span>
				</div>
			</div>
		</c:if> --%>
	</liferay-util:buffer>
	
	<%
		boolean blnButtonLabel = (organization == null) ? true : false;
	
		try {
			if (oKey != null) {
				key = (Key) oKey;
			}
			
			if (oldKey != null && Encrypter.isEncrypted(backURL.toString(), oldKey)) {
				strBackURL = Encrypter.decrypt(backURL, key);
			}
		} catch (Exception e) {
			if (e instanceof IllegalBlockSizeException ||
					e instanceof BadPaddingException) {
				strBackURL = backURL;
			}
		}
		
		String formNavigatorId = "organization." + FormNavigatorConstants.FORM_NAVIGATOR_ID_ORGANIZATIONS;
	%>

	<%-- <liferay-ui:form-navigator
		backURL="<%= backURL %>"
		formModelBean="<%= organization %>"
		htmlTop="<%= htmlTop %>"
		id="<%= FormNavigatorConstants.FORM_NAVIGATOR_ID_ORGANIZATIONS %>"
		markupView="lexicon"
	/> --%>
	
	<liferay-ui:form-navigator
		backURL="<%= CommonUtil.escapeJsPartOfString(backURL) %>"
		formModelBean="<%= organization %>"
		htmlTop="<%= htmlTop %>"
		id="<%= formNavigatorId %>"
		showButtons="<%= blnButtonLabel %>"
		jspPath="/organization/"
	/>
</aui:form>

<aui:script>
	function <portlet:namespace />createURL(href, value, onclick) {
		return '<a href="' + href + '"' + (onclick ? ' onclick="' + onclick + '" ' : '') + '>' + value + '</a>';
	}
</aui:script>

<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery.fileDownload.js"></script>