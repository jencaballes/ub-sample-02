<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portal.kernel.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ include file="/init.jsp" %>

<%@ page import="com.liferay.expando.kernel.service.ExpandoValueLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="com.liferay.portal.kernel.theme.ThemeDisplay" %>
<%@ page import="com.liferay.portal.kernel.util.DateUtil" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.TimeZone" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Set" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.controller.ControllerHelper" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalUserEmailAddressException" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalUserScreenNameException" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.model.UserItem" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.util.CommonUtil" %>

<%@ page import="org.apache.commons.beanutils.BeanComparator" %>

<%@ page import="javax.portlet.PortletSession" %>

<%
PortletSession pSession = renderRequest.getPortletSession(false);
		 
Object oIsFromSearch = pSession.getAttribute(PortalConstants.IS_SEARCHED, PortletSession.PORTLET_SCOPE);
boolean bFromSearch = false;

if (oIsFromSearch != null) {
	bFromSearch = Boolean.parseBoolean(oIsFromSearch.toString());
}

Object oIsFromDeleteUser = pSession.getAttribute(PortalConstants.PARAM_IS_FROM_DELETE_USER, PortletSession.PORTLET_SCOPE);
boolean bIsFromDeleteUser = false;

if (oIsFromDeleteUser != null) {
	bIsFromDeleteUser = Boolean.parseBoolean(oIsFromDeleteUser.toString());
}

boolean bShowDBConnError = false;

try {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	if (!bFromSearch && !bIsFromDeleteUser) {
		bShowDBConnError = true;
	}
}

HttpSession httpSession = ControllerHelper.getHttpSession(renderRequest);
httpSession.setAttribute("Current_screen", "User_Portlet");
Object downloadFrom = httpSession.getAttribute("download_from");
String strDownloadFrom = PortalConstants.STRING_EMPTY;
Object oUserRole = httpSession.getAttribute(PortalConstants.USER_ROLE);
Object objUserId = httpSession.getAttribute(PortalConstants.USER_ID);
int iUserRole = 0;
long iUserId = 0;

if (oUserRole != null) {
	iUserRole = Integer.parseInt(oUserRole.toString());
}

if (objUserId != null) {
	iUserId = Long.parseLong(objUserId.toString());
}

ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
String curURL = td.getURLCurrent();

if (curURL != null && !curURL.isEmpty()) {
	if (curURL.toLowerCase().contains("saveuserslistview")) {
		pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_COL);
		pSession.removeAttribute(PortalConstants.PARAM_ORDER_BY_TYPE);
		request.getSession().removeAttribute("searchedUser");
	}
}

Object oFromSearch = pSession.getAttribute(PortalConstants.IS_SEARCHED);
boolean bIsFromSearch = false;
boolean bRoleSelected = false;
boolean bIsSearched = false;
boolean bNoUserId = false;
boolean bNoUserName = false;
boolean bNoMailAddress = false;
boolean bNoGroupName = false;
boolean bNoExpiryLow = false;
boolean bNoExpiryHigh = false;
boolean bNoLastLoginLow = false;
boolean bNoLastLoginHigh = false;
boolean bNoAccountLock = false;
boolean bNoUserList = false;
boolean bShowPaginationError = false;
boolean bHasSearchInput = false;

String strUserId = null;
String strUserName = null;
String strGroupName = null;
String strExpiryLow = null;
String strExpiryHigh = null;
String strLastLoginLow = null;
String strLastLoginHigh = null;
String strGroup = null;
String[] valueArr = null;
String strMailAdd = null;

Date dteExpiryLow = null;
Date dteExpiryHigh = null;
Date dteLastLoginLow = null;
Date dteLastLoginHigh = null;
Date tempDate = null;

int iAccountLock = 0;
int hookDelta = PortalConstants.PAGINATION_DELTA;
int start = 0;
int end = hookDelta;
int curPage = 1;
int userListTotal = 0;

String strSearchInfo = PortalConstants.STRING_EMPTY;
String strSearchInfoConnector = PortalConstants.STRING_EMPTY;
String strSearchInfoComma = LanguageUtil.get(request, PortalConstants.KEY_FILTER_LIST_COMMA);
Object param = null;
Map<String, String[]> params = null; 
List<Organization> tempOrg = null;
List<UserItem> removeList = new ArrayList<UserItem>();

Map<String, Object> searchedUser = new HashMap<String, Object>();

if (oFromSearch != null && Boolean.parseBoolean(oFromSearch.toString())) {
	bIsFromSearch = true;
	
	param = pSession.getAttribute("group_name", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("group_name", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strGroupName = (String) param;
		currentURLObj.setParameter("group_name", strGroupName);
		
		if (strGroupName != null && !strGroupName.isEmpty()) {
			searchedUser.put("group_name", strGroupName);
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_GROUP_NAME, strGroupName, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	param = pSession.getAttribute("user_id", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("user_id", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strUserId = (String) param;
		currentURLObj.setParameter("user_id", strUserId);
		
		if (strUserId != null && !strUserId.isEmpty()) {
			searchedUser.put("user_id", strUserId);
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_USER_ID, strUserId, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	param = pSession.getAttribute("user_name", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("user_name", PortletSession.PORTLET_SCOPE);
	currentURLObj.setParameter("user_name", strUserName);
	if (Validator.isNotNull(param)) {
		strUserName = (String) param;
		currentURLObj.setParameter("user_name", strUserName);
		
		if (strUserName != null && !strUserName.isEmpty()) {
			searchedUser.put("user_name", strUserName);
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_USERNAME, strUserName, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	param = pSession.getAttribute("mail_address", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("mail_address", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strMailAdd = (String) param;
		currentURLObj.setParameter("mail_address", strMailAdd);
		
		if (strMailAdd != null && !strMailAdd.isEmpty()) {
			searchedUser.put("mail_address", strMailAdd);
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_MAIL_ADDRESS, strMailAdd, false);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	param = pSession.getAttribute("lastLoginLow", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("lastLoginLow", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strLastLoginLow = param.toString();
		if (!strLastLoginLow.equalsIgnoreCase(PortalConstants.INVALID_DATE) && CommonUtil.isValidDate(strLastLoginLow)) {
			searchedUser.put("lastLoginLow", strLastLoginLow);
			dteLastLoginLow = DateUtil.parseDate("yyyy/MM/dd", strLastLoginLow, Locale.JAPAN);
			strLastLoginLow = DateUtil.getDate(dteLastLoginLow, "yyyy/MM/dd", Locale.JAPAN);
			currentURLObj.setParameter("lastLoginLow", strLastLoginLow);
		}
	}
	
	param = pSession.getAttribute("lastLoginHigh", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("lastLoginHigh", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strLastLoginHigh = param.toString();
		
		if (!strLastLoginHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE) && CommonUtil.isValidDate(strLastLoginHigh)) {
			searchedUser.put("lastLoginHigh", strLastLoginHigh);
			dteLastLoginHigh = DateUtil.parseDate("yyyy/MM/dd", strLastLoginHigh, Locale.JAPAN);
			strLastLoginHigh = DateUtil.getDate(dteLastLoginHigh, "yyyy/MM/dd", Locale.JAPAN);
			currentURLObj.setParameter("lastLoginHigh", strLastLoginHigh);
		}
	}
	
	if ((strLastLoginLow != null && !strLastLoginLow.isEmpty())
			&& (strLastLoginHigh != null && !strLastLoginHigh.isEmpty())) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_LAST_LOGIN_DATE, strLastLoginLow + PortalConstants.STRING_TILDE + strLastLoginHigh, false);
			strSearchInfoConnector = strSearchInfoComma;
	} else if ((strLastLoginLow != null && !strLastLoginLow.isEmpty())
			&& (strLastLoginHigh == null || strLastLoginHigh.isEmpty())) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_LAST_LOGIN_DATE, strLastLoginLow + PortalConstants.STRING_TILDE, false);
			strSearchInfoConnector = strSearchInfoComma;
	} else if ((strLastLoginLow == null || strLastLoginLow.isEmpty())
			&& (strLastLoginHigh != null && !strLastLoginHigh.isEmpty())) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_LAST_LOGIN_DATE, PortalConstants.STRING_TILDE + strLastLoginHigh, false);
			strSearchInfoConnector = strSearchInfoComma;
	}
	
	param = pSession.getAttribute("expiryLow", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("expiryLow", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strExpiryLow = param.toString();
		
		if (!strExpiryLow.equalsIgnoreCase(PortalConstants.INVALID_DATE) && CommonUtil.isValidDate(strExpiryLow)) {
			searchedUser.put("expiryLow", strExpiryLow);
			dteExpiryLow = DateUtil.parseDate("yyyy/MM/dd", strExpiryLow, Locale.JAPAN);
			strExpiryLow = DateUtil.getDate(dteExpiryLow, "yyyy/MM/dd", Locale.JAPAN);
			currentURLObj.setParameter("expiryLow", strExpiryLow);
		}
	}
	
	param = pSession.getAttribute("expiryHigh", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("expiryHigh", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		strExpiryHigh = param.toString();
		
		if (!strExpiryHigh.equalsIgnoreCase(PortalConstants.INVALID_DATE) && CommonUtil.isValidDate(strExpiryHigh)) {
			searchedUser.put("expiryHigh", strExpiryHigh);
			dteExpiryHigh = DateUtil.parseDate("yyyy/MM/dd", strExpiryHigh, Locale.JAPAN);
			strExpiryHigh = DateUtil.getDate(dteExpiryHigh, "yyyy/MM/dd", Locale.JAPAN);
			currentURLObj.setParameter("expiryHigh", strExpiryHigh);
		}
	}
	
	if ((strExpiryLow != null && !strExpiryLow.isEmpty())
			&& (strExpiryHigh != null && !strExpiryHigh.isEmpty())) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_EXPIRY_DATE, strExpiryLow + PortalConstants.STRING_TILDE + strExpiryHigh, false);
			strSearchInfoConnector = strSearchInfoComma;
	} else if ((strExpiryLow != null && !strExpiryLow.isEmpty())
			&& (strExpiryHigh == null || strExpiryHigh.isEmpty())) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_EXPIRY_DATE, strExpiryLow + PortalConstants.STRING_TILDE, false);
			strSearchInfoConnector = strSearchInfoComma;
	} else if ((strExpiryLow == null || strExpiryLow.isEmpty())
			&& (strExpiryHigh != null  && !strExpiryHigh.isEmpty())) {
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_EXPIRY_DATE, PortalConstants.STRING_TILDE + strExpiryHigh, false);
			strSearchInfoConnector = strSearchInfoComma;
	}
	
	param = pSession.getAttribute("account_lock", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("account_lock", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		iAccountLock = Integer.valueOf(String.valueOf(param));
		currentURLObj.setParameter("accountLock", String.valueOf(iAccountLock));
		
		if (iAccountLock != 0) {
			String strValue = "";
			switch (iAccountLock) {
				case 1:
					strValue = "label-unlock";
					break;
					
				case 2:
					strValue = "label-lock";
					break;
			}
			searchedUser.put("account_lock", iAccountLock);
			bHasSearchInput = true;
			strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_ACCOUNT_LOCK_STATE, strValue, true);
			strSearchInfoConnector = strSearchInfoComma;
		}
	}
	
	param = pSession.getAttribute("user_role", PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute("user_role", PortletSession.PORTLET_SCOPE);
	if (Validator.isNotNull(param)) {
		bRoleSelected = Boolean.parseBoolean(String.valueOf(param));
		currentURLObj.setParameter("user_role", String.valueOf(bRoleSelected));
	}
	
	currentURLObj.setParameter("isSearched", String.valueOf(String.valueOf(oFromSearch)));
	request.getSession().setAttribute("searchedUser", searchedUser);
} else {
	searchedUser = (Map<String, Object>) request.getSession().getAttribute("searchedUser");
	params = currentURLObj.getParameterMap();
	
	if (params != null && params.get("isSearched") != null) {
		valueArr = params.get("isSearched");
		bIsSearched = Boolean.parseBoolean(valueArr[0]);
	}
	if (searchedUser != null && !searchedUser.isEmpty()) {
		bIsSearched = true;
		
		Object oUserId = searchedUser.get("user_id");
		Object oUserName = searchedUser.get("user_name");
		Object oMailAddress = searchedUser.get("mail_address");
		Object oGroupName = searchedUser.get("group_name");
		Object oExpiryLow = searchedUser.get("expiryLow");
		Object oExpiryHigh = searchedUser.get("expiryHigh");
		Object oLastLoginLow = searchedUser.get("lastLoginLow");
		Object oLastLoginHigh = searchedUser.get("lastLoginHigh");
		Object oAccountLock = searchedUser.get("account_lock");
		
		if (oGroupName != null) {
			strGroupName = oGroupName.toString();
			
			if (strGroupName != null && !strGroupName.isEmpty()) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_GROUP_NAME, strGroupName, false);
				strSearchInfoConnector = strSearchInfoComma;
				currentURLObj.setParameter("group_name", strGroupName);
			}
		}
		
		if (oUserId != null) {
			strUserId = oUserId.toString();
			
			if (strUserId != null && !strUserId.isEmpty()) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_USER_ID, strUserId, false);
				strSearchInfoConnector = strSearchInfoComma;
				currentURLObj.setParameter("user_id", strUserId);
			}
		}
		
		if (oUserName != null) {
			strUserName = oUserName.toString();
			
			if (strUserName != null && !strUserName.isEmpty()) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_USERNAME, strUserName, false);
				strSearchInfoConnector = strSearchInfoComma;
				currentURLObj.setParameter("user_name", strUserName);
			}
		}
		
		if (oMailAddress != null) {
			strMailAdd = oMailAddress.toString();
			
			if (strMailAdd != null && !strMailAdd.isEmpty()) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_MAIL_ADDRESS, strMailAdd, false);
				strSearchInfoConnector = strSearchInfoComma;
				currentURLObj.setParameter("mail_address", strMailAdd);
			}
		}
		
		if (oLastLoginLow != null) {
			strLastLoginLow = valueArr[0];
			dteLastLoginLow = DateUtil.parseDate(strLastLoginLow, Locale.JAPAN);
			currentURLObj.setParameter("lastLoginLow", strLastLoginLow);
		}
		
		if (oLastLoginHigh != null) {
			strLastLoginHigh = valueArr[0];
			dteLastLoginHigh = DateUtil.parseDate(strLastLoginHigh, Locale.JAPAN);
			currentURLObj.setParameter("lastLoginHigh", strLastLoginHigh);
		}
		
		if ((strLastLoginLow != null && !strLastLoginLow.isEmpty())
				&& (strLastLoginHigh != null && !strLastLoginHigh.isEmpty())) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_LAST_LOGIN_DATE, strLastLoginLow + PortalConstants.STRING_TILDE + strLastLoginHigh, false);
				strSearchInfoConnector = strSearchInfoComma;
		} else if ((strLastLoginLow != null && !strLastLoginLow.isEmpty())
				&& (strLastLoginHigh == null || strLastLoginHigh.isEmpty())) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_LAST_LOGIN_DATE, strLastLoginLow + PortalConstants.STRING_TILDE, false);
				strSearchInfoConnector = strSearchInfoComma;
		} else if ((strLastLoginLow == null || strLastLoginLow.isEmpty())
				&& (strLastLoginHigh != null && !strLastLoginHigh.isEmpty())) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_LAST_LOGIN_DATE, PortalConstants.STRING_TILDE + strLastLoginHigh, false);
				strSearchInfoConnector = strSearchInfoComma;
		}
		
		if (oExpiryLow != null) {
			strExpiryLow = valueArr[0];
			dteExpiryLow = DateUtil.parseDate(strExpiryLow, Locale.JAPAN);
			currentURLObj.setParameter("expiryLow", strExpiryLow);
		}
		
		if (oExpiryHigh != null) {
			strExpiryHigh = valueArr[0];
			dteExpiryHigh = DateUtil.parseDate(strExpiryHigh, Locale.JAPAN);
			currentURLObj.setParameter("expiryHigh", strExpiryHigh);
		}
		
		if ((strExpiryLow != null && !strExpiryLow.isEmpty())
				&& (strExpiryHigh != null && !strExpiryHigh.isEmpty())) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_EXPIRY_DATE, strExpiryLow + PortalConstants.STRING_TILDE + strExpiryHigh, false);
				strSearchInfoConnector = strSearchInfoComma;
		} else if ((strExpiryLow != null && !strExpiryLow.isEmpty())
				&& (strExpiryHigh == null || strExpiryHigh.isEmpty())) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_EXPIRY_DATE, strExpiryLow + PortalConstants.STRING_TILDE, false);
				strSearchInfoConnector = strSearchInfoComma;
		} else if ((strExpiryLow == null || strExpiryLow.isEmpty())
				&& (strExpiryHigh != null  && !strExpiryHigh.isEmpty())) {
				bHasSearchInput = true;
				strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_EXPIRY_DATE, PortalConstants.STRING_TILDE + strExpiryHigh, false);
				strSearchInfoConnector = strSearchInfoComma;
		}
		
		if (oAccountLock != null) {
			String strAccountLock = oAccountLock.toString();
			
			if (strAccountLock != null && !strAccountLock.isEmpty()) {
				iAccountLock = Integer.valueOf(strAccountLock);
				
				if (iAccountLock != 0) {
					String strValue = "";
					switch (iAccountLock) {
						case 1:
							strValue = "label-unlock";
							break;
							
						case 2:
							strValue = "label-lock";
							break;
					}
					bHasSearchInput = true;
					strSearchInfo += strSearchInfoConnector + LanguageUtil.format(request, PortalConstants.KEY_FILTER_BY_ACCOUNT_LOCK_STATE, strValue, true);
					strSearchInfoConnector = strSearchInfoComma;
					currentURLObj.setParameter("accountLock", strAccountLock);
				}
			}
		}
	}
}

params = null;
valueArr = null;

String backURL = GetterUtil.getString(request.getAttribute("view.jsp-backURL"));
int inactiveUsersCount = GetterUtil.getInteger(request.getAttribute("view.jsp-inactiveUsersCount"));
PortletURL portletURL = (PortletURL)request.getAttribute("view.jsp-portletURL");
int status = GetterUtil.getInteger(request.getAttribute("view.jsp-status"));
String usersListView = GetterUtil.getString(request.getAttribute("view.jsp-usersListView"));
String viewUsersRedirect = GetterUtil.getString(request.getAttribute("view.jsp-viewUsersRedirect"));

SearchContainer searchContainer = new UserSearch(renderRequest, "cur", currentURLObj);

UserSearchTerms searchTerms = (UserSearchTerms)searchContainer.getSearchTerms();

boolean hasAddUserPermission = PortalPermissionUtil.contains(permissionChecker, ActionKeys.ADD_USER);

if (!searchTerms.isSearch() && hasAddUserPermission) {
	searchContainer.setEmptyResultsMessageCssClass("taglib-empty-result-message-header-has-plus-btn");
}

if (!ParamUtil.getBoolean(renderRequest, "advancedSearch")) {
	currentURLObj.setParameter("status", String.valueOf(status));
}

String orderByCol = ParamUtil.getString(request, "orderByCol");
String orderByType = ParamUtil.getString(request, "orderByType");

searchContainer.setOrderByCol(orderByCol);

if (orderByType == null || orderByType.isEmpty()) {
	Object oOrderByType = pSession.getAttribute("orderByType");
	
	if (oOrderByType != null) {
		orderByType = oOrderByType.toString();
		
		if (orderByType.isEmpty()) {
			searchContainer.setOrderByType("asc");
		} else {
			searchContainer.setOrderByType(orderByType);
		}
	}
	
	Object oOrderByCol = pSession.getAttribute("orderByCol");
	
	if (oOrderByCol != null) {
		orderByCol = oOrderByCol.toString();
		
		if (!orderByCol.isEmpty()) {
			searchContainer.setOrderByCol(orderByCol);
			pSession.setAttribute("orderByCol", orderByCol);
		}
	}
} else {
	searchContainer.setOrderByType(orderByType);
	pSession.setAttribute("orderByType", orderByType);
	pSession.setAttribute("orderByCol", orderByCol);
}

String displayStyle = ParamUtil.getString(request, "displayStyle", "list");
String navigation = ParamUtil.getString(request, "navigation", "active");
String toolbarItem = ParamUtil.getString(request, "toolbarItem", "view-all-users");

if (navigation.equals("active")) {
	status = WorkflowConstants.STATUS_APPROVED;
	searchTerms.setStatus(WorkflowConstants.STATUS_APPROVED);
}
else if (navigation.equals("inactive")) {
	status = WorkflowConstants.STATUS_INACTIVE;
	searchTerms.setStatus(WorkflowConstants.STATUS_INACTIVE);
}

portletURL.setParameter("navigation", navigation);
portletURL.setParameter("status", String.valueOf(status));

boolean showDeleteButton = (searchTerms.getStatus() != WorkflowConstants.STATUS_ANY) && (searchTerms.isActive() || (!searchTerms.isActive() && PropsValues.USERS_DELETE));
boolean showRestoreButton = (searchTerms.getStatus() != WorkflowConstants.STATUS_ANY) && !searchTerms.isActive();

Object oManualDownloadError = httpSession.getAttribute(PortalConstants.ERROR);
String strManualDownloadError = PortalConstants.STRING_EMPTY;

if (!CommonUtil.isObjectNull(oManualDownloadError)) {
	strManualDownloadError = oManualDownloadError.toString();
}

if (!CommonUtil.isObjectNull(downloadFrom)){
	strDownloadFrom = downloadFrom.toString();
}

%>

<aui:form action="<%= portletURL.toString() %>" cssClass="container-fluid-1280 control-panel-form" method="post" name="fm" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "search();" %>'>
	<liferay-portlet:renderURLParams varImpl="portletURL" />
	<aui:input name="<%= Constants.CMD %>" type="hidden" />
	<aui:input name="toolbarItem" type="hidden" value="<%= toolbarItem %>" />
	<aui:input name="redirect" type="hidden" value="<%= portletURL.toString() %>" />
	<aui:input name="backURL" type="hidden" value="<%= portletURL.toString() %>" />

	<c:if test="<%= Validator.isNotNull(viewUsersRedirect) %>">
		<aui:input name="viewUsersRedirect" type="hidden" value="<%= viewUsersRedirect %>" />
	</c:if>

	<liferay-ui:search-container
		cssClass="users-search-container"
		id="users"
		searchContainer="<%= searchContainer %>"
		var="userSearchContainer"
		emptyResultsMessage="error-no-users"
	>
		<%-- <aui:input disabled="<%= true %>" name="usersRedirect" type="hidden" value="<%= currentURLObj.toString() %>" /> --%>
		<aui:input name="deleteUserIds" type="hidden" />
		<aui:input name="status" type="hidden" value="<%= status %>" />

		<%
		if ((searchTerms.getOrganizationId() > 0) && !OrganizationPermissionUtil.contains(permissionChecker, searchTerms.getOrganizationId(), ActionKeys.MANAGE_USERS)) {
			inactiveUsersCount = 0;

			status = WorkflowConstants.STATUS_APPROVED;
		}

		UserDisplayTerms displayTerms = (UserDisplayTerms)userSearchContainer.getDisplayTerms();

		if (!searchTerms.isAdvancedSearch()) {
			if (status == WorkflowConstants.STATUS_APPROVED) {
				displayTerms.setStatus(WorkflowConstants.STATUS_APPROVED);
				searchTerms.setStatus(WorkflowConstants.STATUS_APPROVED);
			}
			else {
				displayTerms.setStatus(WorkflowConstants.STATUS_INACTIVE);
				searchTerms.setStatus(WorkflowConstants.STATUS_INACTIVE);
			}
		}

		long userOrganizationId = searchTerms.getOrganizationId();
		long roleId = searchTerms.getRoleId();
		long userGroupId = searchTerms.getUserGroupId();

		Organization userOrganization = null;

		if (userOrganizationId > 0) {
			try {
				userOrganization = OrganizationLocalServiceUtil.getOrganization(userOrganizationId);

				userSearchContainer.setEmptyResultsMessage("this-organization-does-not-have-any-users");
			}
			catch (NoSuchOrganizationException nsoe) {
			}
		}

		Role role = null;

		if (roleId > 0) {
			try {
				role = RoleLocalServiceUtil.getRole(roleId);
			}
			catch (NoSuchRoleException nsre) {
			}
		}

		UserGroup userGroup = null;

		if (userGroupId > 0) {
			try {
				userGroup = UserGroupLocalServiceUtil.getUserGroup(userGroupId);
			}
			catch (NoSuchUserGroupException nsuge) {
			}
		}

		if (role != null) {
			PortalUtil.addPortletBreadcrumbEntry(request, role.getName(), null);
			PortalUtil.addPortletBreadcrumbEntry(request, LanguageUtil.get(request, "all-users"), currentURL);
		}

		if (userGroup != null) {
			PortalUtil.addPortletBreadcrumbEntry(request, userGroup.getName(), null);
			PortalUtil.addPortletBreadcrumbEntry(request, LanguageUtil.get(request, "all-users"), currentURL);
		}
		%>

		<c:if test="<%= usersListView.equals(UserConstants.LIST_VIEW_FLAT_USERS) && (role == null) && (userGroup == null) %>">

			<%
			if (userOrganization != null) {
				UsersAdminUtil.addPortletBreadcrumbEntries(userOrganization, request, renderResponse);

				PortalUtil.addPortletBreadcrumbEntry(request, LanguageUtil.get(request, "all-users"), currentURL);
			}
			%>
			
			<div style="color: rgb(59, 137, 175); font-weight: bold; font-size: 9px; height: 4px;">
				<liferay-ui:message key="header-account-list" />
			</div>
			
			<div style="height: 8px;">
				<hr style="height: 2px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
			</div>
			
			<div style="margin:0px;">
				<liferay-ui:success key="successDelete" message="success-user-delete" />
				<liferay-ui:success key="successAdd" message="success-user-add" />
				<liferay-ui:success key="successEdit" message="success-user-edit" />
			</div>

			<c:if test="<%= layout.isTypeControlPanel() %>">
				<div id="breadcrumb">
					<liferay-ui:breadcrumb showCurrentGroup="<%= false %>" showGuestGroup="<%= false %>" showLayout="<%= false %>" showPortletBreadcrumb="<%= true %>" />
				</div>
			</c:if>
		</c:if>

		<c:if test="<%= userOrganization != null %>">
			<aui:input name="<%= UserDisplayTerms.ORGANIZATION_ID %>" type="hidden" value="<%= userOrganization.getOrganizationId() %>" />

			<c:if test="<%= usersListView.equals(UserConstants.LIST_VIEW_FLAT_USERS) %>">

				<%
				portletDisplay.setShowBackIcon(true);
				portletDisplay.setURLBack(backURL);

				renderResponse.setTitle(userOrganization.getName());
				%>

			</c:if>
		</c:if>

		<c:if test="<%= role != null %>">
			<aui:input name="<%= UserDisplayTerms.ROLE_ID %>" type="hidden" value="<%= role.getRoleId() %>" />

			<%
			portletDisplay.setShowBackIcon(true);
			portletDisplay.setURLBack(backURL);

			renderResponse.setTitle(role.getTitle(locale));
			%>

		</c:if>

		<c:if test="<%= userGroup != null %>">
			<aui:input name="<%= UserDisplayTerms.USER_GROUP_ID %>" type="hidden" value="<%= userGroup.getUserGroupId() %>" />

			<%
			portletDisplay.setShowBackIcon(true);
			portletDisplay.setURLBack(backURL);

			renderResponse.setTitle(userGroup.getName());
			%>

		</c:if>

		<%
		LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();

		if (userOrganizationId > 0) {
			userParams.put("usersOrgs", Long.valueOf(userOrganizationId));
		}
		else if (usersListView.equals(UserConstants.LIST_VIEW_TREE) && Validator.isNull(searchTerms.getKeywords())) {
			userParams.put("noOrganizations", Boolean.TRUE);
			userParams.put("usersOrgsCount", 0);
		}
		else {
			if (filterManageableOrganizations && !UserPermissionUtil.contains(permissionChecker, ResourceConstants.PRIMKEY_DNE, ActionKeys.VIEW)) {
				long[] organizationIds = user.getOrganizationIds();

				if (ArrayUtil.isEmpty(organizationIds)) {
					organizationIds = new long[] {0};
				}

				userParams.put("usersOrgs", ArrayUtil.toLongArray(organizationIds));
			}
		}

		if (roleId > 0) {
			userParams.put("usersRoles", Long.valueOf(roleId));
		}

		if (userGroupId > 0) {
			userParams.put("usersUserGroups", Long.valueOf(userGroupId));
		}
		
		if (bShowDBConnError) {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="error-orm-exception" />
			</div>
			<%
		} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL) && strDownloadFrom.equals("User_Portlet")) {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
			</div>
		<%
			httpSession.removeAttribute(PortalConstants.ERROR);
		} else if (strManualDownloadError.equals(PortalErrors.DOWNLOAD_MANUAL_FAILED) && strDownloadFrom.equals("User_Portlet")) {
			%>
				<div class="alert alert-danger">
					<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
				</div>
			<%
			httpSession.removeAttribute(PortalConstants.ERROR);
		}
		
		List<User> userLists = null;
		userLists = (List<User>)pSession.getAttribute("userLists", PortletSession.PORTLET_SCOPE);
		pSession.removeAttribute("userLists", PortletSession.PORTLET_SCOPE);
		
		if (Validator.isNull(userLists)) {
			bNoUserList = true;

			if (request.getParameter("cur") == null) {
				curPage = 1;
			} else {
				curPage = Integer.parseInt(request.getParameter("cur").toString());
			}

			start = (curPage - 1) * hookDelta;
			end = curPage * hookDelta;
			
			List<Role> rolesList 		= RoleLocalServiceUtil.getRoles(0, RoleLocalServiceUtil.getRolesCount());
			List<User> tempUserList 	= null;
			List<User> tempUserList2 	= new ArrayList<User>();
			userListTotal 				= 0; 
			userLists 					= null;
			
			for (Role r : rolesList) {
				if (r.getName().equals("System Administrator") && (iUserRole == 5)) { 
					userListTotal += UserLocalServiceUtil.getRoleUsersCount(r.getRoleId());
					userLists = UserLocalServiceUtil.getRoleUsers(r.getRoleId(), 0, userListTotal);
					break;
				} else if (r.getName().equals("Overall Administrator") && (iUserRole == 1)) {
					userListTotal += UserLocalServiceUtil.getRoleUsersCount(r.getRoleId()); 
					userLists = UserLocalServiceUtil.getRoleUsers(r.getRoleId(), 0, userListTotal);
					break;
				} else if ((r.getName().equals("General User") || r.getName().equals("Group Administrator")) && (iUserRole == 2)) {
					userListTotal += UserLocalServiceUtil.getRoleUsersCount(r.getRoleId()); 
					
					tempUserList = UserLocalServiceUtil.getRoleUsers(r.getRoleId(), 0, userListTotal);
					
					if(userLists == null){
						userLists = tempUserList;
					}else{
						userLists.addAll(tempUserList);
					}
				} else if (r.getName().equals("General User") && (iUserRole == 3)) {
					userListTotal += UserLocalServiceUtil.getRoleUsersCount(r.getRoleId()); 
					
					tempUserList = null;
					
					List<Organization> organizationList = OrganizationLocalServiceUtil.getUserOrganizations(iUserId);
					
					for (Organization o : organizationList)
					{
						tempUserList = UserLocalServiceUtil.getOrganizationUsers(o.getOrganizationId());
						
						if(userLists == null){
							userLists = tempUserList;
						}else{
							userLists.addAll(tempUserList);
						}
					}
					
					if(userLists != null){
						// create has set. Set will contains only unique objects
					 	HashSet<User> hashSet = new HashSet(userLists);
						for (User user_item : hashSet) {
				       		tempUserList2.add(user_item);
				       	}
						
						userLists = tempUserList2;
					}
					
					break;
				}
			}
		}
		
		searchedUser = (Map<String, Object>) request.getSession().getAttribute("searchedUser");
		if (searchedUser != null && !searchedUser.isEmpty()) {
			bIsSearched = true;
			
			Object oUserId = searchedUser.get("user_id");
			Object oUserName = searchedUser.get("user_name");
			Object oMailAddress = searchedUser.get("mail_address");
			Object oGroupName = searchedUser.get("group_name");
			Object oExpiryLow = searchedUser.get("expiryLow");
			Object oExpiryHigh = searchedUser.get("expiryHigh");
			Object oLastLoginLow = searchedUser.get("lastLoginLow");
			Object oLastLoginHigh = searchedUser.get("lastLoginHigh");
			Object oAccountLock = searchedUser.get("account_lock");
			
			if (oGroupName != null) {
				strGroupName = oGroupName.toString();
				
				currentURLObj.setParameter("group_name", strGroupName);
			}
			
			if (oUserId != null) {
				strUserId = oUserId.toString();
				
				currentURLObj.setParameter("user_id", strUserId);
			}
			
			if (oUserName != null) {
				strUserName = oUserName.toString();
				
				currentURLObj.setParameter("user_name", strUserName);
			}
			
			if (oMailAddress != null) {
				strMailAdd = oMailAddress.toString();
				
				currentURLObj.setParameter("mail_address", strMailAdd);
			}
			
			if (oLastLoginLow != null) {
				dteLastLoginLow = DateUtil.parseDate("yyyy/MM/dd", oLastLoginLow.toString(), Locale.JAPAN);
				strLastLoginLow = DateUtil.getDate(dteLastLoginLow, "yyyy/MM/dd", Locale.JAPAN);
				
				currentURLObj.setParameter("lastLoginLow", strLastLoginLow);
			}
			
			if (oLastLoginHigh != null) {
				dteLastLoginHigh = DateUtil.parseDate("yyyy/MM/dd", oLastLoginHigh.toString(), Locale.JAPAN);
				strLastLoginHigh = DateUtil.getDate(dteLastLoginHigh, "yyyy/MM/dd", Locale.JAPAN);
				
				currentURLObj.setParameter("lastLoginHigh", strLastLoginHigh);
			}
			
			if (oExpiryLow != null) {
				dteExpiryLow = DateUtil.parseDate("yyyy/MM/dd", oExpiryLow.toString(), Locale.JAPAN);
				strExpiryLow = DateUtil.getDate(dteExpiryLow, "yyyy/MM/dd", Locale.JAPAN);
				
				currentURLObj.setParameter("expiryLow", strExpiryLow);
			}
			
			if (oExpiryHigh != null) {
				dteExpiryHigh = DateUtil.parseDate("yyyy/MM/dd", oExpiryHigh.toString(), Locale.JAPAN);
				strExpiryHigh = DateUtil.getDate(dteExpiryHigh, "yyyy/MM/dd", Locale.JAPAN);
				
				currentURLObj.setParameter("expiryHigh", strExpiryHigh);
			}
			
			if (oAccountLock != null) {
				iAccountLock = Integer.parseInt(oAccountLock.toString());
				
				currentURLObj.setParameter("accountLock", oAccountLock.toString());
			}
		} else {
			currentURLObj.removePublicRenderParameter("group_name");
			currentURLObj.removePublicRenderParameter("user_id");
			currentURLObj.removePublicRenderParameter("user_name");
			currentURLObj.removePublicRenderParameter("mail_address");
			currentURLObj.removePublicRenderParameter("lastLoginLow");
			currentURLObj.removePublicRenderParameter("lastLoginHigh");
			currentURLObj.removePublicRenderParameter("expiryLow");
			currentURLObj.removePublicRenderParameter("expiryHigh");
			currentURLObj.removePublicRenderParameter("accountLock");
		}
		
		List<UserItem> userList = null;
		
		if (!Validator.isNull(userLists)) {
			userList = new ArrayList<UserItem>();
			for (User usr : userLists) {
				UserItem userItem = new UserItem();
				
				List<Organization> org = OrganizationLocalServiceUtil.getUserOrganizations(usr.getUserId());
				String strOrg = "";
				
				if (org.size() > 0) {
					if(org.size() == 1){
						strOrg = org.get(0).getName();
					}else{
						for(Organization o : org){
							String temp = o.getName() + ", ";
							strOrg = strOrg.concat(temp);
						}
					}
				} else {
					strOrg = "";
				}
				
				userItem.setGroupName(strOrg);
				userItem.setuId(usr.getUserId());
				userItem.setUsername(usr.getFirstName());
				userItem.setUserId(usr.getEmailAddress());
				
				String strMail = GetterUtil.getString(ExpandoValueLocalServiceUtil.getData(usr.getCompanyId(),
						User.class.getName(), "CUSTOM_FIELDS", "emailAddress", usr.getUserId()));
				
				userItem.setEmailAddress(strMail);

				List<Role> roles = usr.getRoles();

				for(Role r: roles){
					userItem.setRole(r.getName());
				}
					
				DateFormat formatt = new SimpleDateFormat("yyyy/MM/dd HH:mm");
				formatt.setTimeZone(TimeZone.getTimeZone("Asia/Tokyo"));
				Date dteLastLoginDate = null;
				String lastLogindate = "";
				if(null != usr.getLastLoginDate()){
					dteLastLoginDate = usr.getLastLoginDate();
					lastLogindate = formatt.format(dteLastLoginDate);
				}
				
				userItem.setLastLoginDate(lastLogindate);
				
				formatt = new SimpleDateFormat("yyyy/MM/dd");
				
				Date dteExpirationDate = null;
				
				if (!bShowDBConnError) {
					dteExpirationDate = GetterUtil.getDate(usr.getExpandoBridge().getAttribute("acctexpirationdate"), formatt);
				}
				
				Date dteDefault = new Date(0);
				
				String strExpirationDate = "";
				if (dteExpirationDate != null && (!dteExpirationDate.equals(dteDefault))){
					strExpirationDate = formatt.format(dteExpirationDate);
				}
				
				userItem.setExpiryDate(strExpirationDate);
				userItem.setAccountLock(usr.getLockout());
				
				userList.add(userItem);
			}
		}
		if (userList == null) {
			userList = new ArrayList<UserItem>();
		}
		
		List<UserItem> sortableUsers = new ArrayList<UserItem>(userList);
		
		if (Validator.isNotNull(orderByCol) && !orderByCol.isEmpty()) {
			if (orderByCol.equalsIgnoreCase("groupName")) {
				Collections.sort(sortableUsers, new Comparator<UserItem>() {
					@Override
					public int compare(UserItem u1, UserItem u2) {
						int iCompare = 0;
						
						iCompare = u1.getGroupName().compareToIgnoreCase(u2.getGroupName());
							
					 	return iCompare; 
					}
				});
			} else if (orderByCol.equalsIgnoreCase("username")) {
				Collections.sort(sortableUsers, new Comparator<UserItem>() {
					@Override
					public int compare(UserItem u1, UserItem u2) {
						int iCompare = 0;
						
						iCompare = u1.getUsername().compareToIgnoreCase(u2.getUsername());
					 	
						return iCompare; 
					}
				});
			} else if (orderByCol.equalsIgnoreCase("emailAddress")) {
				Collections.sort(sortableUsers, new Comparator<UserItem>() {
					@Override
					public int compare(UserItem u1, UserItem u2) {
						int iCompare = 0;
						
						iCompare = u1.getEmailAddress().compareToIgnoreCase(u2.getEmailAddress());
					 	
						return iCompare; 
					}
				});
			} else {
				BeanComparator comparator = new BeanComparator(orderByCol);
				Collections.sort(sortableUsers, comparator);
			}
			
			if (orderByType.equalsIgnoreCase("desc")) {
				Collections.reverse(sortableUsers);
			}
		}
		
		userList = new ArrayList<UserItem>();
		userList = sortableUsers;

		if(bIsSearched && !userList.isEmpty()) {
			try {
				if (Validator.isNotNull(strUserId) && !strUserId.isEmpty()) {
					for (UserItem tempUser : userList) {
						if (!tempUser.getUserId().toLowerCase().contains(strUserId.toLowerCase())) {
							removeList.add(tempUser);
						}
					}
				}
				
				if (Validator.isNotNull(strUserName) && !strUserName.isEmpty()) {
					for (UserItem tempUser : userList) {
						if (!tempUser.getUsername().toLowerCase().contains(strUserName.toLowerCase())) {
							removeList.add(tempUser);
						}
					}
				}
				
				if (Validator.isNotNull(strMailAdd) && !strMailAdd.isEmpty()) {
					for (UserItem tempUser : userList) {
						if (!tempUser.getEmailAddress().toLowerCase().contains(strMailAdd.toLowerCase())) {
							removeList.add(tempUser);
						}
					}
				}

				if (Validator.isNotNull(strGroupName) && !strGroupName.isEmpty()) {
					for (UserItem tempUser : userList) {
						if (!tempUser.getGroupName().toLowerCase().contains(strGroupName.toLowerCase())) {
							removeList.add(tempUser);
						}
					}
				}
				
				String strDefaultDate = "1970/01/01";
				Date defaultDate = DateUtil.parseDate(strDefaultDate, Locale.JAPAN);
				
				if (Validator.isNotNull(strLastLoginLow) && !strLastLoginLow.isEmpty()) {
					for (UserItem tempUser : userList) {
						tempDate = DateUtil.parseDate(strLastLoginLow, Locale.JAPAN);
						String lastLoginDate = tempUser.getLastLoginDate();
						
						if ((lastLoginDate != null && !lastLoginDate.isEmpty())
								&& tempDate.after(DateUtil.parseDate(lastLoginDate, Locale.JAPAN))) {
							removeList.add(tempUser);
						}
					}
				}
				
				if (Validator.isNotNull(strLastLoginHigh) && !strLastLoginHigh.isEmpty()) {
					for (UserItem tempUser : userList) {
						tempDate = DateUtil.parseDate(strLastLoginHigh, Locale.JAPAN);
						tempDate.setHours(23);
						tempDate.setMinutes(59);
						tempDate.setSeconds(59);
						String lastLoginDate = tempUser.getLastLoginDate();
						
						if ((lastLoginDate != null && !lastLoginDate.isEmpty())
								&& tempDate.before(DateUtil.parseDate(lastLoginDate, Locale.JAPAN))) {
							removeList.add(tempUser);
						}
					}
				}
				
				if (Validator.isNotNull(strExpiryLow) && !strExpiryLow.isEmpty()) {
					for (UserItem tempUser : userList) {
						tempDate = DateUtil.parseDate(strExpiryLow, Locale.JAPAN);
						String expiryDate = tempUser.getExpiryDate();
						
						if (!tempUser.getExpiryDate().equals(defaultDate)
								&& (expiryDate != null && !expiryDate.isEmpty())
								&& tempDate.after(DateUtil.parseDate(expiryDate, Locale.JAPAN))) {
							removeList.add(tempUser);
						}
					}
				}
				
				if (Validator.isNotNull(strExpiryHigh) && !strExpiryHigh.isEmpty()) {
					for (UserItem tempUser : userList) {
						tempDate = DateUtil.parseDate(strExpiryHigh, Locale.JAPAN);
						String expiryDate = tempUser.getExpiryDate();
						
						if (tempUser.getExpiryDate().equals(defaultDate)
								|| ((expiryDate != null && !expiryDate.isEmpty())
										&& tempDate.before(DateUtil.parseDate(expiryDate, Locale.JAPAN)))) {
							removeList.add(tempUser);
						}
					}
				}
				
				if (Validator.isNotNull(iAccountLock)) {
					for (UserItem tempUser : userList) {
						if ((iAccountLock == 1 && tempUser.isAccountLock()) || (iAccountLock == 2 && !tempUser.isAccountLock())) {
							removeList.add(tempUser);
						}
					}
				}
				
				if (!removeList.isEmpty()) {
					userList.removeAll(removeList);
				}
				
				userListTotal = userList.size();
				
			} catch (Exception e){
				
			}
		}
		
		removeList = null;
		tempOrg = null;

		int size = ListUtil.subList(userList, start, end).size();
		if (size == 0 && Validator.isNotNull(userList) && !userList.isEmpty()) {
			--curPage;
			start = (curPage - 1) * hookDelta;
			end = curPage * hookDelta;
			request.setAttribute("index-overlap", "");
			
			if (SessionErrors.isEmpty(renderRequest) && SessionMessages.isEmpty(renderRequest)) {
				bShowPaginationError = true;
			}
		}
		
		int iRole;
		iRole = Integer.parseInt(oUserRole.toString());
		
		if (bShowPaginationError) {
		%>
			<div class="alert alert-danger">
				<liferay-ui:message key="next-pagination-error" />
			</div>
		<%
		}
		%>
		
	 	<liferay-ui:error exception="<%= UserScreenNameException.class %>" message="error-search-username-too-long" />
		<liferay-ui:error exception="<%= UserEmailAddressException.class %>" message="error-search-user-id-too-long" /> 
		<liferay-ui:error exception="<%= UBSPortalUserEmailAddressException.MustNotExceedMaximumBytes.class %>" message="error-search-user-id-too-long" />
		<liferay-ui:error exception="<%= UBSPortalUserScreenNameException.MustNotExceedMaximumBytes.class %>" message="error-search-username-too-long" />
		<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="error-user-does-not-exist" />
		<liferay-ui:error key="ORMException" message="error-orm-exception" />
		<liferay-ui:error key="error-email-address-invalid" message="error-email-address-invalid" />
		<liferay-ui:error key="error-expiration-date-invalid" message="error-expiration-date-invalid" />
		<liferay-ui:error key="error-last-login-date-invalid" message="error-last-login-date-invalid" />

		<%
			String [] current = currentURL.split("_125_");
			
			String redirect = "";
			
			if (current.length > 0) {
				for (int i = 0; i < current.length; i++) {
					if (!current[i].contains("usersSearchContainerPrimaryKeys")) {
						if (i == 0) {
							redirect += current[i];
						} else {
							redirect += "_125_" + current[i];
						}
					}
				}
			}
		%>
		
		<portlet:renderURL var="addUserURL">
			<portlet:param name="mvcRenderCommandName" value="/users_admin/edit_user" />
			<portlet:param name="redirect" value="<%= redirect %>" />
		</portlet:renderURL>
		
		<aui:button name="editProjectBtn" value="button-new-registration" href="<%= addUserURL %>" />
				
		<div style="height: 15px; padding-top: -10px;">
			<hr style="height: 1px; color: rgb(132, 144, 156); background-color: rgb(132, 144, 156);">
		</div>
		<aui:input disabled="<%= true %>" name="usersRedirect" type="hidden" value="<%= currentURLObj.toString() %>" />
		
		<%
			String taglibSearchURL = "javascript:" + renderResponse.getNamespace() + "searchUser();";
			String taglibClearSearchURL = "javascript:" + renderResponse.getNamespace() + "clearSearchUser();";
		%>
		
		<aui:button-row>
			<%
			if ((bIsFromSearch || bIsSearched) && bHasSearchInput) {
				%>
				<button type="button" class="btn" onClick="<%= taglibClearSearchURL %>" style="width:150px; color: #404040; float: right;"><liferay-ui:message key="button-clear-filter" /></button>
				<%
			} else {
				%>
				<aui:button style="width:150px; color: #404040; float:right;" value="button-clear-filter" disabled="true" />
				<%
			}
			%>
			<a data-toggle="modal" data-target="#userFilterModal"><span class="btn" style="width:120px; color:#404040; float:right; margin-right:5px;"><liferay-ui:message key="button-filter" /></span></a>
		</aui:button-row>
		
		<div class="modal fade" id="userFilterModal" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false" style="width: auto; display: none;">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<liferay-ui:message key="header-user-filter" />
					</div>
					
					<div class="modal-body">
						<table>
							<tr>
								<td class="filter-modal-label"><liferay-ui:message key="label-group-name" /></td>
								<td class="filter-modal-input">
								<%
									boolean bDisabled = false;
								
									if (iRole == 5 || iRole == 1) {
										bDisabled = true;
									}
								%>
									<aui:input type="hidden" name="selectedGroup" value="<%= strGroupName %>" />
									<aui:select disabled="<%= bDisabled %>" name="group_name" label="" cssClass="clearable">
										<%
										int totalOrganization = 0;
										List<Organization> organizationList = null;
										if (!bShowDBConnError) {
											totalOrganization = OrganizationLocalServiceUtil.getOrganizationsCount();
											
											if (iUserRole == 3) {
												organizationList = OrganizationLocalServiceUtil.getUserOrganizations(iUserId);
											} else {
												organizationList = OrganizationLocalServiceUtil.getOrganizations(0, totalOrganization);
											}
										}
										
										%>
											<aui:option></aui:option>
										<%
										if (organizationList != null) {
											for(Organization o: organizationList){
										%>
												
										<c:choose>
											<c:when test="<%= o.getName().equals(strGroupName)%>">
												<aui:option value="<%= HtmlUtil.escapeAttribute(o.getName()) %>" selected="<%= true %>">
													<%= HtmlUtil.escapeAttribute(o.getName()) %>
												</aui:option>
											</c:when>
											<c:otherwise>
												<aui:option value="<%= HtmlUtil.escapeAttribute(o.getName()) %>" >
													<%= HtmlUtil.escapeAttribute(o.getName()) %>
												</aui:option>
											</c:otherwise>
										</c:choose>
										<%	
											}
										}
										%>
								 	</aui:select>	
									
								</td>
							</tr>
							
							<tr>
								<td class="filter-modal-label"><liferay-ui:message key="label-user-id" /></td>
								<td><aui:input type="text" name="user_id" label="" value="<%= strUserId %>"/></td>
							</tr>
							
							<tr>
								<td class="filter-modal-label"><liferay-ui:message key="label-user-name" /></td>
								<td><aui:input type="text" name="user_name" label="" value="<%= strUserName %>" /></td>
							</tr>
							
							<tr>
								<td class="filter-modal-label"><liferay-ui:message key="label-mail-address" /></td>
								<td><aui:input type="text" name="mail_address" label="" value="<%= strMailAdd %>"/></td>
							</tr>
							
							<tr>
								<td class="filter-modal-label"><liferay-ui:message key="label-last-log-in-date" /></td>
								<td>
									<%
										int year = 0;
										int month = -1;
										int day = 0;
										
										if (dteLastLoginLow != null) {
											year = dteLastLoginLow.getYear() + 1900;
											month = dteLastLoginLow.getMonth();
											day = dteLastLoginLow.getDate();
										}
									%>
									<div class="form-group date-input-text">
										<liferay-ui:input-date name="lastLoginDateLow" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= day %>" />
									</div>
								</td>
								<td class="date-hyphen">~</td>
								<td>
									<%
										year = 0;
										month = -1;
										day = 0;
										
										if (dteLastLoginHigh != null) {
											year = dteLastLoginHigh.getYear() + 1900;
											month = dteLastLoginHigh.getMonth();
											day = dteLastLoginHigh.getDate();
										}
									%>
									<div class="form-group date-input-text">
										<liferay-ui:input-date name="lastLoginDateHigh" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= day %>" />
									</div>
								</td>
							</tr>
							
							<tr>
								<td class="filter-modal-label"><liferay-ui:message key="label-expiration-date" /></td>
								<td>
									<%
										year = 0;
										month = -1;
										day = 0;
										
										if (dteExpiryLow != null) {
											year = dteExpiryLow.getYear() + 1900;
											month = dteExpiryLow.getMonth();
											day = dteExpiryLow.getDate();
										}
									%>
									<div class="form-group date-input-text">
										<liferay-ui:input-date name="expirationDateLow" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= day %>" />
									</div>
								</td>
								<td class="date-hyphen">~</td>
								<td>
									<%
										year = 0;
										month = -1;
										day = 0;
										
										if (dteExpiryHigh != null) {
											year = dteExpiryHigh.getYear() + 1900;
											month = dteExpiryHigh.getMonth();
											day = dteExpiryHigh.getDate();
										}
									%>
									<div class="form-group date-input-text">
										<liferay-ui:input-date name="expirationDateHigh" yearValue="<%= year %>" monthValue="<%= month %>" dayValue="<%= day %>" />
									</div>
								<aui:input name="role_token" type="hidden" value="<%= iRole %>" />
								</td>
							</tr>
							
							<tr>
								<td class="filter-modal-label"><liferay-ui:message key="label-account-lock" /></td>
								<td>
									<aui:select name="account_lock" label="">
										<%
										boolean bAccountUnlockSelected = false;
										boolean bAccountLockSelected = false;
										
										if (iAccountLock == 1) {
											bAccountUnlockSelected = true;
										} else if (iAccountLock == 2) {
											bAccountLockSelected = true;
										}
										%>
										
										<aui:option value="<%= 0 %>"></aui:option>
										<aui:option value="<%= 1 %>" selected="<%= bAccountUnlockSelected %>">
											<liferay-ui:message key="label-unlock" />
										</aui:option>
										<aui:option value="<%= 2 %>" selected="<%= bAccountLockSelected %>">
											<liferay-ui:message key="label-lock" />
					 					</aui:option>
									
									</aui:select>
								</td>
							</tr>
						</table>
						
					</div>
								
					<div class="modal-footer">
						<button type="button" class="btn" id="filterGroupBtn" onClick="<%= taglibSearchURL %>"><liferay-ui:message key="button-filter" /></button>
						<button type="button" class="btn" data-dismiss="modal"><liferay-ui:message key="button-cancel" /></button>
					</div>
				</div>
			</div>
		</div>
		
		<%
			if ((bIsFromSearch || bIsSearched) && strSearchInfo != null && !strSearchInfo.isEmpty()) {
				%>
					<div class="alert alert-info">
						<liferay-ui:message arguments="<%= HtmlUtil.escape(strSearchInfo) %>" key="<%= PortalConstants.KEY_FILTER_LIST %>" localizeKey="<%= false %>" />
					</div>
				<%
			}
		%>
		
		<%-- <liferay-ui:user-search-container-results userParams="<%= userParams %>" /> --%>
		
		<liferay-ui:search-container-results>
		<c:choose>
			<c:when test="<%= !bShowDBConnError && bNoUserList && !bIsFromSearch && !bIsSearched %>">
				<%
				results = ListUtil.subList(userList, start, end);
				userSearchContainer.setResults(results);
				userSearchContainer.setTotal(userListTotal);
				
				
				String result = "";
				if(userList != null){
					for(UserItem u: userList){
						result += u.getuId() +",";
					}
				}
				
				userList = null;
				%>
			</c:when>
			<c:otherwise>
					<% 
						if (userList == null) {
							userList = new ArrayList<UserItem>();
						}

						if (request.getParameter("cur") == null) {
							curPage = 1;
						} else {
							curPage = Integer.parseInt(request.getParameter("cur").toString());
						}

						start = (curPage - 1) * hookDelta;
						end = curPage * hookDelta;
						
						final boolean bShowDBConnError1 = bShowDBConnError;
						
						results = ListUtil.subList(userList, start, end);
						userSearchContainer.setResults(results);
						userSearchContainer.setTotal(userList.size());
						
						userList = null;
					%>
			</c:otherwise>
		</c:choose>
	</liferay-ui:search-container-results>

		<%-- <liferay-ui:search-container-row
			className="com.liferay.portal.kernel.model.User"
			escapedModel="<%= true %>"
			keyProperty="userId"
			modelVar="user2"
			rowIdProperty="screenName"
		> --%>
		
		<liferay-ui:search-container-row
			className="jp.ubsecure.portal.jubjub.portlet.model.UserItem"
			escapedModel="<%= true %>"
			keyProperty="uId"
			modelVar="user2"
			rowIdProperty="screenName"
		>
			<liferay-portlet:renderURL varImpl="rowURL">
				<portlet:param name="mvcRenderCommandName" value="/users_admin/edit_user" />
				<portlet:param name="redirect" value="<%= userSearchContainer.getIteratorURL().toString() %>" />
				<portlet:param name="p_u_i_d" value="<%= String.valueOf(user2.getuId()) %>" />
			</liferay-portlet:renderURL>

			<%
			if (!UserPermissionUtil.contains(permissionChecker, user2.getuId(), ActionKeys.UPDATE)) {
				rowURL = null;
			}

			boolean organizationContextView = false;
			long organizationGroupId = 0;
			%>

			<%@ include file="/user/search_columns.jspf" %>

			<liferay-ui:search-container-column-jsp
				cssClass="entry-action-column"
				name="label-action"
				path="/user_action.jsp"
			/>
		</liferay-ui:search-container-row>

		<%
		/* List<User> results = searchContainer.getResults(); */

		showDeleteButton = !results.isEmpty() && showDeleteButton;
		showRestoreButton = !results.isEmpty() && showRestoreButton;
		%>

		<%
		if (!showDeleteButton && !showRestoreButton) {
			userSearchContainer.setRowChecker(null);
		}
		%>

		<liferay-ui:search-iterator />
	</liferay-ui:search-container>
</aui:form>

<%-- <c:if test="<%= hasAddUserPermission %>">
	<liferay-frontend:add-menu>
		<portlet:renderURL var="viewUsersURL">
			<portlet:param name="toolbarItem" value="<%= toolbarItem %>" />
			<portlet:param name="usersListView" value="<%= usersListView %>" />
		</portlet:renderURL>

		<portlet:renderURL var="addUserURL">
			<portlet:param name="mvcRenderCommandName" value="/users_admin/edit_user" />
			<portlet:param name="redirect" value="<%= viewUsersURL %>" />
		</portlet:renderURL>

		<liferay-frontend:add-menu-item title='<%= LanguageUtil.get(request, "user") %>' url="<%= addUserURL.toString() %>" />
	</liferay-frontend:add-menu>
</c:if> --%>

<aui:script>
	Liferay.Util.toggleSearchContainerButton('#<portlet:namespace />deactivate', '#<portlet:namespace /><%= searchContainerReference.getId(request, "userSearchContainer") %>SearchContainer', document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');
	Liferay.Util.toggleSearchContainerButton('#<portlet:namespace />restore', '#<portlet:namespace /><%= searchContainerReference.getId(request, "userSearchContainer") %>SearchContainer', document.<portlet:namespace />fm, '<portlet:namespace />allRowIds');
</aui:script>

<script>
	define._amd = define.amd;
	define.amd = false;
</script>

<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.10.1.min.js" ></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/main.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery.fileDownload.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$("#<portlet:namespace />uid").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	
		
		$("#<portlet:namespace />user_name").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	 
		
		$("#<portlet:namespace />group_name").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	 
		
		$("#<portlet:namespace />mail_address").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	 
		
		$("#<portlet:namespace />user_role").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	 
		
		$("#<portlet:namespace />expirationDateLow").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	 
		
		$("#<portlet:namespace />expirationDateHigh").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	 
		
		$("#<portlet:namespace />account_lock").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});	 
		
		$("#<portlet:namespace />lastLoginDateLow").keypress(function(event){
			console.log("test");
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});
		
		$("#<portlet:namespace />lastLoginDateHigh").keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode =='13'){
					event.preventDefault();
					$("#filterGroupBtn").trigger('click');
				}
		});
		
		$(function() {
			var focusedElement = null;
			$(document).on('focus', 'input', function() {
				if (focusedElement == $(this)) return;
				focusedElement = $(this);
				setTimeout(function() {
					focusedElement.select();
				}, 50);
			});
		});
		
		var selectedGroup = $("#<portlet:namespace />selectedGroup").val();
		$("#<portlet:namespace />group_name > option").each(function() {
			if (this.value == selectedGroup) {
				this.selected = true;
			}
		});
	});
	
	$("#<portlet:namespace />lastLoginDateLow").click(function() {
	   changeZIndex();
	});
	
	$("#<portlet:namespace />lastLoginDateHigh").click(function() {
	   changeZIndex();
	});
	
	$("#<portlet:namespace />expirationDateLow").click(function() {
	   changeZIndex();
	});
	
	$("#<portlet:namespace />expirationDateHigh").click(function() {
	   changeZIndex();
	});
	
	function changeZIndex () {
		var datePicker = document.getElementsByClassName("datepicker-popover");
		   
		   if (datePicker != null) {
			   for (var index = 0; index < datePicker.length; index++) {
				   $(datePicker[index]).css("z-index", 2000);
			   }
		   }
	}
</script>

<script>
	define.amd = define._amd;
</script>