<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>

<%
long organizationId = ParamUtil.getLong(request, "organizationId");

Organization organization = OrganizationServiceUtil.fetchOrganization(organizationId);

long parentOrganizationId = ParamUtil.getLong(request, "parentOrganizationSearchContainerPrimaryKeys", (organization != null) ? organization.getParentOrganizationId() : OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID);

String parentOrganizationName = ParamUtil.getString(request, "parentOrganizationName");

if (parentOrganizationId <= 0) {
	parentOrganizationId = OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID;

	if (organization != null) {
		parentOrganizationId = organization.getParentOrganizationId();
	}
}

String type = BeanParamUtil.getString(organization, request, "type", PropsValues.ORGANIZATIONS_TYPES[0]);
long regionId = BeanParamUtil.getLong(organization, request, "regionId");
long countryId = BeanParamUtil.getLong(organization, request, "countryId");

long groupId = 0;

String groupName = null;
boolean blnCxSuiteValue = false;
boolean blnAndroidValue = false;
boolean blnIOSValue = false;
boolean blnVexValue = false;

PortletSession pSession = renderRequest.getPortletSession(false);
Object oGroupName = null;
Object oCxSuiteValue = null;
Object oAndroidValue = null;
Object oIOSValue = null;
Object oVexValue = null;

if (organization != null) {
	groupId = organization.getGroupId();
}

User selUser = (User)request.getAttribute("user.selUser");

boolean bShowDBConnError = false;

try {
	LinkedHashMap<String, Object> userParams = new LinkedHashMap<String, Object>();
	UserLocalServiceUtil.searchCount(themeDisplay.getCompanyId(), null, WorkflowConstants.STATUS_INACTIVE, userParams);
} catch (Exception e) {
	bShowDBConnError = true;
}

if (!bShowDBConnError && organization != null) {
	groupName = organization.getName();
	blnCxSuiteValue = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute("cxServiceUseAuth"));
	blnAndroidValue = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute("androidServiceUseAuth"));
	blnIOSValue = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute("iOSServiceUseAuth"));
	blnVexValue = GetterUtil.getBoolean(organization.getExpandoBridge().getAttribute("vexServiceUseAuth"));
}
%>

<liferay-util:buffer var="removeOrganizationIcon">
	<liferay-ui:icon
		iconCssClass="icon-remove"
		label="<%= true %>"
		message="remove"
	/>
</liferay-util:buffer>

<liferay-ui:error-marker key="<%= WebKeys.ERROR_SECTION %>" value="details" />

<aui:model-context bean="<%= organization %>" model="<%= Organization.class %>" />

<%
	pSession = renderRequest.getPortletSession(false);
	oGroupName = pSession.getAttribute(PortalConstants.NAME, PortletSession.PORTLET_SCOPE);
	oCxSuiteValue = pSession.getAttribute(PortalConstants.CXSUITE, PortletSession.PORTLET_SCOPE);
	oAndroidValue = pSession.getAttribute(PortalConstants.ANDROID, PortletSession.PORTLET_SCOPE);
	oIOSValue = pSession.getAttribute(PortalConstants.IOS, PortletSession.PORTLET_SCOPE);
	oVexValue = pSession.getAttribute(PortalConstants.VEX, PortletSession.PORTLET_SCOPE);
	
	if (oGroupName != null) {
		groupName = oGroupName.toString();
	}
	
	if (oCxSuiteValue != null) {
		blnCxSuiteValue = Boolean.valueOf(oCxSuiteValue.toString());
	}
	
	if (oAndroidValue != null) {
		blnAndroidValue = Boolean.valueOf(oAndroidValue.toString());
	}
	
	if (oIOSValue != null) {
		blnIOSValue = Boolean.valueOf(oIOSValue.toString());
	}
	
	if (oVexValue != null) {
		blnVexValue = Boolean.valueOf(oVexValue.toString());
	}
	
	pSession.removeAttribute(PortalConstants.NAME, PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute(PortalConstants.CXSUITE, PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute(PortalConstants.ANDROID, PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute(PortalConstants.IOS, PortletSession.PORTLET_SCOPE);
	pSession.removeAttribute(PortalConstants.VEX, PortletSession.PORTLET_SCOPE);
	
	 if (groupName != null && !groupName.isEmpty()) {
		if (organization != null) {
			organization.setName(groupName);
		}
	}  
%>

<div class="row">
	<aui:fieldset cssClass="col-md-6">
		<table>
			<tr>
				<td style="width:130px; height:70px;"><liferay-ui:message
						key="label-group-name" /><span style="color:red">*</span></td style="padding:20px 0px 10px 0px">
				<td><aui:input
						autoFocus="<%=windowState.equals(WindowState.MAXIMIZED)%>"
						name="name" label="" type="text" value="<%= groupName %>" /></td>
			</tr>

			<tr>
				<td style="padding-top: 2px; vertical-align:top;" rowspan="4"><liferay-ui:message
						key="label-available-services" /></td>
				<td style="padding: 10px 0px 5x 0px"><aui:input
						type="checkbox" name="cxsuite" label="label-cx-suite"
						value="<%=blnCxSuiteValue%>" /></td>
			</tr>
			<tr>
				<td style="padding: 10px 0px 0px 0px"><aui:input type="checkbox" name="android"
						label="label-android" value="<%=blnAndroidValue%>" /></td>
			</tr>
			<tr>
				<td style="padding: 10px 0px 0px 0px"><aui:input type="checkbox" name="ios"
						label="label-ios" value="<%=blnIOSValue%>" /></td>
			</tr>
			<tr>
				<td style="padding: 10px 0px 10px 0px"><aui:input type="checkbox" name="vex"
						label="label-vex" value="<%=blnVexValue%>" /></td>
			</tr>
		</table>

		<c:choose>
			<c:when
				test="<%=PropsValues.FIELD_ENABLE_COM_LIFERAY_PORTAL_MODEL_ORGANIZATION_STATUS%>">
				<liferay-ui:error
					key="<%=NoSuchListTypeException.class.getName() + Organization.class.getName() + ListTypeConstants.ORGANIZATION_STATUS%>"
					message="please-select-a-type" />

				<aui:select label="status"
					listType="<%=ListTypeConstants.ORGANIZATION_STATUS%>"
					listTypeFieldName="statusId" name="statusId"
					showEmptyOption="<%=true%>" />
			</c:when>
			<c:otherwise>
				<aui:input name="statusId" type="hidden"
					value="<%=(organization != null) ? organization.getStatusId() : ListTypeConstants.ORGANIZATION_STATUS_DEFAULT%>" />
			</c:otherwise>
		</c:choose>

		<aui:input name="type" type="hidden" value="organization" />
	</aui:fieldset>
</div>

<%
Organization parentOrganization = null;

if ((organization == null) && (parentOrganizationId == OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID) && !permissionChecker.isCompanyAdmin()) {
	List<Organization> manageableOrganizations = new ArrayList<Organization>();

	for (Organization curOrganization : user.getOrganizations()) {
		if (OrganizationPermissionUtil.contains(permissionChecker, curOrganization, ActionKeys.MANAGE_SUBORGANIZATIONS)) {
			manageableOrganizations.add(curOrganization);
		}
	}

	if (manageableOrganizations.size() == 1) {
		parentOrganizationId = manageableOrganizations.get(0).getOrganizationId();
	}
}

if (parentOrganizationId != OrganizationConstants.DEFAULT_PARENT_ORGANIZATION_ID) {
	try {
		if (!bShowDBConnError) {
			parentOrganization = OrganizationLocalServiceUtil.getOrganization(parentOrganizationId);
		}

		parentOrganizationName = parentOrganization.getName();
	}
	catch (NoSuchOrganizationException nsoe) {
	}
}

List<Organization> parentOrganizations = new ArrayList<Organization>();

if (parentOrganization != null) {
	parentOrganizations.add(parentOrganization);
}
%>

<c:if test="<%=organization == null%>">
	<aui:script use="aui-base">
		A.one('#<portlet:namespace />type').on(
			'change',
			function(event) {

				<%
		for (String curType : PropsValues.ORGANIZATIONS_TYPES) {
	%>

					if (event.target.val() == '<%=curType%>') {
						A.one('#<portlet:namespace />countryDiv').<%=GetterUtil.getBoolean(PropsUtil.get(PropsKeys.ORGANIZATIONS_COUNTRY_ENABLED, new Filter(String.valueOf(curType)))) ? "show" : "hide"%>();
					}

				<%
		}
	%>

			}
		);
	</aui:script>
</c:if>