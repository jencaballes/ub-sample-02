package jp.ubsecure.portal.jubjub.portlet.controller;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.util.PortalUtil;

public class ControllerHelper {
	public static HttpSession getHttpSession (PortletRequest portletRequest) {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(portletRequest);
		HttpServletRequest origRequest = PortalUtil.getOriginalServletRequest(request);
		return origRequest.getSession(false);
	}
}
