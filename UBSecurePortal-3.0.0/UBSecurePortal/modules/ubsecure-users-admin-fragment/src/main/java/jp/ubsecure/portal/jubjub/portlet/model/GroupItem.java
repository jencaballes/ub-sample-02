package jp.ubsecure.portal.jubjub.portlet.model;

public class GroupItem {
	private long groupId;
	private String groupName;
	private int noOfUsers;
	private int services;
	
	public long getGroupId() {
		return groupId;
	}
	
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public int getNoOfUsers() {
		return noOfUsers;
	}
	
	public void setNoOfUsers(int noOfUsers) {
		this.noOfUsers = noOfUsers;
	}
	
	public int getServices() {
		return services;
	}
	
	public void setServices(int services) {
		this.services = services;
	}
	
	
}
