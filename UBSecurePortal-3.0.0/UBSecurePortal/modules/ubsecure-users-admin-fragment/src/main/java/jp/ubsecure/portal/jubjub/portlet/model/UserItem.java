package jp.ubsecure.portal.jubjub.portlet.model;


public class UserItem {
	private long uId;
	private String groupName;
	private String userId;
	private String username;
	private String emailAddress;
	private String role;
	private String lastLoginDate;
	private String expiryDate;
	private boolean accountLock;
	
	public long getuId() {
		return uId;
	}
	
	public void setuId(long uId) {
		this.uId = uId;
	}
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public String getLastLoginDate() {
		return lastLoginDate;
	}
	
	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public boolean isAccountLock() {
		return accountLock;
	}
	
	public void setAccountLock(boolean accountLock) {
		this.accountLock = accountLock;
	}
}
