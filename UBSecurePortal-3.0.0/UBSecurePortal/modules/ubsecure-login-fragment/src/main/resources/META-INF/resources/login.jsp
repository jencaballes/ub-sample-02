<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>

<%@ page import="com.liferay.portal.kernel.util.PortalUtil" %>

<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalConstants" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalErrors" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.constants.PortalMessages" %>
<%@ page import="jp.ubsecure.portal.jubjub.portlet.custom.exception.UBSPortalUserPasswordException" %>

<%
	HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(renderRequest);
	HttpServletRequest origRequest = PortalUtil.getOriginalServletRequest(httpRequest);
	HttpSession httpSession = origRequest.getSession(false);
	Object oError = httpSession.getAttribute(PortalConstants.PARAM_ERROR);
	String strError = PortalConstants.STRING_EMPTY;
	
	if (oError != null) {
		strError = oError.toString();
	}
	
	if (!themeDisplay.isSignedIn() && (strError != null && !strError.isEmpty())) {
		if (strError.equalsIgnoreCase(PortalErrors.DOWNLOAD_MANUAL_NO_MANUAL)) {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.NO_MANUAL %>" />
			</div>
			<%
		} else {
			%>
			<div class="alert alert-danger">
				<liferay-ui:message key="<%= PortalMessages.DOWNLOAD_MANUAL_FAILED %>" />
			</div>
			<%
		}
		
		httpSession.removeAttribute(PortalConstants.PARAM_ERROR);
	}
%>

<c:choose>
	<c:when test="<%= themeDisplay.isSignedIn() %>">

		<%
		String signedInAs = HtmlUtil.escape(user.getScreenName());

		if (themeDisplay.isShowMyAccountIcon() && (themeDisplay.getURLMyAccount() != null)) {
			signedInAs = user.getFirstName();
		}
		%>
	</c:when>
	<c:otherwise>

		<%
		String formName = "loginForm";

		if (windowState.equals(LiferayWindowState.EXCLUSIVE)) {
			formName += "Modal";
		}

		String redirect = ParamUtil.getString(request, "redirect");

		String login = LoginUtil.getLogin(request, "login", company);
		String password = StringPool.BLANK;
		boolean rememberMe = ParamUtil.getBoolean(request, "rememberMe");

		if (Validator.isNull(authType)) {
			authType = company.getAuthType();
		}
		%>

		<portlet:actionURL name="/login/login" secure="<%= PropsValues.COMPANY_SECURITY_AUTH_REQUIRES_HTTPS || request.isSecure() %>" var="loginURL">
			<portlet:param name="mvcRenderCommandName" value="/login/login" />
		</portlet:actionURL>

		<aui:form action="<%= loginURL %>" autocomplete='<%= PropsValues.COMPANY_SECURITY_LOGIN_FORM_AUTOCOMPLETE ? "on" : "off" %>' cssClass="sign-in-form" method="post" name="<%= formName %>" onSubmit="event.preventDefault();">
			<aui:input name="saveLastPath" type="hidden" value="<%= false %>" />
			<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
			<aui:input name="doActionAfterLogin" type="hidden" value="<%= portletName.equals(PortletKeys.FAST_LOGIN) ? true : false %>" />

			<div class="inline-alert-container lfr-alert-container"></div>

			<liferay-ui:error key="ORMException" message="error-orm-exception" />
			<liferay-ui:error key="expiredUser" message="error-account-has-expired" />
			<liferay-ui:error exception="<%= AuthException.class %>" message="error-authentication-failed" />
			<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-login-because-the-maximum-number-of-users-has-been-reached" />
			<liferay-ui:error exception="<%= CookieNotSupportedException.class %>" message="error-authentication-failed" />
			<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="error-authentication-failed" />
			<liferay-ui:error exception="<%= UserEmailAddressException.MustNotBeNull.class %>" message="error-authentication-failed" />
			<liferay-ui:error exception="<%= UserLockoutException.LDAPLockout.class %>" message="error-account-has-been-locked" />
			<liferay-ui:error exception="<%= UserLockoutException.PasswordPolicyLockout.class %>" message="error-account-has-been-locked" />
			<liferay-ui:error exception="<%= UBSPortalUserPasswordException.MustNotExceedMaximumBytes.class %>" message="error-authentication-failed" />
			<liferay-ui:error exception="<%= UserScreenNameException.MustNotBeNull.class %>" message="error-authentication-failed" />
			<liferay-ui:error exception="<%= UserPasswordException.MustNotBeNull.class %>" message="error-authentication-failed" />
			
			<aui:fieldset>

				<%
				String loginLabel = null;

				if (authType.equals(CompanyConstants.AUTH_TYPE_EA)) {
					loginLabel = "user-id"; //"email-address";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_SN)) {
					loginLabel = "screen-name";
				}
				else if (authType.equals(CompanyConstants.AUTH_TYPE_ID)) {
					loginLabel = "id";
				}
				%>
				<div class="sign-in-modal-header" ></div>
				<div>
	 		      	<table> 		
				        <tr>
				            <td class="sign-in-label">	
				    			<liferay-ui:message key="label-user-id"/>
				            </td>					          
				            <td>			                		            			            
				                <aui:input id="useridlogin" autoFocus="<%= windowState.equals(LiferayWindowState.EXCLUSIVE) || windowState.equals(WindowState.MAXIMIZED) %>" cssClass="clearable"  label="" name="login" showRequiredLabel="<%=false %>" type="text">
								</aui:input> 	
				            </td>					           
				        </tr>
				        <tr>					           
				            <td class="sign-in-label">
				            	<liferay-ui:message key="label-password"/>	
				            </td>					       
				            <td>			                		                
								<aui:input  name="password" showRequiredLabel="<%=false %>" type="password" value="" label="" >
								</aui:input>						
				            </td>					            
				        </tr>			        
															
						<tr>
						<span id="<portlet:namespace />passwordCapsLockSpan" style="display:none;"><liferay-ui:message key="caps-lock-is-on" /></span>					
						</tr>
	               	</table>
               	</div>
			</aui:fieldset>
			<aui:button-row>
				<aui:button cssClass="btn-lg" type="submit" value="button-log-in" />
			</aui:button-row>
		</aui:form>

		<aui:script sandbox="<%= true %>">
			var form = AUI.$(document.<portlet:namespace /><%= formName %>);

			form.on(
				'submit',
				function(event) {
					<c:if test="<%= Validator.isNotNull(redirect) %>">
						var redirect = form.fm('redirect');

						if (redirect) {
							var redirectVal = redirect.val();

							redirect.val(redirectVal + window.location.hash);
						}
					</c:if>

					submitForm(form);
				}
			);

			form.fm('password').on(
				'keypress',
				function(event) {
					Liferay.Util.showCapsLock(event, '<portlet:namespace />passwordCapsLockSpan');
				}
			);
		</aui:script>
	</c:otherwise>
</c:choose>