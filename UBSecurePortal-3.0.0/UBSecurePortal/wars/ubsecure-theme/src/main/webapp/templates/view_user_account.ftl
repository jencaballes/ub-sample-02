<#assign
	is_impersonated = theme_display.isImpersonated()
/>
<@liferay_util["buffer"] var="userName">
	<#if is_impersonated>
		<b class="alert-icon icon-warning-sign"></b>
	</#if>
	
	<img class="user-avatar-image" src="/o/ubsecure-theme/images/user_male_portrait.png" />
	
	<span class="user-full-name">
		${htmlUtil.escape(user_first_name)}
	</span>
</@>

<#if is_signed_in>
	<#if is_impersonated>
		<#assign user_avatar_css = "user-avatar impersonating-user" />
	<#else>
		<#assign user_avatar_css = "user-avatar" />
	</#if>
	<@liferay_aui["nav-item"] anchorCssClass="user-avatar-link" cssClass="${user_avatar_css}" dropdown=true id="userAvatar" label="${userName}">
		
		<#--<#if is_impersonated>
			<#assign
				impersonating_user_label = "you-are-impersonating-the-guest-user"
				impersonating_user_label = languageUtil.format(locale, "you-are-impersonating-x", [htmlUtil.escape(user_name)])
			/>
			
			<div class="alert alert-info">${impersonating_user_label}</div>
			
			<@liferay_util["buffer"] var="leaveImpersonationLabel">
				<@liferay_ui["message"] key="be-yourself-again" /> (htmlUtil.escape(user_name))
			</@>
			
			<@liferay_aui["nav-item"] href = portalUtil.getLayoutURL(layout, theme_display, false) label="${leaveImpersonationLabel}" />
			
			
		</#if>-->
		
		<#if is_setup_complete && theme_display.getURLMyAccount()??>
			<#assign
				ubsecure_url = theme_display.getURLMyAccount().toString()
				ubsecure_url = httpUtil.setParameter(ubsecure_url, "controlPanelCategory", "user")
			/>
			
			<#-- <@liferay_aui["nav-item"] href="${ubsecure_url}" iconCssClass="icon-user" label="button-password-change" title="button-password-change" useDialog=!is_impersonated /> -->
			<@liferay_aui["nav-item"] id="popupButton" iconCssClass="icon-user" label="button-password-change" />
			
			<@liferay_aui["script"] position="inline" use="liferay-util-window">
				var popupButton = A.one('#<@portlet.namespace />popupButton');
					popupButton.on('click', function(){
						Liferay.Util.openWindow({
							dialog: {
								destroyOnHide:true,
								//height:400,
								//width:400
							},
							id: 'password_change_dialog',
							uri: '${ubsecure_url}',
							title: Liferay.Language.get('button-password-change')
						});
					});
			</@>
			
			<#-- For closing -->
				<@liferay_aui["script"]>
					Liferay.provide(window, 'password_change_closepopup',
						function(dialogId) {
							var A=AUI();
							console.log(dialogId);
							var dialog= Liferay.Util.getWindow(dialogId);
							dialog.destroy();
							//Liferay.fire('closeWindow', {
							//	id:dialogId
							//});
						},
						['aui-base', 'liferay-util-window']
					);
				</@>
		</#if>
		
		<#assign
			serviceContext = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"].getServiceContext()
			httpServletRequest = serviceContext.getRequest()
			sessiona = httpServletRequest.getSession()
			useCxSuiteAttr = sessiona.getAttribute("useCxSuite")
			useAndroidAttr = sessiona.getAttribute("useAndroid")
			useIOSAttr = sessiona.getAttribute("useIOS")
			useVexAttr = sessiona.getAttribute("useVex")
		/>
		
		<#if is_signed_in && is_setup_complete>
			<@liferay_ui["icon-menu"] message="label-download-manual" icon="" cssClass="manual-dropdown">
				<#if useCxSuiteAttr>
					<@liferay_ui["icon"] message="label-cx-suite-manual" url="javascript:downloadCxManual('/c/portal/download_manual', 'cxsuite');" />
				</#if>
				
				<#if useAndroidAttr>
					<@liferay_ui["icon"] message="label-android-manual" url="javascript:downloadCxManual('/c/portal/download_manual', 'android');" />
				</#if>
				
				<#if useIOSAttr>
					<@liferay_ui["icon"] message="label-ios-manual" url="javascript:downloadCxManual('/c/portal/download_manual', 'ios');" />
				</#if>
				
				<#if useVexAttr>
					<@liferay_ui["icon"] message="label-vex-manual" url="javascript:downloadCxManual('/c/portal/download_manual', 'vex');" />
				</#if>
				
			</@>
		</#if>
		
		<#if show_sign_out>
			<@liferay_aui["nav-item"] cssClass="sign-out" href="${sign_out_url}" iconCssClass="icon-off" label="button-sign-out" />
		</#if>
	</@>
<#else>
	<#assign anchorData = {"redirect" : is_login_redirect_required} />
	
	<@liferay_aui["nav-item"] anchorData = anchorData cssClass="sign-in" href="${sign_in_url}" iconCssClass="icon-user" label="button-log-in" />
</#if>

<@liferay.js file_name = "${javascript_folder}/jquery-1.10.1.min.js" />
<@liferay.js file_name = "${javascript_folder}/jquery.fileDownload.js" />
<@liferay.js file_name = "${javascript_folder}/view_user_account.js" />