<#assign
	control_panel_category = theme_display.getURLControlPanel()
/>

<#if (show_control_panel || theme_display.isShowSiteAdministrationIcon()) && (!layout.getGroup().isControlPanel() || (control_panel_category != null && control_panel_category?starts_with("current_site")))>
	<#assign
		url_control_panel_category = httpUtil.setParameter(control_panel_category, "controlPanelCategory", "control_panel.users")
		url_control_panel_category = httpUtil.addParameter(url_control_panel_category, "p_p_lifecycle", 0)
		url_control_panel_category = httpUtil.addParameter(url_control_panel_category, "p_p_state", "maximized")
		url_control_panel_category = httpUtil.addParameter(url_control_panel_category, "p_p_id", "com_liferay_users_admin_web_portlet_UsersAdminPortlet")
	/>
	<@liferay_aui["nav-item"] cssClass="admin-links" dropdown=false id="adminLinks" label="label-management" href="${url_control_panel_category}" />
</#if>