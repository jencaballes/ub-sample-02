<@liferay_aui["nav"] cssClass="nav-account-controls" icon="user" id="navAccountControls">
	<#if is_signed_in>
		<#if is_setup_complete>
			<#include "${full_templates_path}/view_last_login_date.ftl" />
		</#if>
		
		<#assign expiryDate = user.getExpandoBridge().getAttribute("acctexpirationdate")?string["yyyy/MM/dd HH:mm"] />
		<#assign defaultDate = dateUtil.newDate(0)?string["yyyy/MM/dd HH:mm"] />
		
		<#if expiryDate != defaultDate>
			<#include "${full_templates_path}/view_account_expiration_date.ftl" />
		</#if>
	</#if>
	
	<#if is_setup_complete>
		<#include "${full_templates_path}/view_admin_links.ftl" />
	</#if>
	
	<#include "${full_templates_path}/download_manual.ftl" />
	
	<#include "${full_templates_path}/view_user_account.ftl" />
</@>