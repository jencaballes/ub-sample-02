<#--
This file allows you to override and define new FreeMarker variables.
-->

<#assign ubsecure_login_logo_src = "${images_folder}/login.png" />
<#assign ubsecure_logo_css = "${css_class} logo-ubsecure" />
<#assign ubsecure_top_logo_src = "${images_folder}/top_logo.png" />
