<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title}</title><#-- - ${company_name}-->

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />
	<meta http-equiv="Content-Security-Policy" content="default-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval' *.googleapis.com;">

	<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<#--<@liferay.control_menu />-->

<#include "${full_templates_path}/view_user_panel.ftl" />

<div class="container-fluid" id="wrapper">
	<header id="banner" role="banner" style="padding-top: 2em;">
		<div id="heading">
			<h1 class="site-title">
				<a class="${ubsecure_logo_css}" href="${site_default_url}">
					<#if is_signed_in>
						<img alt="Softbank" src="${ubsecure_top_logo_src}" />
						
						<#--<#if show_site_name>
							<span class="site-name">
								${site_name}
							</span>
						</#if>-->
					<#else>
						<img alt="Softbank" src="${ubsecure_login_logo_src}" />
					</#if>
				</a>
			</h1>
			
			<#--<h2 class="page-title">
				<span>$the_title</span>
			</h2>-->
		</div>

		<#if is_signed_in && is_setup_complete>
			<#include "${full_templates_path}/navigation.ftl" />
		</#if>
	</header>

	<section id="content" style="padding:10px; padding-bottom:30px;">
		<h1 class="hide-accessible">${the_title}</h1>

		<#--<nav id="breadcrumbs">
			<@liferay.breadcrumbs />
		</nav>-->
		
		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
	</section>
	
	<footer id="footer" role="contentinfo">
		<p class="powered-by">Serviced by UBsecure Inc.
			<#--<@liferay.language key="powered-by" /> <a href="http://www.liferay.com" rel="external">Liferay</a>-->
		</p>
	</footer>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

</body>

</html>