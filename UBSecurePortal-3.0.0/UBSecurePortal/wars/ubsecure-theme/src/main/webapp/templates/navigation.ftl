<nav class="${nav_css_class}" id="navigation" role="navigation">
	<div class="navigation-bar">
	<#--<h1 class="hide-accessible"><@liferay.language key="navigation" /></h1>-->

		<div class="navigation-bar-inner">
			<ul aria-label="<@liferay.language key="site-pages" />" role="menubar" class="site-pages">
				<#list nav_items as nav_item>
					<#assign
						nav_item_attr_has_popup = ""
						nav_item_attr_selected = ""
						nav_item_css_class = ""
						nav_item_layout = nav_item.getLayout()
					/>
		
					<#if nav_item.isSelected()>
						<#assign
							nav_item_attr_has_popup = "aria-haspopup='true'"
							nav_item_attr_selected = "aria-selected='true'"
							nav_item_css_class = "selected"
						/>
					</#if>
					
					<#assign serviceContext = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"].getServiceContext() />
					<#assign httpServletRequest = serviceContext.getRequest() />
					<#assign sessiona = httpServletRequest.getSession() />
					
					<#assign useCxSuiteAttr = sessiona.getAttribute("useCxSuite") />
					<#assign useAndroidAttr = sessiona.getAttribute("useAndroid") />
					<#assign useIOSAttr = sessiona.getAttribute("useIOS") />
					<#assign useVexAttr = sessiona.getAttribute("useVex") />
					<#assign strNavItem = nav_item.getName() />
					<#assign cxSuiteName = "ソースコード診断" />
					<#assign androidName = "Android静的解析" />
					<#assign iOSName = "iOS静的解析" />
					<#assign vexName = "Vex動的解析" />
					
					<#if nav_item.getName() == cxSuiteName
					|| nav_item.getName() == androidName
					|| nav_item.getName() == iOSName 
					|| nav_item.getName() == vexName >
						<#if (useCxSuiteAttr && (strNavItem == cxSuiteName))
						|| (useAndroidAttr && (strNavItem == androidName))
						|| (useIOSAttr && (strNavItem == iOSName))
						|| (useVexAttr && (strNavItem == vexName))
						|| (sessiona.getAttribute("userRole") == 2 || sessiona.getAttribute("userRole") == 5)>
							<li ${nav_item_attr_selected} class="lfr-nav-item ${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}" role="presentation">
								<a aria-labelledby="layout_${nav_item.getLayoutId()}" ${nav_item_attr_has_popup} href="${nav_item.getURL()}" ${nav_item.getTarget()} role="menuitem"><span><@liferay_theme["layout-icon"] layout=nav_item_layout /> ${nav_item.getName()}</span></a>
				
								<#if nav_item.hasChildren()>
									<ul class="child-menu" role="menu">
										<#list nav_item.getChildren() as nav_child>
											<#assign
												nav_child_attr_selected = ""
												nav_child_css_class = ""
											/>
				
											<#if nav_item.isSelected()>
												<#assign
													nav_child_attr_selected = "aria-selected='true'"
													nav_child_css_class = "selected"
												/>
											</#if>
				
											<li ${nav_child_attr_selected} class="${nav_child_css_class}" id="layout_${nav_child.getLayoutId()}" role="presentation">
												<a aria-labelledby="layout_${nav_child.getLayoutId()}" href="${nav_child.getURL()}" ${nav_child.getTarget()} role="menuitem">${nav_child.getName()}</a>
											</li>
										</#list>
									</ul>
								</#if>
							</li>
						</#if>
					</#if>
				</#list>
			</ul>
		</div>
	</div>
</nav>