<#assign
	expiry_date = user.getExpandoBridge().getAttribute("acctexpirationdate")?string["yyyy/MM/dd"]
 />

<#if is_signed_in>
	<@liferay_aui["nav-item"] cssClass="divider-vertical">
		<span class="nav-item-label"><@liferay_ui["message"] key="label-expiration-period" /> <@liferay_ui["message"] key="${expiry_date}" /></span>
	</@>
</#if>