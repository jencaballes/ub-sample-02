<#assign last_login_date = user.getLastLoginDate()?string["yyyy/MM/dd HH:mm"] />

<#if is_signed_in>
	<@liferay_aui["nav-item"] cssClass="divider-vertical">
		<span class="nav-item-label"><@liferay_ui["message"] key="label-last-login-period" /> <@liferay_ui["message"] key="${last_login_date}" /></span>
	</@>
</#if>